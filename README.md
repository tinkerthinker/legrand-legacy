# README #

This README would normally document whatever steps are necessary to get your application up and running. Please [learn Markdown](https://bitbucket.org/tutorials/markdowndemo) before editing.

## Legrand Legacy ##

We are using this repository for **source code** only. Assets such as audio, textures (raw) and sprite collections (processed) are managed in our cloud drive (Copy). Try to keep .gitignore up-to-date with such files and directories.

Plugins will also be committed in case we need to change something. Remember to disable .dll in global gitignore (Source Tree > Tools > Option > Git > Edit Global Ignore List)

## Development Environments ##

Unity3D 5.1:

* Visible meta files

* Asset Mode -> Force Text

Plugins:

* DOTween

* USequencer

* RAIN

* Advanced Pooling

* Dialoguer

Display:

* Aspect ratio: 4:3 (minimum) to 16:9 (maximum)

* Native resolution: 1280x720
Contribution
##  guidelines ##

Our main rule is TRY NOT TO BREAK ANYTHING. Projects should always can be build successfully.

1. Make branch when creating big modules. Or small modules. 

2. Try to make tests.

### Code Conventions ###

```csharp

// One space after comments, Pascal case for class name
public class ClassName
{
    // All caps for static variables
    public static const int CLASS_CONST = 1;

    // Pascal case for public variable
    public string PublicVariable;

    // Pascal case with _ for private variables
    private string _PrivateVariable;
    public string PrivateVariable
    {
        get { return _PrivateVariable; }
    }

    // Pascal case for all functions (public, private, default)
    public int PublicFunction(int param)
    {
        // Camel case for local variables
        int someSum = 0;

        return someSum;
    }

    // Naming explains what, Comments explains WHY

    // Answer is always the same
    private int CalculateLifeAnwers()
    {
        return 42;
    }
}

```