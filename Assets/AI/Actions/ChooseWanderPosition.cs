using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using RAIN.Action;
using RAIN.Core;
using RAIN.Representation;
using RAIN.Navigation;
using RAIN.Navigation.Graph;

[RAINAction("Choose Wander Position")]
public class ChooseWanderPosition : RAINAction
{
    /// <summary>
    /// Public Expressions are editable in the Behavior Editor
    /// WanderDistance is the max range to use when picking a wander target
    /// </summary>
    public Expression WanderDistance = new Expression();

    /// <summary>
    /// Public Expressions are editable in the Behavior Editor
    /// StayOnGraph is a boolean (true/false) that indicates whether the wander target must be on the nav graph
    /// </summary>
    public Expression StayOnGraph = new Expression();

    /// <summary>
    /// Public Expressions are editable in the Behavior Editor
    /// WanderTargetVariable is the name of the variable that the result will be assigned to
    /// *Don't use quotes when typing in the variable name
    /// </summary>
    public Expression WanderTargetVariable = new Expression();

	/// <summary>
	/// SuccessToWanderVariable is the name of the variable that the result will be assigned to
	/// </summary>
	/// public Expression SuccessToWanderVariable = new Expression();

    /// <summary>
    /// The default wander distance to use when the WanderDistance is invalid
	/// *Don't use quotes when typing in the variable name
    /// </summary>
	private float _defaultWanderDistance = 10f;

    public override ActionResult Execute(RAIN.Core.AI ai)
    {
        if (!WanderTargetVariable.IsVariable)
            throw new Exception("The Choose Wander Position node requires a valid Wander Target Variable");

		/*
		if (!SuccessToWanderVariable.IsVariable)
			throw new Exception ("The Choose Wander Position node requires a valid Success To Wander Variable");
		*/
	
        float tWanderDistance = 0f;
        if (WanderDistance.IsValid)
            tWanderDistance = WanderDistance.Evaluate<float>(ai.DeltaTime, ai.WorkingMemory);

        if (tWanderDistance <= 0f)
            tWanderDistance = _defaultWanderDistance;

       

        List<RAINNavigationGraph> found = new List<RAINNavigationGraph>();
        Vector3 tDestination = Vector3.zero;
        int tried = 0;
        do
        {
            if (tried == 10) return ActionResult.FAILURE;
            Vector3 tDirection = new Vector3(LegrandUtility.Random(-1f, 1f), 0f, LegrandUtility.Random(-1f, 1f));
            tDirection *= tWanderDistance;
            tDestination =  ai.Kinematic.Position + tDirection;
            found = NavigationManager.Instance.GraphsForPoints(ai.Kinematic.Position, tDestination, ai.Motor.MaxHeightOffset, NavigationManager.GraphType.Navmesh, ((BasicNavigator)ai.Navigator).GraphTags);
            tried++;
        }
        while ((Vector3.Distance(ai.Kinematic.Position, tDestination) < 2f) || (found.Count == 0));
        ai.WorkingMemory.SetItem<Vector3>(WanderTargetVariable.VariableName, tDestination);

		//ai.WorkingMemory.SetItem<bool> (SuccessToWanderVariable.VariableName, true);
        return ActionResult.SUCCESS;
    }
}