using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RAIN.Action;
using RAIN.Core;
using RAIN.Entities;
[RAINAction]
public class Die : RAINAction
{
    public override void Start(RAIN.Core.AI ai)
    {
        base.Start(ai);
    }

    public override ActionResult Execute(RAIN.Core.AI ai)
    {
		//COLLIDERS
		Collider[] activeColliders = ai.Body.GetComponents<Collider>();
		foreach (Collider tCollider in activeColliders)
			if (tCollider != null)
				GameObject.DestroyImmediate(tCollider);
		
		//ENTITY RIGS ARE DEACTIVATED, NOT DESTROYED
		EntityRig tEntityRig = ai.Body.GetComponentInChildren<EntityRig>();
		if (tEntityRig != null)
			tEntityRig.Entity.DeactivateEntity();
		
		//RIGID BODIES (only 1 expected)
		Rigidbody tRigidBody = ai.Body.GetComponent<Rigidbody>();
		if (tRigidBody != null)
			tRigidBody.isKinematic = true;

		Renderer[] renderers = ai.Body.GetComponentsInChildren<Renderer> ();
		foreach(Renderer renderer in renderers)
			if (renderer != null)
				renderer.enabled = false;

        return ActionResult.SUCCESS;
    }

    public override void Stop(RAIN.Core.AI ai)
    {
        base.Stop(ai);
    }
}