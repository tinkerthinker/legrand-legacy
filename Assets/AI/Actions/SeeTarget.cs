using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RAIN.Action;
using RAIN.Core;
using RAIN.Representation;
using DG.Tweening;

[RAINAction]
public class SeeTarget : RAINAction
{
	public Expression Target;

    public override void Start(RAIN.Core.AI ai)
    {
        base.Start(ai);
    }

    public override ActionResult Execute(RAIN.Core.AI ai)
    {
		Vector3 TargetPosition = new Vector3();
		if(Target.Evaluate (ai.DeltaTime, ai.WorkingMemory).IsType<Vector3>())
			TargetPosition = Target.Evaluate (ai.DeltaTime, ai.WorkingMemory).GetValue<Vector3> ();
		else
			TargetPosition = Target.Evaluate (ai.DeltaTime, ai.WorkingMemory).GetValue<GameObject> ().transform.position;

		Vector3 currentRotation = ai.Body.transform.eulerAngles;
		ai.Body.transform.LookAt (TargetPosition);
		Vector3 newRotation = ai.Body.transform.eulerAngles;
		ai.Body.transform.eulerAngles = currentRotation;

		ai.Body.transform.DORotate (newRotation, 2f);


        return ActionResult.SUCCESS;
    }

    public override void Stop(RAIN.Core.AI ai)
    {
        base.Stop(ai);
    }
}