using UnityEngine;
using RAIN.Action;

[RAINAction]
public class SearchEnemy : RAINAction
{
    public override void Start(RAIN.Core.AI ai)
    {
        base.Start(ai);
    }

    public override ActionResult Execute(RAIN.Core.AI ai)
    {
        //ai.WorkingMemory.SetItem("EnemyTarget", War.TacticController.Instance.ListPlayer[0].gameObject);
        return ActionResult.SUCCESS;
    }

    public override void Stop(RAIN.Core.AI ai)
    {
        base.Stop(ai);
    }
}