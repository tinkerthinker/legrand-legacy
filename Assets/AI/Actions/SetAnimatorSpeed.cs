﻿using UnityEngine;
using System.Collections;
using RAIN.Action;
using RAIN.Core;
using RAIN.Representation;

[RAINAction]
public class SetAnimatorSpeed : RAINAction
{
    public Expression animSpeed = new Expression();

    public override ActionResult Execute(AI ai)
    {
        float speed = animSpeed.Evaluate<float>(ai.DeltaTime, ai.WorkingMemory);

        if (speed > 0)
            ai.Body.transform.GetComponent<Animator>().speed = speed;

        return ActionResult.SUCCESS;
    }
}
