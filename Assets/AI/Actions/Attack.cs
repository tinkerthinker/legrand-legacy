using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RAIN.Action;
using RAIN.Core;
using RAIN.Representation;

[RAINAction]
public class Attack : RAINAction
{
    public Expression Target = new Expression();

    public override void Start(RAIN.Core.AI ai)
    {
        base.Start(ai);
    }

    public override ActionResult Execute(RAIN.Core.AI ai)
    {
        //Target.Evaluate(ai.DeltaTime,ai.WorkingMemory).GetValue<GameObject>().GetComponent<Troop>().TryDamage(ai.WorkingMemory.GetItem<int>("Damage"), ai.Body);
        return ActionResult.SUCCESS;
    }

    public override void Stop(RAIN.Core.AI ai)
    {
        base.Stop(ai);
    }
}