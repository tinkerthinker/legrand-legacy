﻿/* Bink Player for Unity3D v0.9
 * by AndrewMulti
 * BinkPostprocessor.cs copies all videos and subtitles to build folder
 */

using UnityEngine;
using System.Collections;
using System.IO;
using UnityEditor;
using UnityEditor.Callbacks;

public class BinkPostprocessor {
    public static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
    {
        DirectoryInfo dir = new DirectoryInfo(sourceDirName);
        DirectoryInfo[] dirs = dir.GetDirectories();

        if (!dir.Exists)
            throw new DirectoryNotFoundException("Source directory does not exist or could not be found: " + sourceDirName);
        if (!Directory.Exists(destDirName))
            Directory.CreateDirectory(destDirName);
        FileInfo[] files = dir.GetFiles();
        foreach (FileInfo file in files)
        {
            string temppath = Path.Combine(destDirName, file.Name);
            if ((Path.GetExtension(file.FullName) == ".bik") || (Path.GetExtension(file.FullName) == ".xml"))
                file.CopyTo(temppath, true);
        }
        if (copySubDirs)
        {
            foreach (DirectoryInfo subdir in dirs)
            {
                string temppath = Path.Combine(destDirName, subdir.Name);
                DirectoryCopy(subdir.FullName, temppath, copySubDirs);
            }
        }
    }

    [PostProcessBuild]
    public static void OnPostprocessBuild(BuildTarget target, string pathToBuiltProject)
    {
        var projName = Path.GetFileNameWithoutExtension(pathToBuiltProject);
        DirectoryCopy(Application.dataPath + "\\Video", Path.GetDirectoryName(pathToBuiltProject) + "\\" + projName + "_Data\\Video", true);
    }
}
