﻿using UnityEngine;
using System.Collections;

public class TestTimeScaleInput : MonoBehaviour {
	public float TimeScale;

	void Start()
	{
		TimeScale = Time.timeScale;
	}

	public void ChangeState()
	{
        if (GlobalGameStatus.Instance.IsStateNormal())
            GlobalGameStatus.Instance.StateStatus[(int)GlobalGameStatus.GameState.Pause] = true;
		else if (GlobalGameStatus.Instance.StateStatus[(int)GlobalGameStatus.GameState.Pause])
            GlobalGameStatus.Instance.StateStatus[(int)GlobalGameStatus.GameState.Pause] = false;
	}

	public void ChangeTimeScale()
	{
		if (TimeScale == 0)
			TimeScale1 ();
		else if(TimeScale == 1)
			TimeScale0 ();
	}

	void TimeScale0()
	{
		Time.timeScale = 0;
		TimeScale = Time.timeScale;
	}

	void TimeScale1()
	{
		Time.timeScale = 1;
		TimeScale = Time.timeScale;
	}
}
