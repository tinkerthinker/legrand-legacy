﻿using UnityEngine;
using System.Collections;

public class TestRaycast : MonoBehaviour {
    RaycastHit _hitInfo;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void LateUpdate () {
        if(!Physics.Raycast(new Ray(transform.position, Vector3.down), out _hitInfo, 0.7f, LayerMask.GetMask("Ground"))){
            transform.position = new Vector3(transform.position.x, transform.position.y - 0.1f, transform.position.z);
        }

    }
}
