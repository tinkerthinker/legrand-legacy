﻿using UnityEngine;
using System.Collections;
using TeamUtility.IO;

public class ScrollDownCredit : MonoBehaviour {

	public GameObject lastTextCredit;
	public float scrollSpeed;
	public float fadeSpeed;

	public WwiseManager.BGM BGM;

	private float velocity = 0.0f;
	private float camVerticalSize;
	private bool allowScrolling;
	private SpriteRenderer tinkerLogo;


	void Start()
	{
		camVerticalSize = GetComponent<Camera>().orthographicSize;
		tinkerLogo = lastTextCredit.GetComponent<SpriteRenderer> ();
		allowScrolling = true;
		WwiseManager.Instance.PlayBG (BGM);
	}

	// Update is called once per frame
	void Update () 
	{
		if(allowScrolling)
			transform.Translate (Vector3.down * scrollSpeed * Time.deltaTime);


		if (transform.position.y <= lastTextCredit.transform.position.y) 
		{
			allowScrolling = false;
			FadeInLogo ();
		}

		if (InputManager.GetButtonUp ("Confirm")) 
		{
			SceneManager.SwitchScene("TitleScene");
			WwiseManager.Instance.StopBG();
		}
	}

	void FadeInLogo()
	{
		float transparency = Mathf.SmoothDamp (tinkerLogo.color.a, 1f, ref velocity, fadeSpeed);
		tinkerLogo.color = new Color (1f, 1f, 1f, transparency);
	}
}
