﻿using UnityEngine;

public class WaterRing : MonoBehaviour
{
    public GameObject WaterRingParticle;
    public float YOfset;
    PlayerController PlayerController;
    GameObject PartyLeader;
    Vector3 WaterRingPosition;

    void Awake()
    {
        WaterRingPosition = new Vector3();
        if (AreaController.Instance != null)
        {
            PlayerController = AreaController.Instance.CurrPlayer.GetComponent<PlayerController>();
            PartyLeader = AreaController.Instance.CurrPlayer;
        }
        else
        {
            Invoke("WaitAreaControllerInstantiated", 1f);
        }
    }

    void WaitAreaControllerInstantiated()
    {
        PlayerController = AreaController.Instance.CurrPlayer.GetComponent<PlayerController>();
        PartyLeader = AreaController.Instance.CurrPlayer;
    }

    void OnTriggerEnter(Collider coll)
    {
        if (coll.tag == "PartyLeader")
        {
            WaterRingParticle.SetActive(true);
            if (PlayerController != null)
            {
                PlayerController.ForceWalk = true;
            }
        }
    }
    void OnDisable()
    {
        DisableWaterRing();
    }
    void DisableWaterRing()
    {
        WaterRingParticle.SetActive(false);
        if (PlayerController != null)
        {
            PlayerController.ForceWalk = false;
        }
    }
    void OnTriggerExit(Collider coll)
    {
        if (coll.tag == "PartyLeader")
        {
            DisableWaterRing();
        }
    }

    void Update()
    {
        if (PartyLeader != null && WaterRingParticle.activeInHierarchy)
        {
            WaterRingPosition.Set(PartyLeader.transform.position.x, this.transform.position.y + YOfset, PartyLeader.transform.position.z);
            WaterRingParticle.transform.position = WaterRingPosition;
        }
    }
}
