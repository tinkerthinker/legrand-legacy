﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using Legrand.database;

public class SaveGameEditor : MonoBehaviour
{
    [ContextMenuItem("Create File", "Create")]
    [Tooltip("Right click to create")]
    public string saveName = "Save 01";

    [ContextMenuItem("Load File", "Load")]
    [Tooltip("Right click to create")]
    public string loadName = "Save 01";

    public SaveData SaveModel;
    void Load()
    {
        if(UpdateDataFromFile(loadName))
        {
            Debug.Log("file loaded");
        }
        else
        {
            Debug.Log("load fail");
        }
    }
    void Create()
    {
        if(SaveGame(saveName))
        {
            Debug.Log("file created");
        }
        else
        {
            Debug.Log("create fail");
        }
    }
    public bool SaveGame(string SaveName)
    {
        SaveModel.Clear();

        BinaryFormatter bf = new BinaryFormatter();
        Directory.CreateDirectory(LegrandBackend.SavePath + "/" + SaveName);

        using (FileStream stream = new FileStream(LegrandBackend.SavePath + "/" + SaveName + "/Save.gd", FileMode.Create))
        {
            try
            {
                bf.Serialize(stream, SaveModel);
            }
            catch (System.Exception)
            {
                return false;
            }
        }

        return true;
    }
    public bool UpdateDataFromFile(string FileLocation)
    {
        BinaryFormatter bf = new BinaryFormatter();

        if (File.Exists(LegrandBackend.SavePath + "/" + FileLocation + "/Save.gd"))
        {
            using (FileStream stream = new FileStream(LegrandBackend.SavePath + "/" + FileLocation + "/Save.gd", FileMode.Open))
            {
                try
                {
                    SaveModel = bf.Deserialize(stream) as SaveData;
                }
                catch (System.Exception e)
                {
                    Debug.LogError(e + "Exception caught in encoding");
                    return false;
                }
            }
        }
        else
            return false;

        return true;
    }
}
