﻿using UnityEngine;
using System.Collections;
using TeamUtility.IO;

public class PauseTester : MonoBehaviour {
	bool IsPause = false;
	
	// Update is called once per frame
	void Update () {
		if (InputManager.GetKeyDown (KeyCode.Space)) {
			if (!IsPause) {
				Time.timeScale = 0f;
				IsPause = true;
			}
			else
			{
				Time.timeScale = 1f;
				IsPause = false;
			}
		}
	}
}
