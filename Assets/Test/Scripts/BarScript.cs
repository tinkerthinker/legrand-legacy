﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BarScript : MonoBehaviour {

    public float maxValue;
    public float currValue;

    private Image bar;

    void Awake()
    {
        bar = transform.GetComponent<Image>();
    }

    void Update()
    {
        bar.fillAmount = currValue / maxValue;
    }

    public void setCurrValue(float value)
    {
        currValue = value;
    }

}
