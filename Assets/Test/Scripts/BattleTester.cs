﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class BattleTester : MonoBehaviour {	
	public static BattleTester instance;
	public string EncounterParty;
    public BattleAreaData.DungeonArea Arena;

	public List<string> character;
	public List<string> characterParty;

    public bool TestTutorial;
    public TransitionData TestTransition;

	void Awake (){
		instance = this;
	}
	void Start (){
		initBattle();
	}
	void initBattle () {
		BattlePoolingSystem.Instance.Init();
		StartCoroutine(LegrandBackend.Instance.NewGame ());
		StartCoroutine (WaitUntilGameReady());
	}

	private IEnumerator WaitUntilGameReady()
	{
		while (!PartyManager.Instance._ReadyToPlay)
			yield return null;

		int j = 1;
		foreach(string c in characterParty)
		{
			PartyManager.Instance.AddNewCharacter(c);
		}
		foreach(string i in character)
		{
			PartyManager.Instance.BattleFormations.AddFormation (PartyManager.Instance.AddNewCharacter (i), new Vector2 (j,0));
			j++;
		}
        BattleAreaData area = new BattleAreaData();
		GameObject go = Instantiate(Resources.Load (area.GetBattleAreaName(Arena)) as GameObject);
        EventManager.Instance.AddListener<BattleDonePrepared>(BattleDonePrepared);
		go.GetComponent<Battle> ().StartBattle (EncounterParty, TestTutorial? TestTransition:null, 0);
        if (TestTutorial)
            EventManager.Instance.TriggerEvent(new BattleTransitionDone());
	}

    private void BattleDonePrepared(BattleDonePrepared e)
    {
        EventManager.Instance.RemoveListener<BattleDonePrepared>(BattleDonePrepared);
        EventManager.Instance.TriggerEvent(new OpenBattleEvent());
    }
}

public class characterSetup{
    public string characterId;
    public int formationX;
    public int formationY;

}
