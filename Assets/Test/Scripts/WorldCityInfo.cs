﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WorldCityInfo : MonoBehaviour
{
    public Image AreaPicture;
    public Text AreaName;
    public Text Description;
    
    public void Set(string areaPicture, string  areaName, string description)
    {
        AreaPicture.sprite = LegrandBackend.Instance.GetSpriteData(areaPicture);
        AreaName.text = areaName;
        Description.text = description;

        this.gameObject.SetActive(true);
    }

    public void Disable()
    {
        this.gameObject.SetActive(false);
        AreaPicture.sprite = null;
        AreaName.text = string.Empty;
        Description.text = string.Empty;
    }
}
