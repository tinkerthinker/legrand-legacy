﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TestDBTutorial : MonoBehaviour {
	public Dictionary<string, int> listIdTutorial = new Dictionary<string, int>();
	//[SerializeField]
	//private List<string> _listNameTutorial;
//	public List<string> ListNameTutorial
//	{
//		get{return _listNameTutorial;}
//	}
	[SerializeField]
	private List<TutorialSteps> _listTutorialSteps;
	public List<TutorialSteps> ListTutorialSteps
	{
		get{return _listTutorialSteps;}
	}

	public static TestDBTutorial Instance;

	public string TestTutorialPopUpName;

	void Awake()
	{
		TestDBTutorial.Instance = this;

		int index = 0;
		foreach (TutorialSteps steps in _listTutorialSteps) {
			listIdTutorial.Add(steps.Name, index++);
		}
	}

	public TutorialSteps GetTutorialSteps(string nameTutorial)
	{
		int idSteps = listIdTutorial [nameTutorial];
		idSteps = (idSteps > _listTutorialSteps.Count)? 0: idSteps;

		return _listTutorialSteps[idSteps];
	}
}
