﻿using UnityEngine;
using System.Collections;

public class Paralax : MonoBehaviour {

	public float speed;

	void Update () {
		this.gameObject.transform.Translate (new Vector3(-speed*Time.deltaTime,0f,0f));
	}
}
