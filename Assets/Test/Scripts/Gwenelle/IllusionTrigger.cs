﻿using UnityEngine;
using System.Collections;

public class IllusionTrigger : MonoBehaviour
{
    [SerializeField]
    private int PuzzleNum;
    [SerializeField]
    private IllusionAreaController IlluAreaController;

   void OnTriggerEnter(Collider coll)
    {
        if (coll.tag == "PartyLeader")
        {
            IlluAreaController.CheckArea(PuzzleNum);
        }
    }
}
