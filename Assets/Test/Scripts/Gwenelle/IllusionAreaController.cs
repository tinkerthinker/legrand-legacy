﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class IllusionAreaController : MonoBehaviour
{

    [SerializeField]
    List<GameObject> IllusionAreas;
    [SerializeField]
    private int gPuzzleNum = 1;


    void OnEnable()
    {
        AreaActivated(gPuzzleNum);
    }

    void AreaActivated(int gpuzzlenum)
    {
        if (gpuzzlenum > 0)
        {
            for (int i = 0; i < IllusionAreas.Count; i++)
            {
                IllusionAreas[i].SetActive(false);
            }
            IllusionAreas[gpuzzlenum - 1].SetActive(true);
        }
    }

    public void CheckArea(int puzzlenum)
    {
        gPuzzleNum = puzzlenum;
		if(puzzlenum==1)
			AreaActivated(gPuzzleNum);
    }
}
