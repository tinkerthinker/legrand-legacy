﻿using UnityEngine;
using System.Collections;

public class StatueNPC : StaticNPC {

	// hardcode
	[SerializeField]
	private IllusionAreaController illAreaController;

	public bool IsAreaKey;

	protected override void DialogueEnd(DialogWindowClosed e)
	{
		base.DialogueEnd(e);

		//hardcode : gwenelle lost place
		if(IsAreaKey)
		{
			illAreaController.CheckArea(1);					
		}
	}
}
