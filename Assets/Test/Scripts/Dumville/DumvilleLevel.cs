﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DumvilleLevel : MonoBehaviour {
    public string checkName;
    private int level = 0;
    public GameObject[] go;

    public Memory newFact;
    

	// Use this for initialization
	void Start () {

        PartyManager.Instance.WorldInfo.NewFact(newFact.Name, newFact.Value, "", null, null);

        Memory temp = PartyManager.Instance.WorldInfo.LatestFacts.Find(x => x.Name == checkName);

        if (temp != null)
        {
            level = (int)temp.Value;
        }

        for (int i = 0; i < go.Length; i++)
            go[i].SetActive(false);

        go[level].SetActive(true);
	}
}
