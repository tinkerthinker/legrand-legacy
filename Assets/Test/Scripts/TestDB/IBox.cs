using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;

[ExecuteInEditMode]
public class IBox : MonoBehaviour
{
	public List<CharacterModel> WarCharacters;
	public List<EncounterModel> Encounters;
	public List<BattleAttribute> EncounterBaseAttributes;
	public List<MagicPresetModel> EncounterMagics;
	public List<CharacterItemModel> EncounterItems;
	public List<EncounterParty> EncounterParties;
	public List<EncounterFormation> EncounterFormations;
	public List<EncounterLootModel> EncounterLoots;
	public List<EncounterGrowth> EncounterGrowths;

	// Item Consumable
	public List<ConsumableHealing> ConsumableHealings;
	public List<ConsumableHealing.RestoredAttribute> ConsumableHealingAttribute;

	public List<ConsumableAttribute> ConsumableAttributes;
	public List<ConsumableAttribute.RaisedAttribute> RaisedAttributes;
	public List<ConsumableMagic> ConsumableMagics;

	public List<ConsumableMisc> ConsumableMisc;
	public List<ConsumableMisc.MiscEffect> CosumableMiscEffect;

	// Item Material
	public List<CraftingMaterial> ItemMaterials;

	// Blueprint
	public List<BluePrint> BluePrints;

	public List<Buff> Buffs;

	public List<Key> Keys;
	public List<Key.KeyEffect> KeyEffects;

	public List<StoryData> StoryData;
	public List<ShopModel> ShopModel;

	#region New Game Data

	public List<CharacterModel> NewGameCharacters;
	public List<BattleAttribute> NewGameBattleAttributes;
	public List<BattleClassModel> NewGameBattleClasses;
	public List<OffensiveBattleSkill> NewGameBattleOffensiveSkills;
	public List<DefensiveBattleSkill> NewGameBattleDefensiveSkills;

	public List<ElementModel> NewGameCharacterElements;
	public List<MagicPresetModel> NewGameMagicPresets;
	public List<CommandData> NewGameCommands;
	public List<CharacterItemModel> NewGameCharacterItems;
	public List<EquipmentUpgradeModel> NewGameWeaponUpgrade;
	public List<EquipmentUpgradeModel> NewGameArmorUpgrade;

	public List<BattleFormationModel> NewGameFormations;


	public List<ItemModel> NewGameInventory;
	public int NewGameDanaar;

	public List<string> NewGameActiveEvent;

	#endregion

	#region New Party Data

	public List<CharacterModel> NewPartyMemberCharacters;
	public List<BattleAttribute> NewPartyBattleAttributes;
	public List<BattleClassModel> NewPartyMemberBattleClasses;
	public List<OffensiveBattleSkill> NewPartyMemberBattleOffensiveSkills;
	public List<DefensiveBattleSkill> NewPartyMemberBattleDefensiveSkills;

	public List<ElementModel> NewPartyMemberElements;
	public List<MagicPresetModel> NewPartyMemberMagicPresets;
	public List<CommandData> NewPartyMemberCommands;
	public List<CharacterItemModel> NewPartyMemberCharacterItems;
	public List<EquipmentUpgradeModel> NewPartyMemberWeaponUpgrade;
	public List<EquipmentUpgradeModel> NewPartyMemberArmorUpgrade;

	#endregion

	#region Number Calculation

	public List<BattleGrowthData> BattleAttributeGrowth;
	public List<WeaponCalculationModel> BattlerWeaponCalculations;
	public List<ArmorCalculationModel> BattlerArmorCalculations;

	#endregion
}
