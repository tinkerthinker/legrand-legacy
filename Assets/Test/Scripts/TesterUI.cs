﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TesterUI : MonoBehaviour {
    public GameObject TestRootUI;
	public List<string> character;
    public int MainCharacterATP;
    public List<int> CharacterATP;
    public int NumberAutoPutFormation;

    public List<string> ItemsToAdd;
    public int InitGold;
    public bool UseCursour = true;

    public bool TestCurrRoomSafety = true;

	void Awake (){
        if (TestRootUI)
            TestRootUI.SetActive(false);
		Init();
	}
	void Start (){
        Time.timeScale = 0;
        if(UseCursour)
        {
            SelectedGameObjectModule.current.ShowMarkObject();
        }
        else
        {
            SelectedGameObjectModule.current.HideMarkObject();
        }

        GlobalGameStatus.Instance.SetCurrRoomSafety(TestCurrRoomSafety);
	}
	void Init () {
		StartCoroutine(LegrandBackend.Instance.NewGame ());
		StartCoroutine (WaitUntilGameReady());
	}
	
	private IEnumerator WaitUntilGameReady()
	{
		while (!PartyManager.Instance._ReadyToPlay)
			yield return null;

        int[,] formation = new int[2, 3];
        foreach (BattleFormation bf in PartyManager.Instance.BattleFormations.Formations)
        {
            formation[(int)bf.Position.y, (int)bf.Position.x] = 1;
        }

        int row = 1, col = 0, distributedChar = 0;
        bool found = false;

        PartyManager.Instance.CharacterParty[0].AttributePoint = MainCharacterATP;

        int index = 0;
		foreach(string i in character)
		{
            found = false;
            Legrand.core.MainCharacter newChar = PartyManager.Instance.AddNewCharacter(i);

            if(index < CharacterATP.Count)
                newChar.AttributePoint = CharacterATP[index];

            while (!found && distributedChar < NumberAutoPutFormation)
            {
                if (formation[row, col] == 0)
                {
                    PartyManager.Instance.BattleFormations.AddFormation(newChar, new Vector2(col, row));
                    found = true;
                    distributedChar++;
                }
                col++;
                if (col > 2)
                {
                    row--;
                    col = 0;
                }
            }

            index++;
		}

        foreach(string str in ItemsToAdd)
        {
            PartyManager.Instance.Inventory.AddItem(LegrandBackend.Instance.ItemData[str]);
        }

        PartyManager.Instance.Inventory.Gold = InitGold;

        if (TestRootUI)
            TestRootUI.SetActive(true);
	}
}
