﻿using UnityEngine;
using System.Collections;

public class CreatureRandomStartIdle : MonoBehaviour
{
    public string StartAnim = "BATTLESTANCE";

    private Animator anim;
    
	// Use this for initialization
	void Start ()
	{
		anim = GetComponent<Animator> ();
		anim.Play (StartAnim, -1, LegrandUtility.Random(0.0f, 1.0f));
	}

    void OnEnable()
    {
        anim = GetComponent<Animator>();
        anim.Play(StartAnim, -1, LegrandUtility.Random(0.0f, 1.0f));
    }
}
