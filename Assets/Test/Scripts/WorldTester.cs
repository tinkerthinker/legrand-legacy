﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WorldTester : MonoBehaviour {
	public string gameArea;
	public string gameCluster;
	public string gamePortal;

	public List<string> NewCharacterId;
	public List<string> RemoveCharacter;
	// Use this for initialization
	void Awake ()
    {
		StartCoroutine(LegrandBackend.Instance.NewGame ());
		StartCoroutine (WaitUntilGameReady());
        BattlePoolingSystem.Instance.Init();
    }

	private IEnumerator WaitUntilGameReady()
	{
		while (!PartyManager.Instance._ReadyToPlay)
			yield return null;
		
		PartyManager.Instance.State = new WorldState (gameArea, gameCluster, gamePortal, 1, 1, 0);
		foreach(string i in NewCharacterId) PartyManager.Instance.AddNewCharacter(i);
        if (AreaController.Instance != null)
        {
            AreaController.Instance.StartWorld();
        }
	}
}
