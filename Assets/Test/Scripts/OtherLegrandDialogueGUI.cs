﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using TeamUtility.IO;
using SmartLocalization;

public class OtherLegrandDialogueGUI : MonoBehaviour {
	#region GUI Element dialogue
	private GameObject BoxImage;
	private Text NameText;
	private Text DialogueText;

	public GameObject LeftBox;
	public GameObject RightBox;
	
	public Text LeftNameText;
	public Text LeftDialogueText;
	
	public Text RightNameText;
	public Text RightDialogueText;
	
	public GameObject BranchedTextPanel;
	public Button BranchedTextButtonPref;
	private List<Button> BranchedButtons = new List<Button>();
	
	private bool _IsChoicePopped = false;
	#endregion
	
	public WwiseManager.UIFX AudioTextSound;
	
	private bool _dialogue;
	private bool _showDialogueBox;
	
	// DIALOGUER VARS	
	private string _windowTargetText = string.Empty;
	private string _windowCurrentText = string.Empty;
	
	private string _nameText = string.Empty;
	
	private bool _isBranchedText;
	private string[] _branchedTextChoices;
	private int _currentChoice;
	
	// TWEEN VARS
	private float _windowTweenValue;
	private bool _windowReady;
	
	private float _nameTweenValue;
	
	// TEXT VARS
	private int _textFrames = int.MaxValue;
	
	private bool _confirmInUse = true;
	
	Dictionary<string, Color> NameColor;
	// Use this for initialization
	void Start () {
		
		addDialoguerEvents();
		
		_showDialogueBox = false;
		
		//Invoke("startWindowTweenIn", 1);
		//Invoke("startWindowTweenOut", 5);
	}
	
	void OnDestroy()
	{
		removeDialoguerEvents ();
	}
	
	// Update is called once per frame
	void Update () {
		if(!_dialogue) return;
		
		if(_windowReady) calculateText();
		
		//		if(!_dialogue) return;
		
		if(!_isBranchedText){
			//if(InputManager.GetButtonUp("Confirm"))
			if(InputManager.GetButtonDown("Confirm"))
			{
				if(_windowCurrentText == _windowTargetText){
					Dialoguer.ContinueDialogue(0);
				}else{
					_windowCurrentText = _windowTargetText;
					//audioTextEnd.Play();
					DialogueText.text = _windowCurrentText;
				}
			}
		}else{
			//if(InputManager.GetButtonUp("Confirm"))
			if(InputManager.GetButtonDown("Confirm"))
			{
				if(_windowCurrentText == _windowTargetText){
					//Dialoguer.ContinueDialogue(_currentChoice);
					if(!_IsChoicePopped)
					{
						showChoiceButtons();
						_IsChoicePopped = true;
					}
				}else{
					_windowCurrentText = _windowTargetText;
					//audioTextEnd.Play();
					DialogueText.text = _windowCurrentText;
				}
			}
			/*
			if(InputManager.GetButtonDown("Horizontal")){
				_currentChoice = (int)Mathf.Repeat(_currentChoice + 1, _branchedTextChoices.Length);
				audioText.Play();
			}
			if(InputManager.GetButtonDown("Horizontal")){
				_currentChoice = (int)Mathf.Repeat(_currentChoice - 1, _branchedTextChoices.Length);
				audioText.Play();
			}
			*/
		}
	}
	
	void showChoiceButtons()
	{
		for (int i = 0; i < _branchedTextChoices.Length; i++) {
			Button newChoiceButton = Instantiate (BranchedTextButtonPref) as Button;
			newChoiceButton.transform.SetParent(BranchedTextPanel.transform);
			newChoiceButton.GetComponent<RectTransform>().localScale = new Vector3(1,1,1);
			
			if(newChoiceButton.GetComponent<DialogueChoice>() == null)
				newChoiceButton.gameObject.AddComponent<DialogueChoice>();
			
			newChoiceButton.GetComponent<DialogueChoice>().ChoiceDialogueId = i;
			newChoiceButton.GetComponentInChildren<Text>().text = _branchedTextChoices[i];
			
			BranchedButtons.Add(newChoiceButton);
		}
		if(BranchedButtons.Count > 0)
			BranchedButtons [0].Select ();
		else
			BranchedTextPanel.GetComponentInChildren<Button> ().Select ();
	}
	
	void destroyChoiceButtons()
	{
		//		foreach (DialogueChoice obj in BranchedTextPanel.GetComponentsInChildren<DialogueChoice>()) {
		//			if(obj.gameObject == BranchedTextPanel)
		//				continue;
		//			GameObject.Destroy(obj.gameObject);
		//		}
		foreach (Button obj in BranchedButtons) {
			GameObject.Destroy(obj.gameObject);
		}
		BranchedButtons.RemoveAll( (Button obj) => obj );
	}
	
	#region Dialoguer
	public void addDialoguerEvents(){
		Dialoguer.events.onStarted += onDialogueStartedHandler;
		Dialoguer.events.onInstantlyEnded += onDialogueInstantlyEndedHandler;
		Dialoguer.events.onTextPhase += onDialogueTextPhaseHandler;
		Dialoguer.events.onWindowClose += onDialogueWindowCloseHandler;
		Dialoguer.events.onMessageEvent += onDialoguerMessageEvent;
	}
	
	public void removeDialoguerEvents()
	{
		Dialoguer.events.onStarted -= onDialogueStartedHandler;
		Dialoguer.events.onInstantlyEnded -= onDialogueInstantlyEndedHandler;
		Dialoguer.events.onTextPhase -= onDialogueTextPhaseHandler;
		Dialoguer.events.onWindowClose -= onDialogueWindowCloseHandler;
		Dialoguer.events.onMessageEvent -= onDialoguerMessageEvent;
	}
	
	private void onDialogueStartedHandler(){
		_dialogue = true;
	}
	
	private void onDialogueInstantlyEndedHandler(){
		_dialogue = false;
		_showDialogueBox = false;
	}
	
	private void onDialogueTextPhaseHandler(DialoguerTextData data){
		if (!Dialoguer.GetGlobalBoolean (2))
			return;

		//Try to set which one should appear, left or right baloon
		switch(data.metadata.Substring(0,1))
		{
		case "2":
			LeftBox.SetActive (false);
			RightBox.SetActive (true);
			
			BoxImage = RightBox;
			NameText = RightNameText;
			DialogueText = RightDialogueText;
			break;
		case "1":
		default:
//			LeftBoxImage.transform.parent.gameObject.SetActive (false);
//			RightBoxImage.transform.parent.gameObject.SetActive (false);
//
//			BoxImage = LeftBoxImage;
//			NameText = LeftNameText;
//			DialogueText = LeftDialogueText;
			LeftBox.SetActive (true);
			RightBox.SetActive (false);
			
			BoxImage = LeftBox;
			NameText = LeftNameText;
			DialogueText = LeftDialogueText;
			break;
		}


		_windowCurrentText = string.Empty;
		//_windowTargetText = data.text;
        _windowTargetText = LegrandBackend.Instance.GetLanguageData(data.text);
		if (_windowTargetText == null)
			_windowTargetText = data.text;
		
		//_nameText = data.name;
        _nameText = LegrandBackend.Instance.GetLanguageData(data.name);
		if (_nameText == null)
			_nameText = data.name;
		
		_showDialogueBox = true;
		
		_isBranchedText = data.windowType == DialoguerTextPhaseType.BranchedText;
		_branchedTextChoices = data.choices;
		_currentChoice = 0;
		
		//startWindowTweenIn();
		
		// test process with UI
		//_windowReady = true;
		
		BoxImage.transform.parent.gameObject.SetActive (true);
		
		if (Dialoguer.GetGlobalBoolean (2))
		{
			BoxImage.gameObject.SetActive (true);
			
			//			NameText.alignment = TextAnchor.UpperLeft;
			//			DialogueText.alignment = TextAnchor.UpperLeft;
		} else
		{
			BoxImage.gameObject.SetActive (false);
			return;
			//			NameText.alignment = TextAnchor.UpperCenter;
			//			DialogueText.alignment = TextAnchor.UpperCenter;
		}
		
		if (data.name.Equals (string.Empty))
			NameText.transform.parent.gameObject.SetActive (false);
		else
			NameText.transform.parent.gameObject.SetActive (true);
		
		destroyChoiceButtons ();
		_IsChoicePopped = false;
		
		NameText.gameObject.SetActive (true);
		NameText.text = _nameText;
		
		DialogueText.gameObject.SetActive (true);
		DialogueText.text = _windowCurrentText;
		
		//_windowCurrentText = _windowTargetText;
		windowInComplete ();
	}
	
	private void onDialogueWindowCloseHandler(){
		//startWindowTweenOut();
		
		// test process with UI
		_windowReady = false;
		
		BoxImage.transform.parent.gameObject.SetActive (false);
		//NameText.text = string.Empty;
		//DialogueText.text = string.Empty;
		destroyChoiceButtons ();
		
		windowOutComplete ();
	}
	
	private void onDialoguerMessageEvent(string message, string metadata){
		
	}
	#endregion
	
	#region Tween Handlers
	private void updateWindowTweenValue(float newValue){
		_windowTweenValue = newValue;
	}
	
	private void windowInComplete(){
		_windowReady = true;
	}
	
	private void windowOutComplete(){
		_showDialogueBox = false;
		
		_dialogue = false;
		
		if(EventManager.Instance != null)EventManager.Instance.TriggerEvent(new DialogWindowClosed());
	}
	#endregion
	
	#region Utils
	private void calculateText(){
		// char per char text

		if(_windowTargetText == string.Empty || _windowCurrentText == _windowTargetText) return;
		
		int frameSkip = 1;
		
		if(_textFrames<frameSkip){
			_textFrames += 1;
			return;
		}else{
			_textFrames = 0;
		}
		
		int charsPerInterval = 1;
		if(_windowCurrentText != _windowTargetText){
			for(int i = 0; i<charsPerInterval; i+=1){
				if(_windowTargetText.Length <= _windowCurrentText.Length) break;
				_windowCurrentText += _windowTargetText[_windowCurrentText.Length];
			}
		}
		
		//audioText.Play();
		WwiseManager.Instance.PlaySFX(AudioTextSound);

		DialogueText.text = _windowCurrentText;

		
		// straight text
		//_windowCurrentText = _windowTargetText;
		DialogueText.text = _windowCurrentText;
	}
	#endregion
}
