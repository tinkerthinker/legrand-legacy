﻿using UnityEngine;
using System.Collections.Generic;

[RequireComponent(typeof(PolygonCollider2D))]
public class ColliderToMesh : MonoBehaviour
{
    public Material MeshMaterial;
    public Vector3 Scale;

    void Start()
    {
        PolygonCollider2D pc2 = gameObject.GetComponent<PolygonCollider2D>();
        
        for (int k = 1; k < pc2.pathCount; k++)
        {
            Mesh mesh = new Mesh();
            Vector2[] points = pc2.GetPath(k);
            Vector3[] vertices = new Vector3[points.Length];
            for (int j = 0; j < points.Length; j++)
            {
                Vector2 actual = points[j];
                vertices[j] = new Vector3(actual.x, actual.y, 0);
            }
            Triangulator tr = new Triangulator(points);
            int[] triangles = tr.Triangulate();
            mesh.vertices = vertices;
            mesh.triangles = triangles;

            GameObject newMesh = new GameObject("Polygon Mesh " + k);
            newMesh.transform.SetParent(this.transform, true);
            newMesh.transform.localScale = Scale;
            MeshFilter mf = newMesh.AddComponent<MeshFilter>();
            newMesh.AddComponent<MeshRenderer>().material = MeshMaterial;
            mf.mesh = mesh;
        }
    }
}