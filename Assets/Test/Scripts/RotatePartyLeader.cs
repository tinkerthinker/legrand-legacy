﻿using UnityEngine;
using DG.Tweening;

public class RotatePartyLeader : MonoBehaviour
{
    Vector3 TargetRotation;
    Vector3 currentRotation;

    Tween _CurTween;

    void OnEnable()
    {
        EventManager.Instance.AddListener<RespondInteract>(Rotate);
        EventManager.Instance.AddListener<TalkToPlayerEvent>(TalkToPlayerEventHandler);
    }
    void OnDisable()
    {
        if (EventManager.Instance)
        {
            EventManager.Instance.RemoveListener<RespondInteract>(Rotate);
            EventManager.Instance.RemoveListener<TalkToPlayerEvent>(TalkToPlayerEventHandler);
        }
    }

    void Rotate(RespondInteract i)
    {
        currentRotation = this.transform.eulerAngles;
        transform.LookAt(i.Target.transform);
        TargetRotation = this.transform.eulerAngles;
        TargetRotation.x = currentRotation.x;
        TargetRotation.z = currentRotation.z;

        this.transform.eulerAngles = currentRotation;
        _CurTween = this.transform.DORotate(TargetRotation, 0.5f, RotateMode.Fast);
    }

    void TalkToPlayerEventHandler(TalkToPlayerEvent e)
    {
        if (e.FinishTalking)
            Stop();
    }

    public void Stop()
    {
        if (_CurTween != null)
            _CurTween.Kill();
    }
}
