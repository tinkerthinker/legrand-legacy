﻿using UnityEngine;
using System.Collections;

public static class WaitFor
{
    private static float realTime = 0;

	public static IEnumerator Frames(int frameCount)
	{
		while (frameCount > 0)
		{
			frameCount--;
			yield return null;
		}
	}

    public static IEnumerator RealTime(float seconds)
    {
        realTime = 0;
        while (realTime < seconds)
        {
            realTime += Time.unscaledDeltaTime;
            yield return null;
        }

        realTime = 0;
    }
}
