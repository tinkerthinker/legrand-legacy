﻿using UnityEngine;
using System.Collections;
using SmartLocalization;

public class ButtonChangeExpression : MonoBehaviour {
    public InitDialoguerDouble Initializer;
	public DialoguerDialogues Dialogue;
    public LAppModelProxy SelectedLive2D;
    public string Live2DName;
    public int MotionPerRow = 5;

    private Rect tfPos = new Rect(0,20,200,20);
    private Rect btnMotPos = new Rect(10, 40, 100, 50);

    private string[] Motions = new string[0];

	void Awake()
	{
		int size = Screen.height / 14 ;
		Rect rctGUISize = new Rect(0,0,size, size);
		this.GetComponent<GUITexture>().pixelInset = rctGUISize ;
	}
	
	
	void Start() 
	{
        
	}
	
	void OnMouseUp() 
	{
        LegrandUtility.StartDialogue((int)Dialogue);        
	}

	void OnGUI()
	{		
		if (GUILayout.Button ("Activate Selected Live2D")) {
            SelectedLive2D.path = Initializer.GetPath(Live2DName);
            SelectedLive2D.Init();
            SelectedLive2D.SetVisible(true);
            Motions = SelectedLive2D.GetModel().ModelSetting.GetMotionGroupNames();
		}
        Live2DName = GUI.TextField(tfPos, Live2DName);
        if(Motions.Length > 0)
        {
            for (int i = 0; i < Mathf.CeilToInt((float)Motions.Length / (float)MotionPerRow); i++)
            {
                for (int j = 0; j < MotionPerRow; j++)
                {
                    if ((i * MotionPerRow + j) >= Motions.Length)
                        break;

                    if (GUI.Button(new Rect(btnMotPos.x + (j * btnMotPos.width + 10), btnMotPos.y + (i * btnMotPos.height + 10), btnMotPos.width, btnMotPos.height), Motions[i * MotionPerRow + j]))
                    {
                        SelectedLive2D.GetModel().StartMotion(Motions[i * MotionPerRow + j], 0, 3);
                    }
                }
            }
        }
	}
}
