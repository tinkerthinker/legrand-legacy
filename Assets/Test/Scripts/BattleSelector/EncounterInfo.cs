﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EncounterInfo : MonoBehaviour {
	public string CharID;

	private string _Charname;

	public string CharName
	{
		get {return _Charname;}
		set {NameText.text = value; _Charname = value;}
	}
	public int index;

	public int Level;
	public InputField LevelText;

	public Vector2 Formation;
	public Text FormationText;

	public Text NameText;

	public void Clear()
	{
		Level = 0;
		Formation = new Vector2();
	}

	public void SetText()
	{
		LevelText.text = Level + "";
		FormationText.text = "";

		switch((int)Formation.y)
		{
		case 0 : FormationText.text = "Back"; break;
		case 1 : FormationText.text = "Front"; break;
		break;
		}

		switch((int)Formation.x)
		{
		case 0 : FormationText.text += " Left"; break;
		case 1 : FormationText.text += " Middle"; break;
		case 2 : FormationText.text += " Right"; break;
			break;
		}
	}

	public void ApplyChange()
	{
		Level = int.Parse(LevelText.text);
	}
}
