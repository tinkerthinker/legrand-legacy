﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;

public class StageSelector : MonoBehaviour {
	public List<string> CharacterParty;

    public string BattleParty;
    public string BattleArea;

    [SerializeField]
	private GameObject _Panel;

	[SerializeField]
	private StageEditor _EditorPanel;

	private Battle _BattleGameobject;
	private GameObject _BattlePrefab;

    public BattleAreaData.DungeonArea Arena;


	void Start()
	{
		BattlePoolingSystem.Instance.Init();
		StartCoroutine(LegrandBackend.Instance.NewGame ());
		StartCoroutine (WaitUntilGameReady());
	}

	private IEnumerator WaitUntilGameReady()
	{
		while (!PartyManager.Instance._ReadyToPlay)
			yield return null;
		
		foreach(string c in CharacterParty)
		{
			PartyManager.Instance.AddNewCharacter(c);
		}
		
		PartyManager.Instance.BattleFormations.Clear ();
		for(int i=0;i<3;i++)
			PartyManager.Instance.BattleFormations.AddFormation(PartyManager.Instance.CharacterParty[i], new Vector2((float)i,1f));
		
		Cursor.visible = true;
		EventManager.Instance.AddListener<BattleEndEvent> (BattleEnd);
	}

	void OnDestroy()
	{
		if(EventManager.Instance != null)
			EventManager.Instance.RemoveListener<BattleEndEvent> (BattleEnd);
	}

	public void StartBattle(string partyID, string area)
	{
		BattleParty = partyID;
        BattleAreaData arena = new BattleAreaData();
        BattleArea = arena.GetBattleAreaName(Arena);
		_Panel.SetActive (false);
		_EditorPanel.gameObject.SetActive (true);
	}

	public void InitBattle()
	{
		_Panel.SetActive (false);
		_EditorPanel.gameObject.SetActive (false);

		for (int i=0; i<PartyManager.Instance.CharacterParty.Count; i++)
			PartyManager.Instance.CharacterParty [i].Health.ChangeCurrentValue (PartyManager.Instance.CharacterParty [i].Health.MaxValue);
		_BattlePrefab =	 Instantiate<GameObject>(Resources.Load<GameObject>(BattleArea));
		_BattleGameobject = _BattlePrefab.GetComponent<Battle>();
		TransitionData transData = new TransitionData ();
		transData.AfterFinishedGoTo = GameplayType.UnityScene;
        EventManager.Instance.AddListener<BattleDonePrepared>(BattleDonePrepared);
        _BattleGameobject.StartBattle (BattleParty, transData, _EditorPanel.ListEncounter);
	}

	void BattleEnd(BattleEndEvent e)
	{
//		Destroy (_BattleGameobject.gameObject);
		Destroy(_BattlePrefab);
		_Panel.SetActive (true);
		(EventSystem.current.firstSelectedGameObject.GetComponent<Button>()).Select();
	}

    private void BattleDonePrepared(BattleDonePrepared e)
    {
        EventManager.Instance.RemoveListener<BattleDonePrepared>(BattleDonePrepared);
        EventManager.Instance.TriggerEvent(new OpenBattleEvent());
    }
}
