﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;
using UnityEngine.EventSystems;
using Legrand.database;

public class StageEditor : MonoBehaviour {

	public StageSelector StageSelector;

	public Button FirstButton;
	[Header("MAIN CHARACTER")]
	public GameObject MCStatusCanvas;
	public GameObject DropDownPanel;
	public MainChracterInfo[] ListCharacter;
	public Text CurrentSelectedCharacterName;
	public int SelectedCharacterIndex = 0;

	[Header("ENCOUNTER")]
	public Button DropDownEncounterButton;
	public GameObject EncounterStatusCanvas;
	public GameObject DropDownPanelEncounter;
	public EncounterInfo[] ListEncounter;
	public Text CurrentSelectedEncounterName;
	public int SelectedEncounterIndex = 0;
	public string SelectedEncounterID = "-1";

	void OnEnable()
	{
		MCStatusCanvas.SetActive (false);
		EncounterStatusCanvas.SetActive(false);

		DropDownPanel.SetActive (false);
		DropDownPanelEncounter.SetActive(false);

		foreach (MainChracterInfo b in ListCharacter) 
		{
			b.Clear();
			b.gameObject.SetActive (false);
		}

		foreach (EncounterInfo e in ListEncounter) 
		{
			e.Clear();
			e.gameObject.SetActive (false);
		}

		for (int i = 0; i< PartyManager.Instance.CharacterParty.Count; i++) {
			MainCharacter mc = PartyManager.Instance.CharacterParty[i];
			ListCharacter[i].CharID = mc._ID;
			ListCharacter[i].CharName = mc.name;
			ListCharacter[i].index = i;
			ListCharacter[i].STR = mc.GetAttribute(Attribute.STR);
			ListCharacter[i].VIT = mc.GetAttribute(Attribute.VIT);
			ListCharacter[i].INT = mc.GetAttribute(Attribute.INT);
			ListCharacter[i].LUCK = mc.GetAttribute(Attribute.LUCK);
			ListCharacter[i].AGI = mc.GetAttribute(Attribute.AGI);

			ListCharacter[i].gameObject.SetActive(true);
		}

		bool found = false;
		int index = 0;

		Database dB = LegrandBackend.Instance.LegrandDatabase;
		while(!found && index<dB.EncounterPartyData.Count)
		{
			if(dB.EncounterPartyData.Get(index)._ID.Equals(StageSelector.BattleParty, System.StringComparison.OrdinalIgnoreCase))
			{
				found = true;

				int i = 0;
				foreach (EncounterFormationTest Eform in dB.EncounterPartyData.Get(index).Formations)
				{
					ListEncounter[i].CharID = Eform.Monster.ID;
					ListEncounter[i].CharName = Eform.Monster.Name;
					ListEncounter[i].index = i;
					ListEncounter[i].Formation = Eform.Position;
					ListEncounter[i].Level = Eform.MonsterLvl;
					
					ListEncounter[i].gameObject.SetActive(true);
					i++;
				}
			}

			if(!found) index++;
		}

		CurrentSelectedCharacterName.text = "";
		SelectedCharacterIndex = 0;

		CurrentSelectedEncounterName.text = "";
		SelectedEncounterIndex = 0;

		(EventSystem.current.firstSelectedGameObject.GetComponent<Button>()).Select();
	}

	public void StartSelectCharacter()
	{
		DropDownPanel.SetActive (true);
		ListCharacter [SelectedCharacterIndex].GetComponent<Button> ().Select ();
	}

	public void SelectCharacter(MainChracterInfo m)
	{
		m.SetText ();
		CurrentSelectedCharacterName.text = m.CharName;
		SelectedCharacterIndex = m.index;
		DropDownPanel.SetActive (false);
		MCStatusCanvas.SetActive (true);
		FirstButton.Select ();
	}

	public void CancelSelectCharacter()
	{
		DropDownPanel.SetActive (false);
		FirstButton.Select ();
	}

	public void ConfirmChangeCharacter()
	{
		ListCharacter[SelectedCharacterIndex].ApplyChange();
		foreach(MainCharacter mc in PartyManager.Instance.CharacterParty)
		{
			if(LegrandUtility.CompareString(mc._ID,ListCharacter[SelectedCharacterIndex].CharID))
			{
				mc.ChangeBaseAttribute(Attribute.STR, ListCharacter[SelectedCharacterIndex].STR);
				mc.ChangeBaseAttribute(Attribute.VIT, ListCharacter[SelectedCharacterIndex].VIT);
				mc.ChangeBaseAttribute(Attribute.INT, ListCharacter[SelectedCharacterIndex].INT);
				mc.ChangeBaseAttribute(Attribute.LUCK, ListCharacter[SelectedCharacterIndex].LUCK);
				mc.ChangeBaseAttribute(Attribute.AGI, ListCharacter[SelectedCharacterIndex].AGI);
			}
		}

		WwiseManager.Instance.PlaySFX(WwiseManager.UIFX.Confirm);
	}

	public void CancelChangeCharacter()
	{
		ListCharacter[SelectedCharacterIndex].SetText();
	}

	public void StartSelectEncounter()
	{
		DropDownPanelEncounter.SetActive (true);
		ListEncounter [SelectedEncounterIndex].GetComponent<Button> ().Select ();
	}
	
	public void SelectEncounter(EncounterInfo e)
	{
		e.SetText ();
		CurrentSelectedEncounterName.text = e.CharName;
		SelectedEncounterIndex = e.index;
		DropDownPanelEncounter.SetActive (false);
		EncounterStatusCanvas.SetActive (true);
		DropDownEncounterButton.Select ();
	}
	
	public void CancelSelectEncounter()
	{
		DropDownPanelEncounter.SetActive (false);
		DropDownEncounterButton.Select ();
	}

	public void ConfirmChangeEncounter()
	{
		ListEncounter[SelectedEncounterIndex].ApplyChange();
		WwiseManager.Instance.PlaySFX(WwiseManager.UIFX.Confirm);
	}

	public void CancelChangeEncounter()
	{
		ListEncounter[SelectedEncounterIndex].SetText();
	}
}
