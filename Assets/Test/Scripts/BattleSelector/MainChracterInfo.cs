﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MainChracterInfo : MonoBehaviour {
	public string CharID;

	private string _Charname;

	public string CharName
	{
		get {return _Charname;}
		set {NameText.text = value; _Charname = value;}
	}
	public int index;

	public int STR;
	public InputField STRText;

	public int VIT;
	public InputField VITText;

	public int INT;
	public InputField INTText;

	public int LUCK;
	public InputField WISText;

	public int AGI;
	public InputField AGIText;

	public Text NameText;

	public void Clear()
	{
		STR = 0;
		VIT = 0;
		INT = 0;
		LUCK = 0;
		AGI = 0;
	}

	public void SetText()
	{
		STRText.text = STR + "";
		VITText.text = VIT + "";
		INTText.text = INT + "";
		WISText.text = LUCK + "";
		AGIText.text = AGI + "";
	}

	public void ApplyChange()
	{
		STR = int.Parse(STRText.text);
		VIT = int.Parse(VITText.text);
		INT = int.Parse(INTText.text);
		LUCK = int.Parse(WISText.text);
		AGI = int.Parse(AGIText.text);
	}
}
