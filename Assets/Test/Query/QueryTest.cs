﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.database;

[ExecuteInEditMode]
public class QueryTest : MonoBehaviour
{
    public string fromDatabase;
    public string[] variableName;
    public Condition condition;
    public int tipe;
    public string whereString;
    public int whereInt;
    public float whereFloat;
    public bool whereBool;
    public Buff[] result = new Buff[0];
    void OnEnable ()
    {
        object where = (tipe == 1 ? (object)whereString : tipe == 2 ? (object)whereInt : tipe == 3 ? (object)whereFloat : (object)whereBool);
        result = DatabaseQuery.Instance.Query<Buff>(fromDatabase, variableName, condition, where);
    }
}
