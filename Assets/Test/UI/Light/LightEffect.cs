﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class LightEffect : MonoBehaviour
{
    public Vector2 Range;
    public float StartRange;
    public float Duration;
    private Light _PointLight;

    void Awake()
    {
        _PointLight = GetComponent<Light>();
    }

    void OnEnable()
    {
        DOTween.To(x => _PointLight.range = x, StartRange, LegrandUtility.Random(Range.x, Range.y), Duration).SetLoops(-1, LoopType.Yoyo);
    }
}
