﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MaxContentSizeFitter : MonoBehaviour {
	public ContentSizeFitter SizeFitter;

	public bool ControlWidth;
	public float MaxWidth;

	public bool Controlheight;
	public float MaxHeight;

	private RectTransform rectTransform;

	void Awake () {
		rectTransform = SizeFitter.GetComponent<RectTransform> ();

		ControlSize ();
	}

	void Update () {
		ControlSize ();
	}

	void ControlSize()
	{
		if (ControlWidth) {
			if(rectTransform.sizeDelta.x != MaxWidth)
			{
				SizeFitter.horizontalFit = ContentSizeFitter.FitMode.PreferredSize;
				if(rectTransform.sizeDelta.x > MaxWidth)
				{
					SizeFitter.horizontalFit = ContentSizeFitter.FitMode.Unconstrained;
					rectTransform.sizeDelta = new Vector2(MaxWidth, rectTransform.sizeDelta.y);
				}
				else
				{
					SizeFitter.horizontalFit = ContentSizeFitter.FitMode.PreferredSize;
				}
			}
			
		}
		
		if (Controlheight) {
		}
	}
}
