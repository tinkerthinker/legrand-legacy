﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class SaveAreaIconController : MonoBehaviour
{
    public Area.AreaTypes AreaType;
    public bool SaveArea;
    public float TransitionSpeed = 1f;
    Image[] childs;
    void Awake()
    {
        childs = LegrandUtility.GetComponentsInChildren<Image>(this.gameObject);
    }
    void OnEnable()
    {
        if (EventManager.Instance)
        {
            EventManager.Instance.AddListener<PlayerMoveAreaEvent>(Check);
            EventManager.Instance.AddListener<SetSaveAreaIconEvent>(RefreshAreaIcon);
        }
    }
    void OnDisable()
    {
        if (EventManager.Instance)
        {
            EventManager.Instance.RemoveListener<PlayerMoveAreaEvent>(Check);
            EventManager.Instance.RemoveListener<SetSaveAreaIconEvent>(RefreshAreaIcon);
        }
    }
    void Check(PlayerMoveAreaEvent e)
    {
        if (CheckRoomIsSafe(e.AreaData.areaType))
            setChilds(true);
        else
            setChilds(false);
    }

    bool CheckRoomIsSafe(Area.AreaTypes areaType)
    {
        return areaType == AreaType && GlobalGameStatus.Instance.GetCurrRoomSafety() == SaveArea;
    }

    void RefreshAreaIcon(SetSaveAreaIconEvent e)
    {
        if (e.CheckState && CheckRoomIsSafe(AreaController.Instance.CurrAreaData.areaType))
            setChilds(true);
        else if (!e.CheckState)
            setChilds(e.Enable);
    }

    void setChilds(bool state)
    {
        Color newColor = state == true ? Color.white : Color.clear;
        for (int i = 0; i < childs.Length; i++)
            childs[i].DOColor(newColor, TransitionSpeed);
    }
}
