﻿using UnityEngine;
using System.Collections;

public class ChangeTimeScale : MonoBehaviour {
	public float TimeScale;

	void Start () {
		Time.timeScale = TimeScale;
	}
}
