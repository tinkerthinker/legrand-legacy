﻿using UnityEngine;
using System.Collections;
using System;
using Legrand.core;
using UnityEngine.UI;

public class ItemList : MonoBehaviour, IItem
{
    object thisObj = new object();

    public void RefreshDetail()
    {
        RefreshData();
    }

    public void SetData(object obj)
    {
        thisObj = obj;
        RefreshData();
    }

    void RefreshData()
    {
        string data = (string)thisObj;
        gameObject.name = data.ToString();
    }
}
