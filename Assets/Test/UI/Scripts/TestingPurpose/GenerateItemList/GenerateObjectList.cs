﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using Legrand.database;
using Legrand.core;
using System;

public class GenerateObjectList : MonoBehaviour
{
    private int Amount;
    private Transform ParentList;
    private List<IItem> ItemList = new List<IItem>();
    private List<object> ListObject = new List<object>();
    public GameObject[] PreviewDataObjects;
    List<Button> ButtonList = new List<Button>();

    public GameObject Template;

    protected void SetListParent(Transform transform)
    {
        ParentList = transform;
    }
    private void SetData()
    {
        for(int i=0;i<ItemList.Count;i++)
        {
            ItemList[i].SetData(ListObject[i]);
            GameObject iitemObj = EventSystem.current.currentSelectedGameObject;
            if(iitemObj)
            {
                IItem iitem = iitemObj.GetComponent<IItem>();
                if (iitem != null && ItemList[i] == iitem)
                {
                    ItemList[i].RefreshDetail();
                }
            }
        }
    }
    private void InstantiateItem(int start,int end)
    {
        Template.SetActive(true);
        for (int i = start; i < end; i++)
        {
            GameObject temp = GameObject.Instantiate(Template);
            if(ParentList)
                temp.transform.SetParent(ParentList);
            else
                temp.transform.SetParent(transform);
            temp.transform.position = transform.position;
            temp.transform.localScale = new Vector3(1,1,1);
            ItemList.Add(temp.GetComponent<IItem>());
            ButtonList.Add(temp.GetComponent<Button>());
        }
        Template.SetActive(false);
    }
    private void RemoveItem(int start,int end)
    {
        for (int i = start; i < end; i++)
        {
            Destroy(ButtonList[i].gameObject);
        }
        ItemList.RemoveRange(start, end - start);
        ButtonList.RemoveRange(start, end - start);
    }
    private void Generate()
    {
        if(ItemList.Count < Amount)
        {
            InstantiateItem(ItemList.Count, Amount);
        }
        else if(ItemList.Count > Amount)
        {
            RemoveItem(Amount, ItemList.Count);
        }
    }
    private void SetButtonNavigation(int index)
    {
        LegrandUtility.SetButtonsNavigation(ButtonList, BaseEnum.Orientation.Vertical);
        if(index < ButtonList.Count && index > -1)
            ButtonList[index].Select();
    }
    void SetPreviewData(bool state)
    {
        foreach (GameObject view in PreviewDataObjects)
        {
            view.SetActive(state);
        }
    }
    public void Refresh(IList data,int SelectIndex = 0)
    {
        Template.SetActive(false);
        ListObject.Clear();
        for (int i = 0; i < data.Count; i++)
            ListObject.Add(data[i]);
        Amount = data.Count;
        Generate();
        SetData();
        SetButtonNavigation(SelectIndex);
        SetPreviewData(data.Count > 0 ? true : false);
    }
}
