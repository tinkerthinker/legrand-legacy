﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class TestItemList : GenerateObjectList
{
    public List<string> Data = new List<string>();

    void OnEnable()
    {
        Refresh(Data);
    }
}