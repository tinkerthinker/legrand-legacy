﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public interface IItem
{
    void SetData(object obj);
    void RefreshDetail();
}
