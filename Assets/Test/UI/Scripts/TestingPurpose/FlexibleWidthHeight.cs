﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent (typeof (RectTransform))]
public class FlexibleWidthHeight : MonoBehaviour {
	public enum FlexibleType
	{
		MatchWidthToHeight,
		MatchHeightToWidth
	}
	public FlexibleType sizeMatchMode;
	public Vector2 ratio; //x = width, y = height
	public bool parentReference=true;
	public float parentTop;
	public float parentBottom;
	public float parentLeft;
	public float parentRight;

	private float currHeight;
	private float currWidth;

	// Use this for initialization
	void Start () {
		//Resize (); 	
	}
	
	// Update is called once per frame
	void Update () {
		Resize (); 	
	}

	void Resize()
	{
		if (sizeMatchMode == FlexibleType.MatchHeightToWidth)
		{
			Rect currRect = GetComponent<RectTransform> ().rect;
			if(currRect.width != currWidth)
			{
				currWidth = currRect.width;
				float height = ratio.y/ratio.x * currWidth;
				//height *= PartyManager.Instance.GetCharacter (PauseUIController.instance.selectedCharId).Magics.MagicPresets.Count+1;
				currRect.height = height;
				currHeight = currRect.height;
				GetComponent<RectTransform> ().sizeDelta = new Vector2(currWidth, currHeight); // << Change Rect Transform Height
			}
		}
		else if (sizeMatchMode == FlexibleType.MatchWidthToHeight)
		{
			Rect currRect = GetComponent<RectTransform> ().rect;
			float height = currRect.yMax;// - currRect.yMin;

			if(parentReference)
			{
				if(transform.parent.GetComponent<RectTransform>() != null)
				{
					//calculate height based on parent
					Rect parentRect = transform.parent.GetComponent<RectTransform>().rect;
					height = parentRect.height - (-currRect.yMax - currRect.yMin);
				}
			}

			if(height != currHeight)
			{

				if(parentReference)
				{
				//calculate height based on parent
				//float height = transform.parent.
				}

				//currHeight = currRect.height;
				currHeight = height;
				float width = ratio.x/ratio.y * currHeight;
				//currRect.width = width;
				currWidth = Mathf.Abs(width);
				GetComponent<RectTransform> ().sizeDelta = new Vector2(currWidth, currHeight); // << Change Rect Transform Height
			}
		}
	}

}
