﻿using UnityEngine;
using System.Collections;
using SmartLocalization;

public class TestChangeSuppLanguage : MonoBehaviour {
	public enum LanguageEnum
	{
		en,
		ja
	}
	
	public LanguageEnum SelectLanguageId;

	void Awake()
	{
		LanguageManager.Instance.ChangeLanguage(SelectLanguageId.ToString());
	}
}
