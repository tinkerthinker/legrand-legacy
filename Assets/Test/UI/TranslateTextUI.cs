﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using SmartLocalization;

[RequireComponent(typeof(Text))]
public class TranslateTextUI : MonoBehaviour {
	private string _OriginalText;
	private string _loweredText;
	private string _TranslatedText;

	private string currentLanguage;

	void Awake()
	{
		UpdateData ();
	}

	void Update()
	{
		if (!GetComponent<Text> ().text.Equals (_OriginalText) && !GetComponent<Text> ().text.Equals (_TranslatedText))
			UpdateData ();
	}

	void OnEnable()
	{
		if (!LanguageManager.Instance.LoadedLanguage.Equals (currentLanguage))
			UpdateData ();
		GetComponent<Text> ().text = !string.IsNullOrEmpty(_TranslatedText)? _TranslatedText:_OriginalText;
	}

	void OnDisable()
	{
		GetComponent<Text> ().text = _OriginalText;
	}

	public void UpdateData()
	{
		currentLanguage = LanguageManager.Instance.LoadedLanguage;
		_OriginalText = GetComponent<Text> ().text;
		_loweredText = _OriginalText.ToLower ();
        _TranslatedText = LegrandBackend.Instance.GetLanguageData(_loweredText);

		GetComponent<Text> ().text = !string.IsNullOrEmpty(_TranslatedText)? _TranslatedText:_OriginalText;
	}
}
