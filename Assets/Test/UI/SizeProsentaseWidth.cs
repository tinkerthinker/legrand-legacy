﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SizeProsentaseWidth : MonoBehaviour {
	public LayoutElement PreferredLayout;

	float _ParentWidth;
	float _ProsRight;
	float _ProsLeft;

	float _ProsentaseWidth;

	float oriParWidth;
	
	void Awake()
	{
		_ParentWidth = transform.parent.GetComponent<RectTransform> ().rect.width;
		if (Mathf.Abs (_ProsentaseWidth) == Mathf.Infinity || _ProsentaseWidth == 0) 
			CalculateProsentase ();
		//StartCoroutine(NextFrameCalculate());
		//CalculateProsentase ();
	}

	void OnEnable()
	{
//		Debug.Log (gameObject.GetComponentInParent<IDChar>().gameObject.name);
		//if (Mathf.Abs (_ProsentaseWidth) == Mathf.Infinity || _ProsentaseWidth == 0) 
			//_ParentWidth = transform.parent.GetComponent<RectTransform> ().rect.width;
//		if (Mathf.Abs (_ProsentaseWidth) == Mathf.Infinity || _ProsentaseWidth == 0) 
//			CalculateProsentase ();
		//StartCoroutine(NextFrameCalculate());

		if (Mathf.Abs (_ProsentaseWidth) != Mathf.Infinity) //{
			//getRekt.rect.width = _ProsentaseWidth * _ParentWidth;
			GetComponent<RectTransform> ().sizeDelta = new Vector2 (_ProsentaseWidth * _ParentWidth, GetComponent<RectTransform> ().rect.height);
	}
	
	void Update () {		
	//	if (_ProsentaseWidth == Mathf.Infinity || _ProsentaseWidth == 0)
		if (_ParentWidth != transform.parent.GetComponent<RectTransform> ().rect.width) {
			//CalculateProsentase ();
			if (Mathf.Abs (_ProsentaseWidth) == Mathf.Infinity || _ProsentaseWidth == 0) {
				_ParentWidth = transform.parent.GetComponent<RectTransform> ().rect.width;
				CalculateProsentase ();
			}

			_ParentWidth = transform.parent.GetComponent<RectTransform> ().rect.width;
			RectTransform getRekt = GetComponent<RectTransform> ();

			if (Mathf.Abs (_ProsentaseWidth) != Mathf.Infinity) //{
				//getRekt.rect.width = _ProsentaseWidth * _ParentWidth;
				getRekt.sizeDelta = new Vector2 (_ProsentaseWidth * _ParentWidth, getRekt.rect.height);
//			} else
//				CalculateProsentase ();
		}
	}
	
	void CalculateProsentase()
	{
		float oriObjWidth = GetComponent<RectTransform>().rect.width;
		//if(oriParWidth == 0)
			//oriParWidth = transform.parent.GetComponent<RectTransform> ().rect.width;
		if (PreferredLayout != null)
			oriParWidth = PreferredLayout.preferredWidth;
		else {
			_ParentWidth = transform.parent.GetComponent<RectTransform> ().rect.width;
			oriParWidth = _ParentWidth;
		}

		//_ParentWidth = transform.parent.GetComponent<RectTransform> ().rect.width;
		
		//RectTransform getRekt = GetComponent<RectTransform>();

//		if(oriParWidth == 0)
//			oriParWidth = _ParentWidth;
		_ProsentaseWidth = oriObjWidth / oriParWidth;
	}

	public IEnumerator NextFrameCalculate()
	{
		yield return StartCoroutine(WaitFor.Frames(1)); // wait for 5 frames

		//_ParentWidth = transform.parent.GetComponent<RectTransform> ().rect.width;
		if (Mathf.Abs (_ProsentaseWidth) == Mathf.Infinity || _ProsentaseWidth == 0) 
			CalculateProsentase ();
		Debug.Log("NEXT FRAME prosentase = "+_ProsentaseWidth);
	}
}
