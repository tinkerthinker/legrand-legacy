﻿using UnityEngine;
using System.Collections;

public class EncumberedController : MonoBehaviour {
    public GameObject EncumberedIcon;

    void Start()
    {
        CheckEncumbered(null);
    }

    void OnEnable()
    {
        if (EventManager.Instance)
        {
            EventManager.Instance.AddListener<SetEncumberedEvent>(SetActiveEncumbered);
            EventManager.Instance.AddListener<CheckEncumberedEvent>(CheckEncumbered);
        }
    }
    void OnDisable()
    {
        if (EventManager.Instance)
        {
            EventManager.Instance.RemoveListener<SetEncumberedEvent>(SetActiveEncumbered);
            EventManager.Instance.RemoveListener<CheckEncumberedEvent>(CheckEncumbered);
        }
    }

	void SetActiveEncumbered(SetEncumberedEvent e)
    {
        if(!e.IsEncumbered || (e.IsEncumbered && AreaController.Instance.CurrPlayer.activeInHierarchy))
            EncumberedIcon.SetActive(e.IsEncumbered);
    }

    void CheckEncumbered(CheckEncumberedEvent e)
    {
        if (AreaController.Instance.CurrPlayer.activeInHierarchy)
            EncumberedIcon.SetActive(PartyManager.Instance.Inventory.IsOverWeight);
    }
}
