﻿Shader "Legrand/Creature Dead"
{

	Properties
    {
    	_ColorTint ("Color", Color) = (1, 1, 1, 1)
    	_MainTex ("Texture (RGB)", 2D) = "white" {}
    	_NormalMap ("Normal Map", 2D) = "bump" {}
    	_SliceGuide ("Slice Guide (RGB)", 2D) = "white" {}
    	_SliceAmount ("Slice Amount", Range (0.0, 1.0)) = 0.0
    	_BurnSize ("Burn Size", Range (0.0, 1.0)) = 0.15
    	_BurnRamp ("Burn Ramp (RGB)", 2D) = "white" {}
    	_RimColor ("Rim Color", Color) = (0, 0, 0, 1)
    	_RimPower ("Rim Power", Range (1.0, 6.0)) = 3.0
    	_FlashAmount ("Flash Amount", Range (0.0, 1.0)) = 0.0
    }
    
    SubShader
    {
    	Tags { "RenderType" = "Opaque" }

      	CGPROGRAM
      	#pragma surface surf Lambert addshadow
      	
      	sampler2D _MainTex;
      	sampler2D _NormalMap;
      	sampler2D _SliceGuide;
      	sampler2D _BurnRamp;
      	float _FlashAmount;
      	float _SliceAmount;
      	float _BurnSize;
      	float _RimPower;
      	float4 _RimColor;
      	float4 _ColorTint;
      	
      	struct Input
      	{
      		float2 uv_MainTex;
      		float3 viewDir;
      	};
      	
      	void surf (Input IN, inout SurfaceOutput o)
      	{
         	clip(tex2D (_SliceGuide, IN.uv_MainTex).rgb - _SliceAmount);
			half4 tex = tex2D (_MainTex, IN.uv_MainTex);
			o.Albedo = (lerp (tex.rgb, float3 (1.0, 1.0, 1.0), _FlashAmount)) * _ColorTint;
         	o.Normal = UnpackNormal (tex2D (_NormalMap, IN.uv_MainTex));
			
			half rim = 1.0 - saturate (dot (normalize (IN.viewDir), o.Normal));
			o.Emission = (tex.rgb * 0.8) + (_RimColor.rgb * pow (rim, _RimPower));
      
         	half test = tex2D (_SliceGuide, IN.uv_MainTex).rgb - _SliceAmount;
         	if(test < _BurnSize && _SliceAmount > 0 && _SliceAmount < 1)
         	{
         		o.Emission = tex2D(_BurnRamp, float2(test *(1/_BurnSize), 0));
         	}
        }
        
        ENDCG
	}
	
	Fallback "Diffuse"
}