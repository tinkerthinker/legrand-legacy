﻿Shader "Custom/FogOfWar" {
Properties {
    _Color ("Main Color", Color) = (1,1,1,1)
    _MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
    _FogRadius ("FogRadius", Float) = 1.0
    _FogMaxRadius("FogMaxRadius", Range(0.01,1)) = 0.5
    [Toggle] _UsePlayer1("Use Player1", Float) = 0
    _Player1_Pos ("_Player1_Pos", Vector) = (0,0,0,1)
    _Player1_Fog ("FogRadius", Float) = 1.0
    [Toggle] _UsePlayer2("Use Player2", Float) = 0
    _Player2_Pos ("_Player2_Pos", Vector) = (0,0,0,1)
    _Player2_Fog ("FogRadius", Float) = 1.0
    [Toggle] _UsePlayer3("Use Player3", Float) = 0
    _Player3_Pos ("_Player3_Pos", Vector) = (0,0,0,1)
    _Player3_Fog ("FogRadius", Float) = 1.0
    [Toggle] _UsePlayer4("Use Player4", Float) = 0
    _Player4_Pos ("_Player4_Pos", Vector) = (0,0,0,1)
    _Player4_Fog ("FogRadius", Float) = 1.0
    [Toggle] _UsePlayer5("Use Player4", Float) = 0
    _Player5_Pos ("_Player4_Pos", Vector) = (0,0,0,1)
    _Player5_Fog ("FogRadius", Float) = 1.0
    [Toggle] _UsePlayer6("Use Player4", Float) = 0
    _Player6_Pos ("_Player4_Pos", Vector) = (0,0,0,1)
    _Player6_Fog ("FogRadius", Float) = 1.0
}

SubShader {
    Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
    LOD 200
    Blend SrcAlpha OneMinusSrcAlpha
    Cull Off

    CGPROGRAM
    #pragma surface surf Lambert vertex:vert alpha:blend

    sampler2D _MainTex;
    fixed4     _Color;
    float     _FogRadius;
    float     _FogMaxRadius;
    float4     _Player1_Pos;
    float4     _Player2_Pos;
    float4     _Player3_Pos;
    float4     _Player4_Pos;
    float4     _Player5_Pos;
    float4     _Player6_Pos;
    
    float		_UsePlayer1;
    float		_UsePlayer2;
    float		_UsePlayer3;
    float		_UsePlayer4;
    float		_UsePlayer5;
    float		_UsePlayer6;
    
    float		 _Player1_Fog;
    float		 _Player2_Fog;
    float		 _Player3_Fog;
    float		 _Player4_Fog;
    float		 _Player5_Fog;
    float		 _Player6_Fog;


    struct Input {
        float2 uv_MainTex;
        float2 location;
    };
    
    struct PlayerData
    {
    	float4 position;
    	float fogRadius;
    };

    float powerForPos(float4 pos, float2 nearVertex);
    float powerForPos(float4 pos, float2 nearVertex, float fogRadius);

    void vert(inout appdata_full vertexData, out Input outData) {
        float4 pos = mul(UNITY_MATRIX_MVP, vertexData.vertex);
        float4 posWorld = mul(_Object2World, vertexData.vertex);
        outData.uv_MainTex = vertexData.texcoord;
        outData.location = posWorld.xz;
    }

    void surf (Input IN, inout SurfaceOutput o) {
        fixed4 baseColor = tex2D(_MainTex, IN.uv_MainTex) * _Color;

        float alpha ;//= (1.0 - (baseColor.a + powerForPos(_Player1_Pos, IN.location) + powerForPos(_Player2_Pos, IN.location) + powerForPos(_Player3_Pos, IN.location)));
        float alphaPlayer = baseColor.a;
        if(_UsePlayer1 == 1)
        {
        	alphaPlayer += powerForPos(_Player1_Pos, IN.location, _Player1_Fog);
        }
        
        if(_UsePlayer2 == 1)
        {
        	alphaPlayer += powerForPos(_Player2_Pos, IN.location, _Player2_Fog);
        }
        
        if(_UsePlayer3 == 1)
        {
        	alphaPlayer += powerForPos(_Player3_Pos, IN.location, _Player3_Fog);
        }
        
        if(_UsePlayer4 == 1)
        {
        	alphaPlayer += powerForPos(_Player4_Pos, IN.location, _Player4_Fog);
        }
        
        if(_UsePlayer5 == 1)
        {
        	alphaPlayer += powerForPos(_Player5_Pos, IN.location, _Player5_Fog);
        }
        
        if(_UsePlayer6 == 1)
        {
        	alphaPlayer += powerForPos(_Player6_Pos, IN.location, _Player6_Fog);
        }
        
        alpha = (1.0 - alphaPlayer);

        o.Albedo = baseColor.rgb;
        o.Alpha = alpha;
    }

    //return 0 if (pos - nearVertex) > _FogRadius
    float powerForPos(float4 pos, float2 nearVertex) {
        float atten = clamp(_FogRadius - length(pos.xz - nearVertex.xy), 0.0, _FogRadius);

        return (1.0/_FogMaxRadius)*atten/_FogRadius;
    }
    
    //return 0 if (pos - nearVertex) > _FogRadius
    float powerForPos(float4 pos, float2 nearVertex, float fogRadius) {
        float atten = clamp(fogRadius - length(pos.xz - nearVertex.xy), 0.0, fogRadius);

        return (1.0/_FogMaxRadius)*atten/fogRadius;
    }

    ENDCG
}

Fallback "Transparent/VertexLit"
}