Shader "Legrand/Environment"
{
	Properties
	{
		_Color("Main Color", Color) = (1,1,1,1)
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_NormalMap ("Normal Map", 2D) = "bump" {}
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 200

		CGPROGRAM
		#pragma surface surf Lambert
		
		fixed4 _Color;
		sampler2D _MainTex;
		sampler2D _NormalMap;
		
		struct Input
		{
			float2 uv_MainTex;
			float2 uv_NormalMap;
			float4 color : COLOR;
		};

		void surf (Input IN, inout SurfaceOutput o)
		{
			fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb ;
			o.Albedo *= IN.color.rgb;
			o.Alpha = c.a;
			o.Emission = c.rgb * 0.4;
			o.Normal = UnpackNormal (tex2D (_NormalMap, IN.uv_MainTex));
			
		}
		ENDCG
	} 
	FallBack "Diffuse"
} 
 