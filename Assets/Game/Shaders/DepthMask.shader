Shader "Custom/DepthMask" {
    SubShader {
        Tags { "Queue" = "AlphaTest+1" }
		Pass {
			Blend Zero One
		}       
    }
    FallBack "Diffuse"
}
