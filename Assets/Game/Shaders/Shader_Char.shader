﻿Shader "Legrand/Character"
{
	Properties
	{
		_ColorTint ("Color", Color) = (1, 1, 1, 1)
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_NormalMap ("Normal Map", 2D) = "bump" {}
	}
	SubShader
	{
		Tags { "RenderType" = "Opaque" }
		
		CGPROGRAM
		#pragma surface surf Lambert
		
		float4 _ColorTint;
		sampler2D _MainTex;
		sampler2D _NormalMap;
		
		struct Input
		{
			float2 uv_MainTex;
			float2 uv_NormalMap;
			float3 viewDir;
		};

		void surf (Input IN, inout SurfaceOutput o)
		{
			half4 tex = tex2D (_MainTex, IN.uv_MainTex);
			o.Albedo = tex.rgb * _ColorTint;
			o.Normal = UnpackNormal (tex2D (_NormalMap, IN.uv_NormalMap));
			o.Emission = tex.rgb * 0.8;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
