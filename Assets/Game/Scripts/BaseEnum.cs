﻿using UnityEngine;
using System.Collections;

public class BaseEnum{
	public enum Direction
	{
		North,
		East,
		South,
		West
	}

	public enum Orientation
	{
		Horizontal,
		Vertical,
        Both,
        None
	}

	public enum EventType
	{
		OnEnable,
		OnDisable,
		OnSelect,
		OnDeselect,
		OnCancel,
        OnClick,
        OnStart,
        OnDestroy,
	}
}
