﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;
using Legrand.database;

public class PartyManager : MonoBehaviour
{

	// Tiap Chapter Dibagi jadi 2 fase: Prolog, World
	public enum StoryPhase
	{
		Prolog,
		World
	}

	public static readonly string MainCharactersPath = "Characters/Mains/";

	private static PartyManager s_Instance = null;

	#region Static Function

	public static void SceneFinished (string activatedSceneID)
	{
        if (Instance.StoryProgression.ActiveEventID.Exists(x => x == activatedSceneID))
        {
            Instance.StoryProgression.ActiveEventID.Remove(activatedSceneID);
            Instance.StoryProgression.FinishedEventID.Add(activatedSceneID);
        }
	}

	#endregion

	public List<MainCharacter> CharacterParty;
    public List<MainCharacter> NonActiveParty;
    public Inventory Inventory;
    [System.NonSerialized]
    public Storage Storage;
	public List<BluePrint> UnlockedBlueprints;
	public BattleFormations BattleFormations;
    public string[] StoredBattleFormation;
    public WorldState State;
	public List<WorldStatus> PartyEffect;
	public StoryProgression StoryProgression;

	public List<TutorialSteps> ListTutorialSteps;
	//	public List<TutorialSaveData> TutorialProgress;
	public Dictionary<string, bool> TutorialProgress;

	public WorldInfoModel WorldInfo;

	public int TotalEXP;

	public bool _ReadyToPlay;

	public static PartyManager Instance {
		get {
			if (s_Instance == null) {
				s_Instance = GameObject.FindObjectOfType (typeof(PartyManager)) as PartyManager;
			}
			return s_Instance;
		}
	}

	void Awake ()
	{
		Object.DontDestroyOnLoad (gameObject);
		Initialize ();
	}

	public void Initialize ()
	{
		CharacterParty = new List<MainCharacter> ();
        NonActiveParty = new List<MainCharacter>();
        Inventory = new Inventory ();
        Storage = new Storage();
		UnlockedBlueprints = new List<BluePrint> ();
		BattleFormations = new BattleFormations ();
		PartyEffect = new List<WorldStatus> ();
		WorldInfo = new WorldInfoModel ();
		StoryProgression = new StoryProgression ();
		TutorialProgress = new Dictionary<string, bool> ();
		TotalEXP = 0;
	}

	public void EnableListener ()
	{
		EventManager.Instance.AddListener<PlayerMoveAreaEvent> (PlayerMoveArea);
		EventManager.Instance.AddListener<ChangeModelEvent> (ChangeModel);
	}

    public IEnumerator NewGameInitiation()
    {
        EnableListener();

        foreach (MainCharacter m in CharacterParty)
        {
            if (m != null && m.gameObject != null)
                Destroy(m.gameObject);
            yield return null;
        }

        foreach (MainCharacter m in NonActiveParty)
        {
            if (m != null && m.gameObject != null)
                Destroy(m.gameObject);
            yield return null;
        }

        Initialize();

        // Add Item
        for (int i = 0; i < LegrandBackend.Instance.LegrandDatabase.inventoryData.Count; i++)
        {
            foreach (InventoryModel.ItemID itemInv in LegrandBackend.Instance.LegrandDatabase.inventoryData.Get(i).item)
            {
                Item itemTemp = LegrandBackend.Instance.ItemData[itemInv.AllItemID];
                Inventory.AddItems(LegrandBackend.Instance.ItemData[itemTemp._ID], itemInv.Stack);
                yield return null;
            }
        }

        foreach (ItemModel itemModel in LegrandBackend.Instance.InventoryItems)
        {
            Inventory.AddItems(LegrandBackend.Instance.ItemData[itemModel.ItemId], itemModel.Stack);
            yield return null;
        }

        StoryProgression = new StoryProgression();

        State = new WorldState("Tel Harran", "Tel_Harran_Jubla_Arena", "Portal_west", 1, (int)PartyManager.StoryPhase.Prolog, LegrandBackend.Instance.LegrandDatabase.inventoryData.Get(0).NewGameCash);//new game danaar
        Inventory.Gold = State.currGold;

        TotalEXP = 0;
        SavedQuest firstQuest = new SavedQuest("MQ-0101");
        QuestManager.Instance.InitDataQuest(new List<SavedQuest>() { firstQuest }, null);
        WorldInfo.Copy(LegrandBackend.Instance.WorldInfo);

        AddNewCharacter("0");

        _ReadyToPlay = true;
    }

    public IEnumerator DeserializeFromSave ()
	{
		EnableListener ();

		foreach (MainCharacter m in CharacterParty) {
			if (m != null && m.gameObject != null)
				Destroy (m.gameObject);
			yield return null;
		}

		Initialize ();

        // Add Item Inventory
        foreach (ItemModel itemModel in LegrandBackend.Instance.InventoryItems)
        {
            Inventory.AddItems(LegrandBackend.Instance.ItemData[itemModel.ItemId], itemModel.Stack);
            yield return null;
        }

        // Add Item Storage
        if (LegrandBackend.Instance.StorageItems != null)
        {
            foreach (ItemModel itemModel in LegrandBackend.Instance.StorageItems)
            {
                Storage.AddItems(LegrandBackend.Instance.ItemData[itemModel.ItemId], itemModel.Stack);
                yield return null;
            }
        }

        StoryProgression.Copy(LegrandBackend.Instance.StoryProgressions);

        State = new WorldState(LegrandBackend.Instance.State.gameArea, LegrandBackend.Instance.State.gameSubArea, LegrandBackend.Instance.State.gamePortal, LegrandBackend.Instance.State.currChapter, LegrandBackend.Instance.State.currPhase, LegrandBackend.Instance.State.currGold);
        Inventory.Gold = State.currGold;

        TotalEXP = LegrandBackend.Instance.TotalEXP;
        QuestManager.Instance.InitDataQuest(LegrandBackend.Instance.QuestProgression, LegrandBackend.Instance.TaskProgression);
        WorldInfo.Copy(LegrandBackend.Instance.WorldInfo);

        foreach (CharacterModel model in LegrandBackend.Instance.MainCharacters) {
			CreateCharacter (model, true, model.Active);
			yield return null;
		}

		foreach (BattleFormationModel BattleFormationModel in LegrandBackend.Instance.BattleFormations) {
			bool found = false;
			int index = 0;
			while (!found && index < CharacterParty.Count) {
				if (BattleFormationModel.CharacterID == CharacterParty [index]._ID) {
					BattleFormations.AddFormation (CharacterParty [index], new Vector2 (BattleFormationModel.x, BattleFormationModel.y));
					found = true;
				}
				index++;
				yield return null;
			}
		}

        if (LegrandBackend.Instance.StoredFormations != null)
        {
            StoredBattleFormation = new string[LegrandBackend.Instance.StoredFormations.Count];
            int i = 0;
            foreach (BattleFormationModel BattleFormationModel in LegrandBackend.Instance.StoredFormations)
            {
                StoredBattleFormation[i] = BattleFormationModel.CharacterID + "_" + BattleFormationModel.x + "_" + BattleFormationModel.y;
                i++;
            }
        }

        foreach (TutorialSteps tut in LegrandBackend.Instance.ListTutorialSteps) {
			if (!TutorialProgress.ContainsKey (tut.Name))
				TutorialProgress.Add (tut.Name, false);

			if (LegrandBackend.Instance.TutorialCompleted.Contains (tut.Name))
				TutorialProgress [tut.Name] = true;
			else
				TutorialProgress [tut.Name] = false;
		}

//		for(int inv=0;inv<LegrandBackend.Instance.LegrandDatabase.inventoryData.Count;inv++)
//		{
//			foreach(var it in LegrandBackend.Instance.LegrandDatabase.inventoryData.Get(inv).item)
//			{
//				Inventory.AddItems(LegrandBackend.Instance.GetAllItem(it.All),it.Stack);
//			}
//		}

		_ReadyToPlay = true;
	}
    

	public MainCharacter GetCharacter (string targetId)
	{
		foreach (MainCharacter search in CharacterParty) {
			if (search._ID == targetId) {
				return search;
			}
		}
		return CharacterParty [0];
	}

	public MainCharacter AddNewCharacter (string newCharacterID)
	{
		bool found = false;
		int index = 0;

		MainCharacter newCharacter = null;

        while (!found && index < LegrandBackend.Instance.LegrandDatabase.PartyData.Count)
        {
            PartyDataModelTest partyDataModel = LegrandBackend.Instance.LegrandDatabase.PartyData.Get(index);

            if (newCharacterID.Equals (partyDataModel.ID)) {
				CharacterModel model = new CharacterModel ();
                model.ArmorType = (int)partyDataModel.ArmorType;
				model.AttackType = (int)partyDataModel.AttackType;
				model.gauge = partyDataModel.Gauge;
				model.health = partyDataModel.Health;
				model.ID = partyDataModel.ID;
				model.Name = partyDataModel.Name;
				model.WeaponType = (int)partyDataModel.WeaponType;
				newCharacter = CreateCharacter (model, false);
                if(partyDataModel.Formation.x >= 0f && partyDataModel.Formation.y >= 0f)
                    BattleFormations.AddFormation(newCharacter, partyDataModel.Formation);
                found = true;
			}
			index++;
		}

		return newCharacter;
	}

	public bool RemoveCharacter (string RemovedCharacterID)
	{
		bool found = false;
		int index = 0;

		MainCharacter removedCharacter = null;

		while (!found && index < CharacterParty.Count) {
			if (RemovedCharacterID == CharacterParty [index]._ID) {
				removedCharacter = CharacterParty [index];
				found = true;
			}
			index++;
		}

		if (removedCharacter != null) {
			index--;
			CharacterParty.RemoveAt (index);

			BattleFormations.RemoveFromFormationById (RemovedCharacterID);

			removedCharacter.ReturnItem (0);
			removedCharacter.ReturnItem (1);
			removedCharacter.ReturnItem (2);
			removedCharacter.ReturnItem (3);
			Destroy (removedCharacter.gameObject);

			return true;
		}

		return false;
	}

    public MainCharacter Find(string id)
    {
        MainCharacter mc = CharacterParty.Find(x => x._ID == id);
        if(mc == null) mc = NonActiveParty.Find(x => x._ID == id);
        return mc;
    }

    public bool SetCharacterActive(string ID)
    {
        MainCharacter inactiveMember = NonActiveParty.Find(x => x._ID == ID);
        if(inactiveMember != null)
        {
            NonActiveParty.Remove(inactiveMember);
            CharacterParty.Add(inactiveMember);
            return true;
        }
        return false;
    }

    public bool SetCharacterNonActive(string ID)
    {
        MainCharacter activeMember = CharacterParty.Find(x => x._ID == ID);
        if (activeMember != null)
        {
            CharacterParty.Remove(activeMember);
            NonActiveParty.Add(activeMember);
            BattleFormations.RemoveFromFormationById(ID);
            return true;
        }
        return false;
    }

    public void OnApplicationQuit ()
	{
		s_Instance = null;
	}

	public void PlayerMoveArea (PlayerMoveAreaEvent e)
	{
		State.gameArea = e.AreaData.areaName;
		State.gameSubArea = e.ClusterData.prefabName;
		State.gamePortal = e.Portal;
	}

	public MainCharacter CreateCharacter (CharacterModel c, bool fromSaveGame, bool active = true)
	{
		MainCharacter newCharacter = null;
		newCharacter = Instantiate<MainCharacter> (Resources.Load<MainCharacter> (MainCharactersPath + "Char" + c.ID));

		newCharacter._ID = c.ID;
		newCharacter._Name = c.Name;
        newCharacter.CharacterAttackType = (CharacterAttackType)c.AttackType;
        
        newCharacter.BaseWeapon = new Weapon ((Weapon.DamageType)c.WeaponType);
		newCharacter.BaseArmor = new Armor ((Armor.ArmorType)c.ArmorType);
        newCharacter.CurrentEXP = c.Experience;
		newCharacter.Gauge = c.gauge;
        newCharacter.ClassLevel = c.ClassLevel;
        newCharacter.Level = c.Level;
        newCharacter.AttributePoint = c.ATP;
        newCharacter.LoadCharacter (fromSaveGame);
		if (fromSaveGame)
			newCharacter.StoredExperience = c.Experience;
		else
			newCharacter.StoredExperience = PartyManager.Instance.TotalEXP;
        newCharacter.Equipment().ChangeEquipment(c.EquipmentID);
        newCharacter.gameObject.name = c.Name;
		newCharacter.gameObject.SetActive (false);

        if (active)
            CharacterParty.Add(newCharacter);
        else
            NonActiveParty.Add(newCharacter);

		Vector3 tempLocalPosition = newCharacter.transform.localPosition;
		newCharacter.transform.parent = this.transform;
		newCharacter.transform.localPosition = tempLocalPosition;

		foreach (CharacterMagicPreset chara in newCharacter.Magics.MagicPresets) {
			chara.SetParticleMagic (newCharacter.CharacterAttackType);
		}


		return newCharacter;
	}

	private void ChangeModel (ChangeModelEvent ev)
	{
		foreach (MainCharacter character in CharacterParty) {
			ModelChanger changer = LegrandUtility.GetComponentInChildren<ModelChanger> (character.gameObject);
			if (changer != null && LegrandUtility.CompareString (ev.ModelId, changer.ModelID)) {
                changer.ChangeModel (ev.SetIndex);
                StoryProgression.ChangeModelData(changer.ModelID, ev.SetIndex);
            }
        }
	}

	public void RebindCharacterAnimator (Animator anim)
	{
		StartCoroutine (WaitToRebind (anim));
	}

    public void ApplyStoredFormation()
    {
        string s = string.Empty;
        for (int i=0;i<StoredBattleFormation.Length;i++)
        {
            if (i > 0) s += ",";
            s += StoredBattleFormation[i];
        }
        LegrandSharedEvents.SetFormation(s);
    }

    public void StoreFormation()
    {
        StoredBattleFormation = new string[BattleFormations.Formations.Count];
        int i = 0;
        foreach(BattleFormation formation in BattleFormations.Formations)
        {
            StoredBattleFormation[i] = formation.Character._ID+"_"+formation.Position.x+"_"+formation.Position.y;
            i++;
        }
    }

    public void AddStatstoCharacters(Attribute attType, int value)
    {
        foreach (MainCharacter chara in CharacterParty)
        {
            chara.LevelUp(attType, value);
        }
        foreach (MainCharacter chara in NonActiveParty)
        {
            chara.LevelUp(attType, value);
        }
    }

    private IEnumerator WaitToRebind (Animator anim)
	{
		yield return new WaitForEndOfFrame ();
		anim.Rebind ();
	}
}
