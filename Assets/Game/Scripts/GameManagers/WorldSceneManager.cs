﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using WellFired;

public class WorldSceneManager : MonoBehaviour
{
    public static readonly string PathToScene = "Cinematics/";

    [HideInInspector]
    public static WorldSceneManager _Instance;
    public ScreenFader ScreenFader;
    public Depop Depop;

    ResourceRequest ResourceLoadTask;

    private enum SceneState { Reset, Preload, Unload, Load, Postload, Ready, Run, Count };
    private SceneState _SceneState;
    private delegate void UpdateDelegate();
    private UpdateDelegate[] _UpdateDelegates;
    private string _NextResourceToLoad;
    private TransitionData _TransData;

    // Sequence Property
    private GameObject _SequenceObject;
    private GameObject _LoadedResource;
    // Battle Property
    string PartyId = "";
    GameObject EncounteredMonster;
    int AmbushStats;

    Vector3 _LastPosition;
    Vector3 _LastRotation;

    public static void LoadResource(string ResourceToLoad)
    {
        if (_Instance != null)
        {
            if (_Instance._NextResourceToLoad != ResourceToLoad) _Instance._NextResourceToLoad = ResourceToLoad;
        }
    }

    public static void TransWorldToSequence(GameObject ClusterObject, GameObject Player, string ResourceToLoad)
    {
        if (_Instance != null)
        {
            EventManager.Instance.TriggerEvent(new SetSaveAreaIconEvent(false));
            EventManager.Instance.TriggerEvent(new CutsceneStartedEvent());
            ResourceToLoad = ResourceToLoad.Replace("Cinematics/", "");
            AreaController.Instance.CurrPlayer.GetComponent<PlayerController>().ControllerState = PlayerController.PlayerControllerState.Sequence;
            AreaController.Instance.active = false;

            if (_Instance._NextResourceToLoad != ResourceToLoad) _Instance._NextResourceToLoad = PathToScene + ResourceToLoad;
        }
    }

    public static void TransWorldToSequence(GameObject ClusterObject, GameObject Player, GameObject loadedResource)
    //	Play sequence dengan kondisi Resource sudah terload sebelumnya
    {
        if (_Instance != null)
        {
            EventManager.Instance.TriggerEvent(new SetSaveAreaIconEvent(false));
            Player.GetComponent<PlayerController>().ControllerState = PlayerController.PlayerControllerState.Sequence;
            EventManager.Instance.TriggerEvent(new CutsceneStartedEvent());
            AreaController.Instance.active = false;

            if (AreaController.Instance != null)
            {
                if (AreaController.Instance.CurrPlayer != null)
                {
                    AreaController.Instance.CurrPlayer.SetActive(false);
                }
                if (AreaController.Instance.CurrPrefab != null)
                    AreaController.Instance.CurrPrefab.SetActive(false);
            }

            _Instance._LoadedResource = loadedResource;
            _Instance.ScreenFader.OnFadeOut += _Instance.CutsceneReady;
            _Instance.ScreenFader.EndScene();
        }
    }

    public static void TransWorldToBattle(GameObject ClusterObject, GameObject Player, GameObject monster, string ResourceToLoad, string PartyId, TransitionData TransData, MonsterEncounterEvent.AmbushStat stats)
    {
        if (_Instance != null)
        {
            EventManager.Instance.TriggerEvent(new SetSaveAreaIconEvent(false));
            AreaController.Instance.CurrPlayer.GetComponent<PlayerController>().ControllerState = PlayerController.PlayerControllerState.Sequence;
            _Instance.PartyId = PartyId;
            _Instance._TransData = TransData;
            AreaController.Instance.active = false;
            //SoundManager.PlayBackgroundMusicImmediately("Transisi",false);
            _Instance.EncounteredMonster = monster;
            _Instance.AmbushStats = (int)stats;
            if (_Instance._NextResourceToLoad != ResourceToLoad)
            {
                //				WwiseManager.Instance.PlaySFX(WwiseManager.FX.TRANS_TO_BATTLE);
                _Instance._NextResourceToLoad = ResourceToLoad;
            }
        }
    }

    public static void TransNonWorldToBattle(string ResourceToLoad, string PartyId, TransitionData TransData)
    {
        if (_Instance != null)
        {
            _Instance.PartyId = PartyId;
            _Instance._TransData = TransData;
            _Instance.AmbushStats = (int)InitBuff.Normal;
            if (_Instance._NextResourceToLoad != ResourceToLoad)
            {
                //				WwiseManager.Instance.PlaySFX(WwiseManager.FX.TRANS_TO_BATTLE);
                _Instance._NextResourceToLoad = ResourceToLoad;
            }
        }
    }

    public static void TransNonWorldToSequence(string ResourceToLoad)
    {
        if (_Instance != null)
        {
            if (_Instance._NextResourceToLoad != ResourceToLoad) _Instance._NextResourceToLoad = PathToScene + ResourceToLoad;
        }
    }

    public static void TransToWorld(TransitionData transData)
    {
        if (_Instance != null)
        {
            _Instance._TransData = transData;
            AreaController.Instance.active = true;
            _Instance._NextResourceToLoad = "";
            if (transData.WorldDestination != null && !transData.WorldDestination.Trim().Equals(""))
            {
                LegrandSharedEvents.Teleport(transData.WorldDestination);
            }
        }
    }

    protected void Awake()
    {
        //Setup the singleton instance
        _Instance = this;

        //Setup the array of updateDelegates
        _UpdateDelegates = new UpdateDelegate[(int)SceneState.Count];

        //Set each updateDelegate
        _UpdateDelegates[(int)SceneState.Reset] = UpdateSceneReset;
        _UpdateDelegates[(int)SceneState.Preload] = UpdateScenePreload;
        _UpdateDelegates[(int)SceneState.Unload] = UpdateSceneUnload;
        _UpdateDelegates[(int)SceneState.Load] = UpdateSceneLoad;
        _UpdateDelegates[(int)SceneState.Postload] = UpdateScenePostload;
        _UpdateDelegates[(int)SceneState.Ready] = UpdateSceneReady;
        _UpdateDelegates[(int)SceneState.Run] = UpdateSceneRun;

        //First scene
        _NextResourceToLoad = null;
        _SceneState = SceneState.Run;
        GetComponent<Camera>().orthographicSize = Screen.height / 2;
        EventManager.Instance.AddListener<LastPositionCheckEvent>(LastPositionCheck);
    }

    public void LastPositionCheck(LastPositionCheckEvent e)
    {
        _LastPosition = e.LastPosition;
        _LastRotation = e.LastRotation;
    }

    protected void OnDestroy()
    {
        //Clean up all the updateDelegates
        if (_UpdateDelegates != null)
        {
            for (int i = 0; i < (int)SceneState.Count; i++)
            {
                _UpdateDelegates[i] = null;
            }
            _UpdateDelegates = null;
        }

        //Clean up the singleton instance
        if (_Instance != null)
        {
            _Instance = null;
        }
    }

    protected void OnDisable()
    {

    }

    protected void OnEnable()
    {

    }

    protected void Start()
    {

    }

    protected void Update()
    {
        if (_UpdateDelegates[(int)_SceneState] != null)
        {
            _UpdateDelegates[(int)_SceneState]();
        }
    }

    //Attach the new scene controller to start cascade of loading
    private void UpdateSceneReset()
    {
        // run a gc pass
        System.GC.Collect();
        _SceneState = SceneState.Preload;
    }

    //Handle anything that needs to happen before loading
    private void UpdateScenePreload()
    {
        if (ScreenFader.IsInTransition || Depop.processing) return;

        if (_NextResourceToLoad != "") ResourceLoadTask = Resources.LoadAsync(_NextResourceToLoad);

        _SceneState = SceneState.Unload;
    }

    //Show the loading screen until it's loaded
    private void UpdateSceneLoad()
    {
        //Done loading?
        if (_NextResourceToLoad != "")
        {
            if (ResourceLoadTask.isDone)
            {
                GameObject ScenePrefab = ResourceLoadTask.asset as GameObject;
                GlobalGameStatus.Instance.StateStatus[(int)GlobalGameStatus.GameState.Sequence] = true;
                ScreenFader.StartScene();
                _SequenceObject = (GameObject)Instantiate(ScenePrefab);
                _SequenceObject.name = ScenePrefab.name;
                _SceneState = SceneState.Postload;
            }
        }
        else
        {
            if (AreaController.Instance.CurrPrefab != null)
            {
                AreaController.Instance.CurrPrefab.SetActive(true);
                AreaController.Instance.CurrRoom.UpdateMonsterTrigger();
            }

            if (AreaController.Instance.CurrPlayer != null)
            {
                AreaController.Instance.CurrPlayer.transform.position = _LastPosition;
                AreaController.Instance.CurrPlayer.transform.eulerAngles = _LastRotation;
                AreaController.Instance.CurrPlayer.SetActive(true);
                // Check if there's world cutscene
                if (_TransData != null && _TransData.WorldCutscene != null)
                {
                    AreaController.Instance.CurrPlayer.GetComponent<PlayerController>().ControllerState = PlayerController.PlayerControllerState.Cutscene;
                    GameObject cutscene = Instantiate<GameObject>(_TransData.WorldCutscene);
                    cutscene.name = _TransData.WorldCutscene.name;
                }
                // If not, activate player movement after screen fade;
                else
                    ScreenFader.OnFadeIn += WorldSceneTransitionFinished;
            }

            ScreenFader.StartScene();
            _SceneState = SceneState.Postload;
        }
    }

    //Clean up unused resources by unloading them
    private void UpdateSceneUnload()
    {
        if (AreaController.Instance != null)
        {
            if (AreaController.Instance.CurrPlayer != null)
            {
                AreaController.Instance.CurrPlayer.SetActive(false);
            }
            if (AreaController.Instance.CurrPrefab != null)
                AreaController.Instance.CurrPrefab.SetActive(false);
        }
        if (_SequenceObject != null)
        {
            _SequenceObject.SetActive(false);
            Destroy(_SequenceObject);
        }
        _SceneState = SceneState.Load;

    }

    //Handle anything that needs to happen immediately after loading
    private void UpdateScenePostload()
    {
        if (!PartyId.Equals("")) //Battle
        {
            Battle battlePrefab = _SequenceObject.GetComponent<Battle>();
            EventManager.Instance.AddListener<BattleDonePrepared>(BattlePrepared);
            battlePrefab.StartBattle(PartyId, _TransData, AmbushStats);
            PartyId = "";
            GlobalGameStatus.Instance.StateStatus[(int)GlobalGameStatus.GameState.Battle] = true;
        }

        _NextResourceToLoad = null;
        _SceneState = SceneState.Ready;
    }

    //Handle anything that needs to happen immediately before running
    private void UpdateSceneReady()
    {
        _SceneState = SceneState.Run;
    }

    //Wait for scene change
    private void UpdateSceneRun()
    {
        if (_NextResourceToLoad != null)
        {
            if (_NextResourceToLoad != "" && !PartyId.Equals("")) // Trans battle not using screen fader, but burn fader.
            {
                // Trans Battle from world
                if (_SequenceObject == null)
                {
                    List<GameObject> diactivatedGameobj = new List<GameObject>();
                    diactivatedGameobj.Add(AreaController.Instance.CurrPrefab);
                    diactivatedGameobj.Add(EncounteredMonster);
                    Depop.StartTransition(diactivatedGameobj.ToArray());
                }
                // Trans Battle from non World
                else
                    Depop.StartTransition(_SequenceObject);
            }
            else ScreenFader.EndScene();
            _SceneState = SceneState.Reset;
        }
    }

    private void WorldSceneTransitionFinished()
    {
        // Check if playing dialogue after scene
        ScreenFader.OnFadeIn -= WorldSceneTransitionFinished;
        GlobalGameStatus.Instance.StateStatus[(int)GlobalGameStatus.GameState.Battle] = false;
        // Dialogue id play if id > 0
        if (_TransData != null && _TransData.WorldDialogName != "")
        {
            EventManager.Instance.AddListener<DialogWindowClosed>(DialogueEnd);
            LegrandUtility.StartDialogue(LegrandUtility.GetDialogueID(_TransData.WorldDialogName));
        }
        else
        {
            EventManager.Instance.TriggerEvent(new SetSaveAreaIconEvent(true));
            GlobalGameStatus.Instance.StateStatus[(int)GlobalGameStatus.GameState.Sequence] = false;
            AreaController.Instance.CurrPlayer.GetComponent<PlayerController>().ControllerState = PlayerController.PlayerControllerState.Normal;
        }
    }

    private void CutsceneReady()
    {
        ScreenFader.OnFadeOut -= CutsceneReady;
        _Instance._SequenceObject = (GameObject)Instantiate(_LoadedResource);
        _Instance._SequenceObject.name = _LoadedResource.name;
        ScreenFader.StartScene();
        _Instance._SceneState = SceneState.Postload;
    }
    private void DialogueEnd(DialogWindowClosed d)
    {
        EventManager.Instance.TriggerEvent(new SetSaveAreaIconEvent(true));
        GlobalGameStatus.Instance.StateStatus[(int)GlobalGameStatus.GameState.Sequence] = false;
        EventManager.Instance.TriggerEvent(new TalkToPlayerEvent(true));
        EventManager.Instance.RemoveListener<DialogWindowClosed>(DialogueEnd);
    }

    private void BattlePrepared(BattleDonePrepared e)
    {
        EventManager.Instance.RemoveListener<BattleDonePrepared>(BattlePrepared);
        WwiseManager.Instance.PlaySFX(WwiseManager.BattleFX.Battle_transition_fire);
        Depop.StartDepop(AmbushStats);
        EventManager.Instance.TriggerEvent(new OpenBattleEvent());
    }
}
