﻿using UnityEngine;
using System.Collections;

public class ChangeController : GameEvent
{
	public ControllerType CtrlType;

	public ChangeController(ControllerType ctrlType)
	{
		CtrlType = ctrlType;
	}
}