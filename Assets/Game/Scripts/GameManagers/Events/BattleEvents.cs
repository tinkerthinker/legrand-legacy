﻿using UnityEngine;
using System.Collections;

public class FleeEvent:GameEvent{}

public class OnHit:GameEvent{}

public class CameraFollowEvent : GameEvent
{
	public Transform FollowedObject;

	public CameraFollowEvent(Transform followedObject)
	{
		FollowedObject = followedObject; 
	}
}
public class MoveBattlerEvent:GameEvent
{
	public Vector3 Distance;

	public MoveBattlerEvent(Vector3 newDistance)
	{
		this.Distance = newDistance;
	}
}

public class BattleTransitionDone:GameEvent{}

public class BattleEndEvent:GameEvent{}

public class CameraPreviewEncounter:GameEvent{}

public class CameraStartBattle:GameEvent{}

public class CameraRandomView:GameEvent{}

public class CameraEndBattle:GameEvent{}

public class CameraGameOver:GameEvent{}

public class CameraDeadCinematic:GameEvent{
    public Battler bat;
    public Transform deadTrans;

    public CameraDeadCinematic (Battler battler, Transform deadCamTrans){
        this.bat = battler;
        this.deadTrans = deadCamTrans;
    }
}

public class BattlerBuffEvent : GameEvent
{
    public string[] CharacterNameBuffed;
    public Buff[] BattlerBuff;

    public BattlerBuffEvent(string[] characterNameBuffed, Buff[] battlerBuff)
    {
        CharacterNameBuffed = characterNameBuffed;
        BattlerBuff = battlerBuff;
    }
}

public struct OnDialogStart
{
    public string Name;
    public string Dialog;
    public string Response;

    public OnDialogStart(string name,string dialog,string response)
    {
        this.Name = name;
        this.Dialog = dialog;
        this.Response = response;
    }
}

public struct OnDialogEnd
{
    public string Respond;

    public OnDialogEnd(string responds)
    {
        Respond = responds;
    }
}

public struct OnDialogFinish
{
}

public class SetTimeHitStatusEvent:GameEvent
{
	public Battler Attacker;
	public Battler[] Defenders;
	public bool IsTimeHit;

	public SetTimeHitStatusEvent(Battler attacker, Battler[] defenders, bool isTimeHit)
	{
		this.Attacker = attacker;
		this.Defenders = defenders;
		this.IsTimeHit = isTimeHit;
	}
}

public class ForceEndAction:GameEvent{}

public class UpdateInfoTextEvent:GameEvent
{
	public string NewInfo;

	public UpdateInfoTextEvent(string newInfo)
	{
		NewInfo = newInfo;
	}
}

public class BattleDonePrepared : GameEvent
{
}

public class OpenBattleEvent : GameEvent
{
}