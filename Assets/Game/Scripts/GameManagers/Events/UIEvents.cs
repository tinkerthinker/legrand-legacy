﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;

public class UIEvent : GameEvent
{
    public bool isOpen;
}

public class MenuInGameEvent : UIEvent
{
	public MenuInGameEvent(bool p)
	{
		isOpen = p;
	}
}

public class AlchemistShopEvent : UIEvent
{
    public int level;

	public AlchemistShopEvent(bool p,int Level)
	{
		isOpen = p;
        level = Level;
	}
    public AlchemistShopEvent(bool p)
    {
        isOpen = p;
    }
}

public class CraftingShopEvent : UIEvent
{
    public int level;

    public CraftingShopEvent(bool p,int Level)
    {
        isOpen = p;
        level = Level;
    }
    public CraftingShopEvent(bool p)
    {
        isOpen = p;
    }
}

public class ShopEvent : UIEvent
{
	public string ShopModelId;
	
	public ShopEvent(bool p)
	{
		isOpen = p;
	}
	
	public ShopEvent(bool p, string shopId)
	{
		isOpen = p;
		ShopModelId = shopId;
	}
}

public class FastTravelUIEvent : UIEvent
{
    public FastTravelUIEvent(bool p)
    {
        isOpen = p;
    }
}

public class StorageUIEvent : UIEvent
{
    public StorageUIEvent(bool p)
    {
        isOpen = p;
    }
}

public class PauseEvent : GameEvent{}

public class UnPauseEvent : GameEvent{}

public class ChangeUICharSelectedID :GameEvent{
	public string Id;

	public ChangeUICharSelectedID (string id)
	{
		this.Id = id;
	}
	
}

public class DialogWindowClosed : GameEvent{}

public class DialogTextComplete : GameEvent
{
}

public class ConsumeItemEvent : GameEvent
{
	public Consumable Consumeable;
	
	public ConsumeItemEvent(Consumable consumeable)
	{
		this.Consumeable = consumeable;
	}
}

public class InspectItemEvent : GameEvent
{
	public Item Item;
	public bool IsShown;

	public InspectItemEvent (Item item, bool isShown = true)
	{
		this.Item = item;
		this.IsShown = isShown;
	}	
}

public class InspectQuestEvent : GameEvent
{
	public Quest Quest;
	public bool IsShown;

	public InspectQuestEvent (Quest quest, bool isShown = true)
	{
		this.Quest = quest;
		this.IsShown = isShown;
	}	
}

public class InspectMagicSkillEvent : GameEvent
{
    public Magic Magic;
    public NormalSkill Skill;
    public bool IsShown;

    public InspectMagicSkillEvent(Magic magic, NormalSkill skill, bool isShown)
    {
        this.Magic = magic;
        this.Skill = skill;
        this.IsShown = isShown;
    }
}

public class InspectEnhanceRecipeEvent : GameEvent
{
    public EquipmentDataModel Equip;
    public BluePrint Blueprint;
    public int CreateAmount;
    public bool IsShown;

    public InspectEnhanceRecipeEvent(EquipmentDataModel equip,BluePrint blueprint,int createAmount, bool isShown = true)
    {
        this.Equip = equip;
        this.Blueprint = blueprint;
        this.CreateAmount = createAmount;
        this.IsShown = isShown;
    }
}

public class InspectCombineAlchemistEvent : GameEvent
{
    public Item Item;
    public BluePrint Blueprint;
    public int CreateAmount;
    public bool IsShown;

    public InspectCombineAlchemistEvent(Item item, BluePrint blueprint, int createAmount, bool isShown = true)
    {
        this.Item = item;
        this.Blueprint = blueprint;
        this.CreateAmount = createAmount;
        this.IsShown = isShown;
    }
}

public class InspectAttributeEvent : GameEvent
{
    public Legrand.core.MainCharacter Chara;
    public Legrand.core.Attribute Attrib;

    public InspectAttributeEvent(Legrand.core.MainCharacter chara, Legrand.core.Attribute attrib)
    {
        this.Chara = chara;
        this.Attrib = attrib;
    }
}

public class InspectCharacterEvent : GameEvent
{
    public MainCharacter Chara;
    public string CharId;
    public bool IsVisible;

    public InspectCharacterEvent(MainCharacter chara, bool isVisible = true)
    {
        this.Chara = chara;
        this.CharId = chara._ID;
        this.IsVisible = isVisible;
    }

    public InspectCharacterEvent(string charId, bool isVisible = true)
    {
        this.CharId = charId;
        this.IsVisible = isVisible;
    }
}

public class RequestSimulateAttributeEvent : GameEvent
{
    public string CharId;

    public RequestSimulateAttributeEvent(string charId)
    {
        this.CharId = charId;
    }
}

public class SendSimulateAttributeEvent : GameEvent
{
    public MainCharacter Chara;
    public Dictionary<Attribute, int> SimulateAttrib;

    public SendSimulateAttributeEvent(MainCharacter chara, Dictionary<Attribute, int> simulateAttrib)
    {
        this.Chara = chara;
        this.SimulateAttrib = simulateAttrib;
    }
}

public class SelectFormationPositionEvent : GameEvent
{
    public Vector2 Position;

    public SelectFormationPositionEvent(Vector2 position)
    {
        this.Position = position;
    }
}

public class SelectItemEvent : GameEvent
{
	public Item Item;

	public SelectItemEvent (Item item)
	{
		this.Item = item;
	}	
}

public class ComparingEvent : GameEvent
{
	public bool IsCompare;

	public ComparingEvent (bool isCompare)
	{
		this.IsCompare = isCompare;
	}
}

public class ChangeGameStateEvent : GameEvent{}

public class AssignObjectsDialogueEvent : GameEvent{
	public List<TalkingObject> TalkingObjects;
	
	public AssignObjectsDialogueEvent(List<TalkingObject> talkingObjects)
	{
		this.TalkingObjects = talkingObjects;
	}
}

public class RemoveObjectsDialogueEvent : GameEvent
{
    public List<TalkingObject> TalkingObjects;

    public RemoveObjectsDialogueEvent(List<TalkingObject> talkingObjects)
    {
        this.TalkingObjects = talkingObjects;
    }
}

public class AssignNewCommandEvent : GameEvent{
	public string CharID;

	public AssignNewCommandEvent (string charID)
	{
		this.CharID = charID;
	}	
}

public class SetAmountEvent : GameEvent{
	public int Amount;

	public SetAmountEvent (int amount)
	{
		this.Amount = amount;
	}	
}

public class ChoosenCommandDataEvent : GameEvent
{
    public CommandTypes Type;
    public GameObject SentGameObject;
    private Item Item;
    private Magic Magic;
    private NormalSkill Skill;

    /// <summary>
    /// 
    /// </summary>
    /// <param name="type">the type of event</param>
    /// <param name="data">send the data in correct class, otherwise it will catch an error or null. CommandType -> Class : Item -> Item, Magic -> Magic, Skill -> NormalSkill</param>
    public ChoosenCommandDataEvent(CommandTypes type, object data, GameObject sentGameObject)
    {
        Type = type;
        switch(type)
        {
            case CommandTypes.Item:
                Item = (Item)data;
                break;
            case CommandTypes.Magic:
                Magic = (Magic)data;
                break;
            case CommandTypes.Skill:
                Skill = (NormalSkill)data;
                break;
        }

        SentGameObject = sentGameObject;
    }

    public System.Type GetSentType()
    {
        if(Type == CommandTypes.Item && Item != null)
        {
            return typeof(Item);
        }
        else if (Type == CommandTypes.Magic && Magic != null)
        {
            return typeof(Magic);
        }
        else if (Type == CommandTypes.Skill && Skill != null)
        {
            return typeof(NormalSkill);
        }

        return typeof(object);
    }

    public object GetData()
    {
        if (Type == CommandTypes.Item && Item != null)
        {
            return Item;
        }
        else if (Type == CommandTypes.Magic && Magic != null)
        {
            return Magic;
        }
        else if (Type == CommandTypes.Skill && Skill != null)
        {
            return Skill;
        }

        return null;
    }
}

public class SimulateExpEvent : GameEvent
{
	public Legrand.core.Attribute Attrib;
	public int Value;

	public SimulateExpEvent (Legrand.core.Attribute attrib, int value)
	{
		this.Attrib = attrib;
		this.Value = value;
	}
	
}

public class DistributeExpEvent : GameEvent
{
	
}

public class SetFormationPositionEvent : GameEvent
{
	public string IdChar;
	public int Row, Col;

	public SetFormationPositionEvent (string idChar, int row, int col)
	{
		this.Row = row;
		this.Col = col;
		this.IdChar = idChar;
	}
}

#region PopUp
public class CloseShoutPopUpEvent:GameEvent
{
	public string ShoutDialogue;

	public CloseShoutPopUpEvent (string shoutDialogue)
	{
		this.ShoutDialogue = shoutDialogue;
	}
}

public class RequestObjectPopUp:GameEvent
{
    public string TypePopUp;

    public RequestObjectPopUp(string typePopUp)
    {
        this.TypePopUp = typePopUp;
    }
}

public class RespondObjectPopUp : GameEvent
{
    public GameObject PopUp;

    public RespondObjectPopUp(GameObject popUp)
    {
        this.PopUp = popUp;
    }
}
#endregion

public class InventoryChangedEvent : GameEvent { }

public class SendAcrossStorageEvent : GameEvent
{
    public Item Item;
    public int Amount;

    public SendAcrossStorageEvent(Item item, int amount)
    {
        this.Item = item;
        this.Amount = amount;
    }
}

public class ScrollNextPageEvent : GameEvent
{
    public bool CanScroll;

    public ScrollNextPageEvent(bool canScroll)
    {
        this.CanScroll = canScroll;
    }
}