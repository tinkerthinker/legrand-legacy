﻿using UnityEngine;
using System.Collections;

public class SequenceEvent : GameEvent
{
	public bool IsSequenceFinished;
	
	public SequenceEvent(bool finishSequence)
	{
		IsSequenceFinished = finishSequence;
	}
}

public class StartScreenEvent:GameEvent{}

public class ResumeSequenceEvent : GameEvent
{
	public float SkipTo;

	public ResumeSequenceEvent()
	{
		SkipTo = -1f;
	}

	public ResumeSequenceEvent(float goToTime)
	{
		SkipTo = goToTime;
	}

}

public class LastPositionCheckEvent:GameEvent
{
	public Vector3 LastPosition;
	public Vector3 LastRotation;

	public bool IsPositionChange;
	public bool IsRotationChange;

	public LastPositionCheckEvent(Vector3 lastPosition, Vector3 lastRotation)
	{
		LastPosition = lastPosition;
		LastRotation = lastRotation;
		IsPositionChange = true;
		IsRotationChange = true;
	}

	public LastPositionCheckEvent(bool isPosition, Vector3 lastVector)
	{
		if (isPosition) 
		{
			LastPosition = lastVector;
			IsPositionChange = true;
			IsRotationChange = false;
		} 
		else 
		{
			LastRotation = lastVector;
			IsPositionChange = false;
			IsRotationChange = true;
		}

	}
}

public class NextClipEvent : GameEvent{}

public class CutsceneZoomEvent : GameEvent{}

public class PlayAnimationEvent : GameEvent
{
    public string ActorID;
    public string Animation;
    public bool Crossfade;
    public int Layer;

    public PlayAnimationEvent(string actor, string animation, int layer = -1)
    {
        ActorID = actor;
        Animation = animation;
        Layer = layer;
        Crossfade = false;
    }

    public PlayAnimationEvent(string actor, string animation, bool crossfade,int layer = -1)
    {
        ActorID = actor;
        Animation = animation;
        Layer = layer;
        Crossfade = crossfade;
    }
}