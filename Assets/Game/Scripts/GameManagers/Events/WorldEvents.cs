﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnterTrigger : GameEvent
{
}

public class ExitTrigger : GameEvent
{

}

public class PlayerMoveAreaEvent : GameEvent
{
	public Area AreaData;
	public SubArea ClusterData;
	public string Portal;
	
	public PlayerMoveAreaEvent(Area _NewArea, SubArea _NewCluster, string _NewPortal)
	{
		AreaData = _NewArea;
		ClusterData = _NewCluster;
		Portal = _NewPortal;
	}
}

public class SetSaveAreaIconEvent : GameEvent
{
    public bool CheckState;
    public bool Enable;

    public SetSaveAreaIconEvent(bool checkState, bool enable = false)
    {
        this.CheckState = checkState;
        this.Enable = enable;
    }
}

public class RefreshRoomEvent : GameEvent{}

public class Interact : GameEvent
{
	public GameObject TalkedCharacter;
	
	public Interact(GameObject g)
	{
		TalkedCharacter = g;
	}
	
}

public class RespondInteract : GameEvent
{
    public GameObject Target;

    public RespondInteract(GameObject target)
    {
        Target = target;
    }
}

//public class NewEncounterPartiesEvent : GameEvent
//{
//	public List<string> EncounterPartiesId;
	
//	public NewEncounterPartiesEvent(List<string> newEncounterPartiesId)
//	{
//		EncounterPartiesId = newEncounterPartiesId;
//	}
//}

public class MonsterEncounterEvent : GameEvent
{
	public GameObject MonsterEncounterObject;
    public enum AmbushStat
    {
        Neutral,
        Player,
        Enemy
    }
    public AmbushStat AmbushAdvantage;
	
	public MonsterEncounterEvent(GameObject monsterObject, AmbushStat ambushAdvantage)
	{
		MonsterEncounterObject = monsterObject;
        AmbushAdvantage = ambushAdvantage;
	}
}

public class MinigameNpcEvent : GameEvent
{
    public string npcID;

    public MinigameNpcEvent(string npcID)
    {
        this.npcID = npcID;
    }
}

public class TalkToPlayerEvent : GameEvent
{
	public bool FinishTalking;
	
	public TalkToPlayerEvent(bool finishTalking)
	{
		FinishTalking = finishTalking;
	}
}

public class FootStepEvent : GameEvent
{
	public bool IsRun;
	public GameObject Feet;
	
	public FootStepEvent(bool IsRun, GameObject feet)
	{
		this.IsRun = IsRun;
		this.Feet = feet;
	}
}

public class UnlockCityPointsEvent : GameEvent
{
    public string[] pointsToUnlock;

    public UnlockCityPointsEvent(string[] name)
    {
        pointsToUnlock = name;
    }
}

public class UnlockWorldMapPointEvent : GameEvent
{
    public string[] pointsToUnlock;

    public UnlockWorldMapPointEvent(string[] name)
    {
        pointsToUnlock = name;
    }
}

public class TogglePortalIndicatorEvent : GameEvent
{
    public bool isRendered;

    public TogglePortalIndicatorEvent(bool isRendered)
    {
        this.isRendered = isRendered;
    }
}

public class ModRoomVersionEvent : GameEvent
{
    public SubAreaInfo modInfo;
    public bool modCurrentRoom;
    
    public ModRoomVersionEvent(SubAreaInfo modInfo, bool modCurrentRoom = false)
    {
        this.modInfo = modInfo;
        this.modCurrentRoom = modCurrentRoom;
    }
}

public class ChangeModelEvent : GameEvent
{
	public string ModelId;
	public int SetIndex;

	public ChangeModelEvent(string newModelId, int newSetIndex)
	{
		this.ModelId = newModelId;
		this.SetIndex = newSetIndex;
	}
}

public class PoisonEvent : GameEvent{}

public class FrostBiteResetEvent : GameEvent
{
    public float FadeTime;

    public FrostBiteResetEvent(float fadeTime)
    {
        this.FadeTime = fadeTime;
    }
}

public class FrostBiteEvent : GameEvent
{
    public bool IsDamage;
    public FrostBite.FrostEffect Effect;

    public FrostBiteEvent(FrostBite.FrostEffect effect,bool damage)
    {
        this.IsDamage = damage;
        this.Effect = effect;
    }
}

public class TeleportPlayerEvent : GameEvent
{
    public string targetPlace;
    public string targetPortal;

    public TeleportPlayerEvent(string targetPlace, string targetPortal = null)
    {
        this.targetPlace = targetPlace;
        this.targetPortal = targetPortal;
    }
}

public class LoadAreaEvent : GameEvent
{
    public Area AreaData;

    public LoadAreaEvent(Area area)
    {
        AreaData = area;
    }
}

public class StartTimerEvent : GameEvent
{
    public float timer;

    public StartTimerEvent(float timer)
    {
        this.timer = timer;
    }
}

public class FirstStep : GameEvent { }

public class CutsceneStartedEvent : GameEvent {}

public class SetEncumberedEvent : GameEvent
{
    public bool IsEncumbered;

    public SetEncumberedEvent(bool isEncumbered)
    {
        this.IsEncumbered = isEncumbered;
    }

}

public class CheckEncumberedEvent : GameEvent { }
