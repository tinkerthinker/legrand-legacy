public class AK
{
    public class EVENTS
    {
        public static uint ADD_STATUS_CHARACTER = 975374775U;
        public static uint ALL_DEAD_MONSTERS = 1004016167U;
        public static uint ARIA_AIM = 3564805576U;
        public static uint ARIA_ATTACK = 2565961105U;
        public static uint ARIA_CASTING_MAGIC = 1768243184U;
        public static uint ARIA_DEAD = 2276551071U;
        public static uint ARIA_JUMP = 1934541741U;
        public static uint ARIA_KICK_FINN = 3576083933U;
        public static uint ARIA_POWER_SHOOT_ARROW_SOUND = 3032631282U;
        public static uint ARIA_POWER_SHOOT_ATTACK = 4249148739U;
        public static uint ARIA_POWER_SHOOT_CHARGING = 1287685858U;
        public static uint ARIA_POWER_SHOOT_HIT = 81216594U;
        public static uint ARIA_SCREAM = 3580254290U;
        public static uint ARKWORA_VOICE = 1671502825U;
        public static uint BALOON = 3305856046U;
        public static uint BATTLE_BGM_PLAY = 2380075085U;
        public static uint BATTLE_BGM_STOP = 3497269147U;
        public static uint BATTLE_POINTER = 3345091749U;
        public static uint BATTLE_TRANSITION = 3064055327U;
        public static uint BATTLE_TRANSITION_FIRE = 133684814U;
        public static uint BATTLE_TRANSITION_WIND = 2391973860U;
        public static uint BEFORE_ARIA_APPEAR = 371093020U;
        public static uint BLACKSMITH = 3435117315U;
        public static uint BLAST_HIT = 1498934575U;
        public static uint BLOOD_SPLASH_RAHAS_CAVE_CUTSCENE = 793117904U;
        public static uint BLOODSTONE = 2768015850U;
        public static uint BONFIRE_RAHAS_CAVE_CUTSCENE_PLAY = 340099046U;
        public static uint BONFIRE_RAHAS_CAVE_CUTSCENE_STOP = 2149523672U;
        public static uint BORTTHUNG_ATTACK = 4157510917U;
        public static uint BORTTHUNG_HIT = 2719730904U;
        public static uint BORTTHUNG_INTRO = 2660961521U;
        public static uint BORTTHUNG_SCREAM = 3824648518U;
        public static uint BOSS_BATTLE_1_PLAY = 1606429620U;
        public static uint BOSS_BATTLE_1_STOP = 3310435214U;
        public static uint BUTTON_ERROR = 531269298U;
        public static uint BUY = 714721615U;
        public static uint CANCEL = 2760337059U;
        public static uint CLOWTHERS_ATTACK = 4038766269U;
        public static uint CLOWTHERS_HIT = 504660176U;
        public static uint CLOWTHERS_INTRO = 1431388121U;
        public static uint CLOWTHERS_SCREAM = 2568286142U;
        public static uint CLOWTHERS_SOUND = 4034895442U;
        public static uint CLOWTHERS_WING_DOWN = 1145436047U;
        public static uint CLOWTHERS_WING_UP = 3657161080U;
        public static uint CONFIRM = 1517537939U;
        public static uint CROSSBONE = 233492797U;
        public static uint CURSOR_MOVING = 685702492U;
        public static uint DIALOG_CONFIRM = 858102348U;
        public static uint DORKADHOUR_ATTACK = 3661006893U;
        public static uint DORKADHOUR_HIT = 1958083136U;
        public static uint DORKADHOUR_INTRO = 2346425961U;
        public static uint DORKADHOUR_SCREAM = 3941712302U;
        public static uint DREAM_GEDDO_FACE_ZOOM_IN_CUTSCENE = 1725635407U;
        public static uint DRINGRS_KEEP_GROUND = 1958779670U;
        public static uint DRINGRS_KEEP_OPEN_DOOR = 2276569246U;
        public static uint EARTHQUAKE = 1245902094U;
        public static uint ERIS_ATTACK = 2271154043U;
        public static uint ERIS_EFISIENT_CASTING = 2752521924U;
        public static uint ERIS_FORCE_MISILLE_CAST = 1013805728U;
        public static uint ERIS_FORCE_MISILLE_CHANNELING = 1822912230U;
        public static uint ERIS_FORCE_MISILLE_HIT = 2380797600U;
        public static uint ERIS_FORCE_MISILLE_JOURNEY = 4062226323U;
        public static uint EXP_GAIN = 642116364U;
        public static uint FINN_ARC_SLAH_KIBASAN_AWAL = 1059440890U;
        public static uint FINN_ARC_SLASH_FINAL_HIT = 3489823702U;
        public static uint FINN_ARC_SLASH_HIT = 2212384175U;
        public static uint FINN_ARC_SLASH_JUMP = 609756972U;
        public static uint FINN_ATTACK = 849037029U;
        public static uint FINN_BAD_DREAM_CUTSCENE_PLAY = 2987534014U;
        public static uint FINN_BAD_DREAM_CUTSCENE_STOP = 3983850112U;
        public static uint FINN_CHANNELING_MAGIC = 4270940510U;
        public static uint FINN_COUGH = 304492225U;
        public static uint FINN_DEAD = 79040795U;
        public static uint FINN_DETERMINATION = 3562779982U;
        public static uint FINN_DRINKING = 2347332191U;
        public static uint FINN_SCREAM = 516174374U;
        public static uint FLURDIAANUM_ATTACK = 1520308966U;
        public static uint FLURDIAANUM_HIT = 3092570669U;
        public static uint FLURDIAANUM_INTRO = 3287859328U;
        public static uint FLURDIANUM_SCREAM = 2316632792U;
        public static uint FOOTSTEP_DESSERT = 4052139344U;
        public static uint FOOTSTEP_DRINGRS_KEEP = 1183520215U;
        public static uint FOOTSTEP_ROCK = 2151419677U;
        public static uint GEDDO_ATTACK = 1498764607U;
        public static uint GEDDO_BATTLE_WITH_RAMPOKS_PLAY = 3654440393U;
        public static uint GEDDO_BATTLE_WITH_RAMPOKS_STOP = 881993199U;
        public static uint GEDDO_CHANNELING_MAGIC = 398247088U;
        public static uint GEDDO_SLASH_BY_RAMPOKS = 3276773706U;
        public static uint GLADIATOR_ATTACK = 1923588099U;
        public static uint GLADIATOR_BATTLE_PLAY = 3535472154U;
        public static uint GLADIATOR_BATTLE_STOP = 681060644U;
        public static uint GLADIATOR_HIT = 3283421970U;
        public static uint GLADIATOR_INTRO = 2402202559U;
        public static uint GLADIATOR_SCREAM = 1312245992U;
        public static uint GUARD = 740892324U;
        public static uint HEAL_AREA = 3331984369U;
        public static uint HOROR_SCREAM_DRINGRS_KEEP_ENTRANCE = 630232342U;
        public static uint INFRONT_OF_ARENA = 4128701747U;
        public static uint INSIDE_RAHAS_CAVE_CUTSCENE_BGM_PLAY = 4162455118U;
        public static uint INSIDE_RAHAS_CAVE_CUTSCENE_BGM_STOP = 910603568U;
        public static uint INTENSE_BEFORE_BATTLE_WITH_KLAHMARAN = 3054583231U;
        public static uint INTENSE_BEFORE_RAMPOKS = 3941884373U;
        public static uint ITEM_USE = 905085692U;
        public static uint JOIN_PARTY_BGM = 400258007U;
        public static uint KALKKHUKKA_ATTACK = 1506485958U;
        public static uint KALKKHUKKA_HIT = 840042125U;
        public static uint KALKKHUKKA_INTRO = 513935328U;
        public static uint KALKKHUKKA_SCREAM = 743452417U;
        public static uint KLAHMARAN_ATTACK = 614974003U;
        public static uint KLAHMARAN_DEAD = 4220045385U;
        public static uint KLAHMARAN_DEATH_SCREAM = 2185986325U;
        public static uint KLAHMARAN_FALL_BEFORE_DEATH = 2210084177U;
        public static uint KLAHMARAN_INTRO = 1058130447U;
        public static uint KLAHMARAN_MAGIC = 4185626048U;
        public static uint KLAHMARAN_SCREAM = 2083239384U;
        public static uint KLAHMARAN_SWORD_BEFORE_DEAD = 4118117669U;
        public static uint KOJVAANA_ATTACK = 1018874209U;
        public static uint KOJVAANA_HIT = 1268080676U;
        public static uint KOJVAANA_INTRO = 1589245069U;
        public static uint KOJVAANA_SCREAM = 2052339170U;
        public static uint LAZARUS_ENDING_BGM_PLAY = 2274464369U;
        public static uint LAZARUS_ENDING_BGM_STOP = 3567456039U;
        public static uint LEVEL_UP = 2145291615U;
        public static uint LIVE_2D_CONFIRM = 891027245U;
        public static uint MAGIC_ERIS_EXPLOTION = 71965861U;
        public static uint MUNERATOR_HIT_FINN = 217636596U;
        public static uint NPC_FIGHT_AT_MARKET = 2508479862U;
        public static uint NUTT = 918348566U;
        public static uint PLAY_DRINGRS_KEEP = 3759105791U;
        public static uint PLAY_FMV_CUTSCENE = 4116648476U;
        public static uint PLAY_FMV_CUTSCENE_01 = 2754889666U;
        public static uint PLAY_FMV_CUTSCENE_02 = 2754889665U;
        public static uint PLAY_FMV_CUTSCENE_03 = 2754889664U;
        public static uint PLAY_FMV_CUTSCENE_04 = 2754889671U;
        public static uint PLAY_FOOTSTEP = 1602358412U;
        public static uint PLAY_LOSING_BATTLE = 2805483837U;
        public static uint PLAY_MAIN_MENU = 3306210749U;
        public static uint PLAY_RAHAS_DESERT = 1198150309U;
        public static uint PLAY_TEL_HARAN = 3733814272U;
        public static uint PLAY_WINNING_BATTLE = 1261199315U;
        public static uint POISONED = 1467164614U;
        public static uint PRISON_METAL_VOICE_PLAY = 2653568760U;
        public static uint PRISON_METAL_VOICE_STOP = 731048778U;
        public static uint RAMPOKS_ATTACK = 2859204969U;
        public static uint RAMPOKS_BATTLE_PLAY = 3693092684U;
        public static uint RAMPOKS_BATTLE_STOP = 1367909974U;
        public static uint RAMPOKS_HIT_FINN_CUTSCENE = 2126873235U;
        public static uint RAMPOKS_SCREAM = 1891765738U;
        public static uint RECEIVE_ITEM = 810987758U;
        public static uint ROLLAND_THEME_PLAY = 2500861962U;
        public static uint ROLLAND_THEME_STOP = 3905063988U;
        public static uint RUGGAMSH_ATTACK = 2811450678U;
        public static uint RUGGAMSH_HIT = 1139110397U;
        public static uint RUGGAMSH_INTRO = 3372733872U;
        public static uint RUGGAMSH_SCREAM = 3762665841U;
        public static uint SAVE_BUTTON = 2951792069U;
        public static uint SNOW_STORM_RAHAS_CAVE_CUTSCENE_PLAY = 1681951722U;
        public static uint SNOW_STORM_RAHAS_CAVE_CUTSCENE_STOP = 3127730196U;
        public static uint SORAJUND_ATTACK = 681909904U;
        public static uint SORAJUND_HIT = 4168926147U;
        public static uint SORAJUND_INTRO = 881662642U;
        public static uint SORAJUND_SCREAM = 3440519623U;
        public static uint SPEND_THE_NIGHT = 3954114044U;
        public static uint STOP_DRINGRS_KEEP = 718270513U;
        public static uint STOP_MAIN_MENU = 774860123U;
        public static uint STOP_RAHAS_DESERT = 2584161247U;
        public static uint STOP_TEL_HARAN = 1989506854U;
        public static uint SWORD_SLASH = 1055183530U;
        public static uint TAB_CHANGE = 3967030421U;
        public static uint TEL_HARRAN_MARKET_AMBIENCE_PLAY = 2497999202U;
        public static uint TEL_HARRAN_MARKET_AMBIENCE_STOP = 3099435196U;
        public static uint TIME_HIT_FAIL = 955452739U;
        public static uint TIME_HIT_GOOD = 3391759116U;
        public static uint TIME_HIT_PERFECT = 339515174U;
        public static uint TREASURE = 3284038070U;
        public static uint UNSHEATH_WEAPON_RAMPOKS = 1857344054U;
        public static uint WATERFALL_DRINGRS_KEEP_PLAY = 176670206U;
        public static uint WATERFALL_DRINGRS_KEEP_STOP = 1214563008U;
        public static uint WHITE_SPLASH_CUTSCENE = 3829200125U;
        public static uint WORLD_MAP_BGM_PLAY = 1263912340U;
        public static uint WORLD_MAP_BGM_STOP = 2967815022U;
        public static uint ZOOM_TIME_HIT = 2476419802U;
    } // public class EVENTS

    public class SWITCHES
    {
        public class FOOTSTEP
        {
            public static uint GROUP = 1866025847U;

            public class SWITCH
            {
                public static uint DESERT = 1850388778U;
                public static uint ROCK = 2144363834U;
            } // public class SWITCH
        } // public class FOOTSTEP

    } // public class SWITCHES

    public class GAME_PARAMETERS
    {
        public static uint AMBIENCE = 85412153U;
        public static uint BGM = 412724365U;
        public static uint GLOBAL = 1465331116U;
        public static uint REVERB = 348963605U;
        public static uint SFX = 393239870U;
        public static uint SS_AIR_FEAR = 1351367891U;
        public static uint SS_AIR_FREEFALL = 3002758120U;
        public static uint SS_AIR_FURY = 1029930033U;
        public static uint SS_AIR_MONTH = 2648548617U;
        public static uint SS_AIR_PRESENCE = 3847924954U;
        public static uint SS_AIR_RPM = 822163944U;
        public static uint SS_AIR_SIZE = 3074696722U;
        public static uint SS_AIR_STORM = 3715662592U;
        public static uint SS_AIR_TIMEOFDAY = 3203397129U;
        public static uint SS_AIR_TURBULENCE = 4160247818U;
        public static uint VOICE = 3170124113U;
    } // public class GAME_PARAMETERS

    public class BANKS
    {
        public static uint INIT = 1355168291U;
        public static uint MAIN = 3161908922U;
    } // public class BANKS

    public class BUSSES
    {
        public static uint AMBIENCE = 85412153U;
        public static uint BGM = 412724365U;
        public static uint MASTER_AUDIO_BUS = 3803692087U;
        public static uint MASTER_SECONDARY_BUS = 805203703U;
        public static uint SFX = 393239870U;
        public static uint VOICE = 3170124113U;
    } // public class BUSSES

    public class AUX_BUSSES
    {
        public static uint REVERB_INDOOR = 603224157U;
        public static uint REVERB_OUTDOOR = 1578973140U;
    } // public class AUX_BUSSES

}// public class AK

