﻿using UnityEngine;
using DG.Tweening;
using System.Collections.Generic;

public class WwiseManager : MonoBehaviour {

    #region Singleton Handling

    private static WwiseManager _Instance;

    public static WwiseManager Instance
    {
        get
        {
            if (_Instance == null)
                _Instance = GameObject.FindObjectOfType<WwiseManager>();

            return _Instance;
        }
    }

    #endregion

    public enum BGM
    {
        NONE,
        After_Battle,
        Althea,
        Battle_BGM,
        Boss_Battle_1,
        Boss_Battle_2,
        Cave_Cutscene_BGM,
        Dunabad_Entrance,
        Dringrs_Keep,
        Dunabad_Cave,
        Dunabad_Port,
        Eris_House,
        Finn_Bad_Dream,
        Hero_Kael,
        Kael_kill_Arkwora,
        Kael_Market,
        Main_Menu,
        Rahas_Desert,
        Romantic,
        Sad_Music,
        Shapur,
        Tel_Harran_Front_Arena,
        Tel_Harran_Main,
        Tel_Harran_Prison,
        World_Map,
        Join_Party,
        Misterius_Music_1,
        Lazarus,
        Game_Over,
        Win_Battle,
        Lyon_Battle_Theme,
        Final_Boss_Theme,
        War_Menu_Theme,
        War_Theme,
        Spend_Night,
        Intense_Before_Rampoks,
        Intense_Before_Klahmaran
    }

    public enum UIFX
    {
        Add_Status_Character,
        Heal_Area,
        Baloon,
        Battle_Pointer,
        Exp_Gain,
        Buy,
        Cancel,
        Confirm,
        Cursor_Moving,
        Dialog_confirm,
        EXP_Assigning,
        Item_Use,
        Level_Up,
        Live_2d_Confirm,
        Receive_Item,
        Save_Button,
        Scroll
    }

    public enum ItemGrimoireFX
    {
        Bloodstone,
        Crossbone,

    }

    public enum WorldFX
    {
        NONE,
        Dringrs_Keep_Open_Door,
        Footstep_Dessert,
        Footstep_Dringrs_Keep,
        Footstep_Rock,
        Mpc_Fight_at_Market,
        Treasure,
        Poisoned,
        Join_Party,
        Healing_Point
    }

    public enum CutsceneFX
    {
        Nutt,
        Eris_Channeling,
        Arkwora_Voice,
        Before_Aria_Appear,
        Blacksmith,
        Blood_Splash_Rahas_Cave_Cutscene,
        Clowthers_Sound,
        Clowthers_wing_down,
        Clowthers_wing_up,
        Dream_Geddo_Face_Zoom_In_Cutscene,
        Dringrs_Keep_Ground,
        Earthquake,
        Finn_Cough,
        Finn_Drinking,
        Horor_Scream,
        Kael_Tabrak_Eriss,
        Kapal_ditabrak,
        Layar_Bergetar,
        Magic_Eris_Explotion,
        Munerator_Hit_Finn,
        Punch,
        Rampoks_Hit_Finn_Cutscene,
        Snow_Storm_Rahas_Cave_Cutscene,
        Sword_Slash,
        Unsheath_Weapon_Rampoks,
        White_Splash_Cutscene,
        Geddo_Battle_with_Rampoks,
        Geddo_Slash_by_Rampoks,
        Klahmaran_Fall,
        Klahmaran_Death,
        Aria_Kick_Finn
    }

    public enum BattleFX
    {
        Battle_Transition,
        Battle_transition_fire,
        Battle_transition_wind,
        Blast_Hit,
        Battle_Pointer,
        Guard,
        Time_Hit_Fail,
        Time_Hit_Good,
        Time_Hit_Perfect,
        Zoom,

        //character
        Aria_Aim,
        Aria_Attack,
        Aria_Dead,
        Aria_Jump,
        Aria_Power_Shoot_Arrow_Sound,
        Aria_Power_Shoot_Attack,
        Aria_Power_Shoot_Charging,
        Aria_Power_Shoot_Hit,
        Aria_Scream,

        Eris_Attack,
        Eris_Efficient_Casting,
        Eris_Force_Misille_Cast,
        Eris_Force_Misille_Channeling,
        Eris_Force_Misille_Hit,
        Eris_Force_Misille_Journey,

        Finn_Arc_Slah_Kibasan_Awal,
        Finn_Arc_Slash_Final_Hit,
        Finn_Arc_Slash_Hit,
        Finn_Arc_Slash_Jump,
        Finn_Attack,
        Finn_Dead,
        Finn_Determination,
        Finn_Scream,

        Geddo_Attack,

        //Boss
        Gladiator_Attack,
        Gladiator_Hit,
        Gladiator_Intro,
        Gladiator_Scream,
        Klahmaran_Attack,
        Klahmaran_Dead,
        Klahmaran_Intro,
        Klahmaran_Magic,
        Klahmaran_Scream,
        Arkwora_Attack,
        Arkwora_Casting,
        Arkwora_Channeling,
        Arkwora_Death,
        Arkwora_Skill,
        Rampoks_Attack,

        //Monster Chapter 1
        Flurdiaanum_Attack,
        Flurdiaanum_Hit,
        Flurdiaanum_Intro,
        Flurdiaanum_Scream,
        Borttung_Attack,
        Borttung_Hit,
        Borttung_Intro,
        Borttung_Scream,
        Clowthers_Attack,
        Clowthers_Hit,
        Clowthers_Intro,
        Clowthers_Scream,
        Dorkadhour_Attack,
        Dorkadhour_Hit,
        Dorkadhour_Intro,
        Dorkadhour_Scream,
        Kalkkhukka_Attack,
        Kalkkhukka_Hit,
        Kalkkhukka_Intro,
        Kalkkhukka_Scream,
        Kojvaana_Attack,
        Kojvaana_Hit,
        Kojvaana_Intro,
        Kojvaana_Scream,
        Ruggamsh_Attack,
        Ruggamsh_Hit,
        Ruggamsh_Intro,
        Ruggamsh_Scream,
        Sorajund_Attack,
        Sorajund_Hit,
        Sorajund_Intro,
        Sorajund_Scream
    }

    public enum AmbienceFX
    {
        Bonfire,
        Tel_Harran_Market_Ambience,
        Waterfall_Dringrs_Keep
    }

    public enum SoundType
    {
        BGM,
        SFX,
        Ambience
    }
    private GameObject _GameObject;
    private uint _BGMusicID;

    public BGM CurrentBGM;
    public List<AmbienceFX> CurrentAmbience;
    public bool IsAmbiencePlayed(AmbienceFX ambience)
    {
        return CurrentAmbience.Contains(ambience);
    }

    public bool Ready = false;

    public Dictionary<BGM, uint> BGMDictionary;
    public Dictionary<UIFX, uint> UIFXDictionary;
    public Dictionary<WorldFX, uint> WorldFXDictionary;
    public Dictionary<ItemGrimoireFX, uint> ItemFXDictionary;
    public Dictionary<BattleFX, uint> BattleFXDictionary;
    public Dictionary<CutsceneFX, uint> CutsceneFXDictionary;
    public Dictionary<AmbienceFX, uint> AmbienceFXDictionary;

    void Awake()
    {
        // Future Aldo would see this code sucks

        BGMDictionary = new Dictionary<BGM, uint>();
        for (int i = 0; i < System.Enum.GetValues(typeof(BGM)).Length; i++)
        {
            BGM bgm = (BGM)i;
            BGMDictionary.Add(bgm, SearchBGM(bgm));
        }

        WorldFXDictionary = new Dictionary<WorldFX, uint>();
        for (int i = 0; i < System.Enum.GetValues(typeof(WorldFX)).Length; i++)
        {
            WorldFX fx = (WorldFX)i;
            WorldFXDictionary.Add(fx, SearchWorldFX(fx));
        }

        BattleFXDictionary = new Dictionary<BattleFX, uint>();
        for (int i = 0; i < System.Enum.GetValues(typeof(BattleFX)).Length; i++)
        {
            BattleFX fx = (BattleFX)i;
            BattleFXDictionary.Add(fx, SearchBattleFX(fx));
        }

        UIFXDictionary = new Dictionary<UIFX, uint>();
        for (int i = 0; i < System.Enum.GetValues(typeof(UIFX)).Length; i++)
        {
            UIFX fx = (UIFX)i;
            UIFXDictionary.Add(fx, SearchUIFX(fx));
        }

        ItemFXDictionary = new Dictionary<ItemGrimoireFX, uint>();
        for (int i = 0; i < System.Enum.GetValues(typeof(ItemGrimoireFX)).Length; i++)
        {
            ItemGrimoireFX fx = (ItemGrimoireFX)i;
            ItemFXDictionary.Add(fx, SearchItemFX(fx));
        }

        CutsceneFXDictionary = new Dictionary<CutsceneFX, uint>();
        for (int i = 0; i < System.Enum.GetValues(typeof(CutsceneFX)).Length; i++)
        {
            CutsceneFX fx = (CutsceneFX)i;
            CutsceneFXDictionary.Add(fx, SearchCutsceneSound(fx));
        }

        AmbienceFXDictionary = new Dictionary<AmbienceFX, uint>();
        for (int i = 0; i < System.Enum.GetValues(typeof(AmbienceFX)).Length; i++)
        {
            AmbienceFX fx = (AmbienceFX)i;
            AmbienceFXDictionary.Add(fx, SearchAmbienceSound(fx));
        }

        Ready = true;
        _GameObject = gameObject;
    }

    public float[] RTPVColume = new float[4] {100f,100f,100f,100f};
    public float speed = 2;



    public void SetSoundVolume(SoundType type, float value, float time = 0f)
    {
        float volume = (value / 100f) * GameSetting.Volume[(int)type];
        string typeString = type.ToString();
        if (time == 0f)
        {
            RTPVColume[(int)type] = volume;
            AkSoundEngine.SetRTPCValue(typeString, volume);
        }
        else
        {
            DOTween.To(()=> RTPVColume[(int)type], x=> RTPVColume[(int)type] = x, volume, time).OnUpdate(()=> AkSoundEngine.SetRTPCValue(typeString, RTPVColume[(int)type]));
        }
    }
    
	public void StopAll()
	{
        CurrentAmbience = new List<AmbienceFX>();
		AkSoundEngine.StopAll ();
	}

	#region Background Music
	public void PlayBG(BGM name){
		PlayBG(SearchBGM(name));
		CurrentBGM = name;
	}

	public void PlayBG(uint akEvent){
		AkSoundEngine.StopPlayingID(_BGMusicID, 750, AkCurveInterpolation.AkCurveInterpolation_Exp1);
		
		_BGMusicID = AkSoundEngine.PostEvent(akEvent, _GameObject);
	}

	public void StopBG(){
		AkSoundEngine.StopPlayingID(_BGMusicID, 750, AkCurveInterpolation.AkCurveInterpolation_Exp1);
		CurrentBGM = BGM.NONE;
	}

	public uint SearchBGM(BGM b)
	{

		switch(b)
		{
            case BGM.After_Battle: return AK.EVENTS.PLAY_WINNING_BATTLE;
            case BGM.Althea: return AK.EVENTS.PLAY_TEL_HARAN;
            case BGM.Battle_BGM: return AK.EVENTS.BATTLE_BGM_PLAY;
            case BGM.Boss_Battle_1: return AK.EVENTS.BOSS_BATTLE_1_PLAY;
            case BGM.Boss_Battle_2: return AK.EVENTS.RAMPOKS_BATTLE_PLAY;
            case BGM.Cave_Cutscene_BGM: return AK.EVENTS.INSIDE_RAHAS_CAVE_CUTSCENE_BGM_PLAY;
            case BGM.Dringrs_Keep: return AK.EVENTS.PLAY_DRINGRS_KEEP;
            case BGM.Dunabad_Cave: return AK.EVENTS.PLAY_TEL_HARAN;
            case BGM.Dunabad_Entrance: return AK.EVENTS.PLAY_TEL_HARAN;
            case BGM.Dunabad_Port: return AK.EVENTS.PLAY_TEL_HARAN;
            case BGM.Eris_House: return AK.EVENTS.PLAY_TEL_HARAN;
            case BGM.Final_Boss_Theme: return AK.EVENTS.BOSS_BATTLE_1_PLAY;
            case BGM.Finn_Bad_Dream: return AK.EVENTS.FINN_BAD_DREAM_CUTSCENE_PLAY;
            case BGM.Hero_Kael: return AK.EVENTS.PLAY_TEL_HARAN;
            case BGM.Kael_kill_Arkwora: return AK.EVENTS.PLAY_TEL_HARAN;
            case BGM.Kael_Market: return AK.EVENTS.PLAY_TEL_HARAN;
            case BGM.Lazarus: return AK.EVENTS.INTENSE_BEFORE_BATTLE_WITH_KLAHMARAN;
            case BGM.Lyon_Battle_Theme: return AK.EVENTS.PLAY_TEL_HARAN;
            case BGM.Main_Menu: return AK.EVENTS.PLAY_MAIN_MENU;
            case BGM.Rahas_Desert: return AK.EVENTS.PLAY_RAHAS_DESERT;
            case BGM.Shapur: return AK.EVENTS.PLAY_TEL_HARAN;
            case BGM.Tel_Harran_Front_Arena: return AK.EVENTS.INFRONT_OF_ARENA;
            case BGM.Tel_Harran_Main: return AK.EVENTS.PLAY_TEL_HARAN;
            case BGM.Tel_Harran_Prison: return AK.EVENTS.PLAY_TEL_HARAN;
            case BGM.War_Menu_Theme: return AK.EVENTS.PLAY_TEL_HARAN;
            case BGM.War_Theme: return AK.EVENTS.PLAY_TEL_HARAN;
            case BGM.World_Map: return AK.EVENTS.WORLD_MAP_BGM_PLAY;
            case BGM.Join_Party: return AK.EVENTS.JOIN_PARTY_BGM;
            case BGM.Game_Over: return AK.EVENTS.PLAY_LOSING_BATTLE;
            case BGM.Win_Battle: return AK.EVENTS.PLAY_WINNING_BATTLE;
            case BGM.Spend_Night: return AK.EVENTS.SPEND_THE_NIGHT;
            case BGM.Intense_Before_Rampoks: return AK.EVENTS.INTENSE_BEFORE_RAMPOKS;
            case BGM.Intense_Before_Klahmaran: return AK.EVENTS.INTENSE_BEFORE_BATTLE_WITH_KLAHMARAN;
        }

		return 0;
	}
	#endregion

	#region Sound Effect
	public void PlaySFX(uint akEvent){
        if(akEvent != 0)
		    AkSoundEngine.PostEvent(akEvent, _GameObject);
	}

	public void PlaySFX(uint akEvent, GameObject g){
        if (akEvent != 0)
            AkSoundEngine.PostEvent(akEvent, g);
	}

	public void StopSFX(uint akEvent){
		AkSoundEngine.StopPlayingID(akEvent);
	}



	#region World SFX
	public void PlaySFX(WorldFX f){
		PlaySFX(WorldFXDictionary[f]);
	}
	
	public void PlaySFX(WorldFX f, GameObject g){
		PlaySFX(WorldFXDictionary[f], g);
	}
	
	public void StopSFX(WorldFX f){
		PlaySFX(SearchStopWorldFX(f)) ;
	}

	public uint SearchWorldFX(WorldFX f)
	{
		switch(f)
		{
            case WorldFX.Poisoned: return AK.EVENTS.POISONED;
            case WorldFX.Dringrs_Keep_Open_Door: return AK.EVENTS.DRINGRS_KEEP_OPEN_DOOR;
            case WorldFX.Footstep_Dessert: return AK.EVENTS.FOOTSTEP_DESSERT;
            case WorldFX.Footstep_Dringrs_Keep: return AK.EVENTS.FOOTSTEP_DRINGRS_KEEP;
            case WorldFX.Footstep_Rock: return AK.EVENTS.FOOTSTEP_ROCK;
            case WorldFX.Mpc_Fight_at_Market: return AK.EVENTS.NPC_FIGHT_AT_MARKET;
            case WorldFX.Treasure: return AK.EVENTS.TREASURE;
            case WorldFX.Join_Party: return AK.EVENTS.JOIN_PARTY_BGM;
            case WorldFX.Healing_Point: return AK.EVENTS.HEAL_AREA;
        }
		return 0;
	}
	
	public uint SearchStopWorldFX(WorldFX f)
	{
		return 0;
	}
	#endregion

	#region UI SFX
	public void PlaySFX(UIFX f){
		PlaySFX(UIFXDictionary[f]);
	}
	
	public void PlaySFX(UIFX f, GameObject g){
		PlaySFX(UIFXDictionary[f], g);
	}
	
	public void StopSFX(UIFX f){
		PlaySFX(SearchStopUIFX(f)) ;
	}

	public uint SearchUIFX(UIFX f)
	{
		switch(f)
		{
		    case UIFX.Confirm :  return AK.EVENTS.CONFIRM ;
		    case UIFX.Cancel : return AK.EVENTS.CANCEL ;
		    case UIFX.Cursor_Moving : return AK.EVENTS.CURSOR_MOVING ;
		    case UIFX.Item_Use : return AK.EVENTS.ITEM_USE ;
            case UIFX.Battle_Pointer : return AK.EVENTS.BATTLE_POINTER;
            case UIFX.Dialog_confirm: return AK.EVENTS.DIALOG_CONFIRM;
            case UIFX.Heal_Area: return AK.EVENTS.HEAL_AREA;
            case UIFX.Receive_Item: return AK.EVENTS.RECEIVE_ITEM;
            case UIFX.Save_Button: return AK.EVENTS.SAVE_BUTTON;
            case UIFX.Add_Status_Character: return AK.EVENTS.ADD_STATUS_CHARACTER;
            case UIFX.Buy: return AK.EVENTS.BUY;
            case UIFX.Baloon: return AK.EVENTS.BALOON;
            case UIFX.Live_2d_Confirm: return AK.EVENTS.LIVE_2D_CONFIRM;
            case UIFX.Exp_Gain: return AK.EVENTS.EXP_GAIN;
            case UIFX.Level_Up: return AK.EVENTS.LEVEL_UP;
        }
		return 0;
	}

	public uint SearchStopUIFX(UIFX f)
	{
//		switch(f)
//		{
//			case UIFX.CANCEL : return AK.EVENTS.CANCEL_BATTLE_STOP ; break;
//			case UIFX.CONFIRM_BATTLE : return AK.EVENTS.CONFIRM_BATTLE_STOP ; break;
//		}
		return 0;
	}
	#endregion

	#region Battle SFX
	public void PlaySFX(BattleFX f){
		PlaySFX(BattleFXDictionary[f]);
	}
	
	public void PlaySFX(BattleFX f, GameObject g){
		PlaySFX(BattleFXDictionary[f], g);
	}
	
	public void StopSFX(BattleFX f){
		//		PlaySFX(SearchStopUIFX(f)) ;
	}

	public uint SearchBattleFX(BattleFX f)
	{
		switch(f)
		{
            case BattleFX.Aria_Aim: return AK.EVENTS.ARIA_AIM; break;
            case BattleFX.Aria_Attack: return AK.EVENTS.ARIA_ATTACK; break;
            case BattleFX.Aria_Dead: return AK.EVENTS.ARIA_DEAD; break;
            case BattleFX.Aria_Jump: return AK.EVENTS.ARIA_JUMP; break;
            case BattleFX.Aria_Power_Shoot_Arrow_Sound: return AK.EVENTS.ARIA_POWER_SHOOT_ARROW_SOUND; break;
            case BattleFX.Aria_Power_Shoot_Attack: return AK.EVENTS.ARIA_POWER_SHOOT_ATTACK; break;
            case BattleFX.Aria_Power_Shoot_Charging: return AK.EVENTS.ARIA_POWER_SHOOT_CHARGING; break;
            case BattleFX.Aria_Power_Shoot_Hit: return AK.EVENTS.ARIA_POWER_SHOOT_HIT; break;
            case BattleFX.Aria_Scream: return AK.EVENTS.ARIA_SCREAM; break;

            case BattleFX.Eris_Attack: return AK.EVENTS.ERIS_ATTACK; break;
            case BattleFX.Eris_Efficient_Casting: return AK.EVENTS.ERIS_EFISIENT_CASTING; break;
            case BattleFX.Eris_Force_Misille_Cast: return AK.EVENTS.ERIS_FORCE_MISILLE_CAST; break;
            case BattleFX.Eris_Force_Misille_Channeling: return AK.EVENTS.ERIS_FORCE_MISILLE_CHANNELING; break;
            case BattleFX.Eris_Force_Misille_Hit: return AK.EVENTS.ERIS_FORCE_MISILLE_HIT; break;
            case BattleFX.Eris_Force_Misille_Journey: return AK.EVENTS.ERIS_FORCE_MISILLE_JOURNEY; break;

            case BattleFX.Finn_Arc_Slah_Kibasan_Awal: return AK.EVENTS.FINN_ARC_SLAH_KIBASAN_AWAL; break;
            case BattleFX.Finn_Arc_Slash_Final_Hit: return AK.EVENTS.FINN_ARC_SLASH_FINAL_HIT; break;
            case BattleFX.Finn_Arc_Slash_Hit: return AK.EVENTS.FINN_ARC_SLASH_HIT; break;
            case BattleFX.Finn_Arc_Slash_Jump: return AK.EVENTS.FINN_ARC_SLASH_JUMP; break;
            case BattleFX.Finn_Attack: return AK.EVENTS.FINN_ATTACK; break;
            case BattleFX.Finn_Dead: return AK.EVENTS.FINN_DEAD; break;
            case BattleFX.Finn_Determination: return AK.EVENTS.FINN_DEAD; break;
            case BattleFX.Finn_Scream: return AK.EVENTS.FINN_SCREAM; break;

            case BattleFX.Geddo_Attack: return AK.EVENTS.GEDDO_ATTACK; break;
                
            case BattleFX.Gladiator_Attack: return AK.EVENTS.GLADIATOR_ATTACK; break;
            case BattleFX.Gladiator_Hit: return AK.EVENTS.GLADIATOR_HIT; break;
            case BattleFX.Gladiator_Intro: return AK.EVENTS.GLADIATOR_INTRO; break;
            case BattleFX.Gladiator_Scream: return AK.EVENTS.GLADIATOR_SCREAM; break;

            case BattleFX.Klahmaran_Attack: return AK.EVENTS.KLAHMARAN_ATTACK; break;
            case BattleFX.Klahmaran_Dead: return AK.EVENTS.KLAHMARAN_DEAD; break;
            case BattleFX.Klahmaran_Intro: return AK.EVENTS.KLAHMARAN_INTRO; break;
            case BattleFX.Klahmaran_Magic: return AK.EVENTS.KLAHMARAN_MAGIC; break;
            case BattleFX.Klahmaran_Scream: return AK.EVENTS.KLAHMARAN_SCREAM; break;
                
            case BattleFX.Rampoks_Attack: return AK.EVENTS.RAMPOKS_ATTACK; break;
                
            case BattleFX.Borttung_Attack: return AK.EVENTS.BORTTHUNG_ATTACK; break;
            case BattleFX.Borttung_Scream: return AK.EVENTS.BORTTHUNG_SCREAM; break;
            case BattleFX.Borttung_Hit: return AK.EVENTS.BORTTHUNG_HIT; break;
            case BattleFX.Borttung_Intro: return AK.EVENTS.BORTTHUNG_INTRO; break;
                
            case BattleFX.Clowthers_Attack: return AK.EVENTS.CLOWTHERS_ATTACK; break;
            case BattleFX.Clowthers_Hit: return AK.EVENTS.CLOWTHERS_HIT; break;
            case BattleFX.Clowthers_Intro: return AK.EVENTS.CLOWTHERS_INTRO; break;
            case BattleFX.Clowthers_Scream: return AK.EVENTS.CLOWTHERS_SCREAM; break;
                
            case BattleFX.Dorkadhour_Attack: return AK.EVENTS.DORKADHOUR_ATTACK; break;
            case BattleFX.Dorkadhour_Hit: return AK.EVENTS.DORKADHOUR_HIT; break;
            case BattleFX.Dorkadhour_Intro: return AK.EVENTS.DORKADHOUR_HIT; break;
            case BattleFX.Dorkadhour_Scream: return AK.EVENTS.DORKADHOUR_SCREAM; break;
                
            case BattleFX.Flurdiaanum_Attack: return AK.EVENTS.FLURDIAANUM_ATTACK; break;
            case BattleFX.Flurdiaanum_Hit: return AK.EVENTS.FLURDIAANUM_HIT; break;
            case BattleFX.Flurdiaanum_Intro: return AK.EVENTS.FLURDIAANUM_INTRO; break;
            case BattleFX.Flurdiaanum_Scream: return AK.EVENTS.FLURDIANUM_SCREAM; break;
                
            case BattleFX.Kojvaana_Attack: return AK.EVENTS.KOJVAANA_ATTACK; break;
            case BattleFX.Kojvaana_Hit: return AK.EVENTS.KOJVAANA_HIT; break;
            case BattleFX.Kojvaana_Intro: return AK.EVENTS.KOJVAANA_INTRO; break;
            case BattleFX.Kojvaana_Scream: return AK.EVENTS.KOJVAANA_SCREAM; break;

            case BattleFX.Sorajund_Attack: return AK.EVENTS.SORAJUND_ATTACK; break;
            case BattleFX.Sorajund_Intro: return AK.EVENTS.SORAJUND_INTRO; break;
            case BattleFX.Sorajund_Hit: return AK.EVENTS.SORAJUND_HIT; break;
            case BattleFX.Sorajund_Scream: return AK.EVENTS.SORAJUND_SCREAM; break;
                
            case BattleFX.Ruggamsh_Attack: return AK.EVENTS.SORAJUND_ATTACK; break;
            case BattleFX.Ruggamsh_Hit: return AK.EVENTS.SORAJUND_HIT; break;
            case BattleFX.Ruggamsh_Intro: return AK.EVENTS.SORAJUND_INTRO; break;
            case BattleFX.Ruggamsh_Scream: return AK.EVENTS.SORAJUND_SCREAM; break;

            case BattleFX.Guard: return AK.EVENTS.GUARD; break;
            case BattleFX.Zoom: return AK.EVENTS.ZOOM_TIME_HIT; break;
            case BattleFX.Battle_Pointer: return AK.EVENTS.BATTLE_POINTER; break;
            case BattleFX.Blast_Hit: return AK.EVENTS.BLAST_HIT; break;
            case BattleFX.Time_Hit_Perfect: return AK.EVENTS.TIME_HIT_PERFECT; break;
            case BattleFX.Time_Hit_Good: return AK.EVENTS.TIME_HIT_GOOD; break;
            case BattleFX.Time_Hit_Fail: return AK.EVENTS.TIME_HIT_FAIL; break;
            case BattleFX.Battle_transition_wind: return AK.EVENTS.BATTLE_TRANSITION_WIND;
            case BattleFX.Battle_transition_fire: return AK.EVENTS.BATTLE_TRANSITION_FIRE;
        }
        return 0;
	}
	
	public uint SearchStopBattleFX(BattleFX f)
	{
//		switch(f)
//		{
//			
//		}
		return 0;
	}
	#endregion

	#region ITEM GRIMOIRE SFX
	public void PlaySFX(ItemGrimoireFX f){
		PlaySFX(ItemFXDictionary[f]);
	}
	
	public void PlaySFX(ItemGrimoireFX f, GameObject g){
		PlaySFX(ItemFXDictionary[f], g);
	}
	
	public void StopSFX(ItemGrimoireFX f){
		//		PlaySFX(SearchStopUIFX(f)) ;
	}

	public uint SearchItemFX(ItemGrimoireFX f)
	{
		switch(f)
		{
		case ItemGrimoireFX.Bloodstone :  return AK.EVENTS.BLOODSTONE;
		case ItemGrimoireFX.Crossbone : return AK.EVENTS.CROSSBONE;
		}
		return 0;
	}
	
	public uint SearchStopUIFX(ItemGrimoireFX f)
	{
		return 0;
	}
	#endregion

	#region Cutscene SOUND
	public void PlaySFX(CutsceneFX f){
        if(CutsceneFXDictionary.ContainsKey(f))
		    PlaySFX(CutsceneFXDictionary[f]);
	}
	
	public void PlaySFX(CutsceneFX f, GameObject g){
        if (CutsceneFXDictionary.ContainsKey(f))
            PlaySFX(CutsceneFXDictionary[f], g);
	}
	
	public void StopSFX(CutsceneFX f){
        PlaySFX(SearchStopCutsceneSound(f)) ;
	}
	
	public uint SearchCutsceneSound(CutsceneFX f)
	{
		switch(f)
        {
            case CutsceneFX.Arkwora_Voice: return AK.EVENTS.ARKWORA_VOICE;
            case CutsceneFX.Before_Aria_Appear: return AK.EVENTS.BEFORE_ARIA_APPEAR;
            case CutsceneFX.Blacksmith: return AK.EVENTS.BLACKSMITH;
            case CutsceneFX.Blood_Splash_Rahas_Cave_Cutscene: return AK.EVENTS.BLOOD_SPLASH_RAHAS_CAVE_CUTSCENE;
            case CutsceneFX.Clowthers_Sound: return AK.EVENTS.CLOWTHERS_SOUND;
            case CutsceneFX.Clowthers_wing_down: return AK.EVENTS.CLOWTHERS_WING_DOWN;
            case CutsceneFX.Clowthers_wing_up: return AK.EVENTS.CLOWTHERS_WING_UP;
            case CutsceneFX.Dream_Geddo_Face_Zoom_In_Cutscene: return AK.EVENTS.DREAM_GEDDO_FACE_ZOOM_IN_CUTSCENE;
            case CutsceneFX.Dringrs_Keep_Ground: return AK.EVENTS.DRINGRS_KEEP_GROUND;
            case CutsceneFX.Earthquake: return AK.EVENTS.EARTHQUAKE;
            case CutsceneFX.Finn_Cough: return AK.EVENTS.FINN_COUGH;
            case CutsceneFX.Finn_Drinking: return AK.EVENTS.FINN_DRINKING;
            case CutsceneFX.Horor_Scream: return AK.EVENTS.HOROR_SCREAM_DRINGRS_KEEP_ENTRANCE;
            case CutsceneFX.Magic_Eris_Explotion: return AK.EVENTS.MAGIC_ERIS_EXPLOTION;
            case CutsceneFX.Munerator_Hit_Finn: return AK.EVENTS.MUNERATOR_HIT_FINN;
            case CutsceneFX.Nutt: return AK.EVENTS.NUTT;
            case CutsceneFX.Rampoks_Hit_Finn_Cutscene: return AK.EVENTS.RAMPOKS_HIT_FINN_CUTSCENE;
            case CutsceneFX.Snow_Storm_Rahas_Cave_Cutscene: return AK.EVENTS.SNOW_STORM_RAHAS_CAVE_CUTSCENE_PLAY;
            case CutsceneFX.Sword_Slash: return AK.EVENTS.SWORD_SLASH;
            case CutsceneFX.Unsheath_Weapon_Rampoks: return AK.EVENTS.UNSHEATH_WEAPON_RAMPOKS;
            case CutsceneFX.White_Splash_Cutscene: return AK.EVENTS.WHITE_SPLASH_CUTSCENE;
            case CutsceneFX.Geddo_Battle_with_Rampoks: return AK.EVENTS.GEDDO_BATTLE_WITH_RAMPOKS_PLAY;
            case CutsceneFX.Geddo_Slash_by_Rampoks: return AK.EVENTS.GEDDO_SLASH_BY_RAMPOKS;
            case CutsceneFX.Klahmaran_Fall: return AK.EVENTS.KLAHMARAN_FALL_BEFORE_DEATH;
            case CutsceneFX.Klahmaran_Death: return AK.EVENTS.KLAHMARAN_DEATH_SCREAM;
            //case CutsceneFX.Aria_Kick_Finn: return AK.EVENTS.ARIA_KICK_FINN;
        }
		return 0;
	}
	
	public uint SearchStopCutsceneSound(CutsceneFX f)
	{
        switch (f)
        {
            case CutsceneFX.Snow_Storm_Rahas_Cave_Cutscene: return AK.EVENTS.SNOW_STORM_RAHAS_CAVE_CUTSCENE_STOP;
            case CutsceneFX.Geddo_Battle_with_Rampoks: return AK.EVENTS.GEDDO_BATTLE_WITH_RAMPOKS_STOP;
        }
        return 0;
	}
    #endregion


    #region SetAmbienceList
    private List<AmbienceFX> _AmbienceList = new List<AmbienceFX>();
    public void AmbienceAdd(AmbienceFX sfx)
    {
        if(!_AmbienceList.Exists(element => element == sfx))
        {
            PlaySFX(sfx);
            _AmbienceList.Add(sfx);
        }
    }
    public void AmbienceRemove(AmbienceFX sfx)
    {
        if (_AmbienceList.Exists(element => element == sfx))
        {
            StopSFX(sfx);
            _AmbienceList.Remove(sfx);
        }
    }
    #endregion

    #region Ambience SOUND
    public void PlaySFX(AmbienceFX f){
        if (!IsAmbiencePlayed(f))
        {
            PlaySFX(AmbienceFXDictionary[f]);
            CurrentAmbience.Add(f);
        }
	}
	
	public void PlaySFX(AmbienceFX f, GameObject g){
		PlaySFX(AmbienceFXDictionary[f], g);
	}
	
	public void StopSFX(AmbienceFX f)
    {
        CurrentAmbience.Remove(f);
        PlaySFX(SearchAmbienceSoundStopper(f));
	}
	
	public uint SearchAmbienceSound(AmbienceFX f)
	{
		switch(f)
		{
		    case AmbienceFX.Bonfire :  return AK.EVENTS.BONFIRE_RAHAS_CAVE_CUTSCENE_PLAY;
            case AmbienceFX.Tel_Harran_Market_Ambience: return AK.EVENTS.TEL_HARRAN_MARKET_AMBIENCE_PLAY;
            case AmbienceFX.Waterfall_Dringrs_Keep: return AK.EVENTS.WATERFALL_DRINGRS_KEEP_PLAY;
        }
		return 0;
	}

	public uint SearchAmbienceSoundStopper(AmbienceFX f)
	{
		switch(f)
		{
            case AmbienceFX.Bonfire: return AK.EVENTS.BONFIRE_RAHAS_CAVE_CUTSCENE_STOP;
            case AmbienceFX.Tel_Harran_Market_Ambience: return AK.EVENTS.TEL_HARRAN_MARKET_AMBIENCE_STOP;
            case AmbienceFX.Waterfall_Dringrs_Keep: return AK.EVENTS.WATERFALL_DRINGRS_KEEP_STOP;
        }
		return 0;
	}
	#endregion

#endregion
}
