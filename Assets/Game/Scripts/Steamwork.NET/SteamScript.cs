﻿using UnityEngine;
using System.Collections;
using UnityEngine.Analytics;
using Steamworks;

public class SteamScript : MonoBehaviour {
    void Awake()
    {
        if (SteamManager.Initialized)
            Analytics.SetUserId(SteamUser.GetSteamID().ToString());
    }
}
