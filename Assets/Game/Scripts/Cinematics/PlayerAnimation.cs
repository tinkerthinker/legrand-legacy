﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[RequireComponent(typeof(Animator))]
[ExecuteInEditMode]
public class PlayerAnimation : MonoBehaviour {

    //    public enum AnimationType
    //    {
    //        IDLE, WALK, RUN,
    //    };
    //    
    //    [System.Serializable]
    //    public class Animation
    //    {
    //        public AnimationType Type;
    //    };
    //    public Animation[] PlayerAnimations;
    //    public AnimationType CurrentAnimation;
    public string ActorID;

	public string Idle; 
	public string Walk; 
	public string Run; 
	public string Dead;
	public string Revive;

    private Animator _Animator;
    
	public string StartAnimation;
	public bool RandomStart;
    // Use this for initialization
    void Awake () 
    {
        _Animator = GetComponent<Animator>();
    }
    
    void OnDestroy()
    {
        DisableListener();
    }
    
    void OnEnable()
    {
        EnableListener();

        if (!LegrandUtility.CompareString(StartAnimation, ""))
            PlayAnimation(StartAnimation, false, RandomStart);
    }

    void OnDisable()
    {
        DisableListener();
    }

    void EnableListener()
    {
        if(EventManager.Instance)
        EventManager.Instance.AddListener<PlayAnimationEvent>(PlayAnimationHandler);
    }
    
    void DisableListener()
    {
        if (EventManager.Instance)
            EventManager.Instance.RemoveListener<PlayAnimationEvent>(PlayAnimationHandler);
    }

    void PlayAnimationHandler(PlayAnimationEvent e)
    {
        if (this.ActorID == e.ActorID)
            PlayAnimation(e.Animation, e.Crossfade, e.Layer);
    }
    #region Animation Functions
    
	public void PlayAnimation(string animationName, bool crossfade, bool random = false)
	{
		if (_Animator != null)
		{
			int hashID = Animator.StringToHash(animationName);
			if (!crossfade) {
				if (random)
					_Animator.Play (hashID, -1, LegrandUtility.Random(0.0f, 1.0f));
				else
					_Animator.Play (hashID);
			}
			else
				_Animator.CrossFade(animationName, 0.25f);
		}
	}

    public void PlayAnimation(string animationName, bool crossfade, int layer, bool random = false)
    {
        if (_Animator != null)
        {
            int hashID = Animator.StringToHash(animationName);
            if (!crossfade)
            {
                if (random)
                    _Animator.Play(hashID, layer, LegrandUtility.Random(0.0f, 1.0f));
                else
                    _Animator.Play(hashID, layer);
            }
            else
                _Animator.CrossFade(animationName, 0.25f, layer);
        }
    }

    //    public bool PlayAnimation(AnimationType type)
    //    {
    //        if (_Animator != null)
    //        {
    //            string animationName = PlayerAnimations.Where(data => data.Type == type).First().Type.ToString();
    //
    //            int hashID = Animator.StringToHash(animationName);
    //            _Animator.Play(hashID);
    //
    //            // Done
    //            CurrentAnimation = type;
    //            
    //            return true;
    //        }
    //        
    //        return false;
    //    }

    #endregion

    #region Implementation

    void PlayIdleAnimation()
    {
		PlayAnimation(Idle, true);
    }

	void PlayIdleAnimationImmediately()
	{
		PlayAnimation(Idle, false);
	}

    void PlayWalkAnimation()
    {
		PlayAnimation(Walk, true);
    }
    
    void PlayRunAnimation()
    {
		PlayAnimation(Run, true);
    }
    
	void PlayDeadAnimation()
	{
		PlayAnimation(Dead, true);
	}

	void PlayReviveAnimation()
	{
		PlayAnimation(Revive, true);
	}
    #endregion
}
