﻿using UnityEngine;
using System.Collections;
using RenderHeads.Media.AVProVideo;
using System;

class VideoEventListener : MonoBehaviour {
    public VideoEvent[] Events;

    public void OnVideoEvent(MediaPlayer mp, MediaPlayerEvent.EventType et, RenderHeads.Media.AVProVideo.ErrorCode errorCode)
    {
        switch (et) {
            case MediaPlayerEvent.EventType.FinishedPlaying:
                for(int i=0;i< Events.Length;i++)
                {
                    if (Events[i].EventType == et) Events[i].EventFunction.FireEvent();
                }
                break;
        }
    }
}

[System.Serializable]
class VideoEvent
{
    public MediaPlayerEvent.EventType EventType; 
    public InspectorCutsceneEvent EventFunction;
}
