﻿using UnityEngine;
using System.Collections;
using RenderHeads.Media.AVProVideo;
using TeamUtility.IO;
using DG.Tweening;

public class SkipHandler : MonoBehaviour {
    public MediaPlayer mp;
	public int SkipToFrame;
    public CanvasGroup Canvas;
    public float SkipWaitTime = 3f;
    private int State;

	private bool _IsSkip = false;
    private IEnumerator _Enumerator;

	IEnumerator Resume()
	{
		yield return new WaitForSeconds(0.1f);
		while (mp.Control.GetCurrentTimeMs() < mp.Info.GetDurationMs()) 
		{
			yield return new WaitForEndOfFrame ();
            mp.Control.Seek(mp.Info.GetDurationMs());
		}
	}

	void Skip()
	{
		if (mp.Control.GetCurrentTimeMs() > mp.Info.GetDurationMs())
			return;

		_IsSkip = true;
        Canvas.alpha = 0;

        StartCoroutine(SkipCoroutine());
        StartCoroutine (Resume ());
	}

    IEnumerator SkipCoroutine()
    {
        while (mp.Control.GetCurrentTimeMs() < mp.Info.GetDurationMs())
        {
            mp.Control.Seek(mp.Info.GetDurationMs());
            yield return null;
        }
    }

    IEnumerator DisplaySkip()
    {
        yield return new WaitForSeconds(SkipWaitTime);
    }

    void HideCanvas()
    {
        Canvas.DOFade(0f, 0.5f);
    }

    void Update()
    {
        if (State == 0 && !InputManager.GetButtonDown("Confirm"))
            return;

        if(State == 0 && InputManager.GetButtonDown("Confirm") && !GlobalGameStatus.Instance.StateStatus[(int)GlobalGameStatus.GameState.Pause])
        {
            State = 1;
            Canvas.DOFade(1f, 0.5f);
            if (_Enumerator != null) StopCoroutine(_Enumerator);
            _Enumerator = DisplaySkip();
            StartCoroutine(_Enumerator);
            return;
        }

        if(State == 1 && InputManager.GetButtonDown("Confirm") && !GlobalGameStatus.Instance.StateStatus[(int)GlobalGameStatus.GameState.Pause])
        {
            if (_Enumerator != null) StopCoroutine(_Enumerator);
            HideCanvas();
            Skip();
            State = 2;
        }
        else if (State == 1 && InputManager.GetButtonDown("Cancel") && !GlobalGameStatus.Instance.StateStatus[(int)GlobalGameStatus.GameState.Pause])
        {
            if (_Enumerator != null) StopCoroutine(_Enumerator);
            HideCanvas();
            State = 0;
        }

    }
}
