﻿using UnityEngine;
using System.Collections;
using WellFired;

public class SequencePlayer : MonoBehaviour {
    public GameObject Sequencer;
    public float delay = 1f;

	public bool ActivatePartyLeader = false;
	public bool ActivateCurrentBG = false;

	public string[] ActivateObjectPathsAtStart;

    private USSequencer SequencerScript;



    void Start()
    {
		foreach (string objectPath in ActivateObjectPathsAtStart)
			GameObject.Find(objectPath).SetActive (true);

		if (ActivatePartyLeader)
			AreaController.Instance.CurrPlayer.SetActive (true);
		if (ActivateCurrentBG)
			AreaController.Instance.CurrPrefab.SetActive (true);

        GameObject SequencerGameObject = (GameObject)Instantiate(Sequencer);
        SequencerScript = SequencerGameObject.GetComponent<USSequencer>();
        StartCoroutine(PlayScene(SequencerScript));
    }

    IEnumerator PlayScene(USSequencer SequenceToPlay)
    {
        yield return new WaitForSeconds(delay);
        SequenceToPlay.Play();
    }
}
