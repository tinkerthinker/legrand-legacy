﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using WellFired;

[USequencerFriendlyName("Stop World SFX")]
[USequencerEvent("LegrandCinematic/Sound/Stop World SFX")]
public class StopWorldSFX : USEventBase {
	public List<WwiseManager.WorldFX> SFXName;
	
	public override void FireEvent()
	{
		foreach(WwiseManager.WorldFX soundName in SFXName)
			WwiseManager.Instance.StopSFX (soundName);;
	}
	
	public override void ProcessEvent(float deltaTime)
	{
		
	}
}
