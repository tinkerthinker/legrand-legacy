﻿using UnityEngine;
using System.Collections;

public class PlayAmbienceSFX : MonoBehaviour {
    public WwiseManager.AmbienceFX SFXName;

    public void FireEvent()
    {
        WwiseManager.Instance.PlaySFX(SFXName);
    }
}
