﻿using UnityEngine;
using System.Collections;
using Flux;

[FEvent("Sound/World SFX")]
public class fWorldSFX : FEvent
{
    public WwiseManager.WorldFX[] SFX;
    public bool IsStop;

    protected override void OnTrigger(float timeSinceTrigger)
    {
        base.OnTrigger(timeSinceTrigger);
        if (!IsStop)
        {
            for (int i = 0; i < SFX.Length; i++)
                WwiseManager.Instance.PlaySFX(SFX[i]);
            
        }
        else
        {
            for (int i = 0; i < SFX.Length; i++)
                WwiseManager.Instance.StopSFX(SFX[i]);
        }
    }
}
