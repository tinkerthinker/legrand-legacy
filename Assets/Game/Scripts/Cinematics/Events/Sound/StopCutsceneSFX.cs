﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using WellFired;

[USequencerFriendlyName("Stop Cutscene SFX")]
[USequencerEvent("LegrandCinematic/Sound/Stop Cutscene SFX")]
public class StopCutsceneSFX : USEventBase {
	public List<WwiseManager.CutsceneFX> SFXName;
	
	public override void FireEvent()
	{
		foreach(WwiseManager.CutsceneFX soundName in SFXName)
			WwiseManager.Instance.StopSFX (soundName);;
	}
	
	public override void ProcessEvent(float deltaTime)
	{
		
	}
}
