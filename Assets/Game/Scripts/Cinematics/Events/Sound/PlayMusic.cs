﻿using UnityEngine;
using System.Collections;
using WellFired;

public class PlayMusicFlux : MonoBehaviour{
	public WwiseManager.BGM BGM_name;

	public void FireEvent()
	{
		WwiseManager.Instance.PlayBG (BGM_name);
	}
}
