﻿using UnityEngine;
using System.Collections;
using WellFired;

[USequencerFriendlyName("Sound World Effect")]
[USequencerEvent("LegrandCinematic/Play World SFX")]
public class PlayWorldSFX : USEventBase {
	public WwiseManager.WorldFX SFXName;

	public override void FireEvent()
	{
		WwiseManager.Instance.PlaySFX (SFXName);
		//SoundManager.PlaySoundEffectOneShot (SFXname, loop);
	}
	
	public override void ProcessEvent(float deltaTime)
	{
		
	}
}
