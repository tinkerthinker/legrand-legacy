﻿using UnityEngine;
using System.Collections;

public class PlayItemSFX : MonoBehaviour {
	public WwiseManager.ItemGrimoireFX SFXName;
    public bool OnStart = false;

    void Start()
    {
        if (OnStart)
            WwiseManager.Instance.PlaySFX(SFXName);
    }

	public void FireEvent()
	{
		WwiseManager.Instance.PlaySFX (SFXName);
	}
}
