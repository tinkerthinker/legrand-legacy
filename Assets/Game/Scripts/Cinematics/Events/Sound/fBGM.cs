﻿using UnityEngine;
using System.Collections;
using Flux;

[FEvent("Sound/BGM")]
public class fBGM : FEvent
{
    public WwiseManager.BGM BGM_name;
    public bool IsStop;

    protected override void OnTrigger(float timeSinceTrigger)
    {
        base.OnTrigger(timeSinceTrigger);
        if(!IsStop)
            WwiseManager.Instance.PlayBG(BGM_name);
        else
            WwiseManager.Instance.StopBG();
    }
}
