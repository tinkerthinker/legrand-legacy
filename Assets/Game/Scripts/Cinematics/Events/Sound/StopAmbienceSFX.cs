﻿using UnityEngine;
using System.Collections;

public class StopAmbienceSFX : MonoBehaviour {
    public WwiseManager.AmbienceFX SFXName;

    public void FireEvent()
    {
        WwiseManager.Instance.StopSFX(SFXName);
    }
}
