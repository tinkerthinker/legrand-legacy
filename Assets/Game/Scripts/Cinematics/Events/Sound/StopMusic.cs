﻿using UnityEngine;
using System.Collections;
using WellFired;

[USequencerFriendlyName("MusicStop")]
[USequencerEvent("LegrandCinematic/StopMusic")]
public class StopMusic : USEventBase {
	public bool immediately = true;
	
	public override void FireEvent()
	{
		WwiseManager.Instance.StopBG ();
//		if (!immediately)
//			SoundManager.StopBackgroundMusic (true);
//		else
//			SoundManager.StopBackgroundMusicImmediately (true);
	}
	
	public override void ProcessEvent(float deltaTime)
	{
		
	}
}
