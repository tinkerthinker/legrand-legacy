﻿using UnityEngine;
using System.Collections;
using WellFired;

[USequencerFriendlyName("Music")]
[USequencerEvent("LegrandCinematic/PlayMusic")]
public class PlayMusic : USEventBase {
	public WwiseManager.BGM BGM_name;
	public bool loop = true;

	public override void FireEvent()
	{
		WwiseManager.Instance.PlayBG (BGM_name);
	}
	
	public override void ProcessEvent(float deltaTime)
	{
		
	}
}
