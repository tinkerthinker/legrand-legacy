﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

namespace Flux
{
    [FEvent("Dialogue/Start Dialogue")]
    public class StartDialogue : FEvent
    {
        public string DialogueName = "";
        public bool PauseSequence = true;
        private FSequence sequenceToResume = null;
        
        protected override void OnTrigger(float timeSinceTrigger)
        {
            if(string.IsNullOrEmpty(DialogueName)) DialogFinished(null);
            int dialogueID = LegrandUtility.GetDialogueID(DialogueName);

            if(PauseSequence) sequenceToResume.Pause();

            if (dialogueID >= 0)
            {
                LegrandUtility.StartDialogue(dialogueID);
                EventManager.Instance.AddListener<DialogWindowClosed>(DialogFinished);
                EventManager.Instance.AddListener<ResumeSequenceEvent>(DialogPaused);
            }
            else DialogFinished(null);
        }

        protected override void OnInit()
        {
            sequenceToResume = Owner.GetComponent<FSequence>();
        }

        private void DialogFinished(DialogWindowClosed e)
        {
            EventManager.Instance.RemoveListener<DialogWindowClosed>(DialogFinished);
            EventManager.Instance.RemoveListener<ResumeSequenceEvent>(DialogPaused);

            StartCoroutine(WaitUntilPause());
        }

        private void DialogPaused(ResumeSequenceEvent e)
        {
            EventManager.Instance.RemoveListener<DialogWindowClosed>(DialogFinished);
            EventManager.Instance.RemoveListener<ResumeSequenceEvent>(DialogPaused);

            StartCoroutine(WaitUntilPause());
        }

        IEnumerator WaitUntilPause()
        {
            while (sequenceToResume.IsPlaying) yield return null;
            sequenceToResume.Resume();
        }

        void OnDestroy()
        {
            if (EventManager.Instance)
            {
                EventManager.Instance.RemoveListener<DialogWindowClosed>(DialogFinished);
                EventManager.Instance.RemoveListener<ResumeSequenceEvent>(DialogPaused);
            }
        }
    }
}