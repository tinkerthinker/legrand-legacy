﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

namespace Flux
{
    [FEvent("Dialogue/Resume Dialogue")]
    public class ResumeSequenceDialogue : FEvent
    {
        public bool PauseSequence = true;
        private FSequence sequenceToResume = null;
        
        protected override void OnTrigger(float timeSinceTrigger)
        {
            base.OnTrigger(timeSinceTrigger);
            if(PauseSequence) sequenceToResume.Pause();

            Dialoguer.SetGlobalBoolean(5, true);
            EventManager.Instance.AddListener<DialogWindowClosed>(DialogFinished);
            EventManager.Instance.AddListener<ResumeSequenceEvent>(DialogPaused);
        }

        protected override void OnInit()
        {
            base.OnInit();
            sequenceToResume = Owner.GetComponent<FSequence>();
        }

        private void DialogFinished(DialogWindowClosed e)
        {
            EventManager.Instance.RemoveListener<DialogWindowClosed>(DialogFinished);
            EventManager.Instance.RemoveListener<ResumeSequenceEvent>(DialogPaused);

            StartCoroutine(WaitUntilPause());
        }

        private void DialogPaused(ResumeSequenceEvent e)
        {
            EventManager.Instance.RemoveListener<DialogWindowClosed>(DialogFinished);
            EventManager.Instance.RemoveListener<ResumeSequenceEvent>(DialogPaused);

            StartCoroutine(WaitUntilPause());
        }

        IEnumerator WaitUntilPause()
        {
            while (sequenceToResume.IsPlaying) yield return null;
            sequenceToResume.Resume();
        }

        void OnDestroy()
        {
            if (EventManager.Instance)
            {
                EventManager.Instance.RemoveListener<DialogWindowClosed>(DialogFinished);
                EventManager.Instance.RemoveListener<ResumeSequenceEvent>(DialogPaused);
            }
        }
    }
}