﻿using UnityEngine;
using System.Collections;
using WellFired;

[USequencerFriendlyName("Play Animation")]
[USequencerEvent("LegrandCinematic/PlayAnimation")]
public class PlayPlayerAnimationEvent : USEventBase  {
    private PlayerAnimation PlayerAnimation;
    public string Animation;
	public bool Crossfade = false;

    public override void FireEvent()
    {
		GameObject g = GameObject.Find(TimelineContainer.AffectedObjectPath);
        PlayerAnimation = g.GetComponent<PlayerAnimation>();
        PlayerAnimation.PlayAnimation(Animation, Crossfade);

    }
    
    public override void ProcessEvent(float deltaTime)
    {
        
    }
}
