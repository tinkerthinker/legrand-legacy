﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

namespace Flux
{
    [FEvent("Transform/Set Position")]
    public class SetTransformPosition : FEvent
    {
        public Vector3 Position;
        public bool IsLocal = false;

        protected override void OnTrigger(float timeSinceTrigger)
        {
            base.OnTrigger(timeSinceTrigger);
            if (!IsLocal)
                Owner.transform.position = Position;
            else
                Owner.transform.localPosition = Position;
        }
    }
}