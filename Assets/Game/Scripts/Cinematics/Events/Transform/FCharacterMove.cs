﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

namespace Flux
{
    [FEvent("Transform/Move NPC With Physic")]
    public class FCharacterMove : FEvent
    {
        public Vector2 TargetPosition;
        public float Speed = 2f;
        public float DistanceStop = -1f;
        public bool IsLookForward = true;

        public string OnStartAnimation = "Walk";
        public bool OnStartAnimationCrossfade = true;
        public string OnEndAnimation = "Idle";
        public bool OnEndAnimationCrossfade = true;
        public float AnimationCrossFadeTime = 0.2f;

        private NPCMovementController _npcMoveController;

        protected override void OnTrigger(float timeSinceTrigger)
        {
            base.OnTrigger(timeSinceTrigger);
            if (!string.IsNullOrEmpty(OnStartAnimation))
            {
                int hashID = Animator.StringToHash(OnStartAnimation);
                if (!OnStartAnimationCrossfade)
                    Owner.GetComponent<Animator>().Play(hashID);
                else
                    Owner.GetComponent<Animator>().CrossFade(hashID, AnimationCrossFadeTime);
            }

            _npcMoveController.ClearEndMovementListener();
            if (!string.IsNullOrEmpty(OnEndAnimation))
            {
                _npcMoveController.Done += MoveEndPlayAnimation;
            }

            if(DistanceStop < 0f)
               _npcMoveController.SetMovement(TargetPosition, Speed, IsLookForward);
            else
               _npcMoveController.SetMovement(TargetPosition, Speed, IsLookForward, DistanceStop);
        }
        
        protected override void OnInit()
        {
            base.OnInit();
            _npcMoveController = Owner.GetComponent<NPCMovementController>();    
        }


        private void MoveEndPlayAnimation()
        {
            int hashID = Animator.StringToHash(OnEndAnimation);
            if (!OnEndAnimationCrossfade)
                Owner.GetComponent<Animator>().Play(hashID);
            else
                Owner.GetComponent<Animator>().CrossFade(hashID, AnimationCrossFadeTime);
        }
    }
}