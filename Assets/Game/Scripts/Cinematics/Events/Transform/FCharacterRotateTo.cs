﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

namespace Flux
{
    [FEvent("Transform/Rotate NPC in Degress")]
    public class FCharacterRotateTo : FEvent
    {
        public float TargetRotationY;
        public float Speed = 360f;
        public bool IsLookForward = true;

        private NPCMovementController _npcMoveController;

        protected override void OnTrigger(float timeSinceTrigger)
        {
            base.OnTrigger(timeSinceTrigger);
            Vector3 getRotationVector = Owner.transform.rotation.eulerAngles;
            getRotationVector.y = TargetRotationY;
            Owner.transform.DORotate(getRotationVector, 360f / Speed);
        }
    }
}