﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

namespace Flux
{
    [FEvent("Transform/Set Rotation")]
    public class SetTransformRotation : FEvent
    {
        public Vector3 Rotation;
        public bool IsLocal = false;

        protected override void OnTrigger(float timeSinceTrigger)
        {
            base.OnTrigger(timeSinceTrigger);
            if (!IsLocal)
                Owner.transform.eulerAngles = Rotation;
            else
                Owner.transform.localEulerAngles = Rotation;
        }
        
    }
}