﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

namespace Flux
{
    [FEvent("Transform/Set Parent")]
    public class SetParent : FEvent
    {
        public string ParentTargetPath;
        public bool PositionStay = true;
        private Transform Target;

        protected override void OnTrigger(float timeSinceTrigger)
        {
            Owner.transform.SetParent(Target, PositionStay);
        }

        protected override void OnInit()
        {
            Target = GameObject.Find(ParentTargetPath).transform;
        }
    }
}