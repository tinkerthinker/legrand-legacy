﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

namespace Flux
{
    [FEvent("Transform/Stop Move NPC Controller")]
    public class FCharacterStopMove : FEvent
    {
        private NPCMovementController _npcMoveController;

        protected override void OnTrigger(float timeSinceTrigger)
        {
            base.OnTrigger(timeSinceTrigger);
            _npcMoveController.StopMove();
        }
        
        protected override void OnInit()
        {
            base.OnInit();
            _npcMoveController = Owner.GetComponent<NPCMovementController>();    
        }
    }
}