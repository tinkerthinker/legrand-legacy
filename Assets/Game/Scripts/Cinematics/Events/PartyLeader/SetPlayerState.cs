﻿using UnityEngine;
using System.Collections;
using WellFired;
using Legrand.core;

[USequencerFriendlyName("Set Leader State")]
[USequencerEvent("LegrandCinematic/Party Leader/Set Party Leader State")]
public class SetPlayerState : USEventBase {
	public PlayerController.PlayerControllerState NewState;

	public override void FireEvent()
	{
		AffectedObject.GetComponent<PlayerController> ().ControllerState = NewState;
	}

	public override void ProcessEvent(float deltaTime)
	{

	}
}
