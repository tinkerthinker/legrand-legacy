﻿using UnityEngine;
using System.Collections;

public class ApplyNewPosition : MonoBehaviour {
	public bool ChangePosition = true ;
	public bool ChangeRotation = true;

	public Transform SourceTransform;
	[Header("If not using gameobject transform")]
	public Vector3 NewPosition;
	public Vector3 NewRotation;

	public void FireEvent()
	{
		Vector3 newPosition;
		Vector3 newRotation;

		if (SourceTransform != null) 
		{
			newPosition = SourceTransform.position;
			newRotation = SourceTransform.eulerAngles;
		} else 
		{
			newPosition = NewPosition;
			newRotation = NewRotation;
		}

		if(ChangePosition && ChangeRotation)
			EventManager.Instance.TriggerEvent (new LastPositionCheckEvent (newPosition, newRotation));
		
		else if(ChangePosition)
			EventManager.Instance.TriggerEvent (new LastPositionCheckEvent (true, newPosition));

		else if(ChangeRotation)
			EventManager.Instance.TriggerEvent (new LastPositionCheckEvent (false, newRotation));
	}
}
