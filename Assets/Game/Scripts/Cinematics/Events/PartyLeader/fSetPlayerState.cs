﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

namespace Flux
{
    [FEvent("Party Leader/Set State")]
    public class fSetPlayerState : FEvent
    {
        public PlayerController.PlayerControllerState NewState;
        private PlayerController _pController;

        protected override void OnTrigger(float timeSinceTrigger)
        {
            base.OnTrigger(timeSinceTrigger);
            _pController.ControllerState = NewState;
        }

        protected override void OnInit()
        {
            base.OnInit();
            _pController = Owner.GetComponent<PlayerController>();
        }
    }
}