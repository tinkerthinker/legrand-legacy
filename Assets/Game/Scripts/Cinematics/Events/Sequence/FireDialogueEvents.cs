﻿using UnityEngine;
using System.Collections;
using WellFired;

[USequencerFriendlyName("Start Dialoguer")]
[USequencerEvent("LegrandCinematic/Dialogue/Start Dialog")]
public class FireDialogueEvents : USEventBase 
{
    public string DialogueName = "";
    private USSequencer sequenceToResume = null;
	public bool ForcePause = true;

    public override void FireEvent()
    {
        sequenceToResume = GetComponentInParent<USSequencer> ();
		if(ForcePause)
		sequenceToResume.Pause();

        int dialogueID = LegrandUtility.GetDialogueID(DialogueName);
        if (dialogueID >= 0)
        {
            LegrandUtility.StartDialogue(dialogueID);
            EventManager.Instance.AddListener<DialogWindowClosed>(DialogFinished);
            EventManager.Instance.AddListener<ResumeSequenceEvent>(DialogPaused);
        }
        else DialogFinished(null);
    }

    public void OnDestroy()
    {

    }

    public override void ProcessEvent(float deltaTime)
    {
        
    }

    public void DialogFinished(DialogWindowClosed e)
    {
        EventManager.Instance.RemoveListener<DialogWindowClosed>(DialogFinished);
		EventManager.Instance.RemoveListener<ResumeSequenceEvent>(DialogPaused);

        StartCoroutine (WaitUntilPause());
    }

	public void DialogPaused(ResumeSequenceEvent e)
	{
		EventManager.Instance.RemoveListener<DialogWindowClosed>(DialogFinished);
		EventManager.Instance.RemoveListener<ResumeSequenceEvent>(DialogPaused);

		if(e.SkipTo > 0)
			StartCoroutine (WaitUntilPause(e.SkipTo));
		else
			StartCoroutine (WaitUntilPause());
	}

	IEnumerator WaitUntilPause()
	{
		while(sequenceToResume.IsPlaying)yield return null;
		sequenceToResume.Play();
	}

	IEnumerator WaitUntilPause(float skipTo)
	{
		while(sequenceToResume.IsPlaying)yield return null;

		sequenceToResume.SkipTimelineTo (skipTo);
		sequenceToResume.Play();
	}
}