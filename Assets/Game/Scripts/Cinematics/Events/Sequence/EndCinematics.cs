﻿using UnityEngine;
using System.Collections;
using WellFired;

[USequencerFriendlyName("TransToWorld")]
[USequencerEvent("LegrandCinematic/Transition/TransToWorld")]
public class EndCinematics : USEventBase  {
	public TransitionData TransData;

    public override void FireEvent()
    {
		WorldSceneManager.TransToWorld (TransData);
		//StartCoroutine (StopSequence ());
    }

    public override void ProcessEvent(float deltaTime)
    {
        
    }

	IEnumerator StopSequence()
	{
		yield return new WaitForSeconds(1.5f);
		USSequencer CurrentSequence = GetComponentInParent<USSequencer>();
		CurrentSequence.Stop();
		Destroy(CurrentSequence.gameObject);
	}
}
