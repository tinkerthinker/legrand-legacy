﻿using UnityEngine;
using System.Collections;

public class DestroySequenceFlux : MonoBehaviour  {
    public void FireEvent()
    {
		Destroy(this.gameObject);
    }
}
