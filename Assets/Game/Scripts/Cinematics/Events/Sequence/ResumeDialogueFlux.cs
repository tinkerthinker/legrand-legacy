﻿using UnityEngine;
using System.Collections;
using Flux;

public class ResumeDialogueFlux : MonoBehaviour
{
	public FSequence sequenceToResume = null;

	public void FireEvent()
	{
		sequenceToResume = GetComponentInParent<FSequence> ();

		Dialoguer.SetGlobalBoolean (5, true);
		EventManager.Instance.AddListener<DialogWindowClosed>(DialogFinished);
		EventManager.Instance.AddListener<ResumeSequenceEvent>(DialogPaused);
	}

	public void FireEventForcePause()
	{
		sequenceToResume = GetComponentInParent<FSequence> ();
		sequenceToResume.Pause ();	
		
		Dialoguer.SetGlobalBoolean (5, true);
		EventManager.Instance.AddListener<DialogWindowClosed>(DialogFinished);
		EventManager.Instance.AddListener<ResumeSequenceEvent>(DialogPaused);
	}

	public void DialogFinished(DialogWindowClosed e)
	{
		EventManager.Instance.RemoveListener<DialogWindowClosed>(DialogFinished);
		EventManager.Instance.RemoveListener<ResumeSequenceEvent>(DialogPaused);
		StartCoroutine (WaitUntilPause());
	}
	
	public void DialogPaused(ResumeSequenceEvent e)
	{
		EventManager.Instance.RemoveListener<DialogWindowClosed>(DialogFinished);
		EventManager.Instance.RemoveListener<ResumeSequenceEvent>(DialogPaused);
		StartCoroutine (WaitUntilPause());
	}

	IEnumerator WaitUntilPause()
	{
		while(sequenceToResume.IsPlaying)yield return null;
		sequenceToResume.Resume();
	}
}