﻿using UnityEngine;
using System.Collections;
using WellFired;

[USequencerFriendlyName("DestroySequence")]
[USequencerEvent("LegrandCinematic/Sequence/DestroySequence")]
public class DestroySequence : USEventBase  {
    public bool Stop = true;

    public override void FireEvent()
    {
		USSequencer SequenceToDestroy = GetComponentInParent<USSequencer> ();
        if(Stop) SequenceToDestroy.Stop();
        Destroy(SequenceToDestroy.gameObject);
    }

    public override void ProcessEvent(float deltaTime)
    {
        
    }
}
