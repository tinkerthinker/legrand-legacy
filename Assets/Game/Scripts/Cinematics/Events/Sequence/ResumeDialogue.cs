﻿using UnityEngine;
using System.Collections;
using WellFired;

[USequencerFriendlyName("Resume Dialogue")]
[USequencerEvent("LegrandCinematic/Dialogue/ResumeDialogue")]
public class ResumeDialogue : USEventBase 
{
	public USSequencer sequenceToResume = null;
	public bool ForcePause = true;
	public override void FireEvent()
	{
		sequenceToResume = GetComponentInParent<USSequencer> ();
		if(ForcePause)
		sequenceToResume.Pause ();
		Dialoguer.SetGlobalBoolean (5, true);
		EventManager.Instance.AddListener<DialogWindowClosed>(DialogFinished);
		EventManager.Instance.AddListener<ResumeSequenceEvent>(DialogPaused);
	}
	
	public override void ProcessEvent(float deltaTime)
	{
		
	}

	public void DialogFinished(DialogWindowClosed e)
	{
		EventManager.Instance.RemoveListener<DialogWindowClosed>(DialogFinished);
		EventManager.Instance.RemoveListener<ResumeSequenceEvent>(DialogPaused);
		StartCoroutine (WaitUntilPause());
	}
	
	public void DialogPaused(ResumeSequenceEvent e)
	{
		EventManager.Instance.RemoveListener<DialogWindowClosed>(DialogFinished);
		EventManager.Instance.RemoveListener<ResumeSequenceEvent>(DialogPaused);
		if(e.SkipTo > 0)
			StartCoroutine (WaitUntilPause(e.SkipTo));
		else
			StartCoroutine (WaitUntilPause());
	}

	IEnumerator WaitUntilPause()
	{
		while(sequenceToResume.IsPlaying)yield return null;
		sequenceToResume.Play();
	}

	IEnumerator WaitUntilPause(float skipTo)
	{
		while(sequenceToResume.IsPlaying)yield return null;
		
		sequenceToResume.SkipTimelineTo (skipTo);
		sequenceToResume.Play();
	}
}