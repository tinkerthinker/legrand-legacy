﻿using UnityEngine;
using System.Collections;
using WellFired;

public class EndCinematicsFlux : MonoBehaviour  {
	public TransitionData TransData;

    public void FireEvent()
    {
		WorldSceneManager.TransToWorld (TransData);
    }
}
