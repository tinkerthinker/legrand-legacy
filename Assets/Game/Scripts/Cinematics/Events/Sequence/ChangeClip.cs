﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using WellFired;
using Legrand.core;

[USequencerFriendlyName("Next Clip")]
[USequencerEvent("LegrandCinematic/Sequence/Next Clip")]

public class ChangeClip : USEventBase  {

	public override void FireEvent()
	{
		EventManager.Instance.TriggerEvent (new NextClipEvent());
	}
	
	public override void ProcessEvent(float deltaTime)
	{

	}
}
