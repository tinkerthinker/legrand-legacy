﻿using UnityEngine;
using System.Collections;
using Flux;

public class FireDialogueFlux : MonoBehaviour 
{
    public string DialogueName = "";
    private FSequence sequenceToResume = null;

    void OnDestroy()
    {
        EventManager.Instance.RemoveListener<DialogWindowClosed>(DialogFinished);
        EventManager.Instance.RemoveListener<ResumeSequenceEvent>(DialogPaused);
    }

	public void FireEvent()
	{
		sequenceToResume = GetComponentInParent<FSequence> ();

        int dialogueID = LegrandUtility.GetDialogueID(DialogueName);

        if (dialogueID >= 0)
        {
            LegrandUtility.StartDialogue(dialogueID);
            EventManager.Instance.AddListener<DialogWindowClosed>(DialogFinished);
            EventManager.Instance.AddListener<ResumeSequenceEvent>(DialogPaused);
        }
        else DialogFinished(null);

    }

    public void FireEventForcePause()
    {
        sequenceToResume = GetComponentInParent<FSequence> ();
		sequenceToResume.Pause();

        int dialogueID = LegrandUtility.GetDialogueID(DialogueName);

        if (dialogueID >= 0)
        {
            LegrandUtility.StartDialogue(dialogueID);
            EventManager.Instance.AddListener<DialogWindowClosed>(DialogFinished);
            EventManager.Instance.AddListener<ResumeSequenceEvent>(DialogPaused);
        }
        else DialogFinished(null);
    }

    public void DialogFinished(DialogWindowClosed e)
    {
        EventManager.Instance.RemoveListener<DialogWindowClosed>(DialogFinished);
		EventManager.Instance.RemoveListener<ResumeSequenceEvent>(DialogPaused);

        StartCoroutine (WaitUntilPause());
    }

	public void DialogPaused(ResumeSequenceEvent e)
	{
		EventManager.Instance.RemoveListener<DialogWindowClosed>(DialogFinished);
		EventManager.Instance.RemoveListener<ResumeSequenceEvent>(DialogPaused);

		StartCoroutine (WaitUntilPause());
	}

	IEnumerator WaitUntilPause()
	{
		while(sequenceToResume.IsPlaying)yield return null;
		sequenceToResume.Resume();
	}
}