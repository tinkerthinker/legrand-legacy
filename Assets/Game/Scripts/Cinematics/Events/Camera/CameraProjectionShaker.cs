﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
public class CameraProjectionShaker : MonoBehaviour {
	public CutsceneCameraPanning CameraPanner;

	public float Shake;
	public float ShakeAmount = 0.001f;
	public float DescreaseFactor = 1f;
	public bool InfiniteShake = false;
    private Vector2 _StartPosition;
		

	void StartShake()
	{
        _StartPosition = new Vector2(CameraPanner.zoomX, CameraPanner.zoomY);
		StartCoroutine ("AnimateShake");
	}

	void StopShake()
	{
		StopCoroutine ("AnimateShake");
        CameraPanner.SetVanishingPanning(CameraPanner.Camera.fieldOfView, _StartPosition);
	}

	IEnumerator AnimateShake()
	{
		while(Shake > 0 || InfiniteShake)
		{
			Vector2 randomShakePosition = Random.insideUnitSphere * ShakeAmount;
            CameraPanner.SetVanishingPanning(CameraPanner.Camera.fieldOfView, randomShakePosition + _StartPosition);
			Shake -= Time.deltaTime * DescreaseFactor;
		yield return new WaitForEndOfFrame ();
		}
	}
}
