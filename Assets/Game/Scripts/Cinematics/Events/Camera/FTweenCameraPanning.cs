﻿using UnityEngine;
using System.Collections;
using Flux;

[FEvent("Camera/CameraPanner")]
public class FTweenCameraPanning : FTweenEvent<FTweenVector3>
{
    private CutsceneCameraPanning _CameraPanner;

    protected override void OnTrigger(float timeSinceTrigger)
    {
        base.OnTrigger(timeSinceTrigger);
    }

    protected override void OnInit()
    {
        base.OnInit();
        _CameraPanner = LegrandUtility.GetComponentInChildren<CutsceneCameraPanning>(Owner.gameObject);
    }

    protected override void OnStop()
    {
        base.OnStop();//Owner.localPosition = _startPosition;
    }

    protected override void SetDefaultValues()
    {
        //			_tween = new FTweenVector3( Owner.localPosition, Owner.localPosition );
    }

    protected override void ApplyProperty(float t)
    {
        Vector3 newPanner = _tween.GetValue(t);
        if(_CameraPanner != null)
        _CameraPanner.SetVanishingPanning(newPanner.x, new Vector2(newPanner.y, newPanner.z));
    }
}
