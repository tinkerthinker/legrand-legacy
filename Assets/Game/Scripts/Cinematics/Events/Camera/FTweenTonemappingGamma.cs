﻿using UnityEngine;
using UnityStandardAssets.CinematicEffects;
using Flux;

[FEvent("Camera/Tonemapping/Gamma")]
public class FTweenTonemappingGamma : FTweenEvent<FTweenFloat>
{
    private TonemappingColorGrading _CameraToneMapping;
    TonemappingColorGrading.ColorGrading newColor;

    protected override void OnTrigger(float timeSinceTrigger)
    {
        base.OnTrigger(timeSinceTrigger);
    }

    protected override void OnInit()
    {
        base.OnInit();
        _CameraToneMapping = LegrandUtility.GetComponentInChildren<TonemappingColorGrading>(Owner.gameObject);
    }

    protected override void OnStop()
    {
        base.OnStop();//Owner.localPosition = _startPosition;
    }

    protected override void SetDefaultValues()
    {
        //			_tween = new FTweenVector3( Owner.localPosition, Owner.localPosition );
    }

    protected override void ApplyProperty(float t)
    {
        newColor = _CameraToneMapping.colorGrading;
        newColor.gamma = _tween.GetValue(t);
        _CameraToneMapping.colorGrading = newColor;
    }
}
