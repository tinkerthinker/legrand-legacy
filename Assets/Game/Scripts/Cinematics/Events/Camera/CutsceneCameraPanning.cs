﻿using UnityEngine;
using System.Collections;

public class CutsceneCameraPanning : MonoBehaviour {
	public Camera Camera;

	public float CurrentZoomX = 0f;
	public float CurrentZoomY = 0f;

	public float zoomX;
	public float zoomY;
	public float ZoomFOV;
	public float LerpTime = 2f;

	public bool PanningOnStart;
	public bool EditMode = false;

	void Start()
	{
		if (PanningOnStart)
			InstantZooming(zoomX, zoomY, ZoomFOV);

		EventManager.Instance.AddListener<CutsceneZoomEvent> (EventZoomIn);
	}

	void OnDestroy()
	{
		if(EventManager.Instance != null)
			EventManager.Instance.RemoveListener<CutsceneZoomEvent> (EventZoomIn);
	}

	void EventZoomIn(CutsceneZoomEvent e)
	{
		ZoomIn ();
	}

	public void ZoomIn()
	{
		StartCoroutine(Zooming(CurrentZoomX, zoomX, CurrentZoomY, zoomY, Camera.fieldOfView, ZoomFOV, true));
	}

	IEnumerator Zooming(float xOri, float xDest, float yOri, float yDest, float fovOrigin, float fovFinal, bool zoomingIn = false)
	{
		float t = 0.0f;

		while (t < 1.0f)
		{
			ResetProjectionMatrix();

			t += Time.deltaTime / LerpTime;
			Camera.fieldOfView = Mathf.Lerp(fovOrigin, fovFinal, t);


			float xAxis = Mathf.Lerp(xOri, xDest, t);
			float yAxis = Mathf.Lerp(yOri, yDest, t);
			SetVanishingPoint(Camera, new Vector2(xAxis, yAxis));

			yield return null;
		}
	}

	private void InstantZooming(float xDest, float yDest, float fovFinal)
	{
		ResetProjectionMatrix();
		Camera.fieldOfView = fovFinal;
		SetVanishingPoint(Camera, new Vector2(xDest, yDest));
	}

	void Update()
	{
		if (EditMode) 
		{
			ResetProjectionMatrix();
			Camera.fieldOfView = ZoomFOV;
			SetVanishingPoint(Camera, new Vector2(zoomX, zoomY));
		}
	}


    public void SetVanishingPanning(float FOV, Vector2 perspectiveOffset)
    {
        ResetProjectionMatrix();
        zoomX = perspectiveOffset.x;
        zoomY = perspectiveOffset.y;
        ZoomFOV = FOV;
        Camera.fieldOfView = FOV;
        SetVanishingPoint(Camera, perspectiveOffset);
    }

    void SetVanishingPoint(Camera cam, Vector2 perspectiveOffset)
	{
		Matrix4x4 m = cam.projectionMatrix;
		float w = 2 * cam.nearClipPlane / m.m00;
		float h = 2 * cam.nearClipPlane / m.m11;

		float left = -w / 2 - perspectiveOffset.x;
		float right = left + w;
		float bottom = -h / 2 - perspectiveOffset.y;
		float top = bottom + h;

		cam.projectionMatrix = PerspectiveOffCenter(left, right, bottom, top, cam.nearClipPlane, cam.farClipPlane);
		//print("w : " + w + ", left : " + left + ", right : " + right);
	}

	static Matrix4x4 PerspectiveOffCenter(
		float left, float right,
		float bottom, float top,
		float near, float far)
	{
		float x = (2.0f * near) / (right - left);
		float y = (2.0f * near) / (top - bottom);
		float a = (right + left) / (right - left);
		float b = (top + bottom) / (top - bottom);
		float c = -(far + near) / (far - near);
		float d = -(2.0f * far * near) / (far - near);
		float e = -1.0f;

		Matrix4x4 m = new Matrix4x4();
		m[0, 0] = x; m[0, 1] = 0.0f; m[0, 2] = a; m[0, 3] = 0.0f;
		m[1, 0] = 0.0f; m[1, 1] = y; m[1, 2] = b; m[1, 3] = 0.0f;
		m[2, 0] = 0.0f; m[2, 1] = 0.0f; m[2, 2] = c; m[2, 3] = d;
		m[3, 0] = 0.0f; m[3, 1] = 0.0f; m[3, 2] = e; m[3, 3] = 0.0f;

		return m;
	}

	public void ResetProjectionMatrix()
	{
		Camera.ResetProjectionMatrix();
	}
}
