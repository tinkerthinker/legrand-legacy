﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

namespace Flux
{
    [FEvent("Camera/Camera Projection Shake")]
    public class FTweenProjectionChange : FTweenEvent<FTweenFloat>
    {
        private CameraProjectionShaker _CameraShaker;

        protected override void OnTrigger(float timeSinceTrigger)
        {
            base.OnTrigger(timeSinceTrigger);
        }

        protected override void OnInit()
        {
            base.OnInit();
            _CameraShaker = LegrandUtility.GetComponentInChildren<CameraProjectionShaker>(Owner.gameObject);
        }

        protected override void OnStop()
        {
            base.OnStop();//Owner.localPosition = _startPosition;
        }

        protected override void SetDefaultValues()
        {
            //			_tween = new FTweenVector3( Owner.localPosition, Owner.localPosition );
        }

        protected override void ApplyProperty(float t)
        {
            float newPanner = _tween.GetValue(t);
            if (_CameraShaker != null)
                _CameraShaker.ShakeAmount = newPanner;
        }
    }
}