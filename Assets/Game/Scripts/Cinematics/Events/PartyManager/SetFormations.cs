﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using WellFired;

[USequencerFriendlyName("Set Formations")]
[USequencerEvent("LegrandCinematic/PartyManager/Set Formations")]
public class SetFormations : USEventBase {
	public List<string> CharacterIds;
	public List<Vector2> Positions;
	public bool Clear;

	public override void FireEvent()
	{
		StartCoroutine (Activities());
	}

	IEnumerator Activities()
	{
		if(Clear)PartyManager.Instance.BattleFormations.Formations.Clear ();
		for (int index = 0; index< CharacterIds.Count; index++) 
		{
			yield return null;
			PartyManager.Instance.BattleFormations.AddFormation (CharacterIds[index], Positions[index]);
		}
	}

	public override void ProcessEvent(float deltaTime)
	{
		
	}
}
