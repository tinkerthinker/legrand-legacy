﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using WellFired;

[USequencerFriendlyName("Add Member")]
[USequencerEvent("LegrandCinematic/PartyManager/Add Party Member")]
public class AddParty : USEventBase {
	public List<string> NewMemberIds;

	public override void FireEvent()
	{
		StartCoroutine (Activities());
	}

	IEnumerator Activities()
	{
		foreach (string NewMemberId in NewMemberIds) 
		{
			yield return null;
			PartyManager.Instance.AddNewCharacter (NewMemberId);
		}
	}

	public override void ProcessEvent(float deltaTime)
	{
		
	}
}
