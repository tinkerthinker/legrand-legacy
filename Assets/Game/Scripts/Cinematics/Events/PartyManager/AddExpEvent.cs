﻿using UnityEngine;
using System.Collections;
using Legrand.core;
public class AddExpEvent : MonoBehaviour {
    public int Exp;
    // Use this for initialization
    void FireEvent()
    {
        MainCharacter[] characters = PartyManager.Instance.CharacterParty.ToArray();
        for (int i=0;i< characters.Length; i++)
        {
            characters[i].IncreaseEXP(Exp);
        }
        PartyManager.Instance.TotalEXP += Exp;
    }
}
