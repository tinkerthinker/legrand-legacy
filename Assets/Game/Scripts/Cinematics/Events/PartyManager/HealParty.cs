﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using WellFired;
using Legrand.core;

[USequencerFriendlyName("Heal")]
[USequencerEvent("LegrandCinematic/PartyManager/Heal Party")]
public class HealParty : USEventBase {

	public List<string> CharacterIDs;
	public List<float> HealPercentage;
	
	public override void FireEvent()
	{
		for (int index = 0; index< CharacterIDs.Count; index++) 
		{
			foreach(MainCharacter mc in PartyManager.Instance.CharacterParty)
			{
				if(mc._ID == CharacterIDs[index])
				{
					mc.Health.Increase(mc.Health.MaxValue * (HealPercentage[index]/100f));
				}
			}
		}
	}
	
	public override void ProcessEvent(float deltaTime)
	{
		
	}
}
