﻿using UnityEngine;
using System.Collections;
using WellFired;

[USequencerFriendlyName("Remove Member")]
[USequencerEvent("LegrandCinematic/PartyManager/Remove Party Member")]
public class RemoveParty : USEventBase {
	public string RemovedMemberId;
	
	public override void FireEvent()
	{
		PartyManager.Instance.RemoveCharacter (RemovedMemberId);
	}
	
	public override void ProcessEvent(float deltaTime)
	{
		
	}
}
