﻿using UnityEngine;
using System.Collections;
using WellFired;
using Legrand.core;

[USequencerFriendlyName("Set Animation Parameter")]
[USequencerEvent("LegrandCinematic/Animation/Set Animation Parameter")]
public class SetAnimationParameter : USEventBase {
	public string ParameterName;
	public float Value;
	
	public override void FireEvent()
	{
		GameObject g = TimelineContainer.AffectedObject.gameObject;
		Animator animator = g.GetComponent<Animator>();
		animator.SetFloat (ParameterName, Value);
	}
	
	public override void ProcessEvent(float deltaTime)
	{
		
	}
}
