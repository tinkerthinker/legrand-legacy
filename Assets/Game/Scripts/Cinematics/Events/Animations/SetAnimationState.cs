﻿using UnityEngine;
using System.Collections;
using WellFired;
using Legrand.core;

[USequencerFriendlyName("Set Animation State")]
[USequencerEvent("LegrandCinematic/Animation/Set Animation State")]
public class SetAnimationState : USEventBase {
	public string AnimationName;
	
	public override void FireEvent()
	{
		GameObject g = TimelineContainer.AffectedObject.gameObject;
		Animator animator = g.GetComponent<Animator>();
		animator.CrossFade (AnimationName, 0f);
	}
	
	public override void ProcessEvent(float deltaTime)
	{
		
	}
}
