﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

namespace Flux
{
    [FEvent("Animation/Custom Play Animation")]
    public class PlayAnimation : FEvent
    {
        public string AnimationName;
        public int layer = -1;
        public bool RandomStart = false;
        public bool Crossfade = true;
        public float CrossFadeTime = 0.25f;
        private Animator _Animator;

        protected override void OnTrigger(float timeSinceTrigger)
        {
            base.OnTrigger(timeSinceTrigger);
            int hashID = Animator.StringToHash(AnimationName);
            if (!Crossfade)
            {
                if (RandomStart)
                    _Animator.Play(hashID, layer, LegrandUtility.Random(0.0f, 1.0f));
                else
                    _Animator.Play(hashID, layer);
            }
            else
                _Animator.CrossFade(AnimationName, CrossFadeTime);
        }

        protected override void OnInit()
        {
            base.OnInit();
            _Animator = Owner.GetComponent<Animator>();
        }
        
    }
}