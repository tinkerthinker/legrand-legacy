﻿using UnityEngine;
using System.Collections;
using RenderHeads.Media.AVProVideo;

public class CloseVideo : InspectorCutsceneEvent
{
    public MediaPlayer mp;
    public DisplayUGUI uGUIVideo;

    public override void FireEvent()
    {
        mp.CloseVideo();
        uGUIVideo.gameObject.SetActive(false);
    }
}
