﻿using UnityEngine;
using System.Collections;
using RenderHeads.Media.AVProVideo;
using Flux;

public class FluxResume : InspectorCutsceneEvent
{
    [HideInInspector]
    public FSequence CurrentSequence;

    public override void FireEvent()
    {
        CurrentSequence.Resume();
    }
}
