﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using RenderHeads.Media.AVProVideo;

public class PauseVideo : MonoBehaviour {
    public MediaPlayer mp;

    void Start()
    {
        if(GameSetting.Volume != null && mp.Control != null)
            mp.Control.SetVolume(GameSetting.Volume[0]/100f);
        EventManager.Instance.AddListener<PauseEvent>(PauseVideoHandler);
        EventManager.Instance.AddListener<UnPauseEvent>(UnPauseVideoHandler);
    }

    void OnDestroy()
    {
        if (EventManager.Instance)
        {
            EventManager.Instance.RemoveListener<PauseEvent>(PauseVideoHandler);
            EventManager.Instance.RemoveListener<UnPauseEvent>(UnPauseVideoHandler);
        }
    }

    void PauseVideoHandler(PauseEvent e)
    {
        mp.Control.Pause();
    }

    void UnPauseVideoHandler(UnPauseEvent e)
    {
        if (GameSetting.Volume != null & mp.Control != null)
            mp.Control.SetVolume(GameSetting.Volume[0] / 100f);
        mp.Control.Play();
    }

    public void FireEvent()
	{
		MovieTexture movie = ((MovieTexture)this.gameObject.GetComponent<RawImage> ().mainTexture);
		movie.Pause ();
	}
}
