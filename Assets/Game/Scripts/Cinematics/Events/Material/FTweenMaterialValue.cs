﻿using UnityEngine;


namespace Flux
{
    [FEvent("Material/Tween Value")]
    public class FTweenMaterialValue : FTweenEvent<FTweenVector4>
    {
        public string PropertyName;
        public bool ChangeAllChildren;

        Renderer[] _meshRenderers;

        // 0 - Float, 1 - Vector2, 2 - Vector3, 3 - Color
        public int ShaderPropertyType;

        protected override void ApplyProperty(float t)
        {
            Vector4 newValue = _tween.GetValue(t);
            foreach(Renderer r in _meshRenderers)
            {
                if (ShaderPropertyType == 0)
                {
                    r.material.SetFloat(PropertyName, newValue.x);
                }
                else if (ShaderPropertyType == 1)
                {
                }
                else if (ShaderPropertyType == 2)
                {

                }
                else if (ShaderPropertyType == 3)
                {
                    r.material.SetColor(PropertyName, new Color(newValue.x, newValue.y, newValue.z, newValue.w));
                }
            }
        }

        protected override void OnInit()
        {
            base.OnInit();
            if(ChangeAllChildren)
                _meshRenderers = LegrandUtility.GetComponentsInChildren<Renderer>(Owner.gameObject);
            else
            {
                _meshRenderers = new Renderer[1];
                _meshRenderers[0] = LegrandUtility.GetComponentInChildren<Renderer>(Owner.gameObject);
            }
                
        }
    }
}
