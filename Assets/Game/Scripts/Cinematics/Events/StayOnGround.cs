﻿using UnityEngine;
using System.Collections;

public class StayOnGround : MonoBehaviour {
	private RaycastHit _hitInfo;
	public Transform _BIP;

	// Update is called once per frame
	void LateUpdate () {
		// Handle player physics when going down steep ground
		if (_BIP != null && !Physics.Raycast (new Ray (_BIP.position, Vector3.down), out _hitInfo, 1f, LayerMask.GetMask ("Ground")) && !Physics.Raycast (new Ray (transform.position, transform.forward), out _hitInfo, 2f, LayerMask.GetMask ("Ground"))) {
			transform.position = new Vector3(transform.position.x ,_hitInfo.point.y, transform.position.z);
		}
	}
}
