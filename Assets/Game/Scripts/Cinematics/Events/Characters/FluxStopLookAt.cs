﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
namespace Flux
{
    [FEvent("Character/HeadStopLookAt")]
    public class FluxStopLookAt : FEvent
    {
        public float StopLookAtTimeRange = 1f;
        private HeadLookController _HeadLookController;

        protected override void OnInit()
        {
            _HeadLookController = Owner.GetComponent<HeadLookController>();
        }

        protected override void OnTrigger(float timeSinceTrigger)
        {
            DOTween.To(() => _HeadLookController.effect, x => _HeadLookController.effect = x, 0, StopLookAtTimeRange);
        }
    }
}