﻿using UnityEngine;
using System.Collections;

public class AddForceEvent : MonoBehaviour {

	public Vector3 direction = Vector3.up;

	public float strength = 1.0f;
	public ForceMode type = ForceMode.Impulse;
	Rigidbody affectedBody;
	void Start()
	{
		affectedBody = GetComponent<Rigidbody>();
		affectedBody.useGravity = false;
	}

	public void FireEvent()
	{	
		affectedBody = GetComponent<Rigidbody>();

		affectedBody.AddForceAtPosition(direction * strength, transform.position, type);
		affectedBody.useGravity = true;
	}

	public void StopGravity()
	{
		affectedBody.useGravity = false;
		affectedBody.velocity = new Vector3();
	}
		
}
