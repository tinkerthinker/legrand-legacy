﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using WellFired;
using Legrand.core;

[USequencerFriendlyName("Change Attribute")]
[USequencerEvent("LegrandCinematic/Character/Change Attribute")]
public class ChangeAttributeValue : USEventBase {
	public string CharacterId;
	public List<Attribute> ChangedAttribute;
	public List<int> ChangedAttributeValue;

	public override void FireEvent()
	{
		StartCoroutine (ChangeAttribute());
	}

	IEnumerator ChangeAttribute()
	{
		foreach (MainCharacter character in PartyManager.Instance.CharacterParty) 
		{
			if(character._ID == CharacterId)
			{
				for(int index = 0; index< ChangedAttribute.Count; index++)
				{
					character.ChangeBaseAttribute(ChangedAttribute[index], ChangedAttributeValue[index]);
				}
			}
		}
		yield return null;
	}

	public override void ProcessEvent(float deltaTime)
	{
		
	}
}
