﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

namespace Flux
{
    [FEvent("Character/Head Look At By ID")]
    public class FluxLookByID : FEvent
    {
        public string TargetID;
        public float LookAtTimeRange = 1f;

        private Transform Target;
        private HeadLookController _HeadLookController;
        protected override void OnTrigger(float timeSinceTrigger)
        {
            _HeadLookController.TargetTransform = Target.transform;
            DOTween.To(() => _HeadLookController.effect, x => _HeadLookController.effect = x, 1, LookAtTimeRange);
        }

        protected override void OnInit()
        {
            Target = GlobalGameStatus.GetRegisteredObject(TargetID).transform;
            _HeadLookController = Owner.GetComponent<HeadLookController>();
        }
        
    }
}