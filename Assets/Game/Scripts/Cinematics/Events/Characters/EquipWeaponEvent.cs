﻿using UnityEngine;
using System.Collections;

namespace Flux
{
    [FEvent("Character/Equip Weapon")]
    public class EquipWeaponEvent : FEvent
    {
        public bool Equip;
        private WeaponSlots _WeaponSlot;

        protected override void OnTrigger(float timeSinceTrigger)
        {
            if(_WeaponSlot != null)
            {
                if(Equip)
                    _WeaponSlot.PutWeaponDefault();
                else
                    _WeaponSlot.UnequipAllWeapon()  ;
            }
        }

        protected override void OnInit()
        {
            _WeaponSlot = Owner.GetComponent<WeaponSlots>();
        }
        
    }
}