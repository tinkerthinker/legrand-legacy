﻿using UnityEngine;
using System.Collections.Generic;
using Legrand.core;

public class CutsceneAPCharge : MonoBehaviour {
    public VectorStringInteger[] IdApChargePair;
	
    public void FireEvent()
    {
        string metadata = string.Empty;
        for(int i=0;i<IdApChargePair.Length;i++)
        {
            if (i > 0) metadata += ',';
            metadata += IdApChargePair[i].x + "_" + IdApChargePair[i].y;
        }

        LegrandSharedEvents.SetAP(metadata);
    }
}
