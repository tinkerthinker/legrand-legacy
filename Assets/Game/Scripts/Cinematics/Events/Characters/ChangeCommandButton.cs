﻿using UnityEngine;
using System.Collections.Generic;
using WellFired;
using Legrand.core;

[USequencerFriendlyName("Change Command")]
[USequencerEvent("LegrandCinematic/Character/Change Command Button")]
public class ChangeCommandButton : USEventBase {
	public string CharacterId;
	public List<CommandPos> PanelPos;
	public List<CommandPos> SlotPos;
	public List<CommandTypes> CommandType;
	public List<int> CommandValue;

	public override void FireEvent()
	{
		foreach (MainCharacter character in PartyManager.Instance.CharacterParty) 
		{
			if(character._ID == CharacterId)
			{
				for(int index = 0; index< PanelPos.Count; index++)
				{
					character.SetCommand(PanelPos[index], SlotPos[index], CommandType[index], CommandValue[index]);
				}
				return;
			}
		}
	}
	
	public override void ProcessEvent(float deltaTime)
	{
		
	}
}
