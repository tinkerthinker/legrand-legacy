﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

namespace Flux
{
    [FEvent("Character/HeadLookAt")]
    public class FluxLookAt : FEvent
    {
        public string TargetPath;
        public Transform Target;
        public float LookAtTimeRange = 1f;
        private HeadLookController _HeadLookController;
        protected override void OnTrigger(float timeSinceTrigger)
        {
            _HeadLookController.TargetTransform = Target.transform;
            DOTween.To(() => _HeadLookController.effect, x => _HeadLookController.effect = x, 1, LookAtTimeRange);
        }

        protected override void OnInit()
        {
            if (Target && TargetPath == "") TargetPath = LegrandUtility.GetGameObjectPath(Target.gameObject);

            if (!Target && TargetPath != "")
            {
                GameObject g = GameObject.Find(TargetPath);
                Target = g != null ? g.transform : null;
            }
            _HeadLookController = Owner.GetComponent<HeadLookController>();
        }
        
    }
}