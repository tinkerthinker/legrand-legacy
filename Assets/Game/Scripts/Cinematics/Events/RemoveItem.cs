﻿using UnityEngine;
using System.Collections;
using WellFired;

[USequencerFriendlyName("RemoveItem")]
[USequencerEvent("LegrandCinematic/RemoveItem")]
public class RemoveItem : USEventBase {
	public string itemID;
	public int amount;
	
	public override void FireEvent()
	{
		PartyManager.Instance.Inventory.DeleteItemsByItemID(itemID, amount);
	}
	
	public override void ProcessEvent(float deltaTime)
	{
		
	}
}
