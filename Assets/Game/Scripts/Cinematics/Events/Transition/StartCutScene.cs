﻿using UnityEngine;
using System.Collections;
using WellFired;

[USequencerFriendlyName("TransToCutscene")]
[USequencerEvent("LegrandCinematic/Transition/Trans To CutScene")]
public class StartCutScene : USEventBase {
	public string SceneName;

	public override void FireEvent()
	{
		WorldSceneManager.TransNonWorldToSequence (SceneName);
		//StartCoroutine (StopSequence ());
	}
	
	public override void ProcessEvent(float deltaTime)
	{
		
	}

	IEnumerator StopSequence()
	{
		yield return new WaitForSeconds(1.5f);
		USSequencer CurrentSequence = GetComponentInParent<USSequencer>();
		CurrentSequence.Stop();
		Destroy(CurrentSequence.gameObject);
	}
}
