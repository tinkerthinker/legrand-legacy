﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

namespace Flux
{
    [FEvent("Transition/Start Battle")]
    public class StartBattleFluxEvent : FEvent
    {
        public string PartyId;
        public BattleAreaData.DungeonArea Area;
        public TransitionData TransData;

        protected override void OnTrigger(float timeSinceTrigger)
        {
            BattleAreaData battleAreaData = new BattleAreaData();
            WwiseManager.Instance.StopAll();
            WwiseManager.Instance.PlaySFX(WwiseManager.BattleFX.Battle_transition_wind);
            WorldSceneManager.TransNonWorldToBattle(battleAreaData.GetBattleAreaName(Area), PartyId, TransData);
        }
    }
}