﻿using UnityEngine;
using System.Collections;

public class StartCutSceneFlux : InspectorCutsceneEvent {
	public string SceneName;

	public override void FireEvent()
	{
		WorldSceneManager.TransNonWorldToSequence (SceneName);
	}
}
