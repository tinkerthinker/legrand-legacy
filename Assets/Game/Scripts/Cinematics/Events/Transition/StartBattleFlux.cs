﻿using UnityEngine;
using System.Collections;

public class StartBattleFlux : InspectorCutsceneEvent
{
	public string PartyId;
	public BattleAreaData.DungeonArea Area;
	public TransitionData TransData;

	public override void FireEvent()
	{
        BattleAreaData battleAreaData = new BattleAreaData();
        WwiseManager.Instance.StopAll();
        WwiseManager.Instance.PlaySFX(WwiseManager.BattleFX.Battle_transition_wind);
		WorldSceneManager.TransNonWorldToBattle (battleAreaData.GetBattleAreaName(Area), PartyId, TransData);
	}
}

public class BattleAreaData
{
    public enum DungeonArea
    {
        Rahas,
        TelHarranColiseum,
        Dringrs,
        Dunabad,
        ArkworaDunabad,
        Ostia,
        Finias,
        TrialPlace
    }

    public string GetBattleAreaName(DungeonArea areaName)
    {
        switch(areaName)
        {
            case DungeonArea.Rahas: return "Battle/Arena Rahas Desert";
            case DungeonArea.TelHarranColiseum: return "Battle/Battle Coliseum";
            case DungeonArea.Dringrs: return "Battle/Arena Dringrs Keep";
            case DungeonArea.Dunabad: return "Battle/Arena Dunabad Cave";
            case DungeonArea.ArkworaDunabad: return "Battle/Arena Dunabad Cave Arkwora";
            case DungeonArea.Ostia: return "Battle/Arena Ostia";
            case DungeonArea.Finias: return "Battle/Arena Finias";
            case DungeonArea.TrialPlace: return "Battle/Arena Trial Place";
        }
        return "";
    }
}
