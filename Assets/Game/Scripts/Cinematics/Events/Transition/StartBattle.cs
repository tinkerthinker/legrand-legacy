﻿using UnityEngine;
using System.Collections;
using WellFired;

[USequencerFriendlyName("TransToBattle")]
[USequencerEvent("LegrandCinematic/Transition/Trans To Battle")]
public class StartBattle : USEventBase {
	public string PartyId;
	public BattleAreaData.DungeonArea Area;
	public TransitionData TransData;

	public override void FireEvent()
	{
        BattleAreaData battleAreaData = new BattleAreaData();
        WwiseManager.Instance.StopAll();
        WwiseManager.Instance.PlaySFX(WwiseManager.BattleFX.Battle_transition_wind);
		WorldSceneManager.TransNonWorldToBattle (battleAreaData.GetBattleAreaName(Area), PartyId, TransData);
	}
	
	public override void ProcessEvent(float deltaTime)
	{
		
	}
}