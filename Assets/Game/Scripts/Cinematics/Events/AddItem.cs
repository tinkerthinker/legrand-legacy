﻿using UnityEngine;
using System.Collections;
using WellFired;

[USequencerFriendlyName("AddItem")]
[USequencerEvent("LegrandCinematic/AddItem")]
public class AddItem : USEventBase {
	public string itemID;
	public int amount;
	
	public override void FireEvent()
	{
		PartyManager.Instance.Inventory.AddItems(LegrandBackend.Instance.ItemData[itemID], amount);
	}
	
	public override void ProcessEvent(float deltaTime)
	{
		
	}
}
