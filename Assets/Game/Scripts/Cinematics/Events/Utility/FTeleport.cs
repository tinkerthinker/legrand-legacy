﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

namespace Flux
{
    [FEvent("Utility/Teleport")]
    public class FTeleport : FEvent
    {
        public string metadata;
        
        protected override void OnTrigger(float timeSinceTrigger)
        {
            base.OnTrigger(timeSinceTrigger);
            LegrandSharedEvents.Teleport(metadata);
        }
    }
}