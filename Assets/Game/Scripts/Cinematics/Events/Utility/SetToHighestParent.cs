﻿using UnityEngine;
using System.Collections;
using WellFired;

[USequencerFriendlyName("Change Parent")]
[USequencerEvent("LegrandCinematic/Utility/Set To Highest Parent")]
public class SetToHighestParent : USEventBase {
	public override void FireEvent()
	{
		AffectedObject.transform.parent = AffectedObject.GetComponentInParent<SequencePlayer>().transform;
	}
	
	public override void ProcessEvent(float deltaTime)
	{
		
	}
}
