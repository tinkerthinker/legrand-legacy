﻿using UnityEngine;
using System.Collections;
using WellFired;

[USequencerFriendlyName("New Parent")]
[USequencerEvent("LegrandCinematic/Utility/Set Parent")]
public class SetParent : USEventBase {
	public string ParentPath;

	public override void FireEvent()
	{
		AffectedObject.transform.parent = GameObject.Find (ParentPath).transform;
	}
	
	public override void ProcessEvent(float deltaTime)
	{
		
	}
}
