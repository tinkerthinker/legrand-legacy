﻿using UnityEngine;
using System.Collections;
using WellFired;

[USequencerFriendlyName("SwitchScene")]
[USequencerEvent("LegrandCinematic/Utility/SwitchScene")]
public class SwitchScene : USEventBase {
	public string SceneName;
	
	public override void FireEvent()
	{
		SceneManager.SwitchScene (SceneName);
	}
	
	public override void ProcessEvent(float deltaTime)
	{
		
	}
}
