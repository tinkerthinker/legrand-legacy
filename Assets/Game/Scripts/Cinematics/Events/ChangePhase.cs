﻿using UnityEngine;
using System.Collections;
using WellFired;

[USequencerFriendlyName("ChangePhase")]
[USequencerEvent("LegrandCinematic/ChangePhase")]
public class ChangePhase : USEventBase {
	public PartyManager.StoryPhase NewPhase;

	public override void FireEvent()
	{
		PartyManager.Instance.State.currPhase = (int)NewPhase;
	}
	
	public override void ProcessEvent(float deltaTime)
	{
		
	}
}
