﻿using UnityEngine;
using System;
using UnityStandardAssets.CinematicEffects;

namespace Flux
{
    [FEvent("Script/Tween Tone Mapping")]
    public class FTweenToneMapping : FEvent
    {
        private TonemappingColorGrading _CameraToneMapping;

        FTweenColor get;

        public Color fromShadowColor, fromMidtonesColor, fromHighlightsColor;
        public Color toShadowColor, toMidtonesColor, toHighlighytsColor;
        public FEasingType EasingType;

        private Color _newColor;

        TonemappingColorGrading.ColorGrading newColor;

        float timelastUpdate, lastframetime, totaltime;

        protected override void OnTrigger(float timeSinceTrigger)
        {
            newColor = _CameraToneMapping.colorGrading;
            base.OnTrigger(timeSinceTrigger);
        }

        protected override void OnUpdateEvent(float timeSinceTrigger)
        {
            float t = timeSinceTrigger / LengthTime;
            lastframetime = t - timelastUpdate;
            timelastUpdate = t;
            if(totaltime > 0.2f)
            {
                totaltime = 0f;
                ApplyProperty(t);
            } else
            {
                totaltime += lastframetime;
            }
        }

        protected override void OnInit()
        {
            base.OnInit();
            _CameraToneMapping = LegrandUtility.GetComponentInChildren<TonemappingColorGrading>(Owner.gameObject);
            _newColor = new Color();
        }

        protected override void OnStop()
        {
            base.OnStop();
            ApplyProperty(1f);
        }

        protected override void SetDefaultValues()
        {
            //			_tween = new FTweenVector3( Owner.localPosition, Owner.localPosition );
        }

        protected void ApplyProperty(float t)
        {
            if (_CameraToneMapping != null)
            {
                _newColor.r = FEasing.Tween(fromShadowColor.r, toShadowColor.r, t, EasingType);
                _newColor.g = FEasing.Tween(fromShadowColor.g, toShadowColor.g, t, EasingType);
                _newColor.b = FEasing.Tween(fromShadowColor.b, toShadowColor.b, t, EasingType);
                newColor.lutColors.shadows = _newColor;
                _newColor.r = FEasing.Tween(fromMidtonesColor.r, toMidtonesColor.r, t, EasingType);
                _newColor.g = FEasing.Tween(fromMidtonesColor.g, toMidtonesColor.g, t, EasingType);
                _newColor.b = FEasing.Tween(fromMidtonesColor.b, toMidtonesColor.b, t, EasingType);
                newColor.lutColors.midtones = _newColor;
                _newColor.r = FEasing.Tween(fromHighlightsColor.r, toHighlighytsColor.r, t, EasingType);
                _newColor.g = FEasing.Tween(fromHighlightsColor.g, toHighlighytsColor.g, t, EasingType);
                _newColor.b = FEasing.Tween(fromHighlightsColor.b, toHighlighytsColor.b, t, EasingType);
                newColor.lutColors.highlights = _newColor;
                _CameraToneMapping.colorGrading = newColor;
            }
        }
    }
}
