﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

namespace Flux
{
    [FEvent("Script/Limit Break Done")]
    public class LimitBreakDone : FEvent
    {
        BattlerStateController theBattler;
        // Use this for initialization
        protected override void OnInit()
        {
            base.OnInit();
            theBattler = Owner.GetComponentInChildren<BattlerStateController>();
        }

        protected override void OnTrigger(float timeSinceTrigger)
        {
            base.OnTrigger(timeSinceTrigger);
            if (!theBattler)
            {
                theBattler = Owner.GetComponentInChildren<BattlerStateController>();
            }
            if (theBattler)
            {
                theBattler.DoneSkillEvent();
            }
        }
    }
}
