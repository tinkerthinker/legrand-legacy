﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

namespace Flux
{
    [FEvent("Script/Battler On Hit")]
    public class BattlerOnHit : FEvent
    {
        Battler theBattler;
        // Use this for initialization
        protected override void OnInit()
        {
            base.OnInit();
            theBattler = Owner.GetComponentInChildren<Battler>();

        }
        
        protected override void OnTrigger(float timeSinceTrigger)
        {
            base.OnTrigger(timeSinceTrigger);
            if(!theBattler)
            {
                theBattler = Owner.GetComponentInChildren<Battler>();
            }
            if (theBattler)
            {
                theBattler.HitTarget();
            }
        }
    }
}