﻿using UnityEngine;
using System.Collections;

namespace Flux
{
    [FEvent("Particles/Stop Particle(Legrand)")]
    public class StopParticlesLegrand : FEvent
    {
        ParticleSystem _Particle;

        protected override void OnTrigger(float timeSinceTrigger)
        {
            _Particle.enableEmission = false;
            _Particle.Stop();
        }

        protected override void OnInit()
        {
            _Particle = Owner.GetComponent<ParticleSystem>();
        }
        
    }
}