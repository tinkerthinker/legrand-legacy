﻿using UnityEngine;
using System.Collections;

namespace Flux
{
    [FEvent("Particles/Play Particle(Legrand)")]
    public class PlayParticlesL : FEvent
    {
        ParticleSystem _Particle;

        protected override void OnTrigger(float timeSinceTrigger)
        {
            _Particle.enableEmission = true;
            _Particle.Play();
        }

        protected override void OnInit()
        {
            _Particle = Owner.GetComponent<ParticleSystem>();
        }
        
    }
}