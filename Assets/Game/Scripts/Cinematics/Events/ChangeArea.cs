﻿using UnityEngine;
using System.Collections;
using WellFired;

[USequencerFriendlyName("MoveArea")]
[USequencerEvent("LegrandCinematic/ChangeArea")]
public class ChangeArea : USEventBase {
	public string ClusterDestination; 
	public string PortalDestination;
	
	public override void FireEvent()
	{
		StartCoroutine (MoveArea());
	}

	IEnumerator MoveArea()
	{
		yield return null;
		if(AreaController.Instance.IsSameArea(ClusterDestination))
		{
			AreaController.Instance.MoveToSubArea (AreaController.Instance.CurrPlayer, ClusterDestination, PortalDestination);
		}
		else
		{
			AreaController.Instance.ChangeArea (AreaController.Instance.CurrPlayer, ClusterDestination, PortalDestination);
		}
	}

	public override void ProcessEvent(float deltaTime)
	{
		
	}
}
