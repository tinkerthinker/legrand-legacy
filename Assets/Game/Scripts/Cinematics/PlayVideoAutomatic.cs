﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class PlayVideoAutomatic : MonoBehaviour {

	private MovieTexture _Movie;

	void Awake()
	{
		EventManager.Instance.AddListener<PauseEvent> (Pause);
		EventManager.Instance.AddListener<UnPauseEvent> (Resume);
	}

	void Start () {
		_Movie = ((MovieTexture)GetComponent<RawImage> ().mainTexture);
		_Movie.Play ();
		AudioSource aud = GetComponent<AudioSource>();
		aud.clip = _Movie.audioClip;
		aud.Play ();
	}

	void OnDestroy()
	{
		EventManager.Instance.RemoveListener<PauseEvent> (Pause);
		EventManager.Instance.RemoveListener<UnPauseEvent> (Resume);

		GetComponent<AudioSource>().Stop();
		_Movie.Stop ();
	}

	void Pause(PauseEvent e)
	{
		GetComponent<AudioSource>().Pause();
		_Movie.Pause();
	}

	void Resume(UnPauseEvent e)
	{
		GetComponent<AudioSource>().UnPause();
		_Movie.Play();
	}

}
