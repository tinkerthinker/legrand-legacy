﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ClipController : MonoBehaviour {
	private Clip[] _Clips;
	private int _Index;
	// Use this for initialization
	void Awake()
	{
		_Clips = GetComponentsInChildren<Clip> ();
		_Index = 0;
		for (int i = _Index + 1; i < _Clips.Length; i++)
			_Clips [i].gameObject.SetActive (false);
	}

	void Start () {
		EventManager.Instance.AddListener<NextClipEvent>(StartNextClip);
	}
	
	void OnDestroy()
	{
		if(EventManager.Instance != null)
			EventManager.Instance.RemoveListener<NextClipEvent>(StartNextClip);
	}

	void StartNextClip(NextClipEvent e)
	{
		if(_Index + 1 < _Clips.Length)
			_Clips [_Index+1].gameObject.SetActive (true);

		_Clips [_Index].gameObject.SetActive (false);
		_Index++;
	}

}
