﻿using UnityEngine;
using UnityEngine.Analytics;
using System.Collections.Generic;
using System.Collections;
using Flux;

public class SequencePlayerFlux : MonoBehaviour {
    public GameObject Sequencer;
    public float delay;

    public string CutsceneID;

	public bool ActivatePartyLeader = false;
	public bool ActivateCurrentBG = false;

	public string[] ActivateObjectPathsAtStart;

    private FSequence SequencerScript;

    private float _StartTime;

    void Start()
    {
		foreach (string objectPath in ActivateObjectPathsAtStart)
			GameObject.Find(objectPath).SetActive (true);

		if (ActivatePartyLeader && AreaController.Instance != null)
			AreaController.Instance.CurrPlayer.SetActive (true);
		if (ActivateCurrentBG && AreaController.Instance != null)
			AreaController.Instance.CurrPrefab.SetActive (true);

        FluxResume FRes = GetComponent<FluxResume>();
        GameObject SequencerGameObject = (GameObject)Instantiate(Sequencer);
        SequencerScript = SequencerGameObject.GetComponent<FSequence>();
        if (FRes && SequencerScript)
        {
            FRes.CurrentSequence = SequencerScript;
        }
        
        StartCoroutine(PlayScene(SequencerScript));
    }

    void OnDestroy()
    {
        float time_elapsed = LegrandBackend.Instance.InitialTime + Time.realtimeSinceStartup - LegrandBackend.Instance.WorldSceneStartTime;
        string startTimeString = ((int)time_elapsed / 3600).ToString("00") + "." + (((int)time_elapsed % 3600) / 60).ToString("00") + "." + (((int)time_elapsed % 3600) % 60).ToString("00");
        time_elapsed = Time.unscaledTime - _StartTime;
        string playtimeString = ((int)time_elapsed / 3600).ToString("00") + "." + (((int)time_elapsed % 3600) / 60).ToString("00") + "." + (((int)time_elapsed % 3600) % 60).ToString("00");
        Analytics.CustomEvent("CutsceneCompleted_"+CutsceneID+"_"+gameObject.name, new Dictionary<string, object>
        {
            { "cutscene_starttime", startTimeString },
            { "cutscene_playtime", playtimeString }
        }
        );
    }

    IEnumerator PlayScene(FSequence SequenceToPlay)
    {
        yield return new WaitForSeconds(delay);
        _StartTime = Time.unscaledTime;
        SequenceToPlay.Play();
    }
}
