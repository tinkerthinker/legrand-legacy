﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TeamUtility.IO;

public class CreditSceneController : MonoBehaviour {
    private bool _Transition = false;
    private bool _CanContinue = false;

	void Start ()
    {
        Time.timeScale = 1f;
        WwiseManager.Instance.StopAll();
        StartCoroutine(WaitToContinue());
    }

	void Update()
	{
        if(!_Transition && _CanContinue && InputManager.AnyInput())
        {
            _Transition = true;
            SceneManager.SwitchScene("TitleScene");
        }
    }

    IEnumerator WaitToContinue()
    {
        yield return new WaitForSeconds(5f);
        _CanContinue = true;
    }
}
