﻿ using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class WorldSceneController : MonoBehaviour {
	// Use this for initialization
	public GameObject PartyLeader;
	public Image WorldScreenFaderImage;

	void Start () {
//		SoundManager.StopAllSoundEffects ();
		LegrandBackend.Instance.WorldSceneStartTime = Time.realtimeSinceStartup;
		WwiseManager.Instance.StopBG ();
//		SoundManager.StopBackgroundMusic (true);

		BattlePoolingSystem.Instance.Init ();

		if (PartyManager.Instance.State.currPhase == (int)PartyManager.StoryPhase.Prolog) {
			WorldScreenFaderImage.color = Color.black;
			AreaController.Instance.ScreenFader.OnFadeIn += WaitWorldCreated;
			AreaController.Instance.StartWorld ();
		} 
		else 
		{
			AreaController.Instance.StartWorld ();
		}
	}

	void WaitWorldCreated()
	{
		AreaController.Instance.ScreenFader.OnFadeIn -= WaitWorldCreated;
		WorldSceneManager.TransWorldToSequence (AreaController.Instance.CurrPrefab, AreaController.Instance.CurrPlayer, "Chapter 1/2_FMV Part 1/FMV Part 1");
	}
}
