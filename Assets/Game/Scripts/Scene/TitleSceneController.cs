﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.IO;
using TeamUtility.IO;
using DG.Tweening;
using Legrand.GameSettings;

public class TitleSceneController : MonoBehaviour {
	// Use this for initialization

    public static TitleSceneController Instance;

	private bool _NewGameStarted = false;

	public Image BackGroundDark;
	public GameObject MainCanvas;

	public Button LoadGameButton;
	public Button SettingGameButton;

    public Image cover;
    public float darkAlpha;
    public float fadeSpeed;
    public float delayTime;

    void Awake()
    {
        Instance = this;
    }

    public void OpenSetting()
    {
        GameSettingHandler.Instance.Activate(OnGameSettingExit);
    }

    void OnGameSettingExit()
    {
        Activate();
        SettingGameButton.Select();
    }

    void OnEnable()
    {
        GameplayInputManager.Instance.AddChangeControllerListener();
    }

    void Start ()
    {
        TransToClear();
		WwiseManager.Instance.StopAll ();
		Time.timeScale = 1f;
		WwiseManager.Instance.PlayBG (WwiseManager.BGM.Main_Menu);
        GameplayInputManager.Instance.AddChangeControllerListener();
    }
    
    public void NewGame()
	{
		if (!_NewGameStarted) 
		{
			Time.timeScale = 1f;
			_NewGameStarted = true;
			WwiseManager.Instance.PlaySFX(WwiseManager.UIFX.Confirm);

			LoadingManager._Instance.OnStartLoading += LoadingReady;
			LoadingManager.StartLoading();
		}
	}

	private void LoadingReady()
	{
		LoadingManager._Instance.OnStartLoading -= LoadingReady;
		StartCoroutine(LegrandBackend.Instance.NewGame ());
		StartCoroutine (WaitUntilGameReady());
	}

	private IEnumerator WaitUntilGameReady()
	{
		while (!PartyManager.Instance._ReadyToPlay)
			yield return null;
		SceneManager.SwitchScene ("WorldScene");
	}

	public void Activate()
	{
		MainCanvas.SetActive (true);
	}

    public void ToDark()
    {
        Color color = new Color(0f, 0f, 0f, darkAlpha);
        cover.DOColor (color,fadeSpeed).SetDelay(delayTime);
    }
    public void ToClear()
    {
        cover.DOColor(Color.clear, fadeSpeed).SetDelay(delayTime);
    }
    public void TransToClear()
    {
        cover.color = Color.clear;
    }
}
