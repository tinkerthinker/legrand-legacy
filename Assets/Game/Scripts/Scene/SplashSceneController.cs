﻿using UnityEngine;
using System.Collections;

public class SplashSceneController : MonoBehaviour {
	public float SecondToChangeScene;

	void Start () {
		StartCoroutine (SwitchScene());
	}

	IEnumerator SwitchScene()
	{
		yield return new WaitForSeconds (SecondToChangeScene);
		SceneManager.SwitchScene ("TitleScene");
	}
}
