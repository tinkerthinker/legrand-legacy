﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BattleSelectorButton : MonoBehaviour {
    public StageSelector StageSelector;

    public string PartyID;
    public BattleAreaData.DungeonArea BattleArea;

    [SerializeField]
    private Text _PartyIdTextUI;

    public void Initiate(string partyID, BattleAreaData.DungeonArea battleArea, StageSelector stageSelector)
    {
        this.StageSelector = stageSelector;
        PartyID = partyID;
        BattleArea = battleArea; 
        _PartyIdTextUI.text = PartyID;
        gameObject.name = PartyID;
    }

	public void SelectLevel()
    {
        BattleAreaData area = new BattleAreaData();
        StageSelector.StartBattle(PartyID, area.GetBattleAreaName(BattleArea));
    }
}
