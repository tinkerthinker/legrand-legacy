﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class BattleSelectorGenerator : MonoBehaviour {
    public GameObject ButtonPrefab;
    public StageSelector StageSelector;

    void Awake()
    {
        bool first = true;
        for(int i = 0; i< LegrandBackend.Instance.LegrandDatabase.EncounterPartyData.Count;i++)
        {
            string id = LegrandBackend.Instance.LegrandDatabase.EncounterPartyData.Get(i)._ID;
            if (!id.Contains("RH") && !id.Contains("DK") && !id.Contains("DC")) continue;

            GameObject newButton = Instantiate<GameObject>(ButtonPrefab);
            newButton.transform.SetParent(this.transform, false);

            BattleAreaData.DungeonArea area = BattleAreaData.DungeonArea.Rahas;
            if(id.Contains("RH"))
            {
                area = BattleAreaData.DungeonArea.Rahas;
            }
            else if(id.Contains("DK"))
            {
                area = BattleAreaData.DungeonArea.Dringrs;
            }
            else if(id.Contains("DC"))
            {
                area = BattleAreaData.DungeonArea.Dunabad;
            }

            newButton.GetComponent<BattleSelectorButton>().Initiate(id, area, StageSelector);
            
            if(first)
            {
                EventSystem.current.firstSelectedGameObject = newButton;
                EventSystem.current.SetSelectedGameObject(newButton);
                first = false;
            }
        }
    }

	// Use this for initialization
	void Start () {
	    
	}
}
