﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RegObjDialogue : MonoBehaviour {
	public string RegisterName;

	private bool FirstRegister = true;
	void Start()
	{
		RegisterThisObject ();
	}

	void OnEnable()
	{
		if(!FirstRegister)
			ReRegister ();
	}

    void OnDestroy()
    {
        List<TalkingObject> currObj = new List<TalkingObject>();
        currObj.Add(new TalkingObject(RegisterName, gameObject));
        if(EventManager.Instance)
            EventManager.Instance.TriggerEvent(new RemoveObjectsDialogueEvent(currObj));
    }

    void RegisterThisObject()
	{
		List<TalkingObject> currObj = new List<TalkingObject> ();
		currObj.Add (new TalkingObject(RegisterName, gameObject));
        if (EventManager.Instance)
            EventManager.Instance.TriggerEvent (new AssignObjectsDialogueEvent (currObj));
		FirstRegister = false;
	}

	public void ReRegister()
	{
		RegisterThisObject ();
	}
}
