﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class TalkingObject {
	public string ID;
	public GameObject GObject;

	public TalkingObject (string iD, GameObject gObject)
	{
		this.ID = iD;
		this.GObject = gObject;
	}
}
