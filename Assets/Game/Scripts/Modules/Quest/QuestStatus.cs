﻿using UnityEngine;
using System.Collections;

    public enum QuestStatus
    {
        NotActive,
        OnProgress,
        Finished,
    }