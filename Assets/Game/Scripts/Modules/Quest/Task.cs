﻿using System.Collections.Generic;
    [System.Serializable]
    public class Task
    {
    public string TaskID;
    public string Description;
    public string Objective;
    public string PrevTaskID;
    public string NextTaskID;
    public string FinishOnDialog;
    public List<string> WorldEventGeneratedAfterFinish;
    public QuestEnums.Status Status;

        public Task()
        {
            TaskID = string.Empty;
            Description = string.Empty;
            Objective = string.Empty;
            PrevTaskID = string.Empty;
            NextTaskID = string.Empty;
            FinishOnDialog = string.Empty;
            WorldEventGeneratedAfterFinish = new List<string>();
            Status = QuestEnums.Status.NotStarted;
        }

    public Task Copy()
    {
        Task temp = new Task();
        temp.TaskID = TaskID;
        temp.Description = Description;
        temp.Objective = Objective;
        temp.PrevTaskID = PrevTaskID;
        temp.NextTaskID = NextTaskID;
        temp.FinishOnDialog = FinishOnDialog;
        temp.WorldEventGeneratedAfterFinish = new List<string>(WorldEventGeneratedAfterFinish);
        temp.Status = Status;
        return temp;
    }
}