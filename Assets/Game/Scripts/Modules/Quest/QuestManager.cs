﻿using UnityEngine;
using UnityEngine.Analytics;
using System.Collections.Generic;

public class QuestManager : MonoBehaviour {
    private static QuestManager _Instance;

    public static QuestManager Instance
    {
        get
        {
            if (_Instance == null)
            {
                _Instance = GameObject.FindObjectOfType<QuestManager>();
            }
            return _Instance;
        }
    }

    private Dictionary<string, Quest> _QuestDB;
    [SerializeField]
    private List<Quest> OnProgressQuests;
    [SerializeField]
    private List<Quest> FinishedQuests;
    [SerializeField]
    private List<Quest> FailedQuests;

    private Dictionary<string, object> _AnalyticQuery;
    void Awake()
    {
        _Instance = this;
        _AnalyticQuery = new Dictionary<string, object>();
        _QuestDB = new Dictionary<string, Quest>();
        for(int i=0;i<LegrandBackend.Instance.LegrandDatabase.QuestData.Count; i++)
        {
            Quest q = LegrandBackend.Instance.LegrandDatabase.QuestData.Get(i);
            q.ListTasks = new List<Task>(q.ListTasks);
            _QuestDB[q.QuestID] = q.Copy();
        }
    }

    public void GetUpdatedDataQuest(out List<SavedQuest> quests, out List<SavedTask> tasks)
    {
        quests = new List<SavedQuest>();
        tasks = new List<SavedTask>();
        foreach(Quest activeQuest in OnProgressQuests)
        {
            quests.Add(new SavedQuest(activeQuest.QuestID, activeQuest.Description, false, activeQuest.IsRead));
            foreach (Task task in activeQuest.ListTasks) tasks.Add(new SavedTask(task.TaskID, task.Status));
        }
        foreach (Quest finishedQuest in FinishedQuests)
        {
            quests.Add(new SavedQuest(finishedQuest.QuestID, finishedQuest.Description, true, finishedQuest.IsRead));
            foreach (Task task in finishedQuest.ListTasks) tasks.Add(new SavedTask(task.TaskID, task.Status));
        }
    }

    public void InitDataQuest(List<SavedQuest> quests, List<SavedTask> tasks)
    {
        OnProgressQuests = new List<Quest>();
        FinishedQuests = new List<Quest>();
        FailedQuests = new List<Quest>();

        for (int i=0;i<quests.Count;i++)
        {
            if (!_QuestDB.ContainsKey(quests[i].QuestID)) continue;

            Quest q = _QuestDB[quests[i].QuestID].Copy();
            if (tasks != null && tasks.Count > 0)
            {
                // Filter task by quest ID [-][2 digit chapter][2 digit questnumber] ex:MQ-0102
                List<SavedTask> qTasks = tasks.FindAll(x => x.TaskID.Contains(q.QuestID.Substring(2, 5)));
                foreach (SavedTask qTask in qTasks)
                {
                    // Find corrent task by id then change status
                    Task t = q.ListTasks.Find(x => x.TaskID == qTask.TaskID);
                    if (t != null)
                    {
                        t.Status = qTask.Status;
                        int enumint = (int)t.Status;
                        PartyManager.Instance.WorldInfo.NewFact("Task " + t.TaskID, enumint);
                    }
                }
            }

            if (!quests[i].IsFinished)
            {
                StartQuest(q);
            }
            else
            {
                FinishQuest(q, false);
            }

            if (quests[i].LastestDescription != null && quests[i].LastestDescription != "")
                q.Description = quests[i].LastestDescription;
        }
    }

    public void StartQuest(string questID)
    {   
        Quest q = _QuestDB[questID].Copy();

        _AnalyticQuery.Clear();
        float time_elapsed = LegrandBackend.Instance.InitialTime + Time.realtimeSinceStartup - LegrandBackend.Instance.WorldSceneStartTime;
        _AnalyticQuery["queststarttime"] = ((int)time_elapsed / 3600).ToString("00") + "." + (((int)time_elapsed % 3600) / 60).ToString("00") + "." + (((int)time_elapsed % 3600) % 60).ToString("00");
        Analytics.CustomEvent("QuestStarted_"+ questID, _AnalyticQuery);


        StartQuest(q);
    }

    public void StartQuest(Quest startedQuest)
    {
        if (startedQuest.ListTasks[0].Status == QuestEnums.Status.NotStarted)
        {
            if (!string.IsNullOrEmpty(startedQuest.ListTasks[0].Description))
                startedQuest.Description = startedQuest.ListTasks[0].Description;
            startedQuest.ListTasks[0].Status = QuestEnums.Status.OnGoing;
            PartyManager.Instance.WorldInfo.NewFact("Task "+ startedQuest.ListTasks[0].TaskID,1);
        }
        else
        {
            for (int i = 0; i < startedQuest.ListTasks.Count && startedQuest.ListTasks[i].Status != QuestEnums.Status.NotStarted; i++)
            {
                PartyManager.Instance.WorldInfo.NewFact("Task " + startedQuest.ListTasks[i].TaskID, (int)startedQuest.ListTasks[i].Status);
                if (!string.IsNullOrEmpty(startedQuest.ListTasks[i].Description))
                    startedQuest.Description = startedQuest.ListTasks[i].Description;
            }
        }

        if (!OnProgressQuests.Exists(x => x.QuestID == startedQuest.QuestID))
        {
            PartyManager.Instance.WorldInfo.NewFact("Quest " + startedQuest.QuestID, 1);
            OnProgressQuests.Add(startedQuest);
        }

        else
            Debug.LogError("Quest already exist");
    }

    public void FinishQuest(string questID)
    {
        Quest q = OnProgressQuests.Find(x => x.QuestID == questID);
        if(q != null)
           FinishQuest(q);
    }

    public void FinishQuest(Quest finishedQuest, bool goToNextQuest = true)
    {
        OnProgressQuests.Remove(finishedQuest);
        FinishedQuests.Add(finishedQuest);
        PartyManager.Instance.WorldInfo.NewFact("Quest " + finishedQuest.QuestID, 2);
        if (goToNextQuest && !string.IsNullOrEmpty(finishedQuest.NextQuestID))
        {
            _AnalyticQuery.Clear();
            float time_elapsed = LegrandBackend.Instance.InitialTime + Time.realtimeSinceStartup - LegrandBackend.Instance.WorldSceneStartTime;
            _AnalyticQuery["questfinishtime"] = ((int)time_elapsed / 3600).ToString("00") + "." + (((int)time_elapsed % 3600) / 60).ToString("00") + "." + (((int)time_elapsed % 3600) % 60).ToString("00"); 
            Analytics.CustomEvent("QuestFinished_"+ finishedQuest.QuestID, _AnalyticQuery);

            StartQuest(finishedQuest.NextQuestID);
        }
    }

    public List<Quest> GetFinishQuest()
    {
        return FinishedQuests;
    }

    public Quest GetQuestByKey(string id)
    {
        Quest q = OnProgressQuests.Find(x => x.QuestID == id);
        if(q == null) q = FinishedQuests.Find(x => x.QuestID == id);
        return q;
    }

    public List<Quest> GetActiveQuest()
    {
        return OnProgressQuests;
    }

    public void FinishTask(string taskID)
    {
        Quest quest = OnProgressQuests.Find(x => x.QuestID.Contains(taskID.Substring(2, 5)));
        Task t = quest.ListTasks.Find(x => x.TaskID == taskID);
        FinishTask(quest, t);
    }

    public void FinishTask(Quest quest, Task finishedTask)
    {
        finishedTask.Status = QuestEnums.Status.Done;
        PartyManager.Instance.WorldInfo.NewFact("Task " + finishedTask.TaskID, 2);
        for (int i=0;i<finishedTask.WorldEventGeneratedAfterFinish.Count;i++)
            PartyManager.Instance.StoryProgression.ActiveEventID.Add(finishedTask.WorldEventGeneratedAfterFinish[i]);

        if (!string.IsNullOrEmpty(finishedTask.NextTaskID))
        {
            Task nextTask = quest.ListTasks.Find(x => x.TaskID == finishedTask.NextTaskID);
            if(!string.IsNullOrEmpty(nextTask.Description))
                quest.Description = nextTask.Description;
            nextTask.Status = QuestEnums.Status.OnGoing;
            PartyManager.Instance.WorldInfo.NewFact("Task " + nextTask.TaskID, 1);
        }
        else
        {
            FinishQuest(quest);
        }
    }

    public void CheckTaskDialog(string dialogName)
    {
        List<Task> finishedTasks = new List<Task>();
        List<Quest> finishedTaskQuests = new List<Quest>();
        foreach (Quest q in OnProgressQuests)
        {
            Task finishedTask = q.ListTasks.Find(x => x.FinishOnDialog == dialogName);
            if (finishedTask != null)
            {
                finishedTaskQuests.Add(q);
                finishedTasks.Add(finishedTask);
            }
        }
        for(int i=0;i<finishedTaskQuests.Count;i++)
            FinishTask(finishedTaskQuests[i], finishedTasks[i]);
    }

    public Task GetTaskByKey(string TaskID)
    {
        Quest q = OnProgressQuests.Find(x => x.QuestID.Contains(TaskID.Substring(2, 5)));
        Task searchedTask = null;
        if (q != null)
        {
            searchedTask = q.ListTasks.Find(x => x.TaskID == TaskID);
        }
        return searchedTask;
    }
}
