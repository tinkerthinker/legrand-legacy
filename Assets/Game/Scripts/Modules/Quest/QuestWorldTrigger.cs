﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

[System.Serializable]
public class QuestWorldEvents
{
    public string WorldTriggerID;
    public string WorldEvent;

	public QuestWorldEvents()
	{
		WorldTriggerID = "";
		WorldEvent = "";
	}

	public QuestWorldEvents(string triggerID, string worldEvent)
	{
		WorldTriggerID = triggerID;
		WorldEvent = worldEvent;
	}
}
