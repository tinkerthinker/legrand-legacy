﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;


/// <summary>
/// Saved quest. This class is used only to save current progress of quest.
/// so we got only tiny saved data. 
/// the complete data is in master databases.
/// </summary>
[System.Serializable]
public class SavedQuest {
	//key
	public string QuestID;
    //updated data
    public string LastestDescription;
	public bool IsFinished;
	public bool IsRead;

    public SavedQuest()
    {
        QuestID = "";
        IsFinished = false;
        IsRead = false;
        LastestDescription = "";
    }

    public SavedQuest(string questID)
    {
        QuestID = questID;
        LastestDescription = "";
        IsFinished = false;
        IsRead = false;
    }

    public SavedQuest(string questID, string desc, bool isFinished, bool isRead)
    {
        QuestID = questID;
        LastestDescription = desc;
        IsFinished = isFinished;
        IsRead = isRead;
    }
}