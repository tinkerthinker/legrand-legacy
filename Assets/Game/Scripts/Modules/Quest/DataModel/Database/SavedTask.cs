﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class SavedTask {
	public string TaskID;
	public QuestEnums.Status Status;

    public SavedTask()
    {
        TaskID = "";
        Status = QuestEnums.Status.NotStarted;
    }

    public SavedTask(string taskID, QuestEnums.Status status)
    {
        TaskID = taskID;
        Status = status;
    }
}