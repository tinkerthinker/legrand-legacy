﻿using UnityEngine;
using System.Collections.Generic;
using System;

[System.Serializable]
public class Quest
{
    public string QuestID;
    public string NextQuestID;
    public string PrevQuestID;
    public string Description;
    public string QuestGiver;
    public string QuestName;
    public QuestEnums.StoryType QuestType;
    public Sprite QuestPicture;
    public List<QuestReward> Rewards;
    public List<Task> ListTasks;
    public bool IsRead;

    public Quest()
    {
        QuestID = string.Empty;
        NextQuestID = string.Empty;
        PrevQuestID = string.Empty;
        Description = string.Empty;
        QuestGiver = string.Empty;
        QuestName = string.Empty;
        QuestPicture = null;
        ListTasks = new List<Task>();
        IsRead = false;
        Rewards = new List<QuestReward>();
        QuestType = QuestEnums.StoryType.Main;
    }

    public Quest Copy()
    {
        Quest temp = new Quest();
        temp.QuestID = this.QuestID;
        temp.NextQuestID = NextQuestID;
        temp.PrevQuestID = PrevQuestID;
        temp.Description = Description;
        temp.QuestGiver = QuestGiver;
        temp.QuestName = QuestName;
        temp.QuestPicture = QuestPicture;
        for(int i=0;i<ListTasks.Count;i++)
        {
            Task t = new Task();
            t = ListTasks[i].Copy();
            temp.ListTasks.Add(t);
        }
        temp.Rewards = new List<QuestReward>(Rewards);
        temp.IsRead = IsRead;
        temp.QuestType = QuestType;
        return temp;
    }
}

[System.Serializable]
public class QuestReward
{
    public QuestEnums.RewardType RewardType;
    public string rewardString;
    public string rewardNumber;
}
