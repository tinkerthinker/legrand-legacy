﻿using UnityEngine;
using System.Collections;

public class QuestEnums
{
    public enum StoryType
    {
        Main,
        Side
    }

    public enum Status
    {
        NotStarted,
        OnGoing,
        Done
    }

    public enum RewardType
    {
        Item,
        Money,
        NPC
    }
}
