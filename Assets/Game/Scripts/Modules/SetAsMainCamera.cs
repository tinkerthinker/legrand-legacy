﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Camera))]
public class SetAsMainCamera : MonoBehaviour {
	[Header("Only Fill This if want to set another object to be the main Camera instead of this Camera")]
	public Camera MainCamera;

	void OnEnable()
	{
		if(MainCamera)
			GlobalGameStatus.Instance.MainCamera = MainCamera;
		else
			GlobalGameStatus.Instance.MainCamera = GetComponent<Camera> ();
	}
}
