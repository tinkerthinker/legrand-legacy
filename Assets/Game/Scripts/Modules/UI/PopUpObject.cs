﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class PopUpObject : MonoBehaviour {
	public enum ClosePopUpType
	{
		Disable,
		Destroy
	}

	public bool Choice = true;
	public bool IsStopTime = false;
	public ClosePopUpType CloseType = ClosePopUpType.Disable;

	private GameObject _LastObj;
	private float currTimeScale;

    private bool _LastStateMarkObj;

    private Button _LastButton;

    private bool _IsHidden = false;
    public bool IsHidden
    {
        get { return _IsHidden; }
    }

    void Awake()
    {
        PopUpUI.OnHidePopUp += HidePopUpObject;
    }

    void OnDestroy()
    {
        PopUpUI.OnHidePopUp -= HidePopUpObject;
    }

	public void OnEnable()
	{
		if (IsStopTime) {
			currTimeScale = Time.timeScale;
			Time.timeScale = 0f;
		}

		PopUpUI.OnEndPopUp += ClosePopUpObject;

        if (GlobalGameStatus.Instance)
            GlobalGameStatus.Instance.StateStatus[(int)GlobalGameStatus.GameState.PopUp] = true;

        if (EventSystem.current) {
			if(EventSystem.current.currentSelectedGameObject)
				_LastObj = EventSystem.current.currentSelectedGameObject;
            if (SelectedGameObjectModule.current)
            {
                _LastStateMarkObj = SelectedGameObjectModule.current.IsMarkVisible;
                SelectedGameObjectModule.current.HideMarkObject();
            }
		}
	}

	public void OnDisable()
	{
		PopUpUI.OnEndPopUp -= ClosePopUpObject;

        if (SelectedGameObjectModule.current)
        {
            if(_LastStateMarkObj == true)
                SelectedGameObjectModule.current.ShowMarkObject();
            else
                SelectedGameObjectModule.current.HideMarkObject();
        }
	}

    void HidePopUpObject(bool isHide)
    {
        if(isHide)
        {
            if (!gameObject.activeInHierarchy)
                return;
            _LastButton = EventSystem.current.currentSelectedGameObject.GetComponent<Button>();
            gameObject.SetActive(false);
            _IsHidden = true;
        }
        else
        {
            if (!_IsHidden)
                return;
            gameObject.SetActive(true);
            if (_LastButton)
                _LastButton.Select();
            _IsHidden = false;
        }
    }

	void OnEndProcess()
	{
		PopUpUI.CloseAllPopUp (Choice);
	}

	protected virtual void ClosePopUpObject(bool choice=true)
	{
		this.Choice = choice;

        if (GlobalGameStatus.Instance)
            //			GlobalGameStatus.Instance.CurrentState = GlobalGameStatus.GameState.Normal;
            GlobalGameStatus.Instance.StateStatus[(int)GlobalGameStatus.GameState.PopUp] = false;
		
		if ( _LastObj != null || !Object.Equals(_LastObj, null) ) {
			EventSystem.current.SetSelectedGameObject(_LastObj);
		}
        _LastObj = null;

		if(IsStopTime)
			Time.timeScale = currTimeScale;

		switch (CloseType) {
		case ClosePopUpType.Disable:
			gameObject.SetActive (false);
			break;
		case ClosePopUpType.Destroy:
			GameObject.Destroy(this.gameObject);
			break;
		}
			
	}

	/// <summary>
	/// Call to Closes all pop up, normally there are only one pop up in a time, so use this method to close all pop up.
	/// </summary>
	/// <param name="choice">If set to <c>true</c> choice.</param>
	public void ClosePopUp(bool choice=true)
	{
		this.Choice = choice;
		OnEndProcess ();
	}

	public void DisablePopUp(bool choice=true)
	{
		this.Choice = choice;

		OnEndProcess ();
		if(IsStopTime)
			Time.timeScale = currTimeScale;

		gameObject.SetActive (false);
	}

	public void DestroyPopUp(bool choice=true)
	{
		this.Choice = choice;

		OnEndProcess ();
		if(IsStopTime)
			Time.timeScale = currTimeScale;
		GameObject.Destroy(this.gameObject);
	}

	/// <summary>
	/// Destroy this pop up only without sending onEndPopUp event, normally you should use ClosePopUp method to Close Pop Up.
	/// </summary>
	public void DisableThisPopUp(bool choice=true)
	{
		this.Choice = choice;
		
		if(GlobalGameStatus.Instance)
            GlobalGameStatus.Instance.StateStatus[(int)GlobalGameStatus.GameState.PopUp] = false;
		
		if (_LastObj != null) {
			if(_LastObj.GetComponent<Button>() && EventSystem.current.currentSelectedGameObject != _LastObj)
				_LastObj.GetComponent<Button>().Select();
			else
				EventSystem.current.SetSelectedGameObject(_LastObj);
			_LastObj = null;
		}
		
		if(IsStopTime)
			Time.timeScale = currTimeScale;
		
		if(IsStopTime)
			Time.timeScale = currTimeScale;
		
		gameObject.SetActive (false);
	}

	/// <summary>
	/// Destroy this pop up only without sending onEndPopUp event , normally you should use ClosePopUp method to Close Pop Up. mainly use for dilaguePopUp shouting npc.
	/// </summary>
	public void DestroyThisPopUp(bool choice=true)
	{
		this.Choice = choice;
		
		if(GlobalGameStatus.Instance)
            GlobalGameStatus.Instance.StateStatus[(int)GlobalGameStatus.GameState.PopUp] = false;

            if (_LastObj != null) {
			if(_LastObj.GetComponent<Button>() && EventSystem.current.currentSelectedGameObject != _LastObj)
				_LastObj.GetComponent<Button>().Select();
			else
				EventSystem.current.SetSelectedGameObject(_LastObj);
			_LastObj = null;
		}

		if(IsStopTime)
			Time.timeScale = currTimeScale;
		
		if(IsStopTime)
			Time.timeScale = currTimeScale;
		GameObject.Destroy(this.gameObject);
	}

    public void SetCancelObject(GameObject target)
    {
        _LastObj = target;
    }
}
