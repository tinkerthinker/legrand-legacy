using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using Legrand.core;
using TeamUtility.IO;

public class SetAmountItemAssign : MonoBehaviour{
	int amount = 0;
	public int assignAmount{
		get{return amount;}
	}
	MainCharacter selectedChar;
	public int maxStack;
	public Text amountText;
	
	void Start () {
	}

	void OnEnable()
	{
		Reset ();
		if(PauseUIController.Instance != null)
			selectedChar = PartyManager.Instance.GetCharacter (PauseUIController.Instance.selectedCharId);
		//maxStack = 3;
	}

	void OnDisable()
	{
		Reset ();
	}

	public void Reset()
	{
		amount = 1;
		amountText.text = amount.ToString ();
	}

	void Update () {
		if (this.isActiveAndEnabled) {
			if (InputManager.anyKey || InputManager.AnyInput())
			{
				TeamUtility.IO.StandaloneInputModule module = EventSystem.current.GetComponent<TeamUtility.IO.StandaloneInputModule>();
				bool allow = module.allowMoveEvent;
				float inputH = LegrandUtility.GetAxisRawHorizontal ();

				if (inputH > 0.5f && allow) {
					amount++;
					UpdateAmount();
					
				} else if (inputH < -0.5f && allow) {
					amount--;
					UpdateAmount();
				}

			}


		}
	}

	void UpdateAmount()
	{
		amount = Mathf.Clamp(amount, 1,maxStack);
		//amountSetComponent [1].text = amount.ToString ();
		amountText.text = amount.ToString ();
	}

}
