using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using Legrand.core;

public class ItemInventoryData : MonoBehaviour, IObjectData
{
	private ItemSlot _ItemSlotData;

	public ItemSlot ItemSlotData{
		set{_ItemSlotData = value;
			if(!SetAsNullData) SetItemData();}
		get{return _ItemSlotData;}
	}

    public bool SetAsNullData = false;
	public Image imageItem;
	public Text textItemName;
	public Text textAmountItem;
	public Text textItemWeight;
	public Text textItemDescription;
	public Text textAOE;

	private string _NameItem;

	// Use this for initialization
	void Start () {
		if (GetComponent<Button> ())
			GetComponent<Button> ().onClick.AddListener (() => OnClickHandler ());
	}

	void OnClickHandler()
	{
        //SendItemData ();
	}

	public void SendItemData()
	{
		EventManager.Instance.TriggerEvent (new SelectItemEvent (_ItemSlotData.ItemMaster));
	}

	void SetItemData()
	{
		if (_ItemSlotData == null) {
			if(imageItem != null)
				imageItem.sprite = null;
			if (textAmountItem != null)
				textAmountItem.text = "x";
			if (textItemName != null)
				textItemName.text = "";

			return;
		}

		_NameItem = _ItemSlotData.ItemMaster.Name;

		gameObject.name = _NameItem;

		if(imageItem != null)
			imageItem.sprite = _ItemSlotData.ItemMaster.Icon;

		if (textAmountItem != null)
		{
			textAmountItem.text = "x"+_ItemSlotData.Stack;
		}
		if (textItemName != null)
			textItemName.text = _NameItem;
		if (textAOE != null) {
			if (_ItemSlotData.ItemMaster.ItemType == Item.Type.Consumable && 
				( ((Consumable)_ItemSlotData.ItemMaster).ConsumableType == Consumable.ConsumeType.Healing || 
					((Consumable)_ItemSlotData.ItemMaster).ConsumableType == Consumable.ConsumeType.Magic) ) {
				textAOE.text = ((Consumable)_ItemSlotData.ItemMaster).Target.ToString ().Substring (0, 1);
			} else
				textAOE.text = "";
		}
	}

	public void UpdateData()
	{
		SetItemData ();
	}

	public void OnSelect(BaseEventData eventData)
	{
        if (_ItemSlotData == null || SetAsNullData)
        {
            EventManager.Instance.TriggerEvent(new InspectItemEvent(null));
            return;
        }

		if (textItemDescription != null)
		{
			textItemDescription.text = _ItemSlotData.ItemMaster.Description;
		}
		if (textItemName != null)
			textItemName.text = _NameItem;
		if (textItemWeight != null)
			textItemWeight.text = _ItemSlotData.ItemMaster.Weight.ToString();

		EventManager.Instance.TriggerEvent (new InspectItemEvent (_ItemSlotData.ItemMaster));

		if(PauseUIController.Instance)
			PauseUIController.Instance.currInventoryItem = _ItemSlotData.ItemMaster;
	}

    public void SetData(object obj)
    {
        if (!(obj is ItemSlot) || SetAsNullData)
            return;

        _ItemSlotData = (ItemSlot)obj;

        if (_ItemSlotData == null)
        {
            if (imageItem != null)
                imageItem.sprite = null;
            if (textAmountItem != null)
                textAmountItem.text = "x";
            if (textItemName != null)
                textItemName.text = "";

            return;
        }
        
        _NameItem = _ItemSlotData.ItemMaster.Name;

        gameObject.name = _NameItem;

        if (imageItem != null)
            imageItem.sprite = _ItemSlotData.ItemMaster.Icon;
        
        if (textAmountItem != null)
        {
            textAmountItem.text = "x" + _ItemSlotData.Stack;
        }
        if (textItemName != null)
            textItemName.text = _NameItem;
        if (textAOE != null)
        {
            if (_ItemSlotData.ItemMaster.ItemType == Item.Type.Consumable &&
                (((Consumable)_ItemSlotData.ItemMaster).ConsumableType == Consumable.ConsumeType.Healing ||
                    ((Consumable)_ItemSlotData.ItemMaster).ConsumableType == Consumable.ConsumeType.Magic))
            {
                textAOE.text = ((Consumable)_ItemSlotData.ItemMaster).Target.ToString().Substring(0, 1);
            }
            else
                textAOE.text = "";
        }
    }

    public object GetData()
    {
        return _ItemSlotData;
    }

    public void RefreshData()
    {
        SetData(_ItemSlotData);
    }
}

