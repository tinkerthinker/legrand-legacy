using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;
using Legrand.core;

public class AssignItemToList : MonoBehaviour, ICancelHandler, ISelectHandler, IDeselectHandler {

	private Item itemData;
	private string selectedCharId;
	[System.NonSerialized]
	public bool wannaAssign = false;
	public GameObject amountSetObject;
	public Text amountText;

	private Button _targetButton;
	public Button targetButton
	{
		get{ return _targetButton;}
		set{ _targetButton = value;}
	}

	// Use this for initialization
	void Start () {
		GetComponent<Button> ().onClick.AddListener (() => OnClickHandler()); // ketika klik maka pilih amount dulu, baru di assign
		itemData = GetComponent<ItemInventoryData> ().ItemSlotData.ItemMaster;
	}

	void OnClickHandler()
	{
		//NEW CODE
		AssignItem (amountSetObject.GetComponent<SetAmountItemAssign>().assignAmount);

		wannaAssign = false;
		ChangeInteractableObject.EnableAllButtonInParent(transform.parent.gameObject);

		amountSetObject.SetActive(false);

		/// old code
		/*
		itemData = GetComponent<ItemInventoryData> ().itemData;

		if (wannaAssign) {
			AssignItem (amountSetObject.GetComponent<SetAmountItemAssign>().assignAmount);

			//_targetButton.GetComponent<ItemSlotSelect> ().SetItemSlot ();
			//_targetButton.Select ();

			amountSetObject.SetActive(false);
			wannaAssign = false;
			ChangeInteractableObject.EnableAllButtonInParent(transform.parent.gameObject);
			GetComponent<AssignCommandMagicItemToChar>().AssignCommandItem();
		} else {
			//tampilkan anak panah untuk memilih amount item
			//...............................
			amountSetObject.SetActive(true);
			Vector3 newPos = this.transform.position;
			newPos.x += 150;
			newPos.y -= GetComponent<RectTransform> ().rect.height/4;
			amountSetObject.transform.position = newPos;

			//maxstack = jumlah terkecil dari max stack nya character atau jumlah stack item yang ada di inventory
			//int maxStackInventory = int.Parse( GetComponentsInChildren<Text>()[1].text.Substring(1));
			int maxStackInventory = 0;
			foreach(ItemSlot itemSlot in PartyManager.Instance.Inventory.Items[(int)itemData.ItemType])
			{
				if(itemSlot.ItemMaster._ID == itemData._ID)
				{
					maxStackInventory = itemSlot.Stack;
				}
			}

			//belum pakai ini karena maxstackcharslot belum ada yang ngehandle
			//int maxStackCharSlot;
			//amountSetObject.GetComponent<SetAmountItemAssign>().maxStack = Mathf.Min(maxStackCharSlot, maxStackInventory);
			amountSetObject.GetComponent<SetAmountItemAssign>().maxStack = maxStackInventory;
			wannaAssign = true;
			ChangeInteractableObject.DisableAllButtonInParentExcept(transform.parent.gameObject, gameObject);
		}*/
	}

	#region ISelectHandler implementation

	public void OnSelect (BaseEventData eventData)
	{
		MainCharacter selectedChar = PartyManager.Instance.GetCharacter (PauseUIController.Instance.selectedCharId);
		CommandPos panelPos = _targetButton.GetComponent<CommandButtonSet> ().panelPosition;
		CommandPos slotPos = _targetButton.GetComponent<CommandButtonSet> ().slotPosition;

		//tampilkan anak panah untuk memilih amount item
		//...............................
		amountSetObject.SetActive(true);
//		Vector3 newPos = this.transform.position;
//		newPos.x += GetComponent<RectTransform> ().rect.width *0.75f ;
//		newPos.y -= GetComponent<RectTransform> ().rect.height/4;
		Vector3 newPos = amountText.transform.position;
		amountSetObject.transform.position = newPos;
		
		//maxstack = jumlah terkecil dari max stack nya character atau jumlah stack item yang ada di inventory
		//int maxStackInventory = int.Parse( GetComponentsInChildren<Text>()[1].text.Substring(1));
		int maxStackInventory = 0;
		foreach(ItemSlot itemSlot in PartyManager.Instance.Inventory.Items[(int)itemData.ItemType])
		{
			if(itemSlot.ItemMaster._ID == itemData._ID)
			{
				maxStackInventory = itemSlot.Stack;
			}
		}
		
		//belum pakai ini karena maxstackcharslot belum ada yang ngehandle
		int maxStackCharSlot;
		maxStackCharSlot = selectedChar.MaxItem;
		maxStackCharSlot -= selectedChar.Items[selectedChar.GetCommand(panelPos, slotPos).CommandValue].Stack;
		maxStackCharSlot = Mathf.Clamp (maxStackCharSlot, 0, selectedChar.MaxItem);

		amountSetObject.GetComponent<SetAmountItemAssign>().maxStack = Mathf.Min(maxStackCharSlot, maxStackInventory);
		//amountSetObject.GetComponent<SetAmountItemAssign>().maxStack = maxStackInventory;
		amountSetObject.GetComponent<SetAmountItemAssign>().Reset();
		wannaAssign = true;
		//ChangeInteractableObject.DisableAllButtonInParentExcept(transform.parent.gameObject, gameObject);
	}

	#endregion

	void AssignItem(int amount)
	{
		selectedCharId = PauseUIController.Instance.selectedCharId;
		MainCharacter selectedChar = PartyManager.Instance.GetCharacter (selectedCharId);
		
		//ChangeInteractableObject.EnableAllButtonInParent (ChangeTabPanel.instance.panelItem);

		int indexItemSlot;
		if (_targetButton.GetComponent<CommandButtonSet> () == null)
			return;

		CommandPos panelPos = _targetButton.GetComponent<CommandButtonSet> ().panelPosition;
		CommandPos slotPos = _targetButton.GetComponent<CommandButtonSet> ().slotPosition;

		indexItemSlot = selectedChar.GetCommand(panelPos, slotPos).CommandValue;

		int index = 0;
		//selectedChar.ReturnItem(itemData._ID);

		foreach (ItemSlot itemSlot in selectedChar.Items)
		{
			if(itemSlot.ItemMaster != null && itemSlot.ItemMaster._ID == itemData._ID && index != indexItemSlot)
			{
				//hapus data dari slot sebelumnya dulu, dikembalikan ke inventory item nya
				selectedChar.ReturnItem(index);
				break;
			}
			index++;
		}

		selectedChar.AddItems (itemData, indexItemSlot);	// maybe need a return value to determine if add item success ????
		selectedChar.GetCommand(panelPos, slotPos).CommandType = (int)CommandTypes.Item;
		PartyManager.Instance.Inventory.DeleteItemsByItemID (itemData._ID, amount);

		//ChangeTabPanel.instance.panelItem.GetComponent<PanelItemChildren>().panelItemList.SetActive(false);
	}

	public void OnCancel(BaseEventData eventData)
	{
		//NEW CODE
		amountSetObject.SetActive(false);
		//wannaAssign = false;
		
		ChangeInteractableObject.EnableAllButtonInParent(transform.parent.gameObject);


		//OLD CODE
		/*
		if (!wannaAssign) {
			GetComponent<AssignCommandMagicItemToChar>().FocusToTargetButton();
		} else {
			amountSetObject.SetActive(false);
			wannaAssign = false;

			ChangeInteractableObject.EnableAllButtonInParent(transform.parent.gameObject);
			//ChangeInteractableObject.EnableAllButtonInParent(ChangeTabPanel.instance.panelItem.GetComponent<PanelItemChildren>().panelItemListList.gameObject);
		}*/
	}

	public void OnDeselect(BaseEventData eventData)
	{
		wannaAssign = false;
	}
}
