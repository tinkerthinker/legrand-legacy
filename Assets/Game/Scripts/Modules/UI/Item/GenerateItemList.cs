using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;

public class GenerateItemList : MonoBehaviour {
	int totalItemCount;

	private List<Button> listItemButtons=new List<Button>();
	[System.NonSerialized]
	public bool generateAll=false;

	private float lastRow=1;
	private float lastCol=1;

	private int currentRowIndex=1;
	private int currentColIndex=1;

	public Button itemListButton;

	[SerializeField]
	private GameObject amountSetObject;

	private Button _targetItemButton;
	public Button targetItemButton
	{
		get{return _targetItemButton;}
		set{_targetItemButton = value;}
	}

	public int seenButton;
	private int numberOfStep;
	public Scrollbar scrollBarItemList;
	[SerializeField]
	private Text panelInfoText;

	// Use this for initialization
	void Start () {
		//pre-memory : ambil dari database ke array, proses array dibawah

	}

	void OnEnable()
	{
		LoadItemList ();
	}

	void GenerateItemSlot()
	{
		/*for (int i=0; i<PartyManager.Instance.Inventory.Items.Length; i++) {
			GenerateInventoryItemButton((Item.Type)i);
		}*/
		GenerateItemSlotButton();
		
		numberOfStep = listItemButtons.Count - (seenButton-1);
		
		foreach (var itemButton in listItemButtons) {
			itemButton.transform.SetParent(this.transform);
			itemButton.GetComponent<RectTransform> ().localScale = new Vector3(1,1,1);
			
			itemButton.GetComponent<ScrollUpDown>().seenRow = seenButton;
			itemButton.GetComponent<ScrollUpDown>().scrollBar = scrollBarItemList;
			itemButton.GetComponent<ScrollUpDown>().numberOfStep = numberOfStep;
		}
	}
	
	void GenerateItemSlotButton()
	{
		Button newInventoryItemButton;
		foreach (var itemSlot in PartyManager.Instance.Inventory.Items [(int)Item.Type.Consumable])
		{
			Item item = itemSlot.ItemMaster;
			Consumable temp = (Consumable)item;
			if (temp.ConsumableType == Consumable.ConsumeType.Healing || temp.ConsumableType == Consumable.ConsumeType.Magic)
			{
				newInventoryItemButton = Instantiate (itemListButton) as Button;
				newInventoryItemButton.name = item.Name;
				
				newInventoryItemButton.GetComponent<ItemInventoryData> ().ItemSlotData = itemSlot;
				newInventoryItemButton.GetComponent<ItemInventoryData> ().textItemDescription = panelInfoText;

				newInventoryItemButton.GetComponent<AssignItemToList> ().targetButton = _targetItemButton;
				newInventoryItemButton.GetComponent<AssignItemToList> ().amountSetObject = amountSetObject;
				
				listItemButtons.Add (newInventoryItemButton);
			}
		}
	}

	void PutItemListButton(int indexItemList)
	{
		float x=0;
		float y=0;
		string nameItemListButton = "ItemListButton"+currentRowIndex+currentColIndex;
		Consumable temp = (Consumable)PartyManager.Instance.Inventory.Items [(int)Item.Type.Consumable] [indexItemList].ItemMaster;

		// set x,y position next button
		if (temp.ConsumableType == Consumable.ConsumeType.Healing || temp.ConsumableType == Consumable.ConsumeType.Magic) {
			if (currentColIndex == 1)
				x = 90;
			else
				x = lastCol + 350;

			if (currentRowIndex == 1)
				y = -65;
			else if (currentRowIndex > 1 && currentColIndex == 1)
				y = lastRow - 105;
			else
				y = lastRow;
			//Instantiate (itemListButton, new Vector3 (x, y, 0.0f), transform.rotation);
			lastCol = x;
			lastRow = y;
			if (currentColIndex == 3) {
				currentColIndex = 1;
				currentRowIndex++;
			} else
				currentColIndex++;


			//Button newItemListButton = Instantiate (itemListButton, new Vector3 (x, y, 0.0f), transform.rotation) as Button;
			Button newItemListButton = Instantiate (itemListButton) as Button;
			newItemListButton.transform.SetParent (this.transform);
			newItemListButton.name = nameItemListButton;
			//newItemListButton.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (x, y);
			newItemListButton.GetComponent<RectTransform> ().localScale = new Vector3 (1, 1, 1);

			//newItemListButton.GetComponentsInChildren<Image> ()[1].sprite = DatabaseImageSprite.instance.GetSpriteItemByName ("pot 1");
			newItemListButton.GetComponentsInChildren<Image> ()[1].sprite = DatabaseImageSprite.Instance.GetSpriteItemConsumableByType(temp.ConsumableType);

			Text[] itemListTexts = newItemListButton.GetComponentsInChildren<Text> ();
			//itemListTexts [0].text = listItemName[indexItemList] ;
			itemListTexts [0].text = PartyManager.Instance.Inventory.Items [(int)Item.Type.Consumable] [indexItemList].ItemMaster.Name;
			//itemListTexts [1].text = "x"+listItemAmount[indexItemList];
			itemListTexts [1].text = "x" + PartyManager.Instance.Inventory.Items [(int)Item.Type.Consumable] [indexItemList].Stack;

			if(_targetItemButton == null)
				Debug.Log("NULL");
			newItemListButton.GetComponent<AssignItemToList> ().targetButton = _targetItemButton;
			newItemListButton.GetComponent<AssignItemToList> ().amountSetObject = amountSetObject;
			newItemListButton.GetComponent<ItemInventoryData> ().ItemSlotData = PartyManager.Instance.Inventory.Items [(int)Item.Type.Consumable] [indexItemList];
			newItemListButton.GetComponent<ItemInventoryData> ().textItemDescription = panelInfoText;
			listItemButtons.Add (newItemListButton);
		}
		//Resources.Load(
		//newItemListButton.GetComponent<RectTransform> ().position.y = y;
		//newItemListButton = Instantiate (itemListButton, new Vector3 (x, y, 0.0f), transform.rotation) as Button;
		//newItemListButton.ge
	}

	void RemoveItemList()
	{
		foreach (Button button in listItemButtons) {
			Destroy(button.gameObject);
		}
		listItemButtons.RemoveAll ((Button obj) => obj);
		currentRowIndex=1;
		currentColIndex=1;
	}

	public void LoadItemList()
	{
		//Debug.Log ("loaditemlist "+_targetItemButton.name);
		
		totalItemCount = PartyManager.Instance.Inventory.Items[(int)Item.Type.Consumable].Count;

		//ChangeInteractableObject.DisableAllButtonInParent(ChangeTabPanel.instance.panelItem);
		//ChangeInteractableObject.DisableAllButtonInParent(PauseUIController.instance.panelCharacter);
		RemoveItemList ();
		GenerateItemSlot();
		/*
		for (int i=0; i<totalItemCount; i++) {
			//if (listItemAmount [i] > 0)
				PutItemListButton (i);
		}*/
		if(listItemButtons.Count > 0)
			listItemButtons[0].GetComponent<Button>().Select();

		//hitung height
		Rect currRect = GetComponent<RectTransform> ().rect;
		float height;

		height = transform.parent.GetComponent<RectTransform> ().rect.height / seenButton;

		height *= listItemButtons.Count;
		
		currRect.height = height;
		GetComponent<RectTransform> ().sizeDelta = new Vector2(0, currRect.height); // << Change Rect Transform Height
	}

	void OnGUI() 
	{
		/*if (!generateAll) {
			for (int i=0; i<totalItemCount; i++) {
				if (listItemAmount [i] > 0)
					PutItemListButton (i);
			}
			generateAll = true;
			if(GameObject.Find("ItemListButton11") != null)
				GameObject.Find("ItemListButton11").GetComponent<Button>().Select();
		}*/
	}
}
