﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PanelItemChildren : MonoBehaviour {
	public GameObject firstSelectItemSlot;
	public GameObject panelItemSlot;
	public GameObject panelItemList;
	public GameObject panelItemListList;
	public GameObject amountSetObject;

	public Text infoText;
}
