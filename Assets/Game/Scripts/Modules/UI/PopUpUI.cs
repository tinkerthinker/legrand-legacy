﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PopUpUI{
	public delegate void NotifPopUp(string message, float liveTime);
	public static event NotifPopUp OnNotifPopUp;

	public delegate void ChoicePopUp(string message);
	public static event ChoicePopUp OnChoicePopUp;

	public delegate void TutorialPopUp(string tutorialName, bool forceOpen);
	public static event TutorialPopUp OnTutorialPopUp;

	public delegate void DialoguePopUp(string name, string message, GameObject talkingObj1, global::DialoguePopUp.BalloonType balloonType, bool isNewObject, bool isShouting = false, float liveTime=0f);
	public static event DialoguePopUp OnDialoguePopUp;

	public delegate void DialogueMarkPopUp(GameObject talkingObj, bool isVisible, bool isActiveAfterDialogue);
	public static event DialogueMarkPopUp OnDialogueMarkPopUp;

	public delegate void AmountSetPopUp(int maxAmount, GameObject parentObject, Vector2 addPosition);
	public static event AmountSetPopUp OnAmountSetPopUp;

	public delegate void YesChoicePopUp();
	/// <summary>
	/// Event that will called when popup choice is yes in choice popup.
	/// </summary>
	public static event YesChoicePopUp OnYesChoicePopUp;

	public delegate void NoChoicePopUp();
	/// <summary>
	/// Event that will called when popup choice is no in choice popup.
	/// </summary>
	public static event NoChoicePopUp OnNoChoicePopUp;

	public delegate void EndPopUp(bool choice);
	public static event EndPopUp OnEndPopUp;

    public delegate void HidePopUp(bool isHide);
    public static event HidePopUp OnHidePopUp;

	static YesChoicePopUp _TempYesPopUpFunction;
	static NoChoicePopUp _TempNoPopUpFunction;

	/// <summary> is used to initialize pop up event OnEndPopUp, otherwise yes or no choice event will not be called, when calling ClosePopUp </summary>
	public static void Initialize()
	{
		//subscribe to events
		OnEndPopUp += OnEndPopUpHandler;
	}

	/// <summary> is used to clear all PopUpUI event include OnEndPopUp Event, use it wisely</summary>
	public static void ClearAll()
	{
		OnNotifPopUp = null;
		OnChoicePopUp = null;

		OnTutorialPopUp = null;
		OnDialoguePopUp = null;
		OnDialogueMarkPopUp = null;
		OnAmountSetPopUp = null;

		OnYesChoicePopUp = null;
		OnNoChoicePopUp = null;

		OnEndPopUp = null;
        OnHidePopUp = null;

		Initialize ();
	}

	/// <summary> is used to call OnEndPopUp Event</summary>
	public static void CloseAllPopUp(bool choice = true)
	{
		if(OnEndPopUp != null)
			OnEndPopUp (choice);
	}

    /// <summary>
    /// Is Used to Call OnHidePopUp Event, mainly used to Hide All PopUp (aka. Halt all popup process)
    /// </summary>
    public static void HideAllPopUp(bool isHide)
    {
        if (OnHidePopUp != null)
            OnHidePopUp(isHide);
    }

	/// <summary> is used to call Choice event, parameter is used to ADD 1 function to yes or no choice result when pop up end event is called</summary>
	public static void CallChoicePopUp(string message, YesChoicePopUp yesPopUpFunction=null, NoChoicePopUp noPopUpFunction=null){
		OnYesChoicePopUp += yesPopUpFunction;
		OnNoChoicePopUp += noPopUpFunction;

		if (OnChoicePopUp != null)
		{
			OnChoicePopUp (message);
		}

		_TempYesPopUpFunction = yesPopUpFunction;
		_TempNoPopUpFunction = noPopUpFunction;
		
	}

	/// <summary> is used to call Notif event, parameter is used to ADD 1 function to notif pop up event (1-time only call) when this pop up event is called</summary>
	public static void CallNotifPopUp(string message, YesChoicePopUp onEndNotifPopUpFunction=null, float liveTime=0){
		OnYesChoicePopUp += onEndNotifPopUpFunction;
		if (OnNotifPopUp != null)
		{
			OnNotifPopUp (message, liveTime);
		}
		_TempYesPopUpFunction = onEndNotifPopUpFunction;

	}
	/*
	public static void CallNotifPopUp(string message, NotifPopUp OnNotifPopUpFunction=null){
		OnNotifPopUp += OnNotifPopUpFunction;
		if (OnNotifPopUp != null)
		{
			OnNotifPopUp (message);
		}
		OnNotifPopUp -= OnNotifPopUpFunction;
		
	}*/

	/// <summary> is used to call Tutorial Pop Up event, parameter is used to assign name which tutorial will be appeared based on Tutorial Database when this pop up event is called</summary>
	public static void CallTutorialPopUp(string tutorialName, bool forceOpen = false)
	{
		if (OnTutorialPopUp != null)
		{
			OnTutorialPopUp (tutorialName, forceOpen);
		}
	}

	/// <summary> is used to call Dialogue Pop Up event, parameter is used to give show message in dialogueUI prefab(if there are) or use empty string if using dialoguer</summary>
	public static void CallDialoguePopUp(string name , string message, GameObject talkingObj, global::DialoguePopUp.BalloonType balloonType, bool isNewObject)
	{
		if (OnDialoguePopUp != null)
		{
			OnDialoguePopUp (name, message, talkingObj, balloonType,isNewObject, false, 0f);
		}
	}

	/// <summary> is used to call Dialogue Pop Up event mainly for shoutingNPC, parameter is used to give show message in dialogueUI prefab(if there are) or use empty string if using dialoguer. assuming this method is mainly used for shoutingNPC (and shoutingNPC dont't use dialoguer) because it have liveTime, its recommended to assign message string</summary>
	public static void CallDialoguePopUp(string name , string message,global::DialoguePopUp.BalloonType balloonType, GameObject talkingObj, float liveTime)
	{
		if (OnDialoguePopUp != null)
		{
			OnDialoguePopUp (name, message, talkingObj, balloonType,false, true, liveTime);
		}
	}

	/// <summary> is used to call DialogueMarkPopUp event, an event that been used to show that some object can be talked. parameter is used to assign GameObject</summary>
	public static void CallDialogueMarkPopUp(GameObject talkingObj, bool isVisible, bool isActiveAfterDialogue = false)
	{
		if (OnDialogueMarkPopUp != null)
		{
			OnDialogueMarkPopUp (talkingObj, isVisible, isActiveAfterDialogue);
		}
	}

	/// <summary> is used to call DialogueMarkPopUp event, an event that been used to show that some object can be talked. parameter is used to assign GameObject</summary>
	public static void CallSetAmountPopUp(int maxAmount, GameObject parentObject = null, Vector2 addPosition = new Vector2())
	{
		if (OnAmountSetPopUp != null)
		{
			OnAmountSetPopUp (maxAmount, parentObject, addPosition);
		}
	}

	static void OnEndPopUpHandler(bool choice)
	{
		if (choice)
		{
			if (OnYesChoicePopUp != null)
				OnYesChoicePopUp ();
		} else
		{
			if (OnNoChoicePopUp != null)
				OnNoChoicePopUp ();
		}

		OnYesChoicePopUp -= _TempYesPopUpFunction;
		OnNoChoicePopUp -= _TempNoPopUpFunction;

		_TempYesPopUpFunction = null;
		_TempNoPopUpFunction = null;
	}
}
