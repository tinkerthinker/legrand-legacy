﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DialoguePopUp : PopUpObject {
	public enum BalloonType
	{
		Normal,
		Shout,
		Mind,
        Telepathy
	}
	private BalloonType _CurrentBalloonType;

	public Text DialogueText;
	public Text NameText;

	public int MaxCharInLine=20;

	private GameObject _TalkingObj1;
	public GameObject TalkingObj1
	{
		set{_TalkingObj1 = value; SetPositionToObject();}
		get{return _TalkingObj1;}
	}
	public RectTransform RootObj;
	public Image BalloonImage;
    public RectTransform Arrow;

    public float TestOffsetArrowFlip = 50;

	public GameObject AddShoutPanel;
    public GameObject AddTelepathyPanel;

	Camera currCamera;
	Vector2 _RefResolution;

    [System.NonSerialized]
	public bool MustInsideScreen = true;

	private bool _IsShout = false;
	private string SettedMessage;

    private bool _IsDieByTime = false;
    public bool IsDieByTime
    {
        get { return _IsDieByTime; }
    }

	void Awake()
	{
		addDialoguerEvents ();
		_RefResolution = GetComponentInParent<CanvasScaler>().referenceResolution;
		SetAsNormal (true);
	}

	void OnEnable () {
		currCamera = GlobalGameStatus.Instance.MainCamera;

		if (GetComponent<Canvas> ())
		{
			if (GetComponent<Canvas> ().worldCamera != currCamera)
				GetComponent<Canvas> ().worldCamera = currCamera;
		}
		if(NameText)
			NameText.gameObject.SetActive (false);
		PopUpUI.OnEndPopUp += ClosePopUpObject;	 //already inherit disable from parent, so doesn't need to make OnDisable again
		//SetPositionToObject ();
		SetAsNormal();
	}

	void OnDestroy () {
		removeDialoguerEvents ();
        if (EventManager.Instance && EventManager.Instance.HasListener<CloseShoutPopUpEvent>(CloseShoutPopUpEventHandler))
			EventManager.Instance.RemoveListener<CloseShoutPopUpEvent> (CloseShoutPopUpEventHandler);
	}

	void Update()
	{
        if (_TalkingObj1 == null || !_TalkingObj1.activeInHierarchy)
        {
            DisablePopUp();
        }
		if(gameObject.activeInHierarchy)
			SetPositionToObject ();		
	}

    protected override void ClosePopUpObject(bool choice = true)
    {
        if(!_IsDieByTime)
            base.ClosePopUpObject(choice);
    }

	public void SetPositionToObject()
	{
		if (!_TalkingObj1.gameObject.activeInHierarchy)
			return;
        Vector3 screenPos = currCamera.WorldToScreenPoint(_TalkingObj1.transform.position);
		Vector3 confirmedPos = screenPos;

		if (MustInsideScreen)
			confirmedPos = ConfirmInsideScreen (screenPos);

		RootObj.position = confirmedPos;

        //set arrow direction
        //check if arrow should rotate to left
        if (confirmedPos.x > screenPos.x + (TestOffsetArrowFlip * (Screen.width / _RefResolution.x)))
        {
            if(Arrow.rotation.y != 0)
            {
                Quaternion temp = Arrow.rotation;
                temp.y = 0;
                Arrow.rotation = temp;
            }
            
        }
        else
        {
            if (Arrow.rotation.y != 180)
            {
                Quaternion temp = Arrow.rotation;
                temp.y = 180;
                Arrow.rotation = temp;
            }
        }
	}

	Vector3 ConfirmInsideScreen(Vector3 screenPos)
	{
		float screenHeight = Screen.height;
		float screenWidth = Screen.width;

		float procHeight = screenHeight / _RefResolution.y;
		float procWidth = screenWidth / _RefResolution.x;

		float offsetHeight = 1 * procHeight;
		float offsetWidth = 1 * procWidth;

		float up = screenPos.y - ( (RootObj.rect.height * RootObj.lossyScale.y) * 0.5f);
		float right = screenPos.x + ( (RootObj.rect.width * RootObj.lossyScale.x) * 0.5f);
		float down = screenPos.y + ( (RootObj.rect.height * RootObj.lossyScale.y) * 0.5f);
		float left = screenPos.x - ( (RootObj.rect.width * RootObj.lossyScale.x) * 0.5f);

        Vector3 prevScreenPos = screenPos ;
        Vector2 shiftedPosition = new Vector2();
		bool[] confirmedPosition = new bool[]
		{
			false,false,false,false
		};

		if (CheckPointInsideScreenHeight (up))
			confirmedPosition [0] = true;
		if (CheckPointInsideScreenWidth (right))
			confirmedPosition [1] = true;
		if (CheckPointInsideScreenHeight (down))
			confirmedPosition [2] = true;
		if (CheckPointInsideScreenWidth (left))
			confirmedPosition [3] = true;

		for(int i = 0; i<confirmedPosition.Length;i++)
		{
			if(!confirmedPosition[i])
			{
				switch(i)
				{
				case 0:
					if(up < 0 )	//posisi up ada di bawah layar
					{
                        //screenPos.y = screenHeight + offsetHeight + (RootObj.rect.height * procHeight);
                        screenPos.y = 0 + offsetHeight + (RootObj.rect.height * procHeight);
					}
                    else if (up > screenHeight)	//posisi up ada di atas layar
					{
						screenPos.y = screenHeight - offsetHeight - (RootObj.rect.height * procHeight);
					}

					confirmedPosition[2] = true;
					break;
				case 1:
					if(right < 0 )
					{
                        //screenPos.x = screenWidth + offsetWidth + (RootObj.rect.width * procWidth) * 0.5f;
                        screenPos.x = 0 + offsetWidth + (RootObj.rect.width * procWidth) * 0.5f;
					}
					else if(right > screenWidth)
					{
						screenPos.x = screenWidth - offsetWidth - (RootObj.rect.width * procWidth) * 0.5f;
					}
                    shiftedPosition.x = screenPos.x - prevScreenPos.x;

					confirmedPosition[3] = true;
					break;
				case 2:
					if(down < 0 )
					{
						screenPos.y = 0 + offsetHeight + (RootObj.rect.height * procHeight);
					}
					else if(down > screenHeight)
					{
						screenPos.y = screenHeight - offsetHeight - (RootObj.rect.height * procHeight);
					}
					break;
				case 3:
					if(left < 0 )
					{
						screenPos.x = 0 + offsetWidth + (RootObj.rect.width * procWidth) * 0.5f;
					}
					else if(left > screenWidth)
					{
						screenPos.x = screenWidth - offsetWidth - (RootObj.rect.width * procWidth) * 0.5f;
					}
                    shiftedPosition.x = screenPos.x - prevScreenPos.x;
					break;
				}
			}
		}

        //shift arrow, Test in X-Axis
        if(shiftedPosition.x != 0)
        {
            float sign = -1* (shiftedPosition.x / Mathf.Abs (shiftedPosition.x));
            shiftedPosition.x = sign * Mathf.Clamp(Mathf.Abs(shiftedPosition.x), 0, RootObj.rect.width * 0.5f - 15);
        }

        shiftedPosition.y = Arrow.anchoredPosition.y + shiftedPosition.y;
        Arrow.anchoredPosition = shiftedPosition;

		return screenPos;

	}

	bool CheckPointInsideScreenWidth(float x)
	{
		if (x > 0 && x < Screen.width)
			return true;
		return false;
	}

	bool CheckPointInsideScreenHeight(float y)
	{
		if (y > 0 && y < Screen.height)
			return true;
		return false;
	}

	public void addDialoguerEvents(){
        _IsDieByTime = false;
		Dialoguer.events.onWindowClose += onDialogueWindowCloseHandler;
	}
	
	public void removeDialoguerEvents()
	{
		Dialoguer.events.onWindowClose -= onDialogueWindowCloseHandler;
	}

	//unused, reserved purpose and research only. dont delete if not reach final phase yet.
	private void onDialogueTextPhaseHandler(DialoguerTextData data)
	{
		SetMessage (data.text);
		if (!string.IsNullOrEmpty (data.name)) {
			SetName (data.name);
			if(NameText)
				NameText.gameObject.SetActive (true);
		}
		else
			NameText.gameObject.SetActive (false);
		if (Dialoguer.GetGlobalBoolean (2)) {
			DestroyPopUp ();
		}
	}
	
	private void onDialogueWindowCloseHandler()
	{
//		DisableThisPopUp ();
		ClosePopUp ();
	}

	public void SetMessage(string message)
	{
        string tempMsg = message;
        //message = LegrandBackend.Instance.GetLanguageData(message);
        //if (string.IsNullOrEmpty(message))
        //    message = tempMsg;
		SettedMessage = message;
		if (DialogueText) {
			if(message.Length >  MaxCharInLine)
			{
				int indexLine=0;
				int previousRowCount = 0;
				int startIndex = 0;

				bool ignoreCount = false;

				for(int i=0; i< (message.Length/MaxCharInLine); i++)//for row
				{
					ignoreCount = false;
					for (startIndex = 0; startIndex < MaxCharInLine && indexLine < message.Length-1; startIndex++) {
						if (message [indexLine].Equals ('<'))
							ignoreCount = true;
						else if (message [indexLine].Equals ('>'))
							ignoreCount = false;
						if (ignoreCount)
							startIndex--;
						indexLine++;
					}
					while(!message[indexLine].Equals(' ') && indexLine >previousRowCount && indexLine < message.Length-1)
					{
						indexLine--;
					}
					if (indexLine >= message.Length - 1)
						break;
					else if (indexLine > previousRowCount) {
						message = message.Remove (indexLine, 1);
						message = message.Insert (indexLine, "\n");
					}
					previousRowCount = indexLine;
//					indexLine++;
				}
			}
			//message = message.PadRight(20, ' ');
			DialogueText.text = message;
		}
	}

	public void SetName(string name)
	{
		if (NameText) {
			if (string.IsNullOrEmpty (name))
				NameText.gameObject.SetActive (false);
			else {
				NameText.gameObject.SetActive (true);

				NameText.text = name;
				if (NameText.text.Length > 20)
					NameText.text.Insert (20, "\n ");
			}
		}
	}

	public void SetAsShout(bool isForceChange=false)
	{
		if (_CurrentBalloonType != BalloonType.Shout || isForceChange) {
			AddShoutPanel.SetActive (true);
            AddTelepathyPanel.SetActive(false);
			_CurrentBalloonType = BalloonType.Shout;
		}
	}

	public void SetAsNormal(bool isForceChange=false)
	{
		if (_CurrentBalloonType != BalloonType.Normal || isForceChange) {
			AddShoutPanel.SetActive (false);
            AddTelepathyPanel.SetActive(false);
			_CurrentBalloonType = BalloonType.Normal;
		}
	}

	public void SetAsMind()
	{
		
	}

    public void SetAsTelepathy(bool isForceChange = false)
    {
        if (_CurrentBalloonType != BalloonType.Telepathy || isForceChange)
        {
            AddShoutPanel.SetActive(false);
            AddTelepathyPanel.SetActive(true);
            _CurrentBalloonType = BalloonType.Telepathy;
        }
    }

	//Shouting dialogue = dialogue that NPC use to shout. used by npc that shout even when character isn't talk to them.
	public void SetAsShoutingDialogue()
	{
		_IsShout = true;
		EventManager.Instance.AddListener<CloseShoutPopUpEvent> (CloseShoutPopUpEventHandler);
	}

	void CloseShoutPopUpEventHandler(CloseShoutPopUpEvent e)
	{
		if (e.ShoutDialogue.Equals (SettedMessage)) {
			DisableThisPopUp();
		}
	}

	public void SetLiveTime(float liveTime)
	{
		//StartCoroutine (WaitToDie (liveTime));
        _IsDieByTime = true;
        //Invoke ("DieByTime", liveTime);
        Invoke("HideByTime", liveTime);
	}

	//unused, reserved purpose and research only. dont delete if not reach final phase yet.
	IEnumerator WaitToDie(float liveTime)
	{
		yield return new WaitForSeconds(liveTime);
		//if(gameObject != null)
			DestroyThisPopUp ();
	}

	void DieByTime()
	{
		if(gameObject)
			DestroyThisPopUp ();
	}

    void HideByTime()
    {
        if (gameObject)
            DisableThisPopUp();
    }
}
