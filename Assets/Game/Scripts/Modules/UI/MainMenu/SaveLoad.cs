﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class SaveLoad : MonoBehaviour, ISelectHandler, IDeselectHandler {

	public List<Image> CharacterImages;
	public Text SaveNameText;
	public Text Time;
	public Text Chapter;
    public Text Location;
    public Text Danaar;
	public string EmptySlotName = "Empty Slot";
	public Text EmptySlotText;
	private string SaveName = "";
	private bool IsEmpty = true;
	[SerializeField]
	private bool IsLoad = false;

    public Image CoverImage;

	List<Sprite> ColorfulSprite;

	[SerializeField]
	SaveLoadController Controller;

	void OnEnable()
	{
        Time.text = "";
        Chapter.text = "";
        Location.text = "";
        Danaar.text = "";

        SaveNameText.text = gameObject.name;

		if(SaveName == "")
			SaveName = SaveNameText.text;
		ClearSaveSlot ();
		SaveDataUpdate ();
        SetCharacterBox();
	}
	public void SaveLoadAction()
	{
        if (Controller.SaveMenuState == SaveLoadController.State.Load)
        {
            Load();
        }
        else if (Controller.SaveMenuState == SaveLoadController.State.Save)
        {
            Save();
        }
        else if (Controller.SaveMenuState == SaveLoadController.State.Delete)
        {
            Delete();
        }
	}

	public void Save()
	{
        if (LegrandBackend.Instance.IsSaveGameDataExist(SaveName))
        {
            PopUpUI.CallChoicePopUp("Do you want to overwrite this file?", ConfirmedSave);
        }
        else
            ConfirmedSave();
	}

    void ConfirmedSave()
    {
        WwiseManager.Instance.PlaySFX(WwiseManager.UIFX.Confirm);
        if (LegrandBackend.Instance.SaveGame(SaveName))
        {
            //Update info di save slot
            SaveDataUpdate();
            SetColorActive();
            //Notif Berhasil
            StartCoroutine(WaitToCallNotif("Game Saved Successfully"));
        }
        else
        {
            //Notif Error
            StartCoroutine(WaitToCallNotif("Save Game Failed"));
        }
    }

    IEnumerator WaitToCallNotif(string message)
    {
        yield return null;
        PopUpUI.CallNotifPopUp(message);
    }

	void Load()
	{
		if (!IsEmpty && !IsLoad){
			WwiseManager.Instance.PlaySFX(WwiseManager.UIFX.Confirm);
			if (LegrandBackend.Instance.LoadGame (SaveName))
            {
                IsLoad = true;
                EventSystem.current.SetSelectedGameObject(null);
                LoadingManager._Instance.OnStartLoading += LoadReady;
                LoadingManager.StartLoading();
			} else {
                
			}
		}
	}

    void LoadReady()
    {
        LoadingManager._Instance.OnStartLoading -= LoadReady;
        LegrandBackend.Instance.StartLoadWorldScene();
        StartCoroutine(WaitUntilGameReady());
    }

	void Delete()
	{
		if (!IsEmpty) {
			if (LegrandBackend.Instance.DeleteFile (SaveName)) {
				ClearSaveSlot();
			} else {
			}
		}
	}

	void SaveDataUpdate()
	{
		WorldState state;
		List<CharacterModel> characters;
		if (LegrandBackend.Instance.GetSaveGameData (SaveName, out state, out characters)) 
		{
			ClearSaveSlot();
			this.Time.text = ((int)state.TimeElapsed/3600).ToString("00") +"."+ (((int)state.TimeElapsed%3600)/60).ToString("00") + "." + (((int)state.TimeElapsed%3600)%60).ToString("00");
			this.Chapter.text = "Chapter "+state.currChapter;
            this.Location.text = state.gameArea.Replace("_", " ");
            this.Danaar.text = state.currGold + " D";

			int index = 0;

			foreach(CharacterModel C in characters)
			{
                if (!C.Active) continue;

                string key = "Portrait";
                key += C.Name + C.Model;
                Sprite sprite = LegrandBackend.Instance.GetSpriteData(key);
                if (sprite == null)
                {
                    key = "Portrait" + C.Name;
                    sprite = LegrandBackend.Instance.GetSpriteData(key);
                }

                //ColorfulSprite.Add(LegrandBackend.Instance.GetSpriteData("Portrait" + C.Name));
                ColorfulSprite.Add(sprite);

				CharacterImages[index].color = Color.white;
				index++;
			}

            SetCharacterBox();

			IsEmpty = false;

			this.SaveNameText.text = SaveName;

			if (EmptySlotText != null)	this.EmptySlotText.text = "";
		}
		SetColorInActive();
	}

	void ClearSaveSlot()
	// Clear Slot Terlebih Dahulu, warna tulisan dijadikan transparan dan bewarna pada saat OnSelect. Tulisan juga dikosongkan dan diisi saat update save slot.
	{
		foreach (Image CharacterImage in CharacterImages)
		{
			CharacterImage.sprite = null;
			CharacterImage.color = Color.clear;
		}

		ColorfulSprite = new List<Sprite> ();

		this.Time.text = "";
		this.Chapter.text = "";
        if (EmptySlotText)
        {
            this.EmptySlotText.text = EmptySlotName;
        }

		IsEmpty = true;
	}

	#region ISelectHandler implementation

	public void OnSelect (BaseEventData eventData)
	{
		OnSelect ();
	}

	#endregion

	public void OnSelect()
	// Tulisan dibuat tidak transparan, dan gambar bewarna.
	{
		SetColorActive ();
	}

	#region IDeselectHandler implementation

	public void OnDeselect (BaseEventData eventData)
	{
		OnDeselect ();
	}

	#endregion

	public void OnDeselect()
	// Tulisan dibuat transparan, dan gambar grayscale.
	{
		SetColorInActive ();
	}

    void SetCharacterBox()
    {
        for(int index = 0;index<CharacterImages.Count && index<ColorfulSprite.Count;index++)
        {
            CharacterImages[index].sprite = ColorfulSprite[index];
        }
    }

	public void SetColorActive()
	{
        CoverImage.color = new Color(0,0,0,0);
	}

	public void SetColorInActive()
	{
        CoverImage.color = new Color(0,0,0,0.4f);
	}

	private IEnumerator WaitUntilGameReady()
	{
		while (!PartyManager.Instance._ReadyToPlay)
			yield return null;
		SceneManager.ForceSwitchScene ("WorldScene");
	}
}
