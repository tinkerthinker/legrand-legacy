﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TeamUtility.IO;

public class SettingController : MonoBehaviour {
	public TitleSceneController MainSceneController;

	private Resolution[] _Resolutions;
	private int _CurrentResolutionIndex;
	private int _NewResolutionIndex;

	public Button FirstButton;
	public Text ResolutionText;



	public void Open()
	{
		this.gameObject.SetActive (true);
		FirstButton.Select ();
		_Resolutions = Screen.resolutions;

		bool found = false;
		for(int i = 0; i < _Resolutions.Length && !found; i++)
		{
			if(Screen.currentResolution.width == _Resolutions[i].width && Screen.currentResolution.height == _Resolutions[i].height)
			{
				_CurrentResolutionIndex = i;
				found = true;
			}
		}
		_NewResolutionIndex = _CurrentResolutionIndex;
		SetResolutionText ();
	}

	public void SelectResolution(bool isRight)
	{
		if (isRight && _NewResolutionIndex + 1 < _Resolutions.Length) 
			_NewResolutionIndex++;
		else if(!isRight && _NewResolutionIndex - 1 >= 0)
			_NewResolutionIndex--;
		SetResolutionText ();
		//Screen.SetResolution (_Resolutions[0].width, _Resolutions[0].height, true);
	}

	private void SetResolution()
	{
		if (_NewResolutionIndex != _CurrentResolutionIndex) 
		{
			PlayerPrefs.DeleteAll();
			Screen.SetResolution(_Resolutions[_NewResolutionIndex].width,_Resolutions[_NewResolutionIndex].height, true);
			_CurrentResolutionIndex = _NewResolutionIndex;
		}
	}

	private void SetResolutionText()
	{
		ResolutionText.text = _Resolutions[_NewResolutionIndex].width + "x" + _Resolutions[_NewResolutionIndex].height;
	}

	void Update()
	{
		if (InputManager.GetButtonDown ("Cancel")) 
		{
			Cancel();
		}
	}

	public void Cancel()
	{
		this.gameObject.SetActive(false);
		WwiseManager.Instance.PlaySFX(WwiseManager.UIFX.Cancel);
		MainSceneController.Activate();
		MainSceneController.SettingGameButton.Select();
	}

	public void Apply()
	{
		WwiseManager.Instance.PlaySFX(WwiseManager.UIFX.Confirm);
		SetResolution ();
	}
}
