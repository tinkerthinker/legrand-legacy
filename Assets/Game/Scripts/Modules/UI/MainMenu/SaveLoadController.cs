﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using TeamUtility.IO;

public class SaveLoadController : MonoBehaviour {
	public enum State
	{
		Load,
		Save,
		Delete
	}

	public enum PageState
	{
		Main,
		SaveLoad
	}

	public TitleSceneController MainSceneController;
    public bool FromTitleScene;
	public State SaveMenuState;
	public PageState CurrentPage;

	public GameObject PanelMainPage;
	public GameObject PanelSaveLoad;

	public Button SaveButton;
	public Button LoadButton;
	public Button DeleteButton;

	public Text SaveLoadLabel;

	public GameObject PreviousUI;

    public Button FirstSlot;

    private bool _IsCanFirstSelect = true;

	public static SaveLoadController instance;

	void Awake()
	{
		SaveLoadController.instance = this;
	}

	void OnEnable()
    {
        PreviousUI = null;
        StartCoroutine(WaitForSelect());
	}

    IEnumerator WaitForSelect()
    {
        do
        {
            yield return null;
        } while (!_IsCanFirstSelect);

        FirstSlot.Select();

        if(!FromTitleScene && SaveButton != null)
        {
            SaveButton.Select();
            /*
            if(AreaController.Instance != null)
            {
                if(AreaController.Instance.CurrRoom.IsRoomSafe())
                {
                    SaveButton.interactable = true;
                    SaveButton.Select();
                }
                else
                {
                    SaveButton.interactable = false;
                    LoadButton.Select();
                }
            }
            else
            {
                SaveButton.interactable = true;
                SaveButton.Select();
            }*/
        }
        else
        {
            CurrentPage = PageState.SaveLoad;
            SaveMenuState = State.Load;
        }

        /*
        if (SaveButton)
            SaveButton.Select();
        else
        {
            CurrentPage = PageState.SaveLoad;
            SaveMenuState = State.Load;
        }*/
    }

    public void HoldFirstSelect()
    {
        _IsCanFirstSelect = false;
    }

    public void AllowFirstSelect()
    {
        _IsCanFirstSelect = true;
    }

	public void ChangeToLoad()
	{
		SaveMenuState = State.Load;
        CurrentPage = PageState.SaveLoad;
        if (PreviousUI)
            PreviousUI.GetComponent<Button>().Select();
        else
            FirstSlot.Select();
	}

	public void ChangeToSave()
	{
        if(GlobalGameStatus.Instance.GetCurrRoomSafety() == false)
        {
            PopUpUI.CallNotifPopUp("Not a safe area! can't save here!");
            return;
        }

		SaveMenuState = State.Save;
        CurrentPage = PageState.SaveLoad;
        if (PreviousUI)
            PreviousUI.GetComponent<Button>().Select();
        else
            FirstSlot.Select();
	}

    public void ChangeToDelete()
    {
        SaveMenuState = State.Delete;
        CurrentPage = PageState.SaveLoad;
        if (PreviousUI)
            PreviousUI.GetComponent<Button>().Select();
        else
            FirstSlot.Select();
    }

    public void OnCancel()
    {
        if (CurrentPage == PageState.Main)
        {
            Cancel();
        }
        else
        {
            CurrentPage = PageState.Main;

            if (SaveButton)
            {
                if(SaveMenuState == State.Save)
                    SaveButton.Select();
                else if (SaveMenuState == State.Load)
                    LoadButton.Select();
                else if (SaveMenuState == State.Delete)
                    DeleteButton.Select();
            }
            else
                Cancel();
        }
    }

	public void Cancel()
	{
		this.gameObject.SetActive(false);
		WwiseManager.Instance.PlaySFX(WwiseManager.UIFX.Cancel);
        if (FromTitleScene)
        {
            MainSceneController.Activate();
            MainSceneController.LoadGameButton.Select();
        }
	}
}
