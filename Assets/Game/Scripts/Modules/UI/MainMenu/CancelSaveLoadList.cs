﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class CancelSaveLoadList : MonoBehaviour, ICancelHandler {
	public Button saveBtn;
	public Button loadBtn;
	public Button deleteBtn;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	#region ICancelHandler implementation

	public void OnCancel (BaseEventData eventData)
	{
		if (SaveLoadController.instance.SaveMenuState == SaveLoadController.State.Save)
			saveBtn.Select ();
		else if (SaveLoadController.instance.SaveMenuState == SaveLoadController.State.Load)
			loadBtn.Select ();
		else if (SaveLoadController.instance.SaveMenuState == SaveLoadController.State.Delete)
			deleteBtn.Select ();
	}

	#endregion
}
