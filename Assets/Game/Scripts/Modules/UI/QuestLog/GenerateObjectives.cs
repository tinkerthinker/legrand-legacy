﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GenerateObjectives : MonoBehaviour {
	public GameObject ObjectivePref;
	private Quest _QuestData;
	private List<GameObject> _ListObjectiveObjects = new List<GameObject>();

	public void UpdateObjective(Quest quest)
	{
		_QuestData = quest;
		UpdateObjectiveItem ();
	}

	void UpdateObjectiveItem()
	{
		int index = 0;
		foreach (Task task in _QuestData.ListTasks) {
			UpdateObjectiveData (task, ref index);
		}

		if (_ListObjectiveObjects.Count > index) {
			for(int i=index ;i<_ListObjectiveObjects.Count;i++){
				Destroy(_ListObjectiveObjects[i]);
			}
			_ListObjectiveObjects.RemoveRange(index, _ListObjectiveObjects.Count - index);
		}
	}

	void UpdateObjectiveData(Task task, ref int startIndex)
	{
		int index = startIndex;

		if (task.Status == QuestEnums.Status.NotStarted)
			return;

        if (index >= _ListObjectiveObjects.Count)
        {
            CreateNewObjectiveObject(task);
        }

        _ListObjectiveObjects[index].GetComponentInChildren<IObjectData>().SetData(task);

        //_ListObjectiveObjects[index].GetComponentInChildren<Text>().text = task.Objective + (task.Status == QuestEnums.Status.Done ? " <color=#2222ff>Done</color>" : "");

		index++;

		startIndex = index;
	}

	void CreateNewObjectiveObject(Task task)
	{
		GameObject newObjectiveObj;

		newObjectiveObj = Instantiate (ObjectivePref) as GameObject;
		newObjectiveObj.transform.SetParent (this.transform, false);

        //newObjectiveObj.GetComponentInChildren<Text> ().text = task.Description + (task.Status == QuestEnums.Status.Done?" <color=#2222ff>Done</color>":"");
		_ListObjectiveObjects.Add (newObjectiveObj);
	}
}
