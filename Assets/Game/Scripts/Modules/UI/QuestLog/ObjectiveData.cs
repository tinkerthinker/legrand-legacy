﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ObjectiveData : MonoBehaviour, IObjectData
{
    public Text ObjectiveText;
    public GameObject CompletedObjective;

    private Task TaskData;

    public void SetData(object obj)
    {
        if (!(obj is Task))
            return;

        TaskData = (Task)obj;

        if (ObjectiveText)
            ObjectiveText.text = TaskData.Objective;
        if (CompletedObjective)
        {
            CompletedObjective.SetActive(false);

            if (TaskData.Status == QuestEnums.Status.Done)
            {

                CompletedObjective.SetActive(true);
            }
        }
    }

    public object GetData()
    {
        return TaskData;
    }

    public void RefreshData()
    {
        SetData(TaskData);
    }

    public void OnSelect(UnityEngine.EventSystems.BaseEventData eventData)
    {
        
    }
}
