﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ChangeGenQuestLogData : MonoBehaviour {
	public GenerateQuestLog Target;
	public Button TargetCancelButton;
	public List<QuestEnums.StoryType> QuestTypes;
	public List<QuestEnums.Status> QuestStatus;

    private bool _IsCanFocusFirst = true;

	public void UpdateGenerateQuestLog()
	{
		Target.QuestTypes = QuestTypes;
		Target.QuestStatus = QuestStatus;
		if (!TargetCancelButton)
			TargetCancelButton = this.GetComponent<Button> ();
		if (TargetCancelButton)
			Target.TargetCancelButton = TargetCancelButton;
		Target.UpdateQuestLog ();
	}

    public void FocusFirstButton()
    {
        if (!_IsCanFocusFirst)
            return;
        if (UnityEngine.EventSystems.EventSystem.current)
            UnityEngine.EventSystems.EventSystem.current.SetSelectedGameObject(this.gameObject);

        if (!Target.FocusToFirstButton())
            if (Target.ScrollerController)
                Target.ScrollerController.ScrollbarTarget.Select();
    }

    public void SetCanFocusFirst(bool isCan)
    {
        _IsCanFocusFirst = isCan;
    }
}
