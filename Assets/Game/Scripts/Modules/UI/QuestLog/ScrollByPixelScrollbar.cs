﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class ScrollByPixelScrollbar : MonoBehaviour, IUpdateSelectedHandler {
	public Scrollbar TargetScrollbar;
	public float PixelPerScroll = 10;
	
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	#region IUpdateSelectedHandler implementation

	public void OnUpdateSelected (BaseEventData eventData)
	{

	}

	#endregion
}
