﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class GenerateQuestLog : AbsObjectListGenerator
{
    //public ScrollController ScrollerController;
	public Button QuestButtonPrefab;

	public Text GlobalTextName;
	public Text GlobalTextObjective;
	public Text GlobalTextDetails;
	public Text GlobalTextStatus;
	
	public List<QuestEnums.StoryType> QuestTypes;
	public List<QuestEnums.Status> QuestStatus;

	public Button TargetCancelButton;
	public Scrollbar scrollBarQuestLog;
    //public int seenRow;

    //private List<Button> _ListQuestButtons = new List<Button>();

	void Start ()
	{

	}

	void OnEnable ()
	{
//		UpdateQuestLog ();
	}

	void OnDisable ()
	{
//		QuestTypeGenerated = QuestEnums.Status.OnGoing;
	}

	public void UpdateQuestLog ()
	{
        UpdateList();
	}

	public bool FocusToButton (string idQuest)
	{
        //foreach (var questButton in _ListQuestButtons) {
        foreach (var questButton in _ListButtons)
        {
			if (questButton.GetComponent<QuestLogData> ().QuestData.QuestID.Equals (idQuest)) {
				questButton.Select ();
				return true;
			}
		}
		return false;
	}

    public override void StartLoopUpdate()
    {
        List<Quest> listQuest;
        for (int typeIndex = 0; typeIndex < QuestTypes.Count; typeIndex++)
        {
            foreach (QuestEnums.Status status in QuestStatus)
            {
                if (status == QuestEnums.Status.OnGoing)
                    listQuest = QuestManager.Instance.GetActiveQuest();
                else if (status == QuestEnums.Status.Done)
                    listQuest = QuestManager.Instance.GetFinishQuest();
                else
                    continue;

                foreach (Quest quest in listQuest)
                {
                    if (quest.QuestType != QuestTypes[typeIndex])
                        continue;
                    UpdateEachButton(quest);
                }
            }
        }
    }

    public override bool CheckConditionPreUpdate(object data)
    {
        return true;
    }

    public override void SetButtonName(Button button, object data)
    {
        if (!(data is Quest))
            return;
        button.name = ((Quest)data).QuestID;
        if(button.GetComponent<CancelQuestList>()!=null)
            button.GetComponent<CancelQuestList>().TargetCancelButton = TargetCancelButton;
    }

    public override void AfterDataUpdatedProcess(IObjectData objectData, object data)
    {
        
    }
}