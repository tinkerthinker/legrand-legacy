﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class CancelQuestList : MonoBehaviour, ICancelHandler {
	public PanelQuestChildren PanelQuestLog;
	public Button TargetCancelButton;

	#region ICancelHandler implementation

	public void OnCancel (BaseEventData eventData)
	{
		if (TargetCancelButton)
			TargetCancelButton.Select ();
		else
			PanelQuestLog.GoToMain ();
	}

	#endregion
}
