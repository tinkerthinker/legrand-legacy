﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PanelQuestChildren : MonoBehaviour {
	public GameObject PanelQuestList;
    public Button MainObjectiveBtn;
	
	public void GoToQuestLog()
	{
		PauseUIController.Instance.ChangeState (PauseUIController.PauseUIControllerState.QuestLog);
        //GenerateQuestLog genInvent = PanelQuestList.GetComponent<GenerateQuestLog> ();
	}
	
	public void GoToMain()
	{
		PauseUIController.Instance.ChangeState (PauseUIController.PauseUIControllerState.MainMenu);
        //PauseUIController.Instance.QuestLogButton.Select ();
	}

    public void FirstFocusModule()
    {
        MainObjectiveBtn.Select();
    }
}
