﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class QuestLogData : MonoBehaviour, IObjectData
{
	private Quest _QuestData;
	public bool AsMainQuestData;
	
	public Quest QuestData{
		set{_QuestData = value;
			SetQuestData();}
		get{return _QuestData;}
	}

    public bool SetAsNullData = false;
	public Text TextQuestName;
    public Text TextQuestClient;
	public Text TextQuestStatus;
	
	private string _QuestName;
	private string _Objectives;

	void OnEnable () {
		if (AsMainQuestData) {
			Quest mainQuest = QuestManager.Instance.GetActiveQuest().Find(x => x.QuestType == QuestEnums.StoryType.Main);
			QuestData = mainQuest;
			StartCoroutine (WaitTillQuestHandlerReady ());
		}
	}

	IEnumerator WaitTillQuestHandlerReady()
	{
		yield return null;
		EventManager.Instance.TriggerEvent (new InspectQuestEvent (_QuestData));
	}
	
	void SetQuestData()
	{
        if (SetAsNullData)
            _QuestData = null;

		if (_QuestData == null) {
			if (TextQuestName != null)
				TextQuestName.text = "";
            if (TextQuestClient != null)
                TextQuestClient.text = "";
			if (TextQuestStatus != null)
				TextQuestStatus.text = "";
			
			return;
		}

		_QuestName = _QuestData.QuestName;

		_Objectives = "";
		foreach(Task task in _QuestData.ListTasks)
		{
			if(task.Status != QuestEnums.Status.NotStarted)
				_Objectives += "*" + task.Objective+"\n";
		}
		
		gameObject.name = _QuestName;

		if (TextQuestName != null)
			TextQuestName.text = _QuestName;
        if (TextQuestClient != null)
            TextQuestClient.text = _QuestData.QuestGiver;
		SetQuestStatus ();
	}

	void SetQuestStatus()
	{
		if (TextQuestStatus != null) {
			if (_QuestData.IsRead) {
                /*
				if(_QuestData.Status == QuestEnums.Status.Done)
					TextQuestStatus.text = "Done";
				else
					TextQuestStatus.text = "";
                 * */
                if(QuestManager.Instance.GetFinishQuest().Contains(_QuestData))
                {
                    TextQuestStatus.text = "Done";
                }
                else
                    TextQuestStatus.text = "";
			}
			else
				TextQuestStatus.text = "New";
		}
	}
	
	public void UpdateData()
	{
		SetQuestData ();
	}
	
	public void OnSelect(BaseEventData eventData)
	{
		if (_QuestData == null)
			return;

		_QuestData.IsRead = true;
		SetQuestStatus ();

		StartCoroutine (WaitTillQuestHandlerReady ());
	}

    public void SetData(object obj)
    {
        if (!(obj is Quest))
            return;

        _QuestData = (Quest)obj;

        if (_QuestData == null)
        {
            if (TextQuestName != null)
                TextQuestName.text = "";
            if (TextQuestClient != null)
                TextQuestClient.text = "";
            if (TextQuestStatus != null)
                TextQuestStatus.text = "";

            return;
        }

        _QuestName = _QuestData.QuestName;

        _Objectives = "";
        foreach (Task task in _QuestData.ListTasks)
        {
            if (task.Status != QuestEnums.Status.NotStarted)
                _Objectives += "*" + task.Objective + "\n";
        }

        gameObject.name = _QuestName;

        if (TextQuestName != null)
            TextQuestName.text = _QuestName;
        if (TextQuestClient != null)
            TextQuestClient.text = _QuestData.QuestGiver;
        SetQuestStatus();
    }

    public object GetData()
    {
        return _QuestData;
    }

    public void RefreshData()
    {
        SetData(_QuestData);
    }
}
