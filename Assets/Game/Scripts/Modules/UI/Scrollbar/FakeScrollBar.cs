﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FakeScrollBar : MonoBehaviour
{
	public Slider slider;

	void Start ()
    {
		GetComponent<Scrollbar> ().onValueChanged.AddListener (delegate {
			ValueChanged ();
		});
	}

	void ValueChanged ()
	{
		slider.value = GetComponent<Scrollbar> ().value;
	}
}
