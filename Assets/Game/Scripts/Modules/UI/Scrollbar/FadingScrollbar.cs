﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class FadingScrollbar : MonoBehaviour, ISelectHandler {
	public Animator ScrollbarAnimator;
	
	void Start () {

	}

	void OnEnable()
	{

	}
	
	void Update () {
	}

	#region ISelectHandler implementation

	public void OnSelect (BaseEventData eventData)
	{
		if(ScrollbarAnimator)
			ScrollbarAnimator.SetTrigger ("Fading");
	}

	#endregion
}
