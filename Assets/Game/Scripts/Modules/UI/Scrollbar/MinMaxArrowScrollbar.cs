﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MinMaxArrowScrollbar : MonoBehaviour {
	public Scrollbar TargetScrollbar;

	public Image MinArrow;
	public Image MaxArrow;

	void Start()
	{
		if (!TargetScrollbar)
			TargetScrollbar = GetComponent<Scrollbar> ();
	}

	public void UpdateArrow()
	{
		if (MinArrow) {
			if (TargetScrollbar.value <= 0.001f)
				MinArrow.gameObject.SetActive (false);
			else
				MinArrow.gameObject.SetActive (true);
		}

		if (MaxArrow) {
			if (TargetScrollbar.value >= 0.999f)
				MaxArrow.gameObject.SetActive (false);
			else
				MaxArrow.gameObject.SetActive (true);
		}
	}
}
