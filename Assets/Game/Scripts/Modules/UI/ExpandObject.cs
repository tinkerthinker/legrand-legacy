﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class ExpandObject : MonoBehaviour, ISelectHandler, IDeselectHandler {
	Rect _CurrRect;
	public float Multiplier=1.5f;
	private Vector3 _DefaultScale;

	public bool isSelectTrigger;

	void Awake()
	{
		_DefaultScale = GetComponent<RectTransform> ().transform.localScale;
		_CurrRect = GetComponent<RectTransform> ().rect;
	}

	public void Expand ()
	{
		//GetComponent<RectTransform> ().sizeDelta = new Vector2 (_CurrRect.width * Multiplier, _CurrRect.height * Multiplier);
		GetComponent<RectTransform> ().transform.localScale = new Vector3 (_DefaultScale.x * Multiplier,_DefaultScale.y * Multiplier, _DefaultScale.z);
	}

	public void Normal ()
	{
		//GetComponent<RectTransform> ().sizeDelta = new Vector2 (_CurrRect.width * 1,_CurrRect.height * 1);
		GetComponent<RectTransform> ().transform.localScale = new Vector3 (_DefaultScale.x,_DefaultScale.y, _DefaultScale.z);
	}

	#region ISelectHandler implementation

	public void OnSelect (BaseEventData eventData)
	{
		if(isSelectTrigger)
			Expand ();
	}

	#endregion

	#region IDeselectHandler implementation

	public void OnDeselect (BaseEventData eventData)
	{
		if(isSelectTrigger)
			Normal ();
	}

	#endregion
}
