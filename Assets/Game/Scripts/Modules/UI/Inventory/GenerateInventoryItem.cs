using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GenerateInventoryItem : AbsObjectListGenerator {
    //public ScrollController ScrollerController;
	public Button ItemButtonPrefab;

	public List<Item.Type> ItemTypeGenerated;
	public Item.UsabilityType UsabilityTypeGenerated;

	public Scrollbar scrollBarInventory;

	void Awake()
	{

	}

    public void UpdateInventory()
    {
        UpdateList();
    }

	public Button GetItemButton(string id)
	{
        /*
		foreach (Button inventoryButton in _ListItemButtons) {
			if(inventoryButton.GetComponent<ItemInventoryData>().ItemSlotData.ItemMaster._ID.Equals( id )){
				return inventoryButton;
			}
		}*/

        foreach (Button inventoryButton in _ListButtons)
        {
            if (inventoryButton.GetComponent<ItemInventoryData>().ItemSlotData.ItemMaster._ID.Equals(id))
            {
                return inventoryButton;
            }
        }

		return null;
	}

    public int GetItemButtonIndex(string id)
    {
        for (int i = 0; i < _ListButtons.Count; i++ )
        {
            if (_ListButtons[i].GetComponent<ItemInventoryData>().ItemSlotData.ItemMaster._ID.Equals(id))
            {
                return i;
            }
        }
        return -1;
    }

	public bool FocusToItemButton(string id)
	{
        for (int i = 0; i < _ListButtons.Count; i++)
        {
            if (_ListButtons[i].GetComponent<ItemInventoryData>().ItemSlotData.ItemMaster._ID.Equals(id))
            {
                _ListButtons[i].Select();
                return true;
            }
        }
		
		return false;
	}

    public override void StartLoopUpdate()
    {
        foreach (Item.Type type in ItemTypeGenerated)
        {
            //index = UpdateInventoryItemButton(type, index);
            foreach (var itemSlot in PartyManager.Instance.Inventory.Items[(int)type])
            {
                UpdateEachButton(itemSlot);
            }
        }
    }

    public override bool CheckConditionPreUpdate(object data)
    {
        if (!(data is ItemSlot))
            return false;

        Item item = ((ItemSlot)data).ItemMaster;

        if (UsabilityTypeGenerated != Item.UsabilityType.Normal && UsabilityTypeGenerated != Item.UsabilityType.AllCraftable)
        {
            if (item.Usability != UsabilityTypeGenerated)
                return false;
        }
        else if (UsabilityTypeGenerated == Item.UsabilityType.AllCraftable)
        {
            if (item.Usability != Item.UsabilityType.WeaponCraftable && item.Usability != Item.UsabilityType.ArmorCraftable)
                return false;
        }

        return true;
    }

    public override void SetButtonName(Button button, object data)
    {
        button.name = ((ItemSlot)data).ItemMaster.Name;
    }

    public override void AfterDataUpdatedProcess(IObjectData objectData, object data)
    {
        
    }
}
