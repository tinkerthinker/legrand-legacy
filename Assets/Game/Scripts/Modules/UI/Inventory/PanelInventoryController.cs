using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using TeamUtility.IO;

public class PanelInventoryController : MonoBehaviour {
	[System.Serializable]
	public class TabInventoryType
	{
		public string ItemGeneratedName;
		public List<Item.Type> ItemTypeGenerated;
		public Item.UsabilityType UsabilityType;
	}

	public enum InventoryState
	{
		InList,
		OpenActionList,
		OpenActionCommandSlot,
		Assign,
		ArrangeCharSlot
	}

//	[System.NonSerialized]
	private InventoryState _CurrentState;
	public InventoryState CurrentState
	{
		get{ return _CurrentState; }
	}
	public GameObject PanelInventoryList;
	public GameObject PanelCharacterSelection;
	public GameObject PanelActionOption;
	public GameObject AmountSet;
	public Text AmountSetText;

    public ShiftTabController ShiftController;

    [System.NonSerialized]
    public Item AssignedItem;
	[System.NonSerialized]
	public Item CurrentItem;
    [System.NonSerialized]
    public GameObject ChoosenItemObject;
	[System.NonSerialized]
	public bool IsSwapping = false;
	[System.NonSerialized]
	public AssignCommandMagicItemToChar AssignCommandToArrange;

	private static PanelInventoryController s_Instance;
	public static PanelInventoryController Instance
	{
		get{
			if (s_Instance == null)
				s_Instance = GameObject.FindObjectOfType(typeof(PanelInventoryController)) as PanelInventoryController;
			return s_Instance;
		}
	}

	public TabInventoryType[] TabInventoryTypes;
	public Image[] TabInventoryImages;
	public Text ItemGeneratedText;
	public Color SelectedColor;
	public Color DeselectedColor;

	private GenerateInventoryItem _GenInvent;
	private int _CurrTabInventory = 0;

	private bool _IsShowActiveMembers = true;

	void Awake()
	{
        EventManager.Instance.AddListener<SelectItemEvent>(SelectItemEventHandler);
		_GenInvent = PanelInventoryList.GetComponent<GenerateInventoryItem> ();
        ShiftController.RegisterPreShiftFunction(PreShiftTabAction);
        ShiftController.RegisterShiftingFunction(ShiftTabInventory, TabInventoryTypes.Length - 1);
	}

	void OnEnable()
	{
		EventManager.Instance.AddListener<AssignNewCommandEvent> (AssignNewCommandHandler);
        EventManager.Instance.AddListener<InspectItemEvent>(InspectItemEventHandler);
	}

	void OnDisable()
	{
        if (EventManager.Instance)
        {
            EventManager.Instance.RemoveListener<AssignNewCommandEvent>(AssignNewCommandHandler);
            EventManager.Instance.RemoveListener<InspectItemEvent>(InspectItemEventHandler);
        }
	}

    void OnDestroy()
    {
        if (EventManager.Instance)
        {
            EventManager.Instance.RemoveListener<SelectItemEvent>(SelectItemEventHandler);
        }
    }

	void Update()
	{
        if (InputManager.GetButtonDown ("Menu") && _CurrentState == InventoryState.InList) {
			_CurrentState = InventoryState.ArrangeCharSlot;
			EventManager.Instance.TriggerEvent (new InspectItemEvent (null, false));

            PanelCharacterSelection.GetComponent<GenerateCharacterSelection>().ShowCharactersCommandItem();
			GoToSelectChar();
			ChangeShownMembersConfirm (ActionInventoryButton.InventoryAction.ArrangeItemSlot);
		}
		else if (InputManager.GetButtonDown ("Delete") && _CurrentState == InventoryState.InList) {
			ShowCharacter (!_IsShowActiveMembers);
		}
	}

    bool PreShiftTabAction()
    {
        return _CurrentState == InventoryState.InList;
    }

    public void ShiftTab(int index)
    {
        ShiftController.ShiftTabTo(index);
    }

    public void ShiftTabInventory(int index)
    {
        _GenInvent.ItemTypeGenerated = TabInventoryTypes[index].ItemTypeGenerated;
        _GenInvent.UsabilityTypeGenerated = TabInventoryTypes[index].UsabilityType;

        if (ItemGeneratedText)
            ItemGeneratedText.text = TabInventoryTypes[index].ItemGeneratedName;

        EventSystem.current.SetSelectedGameObject(gameObject);

        EventManager.Instance.TriggerEvent(new InspectItemEvent(null, false));

        UpdateInventory();
        TryToFocusToFirstButton();
    }

	void DisableAllTabInventory()
	{
		for (int i = 0; i < TabInventoryImages.Length; i++) {
			TabInventoryImages [i].color = DeselectedColor;
		}
	}

	void AssignNewCommandHandler(AssignNewCommandEvent e)
	{
		UpdateInventory ();
	}

    void InspectItemEventHandler(InspectItemEvent e)
    {
        CurrentItem = e.Item;
    }

	void SelectItemEventHandler(SelectItemEvent e)
	{
		AssignedItem = e.Item;
	}

	public void GoToInventory()
	{
		PauseUIController.Instance.ChangeState (PauseUIController.PauseUIControllerState.Inventory);		

//		PanelCharacterSelection.GetComponent<GenerateCharacterSelection> ().Generate (ActionInventoryButton.InventoryAction.UseToCharacter);
		_CurrTabInventory = 0;
//		_IsShowActiveMembers = true;
        //ShiftTabInventory (ref _CurrTabInventory, 0);
		ShowCharacter (true);
//		genInvent.UpdateInventory();
	}
	
	public void GoToMain()
	{
		DisableAllTabInventory ();
		PauseUIController.Instance.ChangeState (PauseUIController.PauseUIControllerState.MainMenu);
        //PauseUIController.Instance.InventoryButton.Select ();
	}

    public void TryToFocusToFirstButton()
    {
        GenerateInventoryItem genInvent = PanelInventoryList.GetComponent<GenerateInventoryItem>();

        if (!genInvent.FocusToFirstButton())
        {
            genInvent.scrollBarInventory.Select();
        }
    }

	void ShowCharacter(bool showActiveMember)
	{
		_IsShowActiveMembers = showActiveMember;

		if (_IsShowActiveMembers)
			PanelCharacterSelection.GetComponent<GenerateCharacterSelection> ().ShowActiveMembers ();
		else
			PanelCharacterSelection.GetComponent<GenerateCharacterSelection> ().ShowReserveMembers ();
	}

	void MakeNewCharacterSelection()
	{
		
	}

	public void UpdateInventory()
	{
		PanelInventoryList.GetComponent<GenerateInventoryItem> ().UpdateInventory();
	}

	public void BackToInventoryList()
	{
		GenerateInventoryItem genInvent = PanelInventoryList.GetComponent<GenerateInventoryItem> ();
        //if (ChoosenItem == null || !genInvent.FocusToItemButton (ChoosenItem._ID)) {
        if (AssignedItem == null || !genInvent.FocusToItemButton(AssignedItem._ID))
        {
			if (!genInvent.FocusToFirstButton ())
				genInvent.scrollBarInventory.Select ();
		}
		_CurrentState = InventoryState.InList;
		EventManager.Instance.TriggerEvent (new ComparingEvent(false));
//		PanelInventoryList.GetComponent<GenerateInventoryItem> ().FocusToFirstButton ();
	}

	public void GoToSelectChar()
	{
		PanelActionOption.SetActive (false);
		EnableShownMembersBtn ();
		PanelCharacterSelection.GetComponent<GenerateCharacterSelection> ().GoToFirst ();
	}

	public void BackFromSelectChar()
	{
		PanelActionOption.SetActive (true);
//		DisableShownMembersBtn ();
		EventManager.Instance.TriggerEvent (new ComparingEvent(false));
		PanelActionOption.GetComponentInChildren<Button> ().Select ();
	}

	public void EnableShownMembersBtn()
	{
		PanelCharacterSelection.GetComponent<GenerateCharacterSelection> ().EnableShownMembersBtn ();
	}

	public void DisableShownMembersBtn()
	{
		PanelCharacterSelection.GetComponent<GenerateCharacterSelection> ().DisableShownMembersBtn ();
	}

	public GameObject GetShownMembersObjectById(string id)
	{
		return PanelCharacterSelection.GetComponent<GenerateCharacterSelection> ().GetShownMemberObjectById (id);
	}

	public void ChangeShownMembersConfirm(ActionInventoryButton.InventoryAction confirmAction)
	{
		PanelCharacterSelection.GetComponent<GenerateCharacterSelection> ().ChangeConfirmAction (confirmAction);
	}

	public void ChangeState(InventoryState state)
	{
		_CurrentState = state;
	}

	public void UpdateAllCharacterCommand()
	{
		for (int i = 0; i < PauseUIController.Instance.ActiveMembers.Count; i++) {
			EventManager.Instance.TriggerEvent (new AssignNewCommandEvent (PauseUIController.Instance.ActiveMembers [i]));
		}
	}
}
