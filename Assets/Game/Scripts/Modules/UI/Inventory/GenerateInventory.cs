using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;

public class GenerateInventory : MonoBehaviour {
	int totalItemCount;

	private List<Button> listItemButtons=new List<Button>();

	[System.NonSerialized]
	public bool generateAll=false;

	public int seenRow;
	public int seenCol;
	private int numberOfStep;
	
	private float lastRow=1;
	private float lastCol=1;

	[SerializeField]
	private Button itemInventoryButton;
	public float buttonHeight;
	public Text textItemName;
	public Text textItemDescription;
	public Text textItemEffect;
	public Text textItemWeight;

	public Scrollbar scrollBarInventory;

	void Start()
	{

	}

	void GenerateInventoryItem()
	{
		for (int i=0; i<PartyManager.Instance.Inventory.Items.Length; i++) {
			GenerateInventoryItemButton((Item.Type)i);
		}

		if(listItemButtons.Count%seenCol > 0)
			numberOfStep = (int)(listItemButtons.Count/seenCol)+1 - (seenRow-1);
		else
			numberOfStep = (int)(listItemButtons.Count/seenCol) - (seenRow-1);
		
		foreach (var itemButton in listItemButtons) {
			itemButton.transform.SetParent(this.transform);
			itemButton.GetComponent<RectTransform> ().localScale = new Vector3(1,1,1);
			
			itemButton.GetComponent<ScrollUpDown>().seenRow = seenRow;
			itemButton.GetComponent<ScrollUpDown>().scrollBar = scrollBarInventory;
			itemButton.GetComponent<ScrollUpDown>().numberOfStep = numberOfStep;

			itemButton.GetComponent<FadingScrollbar>().ScrollbarAnimator = scrollBarInventory.GetComponent<Animator>();
		}
	}

	void GenerateInventoryItemButton(Item.Type itemType)
	{
		Button newInventoryItemButton;
		foreach (var itemSlot in PartyManager.Instance.Inventory.Items [(int)itemType])
		{
			Item item = itemSlot.ItemMaster;

			newInventoryItemButton = Instantiate (itemInventoryButton) as Button;
			newInventoryItemButton.name = item.Name;

			newInventoryItemButton.GetComponent<ItemInventoryData> ().ItemSlotData = itemSlot;
			newInventoryItemButton.GetComponent<ItemInventoryData> ().textItemDescription = textItemDescription;
			newInventoryItemButton.GetComponent<ItemInventoryData> ().textItemName = textItemName;
			newInventoryItemButton.GetComponent<ItemInventoryData> ().textItemWeight = textItemWeight;

			listItemButtons.Add (newInventoryItemButton);

			newInventoryItemButton.GetComponent<ScrollUpDown>().Index = (int)((listItemButtons.Count-1)/seenCol)+1;
		}
	}

	void PutItemListButton(int indexItemList, Item.Type itemType)
	{
		string nameItemListButton = "ItemInventoryButton";
		//Consumable temp = (Consumable)PartyManager.Instance.Inventory.Items [(int)itemType] [indexItemList].ItemMaster;
			
			Button newItemListButton = Instantiate (itemInventoryButton) as Button;
			newItemListButton.transform.SetParent (this.transform);
			newItemListButton.name = nameItemListButton;
			//newItemListButton.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (x, y);
			newItemListButton.GetComponent<RectTransform> ().localScale = new Vector3 (1, 1, 1);

			//newItemListButton.GetComponent<Image>().sprite = DatabaseImageSprite.instance.GetSpriteItemByName(listItemSprite[indexItemList]);
			//newItemListButton.GetComponentsInChildren<Image> ()[1].sprite = DatabaseImageSprite.instance.GetSpriteItemByName ("pot 1");

			//newItemListButton.GetComponentsInChildren<Image> (true)[2].sprite = DatabaseImageSprite.instance.GetSpriteItemByType(itemType);
			
			Text[] itemListTexts = newItemListButton.GetComponentsInChildren<Text> ();
			/*itemListTexts [0].text = PartyManager.Instance.Inventory.Items [(int)itemType] [indexItemList].ItemMaster.Name;
			itemListTexts [1].text = "x" + PartyManager.Instance.Inventory.Items [(int)itemType] [indexItemList].Stack;*/
			
			newItemListButton.GetComponent<ItemInventoryData> ().ItemSlotData = PartyManager.Instance.Inventory.Items [(int)itemType] [indexItemList];
			newItemListButton.GetComponent<ItemInventoryData> ().textItemDescription = textItemDescription;

		//newItemListButton.GetComponent<ScrollUpDown> ().numberOfStep = 2;
		newItemListButton.GetComponent<ScrollUpDown> ().Index = indexItemList+1;
		newItemListButton.GetComponent<ScrollUpDown> ().scrollBar = scrollBarInventory;
			listItemButtons.Add (newItemListButton);
		//}
	}

	void CalculateHeight()
	{
		int totalRow = (int)(listItemButtons.Count / seenCol);

		if(listItemButtons.Count%seenCol > 0)
			totalRow++;
		
		float height;

		height = totalRow * buttonHeight;

		GetComponent<RectTransform> ().sizeDelta = new Vector2(GetComponent<RectTransform> ().sizeDelta.x, height); // << Change Rect Transform Height
	}

	void RemoveItemList()
	{
		foreach (Button button in listItemButtons) {
			Destroy(button.gameObject);
		}
		listItemButtons.RemoveAll ((Button obj) => obj);
	}
	
	public void LoadInventory()
	{
		totalItemCount = 0;
		totalItemCount += PartyManager.Instance.Inventory.Items[(int)Item.Type.Consumable].Count;
		
		//ChangeInteractableObject.DisableAllButtonInParent(ChangeTabPanel.instance.panelItem);
		//ChangeInteractableObject.DisableAllButtonInParent(PauseUIController.instance.panelCharacter);

		RemoveItemList ();

		GenerateInventoryItem ();

		CalculateHeight ();

		scrollBarInventory.size = 0;
		/*
		for (int i=0; i<totalItemCount; i++) {
			//if (listItemAmount [i] > 0)
			PutItemListButton (i, Item.Type.Consumable);
		}

		numberOfStep = listItemButtons.Count - (seenButton-1); 
		foreach (var itemButton in listItemButtons) {
			itemButton.GetComponent<ScrollUpDown>().seenButton = seenButton;
			itemButton.GetComponent<ScrollUpDown>().numberOfStep = numberOfStep;

		}*/
		//if(listItemButtons.Count > 0)
		//	listItemButtons[0].GetComponent<Button>().Select();
	}

	public bool FocusToIndexButton(string index)
	{
		/*
		if (magicButtons.Count>0 && magicButtons [index] != null) {
			magicButtons [index].Select ();
			return true;
		}*/
		int i = 0;
		foreach (Button button in listItemButtons) {
			if(button.GetComponent<ItemInventoryData>().ItemSlotData.ItemMaster._ID .Equals(index) )
			{
				button.Select();
				return true;
			}
			i++;
		}
		return false;
	}

	public bool FocusToFirstButton()
	{
		if (listItemButtons.Count>0 && listItemButtons [0] != null) {
			listItemButtons [0].Select ();
			scrollBarInventory.value = 1;
			return true;
		}
		return false;
	}
}
