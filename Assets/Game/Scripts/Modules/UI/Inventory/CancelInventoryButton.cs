using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class CancelInventoryButton : MonoBehaviour,  ICancelHandler{
	private NavigationButtonData _NavigationData;

	void Start()
	{
		_NavigationData = GetComponent<NavigationButtonData> ();
	}

	public void OnCancel(BaseEventData eventData)
	{
//		OnCancelHandler ();
		OnCancel();
	}
	public void OnCancel()
	{
//		OnCancelHandler ();
		if (PanelInventoryController.Instance.CurrentState == PanelInventoryController.InventoryState.OpenActionCommandSlot) {
			PanelInventoryController.Instance.AssignCommandToArrange.GetComponent<Button> ().Select ();
			PanelInventoryController.Instance.ChangeState (PanelInventoryController.InventoryState.ArrangeCharSlot);
			HidePanelAction ();
		} else {
			NewOnCancelHandler ();
		}
	}

	public void OnCancelFromSubmit()
	{
		GameObject panelInventory = PauseUIController.Instance.PanelInventory;

		Item item = PauseUIController.Instance.currInventoryItem;
		panelInventory.GetComponent<PanelInventoryController> ().PanelCharacterSelection.SetActive (false);

		ChangeInteractableObject.EnableAllButtonInParent (panelInventory.GetComponent<PanelInventoryController> ().PanelInventoryList);

		if (!panelInventory.GetComponentInChildren<GenerateInventory> ().FocusToIndexButton (item._ID))
		if (! panelInventory.GetComponentInChildren<GenerateInventory> ().FocusToFirstButton ())
		{
			panelInventory.GetComponent<PanelInventoryController> ().PanelInventoryList.GetComponent<GenerateInventory>().scrollBarInventory.Select();
		}
	}

	public void NewOnCancelFromSubmit()
	{
		GameObject panelInventory = PauseUIController.Instance.PanelInventory;
		
		Item item = PauseUIController.Instance.currInventoryItem;
		panelInventory.GetComponent<PanelInventoryController> ().PanelCharacterSelection.SetActive (false);
		
		ChangeInteractableObject.EnableAllButtonInParent (panelInventory.GetComponent<PanelInventoryController> ().PanelInventoryList);
		
		if (!panelInventory.GetComponentInChildren<GenerateInventory> ().FocusToIndexButton (item._ID))
			if (! panelInventory.GetComponentInChildren<GenerateInventory> ().FocusToFirstButton ())
		{
			panelInventory.GetComponent<PanelInventoryController> ().PanelInventoryList.GetComponent<GenerateInventory>().scrollBarInventory.Select();
		}
	}

	void OnCancelHandler()
	{
		GameObject panelInventory = PauseUIController.Instance.PanelInventory;

		ActionInventoryButton.InventoryAction lastAction = GetComponent<ActionInventoryButton> ().Action;
		if (lastAction == ActionInventoryButton.InventoryAction.OpenActionOption || lastAction == ActionInventoryButton.InventoryAction.Use)
		{
			PauseUIController.Instance.ChangeState(PauseUIController.PauseUIControllerState.MainMenu);
			PauseUIController.Instance.InventoryButton.Select ();
		}
		/*
		else if (lastAction == ActionInventoryButton.InventoryAction.UseToCharacter || 
		         lastAction == ActionInventoryButton.InventoryAction.AssignToCharacterAmount)
		{
			panelInventory.GetComponent<PanelInventoryChildren> ().PanelCharacterSelection.SetActive (false);

			ActionInventoryButton.InventoryAction charSelAction;
			switch(lastAction)
			{
			case ActionInventoryButton.InventoryAction.UseToCharacter:
				charSelAction = ActionInventoryButton.InventoryAction.Use;
				break;
			case ActionInventoryButton.InventoryAction.AssignToCharacter:
				charSelAction = ActionInventoryButton.InventoryAction.Assign;
				break;
			case ActionInventoryButton.InventoryAction.DropAmount:
				charSelAction = ActionInventoryButton.InventoryAction.Drop;
				break;
			default :
				charSelAction = ActionInventoryButton.InventoryAction.Use;
				break;
			}

			foreach(Button button in panelInventory.GetComponent<PanelInventoryChildren> ().PanelActionOption.GetComponentsInChildren<Button>())
			{
				if(button.GetComponent<ActionInventoryButton>().action == charSelAction)
					button.Select();
			}
		}
		*/
		else if (lastAction == ActionInventoryButton.InventoryAction.Use || 
		         lastAction == ActionInventoryButton.InventoryAction.UseToCharacter ||
		         lastAction == ActionInventoryButton.InventoryAction.DropAmount)
		{
			Item item = PauseUIController.Instance.currInventoryItem;

			panelInventory.GetComponent<PanelInventoryController> ().PanelCharacterSelection.SetActive (false);

			ChangeInteractableObject.EnableAllButtonInParent (panelInventory.GetComponent<PanelInventoryController> ().PanelInventoryList);

			if(!panelInventory.GetComponent<PanelInventoryController>().PanelInventoryList.GetComponent<GenerateInventory>().FocusToIndexButton(item._ID))
				panelInventory.GetComponent<PanelInventoryController>().PanelInventoryList.GetComponent<GenerateInventory>().FocusToFirstButton();

		}
		else if(lastAction == ActionInventoryButton.InventoryAction.Drop)
		{
			//hide amount set
			panelInventory.GetComponent<PanelInventoryController>().AmountSet.SetActive(false);

			//change action to drop amount
			//GetComponent<ActionInventoryButton>().action = ActionInventoryButton.InventoryAction.DropAmount;
			GetComponent<ActionInventoryButton>().Action = ActionInventoryButton.InventoryAction.Use;

			//set navigation button again to vertical, , the default navigation
			Navigation vertMode = GetComponent<Button>().navigation;
			//vertMode.mode = Navigation.Mode.Vertical;
			vertMode.mode = Navigation.Mode.Automatic;
			GetComponent<Button>().navigation = vertMode;
		}
	}

	void NewOnCancelHandler()
	{
		ActionInventoryButton.InventoryAction lastAction = GetComponent<ActionInventoryButton> ().Action;

		if (lastAction != ActionInventoryButton.InventoryAction.Drop)
			HidePanelAction ();

		if (lastAction == ActionInventoryButton.InventoryAction.OpenActionOption) {
//			PauseUIController.Instance.ChangeState (PauseUIController.PauseUIControllerState.MainMenu);
//			PauseUIController.Instance.InventoryButton.Select ();
			PanelInventoryController.Instance.GoToMain ();
		} else if (lastAction == ActionInventoryButton.InventoryAction.Use ||
		           lastAction == ActionInventoryButton.InventoryAction.Assign ||
		           lastAction == ActionInventoryButton.InventoryAction.DropAmount) {

            //GenerateInventoryItem genInvent = PanelInventoryController.Instance.PanelInventoryList.GetComponent<GenerateInventoryItem> ();

            //Button targetButton = genInvent.GetItemButton (PanelInventoryController.Instance.ChoosenItem._ID);

                       //PanelInventoryController.Instance.BackToInventoryList();
            /*
			if (targetButton != null) {
				//1st ways
//				targetButton.enabled = true;
//				targetButton.Select();

				//2nd ways
				ColorBlock tempCB = targetButton.colors;
				tempCB.normalColor = Color.clear;
				targetButton.colors = tempCB;

				targetButton.Select ();
			} */
//			else if (!genInvent.FocusToFirstButton ()) {
//				genInvent.scrollBarInventory.Select ();
//			}
			PanelInventoryController.Instance.BackToInventoryList();

//			if (!genInvent.FocusToItemButton (PanelInventoryController.Instance.ChoosenItem._ID)) {
//				if (!genInvent.FocusToFirstButton ()) {
//					genInvent.scrollBarInventory.Select ();
//				}
//			}
		} else if (lastAction == ActionInventoryButton.InventoryAction.UseToCharacter ||
		          lastAction == ActionInventoryButton.InventoryAction.AssignToCharacter) {
			PanelInventoryController.Instance.BackFromSelectChar ();
		} else if (lastAction == ActionInventoryButton.InventoryAction.Drop) {
			PopUpUI.CloseAllPopUp ();
			if (_NavigationData != null)
				_NavigationData.ChangeToLastNavigation ();

			GetComponent<ActionInventoryButton> ().Action = ActionInventoryButton.InventoryAction.DropAmount;
		} else if (lastAction == ActionInventoryButton.InventoryAction.ArrangeItemSlot) {
			if(PauseUIController.Instance.PauseUIState == PauseUIController.PauseUIControllerState.Inventory)
				PanelInventoryController.Instance.BackToInventoryList ();
		}

	}

	void HidePanelAction()
	{
		PanelInventoryController.Instance.PanelActionOption.SetActive (false);
	}
}
