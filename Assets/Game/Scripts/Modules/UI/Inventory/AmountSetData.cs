﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using TeamUtility.IO;

public class AmountSetData : MonoBehaviour {
	private int _Amount=1;
	public int Amount
	{
		get{return _Amount;}
	}
	public const int MinAmount = 1;
    private int _MaxAmount = 10;
	public int MaxAmount
    {
        set { _MaxAmount = value < 1? 1:value;}
        get { return _MaxAmount; }
    }

	public GameObject AmountSetRoot;
	public Text AmountSetText;
	public Image LeftArrow;
	public Image RightArrow;

	public Sprite ArrowNormal;
	public Color ColorNormal = new Color(1,1,1,1);
	public Sprite ArrowHighlight;
	public Color ColorHighlight = new Color(1,1,1,1);

	public int HighlightLiveTimeFrame = 1;

    public WwiseManager.UIFX ShiftSound;

	void OnEnable () {
		_Amount = 1;
		UpdateAmount ();
	}
	
	// Update is called once per frame
	void Update () {
		if (this.isActiveAndEnabled) {
			if (InputManager.anyKey || InputManager.AnyInput())
			{
				TeamUtility.IO.StandaloneInputModule module = EventSystem.current.GetComponent<TeamUtility.IO.StandaloneInputModule>();
				bool allow = module.allowMoveEvent;

				if (LegrandUtility.GetAxisRawHorizontal() > 0.5f && allow) {
                    if (_Amount == MaxAmount)
                        return;
                    _Amount++;
                    HighLightArrow (true);
					UpdateAmount();
					
				} else if (LegrandUtility.GetAxisRawHorizontal() < -0.5f && allow) {
                    if (_Amount == MinAmount)
                        return;
					_Amount--;
                    HighLightArrow (false);
					UpdateAmount();
				}

				if (LegrandUtility.GetAxisRawVertical() > 0.5f && allow) {
                    if (_Amount == MaxAmount)
                        return;
					_Amount+=10;
                    HighLightArrow (true);
					UpdateAmount();

//					if (_Amount > MaxAmount)
//						_Amount = MaxAmount;

					
				} else if (LegrandUtility.GetAxisRawVertical() < -0.5f && allow) {
                    if (_Amount == MinAmount)
                        return;
					_Amount-=10;
				    HighLightArrow (false);
					UpdateAmount();

//					if (_Amount < 1)
//						_Amount = 1;

				}
				
			}
		}
	}

	public void UpdateAmount()
	{
		_Amount = Mathf.Clamp(_Amount, MinAmount, _MaxAmount);
//		if (_Amount > MaxAmount)
//			_Amount = 1;
//		else if (_Amount < 1)
//			_Amount = MaxAmount;
		AmountSetText.text = _Amount.ToString();

        if(_Amount == MinAmount)
        {
            SetLeftArrow(false);
        }
        else
        {
            SetLeftArrow(true);
        }
        if(_Amount == MaxAmount)
        {
            SetRightArrow(false);
        }
        else
        {
            SetRightArrow(true);
        }

        /*
		if (_Amount == MinAmount) {
			if (LeftArrow)
				LeftArrow.gameObject.SetActive (false);
			if (RightArrow)
				RightArrow.gameObject.SetActive (true);
		} else if (_Amount == _MaxAmount) {
			if (LeftArrow)
				LeftArrow.gameObject.SetActive (true);
			if (RightArrow)
				RightArrow.gameObject.SetActive (false);
		} else {
			if (LeftArrow)
				LeftArrow.gameObject.SetActive (true);
			if (RightArrow)
				RightArrow.gameObject.SetActive (true);
		}*/

		EventManager.Instance.TriggerEvent(new SetAmountEvent(_Amount));
	}

	void HighLightArrow(bool IsRight)
	{
		if (IsRight) {
			if (RightArrow)
				StartCoroutine (HighLightLive (RightArrow));
		} else {
			if (LeftArrow)
				StartCoroutine (HighLightLive (LeftArrow));
		}
        WwiseManager.Instance.PlaySFX(ShiftSound);
	}

    void SetRightArrow(bool isActive)
    {
        if (RightArrow)
            RightArrow.gameObject.SetActive(isActive);
    }

    void SetLeftArrow(bool isActive)
    {
        if (LeftArrow)
            LeftArrow.gameObject.SetActive(isActive);
    }

	IEnumerator HighLightLive(Image target)
	{
		target.sprite = ArrowHighlight;
		target.color = ColorHighlight;
		yield return WaitFor.Frames(HighlightLiveTimeFrame);
		target.sprite = ArrowNormal;
		target.color = ColorNormal;
	}
}
