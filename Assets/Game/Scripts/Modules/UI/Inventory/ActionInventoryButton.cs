using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;
using TeamUtility.IO;

public class ActionInventoryButton : MonoBehaviour{
	public enum InventoryAction
	{
		Use,
		Drop,
		OpenActionOption,
		OpenCharacterList,
		OpenCommandSlotOption,
		UseToCharacter,
		DropAmount,
		Assign,
		AssignToCharacter,
		AssignToCommandSlot,
		ArrangeItemSlot,
		SwapCommand,
		ConfirmSwap,
		ReturnItem
	}
	public InventoryAction Action;

    public WwiseManager.UIFX ForbiddenActionSound;

	private string _IdChar;
	private int _actionAmount;

	private NavigationButtonData _NavigationData;

	void Start () {
		if (GetComponent<IDChar> () != null)
			_IdChar = GetComponent<IDChar> ()._ID;
		GetComponent<Button> ().onClick.AddListener (() => OnClickHandler ());
		_NavigationData = GetComponent<NavigationButtonData> ();
	}

	void SetAmountEventHandler(SetAmountEvent e)
	{
		_actionAmount = e.Amount;
	}

	void OnClickHandler()
	{
		Item itemData = PauseUIController.Instance.currInventoryItem;
		Navigation navTemp = GetComponent<Button>().navigation;
		MainCharacter selectedChar = PartyManager.Instance.GetCharacter (PauseUIController.Instance.selectedCharId);
		AssignCommandMagicItemToChar assign = PanelInventoryController.Instance.AssignCommandToArrange;
		GameObject panelCommand;

		switch (Action)
		{
		case InventoryAction.Use:
			itemData = PauseUIController.Instance.currInventoryItem;

			PanelInventoryController.Instance.ChangeShownMembersConfirm(InventoryAction.UseToCharacter);
			PanelInventoryController.Instance.GoToSelectChar ();
			break;
		case InventoryAction.Drop:
			EventManager.Instance.RemoveListener<SetAmountEvent>(SetAmountEventHandler);

			PopUpUI.CloseAllPopUp(); // close set amount popup

			//then call choice popup
            PopUpUI.CallChoicePopUp("Discard " + _actionAmount + " " + PanelInventoryController.Instance.CurrentItem.Name + " ?", DropItem);

			Action = InventoryAction.DropAmount;

			if(_NavigationData != null)
				_NavigationData.ChangeToLastNavigation();

			break;
		case InventoryAction.DropAmount:
			// show amount set to set how much item will drop
			int maxAmount=1;
			foreach (ItemSlot itemSlot in PartyManager.Instance.Inventory.Items [(int)itemData.ItemType]) {
				if(itemSlot.ItemMaster._ID == itemData._ID)
				{
					maxAmount = itemSlot.Stack;
					break;
				}
			}
            EventManager.Instance.AddListener<SetAmountEvent>(SetAmountEventHandler);
			PopUpUI.CallSetAmountPopUp(maxAmount);

			if(_NavigationData != null)
				_NavigationData.ChangeToNone(GetComponent<Button>());
			
			//then change action mode to drop
			Action = InventoryAction.Drop;
			break;
		case InventoryAction.OpenActionOption:
			OpenActionMenu ();
			break;
		case InventoryAction.UseToCharacter:
			if (PanelInventoryController.Instance.CurrentState == PanelInventoryController.InventoryState.OpenActionList) {
				if (UseItem (true)) {
                    EventManager.Instance.TriggerEvent(new InventoryChangedEvent());

					if (GetComponent<ShowCharacterHealth> () != null)
						GetComponent<ShowCharacterHealth> ().RefreshData ();
					if (GetComponent<ShowCharacterAttribute> () != null)
					{
						if(GetComponent<IDChar>() != null)
							GetComponent<ShowCharacterAttribute> ().RefreshData (_IdChar);
					}
					PanelInventoryController.Instance.UpdateAllCharacterCommand ();
					PanelInventoryController.Instance.UpdateInventory ();
					if (!PartyManager.Instance.Inventory.Contains (PanelInventoryController.Instance.AssignedItem)) {
                        PanelInventoryController.Instance.PanelInventoryList.SetActive(false);
                        PanelInventoryController.Instance.PanelInventoryList.SetActive(true);
						PanelInventoryController.Instance.BackToInventoryList ();
						PanelInventoryController.Instance.DisableShownMembersBtn ();
					}
				} else {
                    PanelInventoryController.Instance.PanelInventoryList.SetActive(false);
                    PanelInventoryController.Instance.PanelInventoryList.SetActive(true);
					PanelInventoryController.Instance.BackToInventoryList ();
					PanelInventoryController.Instance.DisableShownMembersBtn ();
				}
			} else if (PanelInventoryController.Instance.CurrentState == PanelInventoryController.InventoryState.OpenActionCommandSlot) {
				if (UseItem (false)) {
					string idChar = PauseUIController.Instance.selectedCharId;
					GameObject charObj = PanelInventoryController.Instance.GetShownMembersObjectById (idChar);

					if (charObj.GetComponent<ShowCharacterHealth> () != null)
						charObj.GetComponent<ShowCharacterHealth> ().RefreshData ();

					PanelInventoryController.Instance.AssignCommandToArrange.GetComponent<CommandButtonSet> ().SetCommand ();
				} else {
					GetComponent<CancelInventoryButton> ().OnCancel ();
				}
			}
			break;
		case InventoryAction.Assign:
			PanelInventoryController.Instance.ChangeShownMembersConfirm(InventoryAction.AssignToCharacter);
			PanelInventoryController.Instance.GoToSelectChar ();
			EventManager.Instance.TriggerEvent (new ComparingEvent(true));
			break;
		case InventoryAction.AssignToCharacter:
			GoToCommandItem ();

			if (GetComponent<CharacterPreview> () != null) {
				GameObject panelCommandItem = GetComponent<CharacterPreview> ().PanelCommandItem;

				foreach (ActionInventoryButton act in panelCommandItem.GetComponentsInChildren<ActionInventoryButton>()) {
					act.Action = InventoryAction.AssignToCommandSlot;
				}
			}
			break;
		case InventoryAction.ArrangeItemSlot:
			GoToCommandItem();

			if (GetComponent<CharacterPreview> () != null) {
				GameObject panelCommandItem = GetComponent<CharacterPreview> ().PanelCommandItem;

				foreach (ActionInventoryButton act in panelCommandItem.GetComponentsInChildren<ActionInventoryButton>()) {
					act.Action = InventoryAction.OpenCommandSlotOption;
				}
			}
			break;
		case InventoryAction.AssignToCommandSlot:
			if (GetComponent<AssignCommandMagicItemToChar> () != null) {
				GetComponent<AssignCommandMagicItemToChar> ().AssignCommand ();
			}
			break;
		case InventoryAction.OpenCommandSlotOption:
			OpenActionMenu ();
			break;
		case InventoryAction.SwapCommand:
			assign.SwapCommand ();

			panelCommand = PanelInventoryController.Instance.GetShownMembersObjectById (PauseUIController.Instance.selectedCharId).GetComponent<CharacterPreview> ().PanelCommandItem;

			foreach (ActionInventoryButton act in panelCommand.GetComponentsInChildren<ActionInventoryButton>()) {
				act.Action = InventoryAction.ConfirmSwap;
			}

			GetComponent<CancelInventoryButton> ().OnCancel ();
			break;
		case InventoryAction.ConfirmSwap:
			if (GetComponent<AssignCommandMagicItemToChar> () != null)
				assign = GetComponent<AssignCommandMagicItemToChar> ();
			assign.SwapCommand ();

			panelCommand = PanelInventoryController.Instance.GetShownMembersObjectById (PauseUIController.Instance.selectedCharId).GetComponent<CharacterPreview> ().PanelCommandItem;

			foreach (ActionInventoryButton act in panelCommand.GetComponentsInChildren<ActionInventoryButton>()) {
				act.GetComponent<CommandButtonSet> ().SetCommand ();
				act.Action = InventoryAction.OpenCommandSlotOption;
			}

			break;
		case InventoryAction.ReturnItem:
			selectedChar.ReturnItem (selectedChar.GetCommand (assign.panelPosition, assign.slotPosition).CommandValue);
			assign.GetComponent<CommandButtonSet> ().SetCommand ();

			PanelInventoryController.Instance.UpdateInventory();

			GetComponent<CancelInventoryButton> ().OnCancel ();
			break;
		}

	}

	bool UseItem(bool isFromInventory)
	{
		string characterID = PauseUIController.Instance.selectedCharId;

		Item itemData = PanelInventoryController.Instance.AssignedItem;
		List<string> listIdCharEffected=new List<string>();
		if (itemData == null || itemData.ItemType != Item.Type.Consumable)
			return false;
		else {
			Consumable consumableItem = (Consumable)itemData;

            listIdCharEffected = SetTargetUseItem(consumableItem, characterID);

            if(consumableItem.ConsumableType == Consumable.ConsumeType.Healing)
            {
                foreach (ConsumableHealing.RestoredAttribute attrib in ((ConsumableHealing)consumableItem).RestoredAttributes)
                {
                    if (attrib._Attribute == Attribute.Health)
                    {
                        for (int i = 0; i < listIdCharEffected.Count; i++ )
                        {
                            Health health = PartyManager.Instance.GetCharacter(listIdCharEffected[i]).Health;
                            if(health.Value >= health.MaxValue)
                            {
                                WwiseManager.Instance.PlaySFX(ForbiddenActionSound);
                                return true;
                            }
                        }
                        break;
                    }
                }
            }

			if (isFromInventory) {
				if (!PartyManager.Instance.Inventory.UseItem (itemData._ID, listIdCharEffected)) {
					return false;
				}
			} else {
				MainCharacter chara = PartyManager.Instance.GetCharacter (characterID);
				int indexItemSlot = PanelInventoryController.Instance.AssignCommandToArrange.valueId;

				if (chara.UseItem (indexItemSlot, listIdCharEffected)) {
					return false;
				}
			}
			return true;
		}
	}

	bool UseItemCharacterSlot()
	{
		string characterID = PauseUIController.Instance.selectedCharId;

		Item itemData = PanelInventoryController.Instance.CurrentItem;
		List<string> listIdCharEffected=new List<string>();
		if (itemData == null || itemData.ItemType != Item.Type.Consumable)
			return false;
		else {
			Consumable consumableItem = (Consumable)itemData;

			SetTargetUseItem (consumableItem, characterID);

			if(!PartyManager.Instance.Inventory.UseItem (itemData._ID , listIdCharEffected)){
				return false;
			}
			Debug.Log ("Use Item " + itemData.Name + " Success");
			return true;
		}
	}

	List<string> SetTargetUseItem(Consumable consumableItem, string charId)
	{
		List<string> listIdCharEffected=new List<string>();

		switch (consumableItem.Target) {
		case TargetType.Single:
			listIdCharEffected.Add (charId);
			break;
		case TargetType.Column:
			//break;
		case TargetType.Row:
			//break;
		case TargetType.Square:
			//break;
		case TargetType.All:
			foreach (MainCharacter chara in PartyManager.Instance.CharacterParty) {
				listIdCharEffected.Add (chara._ID);
			}
			break;
		}
		return listIdCharEffected;
	}


	void DropItem()
	{
		StartCoroutine (WaitToDrop ());
	}

	IEnumerator WaitToDrop()
	{
		yield return null;
		Item item = PanelInventoryController.Instance.CurrentItem;
		PartyManager.Instance.Inventory.DeleteItemsByItemID (item._ID, _actionAmount);
		PanelInventoryController.Instance.UpdateInventory();
        PanelInventoryController.Instance.UpdateAllCharacterCommand();

        EventManager.Instance.TriggerEvent(new InventoryChangedEvent());

		if (!PartyManager.Instance.Inventory.Contains (item)) {
            PanelInventoryController.Instance.PanelInventoryList.SetActive(false);
            PanelInventoryController.Instance.PanelInventoryList.SetActive(true);
			GetComponent<CancelInventoryButton> ().OnCancel ();
			PanelInventoryController.Instance.BackToInventoryList ();
		}
	}

	void OpenActionMenu()
	{
		Item choosenItem = PanelInventoryController.Instance.CurrentItem;

        //if (Action == InventoryAction.OpenActionOption)
        //{
            if (choosenItem == null || choosenItem.ItemType == Item.Type.Key)
                return;
        //}

		GameObject panelActionOption = PanelInventoryController.Instance.PanelActionOption;


		Vector3 temp = panelActionOption.transform.position;
		temp.y = transform.position.y;
		panelActionOption.transform.position = temp;

		panelActionOption.SetActive (true);

		Button[] listButton = panelActionOption.GetComponentsInChildren<Button> (true); //0 = Use, 1 = Assign, 2 = Swap, 3 = Return Item, 4 = Drop
		if (Action == InventoryAction.OpenActionOption) {
			listButton [2].gameObject.SetActive (false);
			listButton [3].gameObject.SetActive (false);


			if (choosenItem.ItemType == Item.Type.Key)
				listButton [4].gameObject.SetActive (false);
			else
				listButton [4].gameObject.SetActive (true);
		
			if (choosenItem.ItemType == Item.Type.Consumable) {
                //listButton[0].gameObject.GetComponent<ActionInventoryButton>().Action = InventoryAction.Use;
                if (((Consumable)choosenItem).ConsumableType == Consumable.ConsumeType.Healing)
                {
                    bool isCanUseItem = false;
                    foreach (ConsumableHealing.RestoredAttribute attrib in ((ConsumableHealing)choosenItem).RestoredAttributes)
                    {
                        if (attrib._Attribute == Attribute.ReviveHealth)
                        {
                            isCanUseItem = false;
                            break;
                        }
                        else if(attrib._Attribute == Attribute.Health)
                        {
                            isCanUseItem = true;
                        }
                    }

                    if(isCanUseItem)
                        listButton[0].gameObject.SetActive(true);
                    else
                        listButton[0].gameObject.SetActive(false);
                }
                else if (((Consumable)choosenItem).ConsumableType == Consumable.ConsumeType.Attributes)
                {
                    listButton[0].gameObject.SetActive(true);
                }
                else
                {
                    listButton[0].gameObject.SetActive(false);
                }

				if (((Consumable)choosenItem).ConsumableType != Consumable.ConsumeType.Attributes)
					listButton [1].gameObject.SetActive (true);
				else
					listButton [1].gameObject.SetActive (false);
			} else {
				listButton [0].gameObject.SetActive (false);
				listButton [1].gameObject.SetActive (false);
			}

			PanelInventoryController.Instance.ChangeState (PanelInventoryController.InventoryState.OpenActionList);
		} else if (Action == InventoryAction.OpenCommandSlotOption) {
			listButton [0].gameObject.SetActive (false);
			listButton [1].gameObject.SetActive (false);
			listButton [2].gameObject.SetActive (true);
			listButton [3].gameObject.SetActive (true);
			listButton [4].gameObject.SetActive (false);

            //listButton [0].gameObject.GetComponent<ActionInventoryButton> ().Action = InventoryAction.UseToCharacter;

			PanelInventoryController.Instance.ChangeState (PanelInventoryController.InventoryState.OpenActionCommandSlot);
		}

		if (panelActionOption.GetComponentInChildren<Button> () != null) {
			//after enable and disable button, send the enabled gameobject to set their navigation
			LegrandUtility.SetButtonsNavigationVertical (panelActionOption.GetComponentsInChildren<Button> ());

			panelActionOption.GetComponentInChildren<Button> ().Select ();
		}


	}

	void GoToCommandItem()
	{
		if (GetComponent<CharacterPreview> () != null) {
			GameObject panelCommandItem = GetComponent<CharacterPreview> ().PanelCommandItem;

			ChangeInteractableObject.EnableAllButtonInParent (panelCommandItem);
			panelCommandItem.GetComponentInChildren<Button>().Select();
		}
	}

	void SetActionCharCommand(InventoryAction action)
	{
		GameObject panelCommandItem = GetComponent<CharacterPreview> ().PanelCommandItem;

		ActionInventoryButton[] actButton = panelCommandItem.GetComponentsInChildren<ActionInventoryButton> ();
	}
}
