﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using Legrand.core;

public class ShowInventoryGoldWeight : MonoBehaviour {
	public Text GoldText;
	public Text WeightText;
	public Image WeightBar;
	
	void OnEnable () {
		CalculateTotalWeight ();
		ShowGold ();
	}

	public void CalculateTotalWeight()
	{
		int currWeight = PartyManager.Instance.Inventory.CurrentWeight;
		int maxWeight = PartyManager.Instance.Inventory.MaxWeight;

		WeightText.text = currWeight.ToString()+"/"+maxWeight;
        if(WeightBar)
		    WeightBar.fillAmount = (float)currWeight / (float)maxWeight;
	}

	public void ShowGold()
	{
		GoldText.text = PartyManager.Instance.Inventory.Gold + " D";
	}
}
