using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;

public class GenerateCharacterSelection : MonoBehaviour {
	public enum ShowMemberFormation
	{
		All,
		ActiveMember,
		ReserveMember,
        None
	}

	[SerializeField]
	private Button InventoryCharacterSelection;
	private ActionInventoryButton.InventoryAction _TargetAction = ActionInventoryButton.InventoryAction.UseToCharacter;
	public ActionInventoryButton.InventoryAction TargetAction{
		set{
			if(value == ActionInventoryButton.InventoryAction.Use)
				_TargetAction = ActionInventoryButton.InventoryAction.UseToCharacter;
			else
				_TargetAction = value;
		}
		get{return _TargetAction;}
	}
	Button[] listButtonCharSel;

	public List<GameObject> PresetCharacter;
	private Dictionary<string, GameObject> _DictObjectChar = new Dictionary<string, GameObject>();

	public Text ShownMembersLabel;

    public bool AutoGenerate = true;

    [Header("Will be used as type generator OnEnable")]
	public ShowMemberFormation GenerateShownMembersType;
    public BaseEnum.Orientation NavigationType;

	private List<string> _ActiveMembers = new List<string>();
	private List<string> _ReserveMembers = new List<string>();

	private List<string> _ShownMembers = new List<string>();
	private List<GameObject> _ShownMembersObj = new List<GameObject>();
    private List<Button> _ShownMembersBtn = new List<Button>();
    public List<Button> ShownMembersBtn
    {
        get { return _ShownMembersBtn; }
    }

	// Use this for initialization
	void Awake () {
        SetDictPresetChar();
	}

    void SetDictPresetChar()
    {
        if (PresetCharacter.Count == 0)
            return;
        foreach (GameObject obj in PresetCharacter)
        {
            string id = obj.GetComponent<IDChar>()._ID;
            if (_DictObjectChar.ContainsKey(id))
                _DictObjectChar[id] = obj;
            else
                _DictObjectChar.Add(id, obj);
        }
    }

	void OnEnable()
	{
        if (!AutoGenerate)
            return;
        GenerateCurrentMembers();
        AddCharactersEvent();
        switch(GenerateShownMembersType)
        {
            case ShowMemberFormation.ActiveMember:
                ShowActiveMembers();
                break;
            case ShowMemberFormation.ReserveMember:
                ShowReserveMembers();
                break;
            case ShowMemberFormation.All:
                ShowActiveMembers();
                break;
        }
		
	}

	void OnDisable()
	{
		RemoveCharactersEvent ();
	}

	public void ChangeConfirmAction(ActionInventoryButton.InventoryAction targetAction)
	{
		foreach (GameObject obj in _ShownMembersObj) {
			obj.GetComponent<ActionInventoryButton> ().Action = targetAction;
		}
	}

    public void ShowCharactersAttribute()
    {
        for(int i=0; i<_ShownMembersObj.Count; i++)
        {
            _ShownMembersObj[i].GetComponent<CharacterPreview>().ShowPanelAttributes();
        }
    }

    public void ShowCharactersCommandItem()
    {
        for (int i = 0; i < _ShownMembersObj.Count; i++)
        {
            _ShownMembersObj[i].GetComponent<CharacterPreview>().ShowPanelCommandItem();
        }
    }

	void AddCharactersEvent()
	{
		List<string> targets = PauseUIController.Instance.ActiveMembers;

		foreach (string str in targets) {
			if (_DictObjectChar.ContainsKey(str)) {
                //if(_DictObjectChar [str].GetComponent<CharacterPreview> () != null)
					_DictObjectChar [str].GetComponent<CharacterPreview> ().AddPreviewEvent ();
			}
		}

		targets = PauseUIController.Instance.ReserveMembers;

		foreach (string str in targets) {
			if (_DictObjectChar.ContainsKey(str)) {
                //if(_DictObjectChar [str].GetComponent<CharacterPreview> () != null)
					_DictObjectChar [str].GetComponent<CharacterPreview> ().AddPreviewEvent ();
			}
		}
	}

	void RemoveCharactersEvent()
	{
		List<string> targets = PauseUIController.Instance.ActiveMembers;

		foreach (string str in targets) {
			if (_DictObjectChar.ContainsKey(str)) {
				if(_DictObjectChar [str].GetComponent<CharacterPreview> () != null)
					_DictObjectChar [str].GetComponent<CharacterPreview> ().RemovePreviewEvent ();
			}
		}

		targets = PauseUIController.Instance.ReserveMembers;

		foreach (string str in targets) {
			if (_DictObjectChar.ContainsKey(str)) {
				if(_DictObjectChar [str].GetComponent<CharacterPreview> () != null)
					_DictObjectChar [str].GetComponent<CharacterPreview> ().RemovePreviewEvent ();
			}
		}
	}

	public void ShowActiveMembers()
	{
		List<string> targets = PauseUIController.Instance.ActiveMembers;

        SetUpShownMembers(targets);

		if (ShownMembersLabel)
			ShownMembersLabel.text = LegrandBackend.Instance.GetLanguageData("Active Members", true);
	}

	public void ShowReserveMembers()
	{
		List<string> targets = PauseUIController.Instance.ReserveMembers;

        SetUpShownMembers(targets);

		if (ShownMembersLabel)
			ShownMembersLabel.text = LegrandBackend.Instance.GetLanguageData("Reserve Members", true);
	}

    void SetUpShownMembers(List<string> targets)
    {
        _ShownMembersObj.Clear();
        if (_DictObjectChar.Count == 0)
            SetDictPresetChar();

        foreach (string str in targets)
        {
            if( !_DictObjectChar.ContainsKey(str))
            {
                MakeNewCharacterSelectionButton(str);
            }
        }

        foreach (KeyValuePair<string, GameObject> k in _DictObjectChar)
        {
            if (targets.Contains(k.Key))
            {
                k.Value.SetActive(true);
                _ShownMembersObj.Add(k.Value);
            }
            else
                k.Value.SetActive(false);
        }

        int index = 0;
        foreach (string str in targets)
        {
            _DictObjectChar[str].transform.SetSiblingIndex(index);
            int targetIndex = _ShownMembersObj.IndexOf(_DictObjectChar[str]);
            if (index != targetIndex)
            {
                GameObject temp = _ShownMembersObj[index];

                _ShownMembersObj[index] = _ShownMembersObj[targetIndex];
                _ShownMembersObj[targetIndex] = temp;
            }
            index++;
        }

        _ShownMembersBtn = new List<Button>();
        foreach (GameObject obj in _ShownMembersObj)
        {
            if (obj.GetComponent<Button>() != null)
                _ShownMembersBtn.Add(obj.GetComponent<Button>());
        }
        //LegrandUtility.SetButtonsNavigationVertical(_ShownMembersBtn, true);
        LegrandUtility.SetButtonsNavigation(_ShownMembersBtn, NavigationType, true);
    }

	public void EnableShownMembersBtn()
	{
        ChangeInteractableObject.EnableAllButtonInList(_ShownMembersBtn);
        //foreach (GameObject obj in _ShownMembersObj) {
        //    if (obj.GetComponent<Button> ())
        //        obj.GetComponent<Button> ().interactable = true;
        //}
	}

	public void DisableShownMembersBtn()
	{
        ChangeInteractableObject.DisableAllButtonInList(_ShownMembersBtn);
        //foreach (GameObject obj in _ShownMembersObj) {
        //    if (obj.GetComponent<Button> ())
        //        obj.GetComponent<Button> ().interactable = false;
        //}
	}

	public GameObject GetShownMemberObjectById(string id)
	{
		foreach (GameObject obj in _ShownMembersObj) {
			if (obj.GetComponent<IDChar> ()._ID == id)
				return obj;
		}
		return null;
	}

	public void GoToFirst()
	{
		EventSystem.current.SetSelectedGameObject (_ShownMembersObj [0]);
	}
	
    void GenerateCurrentMembers()
    {
        List<string> targets = PauseUIController.Instance.ActiveMembers;
        ValidateObject(targets);
        targets = PauseUIController.Instance.ReserveMembers;
        ValidateObject(targets);
    }

    void ValidateObject(List<string> targets)
    {
        foreach (string str in targets)
        {
            if (!_DictObjectChar.ContainsKey(str))
            {
                MakeNewCharacterSelectionButton(str);
            }
        }
    }

    public void Generate(ActionInventoryButton.InventoryAction targetAction)
	{
		if(targetAction == ActionInventoryButton.InventoryAction.Use)
			_TargetAction = ActionInventoryButton.InventoryAction.UseToCharacter;
		else
			_TargetAction = targetAction;

		gameObject.SetActive (true);

		int index = 0;
		listButtonCharSel = GetComponentsInChildren<Button> ();

		foreach (MainCharacter character in PartyManager.Instance.CharacterParty)
		{
			if(index < listButtonCharSel.Length)
			{
				if(listButtonCharSel[index].GetComponent<IDChar>()._ID != character._ID)
				{
					listButtonCharSel[index].GetComponent<IDChar>()._ID = character._ID;
					listButtonCharSel[index].GetComponent<IDChar>().Name = character._Name;
					
					listButtonCharSel[index].GetComponent<Image>().sprite = DatabaseImageSprite.Instance.GetSpriteCharByName(character._Name);
				}
			}
			else
				MakeNewCharacterSelectionButton(character._ID);

			index++;
		}

		listButtonCharSel = GetComponentsInChildren<Button> ();

		for (int i=listButtonCharSel.Length-1; i>0; i--)
		{
			if(!PauseUIController.Instance.AvailableCharName.ContainsKey(listButtonCharSel[i].GetComponent<IDChar>()._ID))
			{
				GameObject.Destroy(listButtonCharSel[i].gameObject);
			}
		}

		foreach (Button button in listButtonCharSel)
		{
			button.GetComponent<ActionInventoryButton>().Action = _TargetAction;
		}
	}

	void MakeNewCharacterSelectionButton(string str)
	{
		Button newGUIChar = Instantiate (InventoryCharacterSelection) as Button;

		if (newGUIChar.GetComponent<IDChar> () == null)
		{
			newGUIChar.gameObject.AddComponent<IDChar>();
		}

		//newGUIChar.transform.SetParent (PauseUIController.instance.panelInventory.GetComponent<PanelInventoryChildren>().PanelCharacterSelection.transform);
		newGUIChar.transform.SetParent (gameObject.transform, false);
		newGUIChar.GetComponent<RectTransform> ().localScale = new Vector3 (1, 1, 1);

        newGUIChar.GetComponent<IDChar>().SetID(str);

        newGUIChar.gameObject.SetActive(true);

        newGUIChar.GetComponent<CharacterPreview>().AddPreviewEvent();

        //newGUIChar.GetComponent<Image>().sprite = DatabaseImageSprite.Instance.GetSpriteCharByName(character._Name);

        _DictObjectChar.Add(str, newGUIChar.gameObject);        
	}
}
