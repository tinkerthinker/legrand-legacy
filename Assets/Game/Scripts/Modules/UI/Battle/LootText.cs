﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LootText : MonoBehaviour {
    public Text LootNameText;
    public Text LootAmountText;

    void OnDisable()
    {
        this.gameObject.SetActive(false);
        LootNameText.text = "";
        LootAmountText.text = "";
    }

    public void SetLoot(string lootName, int lootAmount)
    {
        this.gameObject.SetActive(true);
        LootNameText.text = lootName;
        LootAmountText.text = "x" + lootAmount; 
    }
}
