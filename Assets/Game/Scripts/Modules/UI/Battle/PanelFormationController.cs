﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class PanelFormationController : MonoBehaviour {
	[System.Serializable]
	public class CharacterGameObject
	{
		public string IdChar;
		public GameObject CharObject;
	}

	[SerializeField]
	private int _Row=2, _Col=3;
	[Header("Must be sorted correctly, from position 0,0 to max position")]
	[SerializeField]
	private GameObject[] _FormationButtonList;
	[SerializeField]
	private GameObject[] _ReserveButtonList;
    [SerializeField]
    private GameObject _CharacterInfoObject;
    [SerializeField]
    private GameObject _PointerObject;
    private GameObject _ReserveButtonParent;
    //[SerializeField]
    //private CharacterGameObject[] _CharacterPointerList;
    //private Dictionary<string, GameObject> _DictCharacterPointers = new Dictionary<string, GameObject>();

	private string[,] _AssignedFormation;

    [System.NonSerialized]
	public bool IsFormationReady = false;

	private BattleFormations _BattleFormation;

	private bool _IsFirstLaunch = true;

	private List<string> _ActiveMembers = new List<string>();
    public List<string> ActiveMembers
    {
        get { return _ActiveMembers; }
    }
	private List<string> _ReserveMembers = new List<string>();
    public List<string> ReserveMembers
    {
        get { return _ReserveMembers; }
    }

	#region Data Parameter
	[System.NonSerialized]
	public string AssigningCharId;

	[System.NonSerialized]
	public bool IsAssignCharToFormation=false;

	private string _TempRemovedCharId="";
	public string TempRemovedCharId
	{
		get{return _TempRemovedCharId; }
	}

	private Vector2 _TempPositionRemovedChar = new Vector2();
	public Vector2 TempPositionRemovedChar
	{
		get{return _TempPositionRemovedChar; }
	}
	#endregion

	private bool _CanCancelAssign=false;

	public Color FilledSlotColor = new Color(1,1,1,1);
	public Sprite FilledSlotImage;
	public Color EmptySlotColor = new Color(1,1,1,1);
	public Sprite EmptySlotImage;
	public Color SelectedSlotColor = new Color(1,1,1,1);
	public Sprite SelectedSlotImage;
	public Color ReserveSlotColor = new Color(1,1,1,1);


	private static PanelFormationController s_Instance;
	public static PanelFormationController Instance
	{
		get{
			if (s_Instance == null)
				s_Instance = GameObject.FindObjectOfType(typeof(PanelFormationController)) as PanelFormationController;
			return s_Instance;
		}
	}

	//===============================================================================================================\\

	void Init()
	{
        //_ActiveMembers = PauseUIController.Instance.ActiveMembers;
        //_ReserveMembers = PauseUIController.Instance.ReserveMembers;
	}

	void Awake()
	{
        _ReserveButtonParent = _ReserveButtonList[0].transform.parent.gameObject;
	}

	void Start()
	{
        if (!_IsFirstLaunch)
            return;
		Init ();
		InitFormationData ();
		RefreshFormationData ();

		_IsFirstLaunch = false;
	}

	void OnEnable()
	{
		if (!_IsFirstLaunch) {
			Init ();
			RefreshFormationData ();
		}
	}

	public void GoToFormation()
	{
		PauseUIController.Instance.ChangeState (PauseUIController.PauseUIControllerState.BattleFormation);
        if (_CharacterInfoObject)
            _CharacterInfoObject.SetActive(false);
        if (_PointerObject)
            _PointerObject.SetActive(false);
        //StartCoroutine (WaitToFocusFirstTime ());
		SelectedGameObjectModule.current.HideMarkObject ();
	}

    public void FirstFocusModule()
    {
        StartCoroutine(WaitToFocusFirstTime());
    }

	IEnumerator WaitToFocusFirstTime()
	{
		while (!IsFormationReady)
			yield return null;
        yield return null;
		FocusToTopMostCharInFormationButton ();
	}

	public void GoToMain()
	{
        RefreshActiveMembers();
        PauseUIController.Instance.RefreshActiveReserveMembers();

		PauseUIController.Instance.ChangeState (PauseUIController.PauseUIControllerState.MainMenu);
        //PauseUIController.Instance.FormationButton.Select();
		SelectedGameObjectModule.current.ShowMarkObject ();
	}

	public GameObject GetFormationButtonObject(int row, int col)
	{
		row = Mathf.Clamp(row, -1,_Row-1);
		col = Mathf.Clamp(col, 0,_Col-1);

        if(row >-1)
		    return _FormationButtonList[(row * _Col)+col];
        else
            return _ReserveButtonList[col];
	}

	#region BATTLE FORMATION
	void InitFormationData()
	{
		if(_AssignedFormation == null)
			_AssignedFormation = new string[_Row, _Col];
		
		for (int row=0; row < _AssignedFormation.GetLength(0); row++) {
			for (int col=0; col < _AssignedFormation.GetLength(1); col++){
				_AssignedFormation[row,col] = "";

//				GetFormationButtonObject(row, col).GetComponent<Image>().sprite = null;
//				GetFormationButtonObject(row, col).GetComponent<Image>().enabled = false;
				SetFormationObject("", row, col);
			}
		}

		IsFormationReady = true;
	}

	public void ResetDataParameter()
	{
		AssigningCharId = "";

		IsAssignCharToFormation=false;

		_TempRemovedCharId="";

		_TempPositionRemovedChar = new Vector2();

		_CanCancelAssign=false;
        EventManager.Instance.TriggerEvent(new ComparingEvent(false));
	}

	public void RefreshFormationData()
	{
        if (_AssignedFormation == null)
            InitFormationData();
		int validatedFormations = 0;
		_BattleFormation = PartyManager.Instance.BattleFormations;

//		PanelFormationController panelBattleChildren = panelBattleFormation.GetComponent<PanelFormationController> ();

		//this algorithm still has bug potential, need to improve. kemungkinan bug nya itu ketika formasi kurang dari seharusnya, maka akan terjadi kesalahan
		/*
		foreach (BattleFormation formation in battleFormation.Formations) {
			if(IsCharInFormation(formation.Character._ID))
			{
				if(formation.Position != PositionCharInFormation(formation.Character._ID) )
				{
					needRefresh = true;
					break;
				}
			}
			else
			{
				needRefresh = true;
				break;
			}
		}
		
		if (!needRefresh)
			return;
		*/

		for (int row=0; row < _AssignedFormation.GetLength(0); row++) {
			for (int col=0; col < _AssignedFormation.GetLength(1); col++){
				if(!string.IsNullOrEmpty( _AssignedFormation[row,col] ))
				{
                    if (_BattleFormation.Formations.Find(x => x.Character._ID.Equals(_AssignedFormation[row, col])) != null)
                    {
                        validatedFormations++;
                    }
                    else
                    {
                        validatedFormations = -1;
                        break;
                    }
                    /*
					foreach (BattleFormation formation in _BattleFormation.Formations) {
						if(_AssignedFormation[row,col].Equals(formation.Character._ID))
						{
							if(formation.Position.x == col && formation.Position.y == row)
							{
								validatedFormations++;
							}
						}
					}*/
				}
			}
            if (validatedFormations == -1)
                break;
		}

		if (validatedFormations == _BattleFormation.Formations.Count)
			return;
		
		for (int row=0; row < _AssignedFormation.GetLength(0); row++) {
			for (int col=0; col < _AssignedFormation.GetLength(1); col++){
				_AssignedFormation[row,col] = "";
				SetFormationObject ("", row, col);
			}
		}

		RefreshFormationObject ();


	}

	void RefreshFormationObject()
	{
		//fill active formation
		foreach (BattleFormation formation in _BattleFormation.Formations) {
			int col = (int) formation.Position.x;
			int row = (int) formation.Position.y;

			_AssignedFormation[row,col] = formation.Character._ID;
			SetFormationObject(_AssignedFormation[row,col], row, col);
		}

        RefreshActiveMembers();

        //fill reserve formation		
        for (int index = 0; index < _ReserveButtonList.Length; index++)
        {
            if (index < _ReserveMembers.Count)
                SetFormationObject(_ReserveMembers[index], -1, index);
            else
                SetFormationObject("", -1, index);
        }   
	}

    void RefreshActiveMembers()
    {
        //Set Active Members
        _ActiveMembers.Clear();
        for (int row = _Row - 1; row >= 0; row--)
        {
            for (int col = 0; col < _Col; col++)
            {
                if (!string.IsNullOrEmpty(_AssignedFormation[row, col]))
                    _ActiveMembers.Add(_AssignedFormation[row, col]);
            }
        }

        //Set Reserve Members
        _ReserveMembers.Clear();
        foreach (Legrand.core.MainCharacter chara in PartyManager.Instance.CharacterParty)
        {
            if (!_ActiveMembers.Contains(chara._ID))
                _ReserveMembers.Add(chara._ID);
        }
    }

	public bool IsCharInFormation(string targetId)
	{
		for (int row=0; row<_Row; row++) {
			for (int col=0; col<_Col; col++){
				//if (_assignedFormation[row, col] != "" && _assignedFormation [row, col] != "-1")
				if (_AssignedFormation[row, col] != "")
				if(_AssignedFormation[row, col].Equals( targetId ))
					return true;
			}
		}
		return false;
	}

    public bool IsCharInReserve(string targetId)
    {
        for (int col = 0; col < _ReserveMembers.Count; col++)
        {
            if (_ReserveMembers[col] != "" && _ReserveMembers[col].Equals(targetId))
                    return true;
        }
        return false;
    }

	public Vector2 PositionCharInFormation(string target)
	{
		Vector2 position = new Vector2(-1,-1);
        //search in active formation
		for (int row=0; row<_Row; row++) {
			for(int col=0;col<_Col;col++){
				if(_AssignedFormation[row,col].Equals(target)){
					position.y = row;
					position.x = col;
                    return position;
				}
			}
		}
        //search in reserve formation
        for (int col = 0; col < _Col; col++)
        {
            if (ReserveMembers[col].Equals(target))
            {
                position.y = -1;
                position.x = col;
                return position;
            }
        }
		return position;
	}

	public string GetCharInFormation(int row, int col)
	{
        if (row > -1)
            return _AssignedFormation[row, col];
        else
            return _ReserveMembers[col];
		
	}

    public void SetCharInFormation(string charId, int row, int col)
    {
        if (row > -1)
            _AssignedFormation[row, col] = charId;
        else
            _ReserveMembers[col] = charId;

    }

	public string GetCharInReserve(int col)
	{
		return _ReserveMembers [col];
	}

	public bool AddCharToFormation (string idChar, int row, int col)
	{
		bool foundSelectedCharacter=false;
        bool isCharInReserve = false;

		if (IsCharInFormation (idChar))
		{
			Vector2 positionChar = PositionCharInFormation (idChar);
			RemoveCharFromFormation((int)positionChar.y, (int)positionChar.x, true);
		}
        else if(IsCharInReserve(idChar))
        {
            isCharInReserve = true;
        }

		int i=0;
		int result=1;
		do {
			if(PartyManager.Instance.CharacterParty[i]._ID.Equals( idChar )){
				foundSelectedCharacter = true;
				//result = battleFormation.AddFormation(PartyManager.Instance.CharacterParty[i], new Vector2(col, row));
				result = _BattleFormation.AddFormation(idChar, new Vector2(col, row));
			}
			i++;
		} while(!foundSelectedCharacter && i<PartyManager.Instance.CharacterParty.Count);

		if (result == 0) {
			_AssignedFormation[row,col] = idChar;
            if (isCharInReserve)
            {
                RemoveCharFromFormation(-1, _ReserveMembers.IndexOf(idChar), true);
                RefreshFormationObject();
                EnableReserveButtons();
            }
            else
                SetFormationObject(idChar, row, col);
            StartCoroutine(WaitUpdatePointerPosition(row, col));
			return true;
		}

		return false;
	}

    IEnumerator WaitUpdatePointerPosition(int row, int col)
    {
        yield return null;

        GameObject formationObject = GetFormationButtonObject(row, col);
        formationObject.GetComponent<FormationSelect>().Pointer.UpdatePosition(formationObject.GetComponent<RectTransform>());
    }

	public void RemoveCharFromFormation(int row, int col, bool isPermanent=true)
	{
		if (isPermanent) {
			if (string.IsNullOrEmpty (_TempRemovedCharId))
				_TempRemovedCharId = _AssignedFormation [row, col];
            if (row > -1)
            {
                PartyManager.Instance.BattleFormations.RemoveFromFormationById(_TempRemovedCharId);
                _AssignedFormation[row, col] = ""; //empty string mean no one in that tile formation
            }
            _TempRemovedCharId = "";
		} else {    //if not permanent, that mean player want to swap this position party member to other position
            EventManager.Instance.TriggerEvent(new ComparingEvent(true));

			_CanCancelAssign = true;
            _TempRemovedCharId = GetCharInFormation(row, col);

			_TempPositionRemovedChar.y = row;_TempPositionRemovedChar.x = col;//before deleted, save value first, to proses in cancel method
            if (row == -1)
            {
                FocusToTopMostCharInFormationButton();
                DisableReserveButtons();
                GetFormationButtonObject(row, col).GetComponent<Button>().targetGraphic.gameObject.SetActive(true);
            }            
		}

		SetFormationObject("", row, col);
	}

	public void SwapCharInFormation(int rowFrom, int colFrom, int rowTo, int colTo)
	{
        //if want to swap with a reserve member
        if (rowTo == -1)
        {
            SwapCharFromReserve(GetCharInFormation(rowTo, colTo), rowFrom, colFrom);
            return;
        }
        else if (rowFrom == -1)
        {
            SwapCharFromReserve(GetCharInFormation(rowFrom, colFrom), rowTo, colTo);
            EnableReserveButtons();
            return;
        }

		int resultSwap = 0;

		if (_AssignedFormation [rowTo, colTo] != "") {
			resultSwap += PartyManager.Instance.BattleFormations.SwapFormationByMainCharacterId (_AssignedFormation [rowTo, colTo], TempRemovedCharId);

		} 

		if (resultSwap == 0) {
			string tempId;

			tempId = _AssignedFormation [rowTo, colTo];
//			_AssignedFormation [rowTo, colTo] = _AssignedFormation [rowFrom, colFrom];
			_AssignedFormation [rowTo, colTo] = TempRemovedCharId;
			_AssignedFormation [rowFrom, colFrom] = tempId;

			SetFormationObject(_AssignedFormation [rowTo, colTo], rowTo, colTo);
			SetFormationObject(_AssignedFormation [rowFrom, colFrom], rowFrom, colFrom);
		}
	}

	public void SwapCharFromReserve(string idCharRsv, int rowTo, int colTo )
	{
        int indexRsv = _ReserveMembers.IndexOf(idCharRsv);
        string idForm = GetCharInFormation(rowTo, colTo);

        int result = PartyManager.Instance.BattleFormations.RemoveFromFormationById(idForm);

        if (result == 0)
        {
            result = PartyManager.Instance.BattleFormations.AddFormation(idCharRsv, new Vector2(colTo, rowTo));
            if (result == 0)
            {
                _AssignedFormation[rowTo, colTo] = idCharRsv;
                _ReserveMembers[indexRsv] = idForm;

                SetFormationObject(idForm, -1, indexRsv);
                SetFormationObject(idCharRsv, rowTo, colTo);
            }
        }
        else
        {
            Debug.Log("GAGAL REMOVE CHAR DI FORMASI ROW = "+rowTo+", COL = "+colTo);
        }
	}

	//cancel when want to assign character to formation, make the removed one back to not removed, this proses connect with remove char
	public void CancelAssignChar()
	{
		if (_CanCancelAssign && _TempRemovedCharId != "") {
            //_AssignedFormation [(int)_TempPositionRemovedChar.y, (int)_TempPositionRemovedChar.x] = _TempRemovedCharId;
            SetCharInFormation(_TempRemovedCharId, (int)_TempPositionRemovedChar.y, (int)_TempPositionRemovedChar.x);
			SetFormationObject(_TempRemovedCharId, (int)_TempPositionRemovedChar.y, (int)_TempPositionRemovedChar.x);

            if (_TempPositionRemovedChar.y == -1)
                EnableReserveButtons();

            //IsAssignCharToFormation = false;
            //_CanCancelAssign = false;
            ResetDataParameter();
		}
	}

	public void SetFormationObject(string idChar, int row, int col)
	{
			Image imageObject = GetFormationButtonObject (row, col).GetComponent<Image> ();

			if (!string.IsNullOrEmpty (idChar)) {
                if(row == -1)
                {
                    imageObject.gameObject.SetActive(true);
                    imageObject.color = ReserveSlotColor;
                }
                else
				    imageObject.sprite = FilledSlotImage;
			} else {
                if (string.IsNullOrEmpty(TempRemovedCharId)) // means that a character is really removed from formation
                {
                    if(row == -1)
                        imageObject.gameObject.SetActive(false);
                    else
                        imageObject.sprite = EmptySlotImage;
                }
                else
                {
                    if (row == -1)
                        imageObject.gameObject.SetActive(true);
                    imageObject.color = SelectedSlotColor;
                }
			}

		EventManager.Instance.TriggerEvent(new SetFormationPositionEvent(idChar, row, col));
	}


	#endregion BATTLE FORMATION


    public void FocusToPositionButton(int row, int col)
    {
        GetFormationButtonObject(row, col).GetComponent<Button>().Select();
    }

    public void FocusToMaxPositionButton()
    {
        GetFormationButtonObject(_Row - 1, _Col - 1).GetComponent<Button>().Select();
    }

    public void FocusToTopMostCharInFormationButton()
    {
        Vector2 position = GetMostFrontRightCharPosition();

        if (position.x != -1)
            GetFormationButtonObject((int)position.y, (int)position.x).GetComponent<Button>().Select();
        else
            GetFormationButtonObject(_Row - 1, _Col - 1).GetComponent<Button>().Select();
    }

    Vector2 GetMostFrontRightCharPosition()
    {
        Vector2 position = new Vector2(-1, -1);
        //First Char = char in the most top right position
        /*
        foreach (string str in _ActiveMembers) {
            Vector2 memberPos = PositionCharInFormation (str);
            int memberValue = (int)(memberPos.x + memberPos.y);

            if (memberValue > (position.x + position.y)) {
                position = memberPos;
            } else if (memberValue == (position.x + position.y)) {
                if(memberPos.y > position.y)
                    position = memberPos;
            }
        }*/

        for (int row = _Row - 1; row >= 0; row--)
        {
            for (int col = _Col - 1; col >= 0; col--)
            {
                if (!string.IsNullOrEmpty(_AssignedFormation[row, col]))
                {
                    position.x = col;
                    position.y = row;
                    return position;
                }
            }
        }
        return position;
    }

    public void EnableReserveButtons()
    {
        ChangeInteractableObject.EnableAllButtonInParent(_ReserveButtonParent, true);
        //SetReserveImageColor(ReserveSlotColor);
        SetActiveReserveImage(true);
    }

    public void DisableReserveButtons()
    {
        ChangeInteractableObject.DisableAllButtonInParent(_ReserveButtonParent, true);
        //SetReserveImageColor(Color.clear);
        SetActiveReserveImage(false);
    }

    void SetReserveImageColor(Color color)
    {
        foreach (Button button in _ReserveButtonParent.GetComponentsInChildren<Button>())
        {
            button.targetGraphic.color = color;
        }
    }

    void SetActiveReserveImage(bool active)
    {
        foreach (Button button in _ReserveButtonParent.GetComponentsInChildren<Button>())
        {
            button.targetGraphic.gameObject.SetActive(active);
        }
    }
}
