﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;
using Legrand.core;

public class ResultBattlerExp : MonoBehaviour {
    public Image Avatar;
    public Text LevelText;
    public Text LevelUpText;
    public Text NextExp;
    public Text BonusExp;
    public RectTransform BGBonusEXP;
    public Color PositiveBonusExpColor = new Color(1f, 1f, 1f);
    public Color NegativeBonusExpColor = new Color(1f, 0.42f, 0.42f);
    public Image ExpBar;
    private MainCharacter _ThisMainChar;
    private int _ExpGained;
    private int _PointGained;
    private int _CurrentKnownLevel;
    private bool _IsCalculate = false;
    private IEnumerator _Animation;

    private Vector2 BonusExpOriginalLocation;
    private Sequence _BonusExpTween;

    private bool _IsLevelUp;
    public bool IsLevelUp
    {
        get { return _IsLevelUp; }
    }
    public bool IsCalculate
    {
        get { return _IsCalculate; }
    }
    public MainCharacter Char
    {
        get { return _ThisMainChar; }
    }

    void OnEnable()
    {
        BonusExpOriginalLocation = BonusExp.rectTransform.anchoredPosition;
    }

    void OnDisable()
    {
        _BonusExpTween.Kill(true);
        _BonusExpTween = null;
        BonusExp.color = new Color(BonusExp.color.r, BonusExp.color.g, BonusExp.color.b, 0f);
        BonusExp.rectTransform.anchoredPosition = BonusExpOriginalLocation;
        BGBonusEXP.anchoredPosition = new Vector3(-168, BGBonusEXP.anchoredPosition.y);
        LevelUpText.color = new Color(LevelUpText.color.r, LevelUpText.color.g, LevelUpText.color.b, 0f);
    }

    public void StartCalculate(MainCharacter mainChar, int exp, int bonusExp, bool isReserve = false)
    {
        this.gameObject.SetActive(true);

        _IsLevelUp = false;

        _BonusExpTween = DOTween.Sequence();
        _IsCalculate = true;
        _ThisMainChar = mainChar;
        _ExpGained = exp + bonusExp;

        //Exp Gained
        BonusExp.text = "Exp Gained : " + Mathf.Abs(_ExpGained);
        BonusExp.color = new Color(PositiveBonusExpColor.r, PositiveBonusExpColor.g, PositiveBonusExpColor.b, BonusExp.color.a);

        // Animate Exp Gained
        StartCoroutine(AnimateBonusExp());

        // Set avatar from name by searh it into DB
        Avatar.sprite = mainChar.Avatar[PartyManager.Instance.StoryProgression.PartyModels[int.Parse(mainChar._ID)]];//mainChar.Avatar;//LegrandBackend.Instance.GetSpriteData(mainChar.Avatar);
        // Put temp current level so we know if level up by compare with temp current level
        _CurrentKnownLevel = _ThisMainChar.Level;
        // Put Level into screen
        LevelText.text = "Level " + _CurrentKnownLevel;
        // Hide Level Up with alpha 0
        LevelUpText.color = new Color(LevelUpText.color.r, LevelUpText.color.g, LevelUpText.color.b, 0f);
        // Set Next text from exp differences
        NextExp.text = _ThisMainChar.ExpDifferences() + "";
        // Fill bar between 0-1 set by differences of next/current exp
        ExpBar.fillAmount = _ThisMainChar.CurrentEXP * (1f/(float)_ThisMainChar.NextLevelEXP);

        // Start animate exp
        _Animation = AnimateExp();
        StartCoroutine(_Animation);

        //Check bonus exp
        /*
        if (bonusExp != 0)
        {
            BonusExp.text = (bonusExp > 0 ? "Bonus Exp +" : "Penalty Exp -") + Mathf.Abs(bonusExp);
            BonusExp.color = bonusExp > 0 ?  new Color(PositiveBonusExpColor.r, PositiveBonusExpColor.g, PositiveBonusExpColor.b, BonusExp.color.a)
                                             : 
                                             new Color(NegativeBonusExpColor.r, NegativeBonusExpColor.g, NegativeBonusExpColor.b, BonusExp.color.a);

            // Animate bonus exp
            StartCoroutine(AnimateBonusExp());
        }
        else
            BonusExp.text = string.Empty;
         * */
    }

    public void StopCalculate()
    {
        _IsCalculate = false;
        StopCoroutine(_Animation);
        if (_ExpGained > 0)
            IncreaseExp(_ExpGained);
    }

    IEnumerator AnimateBonusExp()
    {
        yield return new WaitForSeconds(1f);
        if (_BonusExpTween != null)
        {
            _BonusExpTween.Append(DOTween.To(() => BGBonusEXP.anchoredPosition.x, x => BGBonusEXP.anchoredPosition = new Vector3(x, BGBonusEXP.anchoredPosition.y), 261, 2));
            _BonusExpTween.Append(DOTween.ToAlpha(() => BonusExp.color, x => BonusExp.color = x, 1f, 1f));
        }
    }

    IEnumerator AnimateExp()
    {
        float ExpSeed = 1;
        float ExpSpeed = 1.1f;
        while (_ExpGained > 0)
        {
            if(_ExpGained - ExpSeed < 0) ExpSeed = _ExpGained;
            IncreaseExp(Mathf.FloorToInt(ExpSeed));
            _ExpGained -= Mathf.FloorToInt(ExpSeed);
            ExpSeed *= ExpSpeed;
            
            yield return new WaitForSeconds(0.05f);
        }
        StopCalculate();
    }

    private void IncreaseExp(int exp)
    {
        _PointGained += _ThisMainChar.IncreaseEXP(exp);
        if (_CurrentKnownLevel < _ThisMainChar.Level)
        {
            AnimateLevelUp();
            _IsLevelUp = true;
        }
        NextExp.text = _ThisMainChar.ExpDifferences() + "";
        ExpBar.fillAmount = _ThisMainChar.CurrentEXP * (1f / (float)_ThisMainChar.NextLevelEXP);
        WwiseManager.Instance.PlaySFX(WwiseManager.UIFX.Exp_Gain);
    }

    private void AnimateLevelUp()
    {
        WwiseManager.Instance.PlaySFX(WwiseManager.UIFX.Level_Up);
        _CurrentKnownLevel = _ThisMainChar.Level;
        LevelText.text = "Level " + _CurrentKnownLevel;
        LevelUpText.text = "Level Up! ATP+" + _PointGained;
        DOTween.ToAlpha(() => new Color(LevelUpText.color.r, LevelUpText.color.g, LevelUpText.color.b, 0f), x => LevelUpText.color = x, 1f, 1f);
        //LevelUpText.rectTransform.DOLocalMoveY(LevelUpText.rectTransform.localPosition.y + 10, 0.5f);
    }
}
