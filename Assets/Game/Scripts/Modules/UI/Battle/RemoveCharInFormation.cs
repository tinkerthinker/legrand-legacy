using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;
using TeamUtility.IO;

public class RemoveCharInFormation : MonoBehaviour, IUpdateSelectedHandler {
	int _Row;int _Col;

	void Start()
	{
		//NEW ALGO
		_Row = (int)Mathf.Clamp( GetComponent<FormationSelect>().positionBattle.y, 0, GetComponent<FormationSelect>().MaxPositionBattle.y);
		_Col = (int)Mathf.Clamp( GetComponent<FormationSelect>().positionBattle.x, 0, GetComponent<FormationSelect>().MaxPositionBattle.x);


		/*
		row = int.Parse(name.Substring (name.Length - 2, 1))-1;
		col = int.Parse(name.Substring (name.Length - 1, 1))-1;
		*/
	}

	#region IUpdateSelectedHandler implementation

	public void OnUpdateSelected (BaseEventData eventData)
	{
		//if (Input.GetKeyDown (KeyCode.LeftShift)) {
		if(InputManager.GetButtonDown("Delete"))
		{
			//remove character assigned in formation
			//NEW ALGO
//			PauseUIController.Instance.RemoveCharFromFormation(_Row,_Col, true);
//
//			PauseUIController.Instance.TempRemovedCharId = "-1";

			PanelFormationController.Instance.RemoveCharFromFormation(_Row,_Col, true);

			PanelFormationController.Instance.ResetDataParameter ();

			GetComponent<Image>().sprite=null;
		}
	}

	#endregion
}
