using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class FormationSelect : MonoBehaviour, ISelectHandler, IDeselectHandler {
	public enum MemberPositionType
	{
		Active,
		Reserve
	}

	public HoverImage Pointer;
	public Image PionImage;
    //public GameObject CharacterInfo;
    //public MemberPositionType PositionType;
	public Vector2 positionBattle;
	public Vector2 MaxPositionBattle = new Vector2(2,1);
	private int _Row, _Col;

	void Awake () {
		_Row = (int)Mathf.Clamp (positionBattle.y, -1, MaxPositionBattle.y); //because -1 mean that position is reserve position
		_Col = (int)Mathf.Clamp (positionBattle.x, 0, MaxPositionBattle.x);

		EventManager.Instance.AddListener<SetFormationPositionEvent> (SetFormationPositionEventHandler);
	}

	void OnDestroy()
	{
		if(EventManager.Instance)
			EventManager.Instance.RemoveListener<SetFormationPositionEvent> (SetFormationPositionEventHandler);
	}

	void SetFormationPositionEventHandler(SetFormationPositionEvent e)
	{
		if (e.Row == (int)positionBattle.y && e.Col == (int)positionBattle.x) {
			if (PionImage) {
				if (!string.IsNullOrEmpty (e.IdChar)) {
					string idChar = PanelFormationController.Instance.GetCharInFormation (_Row, _Col);
					string nameChar = PartyManager.Instance.GetCharacter (idChar)._Name;

					PionImage.gameObject.SetActive (true);
					PionImage.sprite = LegrandBackend.Instance.GetSpriteData ("Pion" + nameChar);
					PionImage.color = Color.white;
				} else {
					if (!string.IsNullOrEmpty (PanelFormationController.Instance.TempRemovedCharId)) // means that a character is selected and want to assign position
						PionImage.color = new Color (1, 1, 1, 0.6f);
					else
						PionImage.gameObject.SetActive (false);
				}
			}
		}
	}

	public void OnSelect(BaseEventData eventData)
	{
		Vector2 thisButtonPos = GetComponent<RectTransform> ().anchoredPosition;

		Pointer.UpdatePosition (GetComponent<RectTransform> ());

		StartCoroutine (SetSelectedCharId ());
	}

	IEnumerator SetSelectedCharId()
	{
		while (!PanelFormationController.Instance.IsFormationReady)
			yield return null;

        PauseUIController.Instance.selectedCharId = PanelFormationController.Instance.GetCharInFormation(_Row, _Col);
        EventManager.Instance.TriggerEvent(new SelectFormationPositionEvent(positionBattle));

        //if ( string.IsNullOrEmpty(PauseUIController.Instance.selectedCharId) )
        //    CharacterInfo.SetActive (false);
        //else
        //    CharacterInfo.SetActive (true);
	}

	public void RefreshSelectedCharId()
	{
		StartCoroutine (SetSelectedCharId ());
	}

	public void OnDeselect(BaseEventData eventData)
	{
	}

}
