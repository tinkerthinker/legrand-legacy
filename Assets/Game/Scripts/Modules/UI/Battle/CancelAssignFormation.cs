using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class CancelAssignFormation : MonoBehaviour, ICancelHandler {

	public void OnCancel(BaseEventData eventData)
	{
		if (PanelFormationController.Instance.IsAssignCharToFormation) {
			PanelFormationController.Instance.CancelAssignChar ();
			PanelFormationController.Instance.ResetDataParameter();

			if (GetComponent<FormationSelect> () != null)
				GetComponent<FormationSelect> ().RefreshSelectedCharId ();
		} else {
			PanelFormationController.Instance.GoToMain ();
		}
	}
}
