﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using DG.Tweening;
using Legrand.core;

public class ResultScreenController : MonoBehaviour {
    public CanvasGroup GroupCanvas;
    public ResultBattlerExp[] CharacterExpPanels;
    public int EXP;
    public Text EXPText;
    /*------------------------------------------------*/
    public List<VectorStringInteger> Loots;
    public LootText[] LootTexts;

    private Tweener _ResultTween;
    public GameObject LootsPanel;
    public GameObject ResultPanel;
    public GameObject LevelUpPanel;
    public ScreenFader ScreenFade;

    public LevelUpInResultController LevelUpController;
    private bool _IsFinishLevelUp;

	void OnEnable()
	{
        LootsPanel.SetActive(true);
        ResultPanel.SetActive(true);
		Loots = new List<VectorStringInteger> ();

        for (int i = 0; i < CharacterExpPanels.Length; i++)
        {
            CharacterExpPanels[i].gameObject.SetActive(false);
        }

        for (int i = 0; i < Loots.Count; i++)
        {
            LootTexts[i].gameObject.SetActive(false);
        }
    }

    void OnDisable()
    {
        _ResultTween.Kill(false);
        _ResultTween = null;
        GroupCanvas.alpha = 0f;
        Loots = null;

        for (int i = 0; i < LootTexts.Length; i++)
        {
            LootTexts[i].gameObject.SetActive(false);
        }
        EXP = 0;
    }

    public void OnLoseBattle (){
        ScreenFade.FadeSpeed = 1.5f;
        GroupCanvas.alpha = 1f;
        LootsPanel.SetActive(false);
        ResultPanel.SetActive(false);
    }

	public void ResultScreenStart(List<Battler> main,List<Battler> reserve)
	// Activate Result Screen
    {
        _IsFinishLevelUp = false;
        LootsPanel.SetActive(true);
        ResultPanel.SetActive(true);
        EXPText.text = EXP.ToString() + " Exp";
        DOTween.To(() => GroupCanvas.alpha, x => GroupCanvas.alpha = x, 1f, 1f);
        int index = -1;
        for (int i = 0; i < main.Count; i++)
        {
            index++;
            CharacterExpPanels[index].StartCalculate((MainCharacter)main[i].Character, EXP, main[i].BonusExp);
        }
        for (int i = 0; i < reserve.Count; i++)
        {
            index++;
            CharacterExpPanels[index].StartCalculate((MainCharacter)reserve[i].Character, EXP, reserve[i].BonusExp, true);
        }
        for (int i = 0; i < Loots.Count; i++)
        {
            LootTexts[i].gameObject.SetActive(true);
            LootTexts[i].SetLoot(Loots[i].x, Loots[i].y);
        }

        if (Loots.Count == 0 || Loots == null)
            LootsPanel.SetActive(false);
    }

    public void LevelUpScreenStart()
    {
        List<ResultBattlerExp> battlers = new List<ResultBattlerExp>();
        for (int i = 0; i < CharacterExpPanels.Length; i++)
        {
            if (CharacterExpPanels[i].gameObject.activeInHierarchy && CharacterExpPanels[i].IsLevelUp) battlers.Add(CharacterExpPanels[i]);
        }
        LevelUpController.SetLeveledUpBattlers(battlers);

        DOTween.To(() => GroupCanvas.alpha, x => GroupCanvas.alpha = x, 0f, 1f).OnComplete(OpenPanelLevelUp);
        ResultPanel.transform.DOMoveX(ResultPanel.transform.position.x - 100, 1f);
        LootsPanel.transform.DOMoveX(LootsPanel.transform.position.x + 100, 1f);
    }

    void OpenPanelLevelUp()
    {
        ResultPanel.SetActive(false);
        LootsPanel.SetActive(false);

        ResultPanel.transform.DOMoveX(ResultPanel.transform.position.x + 100, 0f);
        LootsPanel.transform.DOMoveX(LootsPanel.transform.position.x - 100, 0f);

        LevelUpController.OpenLevelUpScreen();
    }

    public void ForceFinish()
    // Activate Result Screen
    {
        _ResultTween.Kill(true);
        GroupCanvas.alpha = 1f;
        for (int i = 0; i < PartyManager.Instance.CharacterParty.Count; i++)
            CharacterExpPanels[i].StopCalculate();
    }

    public bool IsAnimationFinished()
    {
        return IsFinishExpCalculate();
    }
    public bool IsFinishExpCalculate()
    {
        for (int i = 0; i < CharacterExpPanels.Length; i++)
        {
            if (CharacterExpPanels[i].gameObject.activeInHierarchy && CharacterExpPanels[i].IsCalculate) return false;
        }
        return true;
    }

    public bool IsAnyCharacterLeveledUp()
    {
        if (LevelUpController.IsFinishLevelUp)
            return false;
        for (int i = 0; i < CharacterExpPanels.Length; i++)
        {
            if (CharacterExpPanels[i].IsLevelUp) return true;
        }
        return false;
    }
}