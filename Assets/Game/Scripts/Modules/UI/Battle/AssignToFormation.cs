using UnityEngine.UI;
using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

[RequireComponent(typeof(Button))]
[RequireComponent(typeof(FormationSelect))]
public class AssignToFormation : MonoBehaviour {
	private Button MyButton = null; // assign in the editor
	private Image myButtonImage;
    //private FormationSelect.MemberPositionType PositionType;

	int _Row;int _Col;

	// Use this for initialization
	void Start() {
		MyButton = GetComponent<Button> ();
		myButtonImage = GetComponent<Image> ();

		_Row = (int)Mathf.Clamp( GetComponent<FormationSelect>().positionBattle.y, -1, GetComponent<FormationSelect>().MaxPositionBattle.y);
		_Col = (int)Mathf.Clamp( GetComponent<FormationSelect>().positionBattle.x, 0, GetComponent<FormationSelect>().MaxPositionBattle.x);

        //PositionType = GetComponent<FormationSelect>().PositionType;

		MyButton.onClick.AddListener(() => { OnClickHandler();});

	}

	void OnClickHandler()
	{
		if (PanelFormationController.Instance.IsAssignCharToFormation) {
			AssignChar ();
            GetComponent<FormationSelect>().RefreshSelectedCharId();
		}
		else {
            if (!string.IsNullOrEmpty(PanelFormationController.Instance.GetCharInFormation(_Row, _Col)))
            {
                PanelFormationController.Instance.IsAssignCharToFormation = true;

                PanelFormationController.Instance.AssigningCharId = PanelFormationController.Instance.GetCharInFormation(_Row, _Col);
                PanelFormationController.Instance.RemoveCharFromFormation(_Row, _Col, false);
            }

            /*
            if (_Row == -1) // if reserve member is clicked, then dont get data from assignedFormation.
            {
                if (PanelFormationController.Instance.ReserveMembers [_Col] != "")
                {
                    PanelFormationController.Instance.IsAssignCharToFormation = true;

                    PanelFormationController.Instance.AssigningCharId = PanelFormationController.Instance.GetCharInFormation(_Row, _Col);
                    PanelFormationController.Instance.RemoveCharFromFormation(_Row, _Col, false);

                    //string nameChar;
                    //PauseUIController.Instance.AvailableCharName.TryGetValue(PanelFormationController.Instance.AssigningCharId, out nameChar);
                }
            }
            else
            {
                if (!string.IsNullOrEmpty( PanelFormationController.Instance.GetCharInFormation( _Row, _Col)) )
                {
                    PanelFormationController.Instance.IsAssignCharToFormation = true;

                    PanelFormationController.Instance.AssigningCharId = PanelFormationController.Instance.GetCharInFormation(_Row, _Col);
                    PanelFormationController.Instance.RemoveCharFromFormation(_Row, _Col, false);

                    //string nameChar;
                    //PauseUIController.Instance.AvailableCharName.TryGetValue(PanelFormationController.Instance.AssigningCharId, out nameChar);
                }
            }*/
			
		}

        //GetComponent<FormationSelect> ().RefreshSelectedCharId ();
	}

	void AssignChar()
	{
		string targetCharId = PanelFormationController.Instance.AssigningCharId;

        if (!string.IsNullOrEmpty(PanelFormationController.Instance.GetCharInFormation(_Row, _Col))) //jika ada character, swap jika selected character ada di formasi atau timpa bila selectedchar tidak ada di formasi
		{
			Vector2 positionChar = PanelFormationController.Instance.TempPositionRemovedChar;
			PanelFormationController.Instance.SwapCharInFormation ((int)positionChar.y, (int)positionChar.x, _Row, _Col);

            PanelFormationController.Instance.ResetDataParameter();
		}
		else{
			Vector2 positionChar = PanelFormationController.Instance.TempPositionRemovedChar;

            //if (positionChar.y == -1)
            //    return;

            if (positionChar.y != -1)
			    PanelFormationController.Instance.RemoveCharFromFormation ((int)positionChar.y, (int)positionChar.x);

			if( PanelFormationController.Instance.AddCharToFormation (PanelFormationController.Instance.AssigningCharId, _Row, _Col))
                PanelFormationController.Instance.ResetDataParameter();
		}		
	}
}
