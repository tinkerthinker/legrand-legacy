﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;
using Legrand.core;

public class LevelUpInResultController : MonoBehaviour
{
    public CanvasGroup GroupCanvas;
    public GameObject LevelUpPanel;
    public ResultBattlerExp TargetBattler;

    public SimulateAttribUpController AttributeSimulator;
    public CharacterPreview CharPreviewer;

    private bool _IsFinishLevelUp = false;
    public bool IsFinishLevelUp
    {
        get { return _IsFinishLevelUp; }
    }
    private List<ResultBattlerExp> LeveledUpBattlers;
    private bool _Continue;
    private bool _IsReserveParty;

    private Transform _CameraHolder;
    private Vector3 _TargetCamPosition;
    private Tween _CamLookTween;

    private Transform _PointerFocus;
    [SerializeField]
    private Vector3 _AdjustFocusPosition = new Vector3(0f, -0, 0f);
    [SerializeField]
    private Vector3 _RadCamPosition = new Vector3(1f,0f,1f);
    [SerializeField]
    private float TimeCameraMove = 2f;

    private Vector3 _CamOriPosition;
    private Quaternion _CamOriRotation;

    private RequestObjectPopUp RequestChoicePopUp = new RequestObjectPopUp("Choice");

    void OnDisable()
    {
        if(_CameraHolder != null)
            _CameraHolder.GetComponent<Animator>().enabled = true;
    }

    public void SetLeveledUpBattlers(List<ResultBattlerExp> battlers)
    {
        _IsFinishLevelUp = false;
        LeveledUpBattlers = battlers;
    }

    public void OpenLevelUpScreen()
    {

        LevelUpPanel.transform.position = LevelUpPanel.transform.position - new Vector3(100, 0, 0);
        LevelUpPanel.SetActive(true);
        EventSystem.current.SetSelectedGameObject(gameObject);
        //temporary solution. redundant with DistributeExpToBattler for the first battler levelup.
        //check for the first battler, because transition that need it run first before camera move
        DetermineBattlerIsReserve();
        TransLvlUpBattler();

        _CameraHolder = Battle.Instance.BattleCameraController.MainCamera.transform.parent;
        _CameraHolder.GetComponent<Animator>().enabled = false;

        _CamOriPosition = _CameraHolder.transform.position;
        _CamOriRotation = _CameraHolder.transform.rotation;

        StartCoroutine(DistributeExpToBattler());
    }

    void TransLvlUpBattler()
    {
        DOTween.To(() => GroupCanvas.alpha, x => GroupCanvas.alpha = x, 1f, 1f).OnComplete(() => LevelUpPanel.GetComponentInChildren<Button>().Select());
        LevelUpPanel.transform.DOMoveX(LevelUpPanel.transform.position.x + 100, 1f);
        CharPreviewer.enabled = false;
        ShowCharPortrait();

        TargetBattler.StartCalculate(LeveledUpBattlers[0].Char, 0, 0);
        EventManager.Instance.TriggerEvent(new ChangeUICharSelectedID(LeveledUpBattlers[0].Char._ID));
    }

    void DetermineBattlerIsReserve()
    {
        if (PartyManager.Instance.BattleFormations.Formations.Find(x => x.Character._ID.Equals(LeveledUpBattlers[0].Char._ID)) == null ||
            LeveledUpBattlers[0].Char.GetComponent<Battler>().BattlerState == BattlerState.Dead)
        {
            _IsReserveParty = true;
        }
        else
            _IsReserveParty = false;
    }

    IEnumerator DistributeExpToBattler()
    {
        while (LeveledUpBattlers.Count > 0)
        {
            _TargetCamPosition = CameraCharForwardPos();
            _TargetCamPosition = ConfirmPositionCamera(_TargetCamPosition - LeveledUpBattlers[0].Char.transform.position);
            //check for the rest of battler
            DetermineBattlerIsReserve();
            StartMoveCamera();

            _Continue = false;
            while (!_Continue)
            {
                yield return null;
                //check if character's ATP is all used up, finish distribute automatically
                if (LeveledUpBattlers[0].Char.AttributePoint == 0)
                {
                    if (AttributeSimulator.ConfirmedSimulationCompleted)
                    {
                        yield return null;
                        ConfirmedNextBattler();
                    }
                }
            }
            LeveledUpBattlers.RemoveAt(0);
        }
        EventSystem.current.SetSelectedGameObject(gameObject);
        _IsFinishLevelUp = true;
    }

    Vector3 CameraCharForwardPos()
    {
        return LeveledUpBattlers[0].Char.transform.position + LeveledUpBattlers[0].Char.transform.forward * 1.5f + LeveledUpBattlers[0].Char.transform.up * 1f;
    }

    Vector3 ConfirmPositionCamera(Vector3 range)
    {
        float x, z;
        float degree = Random.Range(30, 60);
        float rangeForward = Mathf.Sqrt(Mathf.Pow(range.z,2) + Mathf.Pow(range.x,2));

        if (_CameraHolder.transform.position.x < (range.x + LeveledUpBattlers[0].Char.transform.position.x)) //kalau kamera dari sebelah kiri party
            degree = -degree;

        x = LeveledUpBattlers[0].Char.transform.position.x + Mathf.Sin((degree * Mathf.PI) / 180) * rangeForward;
        //x = pivotX + (sin degree * jarak pivot ke tujuan)
        z = LeveledUpBattlers[0].Char.transform.position.z + Mathf.Cos((degree * Mathf.PI) / 180) * rangeForward;

        range = new Vector3(x, range.y, z);

        Debug.Log("Target Degrees : "+degree+" , Test Atan Degree : " + DegreeOfArcXZPosition(range, LeveledUpBattlers[0].Char.transform.position));

        return range;
    }

    /// <summary>
    /// Get Position in a point degree of circle. 0 degrees facing the same direction as the positive X axis
    /// </summary>
    /// <param name="point">Center position of the arc</param>
    /// <param name="range">radius</param>
    /// <param name="degree"></param>
    /// <returns></returns>
    Vector3 ArcXYPosition(Vector3 point,float range, float degree)
    {
        Vector3 position = point;
        position.x = point.x + Mathf.Sin((degree * Mathf.PI) / 180) * range;
        position.y = point.z + Mathf.Cos((degree * Mathf.PI) / 180) * range;

        return position;
    }

    /// <summary>
    /// Get Position in a point degree of circle. 0 degrees facing the same direction as the positive Z axis, clockwise rotation seen from top
    /// </summary>
    /// <param name="point">Center position of the arc</param>
    /// <param name="range">radius</param>
    /// <param name="degree"></param>
    Vector3 ArcXZPosition(Vector3 point, float range, float degree)
    {
        Vector3 position = point;
        position.x = point.x + Mathf.Sin((degree * Mathf.PI) / 180) * range;
        position.z = point.z + Mathf.Cos((degree * Mathf.PI) / 180) * range;

        return position;
    }

    float DegreeOfArcXZPosition(Vector3 point, Vector3 center)
    {
        float degree;

        // use (90 - result) because the algorith is not the same, arcposition measure degree from Z positif and rotate clockwise and mathf.atan measure degree from x positif and rotate counter-clockwise.
        //therefore, we need to recalculate it with the perspective from the arcposition (90 - mathf.atan2 result)
        degree = 90 - ((Mathf.Atan2(point.z - center.z, point.x - center.x) * 180/Mathf.PI + 360 ) % 360); 

        return degree;
    }

    void StartMoveCamera()
    {
        if (_IsReserveParty)
        {
            CameraMoveOfReserveParty();
        }
        else
        {
            float targetDistance = Vector3.Distance(_TargetCamPosition, _CameraHolder.position);
            Vector3 dir = (_TargetCamPosition - _CameraHolder.position).normalized;
            Ray ray = new Ray(_CameraHolder.position, dir);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, targetDistance))
            {
                if (hit.transform.gameObject.GetComponentInParent<Battle>() != LeveledUpBattlers[0])
                {
                    //Vector3 positionDetour;
                    //if (_CameraHolder.transform.position.z > hit.transform.position.z)//camera di depan point collider
                    //    positionDetour = hit.transform.position + (hit.transform.forward * ((hit.collider.bounds.size.z / 2) + 0.1f));
                    //else
                    //    positionDetour = hit.transform.position - (hit.transform.forward * ((hit.collider.bounds.size.z / 2) + 0.1f));
                    //positionDetour = hit.point;
                    //positionDetour = Vector3.Lerp(_CameraHolder.position, _TargetCamPosition, 0.5f);

                    //float distanceDetour = Vector3.Distance(positionDetour, _CameraHolder.position);
                    //float timeDetour = (distanceDetour / targetDistance) * TimeCameraMove;

                    ////positionDetour.y = _TargetCamPosition.y;

                    ////_CameraHolder.transform.DOMove(positionDetour, timeDetour).OnComplete(() => { _CameraHolder.transform.DOMove(_TargetCamPosition, TimeCameraMove - timeDetour); });
                    //StartCoroutine(ArcMoveCamera(positionDetour, distanceDetour));
                    //StartCoroutine(TestArcMove(positionDetour, distanceDetour));
                    StartCoroutine(SpecialMoveCamera());
                }
                else
                {
                    if (Random.Range(0, 100) >= 80)
                    {
                        StartCoroutine(SpecialMoveCamera());
                    }
                    else
                    {
                        _CameraHolder.transform.DOMove(_TargetCamPosition, TimeCameraMove);
                    }
                }
            }
            else
                _CameraHolder.transform.DOMove(_TargetCamPosition, TimeCameraMove);

            _PointerFocus = LeveledUpBattlers[0].Char.GetComponent<BattleHelper>().PointerFocus;
            _AdjustFocusPosition.z = -(_PointerFocus.localPosition.z);

            if (_CameraHolder.transform.position.x > LeveledUpBattlers[0].Char.transform.position.x) //kalau kamera dari sebelah kanan party
                _AdjustFocusPosition.z -= 0.1f;

            SetDoF();

            CheckLookAtTarget();
        }
    }

    //the camera snap in front of target and start moving in circular way. from 0 degree to 90 or -90 degrees
    IEnumerator SpecialMoveCamera()
    {
        //step :
        //1. snap to target position
        //2. rotate it for 90 degrees?
        _CameraHolder.position = _TargetCamPosition;
        yield return null;

        float degree = DegreeOfArcXZPosition(_CameraHolder.position, LeveledUpBattlers[0].Char.transform.position);
        float destDegree = degree - 90f;
        float range = Vector3.Distance( CameraCharForwardPos() , LeveledUpBattlers[0].Char.transform.position);
        Vector3 center = LeveledUpBattlers[0].Char.transform.position;
        float rotateSign = Mathf.Sign(degree);

        if ( rotateSign > 0)//positive
        {
            destDegree = degree - 90f;
        }
        else
            destDegree = degree + 90f;


        center.y = CameraCharForwardPos().y;
        _TargetCamPosition = ArcXZPosition(center, range, destDegree);
        CheckLookAtTarget();

        if (rotateSign > 0)
        {
            while (degree > destDegree)
            {
                _CameraHolder.position = ArcXZPosition(center, range, degree);
                degree -= (Time.deltaTime / TimeCameraMove) * 90f;
                yield return null;
            }
        }
        else
        {
            while (degree < destDegree)
            {
                _CameraHolder.position = ArcXZPosition(center, range, degree);
                degree += (Time.deltaTime / TimeCameraMove) * 90f;
                yield return null;
            }
        }
        degree = destDegree;
        _CameraHolder.position = ArcXZPosition(center, range, degree);
    }

    IEnumerator ArcMoveCamera(Vector3 centerPoint, float range)
    {
        float degree = 0;
        bool isFromPositif;
        float distanceZ = _TargetCamPosition.z - _CameraHolder.position.z;
        Vector3 arcPosition;
        float posZ = _CameraHolder.position.z;
        if (_CameraHolder.position.x < _TargetCamPosition.x)
            isFromPositif = false;
        else
            isFromPositif = true;

        if (isFromPositif)
        {
            while (degree < 180)
            {
                arcPosition = ArcXYPosition(centerPoint, range, degree);
                posZ += Mathf.Sign(distanceZ) * ((Time.deltaTime / TimeCameraMove)*Mathf.Abs(distanceZ));
                arcPosition.z = posZ;

                _CameraHolder.position = arcPosition ;
                degree += (Time.deltaTime / TimeCameraMove) * 180f;
                yield return null;
            }
            degree = 180;
            arcPosition = ArcXYPosition(centerPoint, range, degree);
            posZ = _TargetCamPosition.z;
            arcPosition.z = posZ;

            _CameraHolder.position = arcPosition;
        }
        else
        {
            degree = 180;
            while (degree > 0)
            {
                arcPosition = ArcXYPosition(centerPoint, range, degree);
                posZ += Mathf.Sign(distanceZ) * ((Time.deltaTime / TimeCameraMove) * Mathf.Abs(distanceZ));
                arcPosition.z = posZ;

                _CameraHolder.position = arcPosition;
                degree -= (Time.deltaTime / TimeCameraMove) * 180f;
                yield return null;
            }
            degree = 0;
            arcPosition = ArcXYPosition(centerPoint, range, degree);
            posZ = _TargetCamPosition.z;
            arcPosition.z = posZ;

            _CameraHolder.position = arcPosition;
        }
    }

    IEnumerator TestArcMove(Vector3 centerPoint, float range)
    {
        float time = 0;
        while (time < 10)
        {
            for (int degree = 0; degree < 180; degree++ )
            {
                Debug.DrawLine(ArcXYPosition(centerPoint, range, degree), ArcXYPosition(centerPoint, range, degree + 1));
            }
            time += Time.deltaTime;
            yield return null;
        }
        _CameraHolder.position = _TargetCamPosition;
    }

    void CameraMoveOfReserveParty()
    {
        _CameraHolder.transform.DOMove(_CamOriPosition, 2f);
        _CameraHolder.transform.DORotate(_CamOriRotation.eulerAngles, 2f);        
    }

    void ShowCharPortrait()
    {
        if (CharPreviewer)
        {
            if (_IsReserveParty)
            {
                CharPreviewer.ShowCharacterPortrait(LeveledUpBattlers[0].Char);
            }
            else
            {
                CharPreviewer.HideCharacterPortrait();
            }
        }
    }

    void SetDoF()
    {
        if( Battle.Instance.BattleCameraController is BattleCameraRahas)
        {
            Transform dof = ((BattleCameraRahas)Battle.Instance.BattleCameraController).DOFCenter;
            dof.DOMove(_PointerFocus.position, 1f);
        }
    }

    IEnumerator LookAtTarget()
    {
        if (_CamLookTween != null)
            _CamLookTween.Kill();
        
        _CamLookTween = _CameraHolder.transform.DOLookAt( _PointerFocus.position + _AdjustFocusPosition , 0.5f);
        yield return null;
        CheckLookAtTarget();
    }

    void CheckLookAtTarget()
    {
        if (_CameraHolder.transform.position != _TargetCamPosition)
            StartCoroutine(LookAtTarget());
    }

    public void NextBattler()
    {
        if (AttributeSimulator.ConfirmedSimulationCompleted)
            PopUpUI.CallChoicePopUp("Finish Distribute ATP ?", ConfirmedNextBattler);
        else
            PopUpUI.CallChoicePopUp("Cancel and Finish Distribute ATP ?", ConfirmedNextBattler);
        EventManager.Instance.AddListenerOnce<RespondObjectPopUp>(SetCancelPopUp);
    }

    void SetCancelPopUp(RespondObjectPopUp e)
    {
        //get the popup object from legrandPopUp to set cancel object to this, so when transition focus object is not on a button
        e.PopUp.GetComponent<PopUpObject>().SetCancelObject(gameObject);
    }

    void ConfirmedNextBattler()
    {
        EventSystem.current.SetSelectedGameObject(gameObject);
        //send to legrandPopUp to request PopUp object
        EventManager.Instance.TriggerEvent(RequestChoicePopUp);
        if (LeveledUpBattlers.Count > 1)
        {
            DOTween.To(() => GroupCanvas.alpha, x => GroupCanvas.alpha = x, 0f, 1f).OnComplete(TransLvlUpBattler);
            LevelUpPanel.transform.DOMoveX(LevelUpPanel.transform.position.x - 100, 1f);
            AttributeSimulator.Refresh();
        }
        _Continue = true;
    }
}