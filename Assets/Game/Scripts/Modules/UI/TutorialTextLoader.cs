﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TutorialTextLoader : MonoBehaviour
{
    [System.Serializable]
    public class Data
    {
        public string ID;
        public Text UI;
    }

    public Data[] TutorialTextList;
    public RenderHeads.Media.AVProVideo.MediaPlayer[] Videos;

    void OnEnable()
    {
        LoadText();
        PlayVideos();
    }

    void OnDisable()
    {
        //StopVideos();
    }

    void LoadText()
    {
        for(int i=0;i< TutorialTextList.Length;i++)
        {
            string text = LegrandBackend.Instance.GetLanguageData(TutorialTextList[i].ID);
            if (string.IsNullOrEmpty(text))
                text = TutorialTextList[i].ID;
            TutorialTextList[i].UI.text = text;
        }
    }

    public void PlayVideos()
    {
        for(int i=0;i<Videos.Length;i++)
        {
            Videos[i].Play();
        }
    }

    public void StopVideos()
    {
        for (int i = 0; i < Videos.Length; i++)
        {
            //Videos[i].Pause();
            Videos[i].Stop();
            Videos[i].Rewind(false);
            //Videos[i].Stop();
        }
    }
}
