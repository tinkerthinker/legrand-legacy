﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

// Based on http://unity3d.com/learn/tutorials/projects/stealth/screen-fader
public class ScreenFader : MonoBehaviour
{
    public float FadeSpeed = 1.5f;
	public float FadeDelay = 0f;

    private Image imageTexture;
	private RectTransform rectTransform;
    
    private bool _IsInTransition;
    public bool IsInTransition
    {
        get { return _IsInTransition; }
    }
    
    private IEnumerator _StartCoroutine;
    
    public delegate void ScreenFaderDelegate();
    public ScreenFaderDelegate OnFadeIn;
    public ScreenFaderDelegate OnFadeOut;
	public ScreenFaderDelegate StartFadeIn;
	public ScreenFaderDelegate StartFadeOut;

    // Use this for initialization
    void Awake()
    {
        // Initial color
        imageTexture = GetComponent<Image> ();
		// Set dynamic width and height
		rectTransform = GetComponent<RectTransform> ();
    }
    
	public void TransToClear()
	{
		imageTexture.color = Color.clear;
	}

	public void TransToBlack()
	{
		imageTexture.color = Color.black;
	}

	public void FadeToClear(float delayTime)
    {
//      imageTexture.color = Color.Lerp(imageTexture.color, Color.clear, FadeSpeed * Time.deltaTime);
		imageTexture.DOColor (Color.clear, FadeSpeed).SetDelay(delayTime);
    }
    
	public void FadeToBlack(float delayTime)
    {
//		imageTexture.color = Color.Lerp(imageTexture.color, Color.black, FadeSpeed * Time.deltaTime);
		imageTexture.DOColor (Color.black, FadeSpeed).SetDelay(delayTime);
    }
    
    public void StartScene()
    {
        _IsInTransition = true;
        
		if (StartFadeIn != null) StartFadeIn ();
        if (_StartCoroutine != null)
            StopCoroutine(_StartCoroutine);

        _StartCoroutine = ForceToComplete(FadeSpeed+1f, Color.clear);
        StartCoroutine(_StartCoroutine);
		imageTexture.DOColor (Color.clear, FadeSpeed).OnComplete(StartSceneComplete).SetDelay(FadeDelay);
    }
    
    public void EndScene()
    {
        _IsInTransition = true;
        imageTexture.enabled = true;
        
		if (StartFadeOut != null) StartFadeOut ();
        if (_StartCoroutine != null)
            StopCoroutine(_StartCoroutine);

        _StartCoroutine = ForceToComplete(FadeSpeed + 1f, Color.black);
        StartCoroutine(_StartCoroutine);
        imageTexture.DOColor (Color.black, FadeSpeed).OnComplete(EndSceneComplete).SetUpdate(true).SetDelay(FadeDelay);
    }

    public IEnumerator ForceToComplete(float time, Color color)
    {
        yield return new WaitForSeconds(time);
        imageTexture.color = color;
    }

    void StartSceneComplete()
    {
        imageTexture.color = Color.clear;

        if (_StartCoroutine != null)
            StopCoroutine(_StartCoroutine);

        _IsInTransition = false;
        if (OnFadeIn != null) OnFadeIn();
    }
    
    void EndSceneComplete()
    {
        imageTexture.color = Color.black;

        if (_StartCoroutine != null)
            StopCoroutine(_StartCoroutine);

        _IsInTransition = false;
        if (OnFadeOut != null) OnFadeOut();
    }

    void OnDisable()
    {
        TransToClear();
    }
}
