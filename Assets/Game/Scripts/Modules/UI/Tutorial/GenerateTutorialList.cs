﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GenerateTutorialList : AbsObjectListGenerator {
    string _SelectedTutorial;

    void OnEnable()
    {
        UpdateList();
    }

    public override void StartLoopUpdate()
    {
        //Sorted Version
        List<string> sortedTutorial = new List<string>();
        foreach(KeyValuePair<string, bool> tp in PartyManager.Instance.TutorialProgress)
        {
            sortedTutorial.Add(tp.Key);
        }

        sortedTutorial.Sort();

        foreach(string str in sortedTutorial)
        {
            _SelectedTutorial = str;
            UpdateEachButton(_SelectedTutorial);
        }

        //Unsorted Version, Show up based on which one opened first
        /*
        foreach (KeyValuePair<string, bool> tp in PartyManager.Instance.TutorialProgress)
        {
            _SelectedTutorial = tp.Key;
            UpdateEachButton(_SelectedTutorial);
        }*/
    }

    public override bool CheckConditionPreUpdate(object data)
    {
        if (PartyManager.Instance.TutorialProgress.ContainsKey(_SelectedTutorial) && PartyManager.Instance.TutorialProgress[_SelectedTutorial] == true)
            return true;
        else
            return false;
    }

    public override void SetButtonName(UnityEngine.UI.Button button, object data)
    {
        if(data is string)
        {
            button.name = (string)data;
        }
    }

    public override void AfterDataUpdatedProcess(IObjectData objectData, object data)
    {

    }

    public void TryFocusToFirstButton()
    {
        FocusToFirstButton();
    }
}