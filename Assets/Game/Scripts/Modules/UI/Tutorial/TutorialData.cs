﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TutorialData : MonoBehaviour, IObjectData
{
    public Text TitleText;
    private string TutorialName;

    public void SetData(object obj)
    {
        if (!(obj is string))
            return;
        TutorialName = (string)obj;
        /*
        TutorialName = (string)obj;
        string FormattedName = TutorialName;

        for (int i = 1; i < FormattedName.Length; i++)
        {
            if (char.IsUpper(FormattedName[i]) && (!char.IsUpper(FormattedName[i-1])))
            {
                FormattedName = FormattedName.Insert(i++, " ");
            }
        }

        if (TitleText)
            TitleText.text = FormattedName;
         */
        if (TitleText)
        {
            TutorialSteps step = LegrandBackend.Instance.GetTutorialSteps(TutorialName);
            TitleText.text = step.Title;
        }
    }

    public object GetData()
    {
        return TutorialName;
    }

    public void RefreshData()
    {
        SetData(TutorialName);
    }

    public void OnSelect(UnityEngine.EventSystems.BaseEventData eventData)
    {

    }

    public void ShowTutorial()
    {
        if (string.IsNullOrEmpty(TutorialName))
            return;

        PopUpUI.CallTutorialPopUp(TutorialName, true);
    }
}
