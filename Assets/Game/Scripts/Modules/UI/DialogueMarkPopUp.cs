﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DialogueMarkPopUp : PopUpObject {
	private GameObject _TalkingObj;
    public GameObject BalloonTalk;
    public GameObject BalloonInteraction;

	Camera currCamera;

    bool _IsActiveAfterDialogue;

    void OnDisable()
	{
        Dialoguer.events.onStarted -= HideWhileDialogue;
	}

	void OnEnable () {
        Dialoguer.events.onStarted += HideWhileDialogue;

		currCamera = AreaController.Instance.CurrRoom.cameras [0];
		foreach (Camera cam in AreaController.Instance.CurrRoom.cameras) {
			if (cam.tag.Equals ("MainCamera")) {
				currCamera = cam;
				break;
			}
		}
		if (GetComponent<Canvas> ())
		{
			if (GetComponent<Canvas> ().worldCamera != currCamera)
				GetComponent<Canvas> ().worldCamera = currCamera;
		}

	}

	void Update()
	{
		SetPositionToObject ();
	}

	public void HideWhileDialogue()
	{
		Dialoguer.events.onWindowClose += onDialogueWindowCloseHandler;
        gameObject.SetActive(false);
	}

    public void RemoveDialogueListener()
    {
        Dialoguer.events.onWindowClose -= onDialogueWindowCloseHandler;
    }

	private void onDialogueWindowCloseHandler()
	{
		Dialoguer.events.onWindowClose -= onDialogueWindowCloseHandler;
        if(_TalkingObj != null && _IsActiveAfterDialogue)
            if(_TalkingObj.activeInHierarchy)
                gameObject.SetActive(true);
	}

	public void SetPositionToObject()
	{
		if (_TalkingObj == null || !_TalkingObj.activeInHierarchy) {
			gameObject.SetActive (false);
			return;
		}
		Vector3 objPos = _TalkingObj.transform.position;
//		objPos.y += 2*_TalkingObj.GetComponentInChildren<Renderer> ().bounds.extents.y;
		Vector3 screenPos = currCamera.WorldToScreenPoint (objPos);

		GetComponentInChildren<Image> ().GetComponent<RectTransform> ().position = screenPos;
	}

    public void SetData(GameObject talkingObj, bool isActiveAfterDialogue = true)
    {
        _TalkingObj = talkingObj;
        _IsActiveAfterDialogue = isActiveAfterDialogue;
        if(BalloonTalk != null && BalloonInteraction != null)
        {
            BalloonTalk.SetActive(_IsActiveAfterDialogue);
            BalloonInteraction.SetActive(!_IsActiveAfterDialogue);
        }
        SetPositionToObject();
    }
}
