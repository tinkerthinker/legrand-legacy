﻿using UnityEngine;
using System.Collections;

public class FastTravelAction : MonoBehaviour {
    public FastTravelAttribute FastTravelAttribute
    {
        set; get;
    }

    public FastTravelAction(FastTravelAttribute fta)
    {
        FastTravelAttribute = fta;
    }
    
    public void DoFastTravel()
    {
        //EventManager.Instance.TriggerEvent(new TeleportPlayerEvent(FastTravelAttribute.PrefabName,"Portal_Fast_Travel"));
    }
}
