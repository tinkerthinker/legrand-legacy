﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GenerateFastTravelPoint : AbsObjectListGenerator {
    public bool Test = false;
    public List<FastTravelAttribute> TestUnlockedPoints;

    void OnEnable()
    {
        if(Test)
        {
            foreach(FastTravelAttribute ft in TestUnlockedPoints)
            {
                if (!PartyManager.Instance.WorldInfo.UnlockedFastTravelPoints.Contains(ft))
                    PartyManager.Instance.WorldInfo.UnlockedFastTravelPoints.Add(ft);
            }
        }
        UpdateList();
        StartCoroutine(WaitFocusFirstButton());
    }

    IEnumerator WaitFocusFirstButton()
    {
        yield return null;
        if (!FocusToFirstButton())
        {
            ScrollerController.ScrollbarTarget.Select();
        }
    }

    public override void StartLoopUpdate()
    {
        List<FastTravelAttribute> unlockedFastTravel = PartyManager.Instance.WorldInfo.UnlockedFastTravelPoints;

        foreach(FastTravelAttribute ft in unlockedFastTravel)
        {
            UpdateEachButton(ft);
        }
    }

    public override bool CheckConditionPreUpdate(object data)
    {
        return true;
    }

    public override void SetButtonName(UnityEngine.UI.Button button, object data)
    {
        if(data is FastTravelAttribute)
        {
            button.name = ((FastTravelAttribute)data).Name;
        }
    }

    public override void AfterDataUpdatedProcess(IObjectData objectData, object data)
    {
        
    }
}
