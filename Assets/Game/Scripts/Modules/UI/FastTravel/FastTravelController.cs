﻿using UnityEngine;
using System.Collections;

public class FastTravelController : MonoBehaviour {
    public WorldCityInfo CityInfoHandler;

    public void SetFastTravelInfo(bool isActive, Area area = null)
    {
        if (isActive && area != null)
        {
            if (CityInfoHandler)
            {
                CityInfoHandler.Set(area.areaName, area.areaName, area.areaDescription);
            }
        }
        else
        {
            if (CityInfoHandler)
                CityInfoHandler.Disable();
        }
    }

    public void Close()
    {
        SetFastTravelInfo(false);
        EventManager.Instance.TriggerEvent(new FastTravelUIEvent(false));
    }
}
