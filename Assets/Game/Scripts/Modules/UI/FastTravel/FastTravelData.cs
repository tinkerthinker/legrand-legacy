﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FastTravelData : MonoBehaviour, IObjectData {
    public Text NameCityText;
    public Image CityImage;
    public FastTravelController Controller;

    private FastTravelAttribute _SelectedFT;
    private Area _FastTravelArea;

    public void SetData(object obj)
    {
        if (!(obj is FastTravelAttribute))
            return;

        _SelectedFT = (FastTravelAttribute)obj;

        if (NameCityText)
            NameCityText.text = _SelectedFT.Name;

        if(AreaController.Instance != null)
        {
            foreach (Area area in AreaController.Instance.areaData)
                if (area.areaName.Equals(_SelectedFT.Name)) _FastTravelArea = area;
        }
    }

    public object GetData()
    {
        return _SelectedFT;
    }

    public void RefreshData()
    {
        if(_SelectedFT != null)
            SetData(_SelectedFT);
    }

    public void OnSelect(UnityEngine.EventSystems.BaseEventData eventData)
    {
        if(Controller)
        {
            Controller.SetFastTravelInfo(true, _FastTravelArea);
        }
    }

    public void DoFastTravel()
    {
        if(AreaController.Instance != null)
        {
            EventManager.Instance.TriggerEvent(new TeleportPlayerEvent(AreaController.Instance.GetFastTravelDestination(_SelectedFT.Name), "Portal_Fast_Travel"));
        }
        //EventManager.Instance.TriggerEvent(new TeleportPlayerEvent(_SelectedFT.PrefabName, "Portal_Fast_Travel"));
        EventManager.Instance.TriggerEvent(new FastTravelUIEvent(false));
    }
}
