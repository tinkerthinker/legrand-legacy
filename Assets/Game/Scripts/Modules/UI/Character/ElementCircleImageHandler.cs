﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ElementCircleImageHandler : MonoBehaviour {
	public List<Image> ElementImages;
	private List<Element> SortedElement = new List<Element>();

	public Color StrongerColor = new Color(1,0,0,1);
	public Color WeakerColor = new Color(0,0,1,1);
	public Color NeutralColor = new Color(1,1,1,1);

	void Awake () {
	}

	public void DefineElementCircle(CharacterElement element)
	{
		SortDefaultElement();

		Element strongerAgainst = element.Stronger;
		Element weakerAgainst = element.Weakness;

		/*
		Image strongerImage = null;
		Image weakerImage = null;

		foreach (ElementTypeObject eto in ElementTypeObjects) {
			if (eto.ElementType == strongerAgainst) {
				strongerImage = eto.ImageObject;
				eto.ImageObject.color = StrongerColor;
			}
			else if (eto.ElementType == weakerAgainst) {
				weakerImage = eto.ImageObject;
				eto.ImageObject.color = WeakerColor;
			}
			else {
				eto.ImageObject.color = Color.white;
			}
		}

		if(strongerImage != null)
			strongerImage.transform.SetSiblingIndex (0);
		if (weakerImage != null)
			weakerImage.transform.SetSiblingIndex (1);
		*/

		if (strongerAgainst != Element.None) {
			int index = SortedElement.FindIndex ((Element obj) => strongerAgainst == obj);
			SortedElement.RemoveAt (index);
			SortedElement.Insert (0, strongerAgainst);
			ElementImages [0].color = StrongerColor;
		}
		else
			ElementImages [0].color = NeutralColor;

		if (weakerAgainst != Element.None) {
			int index = SortedElement.FindIndex ((Element obj) => weakerAgainst == obj);
			SortedElement.RemoveAt (index);
			SortedElement.Insert (1, weakerAgainst);
			ElementImages [1].color = WeakerColor;
		}
		else
			ElementImages [1].color = NeutralColor;

		int imgIndex = 0;
		foreach (Image img in ElementImages) {
			img.GetComponentsInChildren<Image>()[1].sprite = LegrandBackend.Instance.GetSpriteData ("Element" + SortedElement [imgIndex]);

			imgIndex++;
		}
	}

	void SortDefaultElement()
	{
		SortedElement.Clear ();
		foreach (Element el in Enum.GetValues(typeof(Element))) {
			SortedElement.Add (el);
		}
	}
}
