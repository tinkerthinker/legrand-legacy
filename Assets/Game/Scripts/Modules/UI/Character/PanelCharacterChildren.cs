﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PanelCharacterChildren : MonoBehaviour {
	public GameObject panelStatusChar;
	public GameObject panelCharacterList;

	public Button GetCharacterButton(string charId)
	{
		IDChar[] CharactersButton = GetComponentsInChildren<IDChar> ();
		foreach (var chara in CharactersButton) {
			if(chara._ID == charId)
			{
				return chara.GetComponent<Button>();
			}
		}

		return CharactersButton [0].GetComponent<Button> ();
	}
}
