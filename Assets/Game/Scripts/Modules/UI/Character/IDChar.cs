﻿using UnityEngine;
using System.Collections;

public class IDChar : MonoBehaviour {
	public string _ID;
	public string Name;

	public void SetID(string id)
	{
        _ID = id;
        Name = PartyManager.Instance.GetCharacter(_ID)._Name;
        gameObject.name = Name;
	}
}
