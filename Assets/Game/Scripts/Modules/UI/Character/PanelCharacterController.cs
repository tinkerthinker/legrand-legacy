﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;
using TeamUtility.IO;

public class PanelCharacterController : MonoBehaviour {
	public SimulateAttribUpController SimulatedController;
    public ShowNextGrimoire NextGrimoireGenerator;
    public ShiftCharacterSelect CharacterShifter;

	public void GoToStatusCharacter()
	{
		PauseUIController.Instance.ChangeState (PauseUIController.PauseUIControllerState.Status);
	}

	public void GoToMain()
	{
		StartCoroutine (WaitSimulationCompleted());
	}

	IEnumerator WaitSimulationCompleted()
	{
		while (!SimulatedController.ConfirmedSimulationCompleted) {
			yield return null;
		}

		//ketika back to main focus diarahkan ke character yang dipilih, untuk sekarang diarahkan ke finn semua dulu aja
		PauseUIController.Instance.ChangeState (PauseUIController.PauseUIControllerState.MainMenu);

		IDChar[] Characters = PauseUIController.Instance.PanelCharacter.GetComponentsInChildren<IDChar> ();
        /*
		string charId = PauseUIController.Instance.selectedCharId;

		if (!PauseUIController.Instance.ActiveMembers.Contains (charId)) {
			Characters[0].GetComponent<Button> ().Select ();
		} else {
			foreach (IDChar chara in Characters) {
				if (chara._ID == charId) {
					chara.GetComponent<Button> ().Select ();
					break;
				}
			}
		}*/
	}

    void Update()
    {
        if (NextGrimoireGenerator)
        {
            if (InputManager.GetButtonDown("Menu") && NextGrimoireGenerator.PreviewHolder.activeInHierarchy)
            {
                if (NextGrimoireGenerator.IsFocusOnChild)
                {
                    NextGrimoireGenerator.OnCancel();
                    if(CharacterShifter)
                        CharacterShifter.SetInteractable(true);
                }
                else
                {
                    if (NextGrimoireGenerator.FocusToFirstButton())
                    {
                        if (CharacterShifter)
                            CharacterShifter.SetInteractable(false);
                    }
                }
            }
        }
    }
}
