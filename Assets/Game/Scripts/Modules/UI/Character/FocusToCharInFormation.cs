using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FocusToCharInFormation : MonoBehaviour {
	
	void Start () {
		GetComponent<Button> ().onClick.AddListener (() => OnClickHandler() );
	}
	
	void OnClickHandler () {

		Vector2 position = PanelFormationController.Instance.PositionCharInFormation (PauseUIController.Instance.selectedCharId);

		PauseUIController.Instance.ChangeState (PauseUIController.PauseUIControllerState.BattleFormation);

//		PauseUIController.Instance.panelBattleFormation.GetComponent<PanelFormationController> ().GetFormationButtonObject ((int)position.y, (int)position.x).GetComponent<Button> ().Select ();
		PauseUIController.Instance.PanelBattleFormation.GetComponent<PanelFormationController> ().FocusToPositionButton((int)position.y, (int)position.x);
//		PauseUIController.Instance.isAssignCharToFormation = true;
//		PauseUIController.Instance.IsArrangeFormation = true;
		PanelFormationController.Instance.IsAssignCharToFormation = true;

//		PauseUIController.Instance.AssigningCharId = PauseUIController.Instance.selectedCharId;
		PanelFormationController.Instance.AssigningCharId = PauseUIController.Instance.selectedCharId;
	}
}
