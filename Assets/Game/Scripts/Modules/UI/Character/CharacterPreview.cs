﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class CharacterPreview : MonoBehaviour, ISelectHandler {
	public Image CharacterImage;
    public Text CharLevelText;
	public Text NameText;
	public Text ClassText;
	public Image WeaponTypeImage;
	public Image ElementImage;

	public GameObject PanelAttributes;
	public GameObject PanelCommandItem;
	public GameObject PanelCommandMagic;

	public ShowCharacterAttribute AttributeHandler;

	public CharacterWeaponCircle CharWeaponType;
	public ElementCircleImageHandler CharElementHandler;

	[Header("Determine if handle character change based on ChangeCharSelectedId Event")]
	public bool UseChangeCharEvent = false;

	[Header("Should be Filled if not handle ChangeCharEvent")]
	public IDChar IDChar;
	private CommandButtonSet[] _CommandButtons;

	private string _IdChar;

	void Awake()
	{
		if(!AttributeHandler)
			AttributeHandler = GetComponent<ShowCharacterAttribute> ();
		
        //if (!UseChangeCharEvent) {
        //    if (IDChar != null) {
        //        _IdChar = IDChar._ID;
        //    }
        //}

		if(PanelCommandItem)
			_CommandButtons = PanelCommandItem.GetComponentsInChildren<CommandButtonSet> ();
	}

	void OnEnable()
	{
        if (UseChangeCharEvent)
        {
            _IdChar = (PauseUIController.Instance && PauseUIController.Instance.isActiveAndEnabled) ? PauseUIController.Instance.selectedCharId : "";
            EventManager.Instance.AddListener<ChangeUICharSelectedID>(ChangeUICharSelectedIDHandler);
        }
        //else
        //{
        //    RefreshIDChar();
        //}
        RefreshData();
	}

	void OnDisable()
	{
		if (EventManager.Instance) {
			EventManager.Instance.RemoveListener<ChangeUICharSelectedID> (ChangeUICharSelectedIDHandler);
		}
	}

    void RefreshIDChar()
    {
        if(IDChar != null)
            _IdChar = IDChar._ID;
    }

    public void RefreshData()
    {
        if (!UseChangeCharEvent)
            RefreshIDChar();
        SetCharacterBiodata();

        if (!AttributeHandler)
            AttributeHandler = GetComponent<ShowCharacterAttribute>();
        if (AttributeHandler)
            AttributeHandler.RefreshData(_IdChar);

        if (PanelCommandItem)
        {
            for (int i = 0; i < _CommandButtons.Length; i++)
            {
                _CommandButtons[i].SetCommand(_IdChar);
            }
        }		
    }

	void ChangeUICharSelectedIDHandler(ChangeUICharSelectedID e)
	{
		_IdChar = e.Id;
		SetCharacterBiodata ();
	}

	void SetCharacterBiodata()
	{
		Legrand.core.MainCharacter chara = PartyManager.Instance.GetCharacter (_IdChar);
        if (chara == null)
            return;

        ShowCharacterPortrait(chara);

        if (CharLevelText)
            CharLevelText.text = chara.Level.ToString();
		if (NameText)
			NameText.text = chara._Name;
		if (ClassText)
			ClassText.text = chara._BattleClass.ClassType.ToString();
		if (ElementImage)
			ElementImage.sprite = LegrandBackend.Instance.GetSpriteData ("Element" + chara.CharacterElements.Elements [0].ToString ());
		if (WeaponTypeImage)
			WeaponTypeImage.sprite = LegrandBackend.Instance.GetSpriteData ("Weapon" + chara.BaseWeapon.Type.ToString ());
		if (CharWeaponType)
			CharWeaponType.DefineWeaponCircle (chara.BaseWeapon);
		if (CharElementHandler)
			CharElementHandler.DefineElementCircle (chara.CharacterElements);
	}

	void AssignNewCommandHandler(AssignNewCommandEvent e)
	{
		if (e.CharID.Equals (_IdChar)) {
			for (int i = 0; i < _CommandButtons.Length; i++) {
				_CommandButtons [i].SetCommand (_IdChar);
			}
		}
	}

	public void AddPreviewEvent()
	{
		if (PanelCommandItem) {
			EventManager.Instance.AddListener<InspectItemEvent> (InspectItemEventHandler);
			EventManager.Instance.AddListener<AssignNewCommandEvent> (AssignNewCommandHandler);
		}
	}

	public void RemovePreviewEvent()
	{
		if (EventManager.Instance) {
			EventManager.Instance.RemoveListener<InspectItemEvent> (InspectItemEventHandler);
			EventManager.Instance.RemoveListener<AssignNewCommandEvent> (AssignNewCommandHandler);
		}
	}

	void InspectItemEventHandler(InspectItemEvent e)
	{
		if (e.Item == null)
			return;
		if (e.Item.ItemType == Item.Type.Consumable) {
			if (e.Item is Consumable && ((Consumable)e.Item).ConsumableType == Consumable.ConsumeType.Attributes) {
                ShowPanelAttributes();
			}
			else
			{
                ShowPanelCommandItem();
			}

		} else {
            ShowPanelCommandItem();
		}
	}

    public void ShowPanelAttributes()
    {
        if (PanelAttributes)
            PanelAttributes.SetActive(true);
        if (PanelCommandItem)
            PanelCommandItem.SetActive(false);
    }

    public void ShowPanelCommandItem()
    {
        if (PanelAttributes)
            PanelAttributes.SetActive(false);
        if (PanelCommandItem)
            PanelCommandItem.SetActive(true);
    }

    public void HideCharacterPortrait()
    {
        if(CharacterImage)
        {
            CharacterImage.gameObject.SetActive(false);
        }
    }

    public void ShowCharacterPortrait(Legrand.core.MainCharacter chara)
    {
        if (CharacterImage)
        {
            //to make sure the object is active
            CharacterImage.gameObject.SetActive(true);

            string key = "Portrait";
            int model = PartyManager.Instance.StoryProgression.GetLatestModelSet(chara._ID);
            key += chara._Name + (model != null ? model.ToString() : "");

            CharacterImage.sprite = LegrandBackend.Instance.GetSpriteData(key);
            if (CharacterImage.sprite == null)
            {
                key = "Portrait" + chara._Name;
                CharacterImage.sprite = LegrandBackend.Instance.GetSpriteData(key);
            }
        }
    }

	#region ISelectHandler implementation

	public void OnSelect (BaseEventData eventData)
	{
		PauseUIController.Instance.selectedCharId = _IdChar;
	}

	#endregion
}
