﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ShowNextGrimoire : AbsObjectListGenerator {
    public GameObject PreviewHolder;
    public bool HandleChangeChar;

    private List<LockedMagic> _ChoosenLockedMagic = new List<LockedMagic>();
    private List<LockedSkill> _ChoosenLockedSkill = new List<LockedSkill>();
    private int[,] _ChoosenGrimoire = new int[5, 2]; // ex : _ChoosenGrimoire[0][x] => x: {0 = type choosen grimoire dari index ke 0. type : 0 = Magic, 1 = skill.},{1 = index choosen grimoire dari index ke 0}

    private Legrand.core.Attribute _Attribute;

    private bool _IsFocusOnChild;
    public bool IsFocusOnChild
    {
        get { return _IsFocusOnChild; }
    }
    private Button _CancelButton;

    string _IdChar;

    void OnEnable()
    {
        EventManager.Instance.AddListener<InspectAttributeEvent>(InspectAttributeEventHandler);
        if(HandleChangeChar)
            EventManager.Instance.AddListener<ChangeUICharSelectedID>(ChangeCharUIEventHandler);
    }

    void OnDisable()
    {
        if(EventManager.Instance)
            EventManager.Instance.RemoveListener<InspectAttributeEvent>(InspectAttributeEventHandler);
    }

    void ResetChoosenGrimoire()
    {
        for(int i = 0; i < _ChoosenGrimoire.GetLength(0); i++)
        {
            _ChoosenGrimoire[i,0] = -1;
            _ChoosenGrimoire[i,1] = -1;
        }
    }

    void ChangeCharUIEventHandler(ChangeUICharSelectedID e)
    {
        _IdChar = e.Id;
        EventManager.Instance.TriggerEvent(new InspectAttributeEvent(PartyManager.Instance.GetCharacter(_IdChar), _Attribute));
    }

    void InspectAttributeEventHandler(InspectAttributeEvent e)
    {
        _Attribute = e.Attrib;

        _ChoosenLockedMagic.Clear();
        _ChoosenLockedSkill.Clear();

        //choosen magic
        foreach(LockedMagic lm in e.Chara._LockedMagics)
        {
            if (lm.AttributeRequirements.Find(x => x.AttributeType == e.Attrib) != null && lm.CharacterClassLevel <= e.Chara.ClassLevel)
                _ChoosenLockedMagic.Add(lm);
        }
        if (_ChoosenLockedMagic.Count > 0)
            _ChoosenLockedMagic.Sort(CompareLockedMagic);

        //choosen skill
        foreach (LockedSkill ls in e.Chara._LockedNormalSkill)
        {
            if (ls.AttributeRequirements.Find(x => x.AttributeType == e.Attrib) != null && ls.CharacterClassLevel <= e.Chara.ClassLevel)
                _ChoosenLockedSkill.Add(ls);
        }
        if (_ChoosenLockedSkill.Count > 0)
            _ChoosenLockedSkill.Sort(CompareLockedSkill);

        if(_ChoosenLockedMagic.Count == 0 && _ChoosenLockedSkill.Count == 0)
        {
            if(PreviewHolder)
                PreviewHolder.SetActive(false);
        }
        else
        {
            if (PreviewHolder)
                PreviewHolder.SetActive(true);
        }

        int indexMagic = 0;
        int indexSkill = 0;

        float magicValue = GetMagicValue(indexMagic);
        float skillValue = GetSkillValue(indexSkill);

        //Set Choosen Grimoire
        ResetChoosenGrimoire();
        for(int i =0; i<5; i++)
        {
            if (magicValue >= 0 && skillValue >= 0)
            {
                if (magicValue < skillValue)
                {
                    _ChoosenGrimoire[i, 0] = 0;
                    _ChoosenGrimoire[i, 1] = indexMagic;
                    magicValue = GetMagicValue(++indexMagic);
                }
                else //if(skillValue < magicValue) // requirement skill lebih kecil dari magic
                {
                    _ChoosenGrimoire[i, 0] = 1;
                    _ChoosenGrimoire[i, 1] = indexSkill;
                    skillValue = GetSkillValue(++indexSkill);
                }
            }
            else
            {
                if(magicValue >= 0 && skillValue <0)
                {
                    _ChoosenGrimoire[i, 0] = 0;
                    _ChoosenGrimoire[i, 1] = indexMagic;
                    magicValue = GetMagicValue(++indexMagic);
                }
                else if(skillValue >= 0 && magicValue < 0)
                {
                    _ChoosenGrimoire[i, 0] = 1;
                    _ChoosenGrimoire[i, 1] = indexSkill;
                    skillValue = GetSkillValue(++indexSkill);
                }
            }
        }

        int currentAttrib = 0;
        int maxAttrib = 0;
        int leastIndex = 0;
        for (int i = 0; i < 5; i++ )
        {
            if(_ChoosenGrimoire[i, 0] == 0)
            {
                currentAttrib = _ChoosenLockedMagic[_ChoosenGrimoire[i, 1]].AttributeRequirements.Count;
            }
            else if (_ChoosenGrimoire[i, 0] == 1)
            {
                currentAttrib = _ChoosenLockedSkill[_ChoosenGrimoire[i, 1]].AttributeRequirements.Count;
            }

            if(currentAttrib > maxAttrib)
            {
                if(maxAttrib > 0)
                {
                    //???? lupa apa ini
                }
                maxAttrib = currentAttrib;
                leastIndex = i;
            }
            else if(currentAttrib < maxAttrib)
            {
                ShiftUpChoosenGrimoire(i, i - leastIndex);
                maxAttrib = currentAttrib;
                i = leastIndex-1;
                i = -1;
            }
        }

        UpdateList();
    }

    void ShiftUpChoosenGrimoire(int index, int count)
    {
        if (count < 0)
            return;
        int[] temp = new int[2];
        temp[0] = _ChoosenGrimoire[index,0];
        temp[1] = _ChoosenGrimoire[index, 1];

        for(int i = 0; i<count; i++)
        {
            _ChoosenGrimoire[index - i, 0] = _ChoosenGrimoire[index - i-1, 0];
            _ChoosenGrimoire[index - i, 1] = _ChoosenGrimoire[index - i-1, 1];
        }

        _ChoosenGrimoire[index - count, 0] = temp[0];
        _ChoosenGrimoire[index - count, 1] = temp[1];
    }

    float GetMagicValue(int index)
    {
        return _ChoosenLockedMagic.Count > index ? _ChoosenLockedMagic[index].AttributeRequirements.Find(x => x.AttributeType == _Attribute).value : -1;
    }

    float GetSkillValue(int index)
    {
        return _ChoosenLockedSkill.Count > index ? _ChoosenLockedSkill[index].AttributeRequirements.Find(x => x.AttributeType == _Attribute).value : -1;
    }

    public int CompareLockedMagic(LockedMagic magicA, LockedMagic magicB)
    {
        float a = magicA.AttributeRequirements.Find(x => x.AttributeType == _Attribute).value;
        float b = magicB.AttributeRequirements.Find(x => x.AttributeType == _Attribute).value;

        return a.CompareTo(b); ;
    }

    public int CompareLockedSkill(LockedSkill skillA, LockedSkill skillB)
    {
        float a = skillA.AttributeRequirements.Find(x => x.AttributeType == _Attribute).value;
        float b = skillB.AttributeRequirements.Find(x => x.AttributeType == _Attribute).value;

        return a.CompareTo(b); ;
    }

    public void ShowPreview()
    {
        if (PreviewHolder)
            PreviewHolder.SetActive(true);
    }

    public void HidePreview()
    {
        if (PreviewHolder)
            PreviewHolder.SetActive(false);
    }

    #region Abstract Implementation
    public override void StartLoopUpdate()
    {
        for (int i = 0; i < 5; i++)
        {
            if (_ChoosenGrimoire[i, 0] == 0)
                UpdateEachButton(_ChoosenLockedMagic[_ChoosenGrimoire[i, 1]].Magic);
            else if (_ChoosenGrimoire[i, 0] == 1)
                UpdateEachButton(_ChoosenLockedSkill[_ChoosenGrimoire[i, 1]].Skill);
            else
                continue;
        }
    }

    public override bool CheckConditionPreUpdate(object data)
    {
        return true;
    }

    public override void SetButtonName(UnityEngine.UI.Button button, object data)
    {
        if (data is Magic)
        {
            button.name = ((Magic)data).Name;
        }
        else if (data is NormalSkill)
        {
            button.name = ((NormalSkill)data).Name;
        }
    }

    public override void AfterDataUpdatedProcess(IObjectData objectData, object data)
    {
        ((MagicSkillData)objectData).SetCharId(_IdChar);
        if (data is Magic)
        {
            ((MagicSkillData)objectData).SetRequirementLockedMagic(_ChoosenLockedMagic.Find(x => x.Magic.Name.Equals(((Magic)data).Name)));
        }
        else if (data is NormalSkill)
        {
            ((MagicSkillData)objectData).SetRequirementLockedSkill(_ChoosenLockedSkill.Find(x => x.Skill.Name.Equals(((NormalSkill)data).Name)));
        }
    }
    #endregion

    public void OnCancel()
    {
        _CancelButton.Select();
        _IsFocusOnChild = false;
    }

    public override bool FocusToFirstButton()
    {
        _CancelButton = UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.GetComponent<Button>();
        _IsFocusOnChild = true;
        return base.FocusToFirstButton();
    }
}
