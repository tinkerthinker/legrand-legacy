﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using TeamUtility.IO;
using Legrand.core;

public class DistributeExp : MonoBehaviour, IUpdateSelectedHandler, ISelectHandler {
	public Attribute DistributeAtt;
	public Text ExpForUpText;
	public Text AttribValueText;
	public Text AttribSimulatedValueText;

	public Image AttribValueBar;

	private Color _DefaultSimulatedColor;
    public Color ChangedValueColor = new Color(0, 0, 1, 1);

    public UIObjectMovePosition SimulatedValueMoveObject;

    private string _CharId;

    private float InputActionPerSecond;

    private bool _IsReady = false;

    private InputTimer _Timer;

	void Init()
	{
		if (AttribSimulatedValueText)
			_DefaultSimulatedColor = AttribSimulatedValueText.color;
		else
			_DefaultSimulatedColor = Color.black;
        _IsReady = true;

        if (EventSystem.current.GetComponent<TeamUtility.IO.StandaloneInputModule>() != null)
            InputActionPerSecond = EventSystem.current.GetComponent<TeamUtility.IO.StandaloneInputModule>().inputActionsPerSecond;
        _Timer = new InputTimer(BaseEnum.Orientation.Horizontal, InputActionPerSecond);
	}

	void Awake()
	{
		Init ();
	}

	void Start()
	{
		GetComponent<Button> ().onClick.AddListener (() => ConfirmDistributeExp ());
	}

    void OnEnable()
    {
        //StartCoroutine(WaitReady()); // don't need to wait init because init is executed in awake
        RefreshData();
        AttribSimulatedValueText.gameObject.SetActive(false);
        EventManager.Instance.AddListener<ChangeUICharSelectedID>(ChangeCharSelectedIdHandler);
    }

    void OnDisable()
    {
        if(EventManager.Instance)
            EventManager.Instance.RemoveListener<ChangeUICharSelectedID>(ChangeCharSelectedIdHandler);
    }

	void ChangeCharSelectedIdHandler(ChangeUICharSelectedID c)
	{
        _CharId = c.Id;
        //if(PauseUIController.Instance != null)
			RefreshData ();
	}

	IEnumerator WaitReady()
	{
		while (!_IsReady) {
			yield return null;
		}
		RefreshData ();
	}

	public void Refresh()
	{
		RefreshData ();
        if (SimulatedValueMoveObject)
        {
            if(EventSystem.current.currentSelectedGameObject != this.gameObject)
                SimulatedValueMoveObject.GoToNormalPos();
        }
	}

	void RefreshData()
	{
		if (PartyManager.Instance.CharacterParty.Count==0 || !_IsReady)
			return;

        MainCharacter chara = PartyManager.Instance.GetCharacter((PauseUIController.Instance && PauseUIController.Instance.isActiveAndEnabled) ? PauseUIController.Instance.selectedCharId : _CharId);

        if (chara == null)
            return;

		int expForUp=0;
		int attribValue = (int)chara.BaseBattleAttributes [DistributeAtt];

		if(ExpForUpText)
        {
            if(expForUp >= 0)
                ExpForUpText.text = expForUp.ToString();
            else
                ExpForUpText.text = "-";
        }
			
		if (AttribValueText)
			AttribValueText.text = (attribValue).ToString();
        SetSimulatedValue(attribValue, attribValue);
	}

	public void SetSimulatedValue(int expForUp,int baseValue, int simulatedValue)
	{
		if (ExpForUpText)
			ExpForUpText.text = expForUp.ToString ();
		if (AttribSimulatedValueText) {
			if (simulatedValue > baseValue) {
				AttribSimulatedValueText.gameObject.SetActive (true);
				AttribSimulatedValueText.color = Color.blue;
			}
			else {
				AttribSimulatedValueText.gameObject.SetActive (false);
				AttribSimulatedValueText.color = _DefaultSimulatedColor;
			}
			AttribSimulatedValueText.text = simulatedValue.ToString ();
		}
	}

    public void SetSimulatedValue(int baseValue, int simulatedValue)
    {
        if (AttribSimulatedValueText)
        {
            if (simulatedValue > baseValue)
            {
                AttribSimulatedValueText.gameObject.SetActive(true);
                AttribSimulatedValueText.color = ChangedValueColor;
                if(SimulatedValueMoveObject)
                {
                    if (SimulatedValueMoveObject.CurrentIndexNormalPosition != 1)
                    {
                        SimulatedValueMoveObject.SetNormalPositionIndex(1);                    
                        //SimulatedValueMoveObject.GoToNormalPos();
                        SimulatedValueMoveObject.GoToEndPos();
                    }
                        
                }
            }
            else
            {
                AttribSimulatedValueText.gameObject.SetActive(false);
                AttribSimulatedValueText.color = _DefaultSimulatedColor;
                if (SimulatedValueMoveObject)
                {
                    if (SimulatedValueMoveObject.CurrentIndexNormalPosition != 0)
                    {
                        SimulatedValueMoveObject.SetNormalPositionIndex(0);                    
                        //SimulatedValueMoveObject.GoToNormalPos();
                        SimulatedValueMoveObject.GoToEndPos();
                    }
                }
            }
            AttribSimulatedValueText.text = simulatedValue.ToString();
        }
    }

	#region IUpdateSelectedHandler implementation

	public void OnUpdateSelected (BaseEventData eventData)
	{
        _Timer.ScanInput();
        if (_Timer.IsAllowed)
        {
            if (_Timer.GetInput() > 0.2f)
            {
                SimulateChange(1);
            }
            if (_Timer.GetInput() < -0.2f)
            {
                SimulateChange(-1);
            }
        }
	}

	#endregion
	
	void SimulateChange(int value)
	{
//		SimulateExpController.Instance.TrySimulateExp (DistributeAtt, value);
		EventManager.Instance.TriggerEvent(new SimulateExpEvent(DistributeAtt, value));
	}

	public void ConfirmDistributeExp()
	{
//		SimulateExpController.Instance.DistributeSimulatedExp ();
		EventManager.Instance.TriggerEvent(new DistributeExpEvent());
	}

    public void OnSelect(BaseEventData eventData)
    {
        MainCharacter chara = PartyManager.Instance.GetCharacter((PauseUIController.Instance && PauseUIController.Instance.isActiveAndEnabled) ? PauseUIController.Instance.selectedCharId : _CharId);
        EventManager.Instance.TriggerEvent(new InspectAttributeEvent(chara, DistributeAtt));
    }
}
