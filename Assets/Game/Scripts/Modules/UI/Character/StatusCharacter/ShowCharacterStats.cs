﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;

public class ShowCharacterStats : MonoBehaviour {
	private string currSelectedCharId;
	private int _Health, _PAtk, _PDef, _MAtk, _MDef, _Crit, _Eva;

	public Text MaxHPText;
	public Text PhyAttackText;
	public Text PhyDefenseText;
	public Text MagAttackText;
	public Text MagDefenseText;
	public Text CritHitText;
	public Text EvasionText;
	public Text AccurText;

	public Text SimulatedMaxHPText;
	public Text SimulatedPhyAttackText;
	public Text SimulatedPhyDefenseText;
	public Text SimulatedMagAttackText;
	public Text SimulatedMagDefenseText;
	public Text SimulatedCritHitText;
	public Text SimulatedEvasionText;
	public Text SimulatedAccurText;

    public bool AlwaysShowSimulation;

	public Color ChangedValueColor = new Color (0, 0, 1, 1);
	public Color DefaultValueColor = new Color (1, 1, 1, 1);

	[Header("Determine if handle character change based on ChangeCharSelectedId Event")]
	public bool HandleChangeChar = true;

	void Init()
	{
		
	}

	void OnDestroy()
	{
//		if (EventManager.Instance && HandleChangeChar) {
//			EventManager.Instance.RemoveListener<ChangeUICharSelectedID> (ChangeCharSelectedIdHandler);
//		}
	}

	void Start()
	{
//		if(HandleChangeChar)
//			Init ();
//		if(HandleChangeChar)
//			RefreshData ();
	}

	void OnEnable()
	{
		if (PauseUIController.Instance != null && PartyManager.Instance != null && HandleChangeChar) {
			EventManager.Instance.AddListener<ChangeUICharSelectedID>(ChangeCharSelectedIdHandler);

			currSelectedCharId = PauseUIController.Instance.selectedCharId;
			RefreshData ();
		}
	}

	void OnDisable()
	{
		if (EventManager.Instance && HandleChangeChar) {
			EventManager.Instance.RemoveListener<ChangeUICharSelectedID> (ChangeCharSelectedIdHandler);
		}
	}

	void ChangeCharSelectedIdHandler(ChangeUICharSelectedID c)
	{
		if (PauseUIController.Instance != null) {
			currSelectedCharId = c.Id;
			RefreshData ();
		}
	}

	public void RefreshData()
	{
//		currSelectedCharId = PauseUIController.Instance.selectedCharId;
		HideAllSimulatedObject ();
		SetData ();
	}

	public void RefreshData(string selectedCharId)
	{
		currSelectedCharId = selectedCharId;

		RefreshData ();
	}

	public void HideAllSimulatedObject()
	{
		SetSimulatedText (SimulatedMaxHPText, 0, 0);
		SetSimulatedText (SimulatedPhyAttackText, 0, 0);
		SetSimulatedText (SimulatedPhyDefenseText, 0, 0);
		SetSimulatedText (SimulatedMagAttackText, 0, 0);
		SetSimulatedText (SimulatedMagDefenseText, 0, 0);
		SetSimulatedText (SimulatedCritHitText, 0, 0);
		SetSimulatedText (SimulatedEvasionText, 0, 0);
		SetSimulatedText (SimulatedAccurText, 0, 0);
	}

	void SetData()
	{		
		MainCharacter chara = PartyManager.Instance.GetCharacter (currSelectedCharId);
        if (chara == null)
            return;

		if (MaxHPText) {
			MaxHPText.text = chara.Health.MaxValue.ToString ("#,#");
		}
		if (PhyAttackText) {
            PhyAttackText.text = Formula.CalculateTotalDamage(chara).ToString("#,#");
		}
		if (PhyDefenseText) {
            PhyDefenseText.text = Formula.CalculatePhysicalDefense(chara).ToString("#,#");
		}
		if (MagAttackText) {
            MagAttackText.text = Formula.CalculateINTMagicDamage(chara).ToString("#,#");
		}
		if (MagDefenseText) {
            MagDefenseText.text = Formula.CalculateMagicDefense(chara).ToString("#,#");
		}
		if (CritHitText) {
            CritHitText.text = Formula.GetCritRate(chara).ToString("#.00");
            //CritHitText.text = Formula.GetCritRate (chara).ToString("F2");
            //CritHitText.text = string.Format("Weight: {0:#.00} kg", currentweight);
		}
		if (EvasionText) {
            //EvasionText.text = Formula.GetEvaRate(chara).ToString("F2");
            EvasionText.text = Formula.GetEvaRate(chara).ToString("#.00");
		}
		if (AccurText) {
            //AccurText.text = Formula.GetAccRate(chara).ToString("F2");
            AccurText.text = Formula.GetAccRate(chara).ToString("#.00");
		}
	}

	public void SetSimulatedValue(Dictionary<Attribute, int> addValue)
	{
		MainCharacter chara = PartyManager.Instance.GetCharacter (currSelectedCharId);

		if (SimulatedMaxHPText) {
			float health = Formula.CalculateHealth (chara,chara.GetAttribute(Attribute.VIT) + addValue[Attribute.VIT]);

			SetSimulatedText (SimulatedMaxHPText, Formula.CalculateHealth (chara), health);

            SimulatedMaxHPText.text = health.ToString("#,#");
		}
		if (SimulatedPhyAttackText) {
			float pAtk = Formula.CalculateTotalDamage (chara, chara.GetAttribute (Attribute.STR) + addValue [Attribute.STR], chara.GetAttribute (Attribute.AGI) + addValue [Attribute.AGI]);

			SetSimulatedText (SimulatedPhyAttackText, Formula.CalculateTotalDamage (chara), pAtk);

            SimulatedPhyAttackText.text = pAtk.ToString("#,#");
		}
		if (SimulatedPhyDefenseText) {
			float pDef = Formula.CalculatePhysicalDefense (chara, chara.GetAttribute (Attribute.VIT) + addValue [Attribute.VIT]);

			SetSimulatedText (SimulatedPhyDefenseText, Formula.CalculatePhysicalDefense (chara), pDef);

            SimulatedPhyDefenseText.text = pDef.ToString("#,#");
		}
		if (SimulatedMagAttackText) {
			float mAtk = Formula.CalculateINTMagicDamage (chara, chara.GetAttribute (Attribute.INT) + addValue [Attribute.INT]);

			SetSimulatedText (SimulatedMagAttackText, Formula.CalculateINTMagicDamage (chara), mAtk);

            SimulatedMagAttackText.text = mAtk.ToString("#,#");
		}
		if (SimulatedMagDefenseText) {
			float mDef = Formula.CalculateMagicDefense (chara, chara.GetAttribute (Attribute.INT) + addValue [Attribute.INT]);

			SetSimulatedText (SimulatedMagDefenseText, Formula.CalculateMagicDefense (chara), mDef);

            SimulatedMagDefenseText.text = mDef.ToString("#,#");
		}
		if (SimulatedCritHitText) {
			float crit = Formula.GetCritRate (chara, chara.GetAttribute (Attribute.AGI) + addValue [Attribute.AGI]);

			SetSimulatedText (SimulatedCritHitText, Formula.GetCritRate (chara), crit);

            SimulatedCritHitText.text = crit.ToString("F2");
		}
		if (SimulatedEvasionText) {
			float eva = Formula.GetEvaRate (chara, chara.GetAttribute (Attribute.AGI) + addValue [Attribute.AGI]);

			SetSimulatedText (SimulatedEvasionText, Formula.GetEvaRate (chara), eva);

            SimulatedEvasionText.text = eva.ToString("F2");
		}
		if (SimulatedAccurText) {
			float acc = Formula.GetAccRate (chara, chara.GetAttribute (Attribute.AGI) + addValue [Attribute.AGI]);

			SetSimulatedText (SimulatedAccurText, Formula.GetAccRate (chara), acc);

            SimulatedAccurText.text = acc.ToString("F2");
		}
	}

    public void SetBonusFormation(bool IsFront)
    {
        MainCharacter chara = PartyManager.Instance.GetCharacter(currSelectedCharId);
        float value;
        if(IsFront)
        {
            if (SimulatedPhyAttackText)
            {
                value = Formula.CalculateTotalDamage(chara);
                SimulatedPhyAttackText.text = (value * 1.1f).ToString("#,#");
                SetSimulatedText(SimulatedPhyAttackText, value, value * 1.1f);
            }
            if (SimulatedMagAttackText)
            {
                value = Formula.CalculateINTMagicDamage(chara);
                SimulatedMagAttackText.text = (value * 1.1f).ToString("#,#");
                SetSimulatedText(SimulatedMagAttackText, value, value * 1.1f);
            }
            SetSimulatedText(SimulatedPhyDefenseText, 0, 0);
            SetSimulatedText(SimulatedMagDefenseText, 0, 0);
        }
        else
        {
            if (SimulatedPhyDefenseText)
            {
                value = Formula.CalculatePhysicalDefense(chara);
                SimulatedPhyDefenseText.text = (value * 1.1f).ToString("#,#");
                SetSimulatedText(PhyDefenseText, value, value * 1.1f);
            }
            if (SimulatedMagDefenseText)
            {
                value = Formula.CalculateMagicDefense(chara);
                SimulatedMagDefenseText.text = (value * 1.1f).ToString("#,#");
                SetSimulatedText(SimulatedMagDefenseText, value, value * 1.1f);
            }
            SetSimulatedText(SimulatedPhyAttackText, 0, 0);
            SetSimulatedText(SimulatedMagAttackText, 0, 0);
        }
    }

	void SetSimulatedText(Text textObj, float baseValue, float newValue)
	{
		if (textObj == null)
			return;
		
		if (newValue > baseValue) {
            if(!AlwaysShowSimulation)
			    textObj.gameObject.SetActive (true);
			textObj.color = ChangedValueColor;
		} else {
            if (!AlwaysShowSimulation)
			    textObj.gameObject.SetActive (false);
			textObj.color = DefaultValueColor;
		}
	}

}
