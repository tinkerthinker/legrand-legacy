﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Legrand.core;

public class ShowCharacterAttribute : MonoBehaviour {
	private string currSelectedCharId;
	private int STR, VIT, INT, WIS, AGI;
	private float expSTR, expVIT, expINT, expWIS, expAGI;

	[Header("Is used to show Current Exp Character Have")]
	public Text CharRemainExpText;

	public Text AgiText;
	public Text StrText;
	public Text VitText;
	public Text IntText;
	public Text WisText;
	
	public Image AgiExpImg;
	public Image StrExpImg;
	public Image VitExpImg;
	public Image IntExpImg;
	public Image WisExpImg;
	
	public RadarGraphUI RadarGraph;

	[Header("Determine if handle character change based on ChangeCharSelectedId Event")]
	public bool HandleChangeChar = true;

    //void Init()
    //{
    //    EventManager.Instance.AddListener<ChangeUICharSelectedID>(ChangeCharSelectedIdHandler);
    //}

    //void OnDestroy()
    //{
    //    if (EventManager.Instance && HandleChangeChar) {
    //        EventManager.Instance.RemoveListener<ChangeUICharSelectedID> (ChangeCharSelectedIdHandler);
    //    }
    //}

	void Start()
	{
		if(RadarGraph)
			RadarGraph.Init ();
		if(HandleChangeChar)
			RefreshData ();
	}

	void OnEnable()
	{
        if (HandleChangeChar)
            EventManager.Instance.AddListener<ChangeUICharSelectedID>(ChangeCharSelectedIdHandler);
		if(PauseUIController.Instance != null && PartyManager.Instance != null && HandleChangeChar)
			RefreshData ();
	}

    void OnDisable()
    {
        if (EventManager.Instance && HandleChangeChar)
        {
            EventManager.Instance.RemoveListener<ChangeUICharSelectedID>(ChangeCharSelectedIdHandler);
        }
    }
	
	void Update()
	{
	}

	void ChangeCharSelectedIdHandler(ChangeUICharSelectedID c)
	{
        currSelectedCharId = c.Id;
        //if(PauseUIController.Instance != null)
			RefreshData ();
	}

	public void RefreshData(string selectedCharId)
	{
		currSelectedCharId = selectedCharId;

		ShowCharRemainingExp ();
		GetCharAtrributes ();

		if(RadarGraph)
			StartCoroutine(WaitRadarGraphReady());
		else
			ShowCharacterAttributes ();
	}

	IEnumerator WaitRadarGraphReady()
	{
		while (!RadarGraph.IsReady)
			yield return null;
		ShowCharacterAttributes ();
	}

	void RefreshData()
	{
        if ((PauseUIController.Instance && PauseUIController.Instance.isActiveAndEnabled))
		    currSelectedCharId = PauseUIController.Instance.selectedCharId;

		ShowCharRemainingExp ();
		GetCharAtrributes ();		
		ShowCharacterAttributes ();
	}

	void ShowCharRemainingExp()
	{
		if (CharRemainExpText) {
			CharRemainExpText.text = PartyManager.Instance.CharacterParty.Count>0? PartyManager.Instance.GetCharacter(currSelectedCharId).StoredExperience.ToString(): "0";
		}
	}

	void GetCharAtrributes()
	{
		if (PartyManager.Instance.CharacterParty.Count == 0)
			return;

		MainCharacter chara = PartyManager.Instance.GetCharacter(currSelectedCharId);
        if (chara == null)
            return;
		STR = (int) chara.BaseBattleAttributes[Attribute.STR];
		VIT = (int) chara.BaseBattleAttributes[Attribute.VIT];
		INT = (int) chara.BaseBattleAttributes[Attribute.INT];
		WIS = (int) chara.BaseBattleAttributes[Attribute.LUCK];
		AGI = (int) chara.BaseBattleAttributes[Attribute.AGI];
	}
	
	void ShowCharacterAttributes()
	{
		//diurutkan berdasar urutan di radar graph, Clock Wise, all text must not null. idc
		AgiText.text = AGI.ToString();
		IntText.text = INT.ToString();
		WisText.text = WIS.ToString();
		VitText.text = VIT.ToString();
		StrText.text = STR.ToString();


		//draw RadarGraph
		if (RadarGraph) {
			RadarGraph.districtValue [0] = AGI;
			RadarGraph.districtValue [1] = INT;
			RadarGraph.districtValue [2] = WIS;
			RadarGraph.districtValue [3] = VIT;
			RadarGraph.districtValue [4] = STR;

			int threshold = 20, step = 5, indexTH = 1;

			foreach (int value in RadarGraph.districtValue) {
				for (int i = 1; i <= step; i++) {
					if (value <= i * threshold) {
						if (i > indexTH) {
							indexTH = i;
						}
						break;
					}
				}

			}
			RadarGraph.maxValue = indexTH * threshold;
		
			RadarGraph.UpdateGraph ();
		}
	}
}
