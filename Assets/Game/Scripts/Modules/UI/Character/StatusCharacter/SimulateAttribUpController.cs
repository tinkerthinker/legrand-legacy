﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;

public class SimulateAttribUpController : MonoBehaviour {
	private int _AtpRemaining;
	public int AtpRemaining
	{
		get{return _AtpRemaining;}
	}
	private int _UsedAtp;
	public int UsedAtp
	{
		get{return _UsedAtp;}
	}
	[Header("Is used to show Remaining Atp while simulate")]
	public Text RemainingAtpText;
	private Color _DefaultRemainingTextColor;

	public List<DistributeExp> DistributeExpObjs;

	public Dictionary<Attribute, int> StoredExp = new Dictionary<Attribute, int>();
	public Dictionary<Attribute, int> InitExp = new Dictionary<Attribute, int>();
	public Dictionary<Attribute, int> SimulatedLevel = new Dictionary<Attribute, int>();

	public ShowCharacterStats CharacterStatsHandler;
    public ShiftCharacterSelect Shifter;
    public PanelCharacterController PanelController;

	public RadarGraphUI RadarGraph;

	List<Attribute> _IterateAttribs = new List<Attribute>();

	private const int maxExp = 999999;

	private bool _ConfirmedSimulationCompleted = true;
	public bool ConfirmedSimulationCompleted
	{
		get{return _ConfirmedSimulationCompleted;}
	}

    private bool _IsGainGriomire = false;
    private List<LockedMagic> _GainedMagic;
    private List<LockedSkill> _GainedSkill;

    private bool _IsHaltProcess;

    private string _CharId;

	void Init()
	{
		EventManager.Instance.AddListener<SimulateExpEvent>(SimulateExpEventHandler);
		EventManager.Instance.AddListener<DistributeExpEvent>(DistributeExpEventHandler);

		if (RemainingAtpText)
			_DefaultRemainingTextColor = RemainingAtpText.color;
		else
			_DefaultRemainingTextColor = Color.black;
	}

	void Awake()
	{
		//StartCoroutine (WaitGameReady());
		Init ();

		_IterateAttribs.Add(Attribute.STR);
		_IterateAttribs.Add(Attribute.VIT);
		_IterateAttribs.Add(Attribute.AGI);
		_IterateAttribs.Add(Attribute.INT);
		_IterateAttribs.Add(Attribute.LUCK);

		foreach (Attribute att in _IterateAttribs) {
			StoredExp.Add (att, 0);
		}

		foreach (Attribute att in _IterateAttribs) {
			InitExp.Add (att, 0);
		}

		foreach (Attribute att in _IterateAttribs) {
			SimulatedLevel.Add (att, 0);
		}

		Refresh ();
	}

	void OnEnable () {
        EventManager.Instance.AddListener<ChangeUICharSelectedID>(ChangeCharSelectedIdHandler);
        EventManager.Instance.AddListener<RequestSimulateAttributeEvent>(RequestSimulateAttributeEventHandler);
		if (StoredExp.Count == 0)
			return;
		Refresh ();
	}

    void OnDisable()
    {
        if (EventManager.Instance)
        {
            EventManager.Instance.RemoveListener<ChangeUICharSelectedID>(ChangeCharSelectedIdHandler);
            EventManager.Instance.RemoveListener<RequestSimulateAttributeEvent>(RequestSimulateAttributeEventHandler);
        }
    }

	void OnDestroy()
	{
		if (EventManager.Instance) {			
			EventManager.Instance.RemoveListener<SimulateExpEvent>(SimulateExpEventHandler);
			EventManager.Instance.RemoveListener<DistributeExpEvent>(DistributeExpEventHandler);
		}
	}

	void ChangeCharSelectedIdHandler(ChangeUICharSelectedID c)
	{
//		if (PauseUIController.Instance != null)
//			Refresh ();
        _CharId = c.Id;
		CancelSimulation();
	}

    void RequestSimulateAttributeEventHandler(RequestSimulateAttributeEvent e)
    {
        MainCharacter chara = PartyManager.Instance.GetCharacter((PauseUIController.Instance && PauseUIController.Instance.isActiveAndEnabled) ? PauseUIController.Instance.selectedCharId : _CharId);
        EventManager.Instance.TriggerEvent(new SendSimulateAttributeEvent(chara, SimulatedLevel));
    }

	void SimulateExpEventHandler(SimulateExpEvent e)
	{
		TrySimulateExp (e.Attrib, e.Value);
	}

	void DistributeExpEventHandler(DistributeExpEvent e)
	{
		DistributeSimulatedExp ();
	}

	public void Refresh()
	{
		_UsedAtp = 0;
		_ConfirmedSimulationCompleted = true;
        AllowProcess();
        if (Shifter)
            Shifter.SetInteractable(true);
		
		if (PartyManager.Instance.CharacterParty.Count==0)
			return;

		MainCharacter chara = PartyManager.Instance.GetCharacter ((PauseUIController.Instance && PauseUIController.Instance.isActiveAndEnabled)? PauseUIController.Instance.selectedCharId:_CharId);
        if (chara == null)
            return;

        //_AtpRemaining = chara.StoredExperience;
        _AtpRemaining = chara.AttributePoint;

		UpdateRemainingAtp ();

        foreach (Attribute att in _IterateAttribs)
        {
            StoredExp[att] = 0;
            InitExp[att] = (int)chara.BattleExperiences[att];
            SimulatedLevel[att] = 0;
        }

        //foreach (Attribute att in _IterateAttribs) {
        //    InitExp[att] = (int) chara.BattleExperiences[att];
        //}

        //foreach (Attribute att in _IterateAttribs) {
        //    SimulatedLevel[att] = 0;//(int) chara.BattleAttributes[att];
        //}

        //float expNeeded;
		foreach (DistributeExp dst in DistributeExpObjs) {
            //expNeeded = (int) (chara.BattleExperiencesNeeded[dst.DistributeAtt] );
			dst.Refresh ();
		}

		if(CharacterStatsHandler)
			CharacterStatsHandler.RefreshData ();
	}

	public void TrySimulateExp(Attribute attrib, int value)
	{
		// cek if exp inserted is available
		if (_UsedAtp + value >= 0 && _UsedAtp + value <= _AtpRemaining) {
			SimulateExp(attrib, value);
		}
	}
	
//	public void SimulateExp(Attribute attrib, int exp)
	public void SimulateExp(Attribute attrib, int incLevel)
	{
        MainCharacter chara = PartyManager.Instance.GetCharacter((PauseUIController.Instance && PauseUIController.Instance.isActiveAndEnabled) ? PauseUIController.Instance.selectedCharId : _CharId);

        //check if simulation will be decreased less than current level attrib, or more than max level attrib
		if (SimulatedLevel [attrib] + incLevel < 0 || (incLevel > 0 && ((int)chara.BaseBattleAttributes [attrib] + SimulatedLevel [attrib]) >= 99) )
			return;

		DistributeExp dst = GetDistributeExpObject (attrib);
		if (dst == null)
			return;

        //int expForLevelUp = Formula.EXPforLevelUp((int)chara.BaseBattleAttributes[attrib]+SimulatedLevel[attrib], MainCharacter.MaxLevel, (float)chara.BattleAttributeGrowths[attrib]);

        //simulate if attribute level is same with current attrib level
        if (SimulatedLevel[attrib] == 0)
        {
            IncreaseAttributeLevel(chara, attrib, incLevel);
        }
        //simulate if level inrease, or can decrease if simulatedlevel >0
        else
        {
            ChangeAttributeLevel(chara, attrib, incLevel);
        }
        WwiseManager.Instance.PlaySFX(WwiseManager.UIFX.Add_Status_Character);

		if(CharacterStatsHandler)
			CharacterStatsHandler.SetSimulatedValue(SimulatedLevel);
	}

    void ChangeAttributeLevel(MainCharacter chara, Attribute attrib, int changeValue)
    {
        SimulatedLevel[attrib] += changeValue;
        _UsedAtp += changeValue;

        if (_UsedAtp == 0)
            _ConfirmedSimulationCompleted = true;
        else
            _ConfirmedSimulationCompleted = false;

        DistributeExp dst = GetDistributeExpObject(attrib);
        
        dst.SetSimulatedValue((int)chara.BaseBattleAttributes[attrib], (int)chara.BaseBattleAttributes[attrib] + SimulatedLevel[attrib]);

        UpdateRemainingAtp();

        ChangeAttributeRadar();
    }

	void IncreaseAttributeLevel(MainCharacter chara, Attribute attrib, int changeValue)
	{
		SimulatedLevel[attrib]++;
        //StoredExp [attrib] += expForLevelUp;
		_UsedAtp += changeValue;

		_ConfirmedSimulationCompleted = false;

		DistributeExp dst = GetDistributeExpObject (attrib);
        //changeValue = Formula.EXPforLevelUp((int)chara.BaseBattleAttributes[attrib]+SimulatedLevel[attrib], MainCharacter.MaxLevel, (float)chara.BattleAttributeGrowths[attrib]);

//		dst.AttribValueText.text =( (int)chara.BaseBattleAttributes[attrib] + SimulatedLevel[attrib] ).ToString();

		dst.SetSimulatedValue((int)chara.BaseBattleAttributes[attrib], (int)chara.BaseBattleAttributes[attrib] + SimulatedLevel[attrib] );

		UpdateRemainingAtp ();

		ChangeAttributeRadar();
	}

	void DecreaseAttributeLevel(MainCharacter chara, Attribute attrib, int expForLevelDown)
	{
		SimulatedLevel[attrib]--;
		StoredExp [attrib] -= expForLevelDown;
		if (StoredExp[attrib] < 0)
			Debug.Log ("ERROR CALCULATION !!!! STORED EXP OF "+attrib.ToString()+" COULDN'T BE LESS THAN 0");
		_UsedAtp -= expForLevelDown;
		if (_UsedAtp < 0)
			Debug.Log ("ERROR CALCULATION !!!! USED EXP COULDN'T BE LESS THAN 0");
		if (_UsedAtp == 0)
			_ConfirmedSimulationCompleted = true;
		DistributeExp dst = GetDistributeExpObject (attrib);

		dst.SetSimulatedValue(expForLevelDown,(int)chara.BaseBattleAttributes[attrib], (int)chara.BaseBattleAttributes[attrib] + SimulatedLevel[attrib] );

		UpdateRemainingAtp ();
//		RemainingExpText.text = (_ExpRemaining - _UsedExp).ToString();

		ChangeAttributeRadar();
	}

	void UpdateRemainingAtp()
	{
		if (RemainingAtpText) {
			RemainingAtpText.text = (_AtpRemaining - _UsedAtp).ToString ();
			if (_UsedAtp == 0)
				RemainingAtpText.color = _DefaultRemainingTextColor;
			else
				RemainingAtpText.color = Color.red;
		}
	}

	DistributeExp GetDistributeExpObject(Attribute attrib)
	{
		foreach (DistributeExp dst in DistributeExpObjs) {
			if (dst.DistributeAtt == attrib) {
				return dst;
			}
		}
		return null;
	}

	void ChangeAttributeRadar()
	{
		if (RadarGraph == null)
			return;
		MainCharacter chara = PartyManager.Instance.GetCharacter (PauseUIController.Instance.selectedCharId);

		RadarGraph.districtValue [0] = (int)chara.BaseBattleAttributes[Attribute.AGI] + SimulatedLevel[Attribute.AGI];
		RadarGraph.districtValue [1] = (int)chara.BaseBattleAttributes[Attribute.INT] + SimulatedLevel[Attribute.INT];
		RadarGraph.districtValue [2] = (int)chara.BaseBattleAttributes[Attribute.LUCK] + SimulatedLevel[Attribute.LUCK];
		RadarGraph.districtValue [3] = (int)chara.BaseBattleAttributes[Attribute.VIT] + SimulatedLevel[Attribute.VIT];
		RadarGraph.districtValue [4] = (int)chara.BaseBattleAttributes[Attribute.STR] + SimulatedLevel[Attribute.STR];
		
		int threshold = 20, step = 5, indexTH=1;
		
		foreach (int value in RadarGraph.districtValue)
		{
			for(int i = 1; i<=step;i++)
			{
				if(value <= i*threshold)
				{
					if(i>indexTH)
					{
						indexTH = i;
					}
					break;
				}
			}
			
		}
		RadarGraph.maxValue = indexTH * threshold;
		
		RadarGraph.UpdateGraph ();
	}

	public void DistributeSimulatedExp(bool isNoMeanRefresh = false)
	{
        if (!_ConfirmedSimulationCompleted && !_IsHaltProcess)
        {
            if (isNoMeanRefresh)
                PopUpUI.CallChoicePopUp("Confirm ATP Distribution ?", ConfirmedDistributeSimulatedExp, Refresh);
            else
                PopUpUI.CallChoicePopUp("Confirm ATP Distribution ?", ConfirmedDistributeSimulatedExp, AllowProcess);
            //HaltProcess();
            if (Shifter)
                Shifter.SetInteractable(false);
		}
	}

	private void ConfirmedDistributeSimulatedExp()
	{
        MainCharacter chara = PartyManager.Instance.GetCharacter((PauseUIController.Instance && PauseUIController.Instance.isActiveAndEnabled) ? PauseUIController.Instance.selectedCharId : _CharId);

		foreach (KeyValuePair<Attribute, int> k in SimulatedLevel) {
            //chara.IncreaseBattleAttributeEXP(k.Key, k.Value);
            //chara.StoredExperience -= StoredExp[k.Key];
            chara.LevelUp(k.Key, k.Value);
            chara.AttributePoint -= k.Value;
		}

        _GainedMagic = chara.LearnMagic();
        _GainedSkill = chara.LearnNormalSkill();
		Refresh ();
        if (_GainedMagic.Count == 0 && _GainedSkill.Count == 0)
        {
            _IsGainGriomire = false;
            _ConfirmedSimulationCompleted = true;
        }
        else
        {
            _IsGainGriomire = true;
            StartCoroutine(NotifGainedMagicSkill());
        }
	}

    IEnumerator NotifGainedMagicSkill()
    {
        _ConfirmedSimulationCompleted = false;

        if (_GainedMagic.Count > 0)
        {
            if (Shifter)
                Shifter.SetInteractable(false);
        }
        yield return null;
        //if (_GainedMagic.Count == 0 && _GainedSkill.Count == 0)
        //{
        //    _IsGainGriomire = false;
        //    _ConfirmedSimulationCompleted = true;
        //}
        //else
        //{
        //_IsGainGriomire = true;
        while (_GainedMagic.Count > 0)
        {
            while (_IsHaltProcess)
                yield return null;
            PopUpLearnMagic();
        }

        while (_GainedSkill.Count > 0)
        {
            while (_IsHaltProcess)
                yield return null;
            //PopUpUI.CallNotifPopUp(nameChar + " Learn " + _GainedSkill[0].Skill.Name, AllowProcess);
            //_GainedSkill.RemoveAt(0);
            //HaltProcess();
            PopUpLearnSkill();
        }

        while (_IsHaltProcess)
            yield return null;
        _ConfirmedSimulationCompleted = true;
        //}
    }

    void PopUpLearnMagic()
    {
        LockedMagic magic = _GainedMagic[0];

        WwiseManager.Instance.PlaySFX(WwiseManager.UIFX.Receive_Item);

        if (magic.PreviousMagicId == -1)
            PopUpUI.CallNotifPopUp("Learn " + _GainedMagic[0].Magic.Name, AllowProcess);
        else
        {
            Magic prevMagic = LegrandBackend.Instance.GetMagic(magic.PreviousMagicId);

            PopUpUI.CallNotifPopUp(prevMagic.Name + " Upgraded to " + _GainedMagic[0].Magic.Name, AllowProcess);
        }
        _GainedMagic.RemoveAt(0);
        HaltProcess();
    }

    void PopUpLearnSkill()
    {
        LockedSkill skill = _GainedSkill[0];

        WwiseManager.Instance.PlaySFX(WwiseManager.UIFX.Receive_Item);

        if (string.IsNullOrEmpty(skill.PreviousSkillId))
            PopUpUI.CallNotifPopUp("Learn " + _GainedSkill[0].Skill.Name, AllowProcess);
        else
        {
            NormalSkill prevSkill = LegrandBackend.Instance.GetNormalSkill(skill.PreviousSkillId);

            PopUpUI.CallNotifPopUp(prevSkill.Name + " Upgraded to " + _GainedSkill[0].Skill.Name, AllowProcess);
        }
        _GainedSkill.RemoveAt(0);
        HaltProcess();
    }

    void ResumeDistributeExp()
    {
        AllowProcess();
        _ConfirmedSimulationCompleted = false;
    }

    void HaltProcess()
    {
        _ConfirmedSimulationCompleted = false;
        _IsHaltProcess = true;
    }

    void AllowProcess()
    {
        _IsHaltProcess = false;
        //_ConfirmedSimulationCompleted = false;
        if (_GainedMagic != null && _GainedSkill != null)
        {
            if (_GainedMagic.Count == 0 && _GainedSkill.Count == 0)
            {
                if (Shifter)
                    Shifter.SetInteractable(true);
                if (_IsGainGriomire)
                    StartCoroutine(WaitNotifGrimoireToTutorial());
            }
        }
        else
        {
            if (Shifter)
                Shifter.SetInteractable(true);
        }
    }

    IEnumerator WaitNotifGrimoireToTutorial()
    {
        yield return new WaitForEndOfFrame();
        if (Shifter)
            Shifter.SetInteractable(false);
        PopUpUI.OnEndPopUp += OnEndPopUpHandler;
        PopUpUI.CallTutorialPopUp("AssignGrimoire");
    }

    private void OnEndPopUpHandler(bool choice)
    {
        EnableShifter();
    }

    void EnableShifter()
    {
        if (Shifter)
            Shifter.SetInteractable(true);
    }

	public void CancelSimulation(bool isGotoMain=false)
	{
		if (!_ConfirmedSimulationCompleted && !_IsHaltProcess) {
            PopUpUI.CallChoicePopUp("Cancel ATP Distribution ?", (isGotoMain ? (PopUpUI.YesChoicePopUp)BackToMain : (PopUpUI.YesChoicePopUp)Refresh));
            //HaltProcess();
            if (Shifter)
                Shifter.SetInteractable(false);
		}
		else
        {
            if (isGotoMain)
                BackToMain();
            else
                Refresh();
        }
			
	}

    void BackToMain()
    {
        Refresh();
        if (Shifter)
            Shifter.SetInteractable(false);
        if(PanelController)
        {
            PanelController.GoToMain();
        }
    }
}
