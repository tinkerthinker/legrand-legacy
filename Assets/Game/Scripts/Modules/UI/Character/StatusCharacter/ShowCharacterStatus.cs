using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;
using TeamUtility.IO;

public class ShowCharacterStatus : MonoBehaviour {
	private string currSelectedCharId;
	public Text healthText;
	public Text nameText;
	public Text classText;
	public Text totalWeightText;
	public Image elementImage;
	public Image statusAilment;

	public List<GameObject> modelCharacters;
	public List<Quaternion> modelsDefaultRotation;
	public float rotateSpeed=5;

//	void Awake()
//	{
//		StartCoroutine (WaitGameReady());
//	}
//	
//	IEnumerator WaitGameReady()
//	{
//		while (!PartyManager.Instance._ReadyToPlay)
//			yield return null;
//		Init ();
//	}

	void Init()
	{
		EventManager.Instance.AddListener<ChangeUICharSelectedID>(ChangeCharSelectedIdHandler);
	}

	void OnDestroy()
	{
		if (EventManager.Instance) {
			EventManager.Instance.RemoveListener<ChangeUICharSelectedID> (ChangeCharSelectedIdHandler);
		}
	}

	void Start()
	{
		Init ();

		modelsDefaultRotation = new List<Quaternion> ();
		foreach (GameObject modelChar in modelCharacters)
		{
			modelsDefaultRotation.Add(modelChar.transform.rotation);
		}
	}

	void Update()
	{
//		if (currSelectedCharId != PauseUIController.instance.selectedCharId)
//		{
//			RefreshData();
//		}
		if (EventSystem.current == null || EventSystem.current.currentSelectedGameObject == null)
			return;

		if (EventSystem.current.currentSelectedGameObject.GetComponent<CommandButtonSet> () != null)
			if (EventSystem.current.currentSelectedGameObject.GetComponent<CommandButtonSet> ().panelPosition == CommandPos.Down)
				CalculateTotalWeight ();

		RotateModelsInput();
		//ShiftTabPanel ();

	}

	void OnEnable()
	{
		if(PauseUIController.Instance != null)
			RefreshData ();
	}

	void ChangeCharSelectedIdHandler(ChangeUICharSelectedID c)
	{
		RefreshData ();
	}

	void RefreshData()
	{
		currSelectedCharId = PauseUIController.Instance.selectedCharId;
		if(PartyManager.Instance.CharacterParty.Count>0)
			SetCharacterStatus ();

		if (modelsDefaultRotation.Count > 0)
		{
			int index = 0;
			foreach (GameObject modelChar in modelCharacters)
			{
				modelChar.transform.rotation = modelsDefaultRotation [index];
				index++;
			}
		}
	}

	void SetCharacterStatus()
	{
		//assign name, health, class, element, and lastly status ailment
		if (nameText != null)
		{
			nameText.text = PartyManager.Instance.GetCharacter(currSelectedCharId)._Name;
			if(nameText.GetComponent<TranslateTextUI>() != null)
				nameText.GetComponent<TranslateTextUI>().UpdateData();
			//nameText.text = GetTranslateJapan.CharacterName( PartyManager.Instance.GetCharacter(currSelectedCharId)._Name);
		}

		if (healthText != null)
		{
			MainCharacter chara = PartyManager.Instance.GetCharacter(currSelectedCharId);
			if(chara.GetComponent<Health>() != null)
			{
				healthText.text = (int)chara.GetComponent<Health>().Value + "/" + (int)chara.GetComponent<Health>().MaxValue;
			}
		}

		if (classText != null)
		{
			classText.text = PartyManager.Instance.GetCharacter(currSelectedCharId)._BattleClass.ClassType.ToString();
			if(classText.GetComponent<TranslateTextUI>() != null)
				classText.GetComponent<TranslateTextUI>().UpdateData();
//			classText.text = GetTranslateJapan.Class( PartyManager.Instance.GetCharacter(currSelectedCharId)._BattleClass.ClassType.ToString());
		}

		if (totalWeightText != null)
		{
			CalculateTotalWeight();
		}

		if (elementImage != null)
		{
			SetElemenetImage();
		}

		foreach (GameObject modelChar in modelCharacters)
		{
			if(modelChar.GetComponent<IDChar>()._ID.Equals(currSelectedCharId) )
			{
				modelChar.SetActive(true);
			}
			else
				modelChar.SetActive(false);
		}
	}

	void SetElemenetImage()
	{
		MainCharacter chara = PartyManager.Instance.GetCharacter(currSelectedCharId);

		Color col = new Color(0,0,0);
		if (chara.CharacterElements.Elements.Count > 0) {
			switch (chara.CharacterElements.Elements [0]) {
			case Element.Fire:
				col.r = 1f;
				break;
			case Element.Earth:
				col.g = 1f;
				break;
			case Element.Water:
				col.b = 1f;
				break;
			case Element.Dark:

				break;
			}
		}

		elementImage.color = col;
	}

	void CalculateTotalWeight()
	{
		int totalWeight=0;
		MainCharacter chara = PartyManager.Instance.GetCharacter(currSelectedCharId);
		
		foreach(ItemSlot itemSlot in chara.Items)
		{
			if(itemSlot.ItemMaster != null)
			{
				totalWeight += itemSlot.ItemMaster.Weight * itemSlot.Stack;
			}
		}
		totalWeightText.text = totalWeight.ToString();
	}

	void RotateModelsInput()
	{
		if (PauseUIController.Instance.PauseUIState != PauseUIController.PauseUIControllerState.MagicRename)
		{
			/*
			if(InputManager.GetAxis("RotateHorizontal")>0.1f)
			{
				Vector3 TargetRotation;// = transform.eulerAngles;
				foreach(MainCharacter chara in modelCharacters)
				{
					GameObject model = chara.gameObject;
					if(model.activeInHierarchy)
					{
						TargetRotation = model.transform.eulerAngles;
						TargetRotation.y -= InputManager.GetAxis("RotateHorizontal") * Time.deltaTime*rotateSpeed;
						model.transform.eulerAngles = TargetRotation;
					}
				}
				
			}
			else if(InputManager.GetAxis("RotateHorizontal")<-0.1f)
			{
				Vector3 TargetRotation;// = transform.eulerAngles;
				foreach(MainCharacter chara in modelCharacters)
				{
					GameObject model = chara.gameObject;
					if(model.activeInHierarchy)
					{
						TargetRotation = model.transform.eulerAngles;
						TargetRotation.y += -InputManager.GetAxis("RotateHorizontal") * Time.deltaTime*rotateSpeed;
						model.transform.eulerAngles = TargetRotation;
					}
				}
				
			}
			*/
			if(InputManager.GetAxisRaw("RotateHorizontal")>0.1f || InputManager.GetAxisRaw("RotateHorizontal")<-0.1f)
			{
				Vector3 TargetRotation;// = transform.eulerAngles;
				foreach(GameObject chara in modelCharacters)
				{
					GameObject model = chara;
					if(model.activeInHierarchy)
					{
						TargetRotation = model.transform.eulerAngles;
						TargetRotation.y -= InputManager.GetAxisRaw("RotateHorizontal") * Time.fixedDeltaTime * rotateSpeed;
						model.transform.eulerAngles = TargetRotation;
					}
				}
			}
		}
	}

	void ShiftTabPanel()
	{
		if (PauseUIController.Instance.PauseUIState != PauseUIController.PauseUIControllerState.MagicTweaker &&
		    PauseUIController.Instance.PauseUIState != PauseUIController.PauseUIControllerState.AssignCommand &&
		    PauseUIController.Instance.PauseUIState != PauseUIController.PauseUIControllerState.MagicRename)
		{
			if (InputManager.GetButtonDown("ChangeTabL"))
			{
				int tabIndex = (int)TabPanelController.instance.CurrentTab;
				tabIndex--;
				if(tabIndex<0)
				{
					tabIndex = TabPanelController.instance.PanelTypeCount - 1;
				}
				TabPanelController.instance.ChangeTabState(tabIndex);
			}
			else if (InputManager.GetButtonDown("ChangeTabR"))
			{
				int tabIndex = (int)TabPanelController.instance.CurrentTab;
				tabIndex++;
				if(tabIndex>TabPanelController.instance.PanelTypeCount-1)
				{
					tabIndex = 0;
				}
				TabPanelController.instance.ChangeTabState(tabIndex);
			}
		}
	}
}
