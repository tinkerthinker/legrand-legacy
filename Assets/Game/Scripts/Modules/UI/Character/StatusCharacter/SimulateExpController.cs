﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;

public class SimulateExpController : MonoBehaviour {
	private int _ExpRemaining;
	public int ExpRemaining
	{
		get{return _ExpRemaining;}
	}
	private int _UsedExp;
	public int UsedExp
	{
		get{return _UsedExp;}
	}
	[Header("Is used to show Remaining Exp while simulate")]
	public Text RemainingExpText;
	private Color _DefaultRemainingTextColor;

	public List<DistributeExp> DistributeExpObjs;

	public Dictionary<Attribute, int> StoredExp = new Dictionary<Attribute, int>();
	public Dictionary<Attribute, int> InitExp = new Dictionary<Attribute, int>();
	public Dictionary<Attribute, int> SimulatedLevel = new Dictionary<Attribute, int>();

	public ShowCharacterStats CharacterStatsHandler;

	public RadarGraphUI RadarGraph;

	List<Attribute> IterateAttribs = new List<Attribute>();

	private const int maxExp = 999999;

	private bool _ConfirmedSimulationCompleted = true;
	public bool ConfirmedSimulationCompleted
	{
		get{return _ConfirmedSimulationCompleted;}
	}

    private List<LockedMagic> _GainedMagic;
    private List<LockedSkill> _GainedSkill;

	void Init()
	{
		EventManager.Instance.AddListener<ChangeUICharSelectedID>(ChangeCharSelectedIdHandler);
		EventManager.Instance.AddListener<SimulateExpEvent>(SimulateExpEventHandler);
		EventManager.Instance.AddListener<DistributeExpEvent>(DistributeExpEventHandler);

		if (RemainingExpText)
			_DefaultRemainingTextColor = RemainingExpText.color;
		else
			_DefaultRemainingTextColor = Color.black;
	}

	void Awake()
	{
		//StartCoroutine (WaitGameReady());
		Init ();

		IterateAttribs.Add(Attribute.STR);
		IterateAttribs.Add(Attribute.VIT);
		IterateAttribs.Add(Attribute.AGI);
		IterateAttribs.Add(Attribute.INT);
		IterateAttribs.Add(Attribute.LUCK);

		foreach (Attribute att in IterateAttribs) {
			StoredExp.Add (att, 0);
		}

		foreach (Attribute att in IterateAttribs) {
			InitExp.Add (att, 0);
		}

		foreach (Attribute att in IterateAttribs) {
			SimulatedLevel.Add (att, 0);
		}

		Refresh ();
	}
	
//	IEnumerator WaitGameReady()
//	{
//		while (!PartyManager.Instance._ReadyToPlay)
//			yield return null;
//		Init ();
//	}

	void OnEnable () {
		if (StoredExp.Count == 0)
			return;
		Refresh ();
	}

	void OnDestroy()
	{
		if (EventManager.Instance) {
			EventManager.Instance.RemoveListener<ChangeUICharSelectedID> (ChangeCharSelectedIdHandler);
			EventManager.Instance.RemoveListener<SimulateExpEvent>(SimulateExpEventHandler);
			EventManager.Instance.RemoveListener<DistributeExpEvent>(DistributeExpEventHandler);
		}
	}

	void ChangeCharSelectedIdHandler(ChangeUICharSelectedID c)
	{
//		if (PauseUIController.Instance != null)
//			Refresh ();
		CancelSimulationExp();
	}

	void SimulateExpEventHandler(SimulateExpEvent e)
	{
		TrySimulateExp (e.Attrib, e.Value);
	}

	void DistributeExpEventHandler(DistributeExpEvent e)
	{
		DistributeSimulatedExp ();
	}

	void Refresh()
	{
		_UsedExp = 0;
		_ConfirmedSimulationCompleted = true;
		
		if (PartyManager.Instance.CharacterParty.Count==0)
			return;

		foreach (Attribute att in IterateAttribs) {
			StoredExp[att] = 0;
		}

		MainCharacter chara = PartyManager.Instance.GetCharacter (PauseUIController.Instance.selectedCharId);
		_ExpRemaining = chara.StoredExperience;

		UpdateRemainingExp ();

		foreach (Attribute att in IterateAttribs) {
			InitExp[att] = (int) chara.BattleExperiences[att];
		}

		foreach (Attribute att in IterateAttribs) {
			SimulatedLevel[att] = 0;//(int) chara.BattleAttributes[att];
		}
		float expNeeded;
		foreach (DistributeExp dst in DistributeExpObjs) {
			expNeeded = (int) (chara.BattleExperiencesNeeded[dst.DistributeAtt] );
			dst.Refresh ();
			//show current character exp progress
//			dst.AttribValueBar.fillAmount = ((float)chara.BattleExperiencesNeeded[dst.DistributeAtt] - (float)chara.BattleExperiences[dst.DistributeAtt]) / (float)chara.BattleExperiencesNeeded[dst.DistributeAtt];
//			dst.AttribValueBar.fillAmount = (float)InitExp[dst.DistributeAtt] / (float)expNeeded;
		}

		if(CharacterStatsHandler)
			CharacterStatsHandler.RefreshData ();
	}

	public void TrySimulateExp(Attribute attrib, int exp)
	{
		// cek if exp inserted is available
		if (_UsedExp + exp >= 0 && _UsedExp + exp <= _ExpRemaining) {
			SimulateExp(attrib, exp);
		}
	}
	
//	public void SimulateExp(Attribute attrib, int exp)
	public void SimulateExp(Attribute attrib, int incLevel)
	{
		MainCharacter chara = PartyManager.Instance.GetCharacter (PauseUIController.Instance.selectedCharId);

		// cek if still can insert exp
		//if (StoredExp [attrib] + exp >= (int)chara.BattleExperiences [attrib] && (int)chara.BattleAttributes [attrib] < 99) {

		/*
		// Kemungkinan BUG, ketika simulate player smpai level 99
		if (StoredExp [attrib] + exp >= 0 && ( (int)chara.BaseBattleAttributes [attrib] + SimulatedLevel[attrib] ) < 99) {
			StoredExp [attrib] += exp;
			_UsedExp += exp;
			RemainingExpText.text = (_ExpRemaining - _UsedExp).ToString();

			foreach(DistributeExp dst in DistributeExpObjs)
			{
				if(dst.DistributeAtt == attrib)
				{
					//simulate if level inrease, or can decrease if simulatedlevel >0
					if(SimulatedLevel[attrib] == 0)
					{
						if( (StoredExp[attrib] + InitExp[attrib]) - (int)chara.BattleExperiencesNeeded[attrib] >=0)
						{
							SimulatedLevel[attrib]++;
							dst.AttribValueText.text =( (int)chara.BaseBattleAttributes[attrib] + SimulatedLevel[attrib] ).ToString();

							ChangeAttributeRadar();
						}
					}
					else
					{
						int expLevel = (int)chara.BattleExperiencesNeeded[attrib];
						for(int i=1 ; i<SimulatedLevel[attrib]; i++)
						{
							expLevel += Formula.EXPforLevelUp((int)chara.BaseBattleAttributes[attrib]+i, MainCharacter.MaxLevel, (float)chara.BattleAttributeGrowths[attrib]);
						}

						//increase
						if((StoredExp[attrib] + InitExp[attrib]) - ( expLevel + Formula.EXPforLevelUp((int)chara.BaseBattleAttributes[attrib]+SimulatedLevel[attrib], MainCharacter.MaxLevel, (float)chara.BattleAttributeGrowths[attrib]) ) >=0)
						{
							SimulatedLevel[attrib]++;
							dst.AttribValueText.text = ( (int)chara.BaseBattleAttributes[attrib] + SimulatedLevel[attrib] ).ToString();

							ChangeAttributeRadar();
						}
						//decrease
						else if((StoredExp[attrib] + InitExp[attrib]) - expLevel <0)
						{
							SimulatedLevel[attrib]--;
							dst.AttribValueText.text = ( (int)chara.BaseBattleAttributes[attrib] + SimulatedLevel[attrib] ).ToString();

							ChangeAttributeRadar();
						}
					}

					//show the exp for level up
					if(SimulatedLevel[attrib] == 0)
					{
						int expForUp = Formula.EXPforLevelUp((int)chara.BaseBattleAttributes[attrib], MainCharacter.MaxLevel, (float)chara.BattleAttributeGrowths[attrib]);
						int expNeeded = expForUp - (InitExp[attrib] + StoredExp[attrib]);
						//dst.ExpForUpText.text = ((int)chara.BattleExperiencesNeeded[attrib] - StoredExp[attrib]).ToString();
						dst.ExpForUpText.text = expNeeded.ToString();
						dst.AttribValueBar.fillAmount = (expForUp - expNeeded) / (float)expForUp;
					}
					else
					{
						int expLevel = (int)chara.BattleExperiencesNeeded[attrib] - InitExp[attrib];
						int expForUp;
						int expNeeded;

						for(int i=1 ; i<SimulatedLevel[attrib]; i++)
						{
							expLevel += Formula.EXPforLevelUp((int)chara.BaseBattleAttributes[attrib]+i, MainCharacter.MaxLevel, (float)chara.BattleAttributeGrowths[attrib]);
						}

						expForUp = Formula.EXPforLevelUp((int)chara.BaseBattleAttributes[attrib]+SimulatedLevel[attrib], MainCharacter.MaxLevel, (float)chara.BattleAttributeGrowths[attrib]);
						expNeeded = expForUp - (StoredExp[attrib] - expLevel);

						dst.ExpForUpText.text = expNeeded.ToString();
						dst.AttribValueBar.fillAmount = (expForUp - expNeeded) / (float)expForUp;

					}
					break;
				}
			}
		}*/

//		if (StoredExp [attrib] + incLevel >= 0 && ( (int)chara.BaseBattleAttributes [attrib] + SimulatedLevel[attrib] ) < 99) {
		if (SimulatedLevel [attrib] + incLevel < 0 || (incLevel > 0 && ((int)chara.BaseBattleAttributes [attrib] + SimulatedLevel [attrib]) >= 99) )
			return;

		//			StoredExp [attrib] += exp;
		//			_UsedExp += exp;
		//			RemainingExpText.text = (_ExpRemaining - _UsedExp).ToString();

		DistributeExp dst = GetDistributeExpObject (attrib);
		if (dst == null)
			return;

		int expForLevelUp = Formula.EXPforLevelUp((int)chara.BaseBattleAttributes[attrib]+SimulatedLevel[attrib], MainCharacter.MaxLevel, (float)chara.BattleAttributeGrowths[attrib]);

		if(SimulatedLevel[attrib] == 0)
		{
			if( expForLevelUp <= _ExpRemaining - _UsedExp)
			{
				IncreaseAttributeLevel (chara, attrib, expForLevelUp);
			}

//			if( (StoredExp[attrib] + InitExp[attrib]) - (int)chara.BattleExperiencesNeeded[attrib] >=0)
//			{
//				SimulatedLevel[attrib]++;
//				dst.AttribValueText.text =( (int)chara.BaseBattleAttributes[attrib] + SimulatedLevel[attrib] ).ToString();
//
//				ChangeAttributeRadar();
//			}
		}
		//simulate if level inrease, or can decrease if simulatedlevel >0
		else
		{
			//increase
			if (incLevel > 0) {
				if (expForLevelUp <= _ExpRemaining - _UsedExp) {
					IncreaseAttributeLevel (chara, attrib, expForLevelUp);
				}
			} else if (incLevel < 0) {
				int expReturned = Formula.EXPforLevelUp((int)chara.BaseBattleAttributes[attrib]+SimulatedLevel[attrib]-1, MainCharacter.MaxLevel, (float)chara.BattleAttributeGrowths[attrib]);

				DecreaseAttributeLevel (chara, attrib, expReturned);
			}
		}
		if(CharacterStatsHandler)
			CharacterStatsHandler.SetSimulatedValue(SimulatedLevel);
	}

	void IncreaseAttributeLevel(MainCharacter chara, Attribute attrib, int expForLevelUp)
	{
		SimulatedLevel[attrib]++;
		StoredExp [attrib] += expForLevelUp;
		_UsedExp += expForLevelUp;

		_ConfirmedSimulationCompleted = false;

		DistributeExp dst = GetDistributeExpObject (attrib);
		expForLevelUp = Formula.EXPforLevelUp((int)chara.BaseBattleAttributes[attrib]+SimulatedLevel[attrib], MainCharacter.MaxLevel, (float)chara.BattleAttributeGrowths[attrib]);

//		dst.AttribValueText.text =( (int)chara.BaseBattleAttributes[attrib] + SimulatedLevel[attrib] ).ToString();
		dst.SetSimulatedValue(expForLevelUp,(int)chara.BaseBattleAttributes[attrib], (int)chara.BaseBattleAttributes[attrib] + SimulatedLevel[attrib] );

		UpdateRemainingExp ();

		ChangeAttributeRadar();
	}

	void DecreaseAttributeLevel(MainCharacter chara, Attribute attrib, int expForLevelDown)
	{
		SimulatedLevel[attrib]--;
		StoredExp [attrib] -= expForLevelDown;
		if (StoredExp[attrib] < 0)
			Debug.Log ("ERROR CALCULATION !!!! STORED EXP OF "+attrib.ToString()+" COULDN'T BE LESS THAN 0");
		_UsedExp -= expForLevelDown;
		if (_UsedExp < 0)
			Debug.Log ("ERROR CALCULATION !!!! USED EXP COULDN'T BE LESS THAN 0");
		if (_UsedExp == 0)
			_ConfirmedSimulationCompleted = true;
		DistributeExp dst = GetDistributeExpObject (attrib);

		dst.SetSimulatedValue(expForLevelDown,(int)chara.BaseBattleAttributes[attrib], (int)chara.BaseBattleAttributes[attrib] + SimulatedLevel[attrib] );

		UpdateRemainingExp ();
//		RemainingExpText.text = (_ExpRemaining - _UsedExp).ToString();

		ChangeAttributeRadar();
	}

	void UpdateRemainingExp()
	{
		if (RemainingExpText) {
			RemainingExpText.text = (_ExpRemaining - _UsedExp).ToString ();
			if (_UsedExp == 0)
				RemainingExpText.color = _DefaultRemainingTextColor;
			else
				RemainingExpText.color = Color.red;
		}
	}

	DistributeExp GetDistributeExpObject(Attribute attrib)
	{
		foreach (DistributeExp dst in DistributeExpObjs) {
			if (dst.DistributeAtt == attrib) {
				return dst;
			}
		}
		return null;
	}

	void ChangeAttributeRadar()
	{
		if (RadarGraph == null)
			return;
		MainCharacter chara = PartyManager.Instance.GetCharacter (PauseUIController.Instance.selectedCharId);

		RadarGraph.districtValue [0] = (int)chara.BaseBattleAttributes[Attribute.AGI] + SimulatedLevel[Attribute.AGI];
		RadarGraph.districtValue [1] = (int)chara.BaseBattleAttributes[Attribute.INT] + SimulatedLevel[Attribute.INT];
		RadarGraph.districtValue [2] = (int)chara.BaseBattleAttributes[Attribute.LUCK] + SimulatedLevel[Attribute.LUCK];
		RadarGraph.districtValue [3] = (int)chara.BaseBattleAttributes[Attribute.VIT] + SimulatedLevel[Attribute.VIT];
		RadarGraph.districtValue [4] = (int)chara.BaseBattleAttributes[Attribute.STR] + SimulatedLevel[Attribute.STR];
		
		int threshold = 20, step = 5, indexTH=1;
		
		foreach (int value in RadarGraph.districtValue)
		{
			for(int i = 1; i<=step;i++)
			{
				if(value <= i*threshold)
				{
					if(i>indexTH)
					{
						indexTH = i;
					}
					break;
				}
			}
			
		}
		RadarGraph.maxValue = indexTH * threshold;
		
		RadarGraph.UpdateGraph ();
	}

	public void DistributeSimulatedExp(bool isNoMeanRefresh = false)
	{
		if (!_ConfirmedSimulationCompleted) {
            if(isNoMeanRefresh)
                PopUpUI.CallChoicePopUp("Are you sure to distribute your ATP ?", ConfirmedDistributeSimulatedExp, Refresh);
            else
			    PopUpUI.CallChoicePopUp ("Are you sure to distribute your ATP ?", ConfirmedDistributeSimulatedExp);
		}
	}

	private void ConfirmedDistributeSimulatedExp()
	{
		MainCharacter chara = PartyManager.Instance.GetCharacter (PauseUIController.Instance.selectedCharId);

		foreach (KeyValuePair<Attribute, int> k in StoredExp) {
			chara.IncreaseBattleAttributeEXP(k.Key, k.Value);
			chara.StoredExperience -= StoredExp[k.Key];
		}

        _GainedMagic = chara.LearnMagic();
        _GainedSkill = chara.LearnNormalSkill();
		Refresh ();
        StartCoroutine(NotifGainedMagicSkill());
	}

    IEnumerator NotifGainedMagicSkill()
    {
        while(_GainedMagic.Count>0)
        {
            yield return null;
            PopUpUI.CallNotifPopUp("You Got " + _GainedMagic[0].Magic.Name);
            _GainedMagic.RemoveAt(0);
        }

        while (_GainedSkill.Count > 0)
        {
            yield return null;
            PopUpUI.CallNotifPopUp("You Got " + _GainedSkill[0].Skill.Name);
            _GainedSkill.RemoveAt(0);
        }
    }

	public void CancelSimulationExp()
	{
		if (!_ConfirmedSimulationCompleted) {
			PopUpUI.CallChoicePopUp ("Are you sure to cancel your distributed XP ?", Refresh);
		}
		else
			Refresh ();
	}

    public void CancelSimulationExpImmediately()
    {
        Refresh();
    }
}
