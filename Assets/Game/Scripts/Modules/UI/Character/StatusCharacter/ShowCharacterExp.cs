﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Legrand.core;

public class ShowCharacterExp : MonoBehaviour {
	private string currSelectedCharId;
    public Text CharLevelText;
    public Text CharNextlevelExpText;

	[Header("Is used to show Current Exp & ATP Character Have")]
	public Text CharRemainExpText;
    public Text CharRemainATPText;
    public GameObject AvailableATPObject;
    
	[Header("Determine if handle character change based on ChangeCharSelectedId Event")]
	public bool HandleChangeChar = true;

	[Header("Fill this if not Handle change character and manually set ID (Only Get Data at Start)")]
	public IDChar IdChar;

	void Init()
	{
		EventManager.Instance.AddListener<ChangeUICharSelectedID>(ChangeCharSelectedIdHandler);
	}

	void OnDestroy()
	{
		if (EventManager.Instance && HandleChangeChar) {
			EventManager.Instance.RemoveListener<ChangeUICharSelectedID> (ChangeCharSelectedIdHandler);
		}
	}

	void Start()
	{
		if (HandleChangeChar)
			Init ();
		else {
			if(IdChar)
				currSelectedCharId = IdChar._ID;
		}

        RefreshData();

        //if (HandleChangeChar)
        //    RefreshData ();
        //else {
        //    if(IdChar)
        //        RefreshData (currSelectedCharId);
        //}
	}

	void OnEnable()
	{
        if (PauseUIController.Instance != null && PartyManager.Instance != null)
        {
            if (HandleChangeChar)
                currSelectedCharId = PauseUIController.Instance.selectedCharId;
            RefreshData();
        }
	}

	void ChangeCharSelectedIdHandler(ChangeUICharSelectedID c)
	{
		if(PauseUIController.Instance != null)
			RefreshData (c.Id);
	}

	public void RefreshData(string selectedCharId)
	{
		currSelectedCharId = selectedCharId;

        ShowCharData();
	}

	public void RefreshData()
	{
        if (IdChar)
            currSelectedCharId = IdChar._ID;

		ShowCharData ();
	}

	void ShowCharData()
	{
        if (PartyManager.Instance.CharacterParty.Count <= 0)
            return;
        MainCharacter chara = PartyManager.Instance.GetCharacter(currSelectedCharId);

        if (CharLevelText)
        {
            CharLevelText.text = chara.Level.ToString();
        }
        if (CharNextlevelExpText)
        {
            CharNextlevelExpText.text = chara.ExpDifferences().ToString("#,#");
        }
		if (CharRemainExpText) {
            CharRemainExpText.text = chara.StoredExperience.ToString("#,#");
		}
        if (CharRemainATPText)
        {
            CharRemainATPText.text = chara.AttributePoint.ToString();
        }
        if(AvailableATPObject)
        {
            if (chara.AttributePoint > 0)
                AvailableATPObject.SetActive(true);
            else
                AvailableATPObject.SetActive(false);
        }
	}
}
