using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ShowCharacterHealth : MonoBehaviour {
	public Text HealthText;
	public Image HealthGauge;
    public Text APText;
    public Image APGauge;
	private string idChar;

	[Header("Determine if handle character change based on ChangeCharSelectedId Event")]
	public bool HandleChangeChar = false;

	void OnEnable()
	{
		if(HandleChangeChar)
			EventManager.Instance.AddListener<ChangeUICharSelectedID>(ChangeCharSelectedIdHandler);
		else if(!HandleChangeChar)
			idChar = GetComponent<IDChar> ()._ID;
        RefreshData();
	}

    void OnDisable()
	{
		if (EventManager.Instance && HandleChangeChar) {
			EventManager.Instance.RemoveListener<ChangeUICharSelectedID> (ChangeCharSelectedIdHandler);
		}
	}

	void Start()
	{
        //RefreshData ();
	}
    /*
	void OnEnable () {
        if (PauseUIController.Instance!=null)
        if (!PauseUIController.Instance.isFirstTimeLaunch)
            RefreshData();
	}*/

	void ChangeCharSelectedIdHandler(ChangeUICharSelectedID e)
	{
		idChar = e.Id;
		RefreshData ();
	}

	public void RefreshData()
	{		
		if (PartyManager.Instance.CharacterParty.Count == 0)
			return;
        if (!HandleChangeChar)
            idChar = GetComponent<IDChar>()._ID;
        Legrand.core.MainCharacter chara = PartyManager.Instance.GetCharacter (idChar);

        if (chara == null)
            return;

        Health charHealth = chara.GetComponent<Health> ();
		if (charHealth != null)
		{
			if(HealthText!=null)
				HealthText.text = (int)charHealth.Value + "/" + (int)charHealth.MaxValue;
			if(HealthGauge!=null)
				HealthGauge.fillAmount = charHealth.Value/charHealth.MaxValue;
		}

        if(APText)
            APText.text = chara.Gauge.ToString();
        if (APGauge)
            APGauge.fillAmount = (float)chara.Gauge / 100f;
	}
}
