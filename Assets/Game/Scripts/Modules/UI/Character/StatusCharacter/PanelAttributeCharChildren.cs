﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PanelAttributeCharChildren : MonoBehaviour {
	public Text AgiText;
	public Text StrText;
	public Text VitText;
	public Text IntText;
	public Text WisText;

	public Image AgiExpImg;
	public Image StrExpImg;
	public Image VitExpImg;
	public Image IntExpImg;
	public Image WisExpImg;

	public RadarGraphUI radarGraph;


}
