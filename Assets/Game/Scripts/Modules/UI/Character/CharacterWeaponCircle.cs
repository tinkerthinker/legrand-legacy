﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class CharacterWeaponCircle : MonoBehaviour {
	public List<Image> WeaponTypeImages;
	private List<Weapon.DamageType> SortedWeaponTypes = new List<Weapon.DamageType>();

	public Color StrongerColor = new Color(1,0,0,1);
	public Color WeakerColor = new Color(0,0,1,1);
	public Color NeutralColor = new Color(1,1,1,1);

	void Start () {
//		foreach (Element el in Enum.GetValues(typeof(Element)) ) {
//			
//		}
	}
	/*
	public void DefineWeaponCircle(Weapon weapon)
	{
		int index = 0;

		Weapon.DamageType strongerAgainst = weapon.StrongerTo;
		Weapon.DamageType weakerAgainst = weapon.WeakerTo;

		Image strongerImage = null;
		Image weakerImage = null;

		foreach (WeaponTypeObject wto in WeaponTypeObjects) {
			if (wto.WeaponType == strongerAgainst) {
//				wco.ImageObject.transform.SetSiblingIndex (0);
				strongerImage = wto.ImageObject;
				wto.ImageObject.color = StrongerColor;
			}
			else if (wto.WeaponType == weakerAgainst) {
//				wco.ImageObject.transform.SetSiblingIndex (1);
				weakerImage = wto.ImageObject;
				wto.ImageObject.color = WeakerColor;
			}
			else {
				wto.ImageObject.color = Color.white;
			}
		}

		if(strongerImage != null)
			strongerImage.transform.SetSiblingIndex (0);
		if (weakerImage != null)
			weakerImage.transform.SetSiblingIndex (1);

//		foreach (Weapon.DamageType dt in Enum.GetValues(typeof(Weapon.DamageType)) ) {
//
//		}
	}*/

	public void DefineWeaponCircle(Weapon weapon)
	{
		SortDefaultWeaponType();

		Weapon.DamageType strongerAgainst = weapon.StrongerTo;
		Weapon.DamageType weakerAgainst = weapon.WeakerTo;

		int index = SortedWeaponTypes.FindIndex ((Weapon.DamageType obj) => strongerAgainst == obj);

		SortedWeaponTypes.RemoveAt (index);
		SortedWeaponTypes.Insert (0, strongerAgainst);
		WeaponTypeImages [0].color = StrongerColor;

		index = SortedWeaponTypes.FindIndex ((Weapon.DamageType obj) => weakerAgainst == obj);
		SortedWeaponTypes.RemoveAt (index);
		SortedWeaponTypes.Insert (1, weakerAgainst);
		WeaponTypeImages [1].color = WeakerColor;

		int imgIndex = 0;
		foreach (Image img in WeaponTypeImages) {
			img.GetComponentsInChildren<Image>()[1].sprite = LegrandBackend.Instance.GetSpriteData ("Weapon" + SortedWeaponTypes [imgIndex]);

			imgIndex++;
		}
	}

	void SortDefaultWeaponType()
	{
		SortedWeaponTypes.Clear ();
		foreach (Weapon.DamageType el in Enum.GetValues(typeof(Weapon.DamageType))) {
			SortedWeaponTypes.Add (el);
		}
	}
}
