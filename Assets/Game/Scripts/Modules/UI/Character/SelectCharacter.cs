using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using Legrand.core;

public class SelectCharacter : MonoBehaviour, ISelectHandler, IDeselectHandler, ICancelHandler {
    private PauseUIController.PauseUIControllerState TargetState = PauseUIController.PauseUIControllerState.Status;

    private IDChar _IDCharObject;
	private string _IdChar;
    public string IdChar
    {
        get {
            if (_IdChar == null)
                SetCharacter();
            return _IdChar; 
        }
    }

	void Awake () {
		SetCharacter ();

		GetComponent<Button> ().onClick.AddListener (() => OnClickHandler() );
	}

	void OnEnable()
	{
		DeselectCharacter ();
	}

	public void SetCharacter () {
		_IDCharObject = GetComponent<IDChar>();
        _IdChar = _IDCharObject._ID;
	}
	
	private void OnClickHandler()
	{
		PauseUIController.Instance.ChangeState (TargetState);
	}

	public void DeselectCharacter()
	{

	}

    public void TargetAsCommand()
    {
        TargetState = PauseUIController.PauseUIControllerState.Command;
    }

    public void TargetAsStatusChar()
    {
        TargetState = PauseUIController.PauseUIControllerState.Status;
    }
	
	public void OnSelect(BaseEventData eventData)
	{
		PauseUIController.Instance.selectedCharId = _IdChar = _IDCharObject._ID;
	}

	#region IDeselectHandler implementation

	public void OnDeselect (BaseEventData eventData)
	{
		DeselectCharacter ();
	}

	#endregion
	
	public void OnCancel(BaseEventData eventData)
	{
        if (TargetState == PauseUIController.PauseUIControllerState.Command)
            PauseUIController.Instance.AssignButton.Select();
        else if (TargetState == PauseUIController.PauseUIControllerState.Status)
            PauseUIController.Instance.CharacterButton.Select();
	}
}
