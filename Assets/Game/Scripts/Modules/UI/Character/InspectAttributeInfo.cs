﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;

public class InspectAttributeInfo : MonoBehaviour {
    [System.Serializable]
    public class AttributeInfo
    {
        public Attribute AttribType;
        public string Description;
    }

    public GameObject Root;
    public List<AttributeInfo> AttributesInfo;
    public Text InfoText;

    void OnEnable()
    {
        EventManager.Instance.AddListener<InspectAttributeEvent>(InspectAttributeEventHandler);
    }

    void OnDisable()
    {
        if(EventManager.Instance)
            EventManager.Instance.RemoveListener<InspectAttributeEvent>(InspectAttributeEventHandler);
    }

    void InspectAttributeEventHandler(InspectAttributeEvent e)
    {
        SetInfoText(AttributesInfo.Find(x => x.AttribType == e.Attrib).Description);
    }

    public void SetInfoText(string info)
    {
        if (Root)
            Root.SetActive(true);
        if (InfoText)
        {
            InfoText.text = LegrandBackend.Instance.GetLanguageData(info, true);
        }
    }
}
