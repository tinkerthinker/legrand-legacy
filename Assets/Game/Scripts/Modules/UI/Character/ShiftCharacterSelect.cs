using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;
using TeamUtility.IO;

public class ShiftCharacterSelect : MonoBehaviour {
	int currCharPartyIndex;
	bool canChangeChar=false;

    public GameObject Template;
	public Image[] TabCharacterImages;
	private Dictionary<string, GameObject> _DictCharTabObject = new Dictionary<string, GameObject>();
	public Text CharNameText;
    //public Color SelectedColor = new Color(1,1,1,1);
    //public Color DeselectedColor;
    public float SelectedAlpha = 1;
    public float DeselectedAlpha = 0;

    public float SelectedScale = 1f;
    public float DeselectedScale = 0.8f;

	public SimulateAttribUpController SimulatedController;
    public bool IsInMenu = true;

	private bool _IsHaltedProcess = false;
    private bool _IsInteractable = true;

    private List<string> _Characters = new List<string>();

	void Awake () {
		foreach (Image img in TabCharacterImages) {
			if (img.GetComponent<IDChar> () == null) {
				img.gameObject.SetActive (false);
				continue;
			}

			string id = img.GetComponent<IDChar> ()._ID;

			_DictCharTabObject.Add (id, img.gameObject);
		}
	}

	void OnEnable()
	{
		currCharPartyIndex = 0;

        SetInteractable(true);

        if (PartyManager.Instance == null || PauseUIController.Instance == null)
        {
            if(IsInMenu)
                return;
        }

		SetImagesTab ();
	}

	void Update () {
		if (_IsHaltedProcess || !_IsInteractable)
			return;

        if (InputManager.GetButtonDown("ChangeTabR"))
        {
            canChangeChar = false;
            ShiftCharacter(currCharPartyIndex + 1);
        }
        else if (InputManager.GetButtonDown("ChangeTabL"))
        {
            canChangeChar = false;
            ShiftCharacter(currCharPartyIndex - 1);
        }	
	}
	
	void ShiftCharacter(int shiftCharId)
	{
        if (_Characters.Count == 0)
            return;
		//shiftCharId, menggeser data Character sesuai dengan object button yang ada. bukan berdasar id yang ada. object button sudah di generate berdasar id character yang tersedia
		if (shiftCharId > _Characters.Count - 1)
			shiftCharId = 0;
		else if (shiftCharId < 0)
			shiftCharId = _Characters.Count-1;

		int prevIndex = currCharPartyIndex;

//		GameObject currButton = EventSystem.current.currentSelectedGameObject;

		StartCoroutine(WaitToChangeImageTab (prevIndex, shiftCharId));
	}

	IEnumerator WaitToChangeImageTab(int prevIndex, int shiftCharId)
	{
		_IsHaltedProcess = true;
		if (SimulatedController) {
			SimulatedController.DistributeSimulatedExp(true);
			while (!SimulatedController.ConfirmedSimulationCompleted)
				yield return null;
		}

        if (IsInMenu)
            PauseUIController.Instance.selectedCharId = _Characters[shiftCharId];
        else
            EventManager.Instance.TriggerEvent(new ChangeUICharSelectedID(_Characters[shiftCharId]));

		currCharPartyIndex = shiftCharId;

		ChangeImagesTab (prevIndex, currCharPartyIndex);
        if (prevIndex != currCharPartyIndex)
            WwiseManager.Instance.PlaySFX(WwiseManager.UIFX.Scroll);
		_IsHaltedProcess = false;
	}

    void SetImagesTab()
    {
        string currentId;

        _Characters.Clear();

        if (IsInMenu)
        {
            foreach (string chara in PauseUIController.Instance.ActiveMembers)
            {
                _Characters.Add(chara);
            }
            foreach (string chara in PauseUIController.Instance.ReserveMembers)
            {
                _Characters.Add(chara);
            }

            currentId = PauseUIController.Instance.selectedCharId;
        }
        else
        {
            foreach(MainCharacter chara in PartyManager.Instance.CharacterParty)
            {
                _Characters.Add(chara._ID);
            }
            if (_Characters.Count > 0)
                currentId = _Characters[0];
            else
                currentId = "0";
        }

        foreach(string id in _Characters)
        {
            if (!_DictCharTabObject.ContainsKey(id))
                MakeNewImageTab(id);
        }

        int index = 0;
        foreach (KeyValuePair<string, GameObject> k in _DictCharTabObject)
        {
            index = _Characters.IndexOf(k.Key);
            if (_Characters.Contains(k.Key))
            {
                k.Value.gameObject.SetActive(true);

                if (k.Key.Equals(currentId))
                {
                    SelectImageTab(index);
                    currCharPartyIndex = index;
                }
                else
                    DeselectImageTab(index);                
                //k.Value.transform.SetSiblingIndex(index);
            }
            else
                k.Value.gameObject.SetActive(false);
        }

        for(int i =0; i<_Characters.Count; i++)
        {
            _DictCharTabObject[_Characters[i]].transform.SetSiblingIndex(i);
        }
    }

    void MakeNewImageTab(string id)
    {
        GameObject newObjTab = Instantiate(Template);
        newObjTab.SetActive(true);
        newObjTab.transform.SetParent(gameObject.transform, false);

        newObjTab.GetComponent<IDChar>().SetID(id);
        newObjTab.GetComponent<CharacterPreview>().RefreshData();
        _DictCharTabObject.Add(id, newObjTab);
    }

	void ChangeImagesTab(int prevIndex, int currIndex)
	{
        //_DictCharTabImage [PartyManager.Instance.CharacterParty [prevIndex]._ID].color = DeselectedColor;
        //_DictCharTabImage [PartyManager.Instance.CharacterParty [currIndex]._ID].color = SelectedColor;
        DeselectImageTab(prevIndex);
        SelectImageTab(currIndex);
	}

    void SelectImageTab(int index)
    {
        if (_DictCharTabObject[_Characters[index]].GetComponent<CanvasGroup>())
            _DictCharTabObject[_Characters[index]].GetComponent<CanvasGroup>().alpha = SelectedAlpha;
        _DictCharTabObject[_Characters[index]].transform.localScale = new Vector3(SelectedScale, SelectedScale, 1f);
        BlendModes.BlendModeEffect bm = _DictCharTabObject[_Characters[index]].GetComponentInChildren<BlendModes.BlendModeEffect>(true);
        if (bm != null)
            bm.gameObject.SetActive(false);
    }

    void DeselectImageTab(int index)
    {
        if (_DictCharTabObject[_Characters[index]].GetComponent<CanvasGroup>())
            _DictCharTabObject[_Characters[index]].GetComponent<CanvasGroup>().alpha = DeselectedAlpha;
        _DictCharTabObject[_Characters[index]].transform.localScale = new Vector3(DeselectedScale, DeselectedScale, 1f);
        BlendModes.BlendModeEffect bm = _DictCharTabObject[_Characters[index]].GetComponentInChildren<BlendModes.BlendModeEffect>(true);
        if (bm != null)
            bm.gameObject.SetActive(true);
    }

    public void SetInteractable(bool isInteractable)
    {
        _IsInteractable = isInteractable;
    }
}
