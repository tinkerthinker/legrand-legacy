﻿using UnityEngine;
using System.Collections;
using Legrand.core;

public interface ICombineBlueprintShopController
{
    void CombineAndCreate(BluePrint SelectedBlueprint,NavigationButtonData navigation);
}
