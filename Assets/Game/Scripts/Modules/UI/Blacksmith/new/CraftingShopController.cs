﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;
using UnityEngine.UI;
using TeamUtility.IO;
using UnityEngine.EventSystems;
using System;

public class CraftingShopController : MonoBehaviour
{
    public ShowInventoryMiscData InventMiscObject;
    public ShiftTabController ShiftCharacter;
    public GenerateEnhanceRecipe GenerateEnchance;
    public ExecuteCraftingBlueprintController ExecuteBlueprint;
    public Scrollbar ScrollbarTarget;

    public int Level;
    public Text LevelText;

    public Color[] LevelColor;
    BluePrint _SelectedBlueprint = new BluePrint();
    NavigationButtonData _NavigationButtonData;

    public Animator CraftingAnimator;
    private bool _InStateTransition = false;
    
    void OnEnable()
    {
        if (LevelText)
        {
            LevelText.text = LegrandBackend.Instance.GetLanguageData("MercLevel" + Level, true);
            if (Level > LevelColor.Length)
                LevelText.color = LevelColor[LevelColor.Length - 1];
            else if (Level < 0)
                LevelText.color = LevelColor[0];
            else
                LevelText.color = LevelColor[Level - 1];
        }
        EventManager.Instance.AddListener<ChangeUICharSelectedID>(ChangeUICharSelectedIDHandler);
        StateTransitionStarted();
        SetSelectedCharacter(PartyManager.Instance.CharacterParty[0]._ID);   
    }

    void OnDisable()
    {
        if (EventManager.Instance)
            EventManager.Instance.RemoveListener<ChangeUICharSelectedID>(ChangeUICharSelectedIDHandler);
    }

    public void Close()
    {
        StartCoroutine(WaitCloseMenuTransition());        
    }

    void SetSelectedCharacter(int index)
    {
        SetSelectedCharacter(PartyManager.Instance.CharacterParty[index]._ID);
    }

    void SetSelectedCharacter(string id)
    {
        if (!gameObject.activeInHierarchy)
            return;
        GenerateEnchance.MerchantLevel = Level;
        GenerateEnchance.UpdateList(id);
        StartCoroutine(WaitTransitionToFocus());
    }

    IEnumerator WaitTransitionToFocus()
    {
        while(_InStateTransition)
        {
            yield return null;
        }
        GenerateEnchance.TryToFocusFirstButton();
    }

    void ChangeUICharSelectedIDHandler(ChangeUICharSelectedID e)
    {
        SetSelectedCharacter(e.Id);
    }
    
    void GenerateCharacterIcon()
    {
        List<Sprite> charImage = new List<Sprite>();
        for(int i = 0;i<PartyManager.Instance.CharacterParty.Count;i++)
        {
            charImage.Add(LegrandBackend.Instance.GetSpriteData("Avatar" + PartyManager.Instance.CharacterParty[i].name));
        }
        ShiftCharacter.SetImageArray(charImage.ToArray());
        ShiftCharacter.RegisterShiftingFunction(SetSelectedCharacter, PartyManager.Instance.CharacterParty.Count - 1);
    }

    public void StateTransitionStarted()
    {
        _InStateTransition = true;
    }

    public void StateTransitionCompleted()
    {
        _InStateTransition = false;
    }

    IEnumerator WaitCloseMenuTransition()
    {
        EventSystem.current.SetSelectedGameObject(gameObject);
        CraftingAnimator.SetTrigger("CloseMenu");
        StateTransitionStarted();
        while (_InStateTransition)
        {
            yield return null;
        }
        EventManager.Instance.TriggerEvent(new CraftingShopEvent(false));
    }
}
