﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Legrand.core;

public class EnhanceRecipeData : MonoBehaviour, IObjectData
{
    public bool SetAsNullData = false;
    public Image IconImage;
    public GameObject ImpossibleIcon;
    public Text NameText;
    public Text PriceText;
    public Text OwnText;
    public GameObject CurrentEquipSymbol;

    BluePrint _BluePrint;
    EquipmentDataModel _Equipment;

    public int CreateAmountPreview = 1;

    public ExecuteCraftingBlueprintController ExecuteBlueprint;
    
    NavigationButtonData _Navigation;

    private bool _IsPossibleToCraft;
    private bool _IsCurrentEquip;

    void Awake()
    {
        _Navigation = GetComponent<NavigationButtonData>();
    }

    public void SetData(object obj)
    {
        if (SetAsNullData)
            return;
        if (obj is BluePrint)
        {
            _BluePrint = (BluePrint)obj;
            _Equipment = LegrandBackend.Instance.GetEquipment(_BluePrint.ResultItemID);
            if (_BluePrint.BluePrintType == BluePrint.Type.Crafting)
            {
                if (!string.IsNullOrEmpty(_BluePrint.CharacterID))
                {
                    Equipments equip = PartyManager.Instance.GetCharacter(_BluePrint.CharacterID).Equipment();
                    if (equip.ID.Equals(_BluePrint.ResultItemID))
                        _IsCurrentEquip = true;
                    else
                        _IsCurrentEquip = false;
                }
            }
        }
        else if(obj is Equipments)
        {
            _BluePrint = null;
            _Equipment = LegrandBackend.Instance.GetEquipment(((Equipments)obj).ID);
        }

        if(IconImage)
            IconImage.sprite = _Equipment.Picture;
        if(NameText)
            NameText.text = _Equipment.Name;
        if (PriceText)
        {
            if (!_IsCurrentEquip)
                PriceText.text = _BluePrint != null ? _BluePrint.Price.ToString("#,#") : "";
            else
                PriceText.text = "-";
        }
        if (OwnText)
            OwnText.text = PartyManager.Instance.Inventory.GetStackItem(LegrandBackend.Instance.ItemData[_BluePrint.ID]).ToString();
        if(CurrentEquipSymbol)
        {
            if (_IsCurrentEquip)
                CurrentEquipSymbol.SetActive(true);
            else
                CurrentEquipSymbol.SetActive(false);
        }
    }

    public object GetData()
    {
        return _Equipment;
    }

    public void RefreshData()
    {
        SetData(_Equipment);
    }

    public void OnSelect(UnityEngine.EventSystems.BaseEventData eventData)
    {
        EventManager.Instance.TriggerEvent(new InspectEnhanceRecipeEvent(_Equipment, _BluePrint, CreateAmountPreview));
    }

    public void CreateBluePrint()
    {
        if (_IsCurrentEquip)
            return;

        if(_IsPossibleToCraft)
            ExecuteBlueprint.Execute(_BluePrint, _Navigation);
        else
            PopUpUI.CallNotifPopUp("This Merchant can't craft this item");
    }

    public void SetPossibleToCraft(bool isPossible)
    {
        _IsPossibleToCraft = isPossible;
        if (ImpossibleIcon)
        {
            if (isPossible)
                ImpossibleIcon.gameObject.SetActive(false);
            else
                ImpossibleIcon.gameObject.SetActive(true);
        }
    }
}
