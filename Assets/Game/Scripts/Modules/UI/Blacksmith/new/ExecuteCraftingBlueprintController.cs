﻿using UnityEngine;
using System.Collections;
using Legrand.core;

public class ExecuteCraftingBlueprintController : MonoBehaviour
{
    BluePrint _SelectedBlueprint;
    NavigationButtonData _Navigation;
    public ShowInventoryMiscData InventoryMiscData;

    public void Execute(BluePrint bp, NavigationButtonData navi)
    {
        _SelectedBlueprint = bp;

        if(bp.TryCombine())
        {
            if (bp.Price < PartyManager.Instance.Inventory.Gold)
            {
                _Navigation = navi;
                _Navigation.ChangeToNone();
                PopUpUI.CallChoicePopUp("Are You Sure?", CreateThis, CancelCreate);
            }
            else
                PopUpUI.CallNotifPopUp("Insufficient Danaar!");
        }
        else
        {
            PopUpUI.CallNotifPopUp("Insufficient Item Material!");
        }
        
    }

    void CreateThis()
    {
        Execute();
        InventoryMiscData.UpdateAll();
        _Navigation.ChangeToLastNavigation();
    }

    void CancelCreate()
    {
        _Navigation.ChangeToLastNavigation();
    }

    void Execute()
    {
        if (_SelectedBlueprint.TryCombine())
        {
            if (_SelectedBlueprint.Price <= PartyManager.Instance.Inventory.Gold)
            {
                for (int i = 0; i < _SelectedBlueprint.RequiredItems.Count; i++)
                {
                    PartyManager.Instance.Inventory.DeleteItemsByItemID(_SelectedBlueprint.RequiredItems[i].AllItemID, _SelectedBlueprint.RequiredItems[i].Amount);
                }
                PartyManager.Instance.Inventory.Gold = PartyManager.Instance.Inventory.Gold - (_SelectedBlueprint.Price);

                PartyManager.Instance.GetCharacter(_SelectedBlueprint.CharacterID).Equipment().ChangeEquipment(_SelectedBlueprint.ResultItemID);
            }
            else
            {
                PopUpUI.CallNotifPopUp("Insufficient Danaar");
            }
        }
    }
}
