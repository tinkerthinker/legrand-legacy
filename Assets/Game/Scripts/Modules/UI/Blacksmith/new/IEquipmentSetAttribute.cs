﻿using UnityEngine;
using System.Collections;
using Legrand.core;

public interface IEquipmentSetAttribute
{
    void SetAttribute(BluePrint SelectedBlueprint);
}
