﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Legrand.core;

public class ChangeGenBlueprint : MonoBehaviour {
    public GenerateEnhanceRecipe Target;
    public BluePrint.Type Type;

    private bool _IsCanFocusFirst = true;

    public void UpdateGenerateBlueprint()
    {
        Target.Type = Type;
        Target.UpdateList();
    }

    public void FocusFirstButton()
    {
        if (!_IsCanFocusFirst)
            return;
        if (UnityEngine.EventSystems.EventSystem.current)
            UnityEngine.EventSystems.EventSystem.current.SetSelectedGameObject(this.gameObject);

        StartCoroutine(WaitFirstSelect());
    }

    IEnumerator WaitFirstSelect()
    {
        yield return null;
        if (!Target.FocusToFirstButton())
            if (Target.ScrollerController)
                Target.ScrollerController.ScrollbarTarget.Select();
    }

    public void SetCanFocusFirst(bool isCan)
    {
        _IsCanFocusFirst = isCan;
    }
}
