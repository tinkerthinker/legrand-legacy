﻿using UnityEngine;
using System.Collections;
using Legrand.core;

public class GenerateEnhanceRecipe : AbsObjectListGenerator {
    public BluePrint.Type Type;
    [System.NonSerialized]
    public int MerchantLevel=1;

    EquipmentDataModel _Equip;
    string _SelectedCharId="0";
    BluePrint _SelectedBlueprint;

    Equipments _CurrEquip;

    void OnEnable()
    {
        //UpdateList();
        //StartCoroutine(WaitToFocusFirstButton());
    }

    public IEnumerator WaitToFocusFirstButton()
    {
        yield return null;
        TryToFocusFirstButton();
    }

    public void TryToFocusFirstButton()
    {
        if (!_IsCanChangeFocus)
            return;
        if(!FocusToFirstButton())
        {
            if (ScrollerController)
                ScrollerController.ScrollbarTarget.Select();
        }
    }

    public void UpdateList(string charId)
    {
        _SelectedCharId = charId;
        base.UpdateList();
    }

    public override void UpdateList()
    {
        _SelectedCharId = "";
        base.UpdateList();
    }

    public override void StartLoopUpdate()
    {
        if (Type == BluePrint.Type.Crafting)
        {
            _CurrEquip = PartyManager.Instance.GetCharacter(_SelectedCharId).Equipment();
            if (_CurrEquip != null)
            {
                _SelectedBlueprint = LegrandBackend.Instance.GetBlueprintByResultItem(_CurrEquip.ID);
                UpdateEachButton(_SelectedBlueprint);
            }
        }
        foreach(BluePrint bp in PartyManager.Instance.UnlockedBlueprints)
        {
            if (Type == BluePrint.Type.Crafting && bp.ResultItemID.Equals(_CurrEquip.ID))
                continue;
            _SelectedBlueprint = bp;
            UpdateEachButton(bp);
        }
    }

    public override bool CheckConditionPreUpdate(object data)
    {
        if(_SelectedBlueprint == null || (Type == BluePrint.Type.Crafting && _CurrEquip == null) )
        {
            return false;
        }

        if (_SelectedBlueprint.BluePrintType == Type && (string.IsNullOrEmpty(_SelectedCharId) || _SelectedBlueprint.CharacterID.Equals(_SelectedCharId)))
        {
            return true;
        }
        return false;
    }

    public override void SetButtonName(UnityEngine.UI.Button button, object data)
    {
        if(data is BluePrint)
            button.name = ((BluePrint)data).ID;
        else if(data is Equipments)
            button.name = ((Equipments)data).ID;        
    }

    public override void AfterDataUpdatedProcess(IObjectData objectData, object data)
    {
        if (Type == BluePrint.Type.Crafting && _SelectedBlueprint.ResultItemID.Equals(_CurrEquip.ID))
        {
            if (objectData is BlueprintData)
            {
                ((BlueprintData)objectData).SetEnoughMaterial(true);
            }
        }

        if (_SelectedBlueprint.Level <= MerchantLevel)
        {
            if (objectData is EnhanceRecipeData)
            {
                ((EnhanceRecipeData)objectData).SetPossibleToCraft(true);
            }
            else if (objectData is BlueprintAlchemistData)
            {
                ((BlueprintAlchemistData)objectData).SetPossibleToCraft(true);
            }
            else if (objectData is BlueprintData)
            {
                ((BlueprintData)objectData).SetPossibleToCraft(true);
            }
        }
        else
        {
            if (objectData is EnhanceRecipeData)
            {
                ((EnhanceRecipeData)objectData).SetPossibleToCraft(false);
            }
            else if (objectData is BlueprintAlchemistData)
            {
                ((BlueprintAlchemistData)objectData).SetPossibleToCraft(false);
            }
            else if (objectData is BlueprintData)
            {
                ((BlueprintData)objectData).SetPossibleToCraft(false);
            }
        }
    }
}
