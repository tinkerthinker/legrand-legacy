﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class CraftingController : MonoBehaviour {
	public enum CraftEquipment
	{
		Weapon,
		Armor
	}
	public CraftEquipment CraftingEquipment;

	public GenerateInventoryItem GenerateInventObj;
	public ShowEquipmentCharacter ShowEquipmentStatsObj;

	private Item _SelectedCraftMaterial;
	public Item SelectedCraftMaterial
	{
		get{ return _SelectedCraftMaterial;}
		set{ 
			_SelectedCraftMaterial = value;

			switch(CraftingEquipment)
			{
			case CraftEquipment.Weapon:
                //foreach(GameObject obj in ShowEquipmentStatsObj.WeaponSockets)
                //{
                //    if(obj.GetComponent<ShowCraftingChange>() != null )
                //        obj.GetComponent<ShowCraftingChange>().SetChangingComponent();
                //}
				break;
			case CraftEquipment.Armor:
                //foreach(GameObject obj in ShowEquipmentStatsObj.ArmorSockets)
                //{
                //    if(obj.GetComponent<ShowCraftingChange>() != null )
                //        obj.GetComponent<ShowCraftingChange>().SetChangingComponent();
                //}
				break;
			}

		}
	}

	private static CraftingController s_Instance;
	public static CraftingController Instance
	{
		get
		{
			if (s_Instance == null)
			{
				s_Instance = GameObject.FindObjectOfType(typeof(CraftingController)) as CraftingController;
			}
			return s_Instance;
		}
	}

	void OnEnable () {
		UpdateCraftItem ();

		DisableSocketButtons ();

	}

	public void UpdateCraftItem () {
		if (CraftingEquipment == CraftEquipment.Weapon) {
			GenerateInventObj.UsabilityTypeGenerated = Item.UsabilityType.WeaponCraftable;
            //ShowEquipmentStatsObj.ValueType = ShowEquipmentCharacter.Type.Weapon;
		} else if (CraftingEquipment == CraftEquipment.Armor) {
			GenerateInventObj.UsabilityTypeGenerated = Item.UsabilityType.ArmorCraftable;
            //ShowEquipmentStatsObj.ValueType = ShowEquipmentCharacter.Type.Armor;
		}
        //ShowEquipmentStatsObj.CurrSelectedCharId = BlacksmithController.Instance.SelectedCharId;
		GenerateInventObj.UpdateInventory ();	
		GenerateInventObj.FocusToFirstButton ();
	}

	public void EnableSocketButtons()
	{
		switch(CraftingEquipment)
		{
		case CraftEquipment.Weapon:
            //ChangeInteractableObject.EnableAllButtonInParent (ShowEquipmentStatsObj.WeaponSockets [0].transform.parent.gameObject);
			break;
		case CraftEquipment.Armor:
            //ChangeInteractableObject.EnableAllButtonInParent (ShowEquipmentStatsObj.ArmorSockets [0].transform.parent.gameObject);
			break;
		}
	}

	public void DisableSocketButtons()
	{
		switch(CraftingEquipment)
		{
		case CraftEquipment.Weapon:
            //ChangeInteractableObject.DisableAllButtonInParent (ShowEquipmentStatsObj.WeaponSockets [0].transform.parent.gameObject);
			break;
		case CraftEquipment.Armor:
            //ChangeInteractableObject.DisableAllButtonInParent (ShowEquipmentStatsObj.ArmorSockets [0].transform.parent.gameObject);
			break;
		}
	}

	public void FocusToFirstSocketButtons()
	{
		switch(CraftingEquipment)
		{
		case CraftEquipment.Weapon:
            //if(ShowEquipmentStatsObj.WeaponSockets [0].GetComponent<Button>() != null)
            //    ShowEquipmentStatsObj.WeaponSockets [0].GetComponent<Button>().Select();
			break;
		case CraftEquipment.Armor:
            //if(ShowEquipmentStatsObj.ArmorSockets [0].GetComponent<Button>() != null)
            //    ShowEquipmentStatsObj.ArmorSockets [0].GetComponent<Button>().Select();
			break;
		}
	}
}
