﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;

public class ShowCraftingChange : MonoBehaviour, ISelectHandler, IDeselectHandler {
	//used to get the component text, serialized to make for flexible if only need to assign manually from editor.
	[SerializeField][Header("Only Fill it if want to manually assign from editor")]
	private ShowEquipmentCharacter _ShowEquipCharObj;
	public ShowEquipmentCharacter ShowEquipCharObj
	{
		set
		{
			_ShowEquipCharObj = value;
			SetChangingComponent();
		}
		get
		{
			return _ShowEquipCharObj;
		}

	}

	//private Dictionary<Attribute, Text> AttributeObjectText = new Dictionary<Attribute, Text>();
	#region Weapon
	private Text AttackStatusInflict;
	private Text DamageValue;
	private Text HitRateValue;
	private Text CriticalValue;
	#endregion
	
	#region Armor
	private Text DefenseStatusResist;
	private Text DefenseValue;
	private Text EvasionValue;
	private Text CriticalResistValue;
	#endregion

	private Item _ItemData;

	//[Header("Is This Object an upgrade slot in equipment")]
	//public bool IsUpgradeSlot = false;
	private bool IsUpgradeSlot = false;
	[Header("If This Object is an upgrade slot, fill it as its index in slot, otherwise it means nothing")]
	public int IndexSocket;

	void Start () {
	}

	public void SetChangingComponent()
	{
	}


	void ChangeValueText(Attribute att, float changedValue)
	{
	}

	void ShowChange () {
		
	}

	void HideChange()
	{
		
	}

	#region ISelectHandler implementation

	public void OnSelect (BaseEventData eventData)
	{
		//if (_ItemData == null)
		//	Start ();
		ShowChange ();
	}

	#endregion

	#region IDeselectHandler implementation

	public void OnDeselect (BaseEventData eventData)
	{
		HideChange ();
	}

	#endregion
}
