﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(ItemInventoryData))]
public class SetCraftingMaterial : MonoBehaviour {

	public void SetSelectedCraftingMaterial()
	{
		CraftingController.Instance.SelectedCraftMaterial = GetComponent<ItemInventoryData> ().ItemSlotData.ItemMaster;
		CraftingController.Instance.EnableSocketButtons ();
		CraftingController.Instance.FocusToFirstSocketButtons ();

		ChangeInteractableObject.DisableAllButtonInParent (transform.parent.gameObject);
	}
}
