﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public abstract class AbsObjectListGenerator : MonoBehaviour {
    public ScrollController ScrollerController;
    public BaseEnum.Orientation Navigation = BaseEnum.Orientation.Vertical;
    [Header("Only used if ScrollerController isn't assigned")]
    public int SeenRow = 1;
    public int SeenCol = 1;
    [Header("It can be an instance object or prefab")]
    public Button TemplateButton;
    private GameObject _Root;
    public GameObject Root;

    protected List<Button> _ListButtons = new List<Button>();
    protected List<Button> _PoolingButtons = new List<Button>();
    private List<IObjectData> _ListDataHandler = new List<IObjectData>();

    public bool HandleScrollNextPage = true;
    protected bool _CanScroll = true;
    protected bool _IsScrollDisabled = false;

    protected int indexButton;

    public abstract void StartLoopUpdate();
    public abstract bool CheckConditionPreUpdate(object data);
    public abstract void SetButtonName(Button button, object data);
    public abstract void AfterDataUpdatedProcess(IObjectData objectData, object data);

    protected bool _IsCanChangeFocus = true;

    void OnEnable()
    {
        SetRoot();
        if(HandleScrollNextPage)
            EventManager.Instance.AddListener<ScrollNextPageEvent>(ScrollNextPageEventHandler);
    }

    void OnDisable()
    {
        if(EventManager.Instance && HandleScrollNextPage)
            EventManager.Instance.RemoveListener<ScrollNextPageEvent>(ScrollNextPageEventHandler);
    }

    void Update()
    {
        if (!HandleScrollNextPage || _IsScrollDisabled)
            return;

        if (_CanScroll)
        {
            int trigger = LegrandUtility.GetTriggerInput();
            if (trigger == 0)
            {
                if (!IsChildSelected())
                    return;
                ScrollPrevPage();
                _CanScroll = false;
            }
            else if (trigger == 1)
            {
                if (!IsChildSelected())
                    return;
                ScrollNextPage();
                _CanScroll = false;
            }
        }
        else
        {
            if (LegrandUtility.GetTriggerInput() == -1)
                _CanScroll = true;
        }
    }

    void ScrollNextPageEventHandler(ScrollNextPageEvent e)
    {
        _IsScrollDisabled = !e.CanScroll;
    }

    bool IsChildSelected()
    {
        Button currBtn = UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.GetComponent<Button>();
        if (currBtn != null && _ListButtons.Contains(currBtn))
            return true;
        else
            return false;
    }

    void SetRoot()
    {
        if (Root)
            _Root = Root;
        else
            _Root = this.gameObject;
    }

    public virtual void UpdateList()
    {
        UpdateListItem();

        if(Navigation == BaseEnum.Orientation.Both)
            LegrandUtility.SetGridButtonsNavigation(_ListButtons, ScrollerController?ScrollerController.SeenCol:SeenCol);
        else
            LegrandUtility.SetButtonsNavigation(_ListButtons, Navigation);
        
        CalculateHeight();

        if (ScrollerController)
        {
            ScrollerController.Reset();
            for (int i = 0; i < _ListButtons.Count; i++)
            {
                ScrollerController.AddObjectChild(_ListButtons[i].gameObject);
            }
        }
    }

    void UpdateListItem()
    {
        indexButton = 0;

        StartLoopUpdate();

        if (_ListButtons.Count > indexButton)
        {
            for (int i = indexButton; i < _ListButtons.Count; i++)
            {
                //Destroy(_ListButtons[i].gameObject);
                _ListButtons[i].gameObject.SetActive(false);
            }
            _ListButtons.RemoveRange(indexButton, _ListButtons.Count - indexButton);
        }
    }

    protected void UpdateEachButton(object data)
    {
        if (CheckConditionPreUpdate(data))
        {
            UpdateOrCreate(data);
            indexButton++;
        }
    }

    void UpdateOrCreate(object data)
    {
        if (indexButton < _ListButtons.Count)
        {
            UpdateButtonData(_ListButtons[indexButton], data);
        }
        else if(indexButton < _PoolingButtons.Count) //if recycled button have unused asset, use it instead
        {
            _ListButtons.Add(_PoolingButtons[indexButton]);
            _ListButtons[indexButton].gameObject.SetActive(true);
            UpdateButtonData(_ListButtons[indexButton], data);
        }
        else
            CreateNewButton(data);

        AfterDataUpdatedProcess(_ListButtons[indexButton].GetComponent<IObjectData>(), data);
    }

    void UpdateButtonData(Button button, object data)
    {
        if (button.GetComponent<IObjectData>().GetData() != data)
        {
            button.GetComponent<IObjectData>().SetData(data);
            SetButtonName(button, data);
        }
        else
            button.GetComponent<IObjectData>().RefreshData();
    }

    void CreateNewButton(object data)
    {
        if (_Root == null)
            SetRoot();

        Button newButton;

        newButton = Instantiate(TemplateButton) as Button;
        newButton.transform.SetParent(_Root.transform, false);

        newButton.gameObject.SetActive(true);
        newButton.GetComponent<IObjectData>().SetData(data);

        SetButtonName(newButton, data);
        
        _ListButtons.Add(newButton);
        _PoolingButtons.Add(newButton);
        _ListDataHandler.Add(newButton.GetComponent<IObjectData>());
    }

    void CalculateHeight()
    {
        if (_Root == null)
            SetRoot();

        RectTransform thisRT;
        Rect currRect;
        thisRT = _Root.GetComponent<RectTransform>();
        currRect = thisRT.rect;
        float height;

        height = _Root.transform.parent.GetComponent<RectTransform>().rect.height / (ScrollerController? ScrollerController.SeenRow:SeenRow);
        if (_Root.GetComponentInChildren<Button>() != null || _ListButtons.Count > 0)
        {
            if ((ScrollerController? ScrollerController.SeenCol:SeenCol) > 1)
                height *= Mathf.CeilToInt(((float)_ListButtons.Count / (float)(ScrollerController? ScrollerController.SeenCol:SeenCol)));
            else
                height *= _ListButtons.Count;
        }
        else
            height *= 0f;

        currRect.height = height;
        thisRT.sizeDelta = new Vector2(thisRT.sizeDelta.x, currRect.height); // << Change Rect Transform Height
    }

    //Is used in editor to access FocusToFirstButton();
    public virtual void SelectFirstButton()
    {
        FocusToFirstButton();
    }

    public virtual bool FocusToFirstButton()
    {
        if (!_IsCanChangeFocus)
            return false;

        if (_ListButtons.Count > 0 && _ListButtons[0] != null)
        {
            _ListButtons[0].Select();
            return true;
        }
        return false;
    }

    public virtual bool FocusToButton(int index)
    {
        if (!_IsCanChangeFocus)
            return false;

        if (_ListButtons.Count == 0)
            return false;

        index = Mathf.Clamp(index, 0, _ListButtons.Count-1);
        _ListButtons[index].Select();
        return true;
    }

    public void SetCanChangeFocus(bool isCanChange)
    {
        _IsCanChangeFocus = isCanChange;
    }

    public void ScrollNextPage()
    {
        if (_ListButtons.Count == 0 || ScrollerController == null)
            return;
        int nextIndex = Mathf.Clamp(ScrollerController.LastIndex + ((ScrollerController.SeenRow -1)*ScrollerController.SeenCol), 1, _ListButtons.Count);

        FocusToButton(nextIndex-1);
    }

    public void ScrollPrevPage()
    {
        if (_ListButtons.Count == 0 || ScrollerController == null)
            return;
        int nextIndex = Mathf.Clamp(ScrollerController.LastIndex - ((ScrollerController.SeenRow - 1) * ScrollerController.SeenCol), 1, _ListButtons.Count);

        FocusToButton(nextIndex-1);
    }
}
