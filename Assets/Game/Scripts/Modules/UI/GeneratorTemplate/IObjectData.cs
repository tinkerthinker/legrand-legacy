﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public interface IObjectData : ISelectHandler {
    void SetData(object obj);
    object GetData();
    void RefreshData();
}
