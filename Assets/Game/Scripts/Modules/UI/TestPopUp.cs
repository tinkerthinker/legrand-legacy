﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class TestPopUp : MonoBehaviour {
	List<int> testList = new List<int>();

	void Awake()
	{
		testList.Add (9);
		testList.Add (2);
		testList.Add (5);
		testList.Add (1);
		testList.Add (3);
		testList.Add (7);
	}

	void Start()
	{
		testList.Sort (CompareValue);
		foreach (int i in testList) {
			Debug.Log(i.ToString()+"\n");
		}
	}

	private int CompareValue(int i1, int i2)
	{
		return i2 - i1;
	}
}
