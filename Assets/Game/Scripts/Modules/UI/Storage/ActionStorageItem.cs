﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ActionStorageItem : MonoBehaviour
{
    public enum StorageAction
    {
        SendAmount,
        SendAcross
    }
    public StorageAction Action;
    private int _actionAmount;

    private NavigationButtonData _NavigationData;

    void Awake()
    {
        GetComponent<Button>().onClick.AddListener(() => OnClickHandler());
        _NavigationData = GetComponent<NavigationButtonData>();
    }

    void SetAmountEventHandler(SetAmountEvent e)
    {
        _actionAmount = e.Amount;
    }

    void OnClickHandler()
    {
        switch (Action)
        {
            case StorageAction.SendAmount:
                int maxAmount = ((ItemSlot)(GetComponent<ItemInventoryData>().GetData())).Stack;

                EventManager.Instance.AddListener<SetAmountEvent>(SetAmountEventHandler);
                PopUpUI.CallSetAmountPopUp(maxAmount);

                if (_NavigationData != null)
                    _NavigationData.ChangeToNone(GetComponent<Button>());

                //then change action mode to send across to inventory or storage
                Action = StorageAction.SendAcross;
                EventManager.Instance.TriggerEvent(new ScrollNextPageEvent(false));
                break;
            case StorageAction.SendAcross:
                /*
                EventManager.Instance.RemoveListener<SetAmountEvent>(SetAmountEventHandler);

                PopUpUI.CloseAllPopUp(); // close set amount popup                

                Action = StorageAction.SendAmount;

                if (_NavigationData != null)
                    _NavigationData.ChangeToLastNavigation();
                EventManager.Instance.TriggerEvent(new ScrollNextPageEvent(true));
                */
                OnCancelHandler();
                EventManager.Instance.TriggerEvent(new SendAcrossStorageEvent(((ItemSlot)(GetComponent<ItemInventoryData>().GetData())).ItemMaster, _actionAmount));
                break;
        }
    }

    public void OnCancelHandler()
    {
        switch (Action)
        {
            case StorageAction.SendAmount:
                EventManager.Instance.TriggerEvent(new StorageUIEvent(false));
                break;
            case StorageAction.SendAcross:
                EventManager.Instance.RemoveListener<SetAmountEvent>(SetAmountEventHandler);

                PopUpUI.CloseAllPopUp(); // close set amount popup

                Action = StorageAction.SendAmount;

                if (_NavigationData != null)
                    _NavigationData.ChangeToLastNavigation();
                EventManager.Instance.TriggerEvent(new ScrollNextPageEvent(true));
                break;
        }
    }
}