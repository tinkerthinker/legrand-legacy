﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GenerateStorageItem : AbsObjectListGenerator {
    public List<Item.Type> ItemTypeGenerated;

    public override void StartLoopUpdate()
    {
        foreach (Item.Type type in ItemTypeGenerated)
        {
            foreach (var itemSlot in PartyManager.Instance.Storage.Items[(int)type])
            {
                UpdateEachButton(itemSlot);
            }
        }
    }

    public override bool CheckConditionPreUpdate(object data)
    {
        return true;
    }

    public override void SetButtonName(UnityEngine.UI.Button button, object data)
    {
        button.name = ((ItemSlot)data).ItemMaster.Name;
    }

    public override void AfterDataUpdatedProcess(IObjectData objectData, object data)
    {

    }

    public Button GetItemButton(string id)
    {
        foreach (Button inventoryButton in _ListButtons)
        {
            if (inventoryButton.GetComponent<ItemInventoryData>().ItemSlotData.ItemMaster._ID.Equals(id))
            {
                return inventoryButton;
            }
        }

        return null;
    }

    public int GetItemButtonIndex(string id)
    {
        for (int i = 0; i < _ListButtons.Count; i++)
        {
            if (_ListButtons[i].GetComponent<ItemInventoryData>().ItemSlotData.ItemMaster._ID.Equals(id))
            {
                return i;
            }
        }
        return -1;
    }

    public bool FocusToItemButton(string id)
    {
        for (int i = 0; i < _ListButtons.Count; i++)
        {
            if (_ListButtons[i].GetComponent<ItemInventoryData>().ItemSlotData.ItemMaster._ID.Equals(id))
            {
                _ListButtons[i].Select();
                return true;
            }
        }

        return false;
    }
}