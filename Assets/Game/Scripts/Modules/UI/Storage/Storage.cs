﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;

public class Storage
{
    public List<ItemSlot>[] Items;
    public List<Fact>[] Facts;

    public int Gold;
    private int _CurrentWeight;
    public int CurrentWeight
    {
        get { return _CurrentWeight; }
        set
        {
            _CurrentWeight = value;
            if (!IsOverWeight && _CurrentWeight > MaxWeight)
            {
                IsOverWeight = true;
                EventManager.Instance.TriggerEvent(new SetEncumberedEvent(IsOverWeight));
            }
            else if (IsOverWeight && _CurrentWeight <= MaxWeight)
            {
                IsOverWeight = false;
                EventManager.Instance.TriggerEvent(new SetEncumberedEvent(IsOverWeight));
            }
        }
    }
    public int MaxWeight;
    public bool IsOverWeight;

    public Storage()
    {
        Gold = 0;
        MaxWeight = 200;
        Items = new List<ItemSlot>[5];
        for (int i = 0; i < Items.Length; i++)
        {
            Items[i] = new List<ItemSlot>();
        }
        Facts = new List<Fact>[5];
        for (int i = 0; i < Facts.Length; i++)
        {
            Facts[i] = new List<Fact>();
        }
        IsOverWeight = false;
    }

    public bool Contains(Item item)
    {
        foreach (ItemSlot invenSlot in Items[(int)item.ItemType])
        {
            if (invenSlot.ItemMaster._ID == item._ID)
            {
                return true;
            }
        }
        return false;
    }

    public int GetStackItem(Item item)
    {
        foreach (ItemSlot invenSlot in Items[(int)item.ItemType])
        {
            if (invenSlot.ItemMaster._ID == item._ID)
            {
                return invenSlot.Stack;
            }
        }
        return 0;
    }

    public bool AddItem(Item itemToAdd)
    {
        return AddItems(itemToAdd, 1);
    }

    public bool AddItems(Item itemToAdd, int amount)
    {
        ItemSlot inventoryItem = Items[(int)itemToAdd.ItemType].Find(x => x.ItemMaster._ID == itemToAdd._ID);
        if (inventoryItem != null)
            inventoryItem.Stack += amount;
        else
        {
            inventoryItem = new ItemSlot(itemToAdd);
            inventoryItem.Stack = amount;
            Items[(int)itemToAdd.ItemType].Add(inventoryItem);
            SortItem((int)itemToAdd.ItemType);
        }

        Fact fact = Facts[(int)itemToAdd.ItemType].Find(x => x.Name == "Has Item " + itemToAdd._ID);
        if (fact == null) Facts[(int)itemToAdd.ItemType].Add(new Fact("Has Item " + inventoryItem.ItemMaster._ID, inventoryItem.Stack, string.Empty, null, null));
        else fact.Value = inventoryItem.Stack;

        CurrentWeight += itemToAdd.Weight * amount; //Add Size

        //// Check key effect
        //if (itemToAdd.ItemType == Item.Type.Key)
        //{
        //    Key addedKeyItem = (Key)itemToAdd;
        //    for (int i = 0; addedKeyItem.ItemEffect != null && i < addedKeyItem.ItemEffect.Count; i++)
        //    {
        //        //if it's blueprint
        //        if (addedKeyItem.ItemEffect[i]._Attribute == MiscAttribute.Blueprint)
        //        {
        //            PartyManager.Instance.UnlockedBlueprints.Add(LegrandBackend.Instance.GetBlueprint(addedKeyItem.ItemEffect[i]._Value));
        //        }
        //    }
        //}

        return true;
    }

    public int DeleteItemsByItemID(string ItemID, int number)
    {
        // 0 = succeed, 1 = item not found, 2 = unrealistic amount, 3 = other
        int i = 0;
        while (i < Items.Length)
        {
            foreach (ItemSlot invenSlot in Items[i])
            {
                if (invenSlot.ItemMaster._ID == ItemID)
                {
                    if (invenSlot.Stack >= number)
                    {
                        invenSlot.Stack -= number;
                        Fact fact = Facts[(int)invenSlot.ItemMaster.ItemType].Find(x => x.Name == "Has Item " + invenSlot.ItemMaster._ID);
                        if (fact == null) Facts[(int)invenSlot.ItemMaster.ItemType].Add(new Fact("Has Item " + invenSlot.ItemMaster._ID, invenSlot.Stack, string.Empty, null, null));
                        else fact.Value = invenSlot.Stack;

                        if (invenSlot.Stack == 0)
                        {
                            Items[i].Remove(invenSlot);
                        }
                        CurrentWeight -= invenSlot.ItemMaster.Weight * number;
                        return 0;
                    }
                    else
                    {
                        return 2;
                    }


                }

            }
            i++;
        }
        return 1;
    }

    public ItemSlot GetItem(string itemID)
    {
        // return true kalau dapat
        foreach (ItemSlot itemSlot in Items[(int)Item.Type.Consumable])
        {
            if (itemSlot.ItemMaster != null && itemSlot.ItemMaster._ID == itemID)
            {
                return itemSlot;
            }
        }
        return null;
    }

    public ItemSlot GetItem(string itemID, Item.Type typeItem)
    {
        // return true kalau dapat
        foreach (ItemSlot itemSlot in Items[(int)typeItem])
        {
            if (itemSlot.ItemMaster != null && itemSlot.ItemMaster._ID == itemID)
            {
                return itemSlot;
            }
        }
        return null;
    }

    public void DeleteAll()
    {
        CurrentWeight = 0;
        for (int i = 0; i < 7; i++)
        {
            Items[i].Clear();
        }
    }

    public int CompareItem(ItemSlot a, ItemSlot b)
    {
        int value = 0;
        // consumable, material, blueprint sorted by Id
        //value = (a.ItemMaster._ID.CompareTo(b.ItemMaster._ID));

        //consumable type = Healing, magic, attribute, misc
        //item type = Weapon, Armor, Consumable, CraftingMaterial, Key

        if (a.ItemMaster.ItemType == Item.Type.Consumable)
        {
            value = (int)((Consumable)a.ItemMaster).ConsumableType - (int)((Consumable)b.ItemMaster).ConsumableType;
            if (value == 0)
                value = (a.ItemMaster._ID.CompareTo(b.ItemMaster._ID));
        }
        else if (a.ItemMaster.ItemType == Item.Type.CraftingMaterial)
        {
            value = (a.ItemMaster.Name.CompareTo(b.ItemMaster.Name));
        }
        else if (a.ItemMaster.ItemType == Item.Type.Key)
        {
            value = value = (a.ItemMaster._ID.Substring(0, 2).CompareTo(b.ItemMaster._ID.Substring(0, 2)));
            if (value == 0)
                value = (a.ItemMaster.Name.CompareTo(b.ItemMaster.Name));
        }
        else
            value = (a.ItemMaster._ID.CompareTo(b.ItemMaster._ID));

        return value;

    }

    public void RecalculateCurrentSize()
    {
        CurrentWeight = 0;
        for (int i = 0; i < Items.Length; i++)
        {
            foreach (var it in Items[i])
            {
                CurrentWeight += (it.Stack * it.ItemMaster.Weight);
            }
        }
    }

    public void SortItem(int type)
    {
        Items[type].Sort(CompareItem);
    }

    void SortAll()
    {
        for (int i = 0; i < Items.Length; i++)
        {
            SortItem(i);
        }
    }

    public List<Fact> GetFacts(Item.Type type)
    {
        return Facts[(int)type];
    }
}
