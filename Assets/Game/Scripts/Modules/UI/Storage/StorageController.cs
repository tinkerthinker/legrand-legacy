﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using TeamUtility.IO;

public class StorageController : MonoBehaviour {
    public GenerateInventoryItem InventoryGenerator;
    public GameObject InventoryHighlight;
    public GenerateStorageItem StorageGenerator;
    public GameObject StorageHighlight;

    private bool _InStorage = false;
    public bool InStorage
    {
        get { return _InStorage; }
    }

    private bool _CanSwitch = true;
    public bool CanSwitch
    {
        get { return _CanSwitch; }
    }

    void OnEnable()
    {
        EventManager.Instance.AddListener<SendAcrossStorageEvent>(SendItemAcross);
        EventManager.Instance.AddListener<ScrollNextPageEvent>(EnableSwitchBox);

        RefreshStorage();
        StartCoroutine(WaitSelectFirstButton());
    }

    void OnDisable()
    {
        if(EventManager.Instance)
            EventManager.Instance.RemoveListener<SendAcrossStorageEvent>(SendItemAcross);
        if(EventSystem.current)
            EventSystem.current.SetSelectedGameObject(gameObject);
    }

    void Update()
    {
        if(InputManager.GetButtonDown("Menu") && _CanSwitch)
        {
            if(_InStorage)
            {
                FocusToInventory();
            }
            else
            {
                FocusToStorage();
            }
        }
    }

    void RefreshStorage()
    {
        if (StorageGenerator)
        {
            StorageGenerator.UpdateList();
        }

        if (InventoryGenerator)
        {
            InventoryGenerator.UpdateInventory();
        }
        EventManager.Instance.TriggerEvent(new InventoryChangedEvent());
    }

    IEnumerator WaitSelectFirstButton()
    {
        yield return null;
        if (InventoryGenerator)
        {
            FocusToInventory();
        }
    }

    void FocusToInventory()
    {
        if (!InventoryGenerator.FocusToFirstButton())
            InventoryGenerator.ScrollerController.ScrollbarTarget.Select();
        _InStorage = false;
        if(InventoryHighlight)
            InventoryHighlight.SetActive(true);
        if (StorageHighlight)
            StorageHighlight.SetActive(false);
    }

    void FocusToStorage()
    {
        if (!StorageGenerator.FocusToFirstButton())
            StorageGenerator.ScrollerController.ScrollbarTarget.Select();
        _InStorage = true;
        if (InventoryHighlight)
            InventoryHighlight.SetActive(false);
        if (StorageHighlight)
            StorageHighlight.SetActive(true);
    }

    void EnableSwitchBox(ScrollNextPageEvent e)
    {
        _CanSwitch = e.CanScroll;
    }

    public void SendItemAcross(SendAcrossStorageEvent e)
    {
        if(_InStorage)
        {
            int indexBtn = StorageGenerator.GetItemButtonIndex(e.Item._ID);

            PartyManager.Instance.Inventory.AddItems(e.Item, e.Amount);
            PartyManager.Instance.Storage.DeleteItemsByItemID(e.Item._ID, e.Amount);

            RefreshStorage();

            if (!StorageGenerator.FocusToItemButton(e.Item._ID))
                if (!StorageGenerator.FocusToButton(indexBtn))
                    StorageGenerator.ScrollerController.ScrollbarTarget.Select();
        }
        else
        {
            int indexBtn = InventoryGenerator.GetItemButtonIndex(e.Item._ID);

            PartyManager.Instance.Storage.AddItems(e.Item, e.Amount);
            PartyManager.Instance.Inventory.DeleteItemsByItemID(e.Item._ID, e.Amount);

            RefreshStorage();

            if (!InventoryGenerator.FocusToItemButton(e.Item._ID))
                if (!InventoryGenerator.FocusToButton(indexBtn))
                    InventoryGenerator.ScrollerController.ScrollbarTarget.Select();
        }   
    }

    public void CloseStorage()
    {
        EventManager.Instance.TriggerEvent(new StorageUIEvent(false));
    }
}
