﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class CancelShopItem : MonoBehaviour, ICancelHandler {

	public void OnCancel(BaseEventData eventData)
	{
		if (ShopController.Instance != null) {
			if (ShopController.Instance.gameObject.activeInHierarchy)
			{
				ShopController.Instance.OnCancelShopHandler ();
				return;
			}
		}
	}
}
