﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using TeamUtility.IO;

public class TransactionController : MonoBehaviour {
	public enum TransactionType
	{
		Buy,
		Sell
	}

	public TransactionType CurrentTransactionType;

	public static TransactionController Instance;

	public GenerateShopItemList GeneratingShopItemObj;
    public ShiftTabController ShiftController;

	public Text CurrentGoldText;
	public Text CurrentWeightText;
	public Image weightBar;
	public Text TotalPriceText;
	public Text TotalWeightText;

    public string HexNormalColor;
    public string HexPositifColor;
    public string HexNegatifColor;

    public WwiseManager.UIFX AddItemSuccess;
    public WwiseManager.UIFX AddItemFailed;

	private Dictionary<string,int>[] _ItemCart;
	public Dictionary<string,int>[] ItemCart {
		get{return _ItemCart;}
	}
    private List<string>[] CartItemsId;

	private int cartGold, cartWeight;	//used for buy transaction
    private int _CurrTabTransaction = 0;

    private string _CallingID;

    public TransactionController()
    {
        _ItemCart = new Dictionary<string, int>[2];
        CartItemsId = new List<string>[2];

        for (int i = 0 ; i < _ItemCart.Length; i++)
        {
            _ItemCart[i] = new Dictionary<string, int>();
            CartItemsId[i] = new List<string>();
        }
            
    }

    private bool _IsHaltProcess = false;

	void Awake () {
        ShiftController.RegisterShiftingFunction(ShiftTabTransaction, _ItemCart.Length-1);
	}

	void OnEnable()
	{
        if (TransactionController.Instance && TransactionController.Instance != this)
            TransactionController.Instance.gameObject.SetActive(false);
		TransactionController.Instance = this;
		cartGold = cartWeight = 0;
        //ChangeTransactionType(TransactionType.Buy);
	}

    void OnDisable()
    {
        ClearCart();
    }

    void Update()
    {
        if (InputManager.GetButtonDown("Delete") && !_IsHaltProcess && !IsCartEmpty())
        {
            HaltProcess();
            PopUpUI.CallChoicePopUp(LegrandBackend.Instance.GetLanguageData("Clear Cart ?",true), ResetCart);
        }
    }

    void ShiftTabTransaction(int index)
    {
        ChangeTransactionType((TransactionType)index);
    }

    void ShiftTabTransaction(ref int index, int increment)
    {
        int lastIndex = index;
        index += increment;
        if (index > _ItemCart.Length - 1)
        {
            index = 0;
        }
        else if (index < 0)
        {
            index = _ItemCart.Length - 1;
        }

        ChangeTransactionType((TransactionType)index);
    }

    public void ChangeTransactionType(TransactionType type)
    {
        CurrentTransactionType = type;
        GeneratingShopItemObj.scrollBarShop.Select();
        GeneratingShopItemObj.UpdateListShop();
        UpdateAmountItem();
        StartCoroutine(WaitSelectFirstButton());
        UpdateCartInfo();
    }

    IEnumerator WaitSelectFirstButton()
    {
        yield return null;
        while(_IsHaltProcess)
        {
            yield return null;
        }
        //GeneratingShopItemObj.FocusToFirstButton();
        TryToFocusFirstButton();
    }

    public void TryToFocusFirstButton()
    {
        if (!GeneratingShopItemObj.FocusToFirstButton())
        {
            GeneratingShopItemObj.scrollBarShop.Select();
        }
    }

    public void HaltProcess()
    {
        _IsHaltProcess = true;
    }

    public void ResumeProcess()
    {
        _IsHaltProcess = false;
    }

	void CalculateInventoryWeight()
	{
		int currWeight = PartyManager.Instance.Inventory.CurrentWeight;
		int maxWeight = PartyManager.Instance.Inventory.MaxWeight;
	}

	public void SetItemCart(List<string> itemsID)
	{
        int CurrentTransId = (int)CurrentTransactionType;
        //_ItemCart[CurrentTransId].Clear ();
		int i = 0;
		foreach (var id in itemsID) {
            if (!_ItemCart[CurrentTransId].ContainsKey(id))
            {
                _ItemCart[CurrentTransId].Add(id, 0);
                CartItemsId[CurrentTransId].Add(id);
            }			    
			i++;
		}
		
		CurrentGoldText.text = PartyManager.Instance.Inventory.Gold + " D";
		CurrentWeightText.text = PartyManager.Instance.Inventory.CurrentWeight + "/" +PartyManager.Instance.Inventory.MaxWeight;
		CalculateInventoryWeight ();
		
		TotalPriceText.text = cartGold +" D";
		TotalWeightText.text = cartWeight.ToString();
	}
	
	public void UpdateCartInfo()
	{
        string gold, weight;
        gold = weight = "<color=#";

        //Add color to total gold transaction
        if (cartGold > 0)
            gold += HexPositifColor;
        else if(cartGold < 0)
            gold += HexNegatifColor;
        else
            gold += HexNormalColor;
        gold += ">" + cartGold + "D</color>";

        //Add color to total weight transaction
        if (cartWeight > 0)
            weight += HexNegatifColor;
        else if (cartWeight < 0)
            weight += HexPositifColor;
        else
            weight += HexNormalColor;
        weight += ">" + cartWeight + "</color>";
        
		TotalPriceText.text = gold;
		TotalWeightText.text = weight;
	}
	
	void ClearCart(bool isClearList = true)
	{
        if(isClearList)
        {
            for (int i = 0; i < _ItemCart.Length; i++)
            {
                _ItemCart[i].Clear();
                CartItemsId[i].Clear();
            } 
        }
        else
        {
            for (int i = 0; i < _ItemCart.Length; i++)
            {
                for (int j = 0; j < CartItemsId[i].Count; j++)
                {
                    _ItemCart[i][CartItemsId[i][j]] = 0;
                }
            }
        }
	}

    public void ResetCart()
    {
        cartGold = cartWeight = 0;
        ResumeProcess();
        ClearCart(false);
        GeneratingShopItemObj.UpdateListShop();
        GeneratingShopItemObj.ResetAmountItem();
        UpdateCartInfo();
    }

    void UpdateAmountItem()
    {
        List<Button> listButtons = GeneratingShopItemObj.ShopItemButtons;
        for (int i = 0; i < listButtons.Count; i++)
        {
            listButtons[i].GetComponent<AddToCartShop>().UpdateAmountText();
        }
    }

    public void AddItemToCart(string id)
    {
        if (CurrentTransactionType == TransactionType.Buy)
            AddItemToBuyCart(id);
        else if (CurrentTransactionType == TransactionType.Sell)
            AddItemToSellCart(id);
    }

    public void DeleteItemFromCart(string id)
    {
        if (CurrentTransactionType == TransactionType.Buy)
            DeleteItemFromBuyCart(id);
        else if (CurrentTransactionType == TransactionType.Sell)
            DeleteItemFromSellCart(id);
    }

    public bool IsCartEmpty()
    {
        for(int i =0; i<2;i++)
        {
            foreach(KeyValuePair<string, int> k in _ItemCart[i])
            {
                if (k.Value > 0)
                    return false;
            }
            
        }
        return true;
    }

    public bool CheckCanAddItem(string itemId)
    {
        if (CurrentTransactionType == TransactionType.Buy)
            return CheckCanBuy(itemId);
        else if (CurrentTransactionType == TransactionType.Sell)
            return CheckCanSell(itemId);
        return true;
    }

	#region BUY
	private bool CheckCanBuy(string itemID)
	{
		int currGold, currWeight, totalStack;
		currGold = PartyManager.Instance.Inventory.Gold;
		currWeight = PartyManager.Instance.Inventory.CurrentWeight;
		
		Item item = LegrandBackend.Instance.ItemData [itemID];
        if(item.ItemType == Item.Type.Key)
        {
            if (PartyManager.Instance.Inventory.Contains(item) || (_ItemCart[(int)TransactionType.Buy].ContainsKey(itemID) && _ItemCart[(int)TransactionType.Buy][itemID] > 0))
            {
                return false;
            }
        }

        totalStack = (PartyManager.Instance.Inventory.Contains(item) ? PartyManager.Instance.Inventory.GetItem(itemID).Stack : 0) + (_ItemCart[(int)TransactionType.Buy].ContainsKey(itemID)? _ItemCart[(int)TransactionType.Buy][itemID] : 0) - (_ItemCart[(int)TransactionType.Sell].ContainsKey (itemID)? _ItemCart[(int)TransactionType.Sell][itemID] : 0);
		
		if (item.BuyPrice <= currGold + cartGold &&
            item.Weight + cartWeight <= PartyManager.Instance.Inventory.MaxWeight - currWeight &&
            totalStack < 99)
        {		
			return true;
		}
		else
			return false;
	}
	
	public void AddItemToBuyCart(string id)
	{
		//if(itemCart [id]<PartyManager.Instance.Inventory.MaxWeight)
		/*
			itemCart [id] += 1;
		if (! CheckCanBuy ()) {
			itemCart [id] -= 1;
		}*/
        if (CheckCanBuy(id))
        {
            Item item = LegrandBackend.Instance.ItemData[id];
            cartGold -= item.BuyPrice;
            cartWeight += item.Weight;

            _ItemCart[(int)TransactionType.Buy][id] += 1;

            WwiseManager.Instance.PlaySFX(AddItemSuccess);
            UpdateCartInfo();
        }
        else
            WwiseManager.Instance.PlaySFX(AddItemFailed);
		
	}
	
	public void DeleteItemFromBuyCart(string id)
	{
		Item item = LegrandBackend.Instance.ItemData [id];

        if (_ItemCart[(int)TransactionType.Buy][id] > 0)
        {
            _ItemCart[(int)TransactionType.Buy][id] -= 1;
			cartGold += item.BuyPrice;
			cartWeight -= item.Weight;

            WwiseManager.Instance.PlaySFX(AddItemSuccess);
            UpdateCartInfo();
		}
        else
            WwiseManager.Instance.PlaySFX(AddItemFailed);
	}
	#endregion
	
	#region SELL
	private bool CheckCanSell(string itemID)
	{
		int currGold, currWeight;
		currGold = PartyManager.Instance.Inventory.Gold;
		currWeight = PartyManager.Instance.Inventory.CurrentWeight;
		
		Item item = LegrandBackend.Instance.ItemData [itemID];
		int stack=0;
		
		//search for stack
		bool found = false;
		int i = 0;
		//for (int i=0; i<PartyManager.Instance.Inventory.Items.Length; i++) {
		while(i < PartyManager.Instance.Inventory.Items.Length && !found)
		{
			//GenerateInventoryItem((Item.Type)i);
			foreach (var itemSlot in PartyManager.Instance.Inventory.Items [(int)i]) {
				if(itemSlot.ItemMaster._ID == itemID)
				{
					stack = itemSlot.Stack;
					found = true;
					break;
				}
			}
			i++;
		}

        if (stack - _ItemCart[(int)TransactionType.Sell] [itemID] > 0)
		{			
			return true;
		}
        else
		    return false;
	}
	
	public void AddItemToSellCart(string id)
	{
        if (CheckCanSell(id))
        {
            Item item = LegrandBackend.Instance.ItemData[id];
            cartGold += item.SellPrice;
            cartWeight -= item.Weight;

            _ItemCart[(int)TransactionType.Sell][id] += 1;

            WwiseManager.Instance.PlaySFX(AddItemSuccess);
            UpdateCartInfo();
        }
        else
            WwiseManager.Instance.PlaySFX(AddItemFailed);
	}
	
	public void DeleteItemFromSellCart(string id)
	{
		Item item = LegrandBackend.Instance.ItemData [id];

        if (_ItemCart[(int)TransactionType.Sell][id] > 0 && PartyManager.Instance.Inventory.Gold + cartGold - item.SellPrice >= 0)
        {
            _ItemCart[(int)TransactionType.Sell][id] -= 1;
			cartGold -= item.SellPrice;
			cartWeight += item.Weight;

            WwiseManager.Instance.PlaySFX(AddItemSuccess);
            UpdateCartInfo();
		}
        else
            WwiseManager.Instance.PlaySFX(AddItemFailed);
		
	}
	#endregion

    public int GetItemAmountInCart(string itemId)
    {
        return _ItemCart[(int)CurrentTransactionType][itemId];
    }

	public void CheckOutCart(string callingID)
	{
        _CallingID = callingID;

        if(!IsCartEmpty())
            PopUpUI.CallChoicePopUp("Check Out Cart ?", ConfirmCheckoutCart);        
	}

    public void ConfirmCheckoutCart()
    {
        //Check Out Sell Cart
        foreach (KeyValuePair<string, int> id in _ItemCart[(int)TransactionType.Sell])
        {
            PartyManager.Instance.Inventory.SellItemsByItemID(id.Key, id.Value);
        }
        //Check Out Buy Cart
        foreach (KeyValuePair<string, int> id in _ItemCart[(int)TransactionType.Buy])
        {
            PartyManager.Instance.Inventory.BuyItemsByItemID(id.Key, id.Value);
        }

        WwiseManager.Instance.PlaySFX(WwiseManager.UIFX.Buy);

        ResetCart();
        StartCoroutine(WaitToCheckFocusButton());
    }

    IEnumerator WaitToCheckFocusButton()
    {
        yield return null;

        if (UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject == null)
        {
            if (!string.IsNullOrEmpty(_CallingID))
            {
                if (!GeneratingShopItemObj.FocusToItemButton(_CallingID))
                {
                    TryToFocusFirstButton();
                }
            }
        }
    }
}
