using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Legrand.database;
//using Legrand.Modules;

public class GenerateShopItemList : AbsObjectListGenerator {
	public Button shopItemButton;
    //public ScrollController ScrollerController;
	public Scrollbar scrollBarShop;
    //public int seenButton;

	//private List<ShopModel> ShopModels = new List<ShopModel>();
	private ShopModel selectedShopModel;

    //private List<Button> _ShopItemButtons = new List<Button>();
	public List<Button> ShopItemButtons{
        //get{return _ShopItemButtons;}
        get { return _ListButtons; }
	}
	
	public Text panelInfoText;


	private List<string> sellItemID = new List<string>();

	void Start () 
	{
        //UpdateListShop();
	}

    public void UpdateListShop()
    {
        UpdateList();
        /*
        switch (TransactionController.Instance.CurrentTransactionType)
        {
            case TransactionController.TransactionType.Buy:
                UpdateShopBuyList();
                break;
            case TransactionController.TransactionType.Sell:
                UpdateShopSellList();
                break;
        }
        LegrandUtility.SetButtonsNavigation(_ShopItemButtons, BaseEnum.Orientation.Vertical);

        ScrollerController.Reset();
        for (int i = 0; i < _ShopItemButtons.Count; i++ )
        {
            ScrollerController.AddObjectChild(_ShopItemButtons[i].gameObject);
        }*/
    }

	void UpdateShopSellList()
	{
		sellItemID.Clear ();

        //int index=0;

		for (int i=0; i<PartyManager.Instance.Inventory.Items.Length; i++) {
            //index = UpdateSellInventoryItem((Item.Type)i, index);
            foreach (var itemSlot in PartyManager.Instance.Inventory.Items[i])
            {
                Item item = itemSlot.ItemMaster;

                UpdateEachButton(item);
                sellItemID.Add(item._ID);
            }
		}

		TransactionController.Instance.SetItemCart(sellItemID);
	}

	void UpdateShopBuyList()
	{
		string shopModelId = ShopController.Instance.selectedShopModelId;

        //int index=0;

		// Database add this
		for(int s=0;s<LegrandBackend.Instance.LegrandDatabase.ShopModelData.Count;s++)
		{
			if(LegrandUtility.CompareString(LegrandBackend.Instance.LegrandDatabase.ShopModelData.Get(s)._ID,shopModelId))
			{
				selectedShopModel = new ShopModel();
				selectedShopModel._ID = LegrandBackend.Instance.LegrandDatabase.ShopModelData.Get(s)._ID;
				foreach(var i in LegrandBackend.Instance.LegrandDatabase.ShopModelData.Get(s).AllItemID)
				{
					selectedShopModel.itemID.Add(i);
				}
				break;
			}
		}

		foreach (var itemId in selectedShopModel.itemID) {
			Item item = LegrandBackend.Instance.ItemData[itemId];

            //index = UpdateShopItemButton(item, index);
            UpdateEachButton(item);

		}

        //if (_ShopItemButtons.Count > index) {
        //    for(int i=index ;i<_ShopItemButtons.Count;i++){
        //        Destroy(_ShopItemButtons[i].gameObject);
        //    }
        //    _ShopItemButtons.RemoveRange(index, _ShopItemButtons.Count - index);
        //}

		TransactionController.Instance.SetItemCart(selectedShopModel.itemID);
	}
	/*
	int UpdateShopItemButton(Item item, int startIndex)
	{
		int index = startIndex;

		if(index < _ShopItemButtons.Count)
		{
            if (_ShopItemButtons[index].GetComponent<ItemShopData>().ItemData != item)
            {
                _ShopItemButtons[index].GetComponent<ItemShopData>().ItemData = item;
                _ShopItemButtons[index].name = item.Name;
            }
            else
                _ShopItemButtons[index].GetComponent<ItemShopData>().UpdateData();

			_ShopItemButtons[index].GetComponent<ItemShopData>().ItemPrice = item.BuyPrice;
		}
		else
			CreateNewShopButton(item);
		index++;
		
		return index; //return index terakhir yang di proses
	}*/
	/*
	void CreateNewShopButton(Item item)
	{
		Button newShopItemButton;

		newShopItemButton = Instantiate (shopItemButton) as Button;
        newShopItemButton.transform.SetParent(transform, false);
		newShopItemButton.name = item.Name;
		
		newShopItemButton.GetComponent<ItemShopData> ().panelInfoText = panelInfoText;
		
		newShopItemButton.GetComponent<ItemShopData> ().ItemData = item;

		if(TransactionController.Instance)
		{
			if(TransactionController.Instance.CurrentTransactionType == TransactionController.TransactionType.Buy)
				newShopItemButton.GetComponent<ItemShopData> ().ItemPrice = item.BuyPrice;
			else if(TransactionController.Instance.CurrentTransactionType == TransactionController.TransactionType.Sell)
				newShopItemButton.GetComponent<ItemShopData> ().ItemPrice = item.SellPrice;
		}
		
		foreach (var itemSlot in PartyManager.Instance.Inventory.Items [(int)item.ItemType]) 
		{
			if(itemSlot.ItemMaster._ID == item._ID)
			{
				newShopItemButton.GetComponent<ItemShopData>().ItemStack = itemSlot.Stack;
				break;
			}
		}
		
		_ShopItemButtons.Add (newShopItemButton);
	}*/

    public void ResetAmountItem()
    {
        for (int i = 0; i < _ListButtons.Count; i++)
        {
            _ListButtons[i].GetComponent<ItemShopData>().SetAmount(0);
        }
    }
		
	public bool FocusToItemButton(string id)
	{
		foreach (var btn in _ListButtons) {
			if(btn.GetComponent<ItemShopData>().ItemData._ID.Equals( id )){
				btn.Select();
				return true;
			}
		}

		return false;
	}

    public override void StartLoopUpdate()
    {
        switch (TransactionController.Instance.CurrentTransactionType)
        {
            case TransactionController.TransactionType.Buy:
                UpdateShopBuyList();
                break;
            case TransactionController.TransactionType.Sell:
                UpdateShopSellList();
                break;
        }
    }

    public override bool CheckConditionPreUpdate(object data)
    {
        if (TransactionController.Instance.CurrentTransactionType == TransactionController.TransactionType.Sell && ((Item)data).ItemType == Item.Type.Key)
            return false;
        return true;
    }

    public override void SetButtonName(Button button, object data)
    {
        if(data is Item)
            button.name = ((Item)data).Name;
    }

    public override void AfterDataUpdatedProcess(IObjectData objectData, object data)
    {
        switch (TransactionController.Instance.CurrentTransactionType)
        {
            case TransactionController.TransactionType.Buy:
                ((ItemShopData)objectData).SetAsBuyItem();
                break;
            case TransactionController.TransactionType.Sell:
                ((ItemShopData)objectData).SetAsSellItem();
                break;
        }
    }
}