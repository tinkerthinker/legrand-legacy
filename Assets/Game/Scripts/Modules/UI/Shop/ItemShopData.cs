﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using Legrand.core;

public class ItemShopData : MonoBehaviour, IObjectData{
	private Item _ItemData;
	public Item ItemData
	{
		set
		{
			_ItemData = value;
            //SetData();
		}
		get{return _ItemData;}
	}

	private int _ItemStack;
	public int ItemStack
	{
		set
		{
			_ItemStack = value;
			if (TextItemInPossession)
				TextItemInPossession.text = _ItemStack.ToString();
		}
		get
		{
			return _ItemStack;
		}
	}

	private int _ItemPrice;
	public int ItemPrice
	{
		set
		{
			_ItemPrice = value;
			if (TextItemPrice)
				TextItemPrice.text = _ItemPrice.ToString();
		}
		get
		{
			return _ItemPrice;
		}
	}

	public Text panelInfoText;

	public Image ImageItem;
	public Text TextItemName;
	public Text TextItemWeight;
	public Text TextItemInPossession;
	public Text TextItemAmount;
	public Text TextItemPrice;
    public Text TextItemAOE;
    public GameObject SoldOutObject;

    private bool _SoldOut;
    public bool IsSoldOut
    {
        get { return _SoldOut; }
    }
    /*
	void SetData()
	{
		if (TextItemName)
			TextItemName.text = _ItemData.Name;
		if (TextItemWeight)
			TextItemWeight.text = _ItemData.Weight.ToString();
		if (TextItemInPossession)
			TextItemInPossession.text = _ItemStack.ToString();
		if (TextItemPrice)
			TextItemPrice.text = _ItemPrice.ToString();
        if(TextItemAOE)
        {
            if(_ItemData.ItemType == Item.Type.Consumable && 
				( ((Consumable)_ItemData).ConsumableType == Consumable.ConsumeType.Healing || 
					((Consumable)_ItemData).ConsumableType == Consumable.ConsumeType.Magic))
            {
                TextItemAOE.text = ((Consumable)_ItemData).Target.ToString().Substring(0, 1);
            }
            else
            {
                TextItemAOE.text = "";
            }
        }

		if (ImageItem)
			ImageItem.sprite = _ItemData.Icon;

		foreach (var itemSlot in PartyManager.Instance.Inventory.Items [(int)_ItemData.ItemType]) 
		{
			if(itemSlot.ItemMaster._ID == _ItemData._ID)
			{
				ItemStack = itemSlot.Stack;
				break;
			}
		}

        if(_ItemData.ItemType == Item.Type.Key && PartyManager.Instance.Inventory.Contains(_ItemData))
        {
            _SoldOut = true;
            if(SoldOutObject)
                SoldOutObject.SetActive(true);
        }
        else
        {
            _SoldOut = false;
            if (SoldOutObject)
                SoldOutObject.SetActive(false);
        }
	}*/
    /*
	public void UpdateData () {
		SetData ();
	}*/

	public void OnSelect(BaseEventData eventData)
	{
		if(panelInfoText)
			panelInfoText.text = _ItemData.Description;

        EventManager.Instance.TriggerEvent(new InspectItemEvent(_ItemData));
	}

    public void SetAmount(int amount)
    {
        if(TextItemAmount)
            TextItemAmount.text = amount.ToString();
    }

    public void SetAsSellItem()
    {
        if (_ItemData == null)
            return;

        if (TextItemPrice)
            TextItemPrice.text = _ItemData.SellPrice.ToString();
    }

    public void SetAsBuyItem()
    {
        if (_ItemData == null)
            return;

        if (TextItemPrice)
            TextItemPrice.text = _ItemData.BuyPrice.ToString();
    }

    public void SetData(object obj)
    {
        if (!(obj is Item))
            return;

        _ItemData = (Item)obj;

        if (TextItemName)
            TextItemName.text = _ItemData.Name;
        if (TextItemWeight)
            TextItemWeight.text = _ItemData.Weight.ToString();
        if (TextItemInPossession)
            TextItemInPossession.text = _ItemStack.ToString();
        if (TextItemAOE)
        {
            if (_ItemData.ItemType == Item.Type.Consumable &&
                (((Consumable)_ItemData).ConsumableType == Consumable.ConsumeType.Healing ||
                    ((Consumable)_ItemData).ConsumableType == Consumable.ConsumeType.Magic))
            {
                TextItemAOE.text = ((Consumable)_ItemData).Target.ToString().Substring(0, 1);
            }
            else
            {
                TextItemAOE.text = "";
            }
        }

        if (ImageItem)
            ImageItem.sprite = _ItemData.Icon;

        foreach (var itemSlot in PartyManager.Instance.Inventory.Items[(int)_ItemData.ItemType])
        {
            if (itemSlot.ItemMaster._ID == _ItemData._ID)
            {
                ItemStack = itemSlot.Stack;
                break;
            }
        }

        if (_ItemData.ItemType == Item.Type.Key && PartyManager.Instance.Inventory.Contains(_ItemData))
        {
            _SoldOut = true;
            if (SoldOutObject)
                SoldOutObject.SetActive(true);
        }
        else
        {
            _SoldOut = false;
            if (SoldOutObject)
                SoldOutObject.SetActive(false);
        }
    }

    public object GetData()
    {
        return _ItemData;
    }

    public void RefreshData()
    {
        SetData(_ItemData);
    }
}
