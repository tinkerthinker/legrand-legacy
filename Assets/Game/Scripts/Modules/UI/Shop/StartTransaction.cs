using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;

public class StartTransaction : MonoBehaviour{
	//public ShopController.ShopType transactionType;
	public TransactionController.TransactionType transactionType;

	void Start () {
		GetComponent<Button> ().onClick.AddListener (() => OnClickHandler ());
	}
	
	void OnClickHandler()
	{
//		ShopController.instance.transactionType = transactionType;

//		TransactionController.instance.CurrentTransactionType = transactionType;
//		GameObject panelShopList = ShopController.instance.panelShopItemList;

		//ChangeInteractableObject.EnableAllButtonInParent (ShopController.instance.panelShopItemList);
		//ShopController.instance.panelShopItemList.SetActive (true);

        //ShopController.Instance.ChangeState (ShopController.ShopState.transaction);

		TransactionController.Instance.CurrentTransactionType = transactionType;
		GameObject panelShopList = ShopController.Instance.panelShopItemList;

		if (ShopController.Instance.isFirstLaunch)
		{
            //ShopController.Instance.isFirstLaunch = false;
			panelShopList.GetComponent<GenerateShopItemList> ().UpdateListShop ();
		} else
			panelShopList.GetComponent<GenerateShopItemList> ().UpdateListShop ();

		if (!panelShopList.GetComponent<GenerateShopItemList> ().FocusToFirstButton ()) {
			panelShopList.GetComponent<GenerateShopItemList> ().scrollBarShop.Select();
			if(TransactionController.Instance.CurrentTransactionType == TransactionController.TransactionType.Buy)
				panelShopList.GetComponent<GenerateShopItemList> ().panelInfoText.text = "Shop don't have any item to sell";
			else if(TransactionController.Instance.CurrentTransactionType == TransactionController.TransactionType.Sell)
				panelShopList.GetComponent<GenerateShopItemList> ().panelInfoText.text = "You don't have any item to sell";
		}

		/*
		if (panelShopList.GetComponent<GenerateShopItemList> ().shopItemButtons.Count > 0)
		{
			ShopController.instance.ChangeState (ShopController.ShopState.transaction);
			panelShopList.GetComponent<GenerateShopItemList> ().FocusToFirstButton ();
		}
		else
		{
			if(TransactionController.instance.CurrentTransactionType == TransactionController.TransactionType.Buy)
				panelShopList.GetComponent<GenerateShopItemList> ().panelInfoText.text = "Shop don't have any item to sell";
			else if(TransactionController.instance.CurrentTransactionType == TransactionController.TransactionType.Sell)
				panelShopList.GetComponent<GenerateShopItemList> ().panelInfoText.text = "You don't have any item to sell";
		}*/
	}

	void UpdateShopPanel()
	{
		ShopController.Instance.panelShopItemList.GetComponent<GenerateShopItemList> ().UpdateListShop ();
	}

	//OLD use OnSelect, NEW use Click so delete baseeventdata
	//public void OnSelect(BaseEventData eventData)
	/*
	public void OnSelect()
	{
		ShopController.instance.transactionType = transactionType;
		if (!ShopController.instance.isFirstLaunch) {
			ShopController.instance.panelShopItemList.GetComponent<GenerateShopItemList> ().UpdateListShop ();
		}
	}
	*/
}
