using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BuyItemShop : MonoBehaviour {


	// Use this for initialization
	void Start () {
		GetComponent<Button> ().onClick.AddListener (() => OnClickHandler ());
	}

	void OnClickHandler()
	{
		/*
		if (ShopController.instance != null) {
			if (ShopController.instance.gameObject.activeInHierarchy)
			{
				ShopController.instance.CheckOutCart (GetComponent<ItemShopData>().itemData._ID);
				return;
			}
		}*/
		TransactionController.Instance.CheckOutCart (GetComponent<ItemShopData> ().ItemData._ID);

	}
}
