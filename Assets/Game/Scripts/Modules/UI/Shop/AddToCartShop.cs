using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using TeamUtility.IO;

public class AddToCartShop : MonoBehaviour, ISelectHandler, IDeselectHandler, IUpdateSelectedHandler {
	public GameObject amountSet;
    public Image RightArrow;
    public Image LeftArrow;
    public Sprite NormalArrow;
    public Sprite HighlightArrow;

	int amount = 0;

	private TeamUtility.IO.StandaloneInputModule module;
    private InputTimer _Timer;

	void Awake()
    {
        float inputActionPerSec = 0;
        if (EventSystem.current.GetComponent<TeamUtility.IO.StandaloneInputModule>() != null)
            inputActionPerSec = EventSystem.current.GetComponent<TeamUtility.IO.StandaloneInputModule>().inputActionsPerSecond;
        _Timer = new InputTimer(BaseEnum.Orientation.Horizontal, inputActionPerSec);
    }

    void OnEnable()
    {
        _Timer.ResetTime();
    }

	void Start () 
	{
        UpdateAmountText();
	}
	
	public void OnUpdateSelected(BaseEventData eventData)
	{
        if (GetComponent<ItemShopData>().IsSoldOut)
            return;

        _Timer.ScanInput();
        if(_Timer.IsAllowed)
        {
            string itemID = GetComponent<ItemShopData>().ItemData._ID;

            if (_Timer.GetInput() > 0.2f)
            {
                TransactionController.Instance.AddItemToCart(itemID);
                UpdateArrow(2);
                UpdateAmountText();
            }
            if (_Timer.GetInput() < -0.2f)
            {
                TransactionController.Instance.DeleteItemFromCart(itemID);
                UpdateArrow(1);
                UpdateAmountText();
            }
        }
	}

    void ResetArrow()
    {
        if (LeftArrow)
        {
            LeftArrow.sprite = NormalArrow;
            //LeftArrow.gameObject.SetActive(true);
        }
        if (RightArrow)
        {
            RightArrow.sprite = NormalArrow;
            //RightArrow.gameObject.SetActive(true);
        }
    }

    void UpdateArrow(int code)
    {
        switch(code)
        {
            case 1:
                if (LeftArrow)
                    LeftArrow.sprite = HighlightArrow;
                break;
            case 2:
                if (RightArrow)
                    RightArrow.sprite = HighlightArrow;
                break;
            default:
                ResetArrow();
                break;
        }

        StartCoroutine(WaitResetArrow());
    }

    IEnumerator WaitResetArrow()
    {
        yield return WaitFor.Frames(3);
        ResetArrow();
    }

	public void UpdateAmountText()
	{
		string itemID = GetComponent<ItemShopData>().ItemData._ID;

        //amount = TransactionController.Instance.ItemCart[(int)TransactionController.Instance.CurrentTransactionType][itemID];
        amount = TransactionController.Instance.GetItemAmountInCart(itemID);

        if (LeftArrow)
        {
            LeftArrow.gameObject.SetActive(true);
        }
        if (RightArrow)
        {
            RightArrow.gameObject.SetActive(true);
        }

        if (amount <= 0)
        {
            if (LeftArrow)
                LeftArrow.gameObject.SetActive(false);
        }
        if (!TransactionController.Instance.CheckCanAddItem(itemID))
        {
            if(RightArrow)
                RightArrow.gameObject.SetActive(false);
        }
        GetComponent<ItemShopData>().SetAmount(amount);
	}

	public void OnSelect(BaseEventData eventData)
	{
		amountSet.SetActive (true);
        //amountSet.GetComponentsInChildren<Text>()[0].text = amount.ToString ();
        //GetComponent<ItemShopData>().textItemAmount.gameObject.SetActive(false);
        UpdateAmountText();
	}

	public void OnDeselect(BaseEventData eventData)
	{
		amountSet.SetActive (false);
        _Timer.ResetTime();
        //GetComponent<ItemShopData>().textItemAmount.gameObject.SetActive(true);
	}
}
