﻿using UnityEngine;
using System.Collections;
using TeamUtility.IO;
using UnityEngine.UI;
using DG.Tweening;

public class CityLegendController : MonoBehaviour {
	public Image LegendPanel;
	public float LegendXMoveSize;
	public bool LegendShown = false;

	private Tween _Tweener;
	void Start()
	{
		
	}

	// Update is called once per frame
	void Update () {
		if (InputManager.GetKeyDown (KeyCode.Space) || InputManager.GetKeyDown (KeyCode.Joystick1Button6)) 
		{
			LegendShown = !LegendShown;
			WwiseManager.Instance.PlaySFX (WwiseManager.UIFX.Confirm);
			if (_Tweener != null) 
			{
				_Tweener.Kill ();
			}
			_Tweener = LegendPanel.rectTransform.DOMoveX (System.Convert.ToSingle(!LegendShown) * -LegendXMoveSize, 1f);
		}
	}
}
