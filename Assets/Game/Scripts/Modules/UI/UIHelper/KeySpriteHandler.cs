﻿using UnityEngine;
using System.Collections.Generic;
using TeamUtility.IO;
using UnityEngine.UI;

public class KeySpriteHandler : MonoBehaviour {
    public string KeyName;
	public bool PrintKeyName=true;
    protected Text _Text;

    void Awake()
    {
        _Text = GetComponent<Text>();
    }

    public virtual void ChangeToKeyBoard()
    {
        if (!_Text) _Text = LegrandUtility.GetComponentInChildren<Text>(this.gameObject);
        _Text.text = InputManager.GetAxisConfiguration(0, KeyName).positive.ToString();
    }

    public virtual void ChangeToController()
    {
        if (!_Text) _Text = LegrandUtility.GetComponentInChildren<Text>(this.gameObject);
        _Text.text = FindControllerName(InputManager.GetAxisConfiguration(0, KeyName).positive);
    }

    protected string FindControllerName(KeyCode key)
    {
        for(int i=0;i<LegrandBackend.Instance.Keys.Length;i++)
        {
            if (LegrandBackend.Instance.Keys[i].x == key) return LegrandBackend.Instance.Keys[i].y;
        }
        return LegrandBackend.Instance.GetKeyName(key.ToString());
    }
}