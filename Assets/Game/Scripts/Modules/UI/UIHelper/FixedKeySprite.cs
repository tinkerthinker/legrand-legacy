﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FixedKeySprite : KeySpriteHandler
{
    public string KeyBoardName;
    public string ControllerName;

    public override void ChangeToKeyBoard()
    {
        if (!_Text) _Text = LegrandUtility.GetComponentInChildren<Text>(this.gameObject);
		_Text.text = KeyBoardName + (PrintKeyName? ")"+KeyName:"");
    }

    public override void ChangeToController()
    {
        if (!_Text) _Text = LegrandUtility.GetComponentInChildren<Text>(this.gameObject);
		_Text.text = ControllerName + (PrintKeyName? ")"+KeyName:"");
    }
}