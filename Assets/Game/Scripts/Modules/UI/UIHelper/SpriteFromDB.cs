﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SpriteFromDB : MonoBehaviour {
    public string Name;
	// Use this for initialization
	void Awake ()
    {
        Image image = LegrandUtility.GetComponentInChildren<Image>(this.gameObject);
        if (image != null)
            image.sprite = LegrandBackend.Instance.GetSpriteData(Name);
	}
}
