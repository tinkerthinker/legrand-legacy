﻿using UnityEngine;
using System.Collections;
using TeamUtility.IO;
using UnityEngine.UI;

public class ChangeableKeySprite : KeySpriteHandler {
    public string InputName;
    public override void ChangeToKeyBoard()
    {
        if(!_Text) _Text = LegrandUtility.GetComponentInChildren<Text>(this.gameObject);
		_Text.text = LegrandBackend.Instance.GetKeyName(InputManager.GetAxisConfiguration(0, InputName).positive.ToString()) + (PrintKeyName? ")"+KeyName:"");
    }

    public override void ChangeToController()
    {
        if (!_Text) _Text = LegrandUtility.GetComponentInChildren<Text>(this.gameObject);
		_Text.text = FindControllerName(InputManager.GetAxisConfiguration(0, InputName).positive) +     (PrintKeyName? ")"+KeyName:"");
    }
}
