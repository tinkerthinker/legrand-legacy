﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class UIController : MonoBehaviour {
	[SerializeField]
	GameObject MenuUI;
	[SerializeField]
	GameObject PauseUI;
	[SerializeField]
	GameObject ShopUI;
    [SerializeField]
    GameObject AlchemistUI;
    [SerializeField]
    GameObject CraftingUI;
    [SerializeField]
    GameObject FastTravelUI;
    [SerializeField]
    GameObject StorageUI;

    float _PreviousTimeScale;

	SoundUIController _SoundController;

	public LAppModelProxy LeftLiveTwoD;
	public LAppModelProxy RightLiveTwoD;

	// Use this for initialization
	void Awake () {
		EventManager.Instance.AddListener<MenuInGameEvent>(MenuInGameEvent);
		EventManager.Instance.AddListener<PauseEvent>(PauseEvent);
		EventManager.Instance.AddListener<UnPauseEvent>(UnPauseEvent);
		EventManager.Instance.AddListener<MonsterEncounterEvent>(MonsterEncounterEvent);
		EventManager.Instance.AddListener<ShopEvent>(ShopUIEvent);
        EventManager.Instance.AddListener<AlchemistShopEvent>(AlchemistUIEvent);
        EventManager.Instance.AddListener<CraftingShopEvent>(CraftingUIEvent);
        EventManager.Instance.AddListener<FastTravelUIEvent>(FastTravelEvent);
        EventManager.Instance.AddListener<StorageUIEvent>(StorageEvent);

        _SoundController = GetComponent<SoundUIController> ();

        StartCoroutine(RenderAllAtFirstTime());
	}

	void OnDestroy()
	{
		if (EventManager.Instance != null) {
			EventManager.Instance.RemoveListener<MenuInGameEvent> (MenuInGameEvent);
			EventManager.Instance.RemoveListener<PauseEvent> (PauseEvent);
			EventManager.Instance.RemoveListener<UnPauseEvent>(UnPauseEvent);
			EventManager.Instance.RemoveListener<MonsterEncounterEvent> (MonsterEncounterEvent);
			EventManager.Instance.RemoveListener<ShopEvent> (ShopUIEvent);
            EventManager.Instance.RemoveListener<AlchemistShopEvent>(AlchemistUIEvent);
            EventManager.Instance.RemoveListener<CraftingShopEvent>(CraftingUIEvent);
            EventManager.Instance.RemoveListener<FastTravelUIEvent>(FastTravelEvent);
            EventManager.Instance.RemoveListener<StorageUIEvent>(StorageEvent);
        }

		Time.timeScale = 1;
	}

	void Start () {
		if (MenuUI && MenuUI.activeInHierarchy)
			MenuUI.SetActive (false);
		if (PauseUI && PauseUI.activeInHierarchy)
			MenuUI.SetActive (false);
		if (ShopUI && ShopUI.activeInHierarchy)
			MenuUI.SetActive (false);
		_SoundController.enabled = false;
		SelectedGameObjectModule.current.HideMarkObject();
	}

	void LateUpdate()
	{
		if(EventSystem.current)
			if(EventSystem.current.currentSelectedGameObject && !EventSystem.current.currentSelectedGameObject.activeInHierarchy)
		   		EventSystem.current.SetSelectedGameObject(gameObject);
	}

    IEnumerator RenderAllAtFirstTime()
    {
        if(MenuUI)
            MenuUI.SetActive(true);
        if(PauseUI)
	        PauseUI.SetActive(true);
        if(ShopUI)
	        ShopUI.SetActive(true);
        if(AlchemistUI)
            AlchemistUI.SetActive(true);
        if(CraftingUI)
            CraftingUI.SetActive(true);
        if(FastTravelUI)
            FastTravelUI.SetActive(true);
        if(StorageUI)
            StorageUI.SetActive(true);

        yield return null;

        if (MenuUI)
            MenuUI.SetActive(false);
        if (PauseUI)
            PauseUI.SetActive(false);
        if (ShopUI)
            ShopUI.SetActive(false);
        if (AlchemistUI)
            AlchemistUI.SetActive(false);
        if (CraftingUI)
            CraftingUI.SetActive(false);
        if (FastTravelUI)
            FastTravelUI.SetActive(false);
        if (StorageUI)
            StorageUI.SetActive(false);
    }

	void SetTimeScale()
	{
		_PreviousTimeScale = Time.timeScale;
		Time.timeScale = 0;
	}

	void RestoreTimeScale()
	{
		Time.timeScale = _PreviousTimeScale;
	}

	void OpenUI(GameObject obj)
	{
        SetTimeScale();

		_SoundController.enabled = true;
		SelectedGameObjectModule.current.ShowMarkObject ();
        
        GlobalGameStatus.Instance.StateStatus[(int)GlobalGameStatus.GameState.OpenMenu] = true;

		LeftLiveTwoD.enabled = false;
		RightLiveTwoD.enabled = false;

		if(obj)
			obj.SetActive (true);
	}

	void CloseUI(GameObject obj)
	{
		_SoundController.enabled = false;
		SelectedGameObjectModule.current.HideMarkObject();

        if (EventSystem.current)
            EventSystem.current.SetSelectedGameObject(gameObject);

        GlobalGameStatus.Instance.StateStatus[(int)GlobalGameStatus.GameState.OpenMenu] = false;

		LeftLiveTwoD.enabled = true;
		RightLiveTwoD.enabled = true;
		LeftLiveTwoD.SetVisible(false);
		RightLiveTwoD.SetVisible(false);

		if(obj)
			obj.SetActive (false);

        RestoreTimeScale();
	}

	

	public void PauseEvent(PauseEvent p)
	{
		_SoundController.enabled = true;
		SelectedGameObjectModule.current.ShowMarkObject ();
	}

	public void UnPauseEvent(UnPauseEvent p)
	{
		_SoundController.enabled = false;
		SelectedGameObjectModule.current.HideMarkObject();
	}

	public void MonsterEncounterEvent(MonsterEncounterEvent p)
	{
		MenuUI.SetActive (false);
	}

    public void MenuInGameEvent(MenuInGameEvent p)
    {
        if (p.isOpen)
        {
            //SetTimeScale();

            OpenUI(MenuUI);
        }
        else
        {

            CloseUI(MenuUI);

            //RestoreTimeScale();
        }
    }

	public void ShopUIEvent(ShopEvent p)
	{
		if (p.isOpen)
		{
			ShopUI.GetComponentsInChildren<ShopController>(true)[0].selectedShopModelId = p.ShopModelId;
			OpenUI(ShopUI);

            //SetTimeScale();
		} else
		{
            //if(EventSystem.current)
            //    EventSystem.current.SetSelectedGameObject(gameObject);

			CloseUI(ShopUI);

            //RestoreTimeScale();
		}
	}

    public void AlchemistUIEvent(AlchemistShopEvent p)
	{
		if (p.isOpen)
		{
            AlchemistUI.GetComponent<AlchemistShopController>().Level = p.level;
			OpenUI(AlchemistUI);
		} else
		{
            //if(EventSystem.current)
            //    EventSystem.current.SetSelectedGameObject(gameObject);

			CloseUI(AlchemistUI);           
		}
	}

    public void CraftingUIEvent(CraftingShopEvent p)
    {
        if (p.isOpen)
        {
            CraftingUI.GetComponent<CraftingShopController>().Level = p.level;
            OpenUI(CraftingUI);
        }
        else
        {
            //if (EventSystem.current)
            //    EventSystem.current.SetSelectedGameObject(gameObject);

            CloseUI(CraftingUI);
        }
    }

    public void FastTravelEvent(FastTravelUIEvent p)
    {
        if (p.isOpen)
        {
            OpenUI(FastTravelUI);
        }
        else
        {
            CloseUI(FastTravelUI);
        }
    }

    public void StorageEvent(StorageUIEvent p)
    {
        if (p.isOpen)
        {
            OpenUI(StorageUI);
        }
        else
        {
            CloseUI(StorageUI);
        }
    }
}
