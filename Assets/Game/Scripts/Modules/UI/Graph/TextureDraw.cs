﻿using UnityEngine;
using System.Collections;

public class TextureDraw{
	public static void DrawLine(Texture2D tex, int x0, int y0, int x1, int y1, Color col)
	{
		int dy = (int)(y1-y0);
		int dx = (int)(x1-x0);
		int stepx, stepy;
		
		if (dy < 0) {dy = -dy; stepy = -1;}
		else {stepy = 1;}
		if (dx < 0) {dx = -dx; stepx = -1;}
		else {stepx = 1;}
		dy <<= 1;
		dx <<= 1;
		
		float fraction = 0;
		
		tex.SetPixel(x0, y0, col);
		if (dx > dy) {
			fraction = dy - (dx >> 1);
			while (Mathf.Abs(x0 - x1) > 1) {
				if (fraction >= 0) {
					y0 += stepy;
					fraction -= dx;
				}
				x0 += stepx;
				fraction += dy;
				tex.SetPixel(x0, y0, col);
			}
		}
		else {
			fraction = dx - (dy >> 1);
			while (Mathf.Abs(y0 - y1) > 1) {
				if (fraction >= 0) {
					x0 += stepx;
					fraction -= dy;
				}
				y0 += stepy;
				fraction += dx;
				tex.SetPixel(x0, y0, col);
			}
		}
	}



	public static void DrawBox(Texture2D tex, int x, int y, int width, int height, Color col)
	{
		for (int row = y-Mathf.RoundToInt(height/2); row<=y+Mathf.RoundToInt(height/2); row++) {
			DrawLine(tex, x-Mathf.RoundToInt(width/2), row, x+Mathf.RoundToInt(width/2), row, col);
		}
	}
}
