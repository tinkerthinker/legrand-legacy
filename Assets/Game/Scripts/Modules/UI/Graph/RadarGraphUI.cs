﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(RawImage))]
public class RadarGraphUI : MonoBehaviour {
	public enum DrawType
	{
		DrawGraph,
		DrawRadarBG,
		DrawGraphWithRadar
	}
	public DrawType drawType;
	
	public int maxValue;
	public List<int> districtValue;
	
	public Color colorFill;
	public Color colorLine;
	public bool isFillGraph=true;
	public bool graphWithBox=true;
	
	Texture2D texture;
	Color transp = Color.clear;
	Vector2 pivot;
	List<Vector2> coordFill = new List<Vector2>();

	private bool _IsReady;
	public bool IsReady
	{
		get {
			return _IsReady;
		}
	}
	
	void Start () 
	{
		if (texture == null)
			Init ();
		else
			return;


	}
	
	public void Init()
	{
		Rect rect = GetComponent<RectTransform> ().rect;
		
		texture = new Texture2D (Mathf.RoundToInt(rect.width), Mathf.RoundToInt(rect.height), TextureFormat.ARGB32, true);
		
		for (int row = 0; row<texture.height; row++) {
			for (int col = 0; col<texture.width; col++)
			{
				texture.SetPixel(col, row, transp);
			}
		}
		
		texture.Apply ();
		
		pivot.x = texture.width / 2;
		pivot.y = texture.height / 2;
		
		if (GetComponent<RawImage> () != null)
			GetComponent<RawImage> ().texture = texture;
		else if(GetComponent<Image>() != null)
			GetComponent<Image>().sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
		
		if (drawType == DrawType.DrawRadarBG) {
			UpdateGraph ();
		}
		_IsReady = true;
	}
	
	public void UpdateGraph()
	{
		if (texture == null)
			return;
		
		for (int row = 0; row<texture.height; row++) {
			for (int col = 0; col<texture.width; col++)
			{
				texture.SetPixel(col, row, transp);
			}
		}
		texture.Apply ();
		
		if (drawType == DrawType.DrawGraph)
			DrawGraph (graphWithBox);
		else if (drawType == DrawType.DrawRadarBG) {
			int index=0;
			foreach (int value in districtValue) {
				districtValue[index] = maxValue;
				index++;
			}
			
			DrawGraph(false);
			DrawGraphLine(false);
			DrawRadar ();
		}
		else if (drawType == DrawType.DrawGraphWithRadar) {
			DrawGraph(graphWithBox);
			DrawGraphLine(false);
			DrawRadar ();
		}
	}
	
	//Draw Graph with Fill process
	void DrawGraph(bool withBox)
	{
		int index=0;
		foreach (int value in districtValue) {
			districtValue[index] = Mathf.Clamp(value, 0, maxValue);
			
			index++;
		}
		
		DrawGraphLine (withBox);
		
		if (isFillGraph) {
			FillGraphFromCenter();
		}
		
		texture.Apply ();
	}
	
	//Draw Graph with Fill process
	void DrawGraphLine(bool withBox)
	{
		float degree = 360 / districtValue.Count;
		float radX = texture.width / 2;
		float radY = texture.height / 2;
		
		float rangeX0, rangeY0, rangeX1, rangeY1;
		int x0, y0,x1,y1;
		
		//draw graph line
		for (int i=1; i<districtValue.Count; i++) {
			//rangeblabla is a range a target point from center, min 0, max 1 then multiply it with diameter/2(radius)
			rangeX0 =((float)districtValue[i-1]/(float)maxValue) * radX;
			rangeY0 = ((float)districtValue[i-1]/(float)maxValue) * radY;
			
			rangeX1 = ((float)districtValue[i]/(float)maxValue) * radX;
			rangeY1 = ((float)districtValue[i]/(float)maxValue) * radY;
			
			for(int thick = -2 ; thick <=2;thick++)
			{
				x0 = Mathf.RoundToInt(pivot.x + Mathf.Sin ((((i-1)*degree) * Mathf.PI)/180) * (rangeX0+thick));
				y0 = Mathf.RoundToInt(pivot.y + Mathf.Cos ((((i-1)*degree) * Mathf.PI)/180) * (rangeY0+thick));
				
				x1 = Mathf.RoundToInt(pivot.x + Mathf.Sin (((i*degree) * Mathf.PI)/180) * (rangeX1+thick));
				y1 = Mathf.RoundToInt(pivot.y + Mathf.Cos (((i*degree) * Mathf.PI)/180) * (rangeY1+thick));
				
				TextureDraw.DrawLine (texture, x0, y0, x1, y1, colorLine);
				if(withBox)
					TextureDraw.DrawBox (texture, x0, y0, 10, 10, colorLine);
			}
		}
		//draw last line, connect last coordinate to first coordinate
		rangeX0 = ((float)districtValue[districtValue.Count-1]/(float)maxValue) * radX;
		rangeY0 = ((float)districtValue[districtValue.Count-1]/(float)maxValue) * radY;
		
		rangeX1 = ((float)districtValue[0]/(float)maxValue) * radX;
		rangeY1 = ((float)districtValue[0]/(float)maxValue) * radY;
		
		for (int thick = -2; thick <=2; thick++)
		{
			x0 = Mathf.RoundToInt (pivot.x + Mathf.Sin ((((districtValue.Count - 1) * degree) * Mathf.PI) / 180) * (rangeX0+thick));
			y0 = Mathf.RoundToInt (pivot.y + Mathf.Cos ((((districtValue.Count - 1) * degree) * Mathf.PI) / 180) * (rangeY0+thick));
			
			x1 = Mathf.RoundToInt (pivot.x + Mathf.Sin (((0 * degree) * Mathf.PI) / 180) * (rangeX1+thick));
			y1 = Mathf.RoundToInt (pivot.y + Mathf.Cos (((0 * degree) * Mathf.PI) / 180) * (rangeY1+thick));
			
			TextureDraw.DrawLine (texture, x0, y0, x1, y1, colorLine);
			if (withBox)
				TextureDraw.DrawBox (texture, x0, y0, 10, 10, colorLine);
		}
	}
	
	void FillGraphFromCenter()
	{
		int x = texture.width / 2;
		int y = texture.height / 2;
		FloodFill4ScanlineStack (x, y, ColorToUInt( colorFill ),ColorToUInt( texture.GetPixel (x, y) ) );
	}
	
	void FloodFill4ScanlineStack(int x, int y, uint newColorUint, uint oldColor)
	{
		if(newColorUint == oldColor) return; //avoid infinite loop
		coordFill.RemoveRange (0, coordFill.Count);
		
		int y1; 
		bool spanLeft, spanRight;
		
		if(coordFill.Contains(new Vector2(x,y))) return; 
		coordFill.Add (new Vector2 (x, y));
		
		int w = texture.width;
		int h = texture.height;
		
		byte[] rawTexture = texture.GetRawTextureData();
		
		while(coordFill.Count > 0)
		{
			int fillX, fillY;
			fillX = (int) coordFill[coordFill.Count-1].x;
			fillY = (int) coordFill[coordFill.Count-1].y;
			coordFill.RemoveAt(coordFill.Count-1);
			
			y1 = fillY;
			
			while(y1 >= 0 && ColorToUInt( texture.GetPixel(fillX, y1)) == oldColor) y1--;	//search for the most left pixel in row
			y1++;
			spanLeft = spanRight = false;
			
			while(y1 < h && ColorToUInt( texture.GetPixel(fillX, y1)) == oldColor )
			{
				texture.SetPixel(fillX, y1, UIntToColor(newColorUint) );
				if(!spanLeft && fillX > 0 && ColorToUInt( texture.GetPixel(fillX - 1, y1)) == oldColor) 
				{
					if(!coordFill.Contains(new Vector2(fillX - 1, y1)))
					{
						coordFill.Add (new Vector2 (fillX - 1, y1));
						spanLeft = true;
					}
				}
				else if(spanLeft && fillX > 0 && ColorToUInt( texture.GetPixel(fillX - 1, y1)) != oldColor)
				{
					spanLeft = false;
				}
				if(!spanRight && fillX < w - 1 && ColorToUInt( texture.GetPixel(fillX + 1, y1)) == oldColor) 
				{
					if(!coordFill.Contains(new Vector2(fillX + 1, y1)))
					{
						coordFill.Add (new Vector2 (fillX + 1, y1));
						spanRight = true;
					}
				}
				else if(spanRight && fillX < w - 1 && ColorToUInt( texture.GetPixel(fillX + 1, y1)) != oldColor)
				{
					spanRight = false;
				} 
				y1++;
			}
		}
	}
	
	private uint ColorToUInt(Color color)
	{
		return (uint)(( ((uint)(color.a*255)) << 24) | ( ((uint)(color.r*255)) << 16) |
		              ( ((uint)(color.g*255)) << 8)  | ( ((uint)(color.b*255)) << 0));
	}
	
	private Color UIntToColor(uint color)
	{
		uint a = ((color >> 24) & 255);
		uint r = ((color >> 16) & 255);
		uint g = ((color >> 8) & 255);
		uint b = ((color >> 0) & 255);
		return new Color((float)r/255f, (float)g/255f, (float)b/255f,(float)a/255f);
	}
	
	void DrawRadar()
	{
		float degree = 360 / districtValue.Count;
		
		int x0, y0,x1,y1;
		x0 = (int)pivot.x;
		y0 = (int)pivot.y;
		
		//draw radar line
		for (int i=0; i<districtValue.Count; i++) {
			x1 = Mathf.RoundToInt(pivot.x + Mathf.Sin (((i*degree) * Mathf.PI)/180) * pivot.x);
			y1 = Mathf.RoundToInt(pivot.y + Mathf.Cos (((i*degree) * Mathf.PI)/180) * pivot.y);
			
			TextureDraw.DrawLine (texture, x0, y0, x1, y1, colorLine);
		}
		
		texture.Apply ();
	}
}