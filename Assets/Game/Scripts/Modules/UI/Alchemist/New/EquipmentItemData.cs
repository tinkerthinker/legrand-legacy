﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;
using System;
using Legrand.core;

public class EquipmentItemData : MonoBehaviour, ISelectHandler, IItem
{
    public Image Icon;
    public Text Name;
    public Text Price;

    public Image IconItem;
    public Text DetailItem;
    public Text ItemName;

    BluePrint blueprint;

    public GameObject CombineControllerObj;
    private ICombineBlueprintShopController CombineController;
    public GameObject EquipmentSetAttributeObj;
    private IEquipmentSetAttribute EquipmentSetAttribute;

    void Awake()
    {
        CombineController = CombineControllerObj.GetComponent<ICombineBlueprintShopController>();
        EquipmentSetAttribute = EquipmentSetAttributeObj.GetComponent<IEquipmentSetAttribute>();
    }

    public void OnSelect(BaseEventData eventData)
    {
        RefreshDetail();
    }

    public void RefreshDetail()
    {
        //SetPreviewData();
        //EquipmentSetAttribute.SetAttribute(blueprint);
        //CombineController.SetRecipeList(blueprint);
    }

    void SetPreviewData()
    {
        if (IconItem)
            IconItem.sprite = LegrandBackend.Instance.GetEquipment(blueprint.ResultItemID).Picture;
        if (DetailItem)
            DetailItem.text = LegrandBackend.Instance.GetEquipment(blueprint.ResultItemID).Description;
        if (ItemName)
            ItemName.text = LegrandBackend.Instance.GetEquipment(blueprint.ResultItemID).Name;
    }

    public void CombineAndCreate()
    {
        CombineController.CombineAndCreate(blueprint, GetComponent<NavigationButtonData>());
    }

    public void SetData(object obj)
    {
        BluePrint bp = (BluePrint)obj;
        blueprint = bp;

        EquipmentDataModel equip = LegrandBackend.Instance.GetEquipment(bp.ResultItemID);

        if (Icon)
            Icon.sprite = equip.Picture;
        if (Name)
            Name.text = equip.Name;
        if (Price)
            Price.text = bp.Price.ToString();
    }
}
