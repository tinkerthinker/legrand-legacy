﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Legrand.core;

public class RecipeItemHandler : MonoBehaviour
{
    public Image Icon;
    public Text Name;
    public Text AmountDetail;

    public void SetThis(bool state)
    {
        gameObject.SetActive(state);
    }

    public void SetItem(Item item,int createAmount)
    {
        Icon.sprite = item.Icon;
        Name.text = item.Name;
        int stack = PartyManager.Instance.Inventory.GetStackItem(item);
        AmountDetail.text = stack.ToString() + " / " + createAmount.ToString();
        if (stack > 0 || stack > createAmount)
            AmountDetail.color = Color.white;
        else
            AmountDetail.color = Color.red;
    }

    public void SetItem(Sprite icon,string name,int use,int total)
    {
        Icon.sprite = icon;
        Name.text = name;
        AmountDetail.text = total.ToString() + " / " + use.ToString();
        if (total > 0 || total > use)
            AmountDetail.color = Color.white;
        else
            AmountDetail.color = Color.red;
    }
}
