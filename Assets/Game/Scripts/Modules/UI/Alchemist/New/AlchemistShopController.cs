﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;
using UnityEngine.UI;
using TeamUtility.IO;
using UnityEngine.EventSystems;
using System;

public class AlchemistShopController : MonoBehaviour
{
    public GenerateEnhanceRecipe GenerateEnchance;
    public ShowInventoryMiscData InventoryMiscData;
    public ExecuteAlchemistBlueprintController ExecuteController;

    public int Level;
    public Text LevelText;
    public Color[] LevelColor;

    public Animator AlchemyAnimator;
    private bool _InStateTransition = false;

    
    void OnEnable()
    {
        if (LevelText)
        {
            LevelText.text = LegrandBackend.Instance.GetLanguageData("MercLevel" + Level, true);
            if (Level > LevelColor.Length)
                LevelText.color = LevelColor[LevelColor.Length - 1];
            else if (Level < 0)
                LevelText.color = LevelColor[0];
            else
                LevelText.color = LevelColor[Level - 1];
        }
        GenerateEnchance.MerchantLevel = Level;
        GenerateEnchance.UpdateList();
        StartCoroutine(WaitTransitionToFocus());
        GenerateEnchance.gameObject.SetActive(true);
        InventoryMiscData.UpdateAll();
    }

    IEnumerator WaitTransitionToFocus()
    {
        StateTransitionStarted();
        while (_InStateTransition)
        {
            yield return null;
        }
        GenerateEnchance.TryToFocusFirstButton();
    }

    public void Close()
    {
        if (ExecuteController.CanExit())
        {
            StartCoroutine(WaitCloseMenuTransition());
        }
    }

    public void StateTransitionStarted()
    {
        _InStateTransition = true;
    }

    public void StateTransitionCompleted()
    {
        _InStateTransition = false;
    }

    IEnumerator WaitCloseMenuTransition()
    {
        EventSystem.current.SetSelectedGameObject(gameObject);
        AlchemyAnimator.SetTrigger("CloseMenu");
        StateTransitionStarted();
        while (_InStateTransition)
        {
            yield return null;
        }
        EventManager.Instance.TriggerEvent(new AlchemistShopEvent(false, Level));
    }
    
}
