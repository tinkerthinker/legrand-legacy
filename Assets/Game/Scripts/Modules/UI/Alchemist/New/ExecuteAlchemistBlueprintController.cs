﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Legrand.core;

public class ExecuteAlchemistBlueprintController : MonoBehaviour
{
    int _ItemAmount;
    int _MaxItem;
    BluePrint _SelectedBlueprint;
    NavigationButtonData _Navigation;
    public ShowInventoryMiscData InventoryMiscData;
    public InspectCombineAlchemistHandler Recipehandler;
    public GenerateEnhanceRecipe GeneratorBP;

    public Vector2 AddPosition;
    public GameObject AmountPanel;
    public GameObject ResultPanel;
    public Image ResultImage;
    public Text ResultText;
    public Image AmountItemIcon;
    public Text AmountItemName;
    public Text AmountGold;
    public Text AmountWeight;
    public RecipeItemHandler[] RecipeItem;
    bool _ShowAmount;
    bool _ShowResult;

    public bool CanExit()
    {
        return (!_ShowAmount && !_ShowResult) ? true : false;
    }
    void SetAmountEventHandler(SetAmountEvent e)
    {
        _ItemAmount = e.Amount;
        AmountGold.text = (_SelectedBlueprint.Price * _ItemAmount).ToString();
        AmountWeight.text = (LegrandBackend.Instance.ItemData[_SelectedBlueprint.ResultItemID].Weight * _ItemAmount).ToString();
        Recipehandler.UpdateRecipes(_SelectedBlueprint, _ItemAmount, RecipeItem);
    }
    void ShowAmountPanelFrame(bool state)
    {
        _ShowAmount = state;
        AmountPanel.SetActive(state);
        if (state)
        {
            AmountItemIcon.sprite = LegrandBackend.Instance.ItemData[_SelectedBlueprint.ResultItemID].Icon;
            AmountItemName.text = LegrandBackend.Instance.ItemData[_SelectedBlueprint.ResultItemID].Name;
        }
    }

    public void CancelCreate()
    {
        if(_ShowAmount)
        {
            ShowAmountPanelFrame(false);
            PopUpUI.CloseAllPopUp();
            EventManager.Instance.RemoveListener<SetAmountEvent>(SetAmountEventHandler);
            SelectedGameObjectModule.current.ShowMarkObject();
            _ShowAmount = false;
            _Navigation.ChangeToLastNavigation();
        }
    }

    public void Execute(BluePrint bp, NavigationButtonData navi)
    {
        if(_ShowResult)
        {
            SetResultPanel(false);
            _ShowResult = false;
            GeneratorBP.UpdateList();

            SelectedGameObjectModule.current.ShowMarkObject();
        }
        else if (_ShowAmount)
        {
            ShowAmountPanelFrame(false);
            Execute();
            _ShowAmount = false;
        }
        else
        {
            if (bp.TryCombine())
            {
                _SelectedBlueprint = bp;
                GetMaxItem();
                if (_MaxItem > 0)
                {
                    if (PartyManager.Instance.Inventory.CurrentWeight + _MaxItem * LegrandBackend.Instance.ItemData[bp.ResultItemID].Weight < PartyManager.Instance.Inventory.MaxWeight)
                    {
                        _SelectedBlueprint = bp;
                        _Navigation = navi;
                        _Navigation.ChangeToNone();

                        SelectedGameObjectModule.current.HideMarkObject();
                        EventManager.Instance.AddListener<SetAmountEvent>(SetAmountEventHandler);
                        PopUpUI.CallSetAmountPopUp(_MaxItem, null, AddPosition);

                        ShowAmountPanelFrame(true);
                        Recipehandler.UpdateRecipes(_SelectedBlueprint, _ItemAmount, RecipeItem);
                    }
                    else
                    {
                        PopUpUI.CallNotifPopUp("Not Enough Space in Inventory");
                    }
                }
                else
                {
                    PopUpUI.CallNotifPopUp("Insufficient Danaar");
                }
            }
            else
            {
                PopUpUI.CallNotifPopUp("Not enough material requirement");
            }
        }
    }

    void CreateThis()
    {
        Execute();
        InventoryMiscData.UpdateAll();
        _Navigation.ChangeToLastNavigation();
    }

    void GetMaxItem()
    {
        int minValue = int.MaxValue;
        for (int i = 0; i < _SelectedBlueprint.RequiredItems.Count; i++)
        {
            int stack = PartyManager.Instance.Inventory.GetStackItem(LegrandBackend.Instance.ItemData[_SelectedBlueprint.RequiredItems[i].AllItemID]);
            int max = stack / _SelectedBlueprint.RequiredItems[i].Amount;
            if (minValue > max)
                minValue = max;
        }
        _MaxItem = minValue;
        for (int p = _MaxItem; p >= 0; p--)
        {
            if (p * _SelectedBlueprint.Price > PartyManager.Instance.Inventory.Gold)
            {
                _MaxItem = p - 1;
            }
            else
            {
                _MaxItem = p;
                break;
            }
        }
    }

    void Execute()
    {
        Item result = LegrandBackend.Instance.ItemData[_SelectedBlueprint.ResultItemID];
        if (PartyManager.Instance.Inventory.AddItems(result, _ItemAmount))
        {
            for (int i = 0; i < _SelectedBlueprint.RequiredItems.Count; i++)
            {
                PartyManager.Instance.Inventory.DeleteItemsByItemID(_SelectedBlueprint.RequiredItems[i].AllItemID, _SelectedBlueprint.RequiredItems[i].Amount * _ItemAmount);
            }
            PartyManager.Instance.Inventory.Gold = PartyManager.Instance.Inventory.Gold - (_ItemAmount * _SelectedBlueprint.Price);
            
            SetResultPanel(true,result);
        }
        else
            Debug.Log("Fail To Add Item");
    }

    void SetResultPanel(bool state, Item result = null)
    {
        _ShowResult = state;
        ResultPanel.SetActive(state);
        if(state && result != null)
        {
            PopUpUI.CloseAllPopUp();
            EventManager.Instance.RemoveListener<SetAmountEvent>(SetAmountEventHandler);
            ResultImage.sprite = result.Icon;
            ResultText.text = _ItemAmount.ToString() + " " + result.Name + " Received";

            _ItemAmount = 0;
            EventManager.Instance.TriggerEvent(new InspectCombineAlchemistEvent(result, _SelectedBlueprint, 1));
        }
        if(!state)
        {
            _Navigation.ChangeToLastNavigation();
        }
    }
}
