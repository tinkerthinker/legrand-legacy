﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Legrand.core;

public class BlueprintAlchemistData : MonoBehaviour, IObjectData
{
    public Image IconImage;
    public GameObject ImpossibleIcon;
    public Text NameText;
    public Text PriceText;
    public Text OwnText;
    public Text AOEText;

    BluePrint _BluePrint;
    Item _ResultItem;

    public int CreateAmountPreview = 1;

    public ExecuteAlchemistBlueprintController ExecuteBlueprint;

    NavigationButtonData _Navigation;
    private bool _IsPossibleToCraft;

    void Awake()
    {
        _Navigation = GetComponent<NavigationButtonData>();
    }

    public void SetData(object obj)
    {
        _BluePrint = (BluePrint)obj;
        _ResultItem = LegrandBackend.Instance.ItemData[_BluePrint.ResultItemID];

        if (IconImage)
            IconImage.sprite = _ResultItem.Icon;
        if (NameText)
            NameText.text = _ResultItem.Name;
        if (PriceText)
            PriceText.text = _BluePrint.Price.ToString("#,#");
        if (OwnText)
            OwnText.text = PartyManager.Instance.Inventory.GetStackItem(LegrandBackend.Instance.ItemData[_BluePrint.ResultItemID]).ToString();
        if(AOEText)
        {
            if (_ResultItem.ItemType == Item.Type.Consumable &&
                (((Consumable)_ResultItem).ConsumableType == Consumable.ConsumeType.Healing ||
                    ((Consumable)_ResultItem).ConsumableType == Consumable.ConsumeType.Magic))
            {
                AOEText.text = ((Consumable)_ResultItem).Target.ToString().Substring(0, 1);
            }
            else
                AOEText.text = "";
        }
    }

    public object GetData()
    {
        return _ResultItem;
    }

    public void RefreshData()
    {
        SetData(_ResultItem);
    }

    public void SetPossibleToCraft(bool isPossible)
    {
        _IsPossibleToCraft = isPossible;
        if (ImpossibleIcon)
        {
            if (isPossible)
                ImpossibleIcon.gameObject.SetActive(false);
            else
                ImpossibleIcon.gameObject.SetActive(true);
        }
    }

    public void OnSelect(UnityEngine.EventSystems.BaseEventData eventData)
    {
        EventManager.Instance.TriggerEvent(new InspectCombineAlchemistEvent(_ResultItem, _BluePrint, CreateAmountPreview));
    }

    public void CreateBluePrint()
    {
        if (_IsPossibleToCraft)
            ExecuteBlueprint.Execute(_BluePrint, _Navigation);
        else
            PopUpUI.CallNotifPopUp("This Merchant can't make this item");
    }

    public void CancelCreate()
    {
        ExecuteBlueprint.CancelCreate();
    }
}
