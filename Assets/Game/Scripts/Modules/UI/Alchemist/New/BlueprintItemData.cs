﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;
using System;
using Legrand.core;

public class BlueprintItemData : MonoBehaviour, ISelectHandler, IItem
{
    public Image Icon;
    public Text Name;
    public Text Weight;
    public Text Owned;
    public Text Price;

    public Image IconItem;
    public Text NameItem;
    public Text DetailItem;
    public Text EffectItem;
    public Text TargetItem;
    public Text WeightItem;

    BluePrint blueprint;

    public GameObject ICombineController;
    ICombineBlueprintShopController CombineController;

    void Awake()
    {
        CombineController = ICombineController.GetComponent<ICombineBlueprintShopController>();
    }
    
    public void OnSelect(BaseEventData eventData)
    {
        SetPreviewData();
        //CombineController.SetRecipeList(blueprint);
    }

    public void CombineAndCreate()
    {
        CombineController.CombineAndCreate(blueprint,GetComponent<NavigationButtonData>());
    }

    void SetPreviewData()
    {
        if(NameItem)
            NameItem.text = LegrandBackend.Instance.ItemData[blueprint.ResultItemID].Name;
        if (IconItem)
            IconItem.sprite = LegrandBackend.Instance.ItemData[blueprint.ResultItemID].Icon;
        if(DetailItem)
            DetailItem.text = blueprint.Description;
        if(EffectItem)
            EffectItem.text = LegrandBackend.Instance.ItemData[blueprint.ResultItemID].Description;
        if (WeightItem)
            WeightItem.text = LegrandBackend.Instance.ItemData[blueprint.ResultItemID].Weight.ToString();
        if (TargetItem)
        {
            Consumable conItem = (Consumable)LegrandBackend.Instance.ItemData[blueprint.ResultItemID];
            TargetItem.text = "Target " + conItem.Target.ToString();
        }
    }

    public void SetData(object obj)
    {
        BluePrint bp = (BluePrint)obj;
        blueprint = bp;
        if (LegrandBackend.Instance.ItemData.ContainsKey(blueprint.ResultItemID))
        {
            if (Icon)
                Icon.sprite = LegrandBackend.Instance.ItemData[blueprint.ResultItemID].Icon;
            if (Name)
                Name.text = LegrandBackend.Instance.ItemData[blueprint.ResultItemID].Name.ToString();
            if (Weight)
                Weight.text = LegrandBackend.Instance.ItemData[blueprint.ResultItemID].Weight.ToString();
            if (Price)
                Price.text = bp.Price.ToString();
            if (Owned)
            {
                ItemSlot item = PartyManager.Instance.Inventory.GetItem(blueprint.ResultItemID);
                Owned.text = item != null ? item.Stack.ToString() : "0";
            }
        }
    }
    
    public void RefreshDetail()
    {
        SetPreviewData();
    }
}
