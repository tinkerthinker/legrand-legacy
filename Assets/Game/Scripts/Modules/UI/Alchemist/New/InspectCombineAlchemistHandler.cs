﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Legrand.core;

public class InspectCombineAlchemistHandler : MonoBehaviour
{
    public GameObject Holder;
    public Image Icon;
    public Text NameText;
    public Text Detail;
    public Text Target;
    public Text Effect;
    public Text Weight;
    public Text LevelRecipeText;
    public Color[] LevelColor;

    public RecipeItemHandler[] RecipeItemHandler;

    private BluePrint _SelectedBlueprint;

    void OnEnable()
    {
        EventManager.Instance.AddListener<InspectCombineAlchemistEvent>(InspectEquipmentRecipeHandler);
    }

    void OnDisable()
    {
        if (EventManager.Instance)
            EventManager.Instance.RemoveListener<InspectCombineAlchemistEvent>(InspectEquipmentRecipeHandler);
    }

    void Start()
    {

    }

    void InspectEquipmentRecipeHandler(InspectCombineAlchemistEvent e)
    {
        if (e.Blueprint == null || e.Item == null || !e.IsShown)
        {
            Holder.SetActive(false);
            return;
        }
        else
            Holder.SetActive(true);
        _SelectedBlueprint = e.Blueprint;

        if (Icon)
            Icon.sprite = e.Item.Icon;
        if (NameText)
            NameText.text = e.Item.Name;
        if (Detail)
            Detail.text = e.Item.Description;
        if (Target)
            Target.text = ((Consumable)e.Item).Target.ToString();
        if (Weight)
            Weight.text = e.Item.Weight.ToString() ;
        if (LevelRecipeText)
        {
            LevelRecipeText.text = LegrandBackend.Instance.GetLanguageData("BPLevel" + e.Blueprint.Level, true);
            Color col;
            if (e.Blueprint.Level > LevelColor.Length)
                col = LevelColor[LevelColor.Length - 1];
            else if (e.Blueprint.Level < 0)
                col = LevelColor[0];
            else
                col = LevelColor[e.Blueprint.Level - 1];
            LevelRecipeText.color = col;
        }
        
        UpdateRecipes(e.Blueprint, e.CreateAmount, RecipeItemHandler);
    }

    public void UpdateRecipes(BluePrint blueprint, int createAmount, RecipeItemHandler[] Recipe)
    {
        for (int i = 0; i < Recipe.Length; i++)
        {
            if (i < blueprint.RequiredItems.Count)
            {
                Recipe[i].SetThis(true);
                Recipe[i].SetItem(LegrandBackend.Instance.ItemData[blueprint.RequiredItems[i].AllItemID], createAmount * blueprint.RequiredItems[i].Amount);
            }
            else
            {
                Recipe[i].SetThis(false);
            }

        }
    }
}
