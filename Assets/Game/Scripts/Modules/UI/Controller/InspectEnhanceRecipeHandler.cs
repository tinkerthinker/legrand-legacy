﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;

public class InspectEnhanceRecipeHandler : MonoBehaviour {
    [System.Serializable]
    public class ObjectAttribute
    {
        public Attribute Attribute;
        public Text ValueText;
        public Text SimulateValueText;
        public Image SimulateArrow;
    }

    public GameObject DetailHolder;
    public Text NameText;
    public Image RecipeImage;
    public Text DescriptionText;
    public Text LevelRecipeText;
    public Color[] LevelColor;
    public Color DefaultColor = new Color(0,0,0);
    public Color ProfitColor = new Color(0, 0, 1);
    public Color DefisitColor = new Color(1, 0, 0);
    public bool IsArrowDefaultTop;

    public List<ObjectAttribute> ObjectAttributes;

    public RecipeItemHandler[] RecipeItemHandler;

    private BluePrint _SelectedBlueprint;

    void OnEnable()
    {
        EventManager.Instance.AddListener<InspectEnhanceRecipeEvent>(InspectEquipmentRecipeHandler);
    }

    void OnDisable()
    {
        if(EventManager.Instance)
            EventManager.Instance.RemoveListener<InspectEnhanceRecipeEvent>(InspectEquipmentRecipeHandler);
    }

    void Start()
    {
        
    }

    void InspectEquipmentRecipeHandler(InspectEnhanceRecipeEvent e)
    {
        if (e.Blueprint == null || e.Equip == null || !e.IsShown)
        {
            if (DetailHolder)
                DetailHolder.SetActive(false);
            _SelectedBlueprint = null;
            return;
        }
        else
        {
            if (e.Blueprint == null)
                _SelectedBlueprint = LegrandBackend.Instance.GetBlueprintByResultItem(e.Equip.ID);
            else
                _SelectedBlueprint = e.Blueprint;
        }        

        if (DetailHolder)
            DetailHolder.SetActive(true);
        if (NameText)
            NameText.text = e.Equip.Name;
        if (LevelRecipeText)
        {
            LevelRecipeText.text = LegrandBackend.Instance.GetLanguageData("BPLevel"+e.Blueprint.Level, true);
            Color col;
            if (e.Blueprint.Level > LevelColor.Length)
                col = LevelColor[LevelColor.Length-1];
            else if (e.Blueprint.Level < 0)
                col = LevelColor[0];
            else
                col = LevelColor[e.Blueprint.Level - 1];
            LevelRecipeText.color = col;
        }
        if (RecipeImage)
            RecipeImage.sprite = e.Equip.Picture;
        if (DescriptionText)
            DescriptionText.text = e.Equip.Description;

        UpdateAttribute(e.Equip);
        UpdateRecipes(_SelectedBlueprint,e.CreateAmount);
    }

    void UpdateRecipes(BluePrint blueprint,int createAmount)
    {
        for(int i=0;i<RecipeItemHandler.Length;i++)
        {
            if (i < blueprint.RequiredItems.Count)
            {
                RecipeItemHandler[i].SetThis(true);
                RecipeItemHandler[i].SetItem(LegrandBackend.Instance.ItemData[blueprint.RequiredItems[i].AllItemID], createAmount * blueprint.RequiredItems[i].Amount);
            }
            else
            {
                RecipeItemHandler[i].SetThis(false);
            }

        }
    }

    void UpdateAttribute(EquipmentDataModel equip)
    {
        if (_SelectedBlueprint == null)
            return;
        MainCharacter chara = PartyManager.Instance.GetCharacter(_SelectedBlueprint.CharacterID);
        if (chara == null)
            return;
        Equipments currEquip = chara.Equipment();

        float currValue;
        float recipeValue;

        foreach (ObjectAttribute oa in ObjectAttributes)
        {
            currValue = 0;
            //currValue = chara.Equipments.GetEquipmentAttribute(oa.Attribute);
            switch (oa.Attribute)
            {
                case Attribute.Health:
                    currValue = chara.Health.MaxValue + currEquip.GetEquipmentAttribute(Attribute.Health);
                break;
                case Attribute.PhysicalAttack:
                    //currValue = (float)Formula.CalculateTotalDamage(chara) + (e.Equip.Attributes.Find(x => x.AttributeType == Attribute.PhysicalAttack).value - currEquip.GetEquipmentAttribute(Attribute.PhysicalAttack) );
                    currValue = (float)Formula.CalculateTotalDamage(chara);
                break;
                case Attribute.PhysicalDefense:
                    currValue = Formula.CalculatePhysicalDefense(chara);
                break;
                case Attribute.MagicAttack:
                    currValue = Formula.CalculateINTMagicDamage(chara);
                break;
                case Attribute.MagicDefense:
                    currValue = Formula.CalculateMagicDefense(chara);
                break;
                case Attribute.EvasionRate:
                    currValue = Formula.GetEvaRate(chara);
                break;
                case Attribute.CriticalRate:
                    currValue = Formula.GetCritRate(chara);
                break;
                case Attribute.Accuracy:
                    currValue = Formula.GetAccRate(chara);
                break;
            }
            //currValue = (float)Formula.CalculateTotalDamage(chara) + (selected recipe attribute - current equipment attribute); jika (seleceted recipe - current equipment) < 0, berarti selected recipe lebih lemah attribute nya

            AttributeValue attribValue = equip.Attributes.Find(x => x.AttributeType == oa.Attribute);

            recipeValue = currValue + ((attribValue != null ? attribValue.value : 0) - currEquip.GetEquipmentAttribute(oa.Attribute));

            if (oa.SimulateValueText != null)
            {
                oa.ValueText.text = currValue.ToString("#,#.##");
                oa.SimulateValueText.text = recipeValue.ToString("#,#.##");

                if (recipeValue > currValue)
                {
                    oa.SimulateValueText.gameObject.SetActive(true);
                    oa.SimulateValueText.color = ProfitColor;

                    if (oa.SimulateArrow)
                    {
                        SetArrow(oa.SimulateArrow, true);
                    }
                }
                else if (recipeValue < currValue)
                {
                    oa.SimulateValueText.gameObject.SetActive(true);
                    oa.SimulateValueText.color = DefisitColor;

                    if (oa.SimulateArrow)
                    {
                        SetArrow(oa.SimulateArrow, false);
                    }
                }
                else
                {
                    oa.SimulateValueText.gameObject.SetActive(false);
                    oa.SimulateValueText.color = DefaultColor;
                }
            }
            else
            {
                oa.ValueText.text = recipeValue.ToString("#,#.##");

                if (recipeValue > currValue)
                {
                    oa.ValueText.color = ProfitColor;

                    if (oa.SimulateArrow)
                    {
                        SetArrow(oa.SimulateArrow, true);
                    }
                }
                else if (recipeValue < currValue)
                {
                    oa.ValueText.color = DefisitColor;

                    if (oa.SimulateArrow)
                    {
                        SetArrow(oa.SimulateArrow, false);
                    }
                }
                else
                {
                    oa.ValueText.color = DefaultColor;
                }
            }
        }
    }

    void SetArrow(Image arrow, bool isProfit)
    {
        //Vector3 temp = arrow.transform.localScale;
        Quaternion tq = arrow.transform.rotation;
        Vector3 temp;

        if (arrow.GetComponent<PingPongMoveOverTime>() != null)
        {
            temp = arrow.GetComponent<PingPongMoveOverTime>().AddLocalPosition;
            if (isProfit)
            {
                temp.y = temp.y > 0?  temp.y : -1 * temp.y;
            }
            else
            {
                temp.y = temp.y < 0?  temp.y : -1 * temp.y;
            }

            arrow.GetComponent<PingPongMoveOverTime>().AddLocalPosition = temp;
            arrow.gameObject.SetActive(false);
            arrow.gameObject.SetActive(true);
            //arrow.GetComponent<PingPongMoveOverTime>().Reset();
        }


        temp = tq.eulerAngles;
        if (isProfit)
        {
            if (IsArrowDefaultTop)
            {
                //temp.y = temp.y > 0? temp.y : -1 * temp.y;
                temp.z = 0;
            }
            else
            {
                //temp.y = temp.y < 0 ? temp.y : -1 * temp.y;
                temp.z = 180;
            }
            arrow.color = ProfitColor;
        }
        else
        {
            if (IsArrowDefaultTop)
            {
                //temp.y = temp.y < 0 ? temp.y : -1 * temp.y;
                temp.z = 180;
            }
            else
            {
                //temp.y = temp.y > 0 ? temp.y : -1 * temp.y;
                temp.z = 0;
            }
            arrow.color = DefisitColor;
        }
        //arrow.transform.localScale = temp;
        tq.eulerAngles = temp;
        arrow.transform.rotation = tq;

        arrow.gameObject.SetActive(true);
    }
}
