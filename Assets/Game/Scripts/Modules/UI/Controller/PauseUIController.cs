using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;
using TeamUtility.IO;

public class PauseUIController : MonoBehaviour {
	public enum PauseUIControllerState
	{
		MainMenu,
		Status,
		Command,
		MagicPreset,
		Equipment,
		Inventory,
		QuestLog,
		BattleFormation,
		Item,
		MagicTweaker,
		MagicRename,
		AssignCommand,
        SaveLoad
	}
    [System.NonSerialized]
	public PauseUIControllerState PauseUIState;

	private static PauseUIController s_Instance = null;
	public static PauseUIController Instance
	{
		get
		{
			if (s_Instance == null)
			{
				s_Instance = GameObject.FindObjectOfType(typeof(PauseUIController)) as PauseUIController;
			}
			return s_Instance;
		}
	}

	[System.NonSerialized]
	public bool isFirstTimeLaunch=true;
	private bool isLocked;
	public Dictionary<string, string> AvailableCharName;

	private List<string> _ActiveMembers = new List<string>();
	private List<string> _ReserveMembers = new List<string>();

	public List<string> ActiveMembers
	{
		get{return _ActiveMembers; }
	}
	public List<string> ReserveMembers
	{
		get{return _ReserveMembers; }
	}

	private string _selectedCharId="";
	public string selectedCharId{
		set
		{
			_selectedCharId = value;
			EventManager.Instance.TriggerEvent(new ChangeUICharSelectedID(_selectedCharId));
		}
		get
		{
			return _selectedCharId;
		}
	}

	public PanelInventoryController PanelInventoryController;
	public PanelCharacterController PanelCharacterController;
	
	#region variabel panel
	public GameObject PanelMain;
	public GameObject PanelCharacter;
    public GameObject PanelReserveCharacter;
	public GameObject PanelBattleFormation;
	public GameObject PanelAssignMgcSkill;
	public GameObject PanelStatusChar;
	public GameObject PanelInventory;
	public GameObject PanelJournal;
    public GameObject PanelSaveLoad;
	#endregion

	#region button panel main
    public Button CharTemplate;
	public Button InventoryButton;
	public Button CharacterButton;
	public Button AssignButton;
	public Button FormationButton;
	public Button QuestLogButton;
	public Button SaveLoadButton;
	public Button QuitGameButton;
	#endregion

	#region Battle Formation Parameter
	private string[,] _assignedFormation = new string[2,3];
	public string[,] AssignedFormation{
		get{ return _assignedFormation;}
	}
	/*
	[System.NonSerialized]
	public string AssigningCharId;
	[System.NonSerialized]
	public bool IsArrangeFormation = false;
	[System.NonSerialized]
	public bool CancelToPanelChar = false;
	[System.NonSerialized]
	public string TempRemovedCharId="";
	//public string tempRemovedCharId="-1";
	[System.NonSerialized]
	public int[] TempPositionRemovedChar = new int[2];
	
	private bool canCancelAssign=false;
	*/
	private BattleFormations battleFormation;
	public BattleFormations BattleFormation
	{
		get{return battleFormation;}
	}
	#endregion

	#region Inventory Parameter
	[System.NonSerialized]
	public Item currInventoryItem;
	#endregion

	public List<Camera> live2DCamera;
	List<Camera> usedLive2DCameras;

    private List<SelectCharacter> _SelectCharacterList = new List<SelectCharacter>();

    public Animator InGameAnimator;
    private bool _InStateTransition = false;

	void Awake()
	{
        //_SelectCharacterList = PanelCharacter.GetComponentsInChildren<SelectCharacter>(true);
	}
	
	void Start () 
	{
		AvailableCharName = new Dictionary<string, string>();

		foreach(Camera camera in live2DCamera)
		{
			camera.gameObject.SetActive(false);
		}
		ChangeState(PauseUIControllerState.MainMenu);
		StartCoroutine (WaitPartyManager());
	}

	IEnumerator WaitPartyManager()
	{
		while (!PartyManager.Instance._ReadyToPlay)
			yield return null;

		ViewCharTillNow();
		
		ForceRenderCameraLive2D ();
		
        //InventoryButton.Select ();
		
		isFirstTimeLaunch = false;
	}

	void OnEnable()
	{
		if (!isFirstTimeLaunch) {

			ViewCharTillNow();
			ChangeState(PauseUIControllerState.MainMenu);

            //StartCoroutine (WaitForButtonSelect ());

		}
	}

	IEnumerator WaitForButtonSelect()
	{
        EventSystem.current.SetSelectedGameObject(gameObject);
		yield return WaitFor.Frames (1);
		InventoryButton.Select ();
	}

	void OnDisable()
	{
		_selectedCharId="";

		if (usedLive2DCameras != null) {
			foreach (Camera camera in usedLive2DCameras) {
				if (camera.GetComponent<CC_Grayscale> () != null)
					camera.GetComponent<CC_Grayscale> ().enabled = true;
			}
		}
	}

	void OnDestroy()
	{

	}

    Button CreateNewCharButton(string idChar, int siblingIndex)
    {
        Button newButton;

        newButton = Instantiate(CharTemplate) as Button;
        newButton.transform.SetParent(PanelCharacter.transform, false);
        newButton.transform.SetSiblingIndex(siblingIndex);
        newButton.gameObject.SetActive(true);

        newButton.GetComponent<IDChar>().SetID(idChar);

        newButton.GetComponent<CharacterPreview>().RefreshData();
        newButton.GetComponent<ShowCharacterExp>().RefreshData();

        return newButton;
    }

    public void RefreshActiveReserveMembers()
    {
        _SelectCharacterList.Clear();

        _ActiveMembers = PanelBattleFormation.GetComponent<PanelFormationController>().ActiveMembers;
        _ReserveMembers = PanelBattleFormation.GetComponent<PanelFormationController>().ReserveMembers;

        if (PanelCharacter.GetComponent<GenerateCharacterSelection>() != null)
        {
            PanelCharacter.GetComponent<GenerateCharacterSelection>().ShowActiveMembers();
        }

        foreach(SelectCharacter sc in PanelCharacter.GetComponentsInChildren<SelectCharacter>(true))
        {
            _SelectCharacterList.Add(sc);
        }

        if (PanelReserveCharacter)
        {
            if (_ReserveMembers.Count == 0)
                PanelReserveCharacter.SetActive(false);
            else
            {
                PanelReserveCharacter.SetActive(true);
                if (PanelReserveCharacter.GetComponent<GenerateCharacterSelection>() != null)
                {
                    PanelReserveCharacter.GetComponent<GenerateCharacterSelection>().ShowReserveMembers();

                    SetCharacterSpecialNavigation();
                }

                foreach (SelectCharacter sc in PanelReserveCharacter.GetComponentsInChildren<SelectCharacter>(true))
                {
                    _SelectCharacterList.Add(sc);
                }
            }
        }
        
    }
    /*
    public void RefreshActiveReserveMembers()
    {
        Button[] currCharButtons = PanelCharacter.GetComponentsInChildren<Button>(true);

        _ActiveMembers = PanelBattleFormation.GetComponent<PanelFormationController>().ActiveMembers;
        _ReserveMembers = PanelBattleFormation.GetComponent<PanelFormationController>().ReserveMembers;        

        int index = 0;
        List<Button> activeMemberButtons = new List<Button>();
        bool foundChar = false;
        foreach (string characterID in _ActiveMembers)
        {
            foundChar = false;
            foreach (Button button in currCharButtons)
            {
                if (button.GetComponent<IDChar>()._ID.Equals(characterID))
                {
                    button.gameObject.SetActive(true);
                    button.transform.SetSiblingIndex(index);
                    activeMemberButtons.Add(button);
                    foundChar = true;
                    break;
                }
            }
            if(!foundChar)
            {
                activeMemberButtons.Add(CreateNewCharButton(characterID, index));
            }
            index++;
        }
        LegrandUtility.SetButtonsNavigation(activeMemberButtons, BaseEnum.Orientation.Horizontal, true);

        currCharButtons = PanelCharacter.GetComponentsInChildren<Button>();
        if (currCharButtons.Length > activeMemberButtons.Count)
        {
            for (int i = activeMemberButtons.Count; i < currCharButtons.Length; i++)
            {
                currCharButtons[i].gameObject.SetActive(false);
            }
        }

        _SelectCharacterList = PanelCharacter.GetComponentsInChildren<SelectCharacter>(true);
    }*/

    void SetCharacterSpecialNavigation()
    {
        List<Button> activeButton = PanelCharacter.GetComponent<GenerateCharacterSelection>().ShownMembersBtn;
        List<Button> reserveButton = PanelReserveCharacter.GetComponent<GenerateCharacterSelection>().ShownMembersBtn;

        Navigation selectedNavi;

        //Set navigation first active members button
        selectedNavi = activeButton[0].navigation;
        selectedNavi.mode = Navigation.Mode.Explicit;
        selectedNavi.selectOnLeft = null;
        activeButton[0].navigation = selectedNavi;

        //Set navigation last active members button
        selectedNavi = activeButton[activeButton.Count-1].navigation;
        selectedNavi.mode = Navigation.Mode.Explicit;
        selectedNavi.selectOnRight = reserveButton[0];
        activeButton[activeButton.Count - 1].navigation = selectedNavi;

        //Set navigation first reserve members button
        selectedNavi = reserveButton[0].navigation;
        selectedNavi.mode = Navigation.Mode.Explicit;
        selectedNavi.selectOnUp = activeButton[activeButton.Count - 1];
        reserveButton[0].navigation = selectedNavi;

        //Set navigation last reserve members button
        selectedNavi = reserveButton[reserveButton.Count - 1].navigation;
        selectedNavi.mode = Navigation.Mode.Explicit;
        selectedNavi.selectOnDown= null;
        reserveButton[reserveButton.Count - 1].navigation = selectedNavi;

        for(int i=0; i<reserveButton.Count;i++)
        {
            selectedNavi = reserveButton[i].navigation;
            selectedNavi.selectOnLeft = activeButton[activeButton.Count - 1];
            reserveButton[i].navigation = selectedNavi;
        }
    }

    void ViewCharTillNow()
    {
        //mengecek jika party tidak berubah, perlu dirubah algoritma nya
        bool needRefresh = false;

        if (PartyManager.Instance.CharacterParty.Count == AvailableCharName.Count)
        {
            foreach (MainCharacter chara in PartyManager.Instance.CharacterParty)
            {
                if (!AvailableCharName.ContainsKey(chara._ID))
                {
                    needRefresh = true;
                    break;
                }
            }
        }
        else
            needRefresh = true;
        if (!needRefresh)
            return;

        AvailableCharName = new Dictionary<string, string>();
        usedLive2DCameras = new List<Camera>();

        foreach (MainCharacter character in PartyManager.Instance.CharacterParty)
        {
            AvailableCharName.Add(character._ID, character._Name);
        }

        PanelBattleFormation.GetComponent<PanelFormationController>().RefreshFormationData();

        RefreshActiveReserveMembers();
    }

	public void FocusToFirstCharacter()
	{
		PanelCharacter.GetComponentInChildren<Button>().Select();
	}

    public bool FocusToCurrSelectedChar()
    {
        return FocusToCharacterButton(selectedCharId);
    }

    public bool FocusToCharacterButton(string idChar)
    {
        for (int i = 0; i < _SelectCharacterList.Count; i++)
        {
            if (_SelectCharacterList[i].IdChar.Equals(idChar) && _SelectCharacterList[i].gameObject.activeSelf)
            {
                _SelectCharacterList[i].GetComponent<Button>().Select();
                return true;
            }
        }
        return false;
    }

	void ForceRenderCameraLive2D()
	{
		if(isFirstTimeLaunch)
			foreach(Camera camera in usedLive2DCameras)
			{
					camera.Render();
				camera.gameObject.SetActive(false);
				
			}
	}

	#region Members
	public void GetActiveMembers()
	{
	}
	#endregion

    public void ChangeCharTargetToStatus()
    {
        for(int i =0;i<_SelectCharacterList.Count;i++)
        {
            _SelectCharacterList[i].TargetAsStatusChar();
        }
    }

    public void ChangeCharTargetToCommand()
    {
        for (int i = 0; i < _SelectCharacterList.Count; i++)
        {
            _SelectCharacterList[i].TargetAsCommand();
        }
    }

    public void ShowPanelByState(PauseUIControllerState panelState)
    {
        switch (panelState)
        {
            case PauseUIControllerState.MainMenu:
                PanelMain.SetActive(true);
                break;
            case PauseUIControllerState.Status:
                PanelStatusChar.SetActive(true);
                break;
            case PauseUIControllerState.Command:
                PanelAssignMgcSkill.SetActive(true);
                break;
            case PauseUIControllerState.Inventory:
                PanelInventory.SetActive(true);
                break;
            case PauseUIControllerState.BattleFormation:
                PanelBattleFormation.SetActive(true);
                break;
            case PauseUIControllerState.QuestLog:
                PanelJournal.SetActive(true);
                break;
            case PauseUIControllerState.SaveLoad:
                PanelSaveLoad.SetActive(true);
                break;
        }
    }

    public void HidePanelByState(PauseUIControllerState panelState)
    {
        switch (panelState)
        {
            case PauseUIControllerState.MainMenu:
                PanelMain.SetActive(false);
                break;
            case PauseUIControllerState.Status:
                PanelStatusChar.SetActive(false);
                break;
            case PauseUIControllerState.Command:
                PanelAssignMgcSkill.SetActive(false);
                break;
            case PauseUIControllerState.Inventory:
                PanelInventory.SetActive(false);
                break;
            case PauseUIControllerState.BattleFormation:
                PanelBattleFormation.SetActive(false);
                break;
            case PauseUIControllerState.QuestLog:
                PanelJournal.SetActive(false);
                break;
            case PauseUIControllerState.SaveLoad:
                PanelSaveLoad.SetActive(false);
                break;
        }
    }

    public void StateTransitionStarted()
    {
        _InStateTransition = true;
    }

    public void StateTransitionCompleted()
    {
        _InStateTransition = false;
    }

    IEnumerator WaitTransitionComplete(PauseUIControllerState state)
    {
        while(_InStateTransition) // state close panel complete
        {
            yield return null;
        }

        StateTransitionStarted();

        if (PauseUIState == PauseUIControllerState.MainMenu)
            PanelMain.SetActive(false);

        switch (state)
        {
            case PauseUIControllerState.MainMenu:
                PanelInventory.SetActive(false);
                PanelAssignMgcSkill.SetActive(false);
                PanelStatusChar.SetActive(false);
                PanelBattleFormation.SetActive(false);
                PanelJournal.SetActive(false);
                PanelSaveLoad.SetActive(false);
                /*
                switch (PauseUIState)
                {
                    case PauseUIControllerState.MainMenu:
                        
                        break;
                    case PauseUIControllerState.Status:
                        
                        break;
                    case PauseUIControllerState.Command:
                        if (!FocusToCurrSelectedChar())
                            FocusToFirstCharacter();
                        break;
                    case PauseUIControllerState.Inventory:
                        InventoryButton.Select();
                        break;
                    case PauseUIControllerState.BattleFormation:
                        FormationButton.Select();
                        break;
                    case PauseUIControllerState.QuestLog:
                        QuestLogButton.Select();
                        break;
                }*/
                InGameAnimator.SetTrigger("OpenMain");
                //panelMain.SetActive(true);
                break;
            case PauseUIControllerState.Status:
                InGameAnimator.SetTrigger("OpenCharacter");
                break;
            case PauseUIControllerState.Command:
                InGameAnimator.SetTrigger("OpenAssign");
                break;
            case PauseUIControllerState.Inventory:
                //panelInventory.SetActive(true);
                InGameAnimator.SetTrigger("OpenInventory");
                yield return null;
                PanelInventoryController.ShiftTab(0);
                EventSystem.current.SetSelectedGameObject(gameObject);
                break;
            case PauseUIControllerState.BattleFormation:
                InGameAnimator.SetTrigger("OpenFormation");
                break;
            case PauseUIControllerState.QuestLog:
                InGameAnimator.SetTrigger("OpenJournal");
                break;
            case PauseUIControllerState.SaveLoad:
                PanelSaveLoad.GetComponent<SaveLoadController>().HoldFirstSelect();
                InGameAnimator.SetTrigger("OpenSaveLoad");
                break;
        }

        while (_InStateTransition) // state open panel complete
        {
            yield return null;
        }

        //jika state dari main menu dan menuju ke selain main menu, maka matikan semua live2D camera, untuk menghemat draw call
        if (PauseUIState == PauseUIControllerState.MainMenu && state != PauseUIControllerState.MainMenu)
        {
            //InGameAnimator.SetTrigger("CloseCurrentPanel");
            PanelMain.SetActive(false);
            //foreach(Camera camera in live2DCamera)
            foreach (Camera camera in usedLive2DCameras)
            {
                if (camera.GetComponent<CC_Grayscale>() != null)
                    camera.GetComponent<CC_Grayscale>().enabled = true;
                camera.gameObject.SetActive(false);
            }
        }

        switch (state)
        {
            case PauseUIControllerState.MainMenu:
                if (PauseUIState == PauseUIControllerState.MainMenu)
                    InventoryButton.Select();

                //InGameAnimator.SetTrigger("OpenMain");

                PanelMain.SetActive(true);

                PanelInventory.SetActive(false);
                PanelAssignMgcSkill.SetActive(false);
                PanelStatusChar.SetActive(false);
                PanelBattleFormation.SetActive(false);
                PanelJournal.SetActive(false);

                switch (PauseUIState)
                {
                    case PauseUIControllerState.MainMenu:

                        break;
                    case PauseUIControllerState.Status:
                    case PauseUIControllerState.Command:
                        if (!FocusToCurrSelectedChar())
                            FocusToFirstCharacter();
                        break;
                    case PauseUIControllerState.Inventory:
                        InventoryButton.Select();
                        break;
                    case PauseUIControllerState.BattleFormation:
                        FormationButton.Select();
                        break;
                    case PauseUIControllerState.QuestLog:
                        QuestLogButton.Select();
                        break;
                    case PauseUIControllerState.SaveLoad:
                        SaveLoadButton.Select();
                        break;
                }

                //InGameAnimator.SetTrigger("OpenMain");

                PauseUIState = PauseUIControllerState.MainMenu;
                break;
            case PauseUIControllerState.Status:
                PauseUIState = PauseUIControllerState.Status;
                PanelStatusChar.SetActive(true);
                PanelStatusChar.GetComponent<PanelChildren>().SelectFirstSelectable();
                StartCoroutine(WaitToCallTutorial("ATP"));

                break;
            case PauseUIControllerState.Command:
                PauseUIState = PauseUIControllerState.Command;
                PanelAssignMgcSkill.SetActive(true);
                StartCoroutine(WaitToCallTutorial("AssignGrimoire"));

                break;
            case PauseUIControllerState.Inventory:
                PauseUIState = PauseUIControllerState.Inventory;
                
                PanelInventory.SetActive(true);

                //PanelInventoryController.ShiftTabInventory(0);
                PanelInventoryController.TryToFocusToFirstButton();
                StartCoroutine(WaitToCallTutorial("AssignItem"));
                break;
            case PauseUIControllerState.BattleFormation:
                PauseUIState = PauseUIControllerState.BattleFormation;
                PanelBattleFormation.SetActive(true);

                PanelBattleFormation.GetComponent<PanelFormationController>().FirstFocusModule();
                StartCoroutine(WaitToCallTutorial("Formation"));

                break;
            case PauseUIControllerState.QuestLog:
                PauseUIState = PauseUIControllerState.QuestLog;

                PanelJournal.SetActive(true);
                PanelJournal.GetComponent<PanelQuestChildren>().FirstFocusModule();
                break;
            case PauseUIControllerState.SaveLoad:
                PauseUIState = PauseUIControllerState.SaveLoad;

                PanelSaveLoad.SetActive(true);
                PanelSaveLoad.GetComponent<SaveLoadController>().AllowFirstSelect();
                break;
        }
    }

    IEnumerator WaitToCallTutorial(string tutorial)
    {
        yield return null;
        PopUpUI.CallTutorialPopUp(tutorial);
    }

	public void ChangeState(PauseUIControllerState state)
	{
        if (_InStateTransition)
            return;

        EventSystem.current.SetSelectedGameObject(gameObject);

        //untuk pertama kali dibuka, ketika state awal main menu dan akan menuju ke main menu, if ini dilewati
        if ( !(PauseUIState == PauseUIControllerState.MainMenu && state == PauseUIControllerState.MainMenu))
        {
            InGameAnimator.SetTrigger("CloseCurrentPanel");
            StateTransitionStarted();
        }

        //if(state == PauseUIControllerState.MainMenu)
        //    InGameAnimator.SetTrigger("OpenMain");

        StartCoroutine(WaitTransitionComplete(state));
	}

    public void GoToMain()
    {
        ChangeState(PauseUIControllerState.MainMenu);
    }

    public void GoToInventory()
    {
        ChangeState(PauseUIControllerState.Inventory);
    }

    public void GoToAssign()
    {
        ChangeState(PauseUIControllerState.Command);
    }

    public void GoToStatusCharacter()
    {
        ChangeState(PauseUIControllerState.Status);
    }

    public void GoToFormation()
    {
        ChangeState(PauseUIControllerState.BattleFormation);
    }

    public void GoToJournal()
    {
        ChangeState(PauseUIControllerState.QuestLog);
    }

    public void GoToSaveLoad()
    {
        ChangeState(PauseUIControllerState.SaveLoad);
    }

	public void ExitToMainMenu()
	{
		PopUpUI.CallChoicePopUp ("Exit the game ?", ConfirmedExitToMainMenu, null);
	}

	void ConfirmedExitToMainMenu()
	{
		CloseInGameMenu ();
		SceneManager.SwitchScene ("TitleScene");
	}

	public void CloseInGameMenu()
	{
        StartCoroutine(CloseInGameTransition());
    }

    IEnumerator CloseInGameTransition()
    {
        EventSystem.current.SetSelectedGameObject(gameObject);
        InGameAnimator.SetTrigger("CloseCurrentPanel");
        StateTransitionStarted();
        while (_InStateTransition) // state close panel complete
        {
            yield return null;
        }
        EventManager.Instance.TriggerEvent(new MenuInGameEvent(false));
    }
}
