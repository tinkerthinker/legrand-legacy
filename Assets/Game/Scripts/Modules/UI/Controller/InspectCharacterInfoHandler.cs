﻿using UnityEngine;
using System.Collections;

public class InspectCharacterInfoHandler : MonoBehaviour {
    public GameObject MainHolder;
    public CharacterPreview MainCharPreview;

    public GameObject CompHolder;
    public CharacterPreview CompCharPreview;

    private bool _IsCompare = false;

    private string SelectedCharId;
    private Vector2 SelectedCharPos;

    public void OnEnable()
    {
        EventManager.Instance.AddListener<ChangeUICharSelectedID>(ChangeUICharSelectedIDHandler);
        EventManager.Instance.AddListener<ComparingEvent>(ComparingEventHandler);
        EventManager.Instance.AddListener<SelectFormationPositionEvent>(SelectFormationPositionEventHandler);
    }

    public void OnDisable()
    {
        if (EventManager.Instance)
        {
            EventManager.Instance.RemoveListener<ChangeUICharSelectedID>(ChangeUICharSelectedIDHandler);
            EventManager.Instance.RemoveListener<ComparingEvent>(ComparingEventHandler);
            EventManager.Instance.RemoveListener<SelectFormationPositionEvent>(SelectFormationPositionEventHandler);
        }
    }

    void ChangeUICharSelectedIDHandler(ChangeUICharSelectedID e)
    {
        if(!_IsCompare)
        {
            SelectedCharId = e.Id;
            if (string.IsNullOrEmpty(e.Id))
            {
                MainHolder.SetActive(false);
            }
            else
            {
                MainHolder.SetActive(true);

                SetCharPreview(MainCharPreview, e.Id);
                SelectedCharPos = PanelFormationController.Instance.PositionCharInFormation(e.Id);
            }
        }
        else
        {
            if (string.IsNullOrEmpty(e.Id) || SelectedCharId.Equals(e.Id))
            {
                CompHolder.SetActive(false);
            }
            else
            {
                CompHolder.SetActive(true);

                SetCharPreview(CompCharPreview, e.Id);
            }
        }
    }

    void SetCharPreview(CharacterPreview target, string id)
    {
        target.IDChar.SetID(id);

        target.RefreshData();
        target.GetComponent<ShowCharacterHealth>().RefreshData();
        target.GetComponent<ShowCharacterStats>().RefreshData(id);

        if (PanelFormationController.Instance.IsCharInFormation(id))
        {
            SetBonusFormation(target, PanelFormationController.Instance.PositionCharInFormation(id));
        }
        else
        {
            target.GetComponent<ShowCharacterStats>().HideAllSimulatedObject();
        }
        
    }

    void SetBonusFormation(CharacterPreview target, Vector2 pos)
    {
        int row = (int)pos.y;

        if (row == 0)
        {
            target.GetComponent<ShowCharacterStats>().SetBonusFormation(false);
        }
        else if (row == 1)
        {
            target.GetComponent<ShowCharacterStats>().SetBonusFormation(true);
        }
        else
        {
            target.GetComponent<ShowCharacterStats>().HideAllSimulatedObject();
        }
    }

    void SelectFormationPositionEventHandler(SelectFormationPositionEvent e)
    {
        if (!string.IsNullOrEmpty(SelectedCharId))
        {
            MainCharPreview.GetComponent<ShowCharacterStats>().RefreshData(SelectedCharId);
            SetBonusFormation(MainCharPreview, e.Position);
        }
        if(_IsCompare)
        {
            string charId = PanelFormationController.Instance.GetCharInFormation((int)e.Position.y, (int)e.Position.x);
            if (!string.IsNullOrEmpty(charId))
            {
                CompCharPreview.GetComponent<ShowCharacterStats>().RefreshData(charId);
                SetBonusFormation(CompCharPreview, SelectedCharPos);
            }
        }
    }

    void ComparingEventHandler(ComparingEvent e)
    {
        _IsCompare = e.IsCompare;
        if(_IsCompare == false)
        {
            CompHolder.SetActive(false);
        }
    }
}
