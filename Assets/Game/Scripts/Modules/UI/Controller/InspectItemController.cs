﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class InspectItemController : MonoBehaviour
{
    public GameObject PanelMain;
    public Text MainTextItemName;
    public Text MainTextItemType;
    public Text MainTextItemDescription;
    public Text MainTextItemEffect;
    public Text MainTextItemWeight;
    public Text MainAOEText;
    public Text MainSellPriceText;
    public GameObject MainAOEHolder;

    public GameObject PanelCompare;
    public Text CompTextItemName;
    public Text CompTextItemType;
    public Text CompTextItemDescription;
    public Text CompTextItemEffect;
    public Text CompTextItemWeight;
    public Text CompAOEText;
    public Text CompSellPriceText;
    public GameObject CompAOEHolder;

    private bool IsComparing = false;

    void OnEnable()
    {
        EventManager.Instance.AddListener<InspectItemEvent>(InspectItemEventHandler);
        EventManager.Instance.AddListener<ComparingEvent>(ComparingItemEventHandler);
        if (PanelCompare)
            PanelCompare.SetActive(false);
    }

    void OnDisable()
    {
        if (EventManager.Instance)
        {
            EventManager.Instance.RemoveListener<InspectItemEvent>(InspectItemEventHandler);
            EventManager.Instance.RemoveListener<ComparingEvent>(ComparingItemEventHandler);
        }
    }

    void InspectItemEventHandler(InspectItemEvent e)
    {
        if (!e.IsShown || e.Item == null || string.IsNullOrEmpty(e.Item._ID))
        {
            if (!IsComparing)
            {
                PanelMain.SetActive(false);
            }
            else
            {
                if (PanelCompare)
                    PanelCompare.SetActive(false);
            }
        }
        else
        {
            if (!IsComparing)
            {
                PanelMain.SetActive(true);
                FillSelectedComponent(e.Item, MainTextItemName, MainTextItemType, MainTextItemDescription, MainTextItemEffect, MainTextItemWeight, MainAOEText, MainSellPriceText, MainAOEHolder);
            }
            else
            {
                if (PanelCompare)
                    PanelCompare.SetActive(true);
                FillSelectedComponent(e.Item, CompTextItemName, CompTextItemType, CompTextItemDescription, CompTextItemEffect, CompTextItemWeight, CompAOEText, CompSellPriceText, CompAOEHolder);
            }
        }
    }

    void FillSelectedComponent(Item item, Text nameText, Text typeText, Text descriptionText, Text effectText, Text weightText, Text aoeText, Text priceText, GameObject aoeHolder)
    {
        if (nameText)
            nameText.text = item.Name;
        if (typeText)
            typeText.text = item.ItemType == Item.Type.CraftingMaterial ? "Crafting Material" : item.ItemType.ToString();
        if (descriptionText)
            descriptionText.text = item.Description;
        //if (effectText)
            //effectText.text = GenerateItemInfo(e.item);			
        if (weightText)
            weightText.text = item.Weight.ToString();

        if (item is Consumable &&
            item.ItemType == Item.Type.Consumable &&
            (((Consumable)item).ConsumableType == Consumable.ConsumeType.Healing ||
                ((Consumable)item).ConsumableType == Consumable.ConsumeType.Magic))
        {
            Consumable consumable = (Consumable)item;

            if (aoeHolder)
            {
                aoeHolder.SetActive(true);
                //				aoeHolder.sprite = DatabaseImageSprite.Instance.GetTargetTypeImage (consumable.Target);
                //				aoeHolder.color = Color.white;
            }
            if (aoeText)
                aoeText.text = consumable.Target.ToString();
        }
        else
        {
            if (aoeHolder)
            {
                //				aoeHolder.sprite = null;
                //				aoeHolder.color = Color.clear;
                aoeHolder.SetActive(false);
            }
            if (aoeText)
                aoeText.text = "";
        }

        //if (item.ItemType == Item.Type.Key)
        //{
        //    if (priceText)
        //        priceText.gameObject.SetActive(false);
        //}
        //else
        //{
            if (priceText)
            {
                priceText.gameObject.SetActive(true);
                priceText.text = item.SellPrice.ToString("N0");
            }
        //}
    }

    void ComparingItemEventHandler(ComparingEvent e)
    {
        IsComparing = e.IsCompare;
        //		if (IsComparing) {
        //			PanelCompare.SetActive (true);
        //		} else {
        //			PanelCompare.SetActive (false);
        //		}
    }
}
