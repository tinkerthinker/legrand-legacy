﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using TeamUtility.IO;
using Legrand.GameSettings;

public class PauseMenuController : MonoBehaviour {
	public Button ResumeButton;
	public Button SaveLoadButton;
	public Button SettingsButton;
	public Button ExitTitleButton;
	public Button ExitWindowsButton;

	List<GameObject> ChildButtons = new List<GameObject>();

	GlobalGameStatus.GameState _PreviousState;
	float _PreviousTimeScale;

	public GameObject Panel;
	public GameObject FirstSelectedObject;

	private IEnumerator _Enumerator;

	private GameObject _LastObj;

    private bool _CanInput = true;

	void Start () {
		if(ResumeButton)
			ChildButtons.Add (ResumeButton.gameObject);
		if(SaveLoadButton)
			ChildButtons.Add (SaveLoadButton.gameObject);
		if(SettingsButton)
			ChildButtons.Add (SettingsButton.gameObject);
		if(ExitTitleButton)
			ChildButtons.Add (ExitTitleButton.gameObject);
		if(ExitWindowsButton)
			ChildButtons.Add (ExitWindowsButton.gameObject);
	}

	void Update () {
		if (InputManager.GetButtonUp ("Pause") && _CanInput) 
		{
			if (!GlobalGameStatus.Instance.StateStatus[(int)GlobalGameStatus.GameState.Pause] && !GlobalGameStatus.Instance.StateStatus[(int)GlobalGameStatus.GameState.OpenMenu])
			{
				if(Battle.Instance == null || (Battle.Instance != null && Battle.Instance.Controller.phase == BattlePhase.Command))	
				{
					SetTimeScale ();

					if (EventSystem.current) {
						if(EventSystem.current.currentSelectedGameObject)
							_LastObj = EventSystem.current.currentSelectedGameObject;
					}

                    PopUpUI.HideAllPopUp(true);
					Panel.SetActive (true);

					// Save load only at world
                    //if(GlobalGameStatus.Instance.IsStateNormal() && AreaController.Instance.CurrRoom.IsRoomSafe()) SaveLoadButton.gameObject.SetActive(true);
                    //else SaveLoadButton.gameObject.SetActive(false);
                    
					LegrandUtility.SetButtonsNavigationVertical(ResumeButton.transform.parent.GetComponentsInChildren<Button>());

                    EventSystem.current.SetSelectedGameObject(FirstSelectedObject);

                    GlobalGameStatus.Instance.StateStatus[(int)GlobalGameStatus.GameState.Pause] = true;

					EventManager.Instance.TriggerEvent (new PauseEvent ());

                    
				}
			}
			else if (GlobalGameStatus.Instance.StateStatus[(int)GlobalGameStatus.GameState.Pause])
				Resume();
		}
	}

	public void Resume()
	{
        StartCoroutine(WaitToResume());
	}

    IEnumerator WaitToResume()
    {
        yield return new WaitForEndOfFrame();
        Panel.SetActive(false);

        GlobalGameStatus.Instance.StateStatus[(int)GlobalGameStatus.GameState.Pause] = false;
        RestoreTimeScale();

        if (_LastObj != null)
        {
            if (_LastObj.GetComponent<Button>() && EventSystem.current.currentSelectedGameObject != _LastObj)
                _LastObj.GetComponent<Button>().Select();
            else
                EventSystem.current.SetSelectedGameObject(_LastObj);
            _LastObj = null;
        }

        EventManager.Instance.TriggerEvent(new UnPauseEvent());
        
        PopUpUI.HideAllPopUp(false);
    }

	public void GoToSaveLoad()
	{

	}

	public void GoToSettings()
	{

	}

	public void ExitToTitle()
	{
		PopUpUI.CallChoicePopUp ("Exit to Main Menu ?\n(Everything After last save will be lost !)", ConfirmedExitToMainMenu, AllowInput);
        HaltInput();
	}

	public void ExitToWindows()
	{
        PopUpUI.CallChoicePopUp("Quit the game ?\n(Everything After last save will be lost !)", ConfirmedExitToWindows, AllowInput);
        HaltInput();
	}

	void ConfirmedExitToMainMenu()
	{
        AllowInput();
		Resume ();
		SceneManager.SwitchScene ("TitleScene");
	}

    void ConfirmedExitToWindows()
    {
        AllowInput();
        WwiseManager.Instance.StopBG();
        Application.Quit();
    }

	public void CloseInGameMenu()
	{
		EventManager.Instance.TriggerEvent(new MenuInGameEvent(false));
	}

	void SetTimeScale()
	{
		_PreviousTimeScale = Time.timeScale;
		_Enumerator = SetZeroTimeScale (); 
		StartCoroutine (_Enumerator);
	}

	IEnumerator SetZeroTimeScale()
	{
		while (true) {
			Time.timeScale = 0;
			yield return null;
		}
	}

	void RestoreTimeScale()
	{
		if(_Enumerator != null)
			StopCoroutine (_Enumerator);
		Time.timeScale = _PreviousTimeScale;
	}

    void HaltInput()
    {
        _CanInput = false;
    }

    void AllowInput()
    {
        _CanInput = true;
    }

    public void OpenSetting()
    {
        HaltInput();
        Panel.SetActive(false);
        GameSettingHandler.Instance.Activate(OnExitSetting);
    }

    void OnExitSetting()
    {
        Panel.SetActive(true);
        SettingsButton.Select();
        AllowInput();
    }
}
