﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class InspectQuestHandler : MonoBehaviour {
	public GameObject PanelQuestDetails;
	public Text QuestTitleText;
	public Text QuestDescriptionText;
	public Text QuestClientText;
	public Text QuestRewardsText;
	public Image QuestImage;
	public GenerateObjectives ObjectiveGenerator;

	void Awake()
	{
		if (QuestTitleText)
			QuestTitleText.text = "";
		if (QuestDescriptionText)
			QuestDescriptionText.text = "";
		if (QuestClientText)
			QuestClientText.text = "";
	}

	void OnEnable()
	{
		EventManager.Instance.AddListener<InspectQuestEvent> (InspectQuestEventHandler);
	}

	void OnDisable()
	{
		if(EventManager.Instance)
			EventManager.Instance.RemoveListener<InspectQuestEvent> (InspectQuestEventHandler);
	}

	void InspectQuestEventHandler(InspectQuestEvent e)
	{
		if (e.Quest == null || !e.IsShown) {
			PanelQuestDetails.SetActive (false);
		} else {
			//make sure panel always active when shown
			PanelQuestDetails.SetActive (true);

			if (QuestTitleText)
				QuestTitleText.text = e.Quest.QuestName;
			if (QuestDescriptionText)
				QuestDescriptionText.text = e.Quest.Description;
			if (QuestClientText)
				QuestClientText.text = e.Quest.QuestGiver;
			if (QuestRewardsText) {
				string rewards = "";

				int index = 0;
                foreach (QuestReward reward in e.Quest.Rewards)
                {
                    if (reward.RewardType == QuestEnums.RewardType.Item)
                    {
                        Item item = LegrandBackend.Instance.ItemData[reward.rewardString];
                        rewards += reward.rewardNumber + "x " + item.Name;
                    }
                    else if (reward.RewardType == QuestEnums.RewardType.Money)
                    {
                        rewards += reward.rewardNumber + " Danaar";
                    }
                    else if (reward.RewardType == QuestEnums.RewardType.NPC)
                    {
                        rewards += reward.rewardString + " join your covenant";
                    }
                    else
                        rewards += reward.rewardString + " unknown ";

                    if (reward != e.Quest.Rewards[e.Quest.Rewards.Count - 1])
                        rewards += ", ";
                }


				//if (index < e.Quest.ListTasks.Count - 1 && e.Quest.ListTasks [index + 1].ListReward.Count > 0) {
				//	rewards += ", ";
				//}

				index++;
			}

			if (QuestImage) {
				QuestImage.sprite = LegrandBackend.Instance.GetSpriteData("Quest"+e.Quest.QuestID);
			}

			if (ObjectiveGenerator)
				ObjectiveGenerator.UpdateObjective (e.Quest);
		}
	}
}