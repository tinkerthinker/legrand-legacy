﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class InspectMagicSkillHandler : MonoBehaviour {
    [System.Serializable]
    public class MagicSkillDataObject
    {
        public GameObject Root;
        public Text NameText;
        public Text TargetText;
        public Text DescriptionText;
        public Text EffectText;
        public Text PowerText;
        public Text CritText;
        public Text HitText;
        public Image MagicSkillImage;

        public void SetData(Magic magic, NormalSkill skill)
        {
            if (NameText)
            {
                if (magic != null)
                    NameText.text = magic.Name;
                else if (skill != null)
                    NameText.text = skill.Name;
            }
            if (TargetText)
            {
                if (magic != null)
                    TargetText.text = magic.MagicTargetType.ToString();
                else if (skill != null)
                    TargetText.text = skill.Target.ToString();
            }
            if (DescriptionText)
            {
                if (magic != null)
                    DescriptionText.text = magic.Description;
                else if (skill != null)
                    DescriptionText.text = skill.Description;
            }
            if (EffectText)
            {
                string effects = "";
                if (magic != null)
                {
                    for (int i = 0; i < magic.BuffEffect.Count; i++)
                    {
                        if (i > 0)
                            effects += ", ";
                        effects += magic.BuffEffect[i].BuffEffect.Name;

                    }
                }
                else if (skill != null)
                {
                    for (int i = 0; i < skill.BuffsChance.Count; i++)
                    {
                        if (i > 0)
                            effects += ", ";
                        effects += skill.BuffsChance[i].BuffEffect.Name;

                    }
                }

                if (string.IsNullOrEmpty(effects))
                    effects = "-";

                EffectText.text = effects;
            }
            if (PowerText)
            {
                if (magic != null)
                    PowerText.text = "x" + magic.MagicDamageModifier;
                else if (skill != null)
                    PowerText.text = "x" + skill.DamagePercentage;
            }
            if (CritText)
            {
                if (magic != null)
                    CritText.text = magic.CritPercentage.ToString();
                else if (skill != null)
                    CritText.text = "-";
            }
            if (HitText)
            {
                if (magic != null)
                    HitText.text = magic.HitPercentage.ToString();
                else if (skill != null)
                    HitText.text = skill.BuffsChance[0].Chance.ToString();
            }
            if (MagicSkillImage)
            {
                if (magic != null)
                    MagicSkillImage.sprite = LegrandBackend.Instance.GetSpriteData("Element" + magic.Element.ToString());
                else if (skill != null)
                {
                    string skillType = "";
                    if (skill.SkillType == SkillTypes.NormalOffense)
                        skillType = "Debuff";
                    else if (skill.SkillType == SkillTypes.NormalDefense)
                    {
                        if (skill.BuffsChance.Count > 0 && skill.BuffsChance[0].BuffEffect.Type == BuffType.HEAL)
                        {
                            skillType = "Heal";
                        }
                        else
                            skillType = "Buff";
                    }
                    MagicSkillImage.sprite = LegrandBackend.Instance.GetSpriteData("Skill" + skillType);
                }
            }
        }
    }

    public MagicSkillDataObject PanelMain;
    public MagicSkillDataObject PanelComparing;

    public bool UseComparing = true;
    private bool _IsComparing = false;

    private Magic _MainMagic;
    private NormalSkill _MainSkill;

    void Awake()
    {
        if (PanelMain.Root)
            PanelMain.Root.SetActive(false);
        if (PanelComparing.Root)
            PanelComparing.Root.SetActive(false);
    }

    void OnEnable()
    {
        EventManager.Instance.AddListener<InspectMagicSkillEvent>(InspectMagicSkillEventHandler);
        EventManager.Instance.AddListener<ComparingEvent>(ComparingEventHandler);
    }

    void OnDisable()
    {
        if (EventManager.Instance)
        {
            EventManager.Instance.RemoveListener<InspectMagicSkillEvent>(InspectMagicSkillEventHandler);
            EventManager.Instance.RemoveListener<ComparingEvent>(ComparingEventHandler);
        }
    }

    void OnDestroy()
    {
        //if (EventManager.Instance)
        //    EventManager.Instance.RemoveListener<InspectMagicSkillEvent>(InspectMagicSkillEventHandler);
    }

    void InspectMagicSkillEventHandler(InspectMagicSkillEvent e)
    {
        if ((e.Magic == null && e.Skill == null) || !e.IsShown)
        {
            if (UseComparing)
            {
                if (!_IsComparing)
                {
                    if (PanelMain.Root)
                        PanelMain.Root.SetActive(false);
                }
                else
                {
                    if (PanelComparing.Root)
                        PanelComparing.Root.SetActive(false);
                }
            }
            else
            {
                if (PanelMain.Root)
                    PanelMain.Root.SetActive(false);
            }
        }
        else
        {
            //make sure panel always active when shown
            //PanelMain.SetActive(true);

            if (UseComparing)
            {
                if (!_IsComparing)
                {
                    if (PanelMain.Root)
                        PanelMain.Root.SetActive(true);
                    PanelMain.SetData(e.Magic, e.Skill);
                    _MainMagic = e.Magic;
                    _MainSkill = e.Skill;
                }
                else
                {
                    if (!PanelMain.Root.activeInHierarchy)
                    {
                        PanelMain.Root.SetActive(true);
                        PanelMain.SetData(e.Magic, e.Skill);
                        _IsComparing = false;
                    }
                    else
                    {
                        if ((e.Magic != null && _MainMagic != e.Magic) ||  (e.Skill != null && _MainSkill != e.Skill))
                        {
                            if (PanelComparing.Root)
                                PanelComparing.Root.SetActive(true);
                            PanelComparing.SetData(e.Magic, e.Skill);
                        }
                        else
                        {
                            if (PanelComparing.Root)
                                PanelComparing.Root.SetActive(false);
                        }
                    }
                }
            }
            else
            {
                if (PanelMain.Root)
                    PanelMain.Root.SetActive(true);
                PanelMain.SetData(e.Magic, e.Skill);
            }
        }
    }

    void ComparingEventHandler(ComparingEvent e)
    {
        SetComparing(e.IsCompare);
    }

    public void SetComparing(bool isCompare)
    {
        _IsComparing = isCompare;
        if(!_IsComparing)
            if (PanelComparing.Root)
                PanelComparing.Root.SetActive(false);
    }
}
