using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class ShopController : MonoBehaviour {
	public enum ShopType
	{
		Buy,
		Sell
	}
    /*
	public enum ShopState
	{
		menu,
		transaction
	}*/

    //public ShopType transactionType;
    //public ShopState shopState;

	public string selectedShopModelId;
    private static ShopController s_Instance = null;
    public static ShopController Instance
    {
        get
        {
            if (s_Instance == null)
            {
                s_Instance = GameObject.FindObjectOfType(typeof(ShopController)) as ShopController;
            }
            return s_Instance;
        }
    }
	public GameObject panelShopItemList;

	public Text panelInventoryGoldText;
	public Text panelInventoryWeightText;
	public Text panelTotalPriceText;
	public Text panelTotalWeightText;
    public Animator ShopAnimator;

    private bool _IsFirstLaunch;
	public bool isFirstLaunch
    {
        get { return _IsFirstLaunch; }
    }

    private bool _InStateTransition = false;

	void Start()
	{
        //ChangeState (ShopState.transaction);
        _IsFirstLaunch = false;
	}

	void OnEnable()
	{
        /*
        if (!_IsFirstLaunch)
        {
            ChangeState (ShopState.menu);
		}*/
        if (SelectedGameObjectModule.current != null)
            SelectedGameObjectModule.current.HideMarkObject();
        StartCoroutine(WaitFirstLaunch());
	}

    IEnumerator WaitFirstLaunch()
    {
        while (TransactionController.Instance == null)
            yield return null;

        TransactionController.Instance.HaltProcess();
        StateTransitionStarted();

        while (_InStateTransition)
            yield return null;

        TransactionController.Instance.ResumeProcess();
        //TransactionController.Instance.TryToFocusFirstButton();
    }

    public void StateTransitionStarted()
    {
        _InStateTransition = true;
    }

    public void StateTransitionCompleted()
    {
        _InStateTransition = false;
    }

	public void OnCancelShopHandler()
	{
        if (TransactionController.Instance)
        {
            if (TransactionController.Instance.IsCartEmpty())
                CloseShopMenu();
            else
                PopUpUI.CallChoicePopUp("Cancel transaction?", CloseShopMenu);
        }
	}

	void CloseShopMenu()
	{
        //FirstLaunch ();
        StartCoroutine(WaitCloseMenuTransition());
	}

    IEnumerator WaitCloseMenuTransition()
    {
        EventSystem.current.SetSelectedGameObject(gameObject);
        ShopAnimator.SetTrigger("CloseMenu");
        StateTransitionStarted();
        while(_InStateTransition)
        {
            yield return null;
        }
        EventManager.Instance.TriggerEvent(new ShopEvent(false));
    }

    /*
	public void ChangeState(ShopState state)
	{

		switch (state)
		{
		case ShopState.menu:
			shopState = ShopState.menu;
			break;
		case ShopState.transaction:
			shopState = ShopState.transaction;
			break;
		}
	}
    */
}
