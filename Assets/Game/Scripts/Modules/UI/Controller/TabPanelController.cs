using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TeamUtility.IO;

public class TabPanelController : MonoBehaviour {
	public enum PanelType
	{
		Status,
		Command,
		MagicPreset,
		Equipment
	}
	int enumCount = 4;
	public int PanelTypeCount
	{
		get{return enumCount;}
	}
	
	public static TabPanelController instance;
	
	public GameObject panelTab;

	public GameObject panelStatus;
	public GameObject panelCommand;
	public GameObject panelMagicPreset;
	public GameObject panelEquipment;

	public Color tabSelectColor;
	public Color tabDeselectColor;

	public GameObject statusCharPanel;

	private PanelType _PreviousTab;
	private PanelType currentTab;
	public PanelType CurrentTab
	{
		get{ return currentTab;}
	}

	public Button StatusTab;
	public Button CommandTab;
	public Button MagicTab;
	public Button EquipmentTab;
	
	void Awake()
	{
		TabPanelController.instance = this;
	}
	
	void Start () {
		//ChangeTab ((int)PanelType.Inventory);
		//statusCharPanel = PauseUIController.instance.panelCharacter.GetComponent<PanelCharacterChildren> ().panelStatusChar;
		foreach (GameObject obj in panelTab.GetComponent<PanelChildren>().ObjectChildren)
		{
			if(obj.GetComponent<TabButtonInteraction>().panelType == currentTab)
			{
				obj.GetComponent<Image>().color = tabSelectColor;
				obj.GetComponent<ExpandObject>().Expand();
			}
			else
			{
				obj.GetComponent<Image>().color = tabDeselectColor;
				obj.GetComponent<ExpandObject>().Normal();
			}
		}
	}

	void OnDisable()
	{
		foreach (GameObject obj in panelTab.GetComponent<PanelChildren>().ObjectChildren)
		{
			obj.GetComponent<Image>().color = tabDeselectColor;
			obj.GetComponent<ExpandObject>().Normal();
		}
	}

	void Update () {
		ShiftTabPanel ();
	}

	void ShiftTabPanel()
	{
		if (PauseUIController.Instance.PauseUIState != PauseUIController.PauseUIControllerState.MainMenu &&
		    PauseUIController.Instance.PauseUIState != PauseUIController.PauseUIControllerState.Inventory &&
		    PauseUIController.Instance.PauseUIState != PauseUIController.PauseUIControllerState.QuestLog &&
		    PauseUIController.Instance.PauseUIState != PauseUIController.PauseUIControllerState.MagicTweaker &&
		    PauseUIController.Instance.PauseUIState != PauseUIController.PauseUIControllerState.AssignCommand &&
		    PauseUIController.Instance.PauseUIState != PauseUIController.PauseUIControllerState.MagicRename)
		{
			if (InputManager.GetButtonDown("ChangeTabL"))
			{
				int tabIndex = (int)CurrentTab;
				tabIndex--;
				if(tabIndex<0)
				{
					tabIndex = PanelTypeCount - 1;
				}
				ChangeTabState(tabIndex);
			}
			else if (InputManager.GetButtonDown("ChangeTabR"))
			{
				int tabIndex = (int)CurrentTab;
				tabIndex++;
				if(tabIndex>PanelTypeCount-1)
				{
					tabIndex = 0;
				}
				ChangeTabState(tabIndex);
			}
		}
	}

	public void GoToTabPanel()
	{
		//ChangeInteractableObject.EnableAllButtonInParent (PauseUIController.instance.panelCharacter);
		//PauseUIController.instance.panelCharacter.GetComponent<PanelCharacterChildren> ().GetCharacterButton (PauseUIController.instance.selectedCharId).Select();
		
		//ChangeInteractableObject.DisableAllButtonInParent(panelTab);
	}
	
	public void DisableAllTabPanel()
	{
		//panelInventory.SetActive(false);
		//inventoryTab.sprite = DatabaseImageSprite.instance.GetSpriteButtonTabSelected(false);
		
		//panelBattle.SetActive(false);
		//battleTab.sprite = DatabaseImageSprite.instance.GetSpriteButtonTabSelected(false);
		
		//panelItem.SetActive(false);
		//partyTab.sprite = DatabaseImageSprite.instance.GetSpriteButtonTabSelected(false);
		if(panelStatus.activeInHierarchy)
			panelStatus.SetActive (false);
		if(panelEquipment.activeInHierarchy)
			panelEquipment.SetActive (false);
		if(panelMagicPreset.activeInHierarchy)
			panelMagicPreset.SetActive(false);
		//MagicTab.GetComponent<Image>().sprite = DatabaseImageSprite.instance.GetSpriteButtonTabSelected(false);
		if(panelCommand.activeInHierarchy)
			panelCommand.SetActive(false);
		//CommandTab.GetComponent<Image>().sprite = DatabaseImageSprite.instance.GetSpriteButtonTabSelected(false);
	}

	public void ChangeTabState(int panelTypeId)
	{
		panelTypeId = Mathf.Clamp (panelTypeId, 0, enumCount-1);
		_PreviousTab = currentTab;
		switch (panelTypeId)
		{
		case (int)PanelType.Status:
			PauseUIController.Instance.ChangeState(PauseUIController.PauseUIControllerState.Status);
			//panelStatus.GetComponent<PanelChildren>().SelectFirstSelectable();
			break;
		case (int)PanelType.Command:
			PauseUIController.Instance.ChangeState(PauseUIController.PauseUIControllerState.Command);

			foreach(GameObject obj in panelCommand.GetComponent<PanelChildren>().ObjectChildren)
			{
				ChangeInteractableObject.EnableAllButtonInParent(obj);
			}

			panelCommand.GetComponent<PanelChildren>().SelectFirstSelectable();
			break;
		case (int)PanelType.MagicPreset:
			PauseUIController.Instance.ChangeState(PauseUIController.PauseUIControllerState.MagicPreset);
			panelMagicPreset.GetComponent<PanelChildren>().SelectFirstSelectable();
			break;
		case (int)PanelType.Equipment:
			PauseUIController.Instance.ChangeState(PauseUIController.PauseUIControllerState.Equipment);
			panelEquipment.GetComponent<PanelChildren>().SelectFirstSelectable();
			break;
		}
	}
	
	public void ChangeTab(PanelType tabPanel)
	{
		currentTab = tabPanel;

		DisableAllTabPanel ();
		SetTabPanel ();
	}
	
	public void SetTabPanel()
	{
		foreach (GameObject obj in panelTab.GetComponent<PanelChildren>().ObjectChildren)
		{
			if(obj.GetComponent<TabButtonInteraction>().panelType == currentTab)
			{
				obj.GetComponent<Image>().color = tabSelectColor;
				obj.GetComponent<ExpandObject>().Expand();
			}
			else if(obj.GetComponent<TabButtonInteraction>().panelType == _PreviousTab)
			{
				obj.GetComponent<Image>().color = tabDeselectColor;
				obj.GetComponent<ExpandObject>().Normal();
			}
		}

		switch (currentTab) 
		{
		case PanelType.Status:
			panelStatus.SetActive(true);
			panelStatus.GetComponent<PanelChildren>().SelectFirstSelectable();
			break;
		case PanelType.Command: //COMMAND
			panelCommand.SetActive(true);

			break;
		case PanelType.MagicPreset: //MAGIC
			panelMagicPreset.SetActive(true);

			ChangeInteractableObject.EnableAllButtonInParent(panelMagicPreset.GetComponent<PanelMagicChildren>().panelMagicList);
			ChangeInteractableObject.EnableAllButtonInParent(panelTab);

			ChangeInteractableObject.DisableAllInteractableObjectInParent (PauseUIController.Instance.PanelAssignMgcSkill);

			break;
		case PanelType.Equipment:
			panelEquipment.SetActive(true);
			break;
		}
		//ShowSelectedCharInfo ();
	}
}
