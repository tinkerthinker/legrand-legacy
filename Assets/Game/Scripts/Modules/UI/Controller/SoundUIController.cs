﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using TeamUtility.IO;

public class SoundUIController : MonoBehaviour{
	GameObject currObj;
	public WwiseManager.UIFX SoundCursorMoving;
	public WwiseManager.UIFX SoundConfirm;
	public WwiseManager.UIFX SoundCancel;

	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (EventSystem.current != null 
            && EventSystem.current.currentSelectedGameObject != null )
        {
            if (currObj != EventSystem.current.currentSelectedGameObject)
            {
                if (currObj != null && EventSystem.current.currentSelectedGameObject.GetComponent<Button>() != null)
                    PlayChangeObjectSFX();
                currObj = EventSystem.current.currentSelectedGameObject;
            }

            if (InputManager.GetButtonDown("Confirm") && EventSystem.current.currentSelectedGameObject.GetComponent<Button>() != null)
            {
                PlayConfirmSFX();
            }

            if (InputManager.GetButtonDown("Cancel"))
            {
                PlayCancelSFX();
            }
        }
	}

	void PlayChangeObjectSFX()
	{
		//WwiseManager.Instance.PlaySFX(WwiseManager.UIFX.CURSOR_MOVING);
		WwiseManager.Instance.PlaySFX(SoundCursorMoving);
	}

	public void PlayConfirmSFX()
	{
		//WwiseManager.Instance.PlaySFX(WwiseManager.UIFX.CONFIRM);
		WwiseManager.Instance.PlaySFX(SoundConfirm);
	}

	public void PlayCancelSFX()
	{
		//WwiseManager.Instance.PlaySFX(WwiseManager.UIFX.CANCEL);
		WwiseManager.Instance.PlaySFX(SoundCancel);
	}
}
