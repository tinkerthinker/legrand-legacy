﻿using UnityEngine;
using System.Collections;

public class MainMenuUIController : MonoBehaviour {

	public void ExitGame()
	{
		WwiseManager.Instance.PlaySFX (WwiseManager.UIFX.Confirm);
		WwiseManager.Instance.StopBG ();
		Application.Quit ();
	}
}
