﻿using UnityEngine;
using System.Collections;

public class DialogueChoice : MonoBehaviour {
	public int ChoiceDialogueId = 0;

	public void ContinueDialogue()
	{
        StartCoroutine(WaitToContinueDialogue());
	}

    IEnumerator WaitToContinueDialogue()
    {
        yield return null;
        Dialoguer.ContinueDialogue(ChoiceDialogueId);
    }
}
