﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DialogueBoxLive2D : MonoBehaviour {
    public Image BoxImage;

    public Text NameText;
    public Text MessageText;

    public Sprite NormalBox;
    public Sprite TelephatyBox;
    public GameObject AddShoutPanel;

    public void SetAsNormal()
    {
        BoxImage.sprite = NormalBox;
        if(AddShoutPanel)
            AddShoutPanel.SetActive(false);
    }

    public void SetAsTelephaty()
    {
        BoxImage.sprite = TelephatyBox;
        if (AddShoutPanel)
            AddShoutPanel.SetActive(false);
    }

    public void SetAsShout()
    {
        BoxImage.sprite = NormalBox;
        if (AddShoutPanel)
            AddShoutPanel.SetActive(true);
    }

    public void SetData(string name, string message)
    {
        SetName(name);
        SetMessage(message);
    }

    public void SetName(string name)
    {
        if (NameText)
            NameText.text = name;
    }

    public void SetMessage(string msg)
    {
        if (MessageText)
            MessageText.text = msg;
    }
}