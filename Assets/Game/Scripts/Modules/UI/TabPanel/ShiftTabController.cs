﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections;
using TeamUtility.IO;

public class ShiftTabController : MonoBehaviour {
    [System.Serializable]
    public class ImageShiftSprite
    {
        public Image TargetImage;
        [Header("Only Used in ChangeSprite Type")]
        public Sprite NormalSprite;
        public Sprite SelectedSprite;

        public UnityEvent OnShiftHere;

        public void ChangeToNormal()
        {
            if(TargetImage)
                TargetImage.sprite = NormalSprite;
        }

        public void ChangeToSelected()
        {
            if (TargetImage)
                TargetImage.sprite = SelectedSprite;
        }
    }

    public enum ChangeGraphicType
    {
        ChangeColor,
        ChangeSprite
    }

    int _CurrIndex = 0;
    int _MaxIndex;

    bool _IsUsePreShift = false;
    bool _CanShift = true;

    bool _IsFirstLaunch = true;

    public ChangeGraphicType ShiftGraphicType;
    public bool FirstTabOnStart = true;

    public ImageShiftSprite[] TabImages;
    public Color SelectedColor;
    public Color DeselectedColor;

    public UnityEvent OnShift;

    public delegate bool PreShift();
    private event PreShift OnPreShift;
    public delegate void ShiftingFunction(int index);
    private event ShiftingFunction OnShiftingFunction;

    public void SetImageArray(Sprite[] items)
    {
        for(int i=0;i<TabImages.Length;i++)
        {
            if (i < items.Length)
            {
                TabImages[i].TargetImage.gameObject.SetActive(true);
                TabImages[i].TargetImage.sprite = items[i];
            }
            else
            {
                TabImages[i].TargetImage.gameObject.SetActive(false);
            }
        }
    }

    void Awake()
    {
        for (int i = 0; i < TabImages.Length; i++)
        {
            DeselectImage(i);
        }
    }

    void Start()
    {
        if(FirstTabOnStart)
            ShiftTabTo(0);
        //FirstLaunchTab();
        _IsFirstLaunch = false;
    }

    void OnEnable()
    {
        if (!_IsFirstLaunch && FirstTabOnStart)
            ShiftTabTo(0);
        if (_MaxIndex == 0)
            _MaxIndex = TabImages.Length - 1;
    }

    void Update()
    {
        if (_IsUsePreShift)
        {
            if(OnPreShift != null)
                _CanShift = OnPreShift();
        }
        if(_CanShift)
        {
            if (InputManager.GetButtonDown("ChangeTabL"))
            {
                ShiftTab(-1);
            }
            else if (InputManager.GetButtonDown("ChangeTabR"))
            {
                ShiftTab(+1);
            }
        }
    }

    void FirstLaunchTab()
    {
        if (TabImages.Length == 0)
            return;
        for(int i=0; i<TabImages.Length; i++)
        {
            DeselectImage(i);
        }
        SelectImage(0);
    }

    void SelectImage(int index)
    {
        if (ShiftGraphicType == ChangeGraphicType.ChangeSprite)
            TabImages[index].ChangeToSelected();
        else
            TabImages[index].TargetImage.color = SelectedColor;

        if (TabImages[index].OnShiftHere != null)
            TabImages[index].OnShiftHere.Invoke();
    }

    void DeselectImage(int index)
    {
        if (ShiftGraphicType == ChangeGraphicType.ChangeSprite)
            TabImages[index].ChangeToNormal();
        else
            TabImages[index].TargetImage.color = DeselectedColor;
    }

    public void ShiftTabTo(int index)
    {
        DeselectImage(_CurrIndex);        
        SelectImage(index);

        if (index != _CurrIndex)
            WwiseManager.Instance.PlaySFX(WwiseManager.UIFX.Scroll);

        _CurrIndex = index;

        if (OnShiftingFunction != null)
            OnShiftingFunction(_CurrIndex);

        if (OnShift != null)
            OnShift.Invoke();
    }

    void ShiftTab(int increment)
    {
        //int lastIndex = _CurrIndex;
        //DeselectImage(_CurrIndex);

        int index = _CurrIndex;
        index += increment;
        if (index > _MaxIndex)
        {
            index = 0;
        }
        else if (index < 0)
        {
            index = _MaxIndex;
        }

        ShiftTabTo(index);

        //SelectImage(_CurrIndex);

        if(OnShiftingFunction != null)
            OnShiftingFunction(_CurrIndex);
    }

    //register function that determine wether can shift tab or not
    public void RegisterPreShiftFunction(PreShift preShift)
    {
        _IsUsePreShift = true;
        OnPreShift += preShift;
    }

    public void RegisterShiftingFunction(ShiftingFunction shiftingFunction, int maxShiftIndex)
    {
        _MaxIndex = maxShiftIndex;
        OnShiftingFunction += shiftingFunction;
    }

    public void RemovePreShiftFunction(PreShift preShift)
    {
        OnPreShift -= preShift;
        if (OnPreShift == null)
            _IsUsePreShift = false;
    }

    public void RemoveShiftingFunction(ShiftingFunction shiftingFunction)
    {
        OnShiftingFunction -= shiftingFunction;
        if(OnShiftingFunction == null)
            _MaxIndex = 0;
    }
}
