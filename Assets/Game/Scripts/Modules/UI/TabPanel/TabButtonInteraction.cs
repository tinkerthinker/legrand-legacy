using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class TabButtonInteraction : MonoBehaviour, ISelectHandler, ICancelHandler {
	public TabPanelController.PanelType panelType;

	void Start()
	{
		GetComponent<Button> ().onClick.AddListener (() => OnClickHandler());
	}

	void OnClickHandler()
	{
		//ChangeTabPanel.instance.GoToTabPanel ();
	}

	public void OnSelect(BaseEventData eventData)
	{
		switch (panelType) 
		{
		case TabPanelController.PanelType.Status:
			PauseUIController.Instance.ChangeState (PauseUIController.PauseUIControllerState.Status);
			break;
		case TabPanelController.PanelType.Command: //COMMAND
			PauseUIController.Instance.ChangeState (PauseUIController.PauseUIControllerState.Command);
			break;
		case TabPanelController.PanelType.MagicPreset: //MAGIC
			PauseUIController.Instance.ChangeState (PauseUIController.PauseUIControllerState.MagicPreset);
			break;
		case TabPanelController.PanelType.Equipment:
			PauseUIController.Instance.ChangeState (PauseUIController.PauseUIControllerState.Equipment);
			break;
		}
		//TabPanelController.instance.ChangeTab (panelType);
	}
	
	public void OnCancel(BaseEventData eventData)
	{
		//PauseUIController.instance.ClosePauseMenu ();
		PauseUIController.Instance.ChangeState (PauseUIController.PauseUIControllerState.MainMenu);
	}
}
