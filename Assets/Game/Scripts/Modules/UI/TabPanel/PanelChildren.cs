﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PanelChildren : MonoBehaviour {
	[SerializeField]
	private Selectable FirstSelectable;
	public GameObject[] ObjectChildren;

    void OnEnable()
    {
        //SelectFirstSelectable();
    }

	public void SelectFirstSelectable()
	{
		if (FirstSelectable == null)
			return;
		if (!FirstSelectable.isActiveAndEnabled)
		{
			FirstSelectable.gameObject.SetActive (true);
			FirstSelectable.interactable = true;
		}
		FirstSelectable.Select ();
	}

    public void GoToMain(bool toCurrentCharacter = false)
    {
        PauseUIController.Instance.ChangeState(PauseUIController.PauseUIControllerState.MainMenu);
        /*
        if (toCurrentCharacter)
        {
            if (!PauseUIController.Instance.FocusToCurrSelectedChar())
                PauseUIController.Instance.FocusToFirstCharacter();            
        }*/
    }
}
