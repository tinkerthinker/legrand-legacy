﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HorizontalFlexibleChildren : MonoBehaviour {
	public int unControlledChildrenAmount;
	public float widthTransform;
	int currAmountChildren;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		if (GetComponentsInChildren<Button> ().Length != currAmountChildren) {
			currAmountChildren = GetComponentsInChildren<Button> ().Length;
			if(currAmountChildren <= unControlledChildrenAmount)
			{
				GetComponent<ContentSizeFitter>().horizontalFit = ContentSizeFitter.FitMode.PreferredSize;
			}
			else{
				GetComponent<ContentSizeFitter>().horizontalFit = ContentSizeFitter.FitMode.Unconstrained;
				Rect rect = GetComponent<RectTransform>().rect;
				rect.width = widthTransform;

				GetComponent<RectTransform> ().sizeDelta = new Vector2(rect.width, rect.height);
			}

		}
	}
}
