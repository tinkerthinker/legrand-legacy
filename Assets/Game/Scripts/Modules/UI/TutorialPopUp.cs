﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

using TeamUtility.IO;

public class TutorialPopUp : MonoBehaviour
{
    public bool test;
    public string TutorialNameTest;

    public string NextTextButtonLanguageID;
    public string PreviousTextButtonLanguageID;
    public string FinishTextButtonLanguageID;

    public Button RightButton;
    public Button LeftButton;

    public Color PressedColor;
    public float HighlightTime;
    
    private string PrefabNameSteps;
	private int _indexStep = 0;
    private int _indexCount = 0;

    const string Path = "TutorialPrefab/";
    public GameObject TutorialParent;
    public GameObject TutorialPanel;

	public Text TutorialCurrentPage;
	public Button ConfirmButton;

	private GameObject lastObj;
	private float currTimeScale;
    private Animator _AnimController;

    public WwiseManager.UIFX ShiftSound;
	public bool StopTime;

	TeamUtility.IO.StandaloneInputModule module;

    private bool _CanInputDirection = true;
    private bool _IsHaltOperation = false;

    private bool _IsHidden = false;
    public bool IsHidden
    {
        get { return _IsHidden; }
    }

    void Awake()
    {
        PopUpUI.OnHidePopUp += HidePopUpObject;
        _AnimController = GetComponent<Animator>();
    }

    void OnDestroy()
    {
        PopUpUI.OnHidePopUp -= HidePopUpObject;
    }

	void Start ()
    {
		module = EventSystem.current.GetComponent<TeamUtility.IO.StandaloneInputModule> ();
	}

	void OnEnable()
	{
        if (SelectedGameObjectModule.current)
            SelectedGameObjectModule.current.HideMarkObject();

        if (StopTime)
        {
			currTimeScale = Time.timeScale;
			Time.timeScale = 0f;
		}

		_indexStep=0;

        if (GlobalGameStatus.Instance)
            GlobalGameStatus.Instance.StateStatus[(int)GlobalGameStatus.GameState.Tutorial] = true;

        if (EventSystem.current)
        {
            lastObj = EventSystem.current.currentSelectedGameObject;
		}

        if (test)
        {
            PrefabNameSteps = LegrandBackend.Instance.GetTutorialSteps(TutorialNameTest).TutorialPrefab;
            UpdateTutorial(PrefabNameSteps);
        }

        ShiftStep (_indexStep);

		StartCoroutine (WaitForButtonSelect ());
	}

    void Update()
    {
        if(!IsHidden)
            InputDirection(LegrandUtility.GetAxisRawHorizontal());
    }

    void HidePopUpObject(bool isHide)
    {
        if (isHide)
        {
            if (!gameObject.activeInHierarchy)
                return;
            _AnimController.enabled = false;
            //_AnimController.updateMode = AnimatorUpdateMode.Normal;
            GetComponent<CanvasGroup>().alpha = 0;
            //TutorialPanel.SetActive(false);
            _IsHidden = true;
        }
        else
        {
            if (!_IsHidden)
                return;
            _AnimController.enabled = true;
            //_AnimController.updateMode = AnimatorUpdateMode.UnscaledTime;
            GetComponent<CanvasGroup>().alpha = 1;
            //TutorialPanel.SetActive(true);
            _IsHidden = false;
        }
    }

    void InputDirection(float inputValue)
    {
        if (inputValue > 0.5f || InputManager.GetButtonDown("Confirm"))
        {
            if (_CanInputDirection)
                ConfirmAction();
            _CanInputDirection = false;
        }
        else if (inputValue < -0.5f || InputManager.GetButtonDown("Cancel"))
        {
            if (_CanInputDirection)
                PreviousTutorial();
            _CanInputDirection = false;
        }
        else// if (inputValue > -0.5f && inputValue < 0.5f)
        {
            _CanInputDirection = true;
        }
    }

    public void DisableInput()
    {
        _IsHaltOperation = true;
    }

    public void EnableInput()
    {
        _IsHaltOperation = false;
    }

	IEnumerator WaitForButtonSelect()
	{
		yield return WaitFor.Frames (1);
        //ConfirmButton.Select ();
        EventSystem.current.SetSelectedGameObject(null);
	}

	public void UpdateTutorial(string prefabName)
	{
		PrefabNameSteps = prefabName;

		_indexStep = 0;

        GameObject tutorial = GameObject.Instantiate((GameObject)Resources.Load(Path + PrefabNameSteps));
        _indexCount = tutorial.transform.childCount;
        SetTutorialCurrentPage();
        tutorial.transform.SetParent(TutorialParent.transform);
        tutorial.transform.localScale = new Vector3(1, 1, 1);
        tutorial.GetComponent<RectTransform>().anchoredPosition = new Vector3(0, 0, 0);

        ShiftStep(_indexStep);
    }

    void SetTutorialState()
    {
        if (TutorialParent.transform.childCount > 0)
        {
            for (int i = 0; i < _indexCount; i++)
            {
                if (i == _indexStep)
                {
                    TutorialParent.transform.GetChild(0).transform.GetChild(i).gameObject.SetActive(true);
                }
                else
                {
                    TutorialParent.transform.GetChild(0).transform.GetChild(i).gameObject.SetActive(false);
                }
            }
        }
    }

    void DestroyTutorial()
    {
        if (TutorialParent.transform.childCount > 0)
        {
            Destroy(TutorialParent.transform.GetChild(0).gameObject);
        }
    }

	void ShiftStep(int index)
	{
        _indexStep = index;        

        SetTutorialState();        
        SetTutorialCurrentPage();
        SetBottomButton();
    }

    void SetBottomButton()
    {
        if (_indexStep == 0)
            LeftButton.gameObject.SetActive(false);
        else
        {
            LeftButton.gameObject.SetActive(true);
            LeftButton.GetComponentInChildren<Text>().text = LegrandBackend.Instance.GetLanguageData(PreviousTextButtonLanguageID, true);
        }

        if(_indexStep + 1 < _indexCount)
            RightButton.GetComponentInChildren<Text>().text = LegrandBackend.Instance.GetLanguageData(NextTextButtonLanguageID, true);
        else
            RightButton.GetComponentInChildren<Text>().text = LegrandBackend.Instance.GetLanguageData(FinishTextButtonLanguageID, true);
    }

    void SetTutorialCurrentPage()
    {
        TutorialCurrentPage.text = (_indexStep + 1) + "/" + _indexCount;
    }

    IEnumerator WaitPrevious()
    {
        yield return WaitFor.RealTime(HighlightTime);
        TutorialParent.transform.GetChild(0).transform.GetChild(_indexStep).GetComponent<TutorialTextLoader>().StopVideos();
        yield return WaitFor.Frames(3);
        PreviousActions();
    }

    public void PreviousTutorial()
    {
        if (_IsHaltOperation)
            return;
        LeftButton.GetComponent<Image>().color = PressedColor;
        if(_indexStep != 0)
            WwiseManager.Instance.PlaySFX(ShiftSound);
        _IsHaltOperation = true;
        StartCoroutine(WaitPrevious());
        //Invoke("PreviousActions", HighlightTime);
    }

    void PreviousActions()
    {
        if (_indexStep > 0)
            ShiftStep(--_indexStep);

        LeftButton.GetComponent<Image>().color = Color.white;
        _IsHaltOperation = false;
    }

    IEnumerator WaitConfirm()
    {
        yield return WaitFor.RealTime(HighlightTime);
        TutorialParent.transform.GetChild(0).transform.GetChild(_indexStep).GetComponent<TutorialTextLoader>().StopVideos();
        yield return WaitFor.Frames(3);
        ConfirmActions();
    }

	public void ConfirmAction()
	{
        if (_IsHaltOperation)
            return;
        RightButton.GetComponent<Image>().color = PressedColor;
        WwiseManager.Instance.PlaySFX(ShiftSound);
        _IsHaltOperation = true;
        StartCoroutine(WaitConfirm());
        //Invoke("ConfirmActions", HighlightTime);
	}

    void ConfirmActions()
    {
        if (_indexStep < _indexCount - 1)
        {
            ShiftStep(++_indexStep);
        }
        else
        {
            CloseTutorial();
        }

        RightButton.GetComponent<Image>().color = Color.white;
        _IsHaltOperation = false;
    }

	void CloseTutorial()
	{
        if (GlobalGameStatus.Instance)
            GlobalGameStatus.Instance.StateStatus[(int)GlobalGameStatus.GameState.Tutorial] = false;

		if (lastObj != null) {
			if(lastObj.GetComponent<Button>())
				lastObj.GetComponent<Button>().Select();
			else
				EventSystem.current.SetSelectedGameObject(lastObj);
			lastObj = null;
		}
        
        DestroyTutorial();
        PopUpUI.CloseAllPopUp (false);
		gameObject.SetActive (false);

		if (StopTime) {
			Time.timeScale = currTimeScale;
		}

        SelectedGameObjectModule.current.ShowMarkObject();
    }

}
