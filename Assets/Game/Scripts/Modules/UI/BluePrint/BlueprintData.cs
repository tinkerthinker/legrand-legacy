﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Legrand.core;

public class BlueprintData : MonoBehaviour, IObjectData
{
    public bool SetAsNullData = false;
    public Image IconImage;
    public GameObject ImpossibleIcon;
    public Text NameText;
    public Text PriceText;
    public Text OwnText;
    public Text AOEText;
    public GameObject CurrentEquipSymbol;
    public GameObject InsuffMaterialObj;

    BluePrint _BluePrint;
    EquipmentDataModel _Equipment;
    Item _ResultItem;

    public int CreateAmountPreview = 1;

    public ExecuteCraftingBlueprintController ExecuteBlacksmithBlueprint;
    public ExecuteAlchemistBlueprintController ExecuteAlchemyBlueprint;

    NavigationButtonData _Navigation;

    private bool _IsPossibleToCraft;
    private bool _IsCurrentEquip;

    void Awake()
    {
        _Navigation = GetComponent<NavigationButtonData>();
    }

    public void SetData(object obj)
    {
        if (SetAsNullData)
            return;
        //Blueprint mean can be a craft or alchemy
        if (obj is BluePrint)
        {
            _BluePrint = (BluePrint)obj;

            if (_BluePrint.BluePrintType == BluePrint.Type.Crafting)
            {
                _Equipment = LegrandBackend.Instance.GetEquipment(_BluePrint.ResultItemID);
                _ResultItem = null;
                if (!string.IsNullOrEmpty(_BluePrint.CharacterID))
                {
                    Equipments equip = PartyManager.Instance.GetCharacter(_BluePrint.CharacterID).Equipment();
                    if (equip.ID.Equals(_BluePrint.ResultItemID))
                        _IsCurrentEquip = true;
                    else
                        _IsCurrentEquip = false;
                }
            }
            else
            {
                _ResultItem = LegrandBackend.Instance.ItemData[_BluePrint.ResultItemID];
                _Equipment = null;
                _IsCurrentEquip = false;
            }
        }
        else if (obj is Equipments)
        {
            _BluePrint = null;
            _ResultItem = null;
            _Equipment = LegrandBackend.Instance.GetEquipment(((Equipments)obj).ID);
        }
        else
        {
            return;
        }

        if (IconImage)
            IconImage.sprite = _Equipment != null? _Equipment.Picture: _ResultItem != null? _ResultItem.Icon: null;
        if (NameText)
            NameText.text = _Equipment != null? _Equipment.Name: _ResultItem != null? _ResultItem.Name: "";
        if (PriceText)
        {
            if (!_IsCurrentEquip)
                PriceText.text = _BluePrint != null ? _BluePrint.Price.ToString("N0") : "0";
            else
                PriceText.text = "-";
        }
        if (OwnText)
            OwnText.text = PartyManager.Instance.Inventory.GetStackItem(LegrandBackend.Instance.ItemData[_BluePrint.ResultItemID]).ToString();
        if (AOEText)
        {
            if (_ResultItem != null)
            {
                if (_ResultItem.ItemType == Item.Type.Consumable &&
                    (((Consumable)_ResultItem).ConsumableType == Consumable.ConsumeType.Healing ||
                        ((Consumable)_ResultItem).ConsumableType == Consumable.ConsumeType.Magic))
                {
                    AOEText.text = ((Consumable)_ResultItem).Target.ToString().Substring(0, 1);
                }
                else
                    AOEText.text = "";
            }
            else
            {
                AOEText.text = "";
            }
        }
        if (CurrentEquipSymbol)
        {
            if (_IsCurrentEquip)
                CurrentEquipSymbol.SetActive(true);
            else
                CurrentEquipSymbol.SetActive(false);
        }
        if(InsuffMaterialObj)
        {
            SetEnoughMaterial(_BluePrint.TryCombine());
        }
    }

    public object GetData()
    {
        return _Equipment;
    }

    public void RefreshData()
    {
        SetData(_Equipment);
    }

    public void OnSelect(UnityEngine.EventSystems.BaseEventData eventData)
    {
        if(_Equipment != null)
            EventManager.Instance.TriggerEvent(new InspectEnhanceRecipeEvent(_Equipment, _BluePrint, CreateAmountPreview));
        else if (_ResultItem != null)
            EventManager.Instance.TriggerEvent(new InspectCombineAlchemistEvent(_ResultItem, _BluePrint, CreateAmountPreview));
        else
        {
            EventManager.Instance.TriggerEvent(new InspectEnhanceRecipeEvent(null, null, CreateAmountPreview));
            EventManager.Instance.TriggerEvent(new InspectCombineAlchemistEvent(null, null, CreateAmountPreview));
        }
    }

    public void CreateBluePrint()
    {
        if (_IsCurrentEquip)
            return;

        if (_IsPossibleToCraft)
        {
            if(_Equipment != null)
                ExecuteBlacksmithBlueprint.Execute(_BluePrint, _Navigation);
            if (_ResultItem != null)
                ExecuteAlchemyBlueprint.Execute(_BluePrint, _Navigation);
        }
        else
            PopUpUI.CallNotifPopUp("This Merchant can't craft this item");
    }

    public void SetPossibleToCraft(bool isPossible)
    {
        _IsPossibleToCraft = isPossible;
        if (ImpossibleIcon)
        {
            if (isPossible)
                ImpossibleIcon.gameObject.SetActive(false);
            else
                ImpossibleIcon.gameObject.SetActive(true);
        }
    }

    public void SetEnoughMaterial(bool isEnough)
    {
        if(!InsuffMaterialObj)
            return;
        if (isEnough)
            InsuffMaterialObj.SetActive(false);
        else
            InsuffMaterialObj.SetActive(true);

    }

    public void CancelCreate()
    {
        if(_ResultItem != null)
            ExecuteAlchemyBlueprint.CancelCreate();
    }
}
