﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class LegrandPopUpGUI : MonoBehaviour {
	public GameObject NotifPopUpObj;    //pooling
    public GameObject ChoicePopUpObj;   //pooling
	public GameObject TutorialPopUpObj;
    public GameObject DialoguePopUpObj; //this variabel is used to know which balloon is used by balloon dialogue //pooling
	public GameObject DialogueMarkPopUpObj;
	public GameObject AmountSetPopUpObj;

	private GameObject _NotifPopUpObj;
	private GameObject _ChoicePopUpObj;
	private GameObject _TutorialPopUpObj;
	private GameObject _DialoguePopUpObj;
	private GameObject _DialogueMarkPopUpObj;
	private GameObject _AmountSetPopUpObj;

    private List<GameObject> _Notifs = new List<GameObject>();
    private List<GameObject> _Choices = new List<GameObject>();
    private List<GameObject> _Balloons = new List<GameObject>();

    private RespondObjectPopUp respondReqObject = new RespondObjectPopUp(null);

	void Awake () {
		Initialize ();

		PopUpUI.OnNotifPopUp += ShowNotifPopUp;
		PopUpUI.OnChoicePopUp += ShowChoicePopUp;
		PopUpUI.OnTutorialPopUp += ShowTutorialPopUp;
		PopUpUI.OnDialoguePopUp += ShowDialoguePopUp;
		PopUpUI.OnDialogueMarkPopUp += ShowDialogueMarkPopUp;
		PopUpUI.OnAmountSetPopUp += ShowAmountSetPopUp;

        EventManager.Instance.AddListener<RequestObjectPopUp>(RequestObjectPopUpHandler);
	}

	void OnDestroy()
	{
		PopUpUI.OnNotifPopUp -= ShowNotifPopUp;
		PopUpUI.OnChoicePopUp -= ShowChoicePopUp;
		PopUpUI.OnTutorialPopUp -= ShowTutorialPopUp;
		PopUpUI.OnDialoguePopUp -= ShowDialoguePopUp;
		PopUpUI.OnDialogueMarkPopUp -= ShowDialogueMarkPopUp;
		PopUpUI.OnAmountSetPopUp -= ShowAmountSetPopUp;

        if(EventManager.Instance)
            EventManager.Instance.RemoveListener<RequestObjectPopUp>(RequestObjectPopUpHandler);
	}

	void Initialize()
	{
		PopUpUI.Initialize ();
	}

    void RequestObjectPopUpHandler(RequestObjectPopUp e)
    {
        switch(e.TypePopUp)
        {
            case "Notif":
                respondReqObject.PopUp = _NotifPopUpObj;
                break;
            case "Choice":
                respondReqObject.PopUp = _ChoicePopUpObj;
                break;
            case "Tutorial":
                respondReqObject.PopUp = _TutorialPopUpObj;
                break;
            case "Dialogue":
                respondReqObject.PopUp = _DialoguePopUpObj;
                break;
            case "DialogueMark":
                respondReqObject.PopUp = _DialogueMarkPopUpObj;
                break;
            case "Amount":
                respondReqObject.PopUp = _AmountSetPopUpObj;
                break;
            default:
                respondReqObject.PopUp = null;
                break;
        }

        EventManager.Instance.TriggerEvent(respondReqObject);
    }

    GameObject MakeNewNotif()
    {
        GameObject popUp;
        if (NotifPopUpObj)
            popUp = Instantiate(NotifPopUpObj as GameObject);
        else
            popUp = Instantiate(Resources.Load<GameObject>("UI/PopUp/NotifPopUp") as GameObject);

        popUp.transform.SetParent(this.transform, true);
        _Notifs.Add(popUp);
        popUp.name = "NotifPopUp" + _Notifs.Count;

        return popUp;
    }

    GameObject MakeNewChoice()
    {
        GameObject popUp;
        if (ChoicePopUpObj)
            popUp = Instantiate(ChoicePopUpObj as GameObject);
        else
            popUp = Instantiate(Resources.Load<GameObject>("UI/PopUp/ChoicePopUp") as GameObject);

        popUp.transform.SetParent(this.transform, true);
        _Choices.Add(popUp);
        popUp.name = "ChoicePopUp" + _Choices.Count;

        return popUp;
    }

    GameObject MakeNewDialogue()
    {
        GameObject popUp;
        if (DialoguePopUpObj)
            popUp = Instantiate(DialoguePopUpObj as GameObject);
        else
            popUp = Instantiate(Resources.Load<GameObject>("UI/PopUp/DialoguePopUp") as GameObject);

        popUp.transform.SetParent(this.transform, true);
        _Balloons.Add(popUp);
        popUp.name = "BalloonPopUp" + _Balloons.Count;

        return popUp;
    }

	void ShowNotifPopUp(string message, float liveTime)
	{
		if (_NotifPopUpObj == null) {
            /*
			GameObject popUp;
			if(NotifPopUpObj)
				popUp = Instantiate (NotifPopUpObj as GameObject);
			else
				popUp = Instantiate (Resources.Load<GameObject> ("UI/PopUp/NotifPopUp") as GameObject);
			_NotifPopUpObj = popUp;*/
            _NotifPopUpObj = MakeNewNotif();
		} else {
            if (!_NotifPopUpObj.activeInHierarchy)
                _NotifPopUpObj.SetActive(true);
            else
            {
                GameObject popUp = MakeNewNotif();
                /*
                if (NotifPopUpObj)
                    popUp = Instantiate(NotifPopUpObj as GameObject);
                else
                    popUp = Instantiate(Resources.Load<GameObject>("UI/PopUp/NotifPopUp") as GameObject);*/

                popUp.GetComponentInChildren<Text>().text = message;
                StartCoroutine(WaitSelectPopUpButton(popUp, liveTime));
                return;
            }
		}

		_NotifPopUpObj.GetComponentInChildren<Text> ().text = message;
        StartCoroutine( WaitSelectPopUpButton(_NotifPopUpObj, liveTime));
	}

    IEnumerator WaitSelectPopUpButton(GameObject popup, float delay)
    {
        Button popupBtn = popup.GetComponentInChildren<Button>();
        popupBtn.gameObject.SetActive(false);
        yield return new WaitForSeconds(delay);
        popupBtn.gameObject.SetActive(true);
        popupBtn.Select();
    }
	
	void ShowChoicePopUp(string message)
	{
		if (_ChoicePopUpObj == null) {
            /*
			GameObject popUp;
			if(ChoicePopUpObj)
				popUp = Instantiate (ChoicePopUpObj as GameObject);
			else
				popUp = Instantiate (Resources.Load<GameObject> ("UI/PopUp/ChoicePopUp") as GameObject);
			_ChoicePopUpObj = popUp;*/
            _ChoicePopUpObj = MakeNewChoice();
		} else {
            if(!_ChoicePopUpObj.activeInHierarchy)
			    _ChoicePopUpObj.SetActive(true);
            else
            {
                GameObject popUp = MakeNewChoice();
                /*
                if (ChoicePopUpObj)
                    popUp = Instantiate(ChoicePopUpObj as GameObject);
                else
                    popUp = Instantiate(Resources.Load<GameObject>("UI/PopUp/ChoicePopUp") as GameObject);*/

                popUp.GetComponentInChildren<Text>().text = message;
                StartCoroutine(WaitSelectPopUpButton(popUp, 0));
                return;
            }
		}

		_ChoicePopUpObj.GetComponentInChildren<Text> ().text = message;
        StartCoroutine( WaitToSelectButton(_ChoicePopUpObj.GetComponentInChildren<Button>()) );
	}

	void ShowTutorialPopUp(string tutorialName, bool forceOpen)
	{
		TutorialSteps currTut = LegrandBackend.Instance.GetTutorialSteps (tutorialName);

		if (PartyManager.Instance.TutorialProgress.ContainsKey(currTut.Name) && PartyManager.Instance.TutorialProgress[currTut.Name] && !forceOpen) {
			PopUpUI.CloseAllPopUp();
			return;
		}

		if (_TutorialPopUpObj == null) {
			GameObject popUp;
			if(TutorialPopUpObj)
				popUp = Instantiate (TutorialPopUpObj as GameObject);
			else
				popUp = Instantiate (Resources.Load<GameObject> ("UI/PopUp/TutorialPopUp") as GameObject);
			_TutorialPopUpObj = popUp;
		}else{
			_TutorialPopUpObj.SetActive(true);
		}

		_TutorialPopUpObj.GetComponentInChildren<TutorialPopUp> ().UpdateTutorial( currTut.TutorialPrefab);
        if(forceOpen)
            _TutorialPopUpObj.GetComponentInChildren<Animator>().SetTrigger("SkipSplash");
		PartyManager.Instance.TutorialProgress [currTut.Name] = true;
	}

    GameObject FindUnUsedDialogue(bool forMainDialogue)
    {
        for (int i = 0; i < _Balloons.Count; i++)
        {
            //if hidden and is not a shout balloon with talking obj shout still active
            if (!_Balloons[i].activeInHierarchy && !(_Balloons[i].GetComponent<DialoguePopUp>().IsDieByTime && _Balloons[i].GetComponent<DialoguePopUp>().TalkingObj1.activeInHierarchy))
            {
                if (_Balloons[i] == _DialoguePopUpObj)
                {
                    if (forMainDialogue)
                        return _Balloons[i];
                    else
                        continue;
                }
                else
                {
                    return _Balloons[i];
                }
            }
        }
        return null;
    }

    GameObject FindShoutDialogue(GameObject talkingObj)
    {
        for (int i = 0; i < _Balloons.Count; i++)
        {
            //if hidden and is not a shout balloon with talking obj shout still active
            if (_Balloons[i].GetComponent<DialoguePopUp>().IsDieByTime && _Balloons[i].GetComponent<DialoguePopUp>().TalkingObj1 == talkingObj)
            {
                return _Balloons[i];
            }
        }
        return null;
    }

	void ShowDialoguePopUp(string name, string message, GameObject talkingObj,DialoguePopUp.BalloonType balloonType,bool isNewObject, bool isShouting, float liveTime)
	{
        //when the talking object isn't a shouting npc
        if (!isShouting && !isNewObject) {
            if (_DialoguePopUpObj == null)
            {
                _DialoguePopUpObj = FindUnUsedDialogue(true);
                if(_DialoguePopUpObj == null)
                {
                    /*
                    GameObject popUp;
                    if (DialoguePopUpObj)
                        popUp = Instantiate(DialoguePopUpObj as GameObject);
                    else
                        popUp = Instantiate(Resources.Load<GameObject>("UI/PopUp/DialoguePopUp") as GameObject);
                    _DialoguePopUpObj = popUp;*/
                    _DialoguePopUpObj = MakeNewDialogue();
                }

                _DialoguePopUpObj.name = "BalloonForDialogue";
            }

            _DialoguePopUpObj.SetActive(true);

            DialoguePopUp dialoguePopUp = _DialoguePopUpObj.GetComponent<DialoguePopUp>();

            if (dialoguePopUp != null)
            {
                dialoguePopUp.SetName(name);
                dialoguePopUp.SetMessage(message);
                dialoguePopUp.TalkingObj1 = talkingObj;
                if(dialoguePopUp.IsDieByTime)
                {
                    dialoguePopUp.addDialoguerEvents();
                }
            }
			DetermineBalloonType (_DialoguePopUpObj, balloonType);

		} else {
			if(AreaController.Instance && AreaController.Instance.CurrRoom && AreaController.Instance.CurrRoom.cameras.Length <=0 )
				return;

			GameObject popUp;
            /*
			if (DialoguePopUpObj)
				popUp = Instantiate <GameObject> (DialoguePopUpObj as GameObject);
			else
				popUp = Instantiate (Resources.Load<GameObject> ("UI/PopUp/DialoguePopUp") as GameObject);*/

            //try to find its own shout
            popUp = FindShoutDialogue(talkingObj);

            if (popUp == null)
            {
                //try to find unuseddialogue
                popUp = FindUnUsedDialogue(false);

                //if all dialoguepopup is used, make new
                if (popUp == null)
                    popUp = MakeNewDialogue();
            }

            if (!popUp.activeInHierarchy)
                popUp.SetActive(true);

            if(isShouting)
			    popUp.GetComponentInParent<Canvas> ().sortingOrder = 0;
            DialoguePopUp dialoguePopUp = popUp.GetComponent<DialoguePopUp>();

            if (dialoguePopUp != null)
            {
                if (isShouting)
                {
                    dialoguePopUp.MustInsideScreen = false;
                    dialoguePopUp.SetAsShoutingDialogue();
                    dialoguePopUp.removeDialoguerEvents();
                    dialoguePopUp.SetLiveTime(liveTime);
                }else
                {
                    dialoguePopUp.SetName(name);
                    dialoguePopUp.CloseType = PopUpObject.ClosePopUpType.Destroy;
                }

                dialoguePopUp.SetMessage(message);
                dialoguePopUp.TalkingObj1 = talkingObj;
            }
			DetermineBalloonType (popUp, balloonType);
		}
	}

	private void DetermineBalloonType(GameObject dialogueObject, DialoguePopUp.BalloonType balloonType)
	{
		switch (balloonType) {
		case DialoguePopUp.BalloonType.Normal:
			dialogueObject.GetComponent<DialoguePopUp> ().SetAsNormal ();
			break;
		case DialoguePopUp.BalloonType.Shout:
			dialogueObject.GetComponent<DialoguePopUp> ().SetAsShout ();
			break;
		case DialoguePopUp.BalloonType.Mind:
			dialogueObject.GetComponent<DialoguePopUp> ().SetAsMind ();
			break;
        case DialoguePopUp.BalloonType.Telepathy:
            dialogueObject.GetComponent<DialoguePopUp>().SetAsTelepathy();
            break;
		default:
			dialogueObject.GetComponent<DialoguePopUp> ().SetAsNormal ();
			break;
		}
	}

	void ShowDialogueMarkPopUp(GameObject talkingObj, bool isVisible, bool isActiveAfterDialogue)
	{
		if (isVisible) {
			if (_DialogueMarkPopUpObj == null) {
				GameObject popUp;
				if (DialogueMarkPopUpObj)
					popUp = Instantiate (DialogueMarkPopUpObj as GameObject);
				else
					popUp = Instantiate (Resources.Load<GameObject> ("UI/PopUp/DialogueMarkPopUp") as GameObject);
				_DialogueMarkPopUpObj = popUp;
			} else {
				_DialogueMarkPopUpObj.SetActive (true);
			}

            _DialogueMarkPopUpObj.GetComponent<DialogueMarkPopUp>().SetData(talkingObj, isActiveAfterDialogue);
		} else {
			if (_DialogueMarkPopUpObj != null) {
                if (isActiveAfterDialogue)
                    _DialogueMarkPopUpObj.GetComponent<DialogueMarkPopUp>().HideWhileDialogue();
                else
                {
                    _DialogueMarkPopUpObj.GetComponent<DialogueMarkPopUp>().RemoveDialogueListener();
                    _DialogueMarkPopUpObj.SetActive(false);
                }
			}
		}
	}

	void ShowAmountSetPopUp(int maxAmount, GameObject parentObject, Vector2 addPosition)
	{
        Vector2 position = new Vector2();
        if (_AmountSetPopUpObj == null) {
			GameObject popUp;
			if(AmountSetPopUpObj)
				popUp = Instantiate (AmountSetPopUpObj as GameObject);
			else
				popUp = Instantiate (Resources.Load<GameObject> ("UI/PopUp/AmountSetPopUp") as GameObject);
			_AmountSetPopUpObj = popUp;
		} else {
			_AmountSetPopUpObj.SetActive(true);
		}

//		_AmountSetPopUpObj.transform.position = parentObject.transform.position;
		if (parentObject)
			position = CalculatePosition (_AmountSetPopUpObj.GetComponent<AmountSetData> ().AmountSetRoot, parentObject);
		else {
			RectTransform parent = _AmountSetPopUpObj.GetComponent<RectTransform> ();
			RectTransform target = _AmountSetPopUpObj.GetComponent<AmountSetData> ().AmountSetRoot.GetComponent<RectTransform> ();
			
			position.x = (-(target.anchorMax.x - 0.5f) ) * parent.rect.size.x;
			position.y = (-(target.anchorMax.y - 0.5f) ) * parent.rect.size.y;
            _AmountSetPopUpObj.GetComponent<AmountSetData>().AmountSetRoot.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);
            _AmountSetPopUpObj.GetComponent<AmountSetData> ().AmountSetRoot.GetComponent<RectTransform> ().anchoredPosition = position;
		}
        _AmountSetPopUpObj.GetComponent<AmountSetData>().AmountSetRoot.GetComponent<RectTransform>().anchoredPosition = position + addPosition;
        _AmountSetPopUpObj.GetComponent<AmountSetData>().MaxAmount = maxAmount;
        _AmountSetPopUpObj.GetComponent<AmountSetData>().UpdateAmount();
	}

	Vector2 CalculatePosition(GameObject child, GameObject parent)
	{
		RectTransform oriRT = parent.GetComponent<RectTransform> ();
		RectTransform childRT = child.GetComponent<RectTransform> ();
		Vector2 desiredPosition;
		
		childRT.position = oriRT.position;
		
//		if (childRT.anchorMax == childRT.anchorMin) {
			desiredPosition = childRT.anchoredPosition;
			
			desiredPosition.x += ((childRT.anchorMax.x - oriRT.pivot.x)*oriRT.rect.width);
			desiredPosition.y += ((childRT.anchorMax.y - oriRT.pivot.y)*oriRT.rect.height);
			childRT.anchoredPosition = desiredPosition;			
//		}

		child.GetComponent<RectTransform> ().anchoredPosition = childRT.anchoredPosition;
        return childRT.anchoredPosition;

    }

    IEnumerator WaitToSelectButton(Button btn)
    {
        yield return null;
        btn.Select();
    }
}
