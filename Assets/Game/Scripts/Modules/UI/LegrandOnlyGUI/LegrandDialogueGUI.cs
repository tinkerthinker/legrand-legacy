﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using TeamUtility.IO;
using DG.Tweening;

public class LegrandDialogueGUI : MonoBehaviour
{
    #region GUI Element dialogue
    public GameObject DialogCanvas;
    private GameObject BoxImage;
    public Text NameText;
    public Text DialogueText;
    public RectTransform DialogueArrow;

    public GameObject LeftBox;

    public Text LeftNameText;
    public Text LeftDialogueText;
    public RectTransform LeftDialogueArrow;

    public GameObject RightBox;

    public Text RightNameText;
    public Text RightDialogueText;
    public RectTransform RightDialogueArrow;

    public GameObject MiddleBox;

    public Text MiddleNameText;
    public Text MiddleDialogueText;
    public RectTransform MiddleDialogueArrow;

    public GameObject NarrationBox;
    public Text NarrationText;

    public GameObject BranchedTextPanel;
    public Button BranchedTextButtonPref;
    public ScrollController ScrollerController;
    private List<Button> BranchedButtons = new List<Button>();

    private bool _IsChoicePopped = false;
    #endregion

    public WwiseManager.UIFX BaloonTextSound;
    public WwiseManager.UIFX Live2DTextSound;

    //	public AudioSource audioText;
    //	public AudioSource audioTextEnd;
    //	public AudioSource audioGood;
    //	public AudioSource audioBad;

    public Dictionary<string, GameObject> TalkingObjects = new Dictionary<string, GameObject>();

    private bool _dialogue;
    private bool _showDialogueBox;

    // DIALOGUER VARS	
    private string _windowTargetText = string.Empty;
    private string _windowCurrentText = string.Empty;

    private int _IndexCurrentText = 0;
    private bool _IsInRichText = false;
    private List<int[]> _IndexRichText = new List<int[]>();// int[4];//0 = "<" pertama, 1 = ">" pertama, 2 = "<" kedua, 3 = ">" kedua
    int _CurrIndexRichGroup = 0;
    List<int> _SubstractedIndexRichText = new List<int>();// store all location of '<' and '>' in rich text group, but does not know which bracket index is been paired with.

    private string _nameText = string.Empty;

    private bool _isBranchedText;
    private string[] _branchedTextChoices;
    private int _currentChoice;

    // TWEEN VARS
    private float _windowTweenValue;
    private bool _windowReady;

    private float _nameTweenValue;

    // TEXT VARS
    private int _textFrames = int.MaxValue;

    private bool _confirmInUse = true;
    private bool _isOnInitConfirmDown = true;

    Dictionary<string, Color> NameColor;

    private Tween _BlinkTween;
    private float BlinkTime = 0.5f;
    bool _DialogueContinued = false;

    bool _IsNewBalloon = false;

    [SerializeField]
    private bool SkipText;
    // Use this for initialization
    void Start()
    {

        addDialoguerEvents();

        _showDialogueBox = false;

        //Invoke("startWindowTweenIn", 1);
        //Invoke("startWindowTweenOut", 5);
    }

    void OnDestroy()
    {
        removeDialoguerEvents();
    }

    // Update is called once per frame
    void Update()
    {
        #if UNITY_EDITOR
        if (InputManager.GetKeyDown(KeyCode.F12)) SkipText = !SkipText;
        #endif
        if (!_dialogue || Time.timeScale == 0) return;

        if (_windowReady) 
            calculateText();

        if (_DialogueContinued) return;

        if (!InputManager.GetButtonDown("Confirm")) _isOnInitConfirmDown = false;

        if (!_isBranchedText)
        {
            if (SkipText)
            {
                Dialoguer.ContinueDialogue(0);
                return;
            }
            //if(InputManager.GetButtonUp("Confirm"))
            if (InputManager.GetButtonDown("Confirm"))
            {
                if (_windowCurrentText == _windowTargetText && !_confirmInUse && !_isOnInitConfirmDown)
                {
                    if (!Dialoguer.GetGlobalBoolean(2))
                        WwiseManager.Instance.PlaySFX(BaloonTextSound);
                    else
                        WwiseManager.Instance.PlaySFX(Live2DTextSound);

                    _confirmInUse = true;
                    StopBlinkEndDialog();
                    _DialogueContinued = true;
                    Dialoguer.ContinueDialogue(0);
                }
                else
                {
                    if (!_confirmInUse)
                        WwiseManager.Instance.PlaySFX(Live2DTextSound);
                    _windowCurrentText = _windowTargetText;
                    //audioTextEnd.Play();
                    if (DialogueText)
                        DialogueText.text = _windowCurrentText;
                    BlinkEndDialog();
                    EventManager.Instance.TriggerEvent(new DialogTextComplete());
                }
            }
        }
        else
        {
            //if(InputManager.GetButtonUp("Confirm"))
            if (InputManager.GetButtonDown("Confirm"))
            {
                if (_windowCurrentText == _windowTargetText)
                {
                    //Dialoguer.ContinueDialogue(_currentChoice);
                    if (!_IsChoicePopped)
                    {
                        if (DialogCanvas)
                            DialogCanvas.gameObject.SetActive(true);
                        BranchedTextPanel.SetActive(true);
                        showChoiceButtons();
                        //ChoiceScrollbar.value = 1;
                        _IsChoicePopped = true;
                    }
                }
                else
                {
                    if (!_confirmInUse)
                        WwiseManager.Instance.PlaySFX(Live2DTextSound);

                    _windowCurrentText = _windowTargetText;
                    //audioTextEnd.Play();
                    DialogueText.text = _windowCurrentText;
                    WwiseManager.Instance.PlaySFX(Live2DTextSound);
                    BlinkEndDialog();
                    EventManager.Instance.TriggerEvent(new DialogTextComplete());
                }
            }
            /*
            if(InputManager.GetButtonDown("Horizontal")){
                _currentChoice = (int)Mathf.Repeat(_currentChoice + 1, _branchedTextChoices.Length);
                audioText.Play();
            }
            if(InputManager.GetButtonDown("Horizontal")){
                _currentChoice = (int)Mathf.Repeat(_currentChoice - 1, _branchedTextChoices.Length);
                audioText.Play();
            }
            */
        }
    }

    void showChoiceButtons()
    {
        for (int i = 0; i < _branchedTextChoices.Length; i++)
        {
            Button newChoiceButton = Instantiate(BranchedTextButtonPref) as Button;
            newChoiceButton.transform.SetParent(BranchedTextPanel.transform);
            newChoiceButton.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);

            if (newChoiceButton.GetComponent<DialogueChoice>() == null)
                newChoiceButton.gameObject.AddComponent<DialogueChoice>();

            newChoiceButton.GetComponent<DialogueChoice>().ChoiceDialogueId = i;
            string choice = LegrandBackend.Instance.GetLanguageData(_branchedTextChoices[i]);
            if (string.IsNullOrEmpty(choice))
                choice = _branchedTextChoices[i];
            newChoiceButton.GetComponentInChildren<Text>().text = choice;

            //newChoiceButton.GetComponent<ScrollUpDown> ().scrollBar = ChoiceScrollbar;

            BranchedButtons.Add(newChoiceButton);
        }

        ScrollerController.Reset();
        if (_branchedTextChoices.Length > ScrollerController.SeenRow)
            ScrollerController.ScrollbarTarget.gameObject.SetActive(true);
        else
            ScrollerController.ScrollbarTarget.gameObject.SetActive(false);
        for (int i = 0; i < BranchedButtons.Count; i++)
        {
            ScrollerController.AddObjectChild(BranchedButtons[i].gameObject);
        }
        StartCoroutine(WaitBranchedButtonReady());
    }

    IEnumerator WaitBranchedButtonReady()
    {
        yield return null;
        if (BranchedButtons.Count > 0)
            BranchedButtons[0].Select();
        else
            BranchedTextPanel.GetComponentInChildren<Button>().Select();
        ScrollerController.ScrollbarTarget.value = 1; ;
    }

    void destroyChoiceButtons()
    {
        //		foreach (DialogueChoice obj in BranchedTextPanel.GetComponentsInChildren<DialogueChoice>()) {
        //			if(obj.gameObject == BranchedTextPanel)
        //				continue;
        //			GameObject.Destroy(obj.gameObject);
        //		}
        foreach (Button obj in BranchedButtons)
        {
            GameObject.Destroy(obj.gameObject);
        }
        BranchedButtons.RemoveAll((Button obj) => obj);
    }

    #region Dialoguer
    public void addDialoguerEvents()
    {
        Dialoguer.events.onStarted += onDialogueStartedHandler;
        Dialoguer.events.onInstantlyEnded += onDialogueInstantlyEndedHandler;
        Dialoguer.events.onTextPhase += onDialogueTextPhaseHandler;
        Dialoguer.events.onWindowClose += onDialogueWindowCloseHandler;
        Dialoguer.events.onMessageEvent += onDialoguerMessageEvent;
    }

    public void removeDialoguerEvents()
    {
        Dialoguer.events.onStarted -= onDialogueStartedHandler;
        Dialoguer.events.onInstantlyEnded -= onDialogueInstantlyEndedHandler;
        Dialoguer.events.onTextPhase -= onDialogueTextPhaseHandler;
        Dialoguer.events.onWindowClose -= onDialogueWindowCloseHandler;
        Dialoguer.events.onMessageEvent -= onDialoguerMessageEvent;
    }

    private void onDialogueStartedHandler()
    {
        _dialogue = true;
        _DialogueContinued = false;
        //		if (SelectedGameObjectModule.current != null)
        //			SelectedGameObjectModule.current.HideMarkObject ();
    }

    private void onDialogueInstantlyEndedHandler()
    {
        _dialogue = false;
        _showDialogueBox = false;
    }

    void AddNewIndexRichText()
    {
        int[] newIndexRichText = new int[4];//0 = '<' pertama, 1 = '>' pertama, 2 = '<' kedua, 3 = '>' kedua
        for (int i = 0; i < 4; i++)
        {
            newIndexRichText[i] = 0;
        }
        _IndexRichText.Add(newIndexRichText);
    }

    private void onDialogueTextPhaseHandler(DialoguerTextData data)
    {
        _DialogueContinued = false;
        _confirmInUse = false;
        _isOnInitConfirmDown = InputManager.GetButtonDown("Confirm") ? true : false;

        _IsInRichText = false;

        _IndexCurrentText = 0;

        StopBlinkEndDialog();

        if (!Dialoguer.GetGlobalBoolean(2))
        {
            if (_IsNewBalloon)
                PopUpUI.CloseAllPopUp();

            if (DialogCanvas)
                DialogCanvas.gameObject.SetActive(false);
            string[] keyObject = data.metadata.Split(';');//.Replace(" ", "");
            string[] nameObject = data.name.Split(';');
            string[] messages = LegrandBackend.Instance.GetLanguageData(data.text, true).Split(';');
            for(int i = 0; i < messages.Length; i++)
                messages [i] = LegrandSharedEvents.ParseTextData(messages[i]);
            //if (keyObject.Equals(""))
            if (keyObject[0].Equals(""))
                //keyObject = data.name;//.Replace(" ", "");
                keyObject = nameObject;

            for (int i = 0; i < keyObject.Length; i++)
            {
                if (GlobalGameStatus.Instance.RegisteredObject.ContainsKey(keyObject[i]))
                {
                    DialoguePopUp.BalloonType ballonType;

                    switch (data.portrait.ToLower())
                    {
                        case "normal":
                            ballonType = DialoguePopUp.BalloonType.Normal;
                            break;
                        case "shout":
                            ballonType = DialoguePopUp.BalloonType.Shout;
                            break;
                        case "mind":
                            ballonType = DialoguePopUp.BalloonType.Mind;
                            break;
                        case "norn":
                            ballonType = DialoguePopUp.BalloonType.Telepathy;
                            break;
                        default:
                            ballonType = DialoguePopUp.BalloonType.Normal;
                            break;
                    }

                    string message;
                    if (i >= messages.Length)
                        message = messages[messages.Length - 1];
                    else
                        message = messages[i];

                    if (i == 0)
                        _IsNewBalloon = false;
                    else
                        _IsNewBalloon = true;

                    PopUpUI.CallDialoguePopUp(nameObject[i], message, GlobalGameStatus.Instance.RegisteredObject[keyObject[i]], ballonType, _IsNewBalloon);
                }
            }

            _isBranchedText = data.windowType == DialoguerTextPhaseType.BranchedText;
            _branchedTextChoices = data.choices;
            _currentChoice = 0;
            _IsChoicePopped = false;

            return;

            /*
                if (GlobalGameStatus.Instance.RegisteredObject.ContainsKey(keyObject))
                {
                    DialoguePopUp.BalloonType ballonType;

                    switch (data.portrait.ToLower())
                    {
                        case "normal":
                            ballonType = DialoguePopUp.BalloonType.Normal;
                            break;
                        case "shout":
                            ballonType = DialoguePopUp.BalloonType.Shout;
                            break;
                        case "mind":
                            ballonType = DialoguePopUp.BalloonType.Mind;
                            break;
                        case "norn":
                            ballonType = DialoguePopUp.BalloonType.Telepathy;
                            break;
                        default:
                            ballonType = DialoguePopUp.BalloonType.Normal;
                            break;
                    }
                    //_isBranchedText = false;
                    _isBranchedText = data.windowType == DialoguerTextPhaseType.BranchedText;
                    _branchedTextChoices = data.choices;
                    _currentChoice = 0;
                    _IsChoicePopped = false;
                    PopUpUI.CallDialoguePopUp(data.name, data.text, GlobalGameStatus.Instance.RegisteredObject[keyObject], ballonType, false);
                    return;
                }*/
        }

        //string metaPosition = !string.IsNullOrEmpty(data.metadata) ? data.metadata.Substring(0, 1) : "";
        string metaPosition = data.metadata.Split(',')[0];
        //Try to set which one should appear, left or right box
        switch (metaPosition)
        {
            case "2":
                LeftBox.SetActive(false);
                RightBox.SetActive(true);
                MiddleBox.SetActive(false);
                NarrationBox.SetActive(false);

                BoxImage = RightBox;
                NameText = RightNameText;
                DialogueText = RightDialogueText;
                DialogueArrow = RightDialogueArrow;
                break;
            case "1":
                LeftBox.SetActive(true);
                RightBox.SetActive(false);
                MiddleBox.SetActive(false);
                NarrationBox.SetActive(false);

                BoxImage = LeftBox;
                NameText = LeftNameText;
                DialogueText = LeftDialogueText;
                DialogueArrow = LeftDialogueArrow;
                break;
            case "narration":
                LeftBox.SetActive(false);
                RightBox.SetActive(false);
                MiddleBox.SetActive(false);
                NarrationBox.SetActive(true);

                BoxImage = NarrationBox;
                NameText = null;
                DialogueText = NarrationText;
                DialogueArrow = null;
                break;
            default:
                LeftBox.SetActive(false);
                RightBox.SetActive(false);
                MiddleBox.SetActive(true);
                NarrationBox.SetActive(false);

                BoxImage = MiddleBox;
                NameText = MiddleNameText;
                DialogueText = MiddleDialogueText;
                DialogueArrow = MiddleDialogueArrow;
                break;
        }

        if(!metaPosition.Equals("narration"))
        {
            switch(data.portrait)
            {
                case "shout":
                    BoxImage.GetComponent<DialogueBoxLive2D>().SetAsShout();
                    break;
                case "norn":
                    BoxImage.GetComponent<DialogueBoxLive2D>().SetAsTelephaty();
                    break;
                default:
                    BoxImage.GetComponent<DialogueBoxLive2D>().SetAsNormal();
                    break;
            }
        }

        _windowCurrentText = string.Empty;
        //_windowTargetText = data.text;
        _windowTargetText = LegrandBackend.Instance.GetLanguageData(data.text);
        if (string.IsNullOrEmpty(_windowTargetText))
            _windowTargetText = data.text;
        
        _windowTargetText = LegrandSharedEvents.ParseTextData(_windowTargetText);
        //_nameText = data.name;
        _nameText = data.name;
        if (_nameText == null)
            _nameText = data.name;


        _showDialogueBox = true;

        _isBranchedText = data.windowType == DialoguerTextPhaseType.BranchedText;
        _branchedTextChoices = data.choices;
        _currentChoice = 0;

        //startWindowTweenIn();

        // test process with UI
        //_windowReady = true;

        BoxImage.transform.parent.gameObject.SetActive(true);

        if (Dialoguer.GetGlobalBoolean(2))
        {
            if (DialogCanvas)
                DialogCanvas.gameObject.SetActive(true);
            BoxImage.gameObject.SetActive(true);
        }
        else
        {
            BoxImage.gameObject.SetActive(false);
        }

        if (NameText != null)
        {
            if (data.name.Equals(string.Empty))
                NameText.transform.parent.gameObject.SetActive(false);
            else
                NameText.transform.parent.gameObject.SetActive(true);
            NameText.text = _nameText;
        }

        if (!_isBranchedText)
            BranchedTextPanel.SetActive(false);
        if (BranchedButtons.Count > 0)
        {
            destroyChoiceButtons();
        }
        _IsChoicePopped = false;

        /*
        if (NameText)
        {
            NameText.gameObject.SetActive(true);
            NameText.text = _nameText;
        }*/

        DialogueText.gameObject.SetActive(true);
        DialogueText.text = _windowCurrentText;

        //_windowCurrentText = _windowTargetText;
        windowInComplete();
    }

    private void onDialogueWindowCloseHandler()
    {
        //startWindowTweenOut();

        // test process with UI
        _windowReady = false;

        if (DialogCanvas)
            DialogCanvas.gameObject.SetActive(false);
        else
            BoxImage.transform.parent.gameObject.SetActive(false);
        //NameText.text = string.Empty;
        //DialogueText.text = string.Empty;
        BranchedTextPanel.SetActive(false);
        destroyChoiceButtons();
        //		if (SelectedGameObjectModule.current != null)
        //			SelectedGameObjectModule.current.ShowMarkObject ();

        windowOutComplete();
    }

    private void onDialoguerMessageEvent(string message, string metadata)
    {

    }

    public void BlinkEndDialog()
    {
        StopBlinkEndDialog();
        if (DialogueArrow != null)
        {
            DialogueArrow.anchoredPosition = new Vector2(0f, 0f);
            DialogueArrow.gameObject.SetActive(true);
            _BlinkTween = DialogueArrow.DOLocalMoveY(3f, BlinkTime).SetLoops(-1, LoopType.Yoyo);
        }
    }

    public void StopBlinkEndDialog()
    {
        if (_BlinkTween != null)
            _BlinkTween.Kill(true);
        if (DialogueArrow != null)
            DialogueArrow.gameObject.SetActive(false);

    }
    #endregion

    #region Tween Handlers
    private void updateWindowTweenValue(float newValue)
    {
        _windowTweenValue = newValue;
    }

    private void windowInComplete()
    {
        _windowReady = true;
    }

    private void windowOutComplete()
    {
        _showDialogueBox = false;

        _dialogue = false;

        if (EventManager.Instance != null) EventManager.Instance.TriggerEvent(new DialogWindowClosed());
    }
    #endregion

    #region Utils
    private void calculateText()
    {
        // char per char text

        if (_windowTargetText == string.Empty || _windowCurrentText == _windowTargetText || Time.timeScale == 0) return;

        int frameSkip = 1;

        if (_textFrames < frameSkip)
        {
            _textFrames += 1;
            return;
        }
        else
        {
            _textFrames = 0;
        }

        int charsPerInterval = 1;

        if (!_windowCurrentText.Equals(_windowTargetText))
        {
            for (int i = 0; i < charsPerInterval; i += 1)
            {
                if (_windowTargetText.Length <= _IndexCurrentText) break;

                //rich text group detected
                if (_windowTargetText[_IndexCurrentText].Equals('<') && !_IsInRichText)
                {
                    _IndexRichText.Clear();
                    _SubstractedIndexRichText.Clear();

                    bool firstBracket = true;
                    bool finishRichSign = false;
                    int maxIndex = 0;
                    _CurrIndexRichGroup = maxIndex;

                    _IsInRichText = true;
                    AddNewIndexRichText();
                    _IndexRichText[_CurrIndexRichGroup][0] = _IndexCurrentText;
                    _SubstractedIndexRichText.Add(_IndexCurrentText);

                    //searching the whole rich text group
                    do
                    {
                        if (_windowTargetText[_IndexCurrentText].Equals('>'))
                        {
                            if (firstBracket)
                            {
                                firstBracket = false;
                                _IndexRichText[_CurrIndexRichGroup][1] = _IndexCurrentText;
                                _SubstractedIndexRichText.Add(_IndexCurrentText);
                            }
                            else
                            {
                                _IndexRichText[_CurrIndexRichGroup][3] = _IndexCurrentText;
                                _SubstractedIndexRichText.Add(_IndexCurrentText);

                                if (_CurrIndexRichGroup == 0)
                                    finishRichSign = true;
                                else
                                {
                                    do
                                    {
                                        _CurrIndexRichGroup--;
                                    } while (_IndexRichText[_CurrIndexRichGroup][3] != 0);
                                }
                            }
                        }
                        else if (_windowTargetText[_IndexCurrentText].Equals('<') && _windowTargetText[_IndexCurrentText + 1].Equals('/')) // check if "</" found
                        {
                            _IndexRichText[_CurrIndexRichGroup][2] = _IndexCurrentText;
                            _SubstractedIndexRichText.Add(_IndexCurrentText);
                        }
                        else if (_IndexCurrentText != _IndexRichText[0][0] && _windowTargetText[_IndexCurrentText].Equals('<'))
                        {
                            firstBracket = true;
                            maxIndex++;
                            _CurrIndexRichGroup = maxIndex;

                            AddNewIndexRichText();

                            _IndexRichText[_CurrIndexRichGroup][0] = _IndexCurrentText;
                            _SubstractedIndexRichText.Add(_IndexCurrentText);
                        }
                        _IndexCurrentText++;
                    } while (!finishRichSign);

                    //_SubstractedIndexRichText.Clear();
                    //foreach (int[] idr in _IndexRichText)
                    //{
                    //    foreach (int ii in idr)
                    //    {
                    //        _SubstractedIndexRichText.Add(ii);
                    //    }
                    //}
                    //_SubstractedIndexRichText.Sort();

                    for (int index = 0; index < _SubstractedIndexRichText.Count - 1; index += 2)
                    {
                        _windowCurrentText += (_windowTargetText.Substring(_SubstractedIndexRichText[index], (_SubstractedIndexRichText[index + 1] - _SubstractedIndexRichText[index] + 1)));
                    }

                    //substract the bracket and add it to string text
                    /*
                    for (int index = 0; index < _IndexRichText.Count; index++)
                    {
                        _windowCurrentText += (_windowTargetText.Substring(_IndexRichText[index][0], (_IndexRichText[index][1] - _IndexRichText[index][0] + 1)))
                                            + (_windowTargetText.Substring(_IndexRichText[index][2], (_IndexRichText[index][3] - _IndexRichText[index][2] + 1)));
                    }*/
                    //_windowCurrentText += (_windowTargetText.Substring(_IndexRichText[0], (_IndexRichText[1] - _IndexRichText[0] + 1)))
                    //                    + (_windowTargetText.Substring(_IndexRichText[2], (_IndexRichText[3] - _IndexRichText[2] + 1)));

                    _IndexCurrentText = _SubstractedIndexRichText[1] + 1;
                    _CurrIndexRichGroup = 0;
                }

                if (_IsInRichText)
                {
                    /*
                    //check if currentindex is in opening bracket one of rich group
                    for (int index = 0; index < _IndexRichText.Count; index++)
                    {
                        if (_IndexCurrentText == _IndexRichText[index][0])
                        {
                            _IndexCurrentText = _IndexRichText[index][1] + 1;
                            _CurrIndexRichGroup = index;
                        }
                    }

                    //check if currentindex is in closing bracket one of rich group
                    for (int index = 0; index < _IndexRichText.Count; index++)
                    {
                        if (_IndexCurrentText == _IndexRichText[index][2])
                        {
                            _IndexCurrentText = _IndexRichText[index][3] + 1;
                            _CurrIndexRichGroup = index;
                        }
                    }*/

                    for (int index = 0; index < _SubstractedIndexRichText.Count; index++)
                    {
                        if (_IndexCurrentText == _SubstractedIndexRichText[index])
                        {
                            _IndexCurrentText = _SubstractedIndexRichText[index+1] + 1;
                            //_CurrIndexRichGroup = index;
                        }
                    }

                    _windowCurrentText = _windowCurrentText.Insert(_IndexCurrentText, _windowTargetText[_IndexCurrentText].ToString());
                    _IndexCurrentText++;

                    //check if the next char is rich bracket
                    //if current index is in the end of rich text group
                    //if (_CurrIndexRichGroup == _IndexRichText.Count - 1 && _IndexCurrentText == _IndexRichText[_CurrIndexRichGroup][2])
                    //{
                    //    _IsInRichText = false;
                    //    _IndexCurrentText = _IndexRichText[_CurrIndexRichGroup][3] + 1;
                    //}

                    if (_IndexCurrentText >= _SubstractedIndexRichText[_SubstractedIndexRichText.Count - 1])
                    {
                        _IsInRichText = false;
                    }/*
                    else
                    {
                        if (_IndexCurrentText == _SubstractedIndexRichText[2] && _SubstractedIndexRichText.Count == 4)
                        {
                            _IsInRichText = false;
                            _IndexCurrentText = _SubstractedIndexRichText[3] + 1;
                        }
                        else //if(_IndexCurrentText == _SubstractedIndexRichText[2])
                        {
                            while (_IndexCurrentText == _SubstractedIndexRichText[2])
                            {
                                _IndexCurrentText = _SubstractedIndexRichText[3] + 1;
                                _SubstractedIndexRichText.RemoveRange(0, 2);

                                if (_SubstractedIndexRichText.Count == 2)
                                {
                                    _IsInRichText = false;
                                    _IndexCurrentText = _SubstractedIndexRichText[1] + 1;
                                    break;
                                }
                            }
                        }
                    }*/

                    /*
                    if (_IndexCurrentText < _IndexRichText[_CurrIndexRichGroup][2])
                    {
                        _windowCurrentText = _windowCurrentText.Insert(_IndexCurrentText, _windowTargetText[_IndexCurrentText].ToString());
                        _IndexCurrentText++;
                        if (_IndexCurrentText == _IndexRichText[_CurrIndexRichGroup][2])
                        {
                            //if current index is in the end of rich text group
                            if (_CurrIndexRichGroup == _IndexRichText.Count - 1)
                            {
                                _IsInRichText = false;
                                _IndexCurrentText = _IndexRichText[_CurrIndexRichGroup][3] + 1;
                            }
                            else
                            {
                                for (int index = 0; index < _IndexRichText.Count; index++)
                                {
                                    if (_IndexCurrentText == _IndexRichText[index][0])
                                    {
                                        _IndexCurrentText = _IndexRichText[index][1] + 1;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    */
                }
                else
                {
                    _windowCurrentText += _windowTargetText[_IndexCurrentText];
                    _IndexCurrentText++;
                }
            }
        }

        //		audioText.Play();

        DialogueText.text = _windowCurrentText;

        if (_windowCurrentText == _windowTargetText)
        {
            BlinkEndDialog();
            EventManager.Instance.TriggerEvent(new DialogTextComplete());
            _windowReady = false;
        }
        // straight text
        //		_windowCurrentText = _windowTargetText;
        //		DialogueText.text = _windowCurrentText;
    }
    #endregion
}
