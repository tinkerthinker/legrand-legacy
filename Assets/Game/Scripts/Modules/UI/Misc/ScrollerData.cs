﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class ScrollerData : MonoBehaviour, ISelectHandler {
    public ScrollController Controller;
    private int Index;

    public void SetIndex(int index)
    {
        Index = index;
    }

    public void OnSelect(BaseEventData eventData)
    {
        Controller.ScrollTo(Index);
    }    
}
