﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ChangeGraphicByEvent : MonoBehaviour, ISelectHandler, IDeselectHandler, ICancelHandler, ISubmitHandler {
	[System.Serializable]
	public class ImageAndSprite
	{
		public Image TargetImage;
		public Sprite TargetSprite;
        public bool AsOverrideSprite;
	}

	public enum ProcessType
	{
		Hide,
		Show,
		ChangeColor,
		ChangeSprite
	}

	public BaseEnum.EventType ChgEventType;
	public ProcessType ChangeGraphicType;

	public Color TargetColor;
	
	public MaskableGraphic[] TargetGraphics;
	[Header("Only Used if change graphic type is ChangeSprite")]
	public ImageAndSprite[] TargetImages;
    [Header("Only Used if change graphic type is Hide or Show and the object don't have graphic component")]
    public GameObject[] TargetObject;

	void Process()
	{
		switch (ChangeGraphicType) {
		case ProcessType.Hide:
			Hide ();
			break;
		case ProcessType.Show:
			Show ();
			break;
		case ProcessType.ChangeColor:
			ChangeColor ();
			break;
		case ProcessType.ChangeSprite:
			ChangeSprite ();
			break;
		}
	}

	void Hide()
	{
		for (int i = 0; i < TargetGraphics.Length; i++) {
			TargetGraphics [i].gameObject.SetActive (false);
		}

        if (TargetObject.Length > 0)
        {
            for (int i = 0; i < TargetObject.Length; i++)
            {
                TargetObject[i].SetActive(false);
            }
        }
	}

	void Show()
	{
		for (int i = 0; i < TargetGraphics.Length; i++) {
			TargetGraphics [i].gameObject.SetActive (true);
		}

        if (TargetObject.Length > 0)
        {
            for (int i = 0; i < TargetObject.Length; i++)
            {
                TargetObject[i].SetActive(true);
            }
        }
	}

	void ChangeColor()
	{
		for (int i = 0; i < TargetGraphics.Length; i++) {
			TargetGraphics [i].color = TargetColor;
		}
	}

	void ChangeSprite()
	{
		for (int i = 0; i < TargetImages.Length; i++) {
            if (TargetImages[i].AsOverrideSprite)
            {
                if (TargetImages[i].TargetImage.overrideSprite != TargetImages[i].TargetSprite)
                    TargetImages[i].TargetImage.overrideSprite = TargetImages[i].TargetSprite;
            }
            else
            {
                if (TargetImages[i].TargetImage.sprite != TargetImages[i].TargetSprite)
                    TargetImages[i].TargetImage.sprite = TargetImages[i].TargetSprite;
            }
		}
	}


	void OnEnable()
	{
		if (ChgEventType == BaseEnum.EventType.OnEnable)
			Process ();
	}

	void OnDisable()
	{
		if (ChgEventType == BaseEnum.EventType.OnDisable || ChgEventType == BaseEnum.EventType.OnDeselect)
			Process ();
	}

	#region ISelectHandler implementation
	
	public void OnSelect (BaseEventData eventData)
	{
		if (ChgEventType == BaseEnum.EventType.OnSelect)
			Process ();
	}
	
	#endregion
	
	#region IDeselectHandler implementation
	
	public void OnDeselect (BaseEventData eventData)
	{
		if (ChgEventType == BaseEnum.EventType.OnDeselect)
			Process ();

	}
	
	#endregion

	#region ICancelHandler implementation

	public void OnCancel (BaseEventData eventData)
	{
		if (ChgEventType == BaseEnum.EventType.OnCancel)
			Process ();
	}

	#endregion

    public void OnSubmit(BaseEventData eventData)
    {
        if (ChgEventType == BaseEnum.EventType.OnClick)
            Process();
    }
}
