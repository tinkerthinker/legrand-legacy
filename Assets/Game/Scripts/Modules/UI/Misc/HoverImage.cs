﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HoverImage : MonoBehaviour {
    //public float fadeMultiplier=0.001f;
    //public float HeightReference;
    //public float WidthReference;
    //public int MaxRowReference;
    //public int MaxColReference;

	public float HeightAddition;

    private RectTransform _RefRT;


	void Start () {
	}

	void Update () {

	}

    void OnEnable()
    {
        if(_RefRT != null)
            UpdatePosition(_RefRT);
    }

	public void UpdatePosition(int row, int col)
	{
        gameObject.SetActive(true);

		RectTransform rt = GetComponent<RectTransform> ();
		float x = 340 - (col * 240);
		float y = 0;
		rt.anchoredPosition = new Vector2(x, y);
	}

	public void UpdatePosition(RectTransform refPosition)
	{
        gameObject.SetActive(true);

        _RefRT = refPosition;
		RectTransform rt = GetComponent<RectTransform> ();
		float x = refPosition.position.x;
		float y = refPosition.position.y;

		Vector2 newPos = refPosition.position;

		rt.position = newPos;
	}
}
