﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;

public class DatabaseImageSprite:MonoBehaviour{
	[System.Serializable]
	public class TargetImage
	{
		public TargetType target;
		public Sprite Image;
	}

	private static DatabaseImageSprite s_Instance = null;
	public static DatabaseImageSprite Instance
	{
		get
		{
			if (s_Instance == null)
			{
				s_Instance = GameObject.FindObjectOfType(typeof(DatabaseImageSprite)) as DatabaseImageSprite;
			}
			return s_Instance;
		}
	}

	#region character sprite
	public Sprite Finn;
	public Sprite Geddo;
	public Sprite Eris;
	public Sprite Aria;
	public Sprite Kael;
	public Sprite Scatia;
	public Sprite Azzam;
	#endregion

	#region button sprite
	public Sprite ButtonSelected;
	public Sprite ButtonDeselected;
	#endregion

	#region magic tweaker bar sprite
	public Sprite MagicTweakBarValue;
	public Sprite MagicTweakBarUnvalue;
	#endregion

	#region item sprite
	public Sprite pot1;
	public Sprite pot2;
	#endregion

	#region magic icon
	public Sprite magicFire;
	public Sprite magicWater;
	public Sprite magicEarth;
	public Sprite magicAir;
	public Sprite magicLightning;
	public Sprite magicLight;
	public Sprite magicDark;
	#endregion

	public List<TargetImage> TargetImages;
	private Dictionary<TargetType, Sprite> _AOEImages = new Dictionary<TargetType, Sprite>();

	void Awake()
	{
		foreach (TargetImage img in TargetImages) {
			if (_AOEImages.ContainsKey (img.target))
				_AOEImages [img.target] = img.Image;
			else
				_AOEImages.Add(img.target, img.Image);
		}
	}

	public Sprite GetSpriteCharByName(string nameChar)
	{

		switch (nameChar.ToLower()) {
		case "フィン":
		case "finn":
			return Finn;
		
		case "ゲド":
		case "geddo":
			return Geddo;
		
		case "エリス":
		case "eris":
			return Eris;

		case "アリヤ":
		case "aria":
			return Aria;

		case "カエッル":
		case "kael":
			return Kael;

		case "scatia":
			return Scatia;
	
		case "azzam":
			return Azzam;

		default:
			return null;
		}
	}

	public Sprite GetSpriteCharById(int idChar)
	{
		
		switch (idChar) {
		case 0:
			return Finn;
		case 1:
			return Eris;
		case 2:
			return Aria;
		case 3:
			return Kael;
		case 4:
			return Azzam;
		case 5:
			return Scatia;
		default:
			return null;
		}
	}

	public Sprite GetSpriteButtonTabSelected(bool selected)
	{
		if (selected)
			return ButtonSelected;
		else
			return ButtonDeselected;
	}

	public Sprite GetSpriteMagicTweakerBar(bool selected)
	{
		if (selected)
			return MagicTweakBarValue;
		else
			return MagicTweakBarUnvalue;
	}

	public Sprite GetSpriteItemConsumableByType(Consumable.ConsumeType type)
	{
		switch (type) {
		case Consumable.ConsumeType.Healing:
			return pot1;
		case Consumable.ConsumeType.Magic:
			return pot2;
		default:
			return null;
		}
	}

	public Sprite GetSpriteItemByName(string nameItem)
	{
		switch (nameItem) {
		case "pot 1":
			return pot1;
		case "pot 2":
			return pot2;
		default:
			return null;
		}
	}

	public Sprite GetSpriteItemByType (Item.Type type)
	{
		switch (type) {
		case Item.Type.Consumable:
			return pot1;
		default:
			return null;
		}
	}

	public Sprite GetSpriteMagicIconElement (Element element)
	{
		switch (element) {
		case Element.Air:
			return magicAir;
		case Element.Dark:
			return magicDark;
		case Element.Earth:
			return magicEarth;
		case Element.Fire:
			return magicFire;
		case Element.Light:
			return magicLight;
		case Element.Lighting:
			return magicLightning;
		case Element.Water:
			return magicWater;
		default:
			return null;
		}
	}

	public Sprite GetTargetTypeImage(TargetType target)
	{
		return _AOEImages [target];
	}
}