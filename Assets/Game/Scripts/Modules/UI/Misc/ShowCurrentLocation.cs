﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ShowCurrentLocation : MonoBehaviour {
	public Text LocationText;
    public Image AreaSigil;

	void OnEnable()
	{
        if (AreaController.Instance == null)
            return;
        SetAreaInfo();
	}

    void SetAreaInfo()
    {
        if (LocationText && AreaController.Instance.CurrSubAreaData != null)
        {
            if(AreaController.Instance.CurrAreaData.areaType == Area.AreaTypes.WorldMap)
                LocationText.text = AreaController.Instance.CurrentRegion;
            else
                LocationText.text = AreaController.Instance.CurrSubAreaData.subAreaName;
        }
        if (AreaSigil && AreaController.Instance.CurrAreaData != null)
        {
            if (AreaController.Instance.CurrAreaData.areaType == Area.AreaTypes.WorldMap)
                AreaSigil.sprite = LegrandBackend.Instance.GetAreaSprite(AreaController.Instance.CurrentRegion);
            else
                AreaSigil.sprite = LegrandBackend.Instance.GetAreaSprite(AreaController.Instance.CurrAreaData.areaName);
        }
    }
}
