﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ShowCurrentChapter : MonoBehaviour {
	public Text ChapterText;

	void OnEnable()
	{
		ChapterText.text = PartyManager.Instance.State.currChapter.ToString ();
	}
}
