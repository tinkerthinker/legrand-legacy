﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ShowInventoryMiscData : MonoBehaviour {
	public Text GoldText;

	public Text WeightText;
	public Image weightBar;

    public bool CatchInventoryChangedEvent = false;

	void Start () {
		UpdateAll ();
	}

	void OnEnable()
	{
        if (CatchInventoryChangedEvent)
            EventManager.Instance.AddListener<InventoryChangedEvent>(InventoryChangedEventHandler);
		if (PartyManager.Instance)
			UpdateAll ();
	}

    void OnDisable()
    {
        if (CatchInventoryChangedEvent)
            if(EventManager.Instance)
                EventManager.Instance.RemoveListener<InventoryChangedEvent>(InventoryChangedEventHandler);
    }

    void InventoryChangedEventHandler(InventoryChangedEvent e)
    {
        UpdateAll();
    }

	public void UpdateAll()
	{
		UpdateGold ();
		UpdateWeight ();
	}

	public void UpdateGold()
	{
		if(GoldText)
			GoldText.text = PartyManager.Instance.Inventory.Gold.ToString();
	}

	public void UpdateWeight()
	{
		int currWeight = PartyManager.Instance.Inventory.CurrentWeight;
		int maxWeight = PartyManager.Instance.Inventory.MaxWeight;

		if(WeightText)
			WeightText.text = currWeight+"/"+maxWeight;
		if(weightBar)
			weightBar.fillAmount = (float)currWeight / (float)maxWeight;
	}
}
