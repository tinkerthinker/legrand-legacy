using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class ScrollUpDown : MonoBehaviour, ISelectHandler {
	[SerializeField]
	private int _numberOfStep=0;
	public int numberOfStep{
		get{return _numberOfStep;}
		set{
			_numberOfStep = value;
			isNosAssigned = true;
		}
	}
	private bool isNosAssigned = false;

	[SerializeField]
	private int _Index=0;	//index di scroll list
	public int Index{
		get{return _Index;}
		set{
			_Index = value;
			isIndexAssigned = true;
			isIndexPublic = true;
		}
	}
	private bool isIndexAssigned = false;
	private bool isIndexPublic = false;
	private int parentLength=0;

	private int _MaxIndex;

	public Scrollbar scrollBar;
	public int seenRow = 3;	//total baris yang terlihat di scroll list, bisa jadi 1 baris berisi 1 button atau lebih

	int lastIndex = 0;

	void Start(){
	}

	public void ScrollHere(int input)
	{
		float shouldBeValueScroll;
		//float shouldBeValueScroll = 1f - ( (index-seenButton)*(1f/(numberOfStep-1f)) ); //scroll down
		//float shouldBeValueScroll = (numberOfStep - index)*(1f/(numberOfStep-1f)); //scroll up
		
		// <0 = push up, >0 = push down
		if (_numberOfStep > 1)
		{
			if (input < 0)
			{
				shouldBeValueScroll = (_numberOfStep - _Index) * (1f / (_numberOfStep - 1f)); //scroll up
				if (scrollBar.value > shouldBeValueScroll && _Index != 1)
					shouldBeValueScroll = scrollBar.value;
			} else if (input > 0)
			{
				shouldBeValueScroll = 1f - ((_Index - seenRow) * (1f / (_numberOfStep - 1f))); //scroll down
				if (scrollBar.value < shouldBeValueScroll && _Index != parentLength)
					shouldBeValueScroll = scrollBar.value;
			} else
				shouldBeValueScroll = scrollBar.value;
		}
		else
			shouldBeValueScroll = 1;

		scrollBar.value = shouldBeValueScroll;
	}

	public void OnSelect(BaseEventData eventData)
	{
//		StartCoroutine (WaitToCalculateScroll ());
		WaitToCalculateScroll ();
	}

//	IEnumerator WaitToCalculateScroll()
	void WaitToCalculateScroll()
	{
//		yield return null;
		//SelectedGameObjectModule.current.currentSelectedGameObject = this.gameObject;
		if (!isNosAssigned)
		{
			_numberOfStep = gameObject.transform.parent.gameObject.GetComponentsInChildren<Button> ().Length - (seenRow - 1);
			//isNosAssigned = true;
		}


		if (!isIndexAssigned)
		{
			Button[] parent = gameObject.transform.parent.gameObject.GetComponentsInChildren<Button> ();
			if(parentLength != parent.Length)
			{
				_Index = 0;

				for (int i=0; i<parent.Length; i++) {
					if(parent[i].gameObject.Equals(this.gameObject))
						_Index = i+1;
					if(_Index!=0)
						break;
				}

				parentLength = parent.Length;
			}

		}
		/*
		if (SelectedGameObjectModule.current != null) {
			if (SelectedGameObjectModule.current.LastSelectedGameObject != null && SelectedGameObjectModule.current.LastSelectedGameObject.GetComponent<ScrollUpDown> () != null)
			{
				lastIndex = SelectedGameObjectModule.current.LastSelectedGameObject.GetComponent<ScrollUpDown> ().Index;
			}
			else
				return;
		}
		*/

		/*
		_index = 0;
		
		for (int i=0; i<parent.Length; i++) {
			if(parent[i].gameObject.Equals(this.gameObject))
				_index = i+1;
			if(parent[i].gameObject.Equals(SelectedGameObjectModule.current.lastSelectedGameObject))
				currentIndex = i+1;

			if(_index!=0 && currentIndex!=0)
				break;
		}
		*/

		if(GetLastIndex() != -1)
			ScrollHere (_Index - lastIndex);

		//		if(SelectedGameObjectModule.current !=null)
		//			SelectedGameObjectModule.current.LastSelectedGameObject = this.gameObject;
	}

	int GetLastIndex()
	{
		if (SelectedGameObjectModule.current != null && EventSystem.current != null) {
			if (SelectedGameObjectModule.current.currentSelectedGameObject != EventSystem.current.currentSelectedGameObject) {
				if (SelectedGameObjectModule.current.currentSelectedGameObject != null && SelectedGameObjectModule.current.currentSelectedGameObject.GetComponent<ScrollUpDown> () != null)
					return lastIndex = SelectedGameObjectModule.current.currentSelectedGameObject.GetComponent<ScrollUpDown> ().Index;
			} else {
				if(SelectedGameObjectModule.current.LastSelectedGameObject != null && SelectedGameObjectModule.current.LastSelectedGameObject.GetComponent<ScrollUpDown> () != null)
					return lastIndex = SelectedGameObjectModule.current.LastSelectedGameObject.GetComponent<ScrollUpDown> ().Index;
			}
				
//
//			if (SelectedGameObjectModule.current.LastSelectedGameObject!= null && SelectedGameObjectModule.current.LastSelectedGameObject.GetComponent<ScrollUpDown> () != null)
//			{
//				return lastIndex = SelectedGameObjectModule.current.LastSelectedGameObject.GetComponent<ScrollUpDown> ().Index;
//			}				
		}
		return -1;
	}
}
