﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Collections;

public class CustomEventTrigger : MonoBehaviour, ISelectHandler, IDeselectHandler, ICancelHandler, ISubmitHandler
{
    public BaseEnum.EventType ChgEventType;
    public UnityEvent Event;

    void Process()
    {
        if (Event != null)
            Event.Invoke();
    }

    void OnEnable()
    {
        if (ChgEventType == BaseEnum.EventType.OnEnable)
            Process();
    }

    void OnDisable()
    {
        if (ChgEventType == BaseEnum.EventType.OnDisable || ChgEventType == BaseEnum.EventType.OnDeselect)
            Process();
    }

    #region ISelectHandler implementation

    public void OnSelect(BaseEventData eventData)
    {
        if (ChgEventType == BaseEnum.EventType.OnSelect)
            Process();
    }

    #endregion

    #region IDeselectHandler implementation

    public void OnDeselect(BaseEventData eventData)
    {
        if (ChgEventType == BaseEnum.EventType.OnDeselect)
            Process();
    }

    #endregion

    #region ICancelHandler implementation

    public void OnCancel(BaseEventData eventData)
    {
        if (ChgEventType == BaseEnum.EventType.OnCancel)
            Process();
    }

    #endregion

    public void OnSubmit(BaseEventData eventData)
    {
        if (ChgEventType == BaseEnum.EventType.OnClick)
            Process();
    }
}
