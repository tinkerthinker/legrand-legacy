﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScrollController : MonoBehaviour {
    public Scrollbar.Direction ScrollDirection;
    public Scrollbar ScrollbarTarget;
    public int SeenRow = 1;
    public int SeenCol = 1;

    [Header("Only Fill This if you want to determine some fixed scroller object")]
    public ScrollerData[] FixScrollers;

    private int _NumberOfStep;
    public int NumberOfStep
    {
        get { return _NumberOfStep; }
    }

    private int _LastIndex;
    public int LastIndex
    {
        get { return _LastIndex; }
    }

    private int _CountedChild = 0;
    public int CountedChild
    {
        get { return _CountedChild; }
    }

    private bool _IsAlreadyInit;

    public void Awake()
    {
        if (!_IsAlreadyInit)
            Init();
    }

    void Init()
    {
        _IsAlreadyInit = true;
        Reset();
    }

	public void AddObjectChild(GameObject obj)
    {
        if (!_IsAlreadyInit)
            Init();
        _CountedChild++;
        _NumberOfStep = Mathf.CeilToInt( (float)_CountedChild/(float)SeenCol) - (SeenRow - 1);

        if(obj.GetComponent<ScrollerData>() == null)
            obj.AddComponent<ScrollerData>();

        ScrollerData scroller = obj.GetComponent<ScrollerData>();
        scroller.Controller = this;
        scroller.SetIndex(_CountedChild);
    }

    public void Reset()
    {
        _NumberOfStep = 0;
        _CountedChild = 0;
        _LastIndex = 0;
        ScrollbarTarget.value = 1;

        if (FixScrollers.Length > 0)
        {
            for (int i = 0; i < FixScrollers.Length; i++)
            {
                AddObjectChild(FixScrollers[i].gameObject);
            }
        }
    }

    public void ScrollTo(int index)
    {
        float shouldBeValueScroll;
        int nextIndex = index;

        index = Mathf.CeilToInt((float)index / (float)SeenCol);
        int lastIndex = Mathf.CeilToInt( (float)_LastIndex/(float)SeenCol);
        int input = index - lastIndex;

        // <0 = push up, >0 = push down
        if (_NumberOfStep > 1)
        {
            if (input < 0)
            {
                shouldBeValueScroll = (_NumberOfStep - index) * (1f / (_NumberOfStep - 1f)); //scroll up
                if (ScrollbarTarget.value > shouldBeValueScroll && index != 1)
                    shouldBeValueScroll = ScrollbarTarget.value;
            }
            else if (input > 0)
            {
                shouldBeValueScroll = 1f - ((index - SeenRow) * (1f / (NumberOfStep - 1f))); //scroll down
                if (ScrollbarTarget.value < shouldBeValueScroll && index != CountedChild)
                    shouldBeValueScroll = ScrollbarTarget.value;
            }
            else
                shouldBeValueScroll = ScrollbarTarget.value;
        }
        else
            shouldBeValueScroll = 1;

        ScrollbarTarget.value = shouldBeValueScroll;
        _LastIndex = nextIndex;
    }
}
