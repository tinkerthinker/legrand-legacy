﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Collections;
using TeamUtility.IO;

//This Class is created Because All Button In Result Scene Battle Somehow can't send onclick event totally. even after add new listener still can't invoke onclick event
//the Button component onclick event is broken if instantiated before result scene is instantiated, all button after that can work normally
public class ForceInvokeButtonClick : MonoBehaviour, IUpdateSelectedHandler {
    public string KeyConfirm = "Confirm";
    public void OnUpdateSelected(BaseEventData eventData)
    {
        if(InputManager.GetButtonDown(KeyConfirm))
        {
            GetComponent<Button>().onClick.Invoke();
        }
    }
}
