﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class UIObjectMovePosition : MonoBehaviour {
    [Header("All Position here are anchored Position")]
    public Vector2 NormalPosition;
    public bool UseNormalPositionOnEnable = true;
    public List<Vector2> NormalPositionPresets;
    public int FirstIndexNormalPositionPreset = -1;
    public Vector2 AddPosition;

    public float Duration = 1;

    private RectTransform ThisRT;
    private Tween _ThisTween;

    private int _IndexNormalPosition = -1;
    public int CurrentIndexNormalPosition
    {
        get { return _IndexNormalPosition; }
    }

    void Awake()
    {
        ThisRT = GetComponent<RectTransform>();
    }

    void OnDisable()
    {
        if (_ThisTween != null)
            _ThisTween.Kill();
        ThisRT.anchoredPosition = NormalPosition;
    }

    void OnEnable()
    {
        if(UseNormalPositionOnEnable)
        {
            if(NormalPositionPresets.Count > 0 && FirstIndexNormalPositionPreset >=0)
            {
                NormalPosition = NormalPositionPresets[FirstIndexNormalPositionPreset];
                _IndexNormalPosition = FirstIndexNormalPositionPreset;
            }
            ThisRT.anchoredPosition = NormalPosition;
        }
    }

    public void NextIndexNormalPosition()
    {
        if(NormalPositionPresets.Count > 0)
        {
            if (++_IndexNormalPosition >= NormalPositionPresets.Count)
                _IndexNormalPosition = 0;
            NormalPosition = NormalPositionPresets[_IndexNormalPosition];
        }
    }

    public void SetNormalPositionIndex(int index)
    {
        if(NormalPositionPresets.Count > 0)
        {
            _IndexNormalPosition = Mathf.Clamp(index, 0, NormalPositionPresets.Count - 1);
            NormalPosition = NormalPositionPresets[_IndexNormalPosition];
        }
    }

    public void GoToEndPos()
    {
        if(_ThisTween != null)
            _ThisTween.Kill();
        _ThisTween = ThisRT.DOAnchorPos(NormalPosition + AddPosition , Duration).SetUpdate(true);
    }

    public void GoToNormalPos()
    {
        if (_ThisTween != null)
            _ThisTween.Kill();
        _ThisTween = ThisRT.DOAnchorPos(NormalPosition, Duration).SetUpdate(true);
    }
}
