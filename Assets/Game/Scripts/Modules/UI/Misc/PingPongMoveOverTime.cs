﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;

public class PingPongMoveOverTime : MonoBehaviour {
	private RectTransform thisRT;
	public Vector3 AddLocalPosition;
	private Vector3 NormalLocalPosition;
	public float Duration = 1f;
    private bool _FirstLaunch = true;

    Tween _ThisTween;

    void Start()
    {
        thisRT = GetComponent<RectTransform>();
        StartCoroutine(WaitUIReady());
    }

	void OnEnable () {
        if (!_FirstLaunch)
            StartCoroutine(WaitUIReady());
	}

    void OnDisable()
    {
        _ThisTween.Kill();
    }

	IEnumerator WaitUIReady()
	{
		yield return null;
        if (_FirstLaunch)
        {
            NormalLocalPosition = thisRT.localPosition;
            _FirstLaunch = false;
        }
		GoToTargetPos ();
	}

	void GoToTargetPos()
	{
		_ThisTween = thisRT.DOLocalMove (NormalLocalPosition + AddLocalPosition, Duration).OnComplete(GoToNormalPos).SetUpdate(true);
	}

	void GoToNormalPos()
	{
        _ThisTween = thisRT.DOLocalMove(NormalLocalPosition, Duration).OnComplete(GoToTargetPos).SetUpdate(true);
	}

    public void Reset()
    {
        if (_ThisTween != null)
            _ThisTween.Kill();
        //if (thisRT != null && NormalLocalPosition != null)
        //    thisRT.localPosition = NormalLocalPosition;
        StartCoroutine(WaitUIReady());
    }
}
