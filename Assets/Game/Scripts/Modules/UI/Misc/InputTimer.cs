﻿using UnityEngine;
using System.Collections;

public class InputTimer{
    private BaseEnum.Orientation _Orientation;

    private bool _Allow;
    public bool IsAllowed
    {
        get { return _Allow; }
    }
    private float _Input;
    private bool _IsInputPositive;

    private float _DefaultActionTime;
    private float _ActionTime;
    private float _ThresholdTime;
    private float _Time = 0;
    private bool _InFastInput = false;

	public InputTimer(BaseEnum.Orientation orientation, float inputActionPerSecond)
    {
        _Orientation = orientation;
        _DefaultActionTime = 1f / inputActionPerSecond;
        ResetTime();
    }

    public void ResetTime()
    {
        _ThresholdTime = _DefaultActionTime;
        _ActionTime = _ThresholdTime;
        _Time = _ActionTime;
        _InFastInput = false;
    }

    public void ScanInput()
    {
        if(_Orientation == BaseEnum.Orientation.Horizontal)
            _Input = LegrandUtility.GetAxisRawHorizontal();
        else if (_Orientation == BaseEnum.Orientation.Vertical)
            _Input = LegrandUtility.GetAxisRawVertical();

        if (_Input > 0.2f || _Input < -0.2f)
        {
            _Time += Time.unscaledDeltaTime;

            if (_Time < _ActionTime)
            {
                _Allow = false;
            }
            else
            {
                //_Time = 0;
                _Allow = true;
                _ActionTime += _ThresholdTime;
                if (_ActionTime >= _ThresholdTime * 10f && !_InFastInput)
                {
                    _ThresholdTime /= 3f;
                    _InFastInput = true;
                }
            }
        }
        else
            ResetTime();
    }

    public float GetInput()
    {
        return _Input;
    }
}
