﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;

[RequireComponent(typeof(Image))]
public class BlinkImageOverTime : MonoBehaviour {
	private Color _DefaultColor;
    public Color FadeOutColor;
    public bool FadeOutColorOnStart = false;

	private Image thisImage;
	public float Duration = 1f;
    Tween tween;
	
	void Awake () {
		thisImage = GetComponent<Image> ();
		_DefaultColor = thisImage.color;
	}

    void OnEnable()
    {
        if (FadeOutColorOnStart)
        {
            thisImage.color = FadeOutColor;
            GoNormal();
        }
        else
            GoFadeOut();
    }

    void OnDisable()
    {
        tween.Kill();
        thisImage.color = _DefaultColor;
    }

	void GoFadeOut()
	{
        tween = thisImage.DOColor(FadeOutColor, Duration).SetUpdate(true).OnComplete(GoNormal);
	}

	void GoNormal()
	{
        tween = thisImage.DOColor(_DefaultColor, Duration).SetUpdate(true).OnComplete(GoFadeOut);
	}
}
