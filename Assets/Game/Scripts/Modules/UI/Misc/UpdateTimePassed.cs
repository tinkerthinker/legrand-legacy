﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UpdateTimePassed : MonoBehaviour {
	public Text timeText;
	float initialTime;
	float timePassed;

	void OnEnable()
	{
		initialTime = LegrandBackend.Instance.InitialTime;
		StartCoroutine(DisplayTime());
	}

	IEnumerator DisplayTime()
	{
		while(true)
		{
			yield return StartCoroutine(LegrandUtility.WaitForRealTime(1f));
			timePassed = initialTime + Time.realtimeSinceStartup - LegrandBackend.Instance.WorldSceneStartTime;
			timeText.text = ((int)timePassed/3600).ToString("00") +"."+ (((int)timePassed%3600)/60).ToString("00") + "." + (((int)timePassed%3600)%60).ToString("00");
		}
	}
}
