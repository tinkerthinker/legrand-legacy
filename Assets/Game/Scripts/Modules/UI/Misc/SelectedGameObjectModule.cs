﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class SelectedGameObjectModule : MonoBehaviour {
	public static SelectedGameObjectModule current;
	private GameObject _LastSelectedGameObject;
	public GameObject LastSelectedGameObject
	{
		get{return _LastSelectedGameObject;}
	}
	[System.NonSerialized]
	public GameObject currentSelectedGameObject;

	public RectTransform MarkSelectedObject;

	private Vector3 _VerifiedObjectPos;

	private bool isMarkVisible = false;
    public bool IsMarkVisible
    {
        get { return isMarkVisible; }
    }

	void Awake()
	{
		SelectedGameObjectModule.current = this;
	}

	void Update()
	{
		if (EventSystem.current == null)
			return;
		if (EventSystem.current.currentSelectedGameObject != currentSelectedGameObject) {
			_LastSelectedGameObject = currentSelectedGameObject;
			currentSelectedGameObject = EventSystem.current.currentSelectedGameObject;
			MarkCurrentObject();
		}

		if (MarkSelectedObject && currentSelectedGameObject != null) {
			if(!VerifyMarkPosition())
				CalculatePosition();
		}
	}

	void MarkCurrentObject()
	{
		if (MarkSelectedObject) {
			CalculatePosition();
		}
	}

	void CalculatePosition()
	{
		if (!isMarkVisible || currentSelectedGameObject == null) {
			MarkSelectedObject.gameObject.SetActive (false);
			return;
		}
			
		if (currentSelectedGameObject.GetComponent<RectTransform> () == null) {
			MarkSelectedObject.gameObject.SetActive (false);
			return;
		}
		if (currentSelectedGameObject.GetComponent<Button> () == null) {
			MarkSelectedObject.gameObject.SetActive (false);
			return;
		} else {
			MarkSelectedObject.gameObject.SetActive (true);
		}
		RectTransform oriRT = (RectTransform) currentSelectedGameObject.transform;
		Vector2 desiredPosition;

		_VerifiedObjectPos = MarkSelectedObject.position = oriRT.position;

		if (MarkSelectedObject.anchorMax == MarkSelectedObject.anchorMin) {
			desiredPosition = MarkSelectedObject.anchoredPosition;

			desiredPosition.x += ((MarkSelectedObject.anchorMax.x - oriRT.pivot.x)*oriRT.rect.width);
			desiredPosition.y += ((MarkSelectedObject.anchorMax.y - oriRT.pivot.y)*oriRT.rect.height);

			MarkSelectedObject.anchoredPosition = desiredPosition;
		}
	}

	IEnumerator WaitForPlacingCursor(Vector2 desiredPosition)
	{
		yield return null;
		MarkSelectedObject.anchoredPosition = desiredPosition;
	}

	bool VerifyMarkPosition()
	{
		if (currentSelectedGameObject.GetComponent<RectTransform> () == null || currentSelectedGameObject.GetComponent<Button> () == null)
			return true; //di return true maksudnya bahwa si current object itu tidak perlu lagi dicek posisi nya, karena dia gk punya rect transform atau bukan button
		if (_VerifiedObjectPos == currentSelectedGameObject.GetComponent<RectTransform> ().position)
			return true;
		return false;
	}

	public void HideMarkObject()
	{
		isMarkVisible = false;
		if(MarkSelectedObject)
			MarkSelectedObject.gameObject.SetActive (false);
	}

	public void ShowMarkObject()
	{
		isMarkVisible = true;
		if (MarkSelectedObject) {
			MarkSelectedObject.gameObject.SetActive (true);
			CalculatePosition ();
		}
	}
}
