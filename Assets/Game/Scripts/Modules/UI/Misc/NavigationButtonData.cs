﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(Button))]
public class NavigationButtonData : MonoBehaviour {
	private Navigation _LastNavigation;
	public Navigation LastNavigation
	{
		get{return _LastNavigation;}
	}

	private Navigation _CurrNavigation;
	public Navigation CurrNavigation
	{
		get{return _CurrNavigation;}
	}

    private Button _LinkedButton;

	void Start()
	{
        _LinkedButton = GetComponent<Button>();
		_CurrNavigation = _LinkedButton.navigation;
	}

	public void ChangeToNone()
	{
        ChangeToNone(_LinkedButton);
	}

	public void ChangeToNone(Button targetButton)
	{
		_LastNavigation = _CurrNavigation;

		Navigation navTemp = targetButton.navigation;

		navTemp = targetButton.navigation;
		navTemp.mode = Navigation.Mode.None;
		targetButton.navigation = navTemp;
	}

	public void ChangeToLastNavigation()
	{
		Navigation navLastTemp = _CurrNavigation;

		_CurrNavigation = _LastNavigation;
		_LastNavigation = navLastTemp;

        _LinkedButton.navigation = _CurrNavigation;
	}
}
