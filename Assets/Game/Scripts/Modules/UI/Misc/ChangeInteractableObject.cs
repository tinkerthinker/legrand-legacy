﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ChangeInteractableObject {
	#region BUTTON
	public static void DisableAllButtonInParent(GameObject targetParent, bool isDisableComponent = false)
	{
		Button[] allButtons = targetParent.GetComponentsInChildren<Button>();
		foreach (Button button in allButtons) {
            if(isDisableComponent)
                button.enabled = false;
            else
			    button.interactable = false;
		}
	}

	public static void DisableAllButtonInParentExcept(GameObject targetParent, GameObject exception)
	{
		Button[] allButtons = targetParent.GetComponentsInChildren<Button>();
		foreach (Button button in allButtons) {
			if(button.gameObject == exception)
				//button.enabled = false;
				button.interactable = true;
			else
				button.interactable = false;
		}
	}

    public static void EnableAllButtonInParent(GameObject targetParent, bool isEnableComponent = false)
	{
		Button[] allButtons = targetParent.GetComponentsInChildren<Button>();
		foreach (Button button in allButtons) {
            if(isEnableComponent)
                button.enabled = true;
            else
			    button.interactable = true;
		}
	}

	public static void EnableAllButtonInParentExcept(GameObject targetParent, GameObject exception)
	{
		Button[] allButtons = targetParent.GetComponentsInChildren<Button>();
		foreach (Button button in allButtons) {
			if(button.gameObject == exception)
				//button.enabled = true;
				button.interactable = false;
			else
				button.interactable = true;
		}
	}

    public static void DisableAllButtonInList(List<Button> targetList, bool isDisableComponent = false)
    {
        foreach (Button button in targetList)
        {
            if (isDisableComponent)
                button.enabled = false;
            else
                button.interactable = false;
        }
    }

    public static void DisableAllButtonInListExcept(List<Button> targetList, Button exception, bool isDisableComponent = false)
    {
        foreach (Button button in targetList)
        {
            if (button == exception)
                continue;
            else
            {
                if (isDisableComponent)
                    button.enabled = false;
                else
                    button.interactable = false;
            }
        }
    }

    public static void EnableAllButtonInList(List<Button> targetList, bool isEnableComponent = false)
    {
        foreach (Button button in targetList)
        {
            if (isEnableComponent)
                button.enabled = true;
            else
                button.interactable = true;
        }
    }

    public static void EnableAllButtonInListExcept(List<Button> targetList, GameObject exception, bool isEnableComponent = false)
    {
        foreach (Button button in targetList)
        {
            if (button.gameObject == exception)
                continue;
            else
            {
                if (isEnableComponent)
                    button.enabled = true;
                else
                    button.interactable = true;
            }
        }
    }

	#endregion

	#region SLIDER
	public static void DisableAllSliderInParent(GameObject targetParent)
	{
		Slider[] allObjects = targetParent.GetComponentsInChildren<Slider>();
		foreach (Slider obj in allObjects) {
			obj.enabled = false;
		}
	}
	
	public static void DisableAllSliderInParentExcept(GameObject targetParent, GameObject exception)
	{
		Slider[] allObjects = targetParent.GetComponentsInChildren<Slider>();
		foreach (Slider obj in allObjects) {
			if(obj.gameObject != exception)
				obj.enabled = false;
		}
	}
	
	public static void EnableAllSliderInParent(GameObject targetParent)
	{
		Slider[] allObjects = targetParent.GetComponentsInChildren<Slider>();
		foreach (Slider obj in allObjects) {
			obj.enabled = true;
		}
	}
	
	public static void EnableAllSliderInParentExcept(GameObject targetParent, GameObject exception)
	{
		Slider[] allObjects = targetParent.GetComponentsInChildren<Slider>();
		foreach (Slider obj in allObjects) {
			if(obj.gameObject != exception)
				obj.enabled = true;
		}
	}
	#endregion


	#region ALL SELECTABLE
	public static void DisableAllInteractableObjectInParent(GameObject targetParent)
	{
		Selectable[] allObjects = targetParent.GetComponentsInChildren<Selectable>();
		foreach (Selectable obj in allObjects) {
			obj.interactable = false;
		}
	}

	public static void DisableAllInteractableObjectInParentExcept(GameObject targetParent, GameObject exception)
	{
		Selectable[] allObjects = targetParent.GetComponentsInChildren<Selectable>();
		foreach (Selectable obj in allObjects) {
			if(obj.gameObject != exception)
				obj.enabled = false;
		}
	}
	
	public static void EnableAllInteractableObjectInParent(GameObject targetParent)
	{
		Selectable[] allObjects = targetParent.GetComponentsInChildren<Selectable>();
		foreach (Selectable obj in allObjects) {
			obj.interactable = true;
		}
	}
	
	public static void EnableAllInteractableObjectInParentExcept(GameObject targetParent, GameObject exception)
	{
		Selectable[] allObjects = targetParent.GetComponentsInChildren<Selectable>();
		foreach (Selectable obj in allObjects) {
			if(obj.gameObject != exception)
				obj.interactable = true;
		}
	}
	#endregion
}
