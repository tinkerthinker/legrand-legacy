﻿using UnityEngine;
using System.Collections;
using Legrand.core;

public class MagicPresetRenameData : MonoBehaviour {
	[System.NonSerialized]
	public CharacterMagicPreset selectedMagicPreset;
	[System.NonSerialized]
	public bool isUseDefaultName;

	void OnEnable()
	{
		if (GetComponent<CancelInputKeyboard> ()._lastButton == null)
			return;
		selectedMagicPreset = GetComponent<CancelInputKeyboard> ()._lastButton.GetComponent<RenameOptionMagicPreset> ().selectedCharMagicPreset;
		isUseDefaultName = selectedMagicPreset.IsNameDefault;

		GetComponent<InputFieldCaret> ().targetTextField.text = selectedMagicPreset.MagicPreset.Name;
		GetComponent<InputFieldCaret> ().MoveToLast ();
	}

	public void UseDefaultName()
	{
		GetComponent<InputFieldCaret> ().targetTextField.text = GenerateMagicName.GenerateNameAttackMagic(selectedMagicPreset.MagicPreset);
		GetComponent<InputFieldCaret> ().MoveToLast ();
		isUseDefaultName = true;
	}
}
