using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;

public class CancelDataTweak : MonoBehaviour, ICancelHandler {

	public void OnCancel(BaseEventData eventData)
	{/*
		DataTweakMagic dataTweakMagic = PauseUIController.instance.panelTweakMagic.GetComponent<DataTweakMagic> ();

		//change state first to make sure that panel magic is ready to process
		PauseUIController.instance.ChangeState(PauseUIController.PauseUIControllerState.MagicPreset);

		ChangeInteractableObject.EnableAllButtonInParent(ChangeTabPanel.instance.panelMagic);
		if (dataTweakMagic.isCreateNew)
			ChangeTabPanel.instance.panelMagic.GetComponent<PanelMagicChildren> ().panelMagicList.GetComponentsInChildren<Button> () [0].Select ();
		else {
			//ChangeTabPanel.instance.panelMagic.GetComponent<PanelMagicChildren> ().panelMagicList.GetComponentsInChildren<Button> () [dataTweakMagic.indexMagicPreset + 1].Select ();
			ChangeTabPanel.instance.panelMagic.GetComponent<PanelMagicChildren> ().panelMagicList.GetComponent<GenerateMagicList>().FocusToIndexButton(dataTweakMagic.idMagicPreset);
		}
		dataTweakMagic.isCreateNew = false;
		PauseUIController.instance.panelTweakMagic.SetActive (false);
*/

	}

	public void OnCancel()
	{
		DataTweakMagic dataTweakMagic = PauseUIController.Instance.PanelAssignMgcSkill.GetComponent<DataTweakMagic> ();
		bool isCreateNew = dataTweakMagic.isCreateNew;

		//to make sure the status for data tweak is already changed before change state
		dataTweakMagic.isCreateNew = false;

		PauseUIController.Instance.ChangeState (PauseUIController.PauseUIControllerState.MagicPreset);

		if (isCreateNew)
			TabPanelController.instance.panelMagicPreset.GetComponent<PanelMagicChildren> ().panelMagicList.GetComponentInChildren<Button> ().Select ();
		else
		{
			dataTweakMagic.CancelTweakMagicName();
			TabPanelController.instance.panelMagicPreset.GetComponent<PanelMagicChildren> ().panelMagicList.GetComponentInChildren<GenerateMagicList> ().FocusToIndexButton (dataTweakMagic.idMagicPreset);
		}
	}
}
