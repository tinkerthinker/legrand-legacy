﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class PanelMagicChildren : MonoBehaviour {
	public GameObject panelMagicList;

	private int currSelectedCharId;

//	void Awake()
//	{
//		StartCoroutine (WaitGameReady());
//	}
//	
//	IEnumerator WaitGameReady()
//	{
//		while (!PartyManager.Instance._ReadyToPlay)
//			yield return null;
//		Init ();
//	}

	void Start()
	{
		Init ();
	}

	void Init()
	{
		EventManager.Instance.AddListener<ChangeUICharSelectedID>(ChangeCharSelectedIdHandler);
	}

	void OnDestroy()
	{
		if (EventManager.Instance) {
			EventManager.Instance.RemoveListener<ChangeUICharSelectedID> (ChangeCharSelectedIdHandler);
		}
	}

//	void Update()
//	{
//		if (currSelectedCharId != PauseUIController.instance.selectedCharId)
//		{
//			currSelectedCharId = PauseUIController.instance.selectedCharId;
//
//
//		}
//	}

	void ChangeCharSelectedIdHandler(ChangeUICharSelectedID c)
	{
		UpdateCurrCharMagicPreset();
	}

	void UpdateCurrCharMagicPreset()
	{
		if(EventSystem.current.currentSelectedGameObject.GetComponent<MagicSkillData>() == null)
//			panelMagicList.GetComponent<GenerateMagicList>().StartListMagic();
			panelMagicList.GetComponent<GenerateMagicList>().UpdateListMagic();
		else
		{
//			panelMagicList.GetComponent<GenerateMagicList>().StartListMagic();
			panelMagicList.GetComponent<GenerateMagicList>().UpdateListMagic();
			if( panelMagicList.GetComponent<GenerateMagicList>().FocusToFirstButton())
			{
				panelMagicList.GetComponentInChildren<UnityEngine.UI.Button>().Select();
			}
		}
	}
}
