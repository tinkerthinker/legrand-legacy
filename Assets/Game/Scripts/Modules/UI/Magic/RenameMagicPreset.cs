using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using TeamUtility.IO;

public class RenameMagicPreset : MonoBehaviour, ISubmitHandler, ICancelHandler, IUpdateSelectedHandler {
	public InputField textField;
	bool isNameDefault;
	CharacterMagicPreset selectedMagicPreset;

	bool wasCancelClicked;

	// Use this for initialization
	void Start () {
		//GetComponent<InputField> ().onValueChange.AddListener ((string arg0) => OnValueChangeHandler());
	}

	void OnEnable () {
		textField.text = "";
	}

	#region IUpdateSelectedHandler implementation

	public void OnUpdateSelected (BaseEventData eventData)
	{
		if (InputManager.GetButtonDown("Confirm")) {
			textField.DeactivateInputField ();
		}
		else if (InputManager.GetButtonDown("Cancel")) {
			OnCancel();
		}
		else if (InputManager.GetButtonDown("Delete")) {
			UseDefaultName();
		}
	}

	#endregion

	public void SetMagicPreset(CharacterMagicPreset magic)
	{
		selectedMagicPreset = magic;
		textField.text = magic.MagicPreset.Name;
		isNameDefault = magic.IsNameDefault;
	}

	public void SaveNewName()
	{
		//isNameDefault = isNameDefault;
		if (!wasCancelClicked)
		{
			selectedMagicPreset.MagicPreset.Name = textField.text;
			selectedMagicPreset.IsNameDefault = isNameDefault;

			TabPanelController.instance.panelMagicPreset.GetComponent<PanelMagicChildren>().panelMagicList.GetComponent<GenerateMagicList> ().UpdateListMagic ();
		}

		PauseUIController.Instance.ChangeState(PauseUIController.PauseUIControllerState.MagicPreset);
		TabPanelController.instance.panelMagicPreset.GetComponent<PanelMagicChildren>().panelMagicList.GetComponent<GenerateMagicList> ().FocusToIndexButton(selectedMagicPreset._ID);

		wasCancelClicked = false;
	}

	void UseDefaultName()
	{
		textField.text = GenerateMagicName.GenerateNameAttackMagic (selectedMagicPreset.MagicPreset);
		//textField.caretPosition = textField.text.Length-1;
		textField.MoveTextEnd (false);

		isNameDefault = true;
	}

	public void OnValueChangeHandler()
	{
		isNameDefault = false;
	}

	#region ISubmitHandler implementation

	public void OnSubmit (BaseEventData eventData)
	{
		SaveNewName ();
	}

	#endregion

	#region ICancelHandler implementation

	public void OnCancel (BaseEventData eventData)
	{
		OnCancel ();
	}

	#endregion

	public void OnCancel()
	{
		PauseUIController.Instance.ChangeState(PauseUIController.PauseUIControllerState.MagicPreset);
		wasCancelClicked = true;

		textField.DeactivateInputField ();
	}
}
