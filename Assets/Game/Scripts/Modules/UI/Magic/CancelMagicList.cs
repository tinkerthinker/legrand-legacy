using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class CancelMagicList : MonoBehaviour,  ICancelHandler{

	// Use this for initialization
	public void OnCancel(BaseEventData eventData)
	{
		/*
		//ChangeInteractableButtonChildren.EnableAllButtonInParent(GameObject.Find("PanelCharacter"));
		ChangeInteractableObject.EnableAllButtonInParent(PauseUIController.instance.panelCharacter);
		//ChangeInteractableButtonChildren.EnableAllButtonInParent(GameObject.Find("PanelTab"));
		//ChangeInteractableButtonChildren.DisableAllButtonInParent(GameObject.Find("PanelMagic"));
		ChangeInteractableObject.DisableAllButtonInParent(ChangeTabPanel.instance.panelMagic);
		//GameObject.Find ("Character"+GUIController.instance.selectedCharId).GetComponent<Button> ().Select ();
		PauseUIController.instance.panelCharacter.GetComponentsInChildren<Button> ()[PauseUIController.instance.selectedCharId].Select ();
		*/

		PauseUIController.Instance.ChangeState (PauseUIController.PauseUIControllerState.MainMenu);
	}
}
