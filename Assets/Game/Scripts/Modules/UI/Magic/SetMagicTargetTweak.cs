using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class SetMagicTargetTweak : MonoBehaviour, ISelectHandler, IDeselectHandler {
	public DataTweakMagic dataTweak;
	public TargetType magicTarget;

	public Image ImageTarget;
	public Sprite selectSprite;
	public Sprite deselectSprite;

	public Text textTarget;
	public Color selectTextCol;
	public Color deselectTextCol;

	void Start()
	{
		GetComponent<Button> ().onClick.AddListener (() => OnClickHandler());
	}

	void OnClickHandler()
	{
		dataTweak.SaveData();
	}

	public void SelectTarget()
	{
		//maskedImageTarget.gameObject.SetActive (true);
		ImageTarget.sprite = selectSprite;
		textTarget.color = selectTextCol;
	}

	public void DeselectTarget()
	{
		//maskedImageTarget.gameObject.SetActive (false);
		ImageTarget.sprite = deselectSprite;
		textTarget.color = deselectTextCol;
	}

	#region ISelectHandler implementation

	public void OnSelect (BaseEventData eventData)
	{
		dataTweak.ChangeMagicTarget (magicTarget);
		dataTweak.UpdateMagicTweaker ();
		
		ChangeInteractableObject.EnableAllButtonInParent(transform.parent.gameObject);
	}

	#endregion

	#region IDeselectHandler implementation

	public void OnDeselect (BaseEventData eventData)
	{
		if(PauseUIController.Instance.PauseUIState == PauseUIController.PauseUIControllerState.MagicTweaker)
			dataTweak.SetFocusMagicTarget();
	}

	#endregion
}
