﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;

public class MagicSkillData : MonoBehaviour, ISelectHandler, IObjectData {
    [System.Serializable]
    public class AttributeText
    {
        public Attribute AttribType;
        public bool IsFixedAttribute = true;
        public Text AttributeLabel;
        public Text AttributeValue;

        public void SetText(Attribute attrib, int value)
        {
            if (AttributeValue)
                AttributeValue.text = value.ToString();

            if (IsFixedAttribute && attrib != AttribType)
                return;
            AttribType = attrib;
            if(AttributeLabel)
                AttributeLabel.text = attrib == Attribute.LUCK? "Luk": attrib.ToString();
        }
    }

	public string characterId;

	public Text NameText;
    public Text TargetText;
    public Text HitText;
    public Text CritText;
    public Text PowerText;
    public Text AOEText;
    public Image MagicSkillImage;
    public GameObject EquippedSymbol;
    public GameObject WillLearnSymbol;

    public List<AttributeText> ReqAtrributeTexts;
    //public bool Handle

	private Magic _MagicPreset;
    public Magic MagicPreset
    {
		get{return _MagicPreset;}
		set{
            _NormalSkillData = null;
			_MagicPreset = value;
			UpdateData ();
		}
	}

    private NormalSkill _NormalSkillData;
    public NormalSkill NormalSkillData
    {
        get { return _NormalSkillData; }
        set
        {
            _MagicPreset = null;
            _NormalSkillData = value;
            UpdateData();
        }
    }

    private bool _SimulateAttribReceived = true;
    private LockedMagic _LockedMagic;
    private LockedSkill _LockedSkill;

    private string _IdChar;

    void OnEnable()
    {
        EventManager.Instance.AddListener<SimulateExpEvent>(SimulateExpEventHandler);
    }

    void OnDisable()
    {
        if(EventManager.Instance)
            EventManager.Instance.RemoveListener<SimulateExpEvent>(SimulateExpEventHandler);
    }

	void SetData (){
        

        if (_MagicPreset != null)
        {
            if (NameText != null)
                NameText.text = _MagicPreset.Name;
            if (TargetText != null)
                TargetText.text = _MagicPreset.MagicTargetType.ToString();
            if (HitText != null)
                HitText.text = _MagicPreset.HitPercentage.ToString();
            if (CritText != null)
                CritText.text = _MagicPreset.CritPercentage.ToString();
            if (PowerText != null)
                PowerText.text = "x" + _MagicPreset.MagicDamageModifier;
            if (MagicSkillImage)
            {
                MagicSkillImage.sprite = LegrandBackend.Instance.GetSpriteData("Element" + _MagicPreset.Element.ToString());
            }
            if(AOEText)
            {
                AOEText.text = _MagicPreset.MagicTargetType.ToString().Substring(0, 1);
            }
        }
        else if(_NormalSkillData != null)
        {
            if (NameText != null)
                NameText.text = _NormalSkillData.Name;
            if (TargetText != null)
                TargetText.text = _NormalSkillData.Target.ToString();
            if (HitText != null)
            {
                if(_NormalSkillData.BuffsChance.Count > 0)
                    HitText.text = _NormalSkillData.BuffsChance[0].Chance.ToString();
                else
                    HitText.text = "100";
            }
            if (CritText != null)
                CritText.text = "-";
            if (PowerText != null)
            {
                PowerText.text = _NormalSkillData.DamagePercentage > 0 ? "x"+_NormalSkillData.DamagePercentage.ToString() : "-";
            }
            if (MagicSkillImage)
            {
                string skillType = "";
                if (_NormalSkillData.TargetFormationType == TargetFormation.Enemy)
                    skillType = "Debuff";
                else// if (_NormalSkillData.SkillType == SkillTypes.NormalDefense)
                {
                    if (_NormalSkillData.BuffsChance.Count > 0 && _NormalSkillData.BuffsChance[0].BuffEffect.Type == BuffType.HEAL)
                    {
                        skillType = "Heal";
                    }
                    else
                        skillType = "Buff";
                }
                MagicSkillImage.sprite = LegrandBackend.Instance.GetSpriteData("Skill" + skillType);
            }
            if (AOEText)
            {
                AOEText.text = _NormalSkillData.Target.ToString().Substring(0, 1);
            }
        }
	}

    public void UpdateData()
    {
        SetData();
    }

    void SimulateExpEventHandler(SimulateExpEvent e)
    {
        SendRequestSimulatAttribute();
    }

    void SendSimulateAttributeEventHandler(SendSimulateAttributeEvent e)
    {
        _SimulateAttribReceived = true;
        Dictionary<Attribute, object> totalAttrib = new Dictionary<Attribute,object>();

        foreach(KeyValuePair<Attribute, object> att in e.Chara.BaseBattleAttributes)
        {
            totalAttrib.Add(att.Key, (int)att.Value + e.SimulateAttrib[att.Key]);
        }

        if(_LockedMagic != null)
        {
            if( _LockedMagic.IsRequirementFulfilled(e.Chara.ClassLevel, totalAttrib))
            {
                SetAsWillLearnGrimoire(true);
            }
            else
                SetAsWillLearnGrimoire(false);
        }
        else if (_LockedSkill != null)
        {
            if (_LockedSkill.IsRequirementFulfilled(e.Chara.ClassLevel, totalAttrib))
            {
                SetAsWillLearnGrimoire(true);
            }
            else
                SetAsWillLearnGrimoire(false);
        }
    }

    void SendRequestSimulatAttribute()
    {
        if (_SimulateAttribReceived)
        {
            _SimulateAttribReceived = false;
            EventManager.Instance.AddListenerOnce<SendSimulateAttributeEvent>(SendSimulateAttributeEventHandler);
        }
        EventManager.Instance.TriggerEvent(new RequestSimulateAttributeEvent((PauseUIController.Instance && PauseUIController.Instance.isActiveAndEnabled)? PauseUIController.Instance.selectedCharId:""));
    }

    public void SetRequirementLockedMagic(LockedMagic lockedMagic)
    {
        _LockedMagic = lockedMagic;
        _LockedSkill = null;

        SetRequirement(lockedMagic.AttributeRequirements);
        SendRequestSimulatAttribute();
    }

    public void SetRequirementLockedSkill(LockedSkill lockedSkill)
    {
        _LockedMagic = null;
        _LockedSkill = lockedSkill;

        SetRequirement(lockedSkill.AttributeRequirements);
        SendRequestSimulatAttribute();
    }

    void SetRequirement(List<AttributeValue> reqs)
    {
        for (int i = 0; i < reqs.Count; i++ )
        {
            ReqAtrributeTexts[i].SetText(reqs[i].AttributeType, (int)reqs[i].value);
            ReqAtrributeTexts[i].AttributeLabel.gameObject.SetActive(true);
        }
        
        if(reqs.Count < ReqAtrributeTexts.Count)
        {
            for (int i = reqs.Count; i < ReqAtrributeTexts.Count; i++)
            {
                ReqAtrributeTexts[i].AttributeLabel.gameObject.SetActive(false);
            }
        }
    }

    void DetermineWillLearnGrimoire()
    {

    }

    public void SetAsEquippedCommand(bool isEquipped)
    {
        if (EquippedSymbol)
        {
            if (isEquipped)
                EquippedSymbol.SetActive(true);
            else
                EquippedSymbol.SetActive(false);
        }
    }

    public void SetAsWillLearnGrimoire(bool willLearn)
    {
        if (WillLearnSymbol)
        {
            if (willLearn)
                WillLearnSymbol.SetActive(true);
            else
                WillLearnSymbol.SetActive(false);
        }
    }

    public void SetCharId(string id)
    {
        _IdChar = id;
    }

    public void OnSelect(BaseEventData eventData)
    {
        EventManager.Instance.TriggerEvent(new InspectMagicSkillEvent(_MagicPreset !=null?_MagicPreset: null, _NormalSkillData, true));
    }

    public void SendCommandDataEvent()
    {
        if(_MagicPreset != null)
            EventManager.Instance.TriggerEvent(new ChoosenCommandDataEvent(CommandTypes.Magic, _MagicPreset, gameObject));
        else if(_NormalSkillData != null)
            EventManager.Instance.TriggerEvent(new ChoosenCommandDataEvent(CommandTypes.Skill, _NormalSkillData, gameObject));
        else
            EventManager.Instance.TriggerEvent(new ChoosenCommandDataEvent(CommandTypes.None, null, gameObject));
    }

    public void SetData(object obj)
    {
        _MagicPreset = obj as Magic;
        _NormalSkillData = obj as NormalSkill;
        SetData();
    }

    public object GetData()
    {
        if (_MagicPreset != null)
            return _MagicPreset;
        else if (_NormalSkillData != null)
            return _NormalSkillData;
        return null;
    }

    public void RefreshData()
    {
        UpdateData();
    }
}
