using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using Legrand.core;
using TeamUtility.IO;

public class DeleteMagicPreset : MonoBehaviour, IUpdateSelectedHandler {
	DataTweakMagic dataTweakMagic;
	int previousIdDeletedMP=-1;

	// Use this for initialization
	void Start () {
		dataTweakMagic = PauseUIController.Instance.PanelAssignMgcSkill.GetComponent<DataTweakMagic> ();
		//GetComponent<Button> ().onClick.AddListener (() => OnClickHandler ());
	}

	#region IUpdateSelectedHandler implementation

	public void OnUpdateSelected (BaseEventData eventData)
	{
		if (InputManager.GetButtonDown("Delete"))
		{
			DeleteMagicFromPreset();
			
			TabPanelController.instance.panelMagicPreset.GetComponent<PanelMagicChildren>().panelMagicList.GetComponent<GenerateMagicList> ().UpdateListMagic ();
			
			if(previousIdDeletedMP >= 0)
			{
				TabPanelController.instance.panelMagicPreset.GetComponent<PanelMagicChildren>().panelMagicList.GetComponent<GenerateMagicList> ().FocusToIndexButton(previousIdDeletedMP);
			}
			else
				TabPanelController.instance.panelMagicPreset.GetComponent<PanelMagicChildren>().panelMagicList.GetComponentInChildren<Button>().Select();
			previousIdDeletedMP = -1;
		}
	}

	#endregion

	void DeleteMagicFromPreset()
	{
		MainCharacter selectedChar = PartyManager.Instance.GetCharacter (PauseUIController.Instance.selectedCharId);
		int deleteId = dataTweakMagic.idMagicPreset;

		foreach (CharacterMagicPreset magic in selectedChar.Magics.MagicPresets) {
			if (magic._ID == deleteId)
			{
				//selectedChar.Magics.MagicPresets.Remove(magic);
				selectedChar.Magics.UnlearnMagicPreset(magic._ID);
				break;
			}
			else
				previousIdDeletedMP = magic._ID;
		}
	}
}
