using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;
using TeamUtility.IO;

public class DataTweakMagic : MonoBehaviour {

	#region data & visual Tweak
	private float _DamageMagicBar;
	public float DamageMagicBar{get{return _DamageMagicBar;}}
	private int _InterruptMagicBar;
	public int InterruptMagicBar{get{return _InterruptMagicBar;}}
	private int _CostMagicBar;
	public int CostMagicBar{get{return _CostMagicBar;}}

	private int _idMagicPreset;
	public int idMagicPreset{
		get{return _idMagicPreset;}
	}

	private string _previousName;
	public string previousName{
		get{return _previousName;}
	}

	private bool _isNameDefault;
	public bool IsNameDefault{
		get{return _isNameDefault;}
	}
	private Magic _tweakedSelectedMagic;
	public Magic tweakedSelectedMagic{
		get{return _tweakedSelectedMagic;}
	}

	//private Image[] listPointTweak;
	[SerializeField]
	private Slider[] listTweakSlider;
	[SerializeField]
	private Text[] listValueText;

	public GameObject magicElementIconList; //object that accomodate the magicElementIcon, visually as a list
	private MagicElementData[] magicElementList;
	private int magicElementIndex=0;
	private bool updateElementIcon = false;
	#endregion

	#region Magic Target Panel
//	public Transform singleTarget;
//	public Transform columnTarget;
//	public Transform rowTarget;
//	public Transform squareTarget;
//	public Transform allTarget;
	public List<GameObject> listTargetObject;
	#endregion
	
	#region data for create new
	public bool isCreateNew=false;
	#endregion

	public GameObject boxSelect;			// for magic target
	public GameObject magicIconBoxSelect;	// for magic icon
	public Text nameMagicText;

	void Start()
	{
		//RefreshMagicIconList (true);
	}


	void OnEnable () {


		if (magicElementList == null)
			Start ();

		//sementara awalnya dibuat 1 semua nilainya
		/*
		_damageMagicBar = 4;
		_delayMagicBar = 6;
		_costMagicBar = 2;
		*/

		_DamageMagicBar = Mathf.Clamp (_DamageMagicBar, 1, 4);
		_InterruptMagicBar = Mathf.Clamp (_InterruptMagicBar, 0, 8);
		_CostMagicBar = Mathf.Clamp (_CostMagicBar, 0, 8);

		listTweakSlider[0].value = _DamageMagicBar;
		listTweakSlider[1].value = _InterruptMagicBar;
		listTweakSlider[2].value = _CostMagicBar;

		if (PauseUIController.Instance == null)
			return;

		GameObject pointTweakImageHolder = PauseUIController.Instance.PanelAssignMgcSkill;//.GetComponentInParent<Transform>().gameObject;

		//listPointTweak = pointTweakImageHolder.GetComponentsInChildren<Image> ();
	}

	void OnDisable()
	{
		/*
		for (int i=0; i<magicElementList.Length; i++)
		{
			magicElementList [i].gameObject.SetActive (false);
		}
		*/
	}

	void Update()
	{
		// ada 2 cara untuk tahu, dengan state, dan dengan membaca hubungan parent-child. sekarang pakai baca state dulu
		//if (EventSystem.current.currentSelectedGameObject.transform.IsChildOf (gameObject.transform))
		if (PauseUIController.Instance.PauseUIState == PauseUIController.PauseUIControllerState.MagicTweaker)
		{
			//if (Input.GetKeyDown (KeyCode.E))
			if (InputManager.GetButtonDown("ChangeTabR"))
			{
				ShiftMagicElementIcon (magicElementIndex + 1);
			} else if (InputManager.GetButtonDown("ChangeTabL"))
			{
				ShiftMagicElementIcon (magicElementIndex - 1);
			}
		} else
		{
			if(EventSystem.current)
				if(EventSystem.current.currentSelectedGameObject.GetComponent<MagicSetButton>() == null)
				{
					gameObject.SetActive(false);
				}
		}

		if(updateElementIcon)
			UpdateMagicIconBoxPosition ();
	}

	void RefreshMagicIconList(bool hideAll)
	{
		magicElementList = magicElementIconList.GetComponentsInChildren<MagicElementData> (true);
		if(hideAll)
		{
			for (int i=0; i<magicElementList.Length; i++)
			{
				magicElementList [i].gameObject.SetActive (false);
			}
		}
	}

	void UpdateTweakMagicName()
	{
		if (_isNameDefault) {
			//nameMagicText.text = _tweakedSelectedMagic.Name = GenerateMagicName.GenerateNameAttackMagic (_tweakedSelectedMagic);
			Button magicButton = TabPanelController.instance.panelMagicPreset.GetComponent<PanelMagicChildren> ().panelMagicList.GetComponentInChildren<GenerateMagicList> ().GetMagicPresetButton(_idMagicPreset);
			magicButton.GetComponentInChildren<Text>().text = _tweakedSelectedMagic.Name = GenerateMagicName.GenerateNameAttackMagic (_tweakedSelectedMagic);
		}
	}

	public void CancelTweakMagicName()
	{
		Button magicButton = TabPanelController.instance.panelMagicPreset.GetComponent<PanelMagicChildren> ().panelMagicList.GetComponentInChildren<GenerateMagicList> ().GetMagicPresetButton(_idMagicPreset);
		magicButton.GetComponentInChildren<Text>().text = _previousName;
	}

	public void UpdateMagicTweaker () {
		//update magic temp that represent magic tweaker
		if (_tweakedSelectedMagic == null)
			return;
		_tweakedSelectedMagic.MagicDamageModifier = _DamageMagicBar;

		UpdateTweakMagicName ();


		listTweakSlider[0].value = _DamageMagicBar;
		listTweakSlider[1].value = _InterruptMagicBar;
		listTweakSlider[2].value = _CostMagicBar;

		//fill in text value
		listValueText [0].text = (_DamageMagicBar * 100).ToString();

//		float modifier = 0.25f + _costMagicBar * 0.25f;
		MainCharacter selectedChar = PartyManager.Instance.GetCharacter(PauseUIController.Instance.selectedCharId);
//		int INT, WIS;
//		INT = (int) selectedChar.BattleAttributes[Attribute.INT];
//		WIS = (int) selectedChar.BattleAttributes[Attribute.WIS];
//		listValueText [2].text = (modifier *((INT + WIS)/2)*10).ToString();
		//float costDmg = BattleFormula.GetCostDmg(selectedChar, _costMagicBar, _tweakedSelectedMagic.MagicTargetType);
		//float costDmg = BattleFormula.GetCostDmg ((Character) selectedChar, _tweakedSelectedMagic);
		float costDmg = Formula.GetCostDmg(selectedChar, CostMagicBar, _tweakedSelectedMagic.MagicTargetType, null, null);
		listValueText [2].text = Mathf.RoundToInt(costDmg).ToString();
	}

	public void ChangeDataMagicTweaker()
	{
		if (EventSystem.current == null)
			return;
		if (EventSystem.current.currentSelectedGameObject == listTweakSlider [0].gameObject) //it means player is selecting damage bar and want to change its value
		{
			float currValue = Mathf.Clamp( listTweakSlider[0].value, 1, 4);
			int dValue = (int)(currValue - _DamageMagicBar);

			if( dValue > 0 )
			{
				if((int)listTweakSlider[1].value < (int)listTweakSlider[1].maxValue)
					_InterruptMagicBar += dValue;
				else
					_CostMagicBar += dValue;

				if((int)listTweakSlider[2].value < (int)listTweakSlider[2].maxValue)
					_CostMagicBar += dValue;
				else
					_InterruptMagicBar += dValue;
			}
			else if( dValue < 0 )
			{
				if((int)listTweakSlider[1].value > (int)listTweakSlider[1].minValue)
					_InterruptMagicBar += dValue;
				else
					_CostMagicBar += dValue;
				
				if((int)listTweakSlider[2].value > (int)listTweakSlider[2].minValue)
					_CostMagicBar += dValue;
				else
					_InterruptMagicBar += dValue;
			}
			_DamageMagicBar = currValue;
		}
		else if (EventSystem.current.currentSelectedGameObject == listTweakSlider [1].gameObject) //it means player is selecting interrupt bar and want to change its value
		{

			int currValue = (int)listTweakSlider[1].value;
			int dValue = currValue - _InterruptMagicBar;

			if( (dValue < 0 && ((int)listTweakSlider[2].value < (int)listTweakSlider[2].maxValue)) || (dValue > 0 && ((int)listTweakSlider[2].value > (int)listTweakSlider[2].minValue)) )
			{
				_InterruptMagicBar = currValue;
				_CostMagicBar -= dValue;
			}



		}
		else if (EventSystem.current.currentSelectedGameObject == listTweakSlider [2].gameObject) //it means player is selecting cost bar and want to change its value
		{
			int currValue = (int)listTweakSlider[2].value;
			int dValue = currValue - _CostMagicBar;

			if( (dValue < 0 && ((int)listTweakSlider[1].value < (int)listTweakSlider[1].maxValue)) || (dValue > 0 && ((int)listTweakSlider[1].value > (int)listTweakSlider[1].minValue)) )
			{
				_CostMagicBar = currValue;
				_InterruptMagicBar -= dValue;
			}

		}

		UpdateMagicTweaker ();
	}

	void ShiftMagicElementIcon(int magicIconIndex)
	{
		int amountElement = PartyManager.Instance.GetCharacter (PauseUIController.Instance.selectedCharId).CharacterElements.Elements.Count;
		if (magicIconIndex >= amountElement) //mean magicIconIndex over the character max element index
		{
			magicIconIndex = 0;
		}
		else if(magicIconIndex <0)
		{
			magicIconIndex = amountElement-1;
		}

		magicElementIndex = magicIconIndex;
		tweakedSelectedMagic.Element = PartyManager.Instance.GetCharacter (PauseUIController.Instance.selectedCharId).CharacterElements.Elements[magicElementIndex];

		UpdateTweakMagicName ();
		//UpdateMagicIconBoxPosition ();
		updateElementIcon = true;
	}

	void SetCharacterMagicElementIcon()
	{
		/*
		foreach (var element in PartyManager.Instance.GetCharacter (GUIController.instance.selectedCharId).Elements)
		{
			GameObject newElementIcon = Instantiate(magicElementIcon)as GameObject;
			newElementIcon.name = "MagicElementIcon-"+element.ToString();
			newElementIcon.transform.SetParent(magicElementIconList.transform);
			newElementIcon.GetComponent<RectTransform> ().localScale = new Vector3(1,1,1);

			newElementIcon.GetComponent<Image>().sprite = DatabaseImageSprite.instance.GetSpriteMagicIconElement(element);
		}*/
		RefreshMagicIconList (true);
		int indexElement = 0;
		foreach (var element in PartyManager.Instance.GetCharacter (PauseUIController.Instance.selectedCharId).CharacterElements.Elements)
		{
			for (int i=0; i<magicElementList.Length; i++)
			{
				if(element == magicElementList[i].magicElement){
					magicElementList[i].gameObject.SetActive(true);
					magicElementList[i].transform.SetSiblingIndex(indexElement);
//					if(indexElement != i) //swap position with other element icon
//					{
//						MagicElementData temp = magicElementList[i];
//						magicElementList[i] = magicElementList[indexElement];
//						magicElementList[indexElement] = temp;
//					}
				}
			}
			indexElement++;
		}

		for (int i=indexElement; i<magicElementList.Length; i++)
		{
			//magicElementList[i].gameObject.SetActive(false);
		}


		magicElementIndex = (int)tweakedSelectedMagic.Element;
		//UpdateMagicIconBoxPosition ();
		updateElementIcon = true;
	}

	void UpdateMagicIconBoxPosition()
	{
		updateElementIcon = false;
		RefreshMagicIconList (false);
		//Image selectedMagicIcon = magicElementIconList.GetComponentsInChildren<Image> () [magicElementIndex + 1]; // ditambahin +1 karena magicElementIconList punya component image
		//magicIconBoxSelect.transform.position = magicElementIconList.GetComponentsInChildren<Image> () [magicElementIndex + 1].transform.position;
		//magicIconBoxSelect.transform.position = selectedMagicIcon.transform.position;

		/*
		for (int i=0; i<magicElementList.Length; i++)
		{
			if(magicElementIndex == (int) magicElementList[i].magicElement)
				magicIconBoxSelect.transform.position = magicElementList[i].transform.position;
		}*/
		for (int i=0; i<magicElementList.Length; i++)
		{
			if((int)magicElementList[i].magicElement == magicElementIndex)
			{
				magicIconBoxSelect.transform.position = magicElementList[i].gameObject.transform.position;
			}
		}
		//magicIconBoxSelect.transform.position = magicElementList[magicElementIndex].transform.position;
	}

	public void SetTweakValue(int idMagic)
	{
		CharacterMagicPreset selectedMagicPreset = PartyManager.Instance.GetCharacter (PauseUIController.Instance.selectedCharId).Magics.GetMagicPreset(idMagic);
		_isNameDefault = selectedMagicPreset.IsNameDefault;
		_idMagicPreset = idMagic;

		nameMagicText.text = selectedMagicPreset.MagicPreset.Name;
		_previousName = selectedMagicPreset.MagicPreset.Name;

		_DamageMagicBar = (int)selectedMagicPreset.MagicPreset.MagicDamageModifier;


		Magic tweakMagic = new Magic();
		tweakMagic.MagicDamageModifier = _DamageMagicBar;
		tweakMagic.Name = selectedMagicPreset.MagicPreset.Name;
		tweakMagic.Element = selectedMagicPreset.MagicPreset.Element;
		tweakMagic.MagicTargetType = selectedMagicPreset.MagicPreset.MagicTargetType;

		//_tweakedSelectedMagicPreset = new CharacterMagicPreset(selectedMagicPreset._ID, selectedMagicPreset.MagicPreset);
		_tweakedSelectedMagic = tweakMagic;

		ChangeMagicTarget (_tweakedSelectedMagic.MagicTargetType);

		SetCharacterMagicElementIcon ();
		UpdateMagicTweaker ();
	}

	//<summary>only use it when create new magic</summary>
	public void SetTweakValue(Magic magic, bool isNameDefault)
	{
		//selectedMagicPreset = magic;
		_isNameDefault = isNameDefault;
		nameMagicText.text = magic.Name;
		_previousName = magic.Name;

		_DamageMagicBar = magic.MagicDamageModifier;
		ChangeMagicTarget (magic.MagicTargetType);

		_tweakedSelectedMagic = magic;

		SetCharacterMagicElementIcon ();
		UpdateMagicTweaker ();
	}

	public void SetFocusMagicTarget()
	{
		//focus to character magic preset target type
		TargetType target = TargetType.Single;
		if (_tweakedSelectedMagic != null)
			target = _tweakedSelectedMagic.MagicTargetType;
		else
			return;
		
		GameObject selectedTargetMagicObject = listTargetObject[0];

		/*
		switch (target) {
		case TargetType.Single:
			selectedTargetMagicObject = singleTarget;
			break;
		case TargetType.Row:
			selectedTargetMagicObject = rowTarget;
			break;
		case TargetType.Column:
			selectedTargetMagicObject = columnTarget;
			break;
		case TargetType.Square:
			selectedTargetMagicObject = squareTarget;
			break;
		case TargetType.All:
			selectedTargetMagicObject = allTarget;
			break;
		default:
			selectedTargetMagicObject = this.transform;
			break;
		}
		*/
		foreach (var obj in listTargetObject)
		{
			if(target == obj.GetComponent<SetMagicTargetTweak>().magicTarget)
				selectedTargetMagicObject = obj;
		}

		ChangeInteractableObject.DisableAllButtonInParentExcept (GetComponent<PanelChildren> ().ObjectChildren [1], selectedTargetMagicObject);
	}

	public void ChangeMagicTarget(TargetType target)
	{
		if (_tweakedSelectedMagic == null)
			return;
		
		_tweakedSelectedMagic.MagicTargetType = target;
		
		//UpdateMagicTarget ();
		// operate update magic target in here
		GameObject selectedTargetMagicObject = listTargetObject[0];

		/*
		switch (target) {
		case TargetType.Single:
			selectedTargetMagicObject = singleTarget;
			break;
		case TargetType.Row:
			selectedTargetMagicObject = rowTarget;
			break;
		case TargetType.Column:
			selectedTargetMagicObject = columnTarget;
			break;
		case TargetType.Square:
			selectedTargetMagicObject = squareTarget;
			break;
		case TargetType.All:
			selectedTargetMagicObject = allTarget;
			break;
		default:
			selectedTargetMagicObject = this.transform;
			break;
		}
		*/
		foreach (var obj in listTargetObject)
		{
			if(target == obj.GetComponent<SetMagicTargetTweak>().magicTarget)
				obj.GetComponent<SetMagicTargetTweak>().SelectTarget();
			else
				obj.GetComponent<SetMagicTargetTweak>().DeselectTarget();
		}

		//selectedTargetMagicObject.GetComponent<SetMagicTargetTweak> ().SelectTarget ();

//		if (!boxSelect.activeInHierarchy)
//			boxSelect.SetActive (true);
//
//		boxSelect.transform.position = selectedTargetMagicObject.position;
	}

	public void SaveData()
	{
		int idMagic = _idMagicPreset;
		MainCharacter selectedChar = PartyManager.Instance.GetCharacter (PauseUIController.Instance.selectedCharId);
		if (isCreateNew) {


			idMagic = selectedChar.Magics.GenerateID(); // get the predicted generated ID first, before its ID is assigned to new magic preset :) , change algorithm for get idmagic
			selectedChar.Magics.LearnMagicPreset (tweakedSelectedMagic, selectedChar._ID, true);

			isCreateNew = false;
		} else {
			SaveTweakedData (idMagicPreset, 
			                 tweakedSelectedMagic.Name,
			                 tweakedSelectedMagic.Description,
			                 tweakedSelectedMagic.MagicTargetType,
			                 tweakedSelectedMagic.MagicDamageModifier,
			                 IsNameDefault,
			                 tweakedSelectedMagic.Element);
		}
		selectedChar = PartyManager.Instance.GetCharacter (PauseUIController.Instance.selectedCharId);
		//change state first to make sure that panel magic is ready to process
		PauseUIController.Instance.ChangeState (PauseUIController.PauseUIControllerState.MagicPreset);
		
		TabPanelController.instance.panelMagicPreset.GetComponent<PanelMagicChildren> ().panelMagicList.GetComponent<GenerateMagicList> ().UpdateListMagic ();
		selectedChar = PartyManager.Instance.GetCharacter (PauseUIController.Instance.selectedCharId);

		TabPanelController.instance.panelMagicPreset.GetComponent<PanelMagicChildren> ().panelMagicList.GetComponent<GenerateMagicList> ().FocusToIndexButton (idMagic);
		selectedChar = PartyManager.Instance.GetCharacter (PauseUIController.Instance.selectedCharId);
		//SetFocusMagicTarget ();
	}
	
	void SaveTweakedData(int id,string name, string description, TargetType targetType, float damage, bool isNameDefault, Element element)
	{
		MainCharacter selectedChar = PartyManager.Instance.GetCharacter (PauseUIController.Instance.selectedCharId);

		selectedChar.Magics.ChangeMagicPreset (id, name,description, targetType, damage, 65f, isNameDefault);
		selectedChar.Magics.ChangeMagicPresetElement (id, element);
	}

	/*
	public void IncreasePointTweakDamage()
	{
		if (_damageMagicBar < 4) {
			_damageMagicBar++;
			if(_delayMagicBar<8){
				_delayMagicBar++;
			}
			else{
				_costMagicBar++;
			}
			if(_costMagicBar<8){
				_costMagicBar++;
			}
			else{
				_delayMagicBar++;
			}

			UpdateMagicTweaker();
			//_tweakedSelectedMagicPreset.IncreaseAttack();
		}
	}
	public void DecreasePointTweakDamage()
	{
		if (_damageMagicBar > 1) {
			_damageMagicBar--;
			if(_delayMagicBar>0){
				_delayMagicBar--;
			}
			else{
				_costMagicBar--;
			}
			if(_costMagicBar>0){
				_costMagicBar--;
			}
			else{
				_delayMagicBar--;
			}
			UpdateMagicTweaker();
			//_tweakedSelectedMagicPreset.DecreaseAttack();
		}
	}
	//function tweak delay
	public void IncreasePointTweakDelay()
	{
		if (_delayMagicBar < 8 && _costMagicBar > 0) {
			_delayMagicBar++;
			_costMagicBar--;

		}
		UpdateMagicTweaker();
	}
	public void DecreasePointTweakDelay()
	{
		if (_delayMagicBar > 0 && _costMagicBar < 8) {
			_delayMagicBar--;
			_costMagicBar++;

		}
		UpdateMagicTweaker();
	}
	//function tweak cost
	public void IncreasePointTweakCost()
	{
		if (_costMagicBar < 8 && _delayMagicBar > 0) {
			_costMagicBar++;
			_delayMagicBar--;
		}
		UpdateMagicTweaker();
	}
	public void DecreasePointTweakCost()
	{
		if (_costMagicBar > 0 && _delayMagicBar < 8) {
			_costMagicBar--;
			_delayMagicBar++;
		}
		UpdateMagicTweaker();
	}*/


}