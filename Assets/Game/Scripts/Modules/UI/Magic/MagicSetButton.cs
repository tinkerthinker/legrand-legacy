using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

[RequireComponent(typeof(MagicSkillData))]
public class MagicSetButton : MonoBehaviour, ISelectHandler, IDeselectHandler {
	public int Id;	//id Magic preset Character

//	private GameObject panelTweakMagic;

	void Start () {
//		panelTweakMagic = PauseUIController.Instance.panelTweakMagic;
		GetComponent<Button>().onClick.AddListener(() => OnClickHandler() );
	}

	void OnClickHandler()
	{
		PauseUIController.Instance.ChangeState (PauseUIController.PauseUIControllerState.MagicTweaker);
		/*
		panelTweakMagic.GetComponent<DataTweakMagic> ().indexMagicPreset = indexMagicPreset-1;

		panelTweakMagic.GetComponent<DataTweakMagic> ().SetTweakValue (id);
		PauseUIController.instance.panelTweakMagic.GetComponentInChildren<Button>().Select();
		ChangeInteractableObject.DisableAllButtonInParent(ChangeTabPanel.instance.panelMagic);
		*/

	}

	#region ISelectHandler implementation

	public void OnSelect (BaseEventData eventData)
	{
		int id = GetComponent<MagicSkillData> ().MagicPreset.ID;

		PauseUIController.Instance.PanelAssignMgcSkill.SetActive(true);
		PauseUIController.Instance.PanelAssignMgcSkill.GetComponent<DataTweakMagic> ().SetTweakValue (id);

//		panelTweakMagic.SetActive(true);
//		panelTweakMagic.GetComponent<DataTweakMagic> ().SetTweakValue (id);
	}

	#endregion

	#region IDeselectHandler implementation

	public void OnDeselect (BaseEventData eventData)
	{

	}

	#endregion
}
