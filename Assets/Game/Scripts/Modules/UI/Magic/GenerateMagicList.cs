using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class GenerateMagicList : AbsObjectListGenerator {
    //public ScrollController ScrollerController;
	public Button TargetMagicButton;
    public Button RemoveButton;
	public int seenButton;

	private int numberOfStep;

	private List<CharacterMagicPreset> selectedMagicPresets = new List<CharacterMagicPreset>();
    private List<NormalSkill> selectedNormalSkill = new List<NormalSkill>();
    private List<int> _SelectedCommandValue = new List<int>();
	private int currCharacterParty=0;

    //List<Button> _ListButtons = new List<Button>();

    //int indexButton;

    void Awake()
    {
        if (RemoveButton)
            _ListButtons.Add(RemoveButton);
    }

	void OnEnable()
	{
		if (PauseUIController.Instance != null && PartyManager.Instance != null)
		if (!PauseUIController.Instance.isFirstTimeLaunch)
        {            
            UpdateList(0);
            EventManager.Instance.AddListener<ChangeUICharSelectedID>(ChangeUICharSelectedIDHandler);
            StartCoroutine(WaitFocusFirstButton());
        }
	}

    IEnumerator WaitFocusFirstButton()
    {
        yield return null;
        if (!FocusToFirstButton())
        {
            if (ScrollerController.ScrollbarTarget)
                ScrollerController.ScrollbarTarget.Select();
        }
    }

    void OnDisable()
    {
        if(EventManager.Instance)
            EventManager.Instance.RemoveListener<ChangeUICharSelectedID>(ChangeUICharSelectedIDHandler);
    }

    void ChangeUICharSelectedIDHandler(ChangeUICharSelectedID e)
    {
        UpdateList(0);
        FocusToFirstButton();
    }

    /// <summary>
    /// 0 = all magic and normal skill, 1 = Magic Only, 2 = Skill Only
    /// </summary>
    public void UpdateList(int type=0)
    {
        

        base.UpdateList();

        for (int i = RemoveButton ? _SelectedCommandValue.Count + 1 : _SelectedCommandValue.Count; i < _ListButtons.Count; i++)
        {
            _ListButtons[i].GetComponent<MagicSkillData>().SetAsEquippedCommand(false);
        }
    }

    void UpdateListActiveCommandButton()
    {
        Legrand.core.MainCharacter selectedChar = PartyManager.Instance.GetCharacter(PauseUIController.Instance.selectedCharId);
        _SelectedCommandValue.Clear();
        for(int i=0; i<4; i++)
        {
            if (selectedChar.CommandPresets[i].CommandType != (int)CommandTypes.None)
            {
                if (selectedChar.CommandPresets[i].CommandType == (int)CommandTypes.Skill)
                {
                    NormalSkill skill = PartyManager.Instance.GetCharacter(PauseUIController.Instance.selectedCharId).NormalSkills.Find(x => x.CommandValue == selectedChar.CommandPresets[i].CommandValue);
                    if (skill != null)
                        //indexButton = UpdateEachSkillButton(skill, indexButton);
                        UpdateEachButton(skill);
                    else
                        continue;
                }
                else if (selectedChar.CommandPresets[i].CommandType == (int)CommandTypes.Magic)
                {
                    CharacterMagicPreset magic = PartyManager.Instance.GetCharacter(PauseUIController.Instance.selectedCharId).Magics.MagicPresets.Find(x => x.MagicPreset.ID == selectedChar.CommandPresets[i].CommandValue);
                    if(magic != null && magic.MagicPreset != null)
                        //indexButton = UpdateEachMagicButton(magic, indexButton);
                        UpdateEachButton(magic.MagicPreset);
                    else
                        continue;
                }
                _SelectedCommandValue.Add(selectedChar.CommandPresets[i].CommandValue);
                _ListButtons[indexButton - 1].GetComponent<MagicSkillData>().SetAsEquippedCommand(true);
            }
        }
    }

    //potentially unused
    public void UpdateListMagic()
    {
        UpdateListMagicButton();
        LegrandUtility.SetButtonsNavigationVertical(_ListButtons);

        if (_ListButtons.Count > indexButton)
        {
            for (int i = indexButton; i < _ListButtons.Count; i++)
            {
                Destroy(_ListButtons[i].gameObject);
            }
            _ListButtons.RemoveRange(indexButton, _ListButtons.Count - indexButton);
        }

        if (ScrollerController)
        {
            ScrollerController.Reset();
            for (int i = 0; i < _ListButtons.Count; i++)
            {
                ScrollerController.AddObjectChild(_ListButtons[i].gameObject);
            }
        }
    }

	void UpdateListMagicButton()
	{
        //int index=0;

		selectedMagicPresets = PartyManager.Instance.GetCharacter(PauseUIController.Instance.selectedCharId).Magics.MagicPresets;

		foreach (CharacterMagicPreset magic in selectedMagicPresets) {
            if (_SelectedCommandValue.Contains(magic.MagicPreset.ID))
                continue;
            //indexButton = UpdateEachMagicButton(magic, indexButton);
            UpdateEachButton(magic.MagicPreset);
		}
	}

    void UpdateListSkillButton()
    {
        selectedNormalSkill = PartyManager.Instance.GetCharacter(PauseUIController.Instance.selectedCharId).NormalSkills;

        foreach (NormalSkill skill in selectedNormalSkill)
        {
            if (_SelectedCommandValue.Contains(skill.CommandValue))
                continue;
            //indexButton = UpdateEachSkillButton(skill, indexButton);
            UpdateEachButton(skill);
        }
    }

	public Button GetMagicPresetButton(int idMagic)
	{
		int i = 0;
		foreach (CharacterMagicPreset magic in selectedMagicPresets) {
			if(magic._ID == idMagic)
			{
				return _ListButtons[i];
			}
			i++;
		}
		return null;
	}

	public bool FocusToIndexButton(int idMagic)
	{
		int i = 0;
		foreach (CharacterMagicPreset magic in selectedMagicPresets) {
			if(magic._ID == idMagic)
			{
				_ListButtons[i].Select();
				return true;
			}
			i++;
		}
		return false;
	}

	public override bool FocusToFirstButton()
	{
        if (RemoveButton)
        {
            RemoveButton.Select();
            return true;
        }
        else if (_ListButtons.Count > 0 && _ListButtons[0] != null)
        {
            _ListButtons[0].Select();
            return true;
        }
		return false;
	}

    public bool FocusToButton(CommandTypes type, int id)
    {
        Magic magicPreset;
        NormalSkill skill;

        if (type == CommandTypes.None && RemoveButton)
        {
            RemoveButton.Select();
            return true;
        }
        for (int i = 0; i < _ListButtons.Count;i++ )
        {
            magicPreset = _ListButtons[i].GetComponent<MagicSkillData>().MagicPreset;
            skill = _ListButtons[i].GetComponent<MagicSkillData>().NormalSkillData;

            if(type == CommandTypes.Magic && magicPreset != null)
            {
                if(magicPreset.ID == id)
                {
                    _ListButtons[i].Select();
                    return true;
                }
            }
            else if (type == CommandTypes.Skill && skill != null)
            {
                if (skill.CommandValue == id)
                {
                    _ListButtons[i].Select();
                    return true;
                }
            }
        }
        return false;
    }

    public override void StartLoopUpdate()
    {
        if (RemoveButton)
            indexButton = 1;
        else
            indexButton = 0;

        UpdateListActiveCommandButton();
        UpdateListSkillButton();
        UpdateListMagicButton();
    }

    public override bool CheckConditionPreUpdate(object data)
    {
        return true;
    }

    public override void SetButtonName(Button button, object data)
    {
        if(data is Magic)
            button.name = ((Magic)data).Name;
        else if (data is NormalSkill)
            button.name = ((NormalSkill)data).Name;
    }

    public override void AfterDataUpdatedProcess(IObjectData objectData, object data)
    {
        
    }
}