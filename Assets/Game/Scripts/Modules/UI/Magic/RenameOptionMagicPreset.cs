using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;
using Legrand.core;
using TeamUtility.IO;

public class RenameOptionMagicPreset : MonoBehaviour, IUpdateSelectedHandler {
	public Image defaultNameImage;

	[System.NonSerialized]
	public CharacterMagicPreset selectedCharMagicPreset;

	#region IUpdateSelectedHandler implementation

	public void OnUpdateSelected (BaseEventData eventData)
	{
		if (InputManager.GetButtonDown("Menu")) {
			PauseUIController.Instance.ChangeState(PauseUIController.PauseUIControllerState.MagicRename);
		}
	}

	#endregion
}
