﻿using UnityEngine;
using System.Collections;

public class GenerateMagicName : MonoBehaviour {
	//nama berdasar damage value
	private static string[] fireMagicName = {"Tine","Tinemor","Tinesaigh","Tinelast"};
	private static string[] earthMagicName = {"Talamh","Talamhor","Talamhsaigh","Talamhlast"};
	private static string[] waterMagicName = {"Uisce","Uiscemor","Uiscesaigh","Uiscelast"};
	private static string[] lightningMagicName = {"Tintre","Tintremor","Tintresaigh","Tintresaigh"};
	private static string[] lightMagicName = {"Solas","Solamor","Solasaigh","Solaslast"};
	private static string[] darkMagicName = {"Dorka","Dorkamor","Dorkasaigh","Dorkalast"};
	private static string[] airMagicName = {"Aer","Aermor","Aersaigh","Aerlast"};

	static string GenerateCharMagicName(Element element,float fDamage, TargetType targetMagic)
	{
        int damage = Mathf.RoundToInt(fDamage);
		string magicName;
		switch (element) {
		case Element.Fire:
			return magicName = fireMagicName[damage-1]+GenerateMagicTargetName(targetMagic);
		case Element.Earth:
			return magicName = earthMagicName[damage-1]+GenerateMagicTargetName(targetMagic);
		case Element.Water:
			return magicName = waterMagicName[damage-1]+GenerateMagicTargetName(targetMagic);
		case Element.Lighting:
			return magicName = lightningMagicName[damage-1]+GenerateMagicTargetName(targetMagic);
		case Element.Light:
			return magicName = lightMagicName[damage-1]+GenerateMagicTargetName(targetMagic);
		case Element.Dark:
			return magicName = darkMagicName[damage-1]+GenerateMagicTargetName(targetMagic);
		case Element.Air:
			return magicName = airMagicName[damage-1]+GenerateMagicTargetName(targetMagic);
		default:
			return "Ruin"+" "+GenerateMagicTargetName(targetMagic);
		}

	}


	static string GenerateMagicTargetName(TargetType targetMagic)
	{
		switch (targetMagic) {
		case TargetType.Single:
			return "";
		case TargetType.Column:
			return " Thrust";
		case TargetType.Row:
			return " Slash";
		case TargetType.Square:
			return " Burst";
		case TargetType.All:
			return " Grand";
		default:
			return "";
		}
	}

	public static string GenerateNameAttackMagic(Magic selectedMagic)
	{
		string name;

		name = GenerateCharMagicName (selectedMagic.Element, selectedMagic.MagicDamageModifier, selectedMagic.MagicTargetType);
		return name;
	}

}
