using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;

public class CreateNewMagicPreset : MonoBehaviour {
	int indexNewMagicPreset=0;
	MainCharacter selectedChar;

	public void CreateNewMagic()
	{
		string selectedCharId = PauseUIController.Instance.selectedCharId;

		selectedChar = PartyManager.Instance.GetCharacter (selectedCharId);

		if (selectedChar.Magics.MagicPresets.Count >= selectedChar.Magics.MaxMagicPreset) {
			PopUpUI.OnYesChoicePopUp += SelectThisButton;
			PopUpUI.CallNotifPopUp("Maximum preset reached, can't create other magic preset for this character");
			return;
		}

		Magic newMagic = new Magic ();

		newMagic.Element = selectedChar.CharacterElements.Elements [0];//get the first and only element from character besides eris
		
		newMagic.MagicDamageModifier = 100f;
		newMagic.InteruptChance = 65f;
		newMagic.MagicTargetType = TargetType.Single;
		newMagic.Name = GenerateMagicName.GenerateNameAttackMagic (newMagic);
		
		
		UpdateAndTweakNewMagic (newMagic);
	}

	void SelectThisButton()
	{
		GetComponent<Button> ().Select ();
		PopUpUI.OnYesChoicePopUp -= SelectThisButton;
	}

	private void UpdateAndTweakNewMagic(Magic newMagic)
	{
		//buka panel magic tweak, ditujukan ke preset baru
		GameObject panelTweakMagic = PauseUIController.Instance.PanelAssignMgcSkill;
		
		panelTweakMagic.GetComponent<DataTweakMagic> ().SetTweakValue (newMagic, true);
		panelTweakMagic.GetComponent<DataTweakMagic> ().isCreateNew = true;

		PauseUIController.Instance.ChangeState (PauseUIController.PauseUIControllerState.MagicTweaker);

		//PauseUIController.instance.panelTweakMagic.GetComponentInChildren<Button>().Select();

		//ChangeInteractableObject.DisableAllButtonInParent(ChangeTabPanel.instance.panelMagic.GetComponentInChildren<GenerateMagicList> ().gameObject);
	}
}
