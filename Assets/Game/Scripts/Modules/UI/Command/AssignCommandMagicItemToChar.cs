using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;
using TeamUtility.IO;

public class AssignCommandMagicItemToChar : MonoBehaviour, ISelectHandler, IDeselectHandler, IUpdateSelectedHandler {
	public Button targetButton;
	public CommandPos panelPosition;
	public CommandPos slotPosition;
	public CommandTypes commandType;
    public bool IsFixedType = true;

	public int valueId;
	[SerializeField]
	private Button targetCancelButton;
    public bool IsFixedCancelButton = true;

	public GameObject HintSwapObject;

    public GenerateMagicList Generator;

    private Item _ChoosenItem;
    private Magic _ChoosenMagic;
    private NormalSkill _ChoosenSkill;

	void Start()
	{
		GetComponent<Button>().onClick.AddListener(() => OnClickHandler() );
	}

    void OnEnable()
    {
        if(!IsFixedType)
            EventManager.Instance.AddListener<ChoosenCommandDataEvent>(ChoosenCommandDataEventHandler);
    }

    void OnDisable()
    {
        if(!IsFixedType && EventManager.Instance)
            EventManager.Instance.RemoveListener<ChoosenCommandDataEvent>(ChoosenCommandDataEventHandler);
    }

    void ChoosenCommandDataEventHandler(ChoosenCommandDataEvent e)
    {
        commandType = e.Type;
        switch(commandType)
        {
            case CommandTypes.Item:
                _ChoosenItem = ((Item)e.GetData());
                break;
            case CommandTypes.Magic:
                _ChoosenMagic = ((Magic)e.GetData());
                break;
            case CommandTypes.Skill:
                _ChoosenSkill = ((NormalSkill)e.GetData());
                break;
        }

        SetTargetCancelButton(e.SentGameObject.GetComponent<Button>());
    }

	void OnClickHandler ()
	{
        if (commandType == CommandTypes.Item && PanelInventoryController.Instance != null)
        {
            if (!PanelInventoryController.Instance.IsSwapping)
                PanelInventoryController.Instance.AssignCommandToArrange = this;
        }
	}

	public void SwapCommand()
	{
        if (commandType == CommandTypes.Magic || commandType == CommandTypes.Skill)
        {
			
		} else if (commandType == CommandTypes.Item) {
			if (PanelInventoryController.Instance.IsSwapping) {
				SwapCommandItem ();

				PanelInventoryController.Instance.IsSwapping = false;
			} else {
				PanelInventoryController.Instance.AssignCommandToArrange = this;

				if(HintSwapObject)
					HintSwapObject.SetActive (true);

				PanelInventoryController.Instance.IsSwapping = true;
			}
		}
	}

	public void AssignCommand()
	{
		if (commandType == CommandTypes.Magic) {
			AssignCommandMagic ();
        }
        else if (commandType == CommandTypes.Skill)
        {
            AssignCommandSkill();
        }
        else if (commandType == CommandTypes.Item)
        {
            AssignCommandItem();
            if (!PartyManager.Instance.Inventory.Contains(PanelInventoryController.Instance.AssignedItem))
            {
                OnCancel();
                PanelInventoryController.Instance.BackToInventoryList();
                PanelInventoryController.Instance.DisableShownMembersBtn();
            }
            EventManager.Instance.TriggerEvent(new AssignNewCommandEvent(PauseUIController.Instance.selectedCharId));
        }
        else if(commandType == CommandTypes.None)
        {
            RemoveCommand();
        }
        GetComponent<CommandButtonSet>().SetCommand();
        GetComponent<CommandButtonSet>().SendInspectEvent();

        if (Generator)
        {
            Generator.UpdateList();
            HighlightChoosenAgain();
        }
	}

    void HighlightChoosenAgain()
    {
        FocusToTargetButton();
        Generator.FocusToButton(commandType, commandType == CommandTypes.Magic ? _ChoosenMagic.ID : commandType == CommandTypes.Magic ?_ChoosenSkill.CommandValue:-1);
    }

	void SwapCommandMagic()
	{

	}

	void SwapCommandItem()
	{
		MainCharacter selectedChar = PartyManager.Instance.GetCharacter (PauseUIController.Instance.selectedCharId);
		CommandData commandTo = selectedChar.GetCommand(panelPosition, slotPosition);
		AssignCommandMagicItemToChar assignCommandFrom = PanelInventoryController.Instance.AssignCommandToArrange;
		CommandData commandFrom = selectedChar.GetCommand(assignCommandFrom.panelPosition, assignCommandFrom.slotPosition);

		selectedChar.SwapItem (commandFrom.CommandValue, commandTo.CommandValue);

		//to make sure the hint object is disabled
		if(assignCommandFrom.HintSwapObject)
			assignCommandFrom.HintSwapObject.SetActive (false);

		foreach(CommandButtonSet command in targetButton.transform.parent.GetComponentsInChildren<CommandButtonSet>())
		{
			command.SetCommand();
		}
	}

	void AssignCommandMagic()
	{
        if (_ChoosenMagic == null)
            return;

		MainCharacter selectedChar = PartyManager.Instance.GetCharacter (PauseUIController.Instance.selectedCharId);
		CommandData selectedCharCommand = selectedChar.GetCommand(panelPosition, slotPosition);

        valueId = _ChoosenMagic.ID;

		//Find if current selected command already assigned.
		CommandData deletedCommand = FindCommmandAssigned (commandType, selectedChar, valueId);

		//temp command
		int tempCommandType, tempCommandValue;
		tempCommandType = selectedCharCommand.CommandType;
		tempCommandValue = selectedCharCommand.CommandValue;

		deletedCommand.CommandType = tempCommandType;
		deletedCommand.CommandValue = tempCommandValue;

		selectedCharCommand.CommandType = (int)commandType;
		selectedCharCommand.CommandValue = valueId;

		//update command UI
		foreach (CommandButtonSet command in targetButton.transform.parent.GetComponentsInChildren<CommandButtonSet>())
		{
			if ((int)command.panelPosition == deletedCommand.PanelPos && (int)command.slotPosition == deletedCommand.SlotPos)
				command.SetCommand ();
		}
	}

    void AssignCommandSkill()
    {
        if (_ChoosenSkill == null)
            return;

        MainCharacter selectedChar = PartyManager.Instance.GetCharacter(PauseUIController.Instance.selectedCharId);
        CommandData selectedCharCommand = selectedChar.GetCommand(panelPosition, slotPosition);

        valueId = _ChoosenSkill.CommandValue;

        //Find if current selected command already assigned.
        CommandData deletedCommand = FindCommmandAssigned(commandType, selectedChar, valueId);

        if (deletedCommand != selectedCharCommand)
        {
            //temp command
            int tempCommandType, tempCommandValue;
            tempCommandType = selectedCharCommand.CommandType;
            tempCommandValue = selectedCharCommand.CommandValue;

            deletedCommand.CommandType = tempCommandType;
            deletedCommand.CommandValue = tempCommandValue;

            selectedCharCommand.CommandType = (int)commandType;
            selectedCharCommand.CommandValue = valueId;

            //update deleted command UI
            foreach (CommandButtonSet command in targetButton.transform.parent.GetComponentsInChildren<CommandButtonSet>())
            {
                if ((int)command.panelPosition == deletedCommand.PanelPos && (int)command.slotPosition == deletedCommand.SlotPos)
                    command.SetCommand();
            }
        }
        else
        {
            selectedCharCommand.CommandType = (int)commandType;
            selectedCharCommand.CommandValue = valueId;
        }
    }

	void AssignCommandItem()
	{
		if(transform.parent.GetComponent<GenerateMagicItemCommandList>() != null)
			transform.parent.GetComponent<GenerateMagicItemCommandList>().UpdateListMagicItemCommand();

		MainCharacter selectedChar = PartyManager.Instance.GetCharacter (PauseUIController.Instance.selectedCharId);
		Item itemData = PanelInventoryController.Instance.AssignedItem;

		int index = 0;
		int indexPrevSlot = -1;
		int indexItemSlot = valueId;

		foreach (ItemSlot itemSlot in selectedChar.Items)
		{
			if(itemSlot != null && 
				itemSlot.ItemMaster != null && 
				itemSlot.ItemMaster._ID == itemData._ID && 
				index != indexItemSlot)
			{
				//hapus data dari slot sebelumnya dulu jika ada ID yang sama di slot yang berbeda, dikembalikan ke inventory item nya
				indexPrevSlot = index;
				break;
			}
			index++;
		}
		
		selectedChar.AddItems (itemData, indexItemSlot);	// maybe need a return value to determine if add item success ????

		if(indexPrevSlot >= 0)
			selectedChar.ReturnItem(indexPrevSlot);
	}

    void RemoveCommand()
    {
        MainCharacter selectedChar = PartyManager.Instance.GetCharacter(PauseUIController.Instance.selectedCharId);
        valueId = selectedChar.GetCommand(panelPosition, slotPosition).CommandValue;
        selectedChar.SetCommand(panelPosition, slotPosition, CommandTypes.None, valueId);
    }

	private CommandData FindCommmandAssigned(CommandTypes target, MainCharacter character, int commandValue)
	{
		foreach (var command in character.CommandPresets) {
			if(command.CommandType == (int)target && command.CommandValue == commandValue)
			{
				return command;
			}
		}
		return character.GetCommand(panelPosition, slotPosition);
	}

	//focus to targeted button, which is the button that want to assign
	public void FocusToTargetButton()
	{
		if (targetCancelButton)
			targetCancelButton.Select ();
		return;
	}

    public void SetTargetCancelButton(Button btn)
    {
        if (!IsFixedCancelButton)
            targetCancelButton = btn;
    }

	public void OnCancel()
	{
		if (commandType == CommandTypes.Item && PanelInventoryController.Instance.CurrentState == PanelInventoryController.InventoryState.ArrangeCharSlot) {
			if (PanelInventoryController.Instance.IsSwapping) {
				AssignCommandMagicItemToChar assignCommandFrom = PanelInventoryController.Instance.AssignCommandToArrange;

				if(assignCommandFrom.HintSwapObject)
					assignCommandFrom.HintSwapObject.SetActive (false);
				PanelInventoryController.Instance.IsSwapping = false;
			} else {
				FocusToTargetButton ();
			}
		} else {

            if (commandType == CommandTypes.Magic || commandType == CommandTypes.Skill || commandType == CommandTypes.None)
            {
				FocusToTargetButton ();
			}
            else if (commandType == CommandTypes.Item)
            {
                FocusToTargetButton();
                ChangeInteractableObject.DisableAllButtonInParent(transform.parent.gameObject);
            }
		}
	}

	#region IUpdateSelectedHandler implementation

	public void OnUpdateSelected (BaseEventData eventData)
	{
		
	}

	#endregion

	#region ISelectHandler implementation

	public void OnSelect (BaseEventData eventData)
	{
		
	}

	#endregion

	#region IDeselectHandler implementation

	public void OnDeselect (BaseEventData eventData)
	{

		
	}

	#endregion
}
