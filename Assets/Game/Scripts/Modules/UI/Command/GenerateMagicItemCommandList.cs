using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;

public class GenerateMagicItemCommandList : MonoBehaviour {
	public Button targetMICommandButton; //semua nama magic item command disingkat jadi MICommand, MI = MagicItem
	public Scrollbar scrollBarMICommand;
	public int currentIndexMICommandButton=0;
	public int seenButton;
	private Button _selectedCommandSetButton;	//untuk digunakan di variabel target button di assign command magic item
	public Button selectedCommandSetButton
	{
		set{
			_selectedCommandSetButton = value;
			foreach (var MICommandButton in MICommandButtons) {
				MICommandButton.GetComponent<AssignCommandMagicItemToChar>().targetButton = _selectedCommandSetButton;
				if(selectedCommandType == CommandTypes.Item)
					MICommandButton.GetComponent<AssignItemToList>().targetButton = _selectedCommandSetButton;
			}
		}
		get{return _selectedCommandSetButton;}
	}
	public CommandTypes selectedCommandType;
	private CommandPos _panelPosition;
	public CommandPos panelPosition
	{
		set{
			_panelPosition = value;
			foreach (var MICommandButton in MICommandButtons) {
				MICommandButton.GetComponent<AssignCommandMagicItemToChar>().panelPosition = _panelPosition;
			}
		}
		get{return _panelPosition;}
	}
	private CommandPos _slotPosition;
	public CommandPos slotPosition
	{
		set{
			_slotPosition = value;
			foreach (var MICommandButton in MICommandButtons) {
				MICommandButton.GetComponent<AssignCommandMagicItemToChar>().slotPosition = _slotPosition;
			}
		}
		get{return _slotPosition;}
	}
	public Text panelInfoText;

	private int numberOfStep;
	List<Button> MICommandButtons = new List<Button>();
	
	void Start () 
	{
	}

	void OnEnable()
	{
		if(PauseUIController.Instance != null)
			if(!PauseUIController.Instance.isFirstTimeLaunch)
				UpdateListMagicItemCommand ();
	}
	
	void GenerateListMagic()
	{
		Button newMICommandSetButton;
		
		string selectedCharId = PauseUIController.Instance.selectedCharId;
		CharacterMagicPresets selectedMagicPresets;

		selectedMagicPresets = PartyManager.Instance.GetCharacter (PauseUIController.Instance.selectedCharId).Magics;

		int i=0;

		foreach (var magic in selectedMagicPresets.MagicPresets) {
			newMICommandSetButton = Instantiate (targetMICommandButton) as Button;
			newMICommandSetButton.name = magic.MagicPreset.Name;

			newMICommandSetButton.GetComponentInChildren<Text>().text = magic.MagicPreset.Name;

			newMICommandSetButton.GetComponent<MagicSkillData>().MagicPreset = magic.MagicPreset;

			MICommandButtons.Add (newMICommandSetButton);
			i++;
		}

		numberOfStep = MICommandButtons.Count - (seenButton-1);
		
		foreach (var MICommandButton in MICommandButtons) {
			MICommandButton.transform.SetParent(this.transform);
			MICommandButton.GetComponent<RectTransform> ().localScale = new Vector3(1,1,1);

			MICommandButton.GetComponent<ScrollUpDown>().seenRow = seenButton;
			MICommandButton.GetComponent<ScrollUpDown>().scrollBar = scrollBarMICommand;

			MICommandButton.GetComponent<AssignCommandMagicItemToChar>().targetButton = _selectedCommandSetButton;
			MICommandButton.GetComponent<AssignCommandMagicItemToChar>().panelPosition = panelPosition;
			MICommandButton.GetComponent<AssignCommandMagicItemToChar>().slotPosition = slotPosition;
			MICommandButton.GetComponent<AssignCommandMagicItemToChar>().commandType = CommandTypes.Magic;

			MICommandButton.GetComponent<FadingScrollbar>().ScrollbarAnimator = scrollBarMICommand.GetComponent<Animator>();
		}

	}

	void GenerateListItem()
	{
		Button newMICommandSetButton;

		MainCharacter selectedChar = PartyManager.Instance.GetCharacter(PauseUIController.Instance.selectedCharId);

		GenerateItemListButton ();

		numberOfStep = MICommandButtons.Count - (seenButton-1);

		foreach (var MICommandButton in MICommandButtons) {
			MICommandButton.transform.SetParent(this.transform);
			MICommandButton.GetComponent<RectTransform> ().localScale = new Vector3(1,1,1);

			MICommandButton.GetComponent<ScrollUpDown>().seenRow = seenButton;
			MICommandButton.GetComponent<ScrollUpDown>().scrollBar = scrollBarMICommand;

			MICommandButton.GetComponent<AssignCommandMagicItemToChar>().targetButton = _selectedCommandSetButton;
			MICommandButton.GetComponent<AssignCommandMagicItemToChar>().panelPosition = panelPosition;
			MICommandButton.GetComponent<AssignCommandMagicItemToChar>().slotPosition = slotPosition;
			MICommandButton.GetComponent<AssignCommandMagicItemToChar>().commandType = CommandTypes.Item;

			MICommandButton.GetComponent<AssignItemToList>().amountSetObject = TabPanelController.instance.panelCommand.GetComponent<PanelChildren>().ObjectChildren[1].GetComponent<PanelItemChildren>().amountSetObject;
			MICommandButton.GetComponent<AssignItemToList>().targetButton = _selectedCommandSetButton;

			MICommandButton.GetComponent<FadingScrollbar>().ScrollbarAnimator = scrollBarMICommand.GetComponent<Animator>();
		}

	}

	void GenerateItemListButton()
	{
		MainCharacter selectedChar = PartyManager.Instance.GetCharacter(PauseUIController.Instance.selectedCharId);

		foreach (var itemSlot in PartyManager.Instance.Inventory.Items [(int)Item.Type.Consumable])
		//foreach (ItemSlot itemSlot in selectedChar.Items)
		{
			Item item = itemSlot.ItemMaster;
			if(item.ItemType != Item.Type.Consumable)
				continue;
			Consumable temp = (Consumable)item;
			if (temp.ConsumableType == Consumable.ConsumeType.Healing || temp.ConsumableType == Consumable.ConsumeType.Magic)
			{
				Button newMICommandSetButton = Instantiate (targetMICommandButton) as Button;
				newMICommandSetButton.name = item.Name;
				
				newMICommandSetButton.GetComponent<ItemInventoryData> ().ItemSlotData = itemSlot;
				newMICommandSetButton.GetComponent<ItemInventoryData> ().textItemDescription = panelInfoText;
				
				MICommandButtons.Add (newMICommandSetButton);
			}
		}
	}

	void CalculateHeight()
	{
		Rect currRect = GetComponent<RectTransform> ().rect;
		float height;

		height = transform.parent.GetComponent<RectTransform> ().rect.height / seenButton;

		if(GetComponentInChildren<Button>() != null)
			height *= MICommandButtons.Count;
		else
			height *= 0f;
		
		currRect.height = height;
		GetComponent<RectTransform> ().sizeDelta = new Vector2(0, currRect.height); // << Change Rect Transform Height
	}

	void RemoveList()
	{
		foreach (Button button in MICommandButtons) {
			Destroy(button.gameObject);
		}
		MICommandButtons.RemoveAll ((Button obj) => obj);
	}
	
	public void UpdateListMagicItemCommand()
	{
		RemoveList ();

		if(selectedCommandType == CommandTypes.Magic)
			GenerateListMagic ();
		else if(selectedCommandType == CommandTypes.Item)
			GenerateListItem ();

		CalculateHeight ();
	}

	public bool FocusToFirstButton()
	{
		if (MICommandButtons.Count>0 && MICommandButtons [0] != null) {
			MICommandButtons [0].Select ();
			scrollBarMICommand.value = 1;
			return true;
		}
		return false;
	}
}
