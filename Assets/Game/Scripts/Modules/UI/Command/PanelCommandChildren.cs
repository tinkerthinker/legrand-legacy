using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class PanelCommandChildren : MonoBehaviour {
	public GameObject panelCommandButton;
	public GameObject panelSetCommand;
	public GameObject panelCommandList;
	public GameObject panelMagicItemCommand;

	public GameObject panelMagicCommandList;
	public GameObject panelItemCommandList;

	private int currSelectedCharId;

	void Init()
	{
		EventManager.Instance.AddListener<ChangeUICharSelectedID>(ChangeCharSelectedIdHandler);
	}

	void Start()
	{
		Init ();
	}

	void OnDestroy()
	{
		if (EventManager.Instance) {
			EventManager.Instance.RemoveListener<ChangeUICharSelectedID> (ChangeCharSelectedIdHandler);
		}
	}

	public void UpdateCommandButton()
	{
		foreach(var commandButton in panelCommandButton.GetComponentsInChildren<CommandButtonSet>())
		{
			commandButton.SetCommand();
		}
	}

	void ChangeCharSelectedIdHandler(ChangeUICharSelectedID c)
	{
		SetCurrCharacterCommandButton ();
	}

	void SetCurrCharacterCommandButton()
	{
		if(panelMagicCommandList.GetComponent<GenerateMagicItemCommandList>() != null)
		{
			bool inCommandPanel=false;
			if (EventSystem.current.currentSelectedGameObject.transform.IsChildOf (panelMagicCommandList.transform))
				inCommandPanel = true;
			
			panelMagicCommandList.GetComponent<GenerateMagicItemCommandList>().UpdateListMagicItemCommand();
			
			if(inCommandPanel)
			{
				if( !panelMagicCommandList.GetComponent<GenerateMagicItemCommandList>().FocusToFirstButton())
				{
					Button button = panelMagicCommandList.GetComponent<GenerateMagicItemCommandList>().selectedCommandSetButton;
					button.Select();
					ChangeInteractableObject.EnableAllButtonInParent(button.transform.parent.gameObject);
				}
			}
		}
		
		if(panelItemCommandList.GetComponent<GenerateMagicItemCommandList>() != null)
		{
			bool inCommandPanel=false;
			if (EventSystem.current.currentSelectedGameObject.transform.IsChildOf (panelItemCommandList.transform))
				inCommandPanel = true;
			
			panelItemCommandList.GetComponent<GenerateMagicItemCommandList>().UpdateListMagicItemCommand();
			
			if(inCommandPanel)
			{
				if( !panelItemCommandList.GetComponent<GenerateMagicItemCommandList>().FocusToFirstButton())
				{
					Button button = panelItemCommandList.GetComponent<GenerateMagicItemCommandList>().selectedCommandSetButton;
					button.Select();
					ChangeInteractableObject.EnableAllButtonInParent(button.transform.parent.gameObject);
				}
			}
		}
		
		foreach(CommandButtonSet command in GetComponent<PanelChildren>().ObjectChildren[0].GetComponentsInChildren<CommandButtonSet>())
		{
			command.SetCommand();
		}
		
		foreach(CommandButtonSet command in GetComponent<PanelChildren>().ObjectChildren[1].GetComponentsInChildren<CommandButtonSet>())
		{
			command.SetCommand();
		}

	}

}
