using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using Legrand.core;
using TeamUtility.IO;

public class CommandButtonSet : MonoBehaviour, IUpdateSelectedHandler, ISelectHandler, IDeselectHandler {
	public Text CommandButtonText;
	public Text AlternateInfoText;
	public Image MagicImage;
	public Text ItemAOEText;

	public Image CommandImage;
	public Sprite NoCommandSprite;

    public Image SlotImage;
    public Color FilledSlotColor;
    public Color EmptySlotColor;

	public CommandPos panelPosition;
	public CommandPos slotPosition;

	[SerializeField]
	private Button targetCancelButton;
    [SerializeField]
    private bool _IsHandleChangeChar = false;


    //private GameObject panelSetCommand;
    //private GameObject panelCommandList;

	private GenerateMagicItemCommandList generatingObject = null;
	private GameObject buttonPanel = null, otherPanel;

	private CommandData _SelectedCommandData;

    void Awake()
    {
        
    }

	void Start(){
		MainCharacter selectedChar = PartyManager.Instance.GetCharacter (PauseUIController.Instance.selectedCharId);
		_SelectedCommandData = selectedChar.GetCommand(panelPosition, slotPosition);
		
		//panelSetCommand = ChangeTabPanel.instance.panelCommand.GetComponent<PanelCommandChildren> ().panelSetCommand;
		//panelCommandList = ChangeTabPanel.instance.panelCommand.GetComponent<PanelCommandChildren>().panelCommandList;
	}

	void OnEnable()
	{
        if (_IsHandleChangeChar)
        {
            EventManager.Instance.AddListener<ChangeUICharSelectedID>(ChangeUICharSelectedIDHandler);
            SetCommand();
        }
	}

    void  OnDestroy()
    {
        if(EventManager.Instance)
            EventManager.Instance.RemoveListener<ChangeUICharSelectedID>(ChangeUICharSelectedIDHandler);
    }

	void OnDisable()
	{
        if (EventManager.Instance)
            EventManager.Instance.RemoveListener<ChangeUICharSelectedID>(ChangeUICharSelectedIDHandler);

		if (PauseUIController.Instance.isFirstTimeLaunch)
			return;
		if (EventSystem.current && EventSystem.current.currentSelectedGameObject == this.gameObject)
		{
			if(otherPanel)
				ChangeInteractableObject.EnableAllButtonInParent (otherPanel);
			targetCancelButton.interactable = true;
		}
	}

    void ChangeUICharSelectedIDHandler(ChangeUICharSelectedID e)
    {
        SetCommand();
    }

   public void SendInspectEvent()
    {
        if (_SelectedCommandData == null)
        {
            EventManager.Instance.TriggerEvent(new InspectMagicSkillEvent(null, null, true));
            return;
        }

        if (_SelectedCommandData.CommandType == (int)CommandTypes.Item)
        {
            if (_SelectedCommandData.CommandValue >= 0)
            {
                MainCharacter selectedChar = PartyManager.Instance.GetCharacter(PauseUIController.Instance.selectedCharId);

                ItemSlot itemSlot = selectedChar.Items[_SelectedCommandData.CommandValue];
                if (itemSlot != null &&
                    itemSlot.ItemMaster != null) //&&
                    //itemSlot.Stack > 0)
                {
                    //					EventManager.Instance.TriggerEvent (new ComparingItemEvent(true));
                    EventManager.Instance.TriggerEvent(new InspectItemEvent(itemSlot.ItemMaster));
                    EventManager.Instance.TriggerEvent(new ChoosenCommandDataEvent(CommandTypes.Item, itemSlot.ItemMaster, gameObject));
                }
            }
        }
        else if (_SelectedCommandData.CommandType == (int)CommandTypes.Magic)
        {
            if (_SelectedCommandData.CommandValue >= 0)
            {
                Magic selectedMagic = PartyManager.Instance.GetCharacter(PauseUIController.Instance.selectedCharId).GetMagic(_SelectedCommandData.CommandValue);

                EventManager.Instance.TriggerEvent(new InspectMagicSkillEvent(selectedMagic, null, true));
            }
        }
        else if (_SelectedCommandData.CommandType == (int)CommandTypes.Skill)
        {
            if (_SelectedCommandData.CommandValue >= 0)
            {
                NormalSkill selectedSkill = PartyManager.Instance.GetCharacter(PauseUIController.Instance.selectedCharId).GetNormalSkill(_SelectedCommandData.CommandValue);

                EventManager.Instance.TriggerEvent(new InspectMagicSkillEvent(null, selectedSkill, true));
            }
        }
        else
        {
            EventManager.Instance.TriggerEvent(new InspectMagicSkillEvent(null, null, true));
        }
    }

	public void SetCommand () {
		SetCommand (PauseUIController.Instance.selectedCharId);
	}

	public void SetCommand (string 	characterID) {
		MainCharacter selectedChar = PartyManager.Instance.GetCharacter (characterID);
        _SelectedCommandData = selectedChar.GetCommand(panelPosition, slotPosition);

        if (_SelectedCommandData.CommandType == (int)CommandTypes.Magic)
		{
            CharacterMagicPreset selectedMagicPreset = selectedChar.Magics.GetMagicPreset(_SelectedCommandData.CommandValue);
            if (selectedMagicPreset != null)
            {
                if (CommandButtonText)
                    CommandButtonText.text = selectedMagicPreset.MagicPreset.Name;
                if (ItemAOEText != null)
                    ItemAOEText.text = selectedMagicPreset.MagicPreset.MagicTargetType.ToString().Substring(0, 1);
                if (CommandImage != null)
                {
                    CommandImage.sprite = LegrandBackend.Instance.GetSpriteData("Element" + selectedMagicPreset.MagicPreset.Element.ToString());
                    if (SlotImage)
                        SlotImage.color = FilledSlotColor;
                }
            }
            else
                _SelectedCommandData.CommandType = (int)CommandTypes.None;
        }
        else if (_SelectedCommandData.CommandType == (int)CommandTypes.Skill)
        {
            NormalSkill selectedSkill = selectedChar.GetNormalSkill(_SelectedCommandData.CommandValue);
            if (selectedSkill != null)
            {
                if (CommandButtonText)
                    CommandButtonText.text = selectedSkill.Name;
                if (ItemAOEText != null)
                    ItemAOEText.text = selectedSkill.Target.ToString().Substring(0, 1);
                if (CommandImage != null)
                {
                    string skillType = "";
                    if (selectedSkill.SkillType == SkillTypes.NormalOffense)
                        skillType = "Debuff";
                    else if (selectedSkill.SkillType == SkillTypes.NormalDefense)
                    {
                        if (selectedSkill.BuffsChance.Count > 0 && selectedSkill.BuffsChance[0].BuffEffect.Type == BuffType.HEAL)
                        {
                            skillType = "Heal";
                        }
                        else
                            skillType = "Buff";
                    }
                    CommandImage.sprite = LegrandBackend.Instance.GetSpriteData("Skill" + skillType);

                    if (SlotImage)
                        SlotImage.color = FilledSlotColor;
                }
            }
            else
                _SelectedCommandData.CommandType = (int)CommandTypes.None;
        }
        else if (_SelectedCommandData.CommandType == (int)CommandTypes.Item)
		{

            if (_SelectedCommandData.CommandValue >= 0)
			{
                if (selectedChar.Items[_SelectedCommandData.CommandValue] != null &&
                    selectedChar.Items[_SelectedCommandData.CommandValue].ItemMaster != null) //&&
                    //selectedChar.Items[_SelectedCommandData.CommandValue].Stack > 0)
				{
                    ItemSlot itemSlot = selectedChar.Items[_SelectedCommandData.CommandValue];

                    if (itemSlot.ItemMaster is Consumable)
                    {
                        if (CommandButtonText)
                            CommandButtonText.text = itemSlot.ItemMaster.Name;
                        if (AlternateInfoText != null)
                            AlternateInfoText.text = PartyManager.Instance.Inventory.GetStackItem(itemSlot.ItemMaster).ToString(); //itemSlot.Stack.ToString();
                        if (ItemAOEText != null)
                            ItemAOEText.text = ((Consumable)itemSlot.ItemMaster).Target.ToString().Substring(0, 1);
                        if (CommandImage != null)
                        {
                            CommandImage.sprite = itemSlot.ItemMaster.Icon;

                            if (SlotImage)
                                SlotImage.color = FilledSlotColor;
                        }
                    }
                    else
                    {
                        SetEmptySlot();
                    }
				} else
				{
                    SetEmptySlot();
				}
			} else
			{
                _SelectedCommandData.CommandType = (int)CommandTypes.None;
				if(CommandButtonText)
                    CommandButtonText.text = ((CommandTypes)_SelectedCommandData.CommandType).ToString();
				//CommandButtonText.text = GetTranslateJapan.CommandType ((CommandTypes)selectedCommandData.CommandType);
			}
		}

        if (_SelectedCommandData.CommandType == (int)CommandTypes.None)
		{
            SetEmptySlot();
		}
	}

    void SetEmptySlot()
    {
        if (CommandButtonText)
            CommandButtonText.text = CommandTypes.None.ToString();
        if (AlternateInfoText != null)
            AlternateInfoText.text = "---";
        if (ItemAOEText != null)
            ItemAOEText.text = "";
        if (CommandImage != null)
        {
            CommandImage.sprite = NoCommandSprite;

            if (SlotImage)
                SlotImage.color = EmptySlotColor;
        }
    }

	void ReturnMagic()
	{
		MainCharacter selectedChar = PartyManager.Instance.GetCharacter (PauseUIController.Instance.selectedCharId);
		CommandData selectedCommandData = selectedChar.GetCommand(panelPosition, slotPosition);
		
		if (selectedCommandData.CommandType != (int)CommandTypes.Magic)
			return;
		
		selectedCommandData.CommandType = (int)CommandTypes.None;
		SetCommand ();
	}

	void ReturnItem()
	{
		MainCharacter selectedChar = PartyManager.Instance.GetCharacter (PauseUIController.Instance.selectedCharId);
		CommandData selectedCommandData = selectedChar.GetCommand(panelPosition, slotPosition);

		if (selectedCommandData.CommandType != (int)CommandTypes.Item)
			return;

		int idItemSlot = selectedCommandData.CommandValue;

		Item itemData = selectedChar.Items [idItemSlot].ItemMaster;
		if (itemData == null || itemData.ItemType != Item.Type.Consumable)
			return;
		else {
			if(selectedChar.ReturnItem (idItemSlot) == 0){
				SetCommand();
				PanelInventoryController.Instance.UpdateInventory ();
//				TabPanelController.instance.panelCommand.GetComponent<PanelChildren>().ObjectChildren[1].GetComponent<PanelItemChildren>().panelItemListList.GetComponent<GenerateMagicItemCommandList>().UpdateListMagicItemCommand();
			}
		}
	}

	public void OnCancel()
	{
		if(otherPanel)
			ChangeInteractableObject.EnableAllButtonInParent(otherPanel);
		targetCancelButton.interactable = true;
		targetCancelButton.Select();
	}

    #region IUpdateSelectedHandler implementation

    public void OnUpdateSelected(BaseEventData eventData)
    {
    }

    #endregion

    #region ISelectHandler implementation

    public void OnSelect(BaseEventData eventData)
    {
        SendInspectEvent();
    }

    #endregion

    #region IDeselectHandler implementation

    public void OnDeselect(BaseEventData eventData)
    {
        if (_SelectedCommandData.CommandType == (int)CommandTypes.Item)
        {
            //			EventManager.Instance.TriggerEvent (new ComparingItemEvent(false));
            EventManager.Instance.TriggerEvent(new InspectItemEvent(null, false));
        }
    }

    #endregion
}
