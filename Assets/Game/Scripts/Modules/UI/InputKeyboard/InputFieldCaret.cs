﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class InputFieldCaret : MonoBehaviour {
	public InputField targetTextField;
	private int _caretPosition;
	public int caretPosition{
		//set{ _caretPosition = Mathf.Clamp (value, 0, targetTextField.text.Length);}
		get{return _caretPosition;}
	}
	public Image caretBlinkImage;
	private float caretBlinkImageLeftBorder;
	public bool isWordMode=true;

	HorizontalLayoutGroup[] rowsKeyboard;

	void OnEnable () {
		_caretPosition = targetTextField.text.Length;
		caretBlinkImageLeftBorder = targetTextField.textComponent.rectTransform.offsetMin.x;

		GameObject panelKeyboard = GetComponentInChildren<VerticalLayoutGroup>().gameObject;
		rowsKeyboard = panelKeyboard.GetComponentsInChildren<HorizontalLayoutGroup> (true);

		isWordMode=true;
		WordModeKeyboard ();
	}

	public void BackToDefaultKeyboard()
	{
		targetTextField.text = "";

		foreach (var rowsInput in rowsKeyboard) {
			rowsInput.gameObject.SetActive(true);
		}

		for(int i=5;i<8;i++)
		{
			foreach(var text in rowsKeyboard[i].GetComponentsInChildren<Text>())
			{
				text.text = text.text.ToLower();
			}
		}
	}

	// Update is called once per frame
	void Update () {
		PutCaretImage();
	}

	private void PutCaretImage()
	{
		Vector3 temp = caretBlinkImage.rectTransform.localPosition;
		temp.x = caretBlinkImageLeftBorder + (caretPosition * 22);
		caretBlinkImage.rectTransform.localPosition = temp;
	}

	public void MoveRight()
	{
		_caretPosition = Mathf.Clamp (++_caretPosition, 0, targetTextField.text.Length);
		Debug.Log ("move right " +_caretPosition);
	}
	public void MoveLeft()
	{
		_caretPosition = Mathf.Clamp (--_caretPosition, 0, targetTextField.text.Length);
		Debug.Log ("move left " +_caretPosition);
	}
	public void MoveToLast()
	{
		_caretPosition = targetTextField.text.Length;
	}
	public void MoveToFirst()
	{
		_caretPosition = 0;
	}

	public void CapslockOrShiftMode()
	{
		if (isWordMode) {
			//GameObject panelKeyboard = GetComponentInChildren<VerticalLayoutGroup>().gameObject;
			//HorizontalLayoutGroup[] rowsKeyboard = panelKeyboard.GetComponentsInChildren<HorizontalLayoutGroup> ();

			bool isUpperWord=false;
			string sampleWord = rowsKeyboard[5].GetComponentInChildren<Text>().text;
			if(sampleWord.Equals(sampleWord.ToUpper()))
				isUpperWord = true;

			for(int i=5;i<8;i++)
			{
				foreach(var text in rowsKeyboard[i].GetComponentsInChildren<Text>())
				{
					if(isUpperWord){
						text.text = text.text.ToLower();
						rowsKeyboard[0].gameObject.SetActive(false);	// rows symbol 1
						rowsKeyboard[4].gameObject.SetActive(true);		// rows number
					}
					else{
						text.text = text.text.ToUpper();
						rowsKeyboard[0].gameObject.SetActive(true);		// rows symbol 1
						rowsKeyboard[4].gameObject.SetActive(false);	// rows number
					}
				}
			}
		}
	}

	private void WordModeKeyboard()
	{
		//GameObject panelKeyboard = GetComponentInChildren<VerticalLayoutGroup>().gameObject;
		//HorizontalLayoutGroup[] rowsKeyboard = panelKeyboard.GetComponentsInChildren<HorizontalLayoutGroup> ();

		for (int i=0; i<8; i++) {
			if(i>=4)
				rowsKeyboard[i].gameObject.SetActive(true);
			else
				rowsKeyboard[i].gameObject.SetActive(false);
		}
	}

	private void SymbolModeKeyboard()
	{
		//GameObject panelKeyboard = GetComponentInChildren<VerticalLayoutGroup>().gameObject;
		//HorizontalLayoutGroup[] rowsKeyboard = panelKeyboard.GetComponentsInChildren<HorizontalLayoutGroup> ();
		
		for (int i=0; i<8; i++) {
			if(i<4)
				rowsKeyboard[i].gameObject.SetActive(true);
			else
				rowsKeyboard[i].gameObject.SetActive(false);
		}
	}

	public void SwitchKeyboardMode()
	{
		isWordMode = !isWordMode;
		//row 1-4 = number, word 3x   , row 5-8 = symbol row
		if (isWordMode) {
			WordModeKeyboard ();
		} else
			SymbolModeKeyboard ();
	}
}
