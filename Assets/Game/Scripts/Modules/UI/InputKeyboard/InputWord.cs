﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class InputWord : MonoBehaviour {
	private string word=string.Empty;
	private bool isUnique = false;
	public InputField targetTextField;
	public InputFieldCaret inputFieldCaret;

	// Use this for initialization
	void Start () {
		GetComponent<Button> ().onClick.AddListener (() => OnClickHandler ());
	}

	void Update()
	{

	}

	void OnClickHandler ()
	{
		if(!isUnique)
			word = GetComponentInChildren<Text> ().text;

		if (!string.IsNullOrEmpty (word) && targetTextField.text.Length < targetTextField.characterLimit) {
			targetTextField.text = targetTextField.text.Insert (inputFieldCaret.caretPosition, word);
			MoveRight ();
			inputFieldCaret.GetComponent<MagicPresetRenameData>().isUseDefaultName = false;
		}
	}

	public void DefineUniqueWord(string uWord)
	{
		isUnique = true;
		word = uWord;
	}

	public void BackSpace()
	{
		string text = targetTextField.text;
		if (!string.IsNullOrEmpty(text) && inputFieldCaret.caretPosition>0) {
			//text = text.Remove (text.Length - 1);
			text = text.Remove (inputFieldCaret.caretPosition-1,1);
			targetTextField.text = text;
		}
		MoveLeft ();
	}

	public void MoveRight()
	{
		inputFieldCaret.MoveRight();
	}
	public void MoveLeft()
	{
		inputFieldCaret.MoveLeft();
	}

	public void CapslockOrShift()
	{
		inputFieldCaret.CapslockOrShiftMode ();
	}

	public void SwitchInputMode()
	{
		inputFieldCaret.SwitchKeyboardMode ();
	}
}
