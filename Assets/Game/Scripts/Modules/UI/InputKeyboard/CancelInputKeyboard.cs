using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class CancelInputKeyboard : MonoBehaviour {
	private Button lastButton;
	public Button _lastButton{
		get{return lastButton;}
	}

	void OnEnable()
	{
		if (EventSystem.current.currentSelectedGameObject == null)
			return;
		lastButton = EventSystem.current.currentSelectedGameObject.GetComponent<Button> ();
	}

	public void OnCancel()
	{
		//GUIController.instance.ChangeState(GUIController.PauseUIControllerState.MagicPreset);

		GetComponent<InputFieldCaret> ().BackToDefaultKeyboard ();

		lastButton.Select ();

		gameObject.SetActive (false);
	}

	public void EnableChildrenButtonOf(GameObject obj)
	{
		ChangeInteractableObject.EnableAllButtonInParent (obj);
	}
}
