using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;
using SmartLocalization;

public class ShowEquipmentCharacter : MonoBehaviour {
    [System.Serializable]
    public class ObjectAttribute
    {
        public Attribute Attribute;
        public Text ValueText;
    }

	#region Equipment
    public Image EquipmentImage;
    public Text EquipmentName;
    public List<ObjectAttribute> ObjectAttributes;
	#endregion
    public bool IsHandleChangeChar;

    private MainCharacter _CurrChara;
    private Equipments _CurrEquip;

	void Awake()
	{
	}

	void OnDestroy()
	{
	}

	void Start () {
	}

	void OnEnable()
	{
        if (IsHandleChangeChar)
        {
            EventManager.Instance.AddListener<ChangeUICharSelectedID>(ChangeUICharEventHandler);
            UpdateEquipmentData(PauseUIController.Instance.selectedCharId);
        }
	}

    void OnDisable()
    {
        if(EventManager.Instance)
            EventManager.Instance.RemoveListener<ChangeUICharSelectedID>(ChangeUICharEventHandler);
    }

    private void ChangeUICharEventHandler(ChangeUICharSelectedID e)
    {
        UpdateEquipmentData(e.Id);
    }

    public void UpdateEquipmentData(string charId)
    {
        _CurrChara = PartyManager.Instance.GetCharacter(charId);
        ShowEquipmentData();
    }

	public void UpdateEquipmentData()
	{
        _CurrChara = PartyManager.Instance.GetCharacter(PauseUIController.Instance.selectedCharId);
        ShowEquipmentData();
	}

    void ShowEquipmentData()
    {
        _CurrEquip = _CurrChara.Equipment();
        ShowEquipmentInfo();
        ShowEquipmentAttribute();        
    }

	void ShowEquipmentInfo()
	{
        if (EquipmentImage)
            EquipmentImage.sprite = _CurrEquip.Picture;
        if (EquipmentName)
            EquipmentName.text = _CurrEquip.Name;
	}

	void HideEquipmentImage()
	{
	}

	void ShowEquipmentAttribute()
	{
        float value;
        foreach(ObjectAttribute ob in ObjectAttributes)
        {
            value = _CurrEquip.GetEquipmentAttribute(ob.Attribute);
            if (value == 0)
                ob.ValueText.gameObject.SetActive(false);
            else
            {
                ob.ValueText.gameObject.SetActive(true);
                if(value>0)
                    ob.ValueText.text = "+"+value.ToString();
                else
                    ob.ValueText.text = value.ToString();
            }
        }
	}
}