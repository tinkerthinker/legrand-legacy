﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TutorialSaveData
{
	public string Name;
	public bool IsCompleted;
}

[System.Serializable]
public class TutorialSteps
{
	public string Name;
    public string Title;
	public string TutorialPrefab;
	public bool IsCompleted;
	
	public TutorialSteps()
	{
		Name = "";
        Title = "";
        TutorialPrefab = "";
		IsCompleted = false;
	}
}