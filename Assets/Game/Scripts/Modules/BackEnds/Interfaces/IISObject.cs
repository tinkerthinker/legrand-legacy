﻿using UnityEngine;
using System.Collections;

public interface IISObject{
	string ISName { get; set;}
	string ISValue {get; set;}
	Sprite ISIcon { get; set;}
}
