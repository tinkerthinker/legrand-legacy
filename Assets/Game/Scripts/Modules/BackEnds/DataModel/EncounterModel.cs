﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;

[System.Serializable]
public class EncounterModel{
    public string ID;
    public string Name;
	public string Description;
	public int baseExp;
	public int baseLevel;
	public float HealthModifier;
	public bool IsFlying;
    public CharacterAttackType Class;
    public Element Element;
	public ProjectileType Projectile;

	public Weapon.DamageType Weapon;
	public Armor.ArmorType Armor;
	public EnemyType EnemyType;
	public string Attribute; 
//	public AttackTarget AttackTarget;
}