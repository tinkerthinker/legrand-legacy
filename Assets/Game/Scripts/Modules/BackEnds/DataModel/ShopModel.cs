﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class ShopModel {
	public string _ID;
	public List<string> itemID;

	public ShopModel()
	{
		_ID = "";
		itemID = new List<string>();
	}

}
