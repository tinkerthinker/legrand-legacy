﻿using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class EquipmentUpgradeModel{
	public string CharacterID;
	public float Value;
	public int Type;
	public int Index;
	public int UpgradeID;
	
	public EquipmentUpgradeModel()
	{
		CharacterID = "";
		Value = 0;
		Type = 0;
		Index = 0;
		UpgradeID = 0;
	}

	public EquipmentUpgradeModel(string characterId, float value, int type, int index, int enumIndex)
	{
		CharacterID = characterId;
		Value = value;
		Type = type;
		Index = index;
		UpgradeID = enumIndex;
	}
}
