﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class MerchantLevel
{
    public string ID;
    public int Level;

    public MerchantLevel()
    {
        ID = "";
        Level = 1;
    }
}
