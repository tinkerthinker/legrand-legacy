﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class ItemModel{
	public string ItemId;
	public int Stack;
	
	public ItemModel()
	{
		ItemId = "";
		Stack = 0;
	}

	public ItemModel(string id, int stack)
	{
		ItemId = id;
		Stack = stack;
	}
}
