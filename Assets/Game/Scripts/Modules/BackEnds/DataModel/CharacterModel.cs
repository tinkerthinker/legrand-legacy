﻿using UnityEngine;
using System.Collections;
using Legrand.core;

[System.Serializable]
public class CharacterModel{
    	public string ID;
	public int AttackType;
    	public string Name;
    public int Level;
    public int ATP;
	public int WeaponType;
	public int ArmorType;
	public float health;
	public int gauge;
	public int Experience;
	public int Model;
    public int ClassLevel;
    public string EquipmentID;
    public bool Active;

	public CharacterModel()
	{
		ID = "";
		AttackType = 0;
		Name = "";
        Level = 0;
        ATP = 0;
		WeaponType = 0;
		ArmorType = 0;
		health = 0;
		gauge = 0;
		Experience = 0;
		Model = 0;
        ClassLevel = 0;
        EquipmentID = "";
        Active = true;
    }

	public CharacterModel(string newID,string newName, int level, int atp, int newAttackType, int newWeaponType, int newArmorType, float newhealth, int newgauge, int experience, int model, int classLevel, string equipmentID, bool active)
	{
		ID = newID;
		AttackType = newAttackType;
		Name = newName;
        Level = level;
        ATP = atp;
		WeaponType = newWeaponType;
		ArmorType = newArmorType;
		health = newhealth;
		gauge = newgauge;
		Experience = experience;
		Model = model;
        ClassLevel = classLevel;
        EquipmentID = equipmentID;
        Active = active;
    }
}
