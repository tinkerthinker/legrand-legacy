﻿using UnityEngine;
using System.Collections;
using Legrand.core;

[System.Serializable]
public class BattleGrowthData{
	public int id;
	public string chraracterId;
	public int attributeType;
	public float value;
	
	public BattleGrowthData()
	{
		id = 0;
		chraracterId = "";
		attributeType = 0;
		value = 0;
	}
}

