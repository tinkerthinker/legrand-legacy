﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class EncounterLootModel{
	public int ID;
	public string EncounterID;
	public string ItemID;
	public int Amount;
	public int Chance;

	public EncounterLootModel()
	{
		ID = 0;
		EncounterID = "";
		ItemID = "";
		Amount = 0;
		Chance = 0;
	}
}
