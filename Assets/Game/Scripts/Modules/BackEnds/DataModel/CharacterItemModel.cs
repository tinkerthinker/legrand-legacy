using UnityEngine;
using System.Collections;

[System.Serializable]
public class CharacterItemModel{
	public string CharacterId;
	public int Index;
	public string ItemId;

	public CharacterItemModel()
	{
		CharacterId = "";
		Index = 0;
		ItemId = "";
	}
	
	public CharacterItemModel(string characterId, int index, string itemId)
	{
		CharacterId = characterId;
		Index = index;
		ItemId = itemId;
	}
}
