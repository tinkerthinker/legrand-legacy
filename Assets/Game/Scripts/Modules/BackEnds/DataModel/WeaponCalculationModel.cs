﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;

[System.Serializable]
public class WeaponCalculationModel{
	public Weapon.DamageType DamageType;
	public List<Buff> WeaponBuff;

	public WeaponCalculationModel()
	{
		DamageType = Weapon.DamageType.Slash;
		WeaponBuff = new List<Buff>();
	}
}
