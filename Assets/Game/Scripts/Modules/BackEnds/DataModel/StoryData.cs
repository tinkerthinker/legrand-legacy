﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class StoryData
{
    public string ID;
	public QuestWorldEvents Scene;
    public List<QuestWorldEvents> SceneActiveAfterTrigger;

	public StoryData()
	{
        ID = "";
		Scene = new QuestWorldEvents();
		SceneActiveAfterTrigger = new List<QuestWorldEvents>();
	}
}
