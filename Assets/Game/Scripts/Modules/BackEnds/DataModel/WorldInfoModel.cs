﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class WorldInfoModel
{
    public List<string> TakenTreasures;
    public List<CityPointInfo> UnlockedCityPoints;
    public List<WorldMapPointInfo> UnlockedWorldMapPoint;
    public List<SubAreaInfo> DynamicSubArea;
    public List<Fact> LatestFacts;
    public List<Fact> MonsterKilledFacts;
    public List<FastTravelAttribute> UnlockedFastTravelPoints;

    public void Copy(WorldInfoModel newWorldInfo)
    {
        TakenTreasures = new List<string>(newWorldInfo.TakenTreasures);
        UnlockedCityPoints = new List<CityPointInfo>(newWorldInfo.UnlockedCityPoints);
        DynamicSubArea = new List<SubAreaInfo>(newWorldInfo.DynamicSubArea);
        LatestFacts = new List<Fact>(newWorldInfo.LatestFacts);
        MonsterKilledFacts = new List<Fact>(newWorldInfo.MonsterKilledFacts);
        UnlockedFastTravelPoints = new List<FastTravelAttribute>(newWorldInfo.UnlockedFastTravelPoints);
        UnlockedWorldMapPoint = new List<WorldMapPointInfo>(newWorldInfo.UnlockedWorldMapPoint != null ? newWorldInfo.UnlockedWorldMapPoint : new List<WorldMapPointInfo>());
    }

    public void NewFact(string key, double value, string owner, List<string> activeGO, List<string> inactiveGO)
    {
        if (LatestFacts == null) LatestFacts = new List<Fact>();
        Fact findFact = LatestFacts.Find(fact => LegrandUtility.CompareString(fact.Name, key));
        if (findFact != null)
        {
            findFact.Value = value;
            findFact.ActiveGameObjects = activeGO;
            findFact.InactiveGameObjects = inactiveGO;
        }
        else
        {
            Fact newFact = new Fact(key, value, owner, activeGO, inactiveGO);
            LatestFacts.Add(newFact);
        }

        //SubAreaFact subAreaFact = LatestFacts.Find(item => LegrandUtility.CompareString(item.subAreaName, AreaController.Instance.CurrSubAreaData.prefabName));
        //if(subAreaFact != null)
        //{
        //    Fact thisFact = subAreaFact.fact.Find(item => item.Name == key);
        //    if(subAreaFact.fact.Contains(thisFact))
        //        subAreaFact.fact[subAreaFact.fact.IndexOf(thisFact)].Value = value;
        //    else
        //        subAreaFact.fact.Add(new Fact(key, value, activeGO, inactiveGO));
        //}
        //else
        //{
        //    SubAreaFact newSubAreaFact = new SubAreaFact();
        //    newSubAreaFact.subAreaName = AreaController.Instance.CurrSubAreaData.prefabName;
        //    newSubAreaFact.fact.Add(new Fact(key, value, activeGO, inactiveGO));

        //    LatestFacts.Add(newSubAreaFact);
        //}
    }

    public void NewFact(Fact newFact)
    {
        NewFact(newFact.Name, newFact.Value,newFact.Owner,newFact.ActiveGameObjects,newFact.InactiveGameObjects);
    }

    public void NewFact(string key, double value)
    {
        NewFact(key, value, null, null, null);
    }

    public Fact GetFact(string key)
    {
        return LatestFacts.Find(fact => LegrandUtility.CompareString(fact.Name, key));
    }

    public bool RemoveFact(string key)
    {
        /* Return is success to remove or not*/
        Fact fact = GetFact(key);
        if (fact == null) return false;
        
        return LatestFacts.Remove(fact);
    }
    
    public void UpdateMonsterKilled(string monsterID, double value)
    {
        string key = "Have Killed " + monsterID;
        Fact findFact = MonsterKilledFacts.Find(x => x.Name == key);
        if (findFact != null)
        {
            findFact.Value = value;
        }
        else
        {
            Fact newFact = new Fact(key, value, string.Empty, null, null);
            MonsterKilledFacts.Add(newFact);
        }
    }

    public double GetMonsterKillCount(string monsterID)
    {
        string key = "Have Killed " + monsterID;
        Fact findFact = MonsterKilledFacts.Find(x => x.Name == key);
        if (findFact != null)
        {
            return findFact.Value;
        }
        return 0;
    }

    public bool ExistMonsterKilled(string monsterID)
    {
        string key = "Have Killed " + monsterID;
        return MonsterKilledFacts.Exists(x => x.Name == key);
    }
}