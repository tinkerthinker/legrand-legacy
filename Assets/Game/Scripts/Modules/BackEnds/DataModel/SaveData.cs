using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;

[System.Serializable]
public class SaveData
{
    public List<CharacterModel> MainCharacters;
    public List<BattleAttribute> MainCharacterBattleAttributes;

    public List<ElementModel> MainCharacterElements;
    public List<CommandData> MainCharacterCommands;
    public List<CharacterItemModel> MainCharacterItems;

    public List<BattleFormationModel> BattleFormations;
    public List<BattleFormationModel> StoredFormations;

    public List<ItemModel> InventoryItems;
    public List<ItemModel> StorageItems;
    
    public StoryProgression StoryProgressions;

    public WorldState State;
    public int TotalEXP;
    public WorldInfoModel WorldInfo;
    public List<SavedQuest> QuestProgress;
    public List<SavedTask> TaskProgress;
	public List<string> TutorialCompleted;

    

    public SaveData()
    {
        MainCharacters = new List<CharacterModel>();
        MainCharacterBattleAttributes = new List<BattleAttribute>();

        MainCharacterElements = new List<ElementModel>();
        MainCharacterCommands = new List<CommandData>();
        MainCharacterItems = new List<CharacterItemModel>();

        BattleFormations = new List<BattleFormationModel>();
        StoredFormations = new List<BattleFormationModel>();

        InventoryItems = new List<ItemModel>();
        StorageItems = new List<ItemModel>();

        State = new WorldState("", "", "", 0, 0, 0);
        TotalEXP = 0;
        WorldInfo = new WorldInfoModel();
        TaskProgress = new List<SavedTask>();
        QuestProgress = new List<SavedQuest>();
        TutorialCompleted = new List<string>();
    }

	public void Clear()
	{
		MainCharacters = new List<CharacterModel>();
		MainCharacterBattleAttributes = new List<BattleAttribute>();
		
		MainCharacterElements = new List<ElementModel>();
		MainCharacterCommands = new List<CommandData>();

		BattleFormations = new List<BattleFormationModel>();
        StoredFormations = new List<BattleFormationModel>();

        InventoryItems = new List<ItemModel>();
        StorageItems = new List<ItemModel>();

		MainCharacterItems = new List<CharacterItemModel>();

		StoryProgressions= new StoryProgression();
		
		TotalEXP = 0;

		WorldInfo.DynamicSubArea = new List<SubAreaInfo>();
		WorldInfo.TakenTreasures = new List<string>();
		WorldInfo.UnlockedCityPoints = new List<CityPointInfo>();
        WorldInfo.LatestFacts = new List<Fact>();

		QuestProgress = new List<SavedQuest>();
		TutorialCompleted = new List<string> ();
	}
}
