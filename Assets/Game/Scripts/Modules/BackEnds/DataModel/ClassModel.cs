﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;

[System.Serializable]
public class BattleClassModel
{
    public int ID;
    public string CharacterID;
    public int BattleClass;
    
    public BattleClassModel()
    {
   	 ID = 0;
   	 CharacterID = "";
   	 BattleClass = 0;
    }
	public BattleClassModel(string characterID, int battleClass)
	{
		ID = -1;
		CharacterID = characterID;
		BattleClass = battleClass;
	}
}
[System.Serializable]
public class EncounterClassModel{
    public int ID;
    public int CharacterID;
    public int EncounterClass;
    [HideInInspector]
    public int EncounterClassData;
}
