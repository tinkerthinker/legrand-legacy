﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class ElementModel{
    public int Id;
    public string CharacterId;
    public int Element;
    
    public ElementModel()
    {
    	Id = 0;
    	CharacterId = "";
    	Element = 0;
    }
	public ElementModel(string characterID, int element)
	{
		Id = -1;
		CharacterId = characterID;
		Element = element;
	}
}
