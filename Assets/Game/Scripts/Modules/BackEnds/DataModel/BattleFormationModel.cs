﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class BattleFormationModel{
    public int ID;
    public string CharacterID;
    public float x;
	public float y;
	
	public BattleFormationModel()
	{
		ID= 0;
		CharacterID = "";
		x = 0;
		y = 0;
	}

	public BattleFormationModel(string characterId, Vector2 position)
	{
		ID = -1;
		CharacterID = characterId;
		x = position.x;
		y = position.y;
	}
}
