﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;

[System.Serializable]
public class MonsterDefeated{
	public string ID;
	public int Count;

    public MonsterDefeated()
    {
        ID = "";
        Count = 0;
    }

	public MonsterDefeated(string id, int count)
	{
		this.ID = id;
		this.Count = count;
	}
}