﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class MagicPresetModel{
    public int MagicPresetID;
    public string CharacterId;
    public string Name;
    public string Description;
    public int Element;
    public int TargetType;
    public float MagicDamageModifier;
    public float MagicInterruptChance;

	public string CastingMagicEffect;
	public string AttackMagicEffect;
	public string HitEffect;
    public bool IsNameDefault;

	public MagicPresetModel()
	{
		MagicPresetID = 0;
		CharacterId = "";
		Name = "";
		Description = "";
		Element = 0;
		TargetType = 0;
		MagicDamageModifier = 100f;
        MagicInterruptChance = 65f;
		CastingMagicEffect = "";
		AttackMagicEffect = "";
		HitEffect = "";
		IsNameDefault = false;
	}

	public MagicPresetModel(int magicPresetId, string characterId, string name, string description, int element, int targetType, float damageModifier, float interuptChance, string castingEffect, string attackEffect, string hitEffect, bool isNameDefault)
	{
		MagicPresetID = magicPresetId;
		CharacterId = characterId;
		Name = name;
		Description = description;
		Element = element;
		TargetType = targetType;
		MagicDamageModifier = damageModifier;
		MagicInterruptChance = interuptChance;

		CastingMagicEffect = castingEffect;
		AttackMagicEffect = attackEffect;
		HitEffect = hitEffect;

		IsNameDefault = isNameDefault;
	}
}



