﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;

[System.Serializable]
public class ArmorCalculationModel{
	public Armor.ArmorType ArmorType;
	public List<Buff> ArmorBuff;

	public ArmorCalculationModel()
	{
		ArmorType = Armor.ArmorType.Light;
		ArmorBuff = new List<Buff>();
	}
}
