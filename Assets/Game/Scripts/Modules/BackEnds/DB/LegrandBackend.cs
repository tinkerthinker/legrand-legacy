using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using Legrand.database;
using Legrand.GameSettings;

public class LegrandBackend : MonoBehaviour
{
    #region Singleton Handling

    private static LegrandBackend _Instance;

    public static LegrandBackend Instance
    {
        get
        {
            if (_Instance == null)
            {
                _Instance = GameObject.FindObjectOfType<LegrandBackend>();
            }
            return _Instance;
        }
    }

    #endregion

    [SerializeField]
    private SaveData
        SaveModel;
    private static byte[] savedKey = ASCIIEncoding.UTF8.GetBytes("hNN2Umur68ODNQWNpplzsKQbJymEpqzK");
    private static byte[] savedIV = ASCIIEncoding.UTF8.GetBytes("1Q1zoGKsY0qK9mOa");

    public static string SavePath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments) + "/Semisoft/Legrand Legacy";
    public bool TestingScene = false;
    #region Getter Setter

    public List<CharacterModel> MainCharacters
    {
        get { return SaveModel.MainCharacters; }
        set { SaveModel.MainCharacters = value; }
    }

    public List<BattleAttribute> MainCharacterBattleAttributes
    {
        get { return SaveModel.MainCharacterBattleAttributes; }
        set { SaveModel.MainCharacterBattleAttributes = value; }
    }
    
    public List<ElementModel> MainCharacterElements
    {
        get { return SaveModel.MainCharacterElements; }
        set { SaveModel.MainCharacterElements = value; }
    }

    public List<CommandData> MainCharacterCommands
    {
        get { return SaveModel.MainCharacterCommands; }
        set { SaveModel.MainCharacterCommands = value; }
    }

    public List<CharacterItemModel> MainCharacterItems
    {
        get { return SaveModel.MainCharacterItems; }
        set { SaveModel.MainCharacterItems = value; }
    }

    public List<BattleFormationModel> BattleFormations
    {
        get { return SaveModel.BattleFormations; }
        set { SaveModel.BattleFormations = value; }
    }

    public List<BattleFormationModel> StoredFormations
    {
        get { return SaveModel.StoredFormations; }
        set { SaveModel.StoredFormations = value; }
    }

    public List<ItemModel> InventoryItems
    {
        get { return SaveModel.InventoryItems; }
        set { SaveModel.InventoryItems = value; }
    }

    public List<ItemModel> StorageItems
    {
        get { return SaveModel.StorageItems; }
        set { SaveModel.StorageItems = value; }
    }

    public StoryProgression StoryProgressions
    {
        get { return SaveModel.StoryProgressions; }
        set { SaveModel.StoryProgressions = value; }
    }

    public WorldState State
    {
        get { return SaveModel.State; }
        set { SaveModel.State = value; }
    }

    public WorldInfoModel WorldInfo
    {
        get { return SaveModel.WorldInfo; }
        set { SaveModel.WorldInfo = value; }
    }

    public int TotalEXP
    {
        get { return SaveModel.TotalEXP; }
        set { SaveModel.TotalEXP = value; }
    }

	public List<SavedQuest> QuestProgression
    {
        get { return SaveModel.QuestProgress; }
        set { SaveModel.QuestProgress = value; }
    }

    public List<SavedTask> TaskProgression
    {
        get { return SaveModel.TaskProgress; }
        set { SaveModel.TaskProgress = value; }
    }

	public List<string> TutorialCompleted
	{
		get { return SaveModel.TutorialCompleted; }
		set { SaveModel.TutorialCompleted = value; }
	}

  
    public Dictionary<string, Item> ItemData
    {
        get { return BackEndDictionary.ItemData; }
    }

	public Dictionary<string, QuestWorldEvents> WorldEventData
	{
		get{ return BackEndDictionary.WorldEventData;}
	}
    
    public Dictionary<int, Buff> BuffDictionary
    {
        get { return BackEndDictionary.BuffDictionary; }
    }

    public BluePrint GetBlueprint(string ID)
    {
        if(BackEndDictionary.BlueprintDictionary.ContainsKey(ID))
        {
            return BackEndDictionary.BlueprintDictionary[ID];
        }
        return null;
    }

    public BluePrint GetBlueprintByResultItem(string ID)
    {
        foreach (KeyValuePair<string, BluePrint> bd in BackEndDictionary.BlueprintDictionary)
        {
            if(bd.Value.ResultItemID.Equals(ID))
            {
                return bd.Value;
            }
        }
        return null;
    }

    public Sprite GetAreaSprite(string areaName)
    {
        if (BackEndDictionary.MapAreaSpriteDictionary.ContainsKey(areaName))
        {
            return BackEndDictionary.MapAreaSpriteDictionary[areaName];
        }
        return null;
    }

    public Magic GetMagic(int id)
    {
        if (BackEndDictionary.MagicDictionary.ContainsKey(id))
        {
            return BackEndDictionary.MagicDictionary[id];
        }
        return null;
    }

    public Database LegrandDatabase;

    private float _InitialTime;
    public float InitialTime
    {
        get { return _InitialTime; }
    }

    private float _WorldSceneStartTime;
    public float WorldSceneStartTime
    {
        get { return _WorldSceneStartTime; }
        set { _WorldSceneStartTime = value; }
    }

	public Dictionary<string,int> listIdTutorial = new Dictionary<string, int>();
	private List<TutorialSteps> _listTutorialSteps = new List<TutorialSteps>();
	public List<TutorialSteps> ListTutorialSteps
	{
		get{return _listTutorialSteps;}
	}

    public Sprite GetSpriteData(string key)
    {
        if (BackEndDictionary.SpriteData.ContainsKey(key))
        {
            return BackEndDictionary.SpriteData[key];
        }
        return null;
    }
    
    public Vector2KeycodeString[] Keys;

    public CharacterLevel GetCharacterLevel(string CharacterID)
    {
        if (BackEndDictionary.CharacterLevel.ContainsKey(CharacterID))
            return BackEndDictionary.CharacterLevel[CharacterID];
        else
            return null;
    }

    public string GetKeyName(string KeyCode)
    {
        if (BackEndDictionary.KeyCode.ContainsKey(KeyCode))
        {
            return BackEndDictionary.KeyCode[KeyCode];
        }
        return KeyCode;
    }

    public string GetLanguageData(string id, bool returnIdIfNotFound = false)
    {
        if (BackEndDictionary.Language != null && BackEndDictionary.Language.ContainsKey(id))
        {
            return BackEndDictionary.Language[id];
        }
        if (returnIdIfNotFound)
            return id;
        else
            return "";
    }

    public int GetLevelATP(int level)
    {
        if (BackEndDictionary.ATPDictionary.ContainsKey(level)) return BackEndDictionary.ATPDictionary[level];
        return 0;
    }
    public EquipmentDataModel GetEquipment(string ID)
    {
        if (BackEndDictionary.EquipmentsDictionary.ContainsKey(ID)) return BackEndDictionary.EquipmentsDictionary[ID];
        return null;
    }

    public OffensiveBattleSkill GetOffensiveSkill(string id)
    {
        return BackEndDictionary.OffensiveSkill[id];
    }
    public DefensiveBattleSkill GetDefensiveSkill(string id)
    {
        return BackEndDictionary.DefensiveSkill[id];
    }
    public NormalSkill GetNormalSkill(string id)
    {
        return BackEndDictionary.NormalSkill[id];
    }
    public GuardBuff GetGuardBuffSkill(string id)
    {
        return BackEndDictionary.GuardSkill[id];
    }
    public float GetConstantValueFormula(string name)
    {
        if (!BackEndDictionary.ConstantPercentageValueDictionary.ContainsKey(name))
        {
            Debug.Log("constant value named : " + name + " not exist");
            return 0f;
        }
        else
            return BackEndDictionary.ConstantPercentageValueDictionary[name];
    }

    public int GetMerchantLevel(string ID)
    {
        if (BackEndDictionary.MerchantLevelDictionary.ContainsKey(ID))
            return BackEndDictionary.MerchantLevelDictionary[ID].Level;
        return -1;
    }

    #endregion

    private BackendDictionary BackEndDictionary;

    void Awake()
    {
        Object.DontDestroyOnLoad(gameObject);
        BackEndDictionary = new BackendDictionary(LegrandDatabase);
        
        #region tutorial
        for (int t = 0; t < LegrandDatabase.TutorialData.Count; t++)
        {
            listIdTutorial.Add(LegrandDatabase.TutorialData.Get(t).Name, t);
            TutorialSteps tutorial = new TutorialSteps();
            tutorial.Name = LegrandDatabase.TutorialData.Get(t).Name;
            tutorial.Title = LegrandDatabase.TutorialData.Get(t).Title;
            tutorial.IsCompleted = LegrandDatabase.TutorialData.Get(t).IsCompleted;
            tutorial.TutorialPrefab = LegrandDatabase.TutorialData.Get(t).TutorialPrefab;
            _listTutorialSteps.Add(tutorial);
        }
        #endregion
    }

    public void StartNewWorldScene()
    {
        _InitialTime = State.TimeElapsed;
        if(!TestingScene)
            EventManager.Instance.RemoveAll();                      // Reset all event manager\
        GameplayInputManager.Instance.AddChangeControllerListener();
        StartCoroutine(PartyManager.Instance.NewGameInitiation());
    }

    public void StartLoadWorldScene()
    {
        _InitialTime = State.TimeElapsed;
        EventManager.Instance.RemoveAll();                      // Reset all event manager\
        GameplayInputManager.Instance.AddChangeControllerListener();
        StartCoroutine(PartyManager.Instance.DeserializeFromSave());
    }

    public IEnumerator NewGame()
    {
        PartyManager.Instance._ReadyToPlay = false;
		SaveModel.Clear();
        yield return null;
        StartNewWorldScene();

        QuestProgression = new List<SavedQuest>();
    }

    public bool IsSaveGameDataExist(string saveName)
    {
        if (File.Exists(SavePath + "/" + saveName + "/Save.gd"))
        {
            return true;
        }
        else
            return false;
    }

    public bool GetSaveGameData(string saveName, out WorldState state, out List<CharacterModel> characters)
    {
        BinaryFormatter bf = new BinaryFormatter();

        state = null;
        characters = null;

        if (File.Exists(SavePath + "/" + saveName + "/Save.gd"))
        {
            SaveData save;

            using (FileStream stream = new FileStream(SavePath + "/" + saveName + "/Save.gd", FileMode.Open))
            {
                try
                {
                    save =  bf.Deserialize(stream) as SaveData;
                }
                catch (System.Exception e)
                {
                    Debug.LogError(e + "Exception caught in encoding");
                    return false;
                }
            }


            //using (Stream inner = File.Open(SavePath + "/" + saveName + "/Save.gd", FileMode.Open))
            //{
            //    using (RijndaelManaged rijndael = new RijndaelManaged())
            //    {
            //        using (ICryptoTransform rdTransform = rijndael.CreateDecryptor((byte[])savedKey.Clone(), (byte[])savedIV.Clone()))
            //        {
            //            using (CryptoStream crypto = new CryptoStream(inner, rdTransform, CryptoStreamMode.Read))
            //            {
            //                try
            //                {
            //                    save = (SaveData)bf.Deserialize(crypto);
            //                    inner.Close();
            //                    crypto.Close();
            //                }
            //                catch (System.Exception e)
            //                {
            //                    inner.Close();
            //                    crypto.Close();
            //                    Debug.LogError(e + "Exception caught in encoding");
            //                    return false;
            //                }
            //            }
            //        }
            //    }
            //}

            state = save.State;
            characters = save.MainCharacters;
            return true;
        }
        else
            return false;
    }

	public TutorialSteps GetTutorialSteps(string nameTutorial)
	{
		int idSteps = listIdTutorial[nameTutorial];
		idSteps = (idSteps > LegrandDatabase.TutorialData.Count)? 0: idSteps;
		return ListTutorialSteps[idSteps];
	}

    #region LoadGame

    public bool LoadGame(string saveName)
    {
        if (UpdateDataFromFile(saveName))
        {
            return true;
        }
        else
            return false;
    }

    public bool UpdateDataFromFile(string FileLocation)
    {
        BinaryFormatter bf = new BinaryFormatter();

        if (File.Exists(SavePath + "/" + FileLocation + "/Save.gd"))
        {
            using (FileStream stream = new FileStream(SavePath + "/" + FileLocation + "/Save.gd", FileMode.Open))
            {
                try
                {
                    SaveModel = bf.Deserialize(stream) as SaveData;
                }
                catch (System.Exception e)
                {
                    Debug.LogError(e + "Exception caught in encoding");
                    return false;
                }
            }
        }
        else
            return false;

        return true;
    }

    #endregion

    #region SaveGame

    public bool SaveGame(string SaveName)
    {
		SaveModel.Clear();
        GetGameCurrentData();

            BinaryFormatter bf = new BinaryFormatter();
            System.IO.Directory.CreateDirectory(SavePath + "/" + SaveName);

            using (FileStream stream = new FileStream(SavePath + "/" + SaveName + "/Save.gd", FileMode.Create))
            {
                try
                {
                    bf.Serialize(stream, SaveModel);
                }
                catch (System.Exception)
                {
                    return false;
                }
            }

            //using (Stream inner = File.Create(SavePath + "/" + SaveName + "/Save.gd"))
            //{
            //    using (RijndaelManaged rijndael = new RijndaelManaged())
            //    {
            //        using (ICryptoTransform rdTransform = rijndael.CreateEncryptor((byte[])savedKey.Clone(), (byte[])savedIV.Clone()))
            //        {
            //            using (CryptoStream crypto = new CryptoStream(inner, rdTransform, CryptoStreamMode.Write))
            //            {
            //                bf.Serialize(crypto, SaveModel);
            //                inner.Close();
            //                crypto.Close();
            //            }
            //        }
            //    }
            //}

            WwiseManager.Instance.PlaySFX(AK.EVENTS.CONFIRM);

            return true;
    }

    #region Save Setting
    public bool SaveController(string FolderName, string FileName, ControllerSaveClass Data)
    {
        try
        {
            BinaryFormatter bf = new BinaryFormatter();
            System.IO.Directory.CreateDirectory(SavePath + "/" + FolderName);

            using (Stream inner = File.Create(SavePath + "/" + FolderName + "/" + FileName))
            {
                bf.Serialize(inner, Data);
            }

            WwiseManager.Instance.PlaySFX(AK.EVENTS.CONFIRM);

            return true;
        }
        catch (IOException e)
        {
            return false;
        }
    }
    public bool SaveSetting(string FolderName,string FileName, string Data)
    {
        try
        {
            BinaryFormatter bf = new BinaryFormatter();
            System.IO.Directory.CreateDirectory(SavePath + "/" + FolderName);

            using (Stream inner = File.Create(SavePath + "/" + FolderName + "/" + FileName))
            {
                bf.Serialize(inner, Data);
            }

            WwiseManager.Instance.PlaySFX(AK.EVENTS.CONFIRM);

            return true;
        }
        catch (IOException e)
        {
            return false;
        }
    }
    #endregion

    #region Load Setting
    public string LoadSetting(string FolderName,string FileName)
    {
        BinaryFormatter bf = new BinaryFormatter();

        string Data = "";

        if (File.Exists(SavePath + "/" + FolderName + "/" + FileName))
        {
            using (Stream inner = File.Open(SavePath + "/" + FolderName + "/" + FileName, FileMode.Open))
            {
                try
                {
                    Data = (string)bf.Deserialize(inner);
                }
                catch (System.Exception e)
                {
                    Debug.LogError(e + "Exception caught in encoding");
                    return "";
                }
            }
        }
        else
            return "";

        return Data;
    }
    public ControllerSaveClass LoadController(string FolderName, string FileName)
    {
        BinaryFormatter bf = new BinaryFormatter();

        ControllerSaveClass Data = new ControllerSaveClass();

        if (File.Exists(SavePath + "/" + FolderName + "/" + FileName))
        {
            using (Stream inner = File.Open(SavePath + "/" + FolderName + "/" + FileName, FileMode.Open))
            {
                try
                {
                    Data = (ControllerSaveClass)bf.Deserialize(inner);
                }
                catch(System.Exception e)
                {
                    Debug.LogError(e + "Exception caught in encoding");
                    return null;
                }
            }
        }
        else
            return null;

        return Data;
    }
    #endregion

    public void GetGameCurrentData()
    // Ambil data-data dari current game
    {
        PartyManager partyManager = PartyManager.Instance;
        ModelChanger modelChanger = null;
        foreach (MainCharacter MC in partyManager.CharacterParty)
        {
            // Character Model
            int model = 0;
            modelChanger = LegrandUtility.GetComponentInChildren<ModelChanger>(MC.gameObject);
            if (modelChanger != null) model = modelChanger.CurrentModelSet;
            MainCharacters.Add(new CharacterModel(MC._ID, MC._Name, MC.Level, MC.AttributePoint, (int)MC.CharacterAttackType, (int)MC.BaseWeapon.Type, (int)MC.BaseArmor.Type, MC.Health.Value, MC.Gauge, MC.CurrentEXP, model, MC.ClassLevel, MC.Equipment().ID, true));

            foreach (KeyValuePair<Attribute, object> baseAttribute in MC.BaseBattleAttributes)
            {
                MainCharacterBattleAttributes.Add(new BattleAttribute(MC._ID, (int)baseAttribute.Key, (int)baseAttribute.Value, (int)MC.BattleExperiences[baseAttribute.Key], ""));
            }
			
            foreach (Element element in MC.CharacterElements.Elements)
            {
                MainCharacterElements.Add(new ElementModel(MC._ID, (int)element));
            }

            foreach (CommandData command in MC.CommandPresets)
            {
                MainCharacterCommands.Add(command);
            }

            // Ambil data item dari karakter
            for (int index = 0; index < MC.MaxItem; index++)
            {
                if (MC.Items[index] != null && MC.Items[index].ItemMaster != null && MC.Items[index].Stack > 0)
                    MainCharacterItems.Add(new CharacterItemModel(MC._ID, index, MC.Items[index].ItemMaster._ID));
            }
        }

        foreach (MainCharacter MC in partyManager.NonActiveParty)
        {
            // Character Model
            int model = 0;
            modelChanger = LegrandUtility.GetComponentInChildren<ModelChanger>(MC.gameObject);
            if (modelChanger != null) model = modelChanger.CurrentModelSet;
            MainCharacters.Add(new CharacterModel(MC._ID, MC._Name, MC.Level, MC.AttributePoint, (int)MC.CharacterAttackType, (int)MC.BaseWeapon.Type, (int)MC.BaseArmor.Type, MC.Health.Value, MC.Gauge, MC.CurrentEXP, model, MC.ClassLevel, MC.Equipment().ID, false));

            foreach (KeyValuePair<Attribute, object> baseAttribute in MC.BaseBattleAttributes)
            {
                MainCharacterBattleAttributes.Add(new BattleAttribute(MC._ID, (int)baseAttribute.Key, (int)baseAttribute.Value, (int)MC.BattleExperiences[baseAttribute.Key], ""));
            }

            foreach (Element element in MC.CharacterElements.Elements)
            {
                MainCharacterElements.Add(new ElementModel(MC._ID, (int)element));
            }

            foreach (CommandData command in MC.CommandPresets)
            {
                MainCharacterCommands.Add(command);
            }

            // Ambil data item dari karakter
            for (int index = 0; index < MC.MaxItem; index++)
            {
                if (MC.Items[index] != null && MC.Items[index].ItemMaster != null && MC.Items[index].Stack > 0)
                    MainCharacterItems.Add(new CharacterItemModel(MC._ID, index, MC.Items[index].ItemMaster._ID));
            }
        }

        foreach (BattleFormation formation in partyManager.BattleFormations.Formations)
        {
            BattleFormations.Add(new BattleFormationModel(formation.Character._ID, formation.Position));
        }

        if (partyManager.StoredBattleFormation != null)
        {
            foreach (string vectorData in partyManager.StoredBattleFormation)
            {
                string[] formationData = vectorData.Split('_');
                StoredFormations.Add(new BattleFormationModel(formationData[0], new Vector2(float.Parse(formationData[1]), float.Parse(formationData[2]))));
            }
        }

        for (int i = 0; i < partyManager.Inventory.Items.Length; i++)
        {
            foreach (ItemSlot item in partyManager.Inventory.Items[i])
            {
                InventoryItems.Add(new ItemModel(item.ItemMaster._ID, item.Stack));
            }
        }

        for (int i = 0; i < partyManager.Storage.Items.Length; i++)
        {
            foreach (ItemSlot item in partyManager.Storage.Items[i])
            {
                StorageItems.Add(new ItemModel(item.ItemMaster._ID, item.Stack));
            }
        }

		foreach (KeyValuePair<string,bool> tut in partyManager.TutorialProgress) {
			if(tut.Value)
			{
				if(!TutorialCompleted.Contains(tut.Key))
					TutorialCompleted.Add(tut.Key);
			}
		}

        TotalEXP = partyManager.TotalEXP;

        StoryProgressions.Copy(partyManager.StoryProgression);

        State = new WorldState(partyManager.State.gameArea, partyManager.State.gameSubArea, partyManager.State.gamePortal, partyManager.State.currChapter, partyManager.State.currPhase, partyManager.State.currGold);
        State.currGold = partyManager.Inventory.Gold;
        Vector3 Pos = AreaController.Instance.CurrPlayer.transform.position;
        Quaternion Rot = AreaController.Instance.CurrPlayer.transform.rotation;
        State.PosX = Pos.x;
        State.PosY = Pos.y;
        State.PosZ = Pos.z;

        State.RotX = Rot.x;
        State.RotY = Rot.y;
        State.RotZ = Rot.z;
        State.RotW = Rot.w;
        State.TimeElapsed = _InitialTime + Time.realtimeSinceStartup - _WorldSceneStartTime;
        List<SavedQuest> savedQuest;
        List<SavedTask> savedTask;
        QuestManager.Instance.GetUpdatedDataQuest(out savedQuest, out savedTask);
        QuestProgression = savedQuest;
        TaskProgression = savedTask;

        WorldInfo.Copy(partyManager.WorldInfo);
    }

    #endregion

    public bool DeleteFile(string saveName)
    {
        try
        {
            Directory.Delete(SavePath + "/" + saveName, true);
            return true;
        }
        catch (IOException)
        {
            return false;
        }
    }
}

[System.Serializable]
public class WorldState
{
    public string gameArea = "";
    public string gameSubArea = "";
    public string gamePortal = "";
    public int currChapter;
    public int currPhase;
    public int currGold;

    public float PosX;
    public float PosY;
    public float PosZ;

    public float RotX;
    public float RotY;
    public float RotZ;
    public float RotW;

    public string SaveTime;
    public float TimeElapsed;

    public WorldState(string newArea, string newSubArea, string newPortal, int newChapter, int newPhase, int newGold)
    {
        gameArea = newArea;
        gameSubArea = newSubArea;
        gamePortal = newPortal;
        currChapter = newChapter;
        currPhase = newPhase;
        currGold = newGold;
        SaveTime = System.DateTime.Now.ToString("dd.MM.yy");
    }
}