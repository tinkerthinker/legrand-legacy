﻿using System;
using UnityEngine;
using System.Collections.Generic;
using Legrand.database;
using Legrand.core;

public class BackendDictionary{
    public Dictionary<string, Item> ItemData;
    public Dictionary<int, Buff> BuffDictionary;
    public Dictionary<string, QuestWorldEvents> WorldEventData;
    public Dictionary<string, Sprite> SpriteData = new Dictionary<string, Sprite>();
    public Dictionary<string, CharacterLevel> CharacterLevel = new Dictionary<string, CharacterLevel>();
    public Dictionary<string, string> KeyCode = new Dictionary<string, string>();
    public Dictionary<string, string> Language;
    public Dictionary<int, int> ATPDictionary;
    public Dictionary<string, BluePrint> BlueprintDictionary;
    public Dictionary<string, EquipmentDataModel> EquipmentsDictionary;
    public Dictionary<string, Sprite> MapAreaSpriteDictionary;
    public Dictionary<int, Magic> MagicDictionary = new Dictionary<int, Magic>();
    public Dictionary<string, OffensiveBattleSkill> OffensiveSkill = new Dictionary<string, OffensiveBattleSkill>();
    public Dictionary<string, DefensiveBattleSkill> DefensiveSkill = new Dictionary<string, DefensiveBattleSkill>();
    public Dictionary<string, NormalSkill> NormalSkill = new Dictionary<string, NormalSkill>();
    public Dictionary<string, GuardBuff> GuardSkill = new Dictionary<string, GuardBuff>();
    public Dictionary<string, float> ConstantPercentageValueDictionary = new Dictionary<string, float>();
    public Dictionary<string, MerchantLevel> MerchantLevelDictionary = new Dictionary<string, MerchantLevel>();
    public BackendDictionary(Database db)
    {

        #region Item
        ItemData = new Dictionary<string, Item>();
        for (int i = 0; i < db.ConsumableHealingItemData.Count; i++)
        {
            ItemData.Add(db.ConsumableHealingItemData.GetFieldValue(i, "_ID"), db.ConsumableHealingItemData.Get(i));
        }

        for (int i = 0; i < db.ConsumableItemAttributeData.Count; i++)
        {
            ItemData.Add(db.ConsumableItemAttributeData.GetFieldValue(i, "_ID"), db.ConsumableItemAttributeData.Get(i));
        }

        for (int i = 0; i < db.MaterialItemData.Count; i++)
        {
            ItemData.Add(db.MaterialItemData.GetFieldValue(i, "_ID"), db.MaterialItemData.Get(i));
        }
        
        for (int i = 0; i < db.MagicItemData.Count; i++)
        {
            ItemData.Add(db.MagicItemData.GetFieldValue(i, "_ID"), db.MagicItemData.Get(i));
        }

        for (int i = 0; i < db.KeyData.Count; i++)
        {
            ItemData.Add(db.KeyData.GetFieldValue(i, "_ID"), db.KeyData.Get(i));
        }
        #endregion

        #region buff
        BuffDictionary = new Dictionary<int, Buff>();
        for (int i = 0; i < db.BuffData.Count; i++)
        {
            BuffDictionary.Add(int.Parse(db.BuffData.GetFieldValue(i, "_ID").ToString()), db.BuffData.Get(i));
        }
        #endregion

        #region ATP
        ATPDictionary = new Dictionary<int, int>();
        for (int i = 0; i < db.AttributePointData.Count; i++)
        {
            AttributePoint atp = db.AttributePointData.Get(i);
            ATPDictionary.Add(atp.Level, atp.Point);
        }
        #endregion

        #region World events
        WorldEventData = new Dictionary<string, QuestWorldEvents>();
        for (int i = 0; i < db.storyData.Count; i++)
            WorldEventData.Add(db.storyData.Get(i).ID, db.storyData.Get(i).Scene);
        #endregion
        
        #region Sprite
        for (int i = 0; i < db.SpriteData.Count; i++)
        {
            SpriteDataModel spriteData = db.SpriteData.Get(i);
            SpriteData.Add(spriteData.ID, spriteData.sprite);
        }
        #endregion

        #region CharacterLevel
        for (int i = 0; i < db.CharacterLevelData.Count; i++)
        {
            CharacterLevel cl = db.CharacterLevelData.Get(i);
            CharacterLevel.Add(cl.ID_Character, cl);
        }
        #endregion

        #region KeyCode
        string[] KeyCodeMember = Enum.GetNames(typeof(KeyCode));
        for (int i = 0; i < db.KeyCodeData.Count; i++)
        {
            string key = db.KeyCodeData.Get(i).KeyCode != UnityEngine.KeyCode.None ? KeyCodeMember[(int)db.KeyCodeData.Get(i).KeyCode] : db.KeyCodeData.Get(i).AxisCode;
            if (!KeyCode.ContainsKey(key))
                KeyCode.Add(key, db.KeyCodeData.Get(i).KeyName);
            else
                Debug.LogError("keycode error");
        }
        #endregion

        Language = new Dictionary<string, string>();
        SetLanguage();
        SetBlueprintDictionary(db);
        SetEquipmentDictionary(db);

        #region SpriteArea
        MapAreaSpriteDictionary = new Dictionary<string, Sprite>();
        for (int i=0;i<db.AreaSpriteDatabase.Count;i++)
        {
            MapAreaSpriteDictionary.Add(db.AreaSpriteDatabase.Get(i).ID, db.AreaSpriteDatabase.Get(i).sprite);
        }
        #endregion
        #region magic
        for (int i = 0; i < db.MagicData.Count; i++)
        {
            MagicDictionary.Add(db.MagicData.Get(i).ID, db.MagicData.Get(i));
        }
        #endregion
        #region skill
        for (int i = 0; i < db.OffensiveSkillData.Count; i++)
        {
            OffensiveSkill.Add(db.OffensiveSkillData.Get(i).IdSkill, db.OffensiveSkillData.Get(i));
        }
        for (int i = 0; i < db.DefensiveSkillData.Count; i++)
        {
            DefensiveSkill.Add(db.DefensiveSkillData.Get(i).IdSkill, db.DefensiveSkillData.Get(i));
        }
        for (int i = 0; i < db.NormalSkillData.Count; i++)
        {
            NormalSkill.Add(db.NormalSkillData.Get(i).IdSkill, db.NormalSkillData.Get(i));
        }
        for (int i = 0; i < db.GuardBuffData.Count; i++)
        {
            GuardSkill.Add(db.GuardBuffData.Get(i).IdSkill, db.GuardBuffData.Get(i));
        }
        #endregion
        #region constant value formula
        for (int i=0;i<db.constantValueFormula.Count;i++)
        {
            ConstantPercentageValueDictionary.Add(db.constantValueFormula.Get(i).Name, db.constantValueFormula.Get(i).Value);
        }
        #endregion

        #region merchant level
        MerchantLevelDictionary.Clear();
        for(int i=0;i<db.MerchantLevelDatabase.Count;i++)
        {
            MerchantLevelDictionary.Add(db.MerchantLevelDatabase.Get(i).ID, db.MerchantLevelDatabase.Get(i));
        }
        #endregion
    }

    public void SetLanguage()
    {
        Language.Clear();
        Language = GameSetting.GetLanguage();
    }
    public void SetBlueprintDictionary(Database db)
    {
        BlueprintDictionary = new Dictionary<string, BluePrint>();
        for(int i=0;i<db.BlueprintData.Count;i++)
        {
            BlueprintDictionary.Add(db.BlueprintData.Get(i).ID, db.BlueprintData.Get(i));
        }
    }
    public void SetEquipmentDictionary(Database db)
    {
        EquipmentsDictionary = new Dictionary<string, EquipmentDataModel>();
        for(int i=0;i<db.EquipmentDatabase.Count;i++)
        {
            EquipmentsDictionary.Add(db.EquipmentDatabase.Get(i).ID, db.EquipmentDatabase.Get(i));
        }
    }
}
