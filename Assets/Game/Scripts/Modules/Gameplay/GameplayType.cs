﻿using UnityEngine;
using System.Collections;

public enum GameplayType
{
	World,
	Sequence,
	Battle,
	UnityScene
}