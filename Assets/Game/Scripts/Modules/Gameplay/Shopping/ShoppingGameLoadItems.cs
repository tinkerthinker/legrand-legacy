﻿using System.Collections.Generic;
using System.Linq;

public class ShoppingGameLoadItems
{
    int SlotCount;
    int MaxItem;
    List<ShoppingGame.Grid> Items = new List<ShoppingGame.Grid>();
    List<int> itemsIndex = new List<int>();
    List<ShoppingGame.Grid> Template = new List<ShoppingGame.Grid>();
    List<ShoppingGame.Grid> RandomTemp = new List<ShoppingGame.Grid>();

    bool isItemValid(int index)
    {
        Item item = LegrandBackend.Instance.ItemData.Values.ElementAt(index);
        if (item.Icon == null)
            return false;

        for (int i = 0; i < itemsIndex.Count; i++)
        {
            if (itemsIndex[i] == index)
                return false;
        }
        return true;
    }

    bool isGridItemValid(ShoppingGame.Grid item)
    {
        for(int i=0;i<RandomTemp.Count;i++)
        {
            if (RandomTemp[i].Name.Equals(item.Name))
                return false;
        }
        RandomTemp.Add(item);
        return true;
    }
    
    ShoppingGame.Grid GetItem(Item item)
    {
        ShoppingGame.Grid gridItem = new ShoppingGame.Grid();
        do
        {
            gridItem = ShoppingGame.Grid.CopyData(Template[LegrandUtility.Random(0, SlotCount)]);
        } while (!isGridItemValid(gridItem));

        gridItem.SetItem(item.Name, item.Icon);

        return gridItem;
    }

    void LoadSelectedItem()
    {
        Items.Clear();
        itemsIndex.Clear();
        int itemCount = LegrandBackend.Instance.ItemData.Count;
        int currSlot = 0;
        for (int i = 0; i < MaxItem; i++)
        {
            int random = -1;
            do
            {
                random = LegrandUtility.Random(0, itemCount);
            } while (!isItemValid(random));
            itemsIndex.Add(random);
            Items.Add(GetItem(LegrandBackend.Instance.ItemData.Values.ElementAt(random)));
            currSlot++;
            if (currSlot >= SlotCount)
            {
                RandomTemp.Clear();
                currSlot = 0;
            }
        }
    }

    public List<ShoppingGame.Grid> LoadItems(List<ShoppingGame.Grid> template, int slotCount, int roundCount)
    {
        RandomTemp.Clear();
        Template = template;
        SlotCount = slotCount;
        MaxItem = template.Count * roundCount;
        LoadSelectedItem();
        return Items;
    }
}
