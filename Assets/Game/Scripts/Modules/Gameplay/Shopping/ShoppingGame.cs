﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using TeamUtility.IO;

public class ShoppingGame : MonoBehaviour
{
    public enum GridState { True, False, BauBau };

    [System.Serializable]
    public class Grid
    {
        public string Name;
        public GridState State;
        public string ItemName;
        public Sprite ItemIcon;

        public void SetItem(string itemName,Sprite itemIcon)
        {
            ItemName = itemName;
            ItemIcon = itemIcon;
        }

        public Grid()
        {
            Name = "";
            State = GridState.False;
            ItemName = "";
            ItemIcon = new Sprite();
        }

        public static Grid CopyData(Grid data)
        {
            Grid newData = new Grid();
            newData.Name = data.Name;
            newData.State = data.State;
            newData.ItemName = data.ItemName;
            newData.ItemIcon = data.ItemIcon;
            return newData;
        }
    }
    public List<Grid> GridTemplate;
    public int RoundCount;
    int CurrRound;
    public float RoundTime = 0f;
    public float InputTimeout = 0f;
    public List<Grid> Grids = new List<Grid>();

    ShoppingGameLoadItems loadItems = new ShoppingGameLoadItems();
    bool isGetInput = false;
    int inputIndex = 0;

    void OnEnable()
    {
        StartCoroutine(Startgame());
    }

    public void StartRound(int roundCount)
    {
        RoundCount = roundCount;
        StartCoroutine(Startgame());
    }

    IEnumerator Startgame()
    {
        Grids = loadItems.LoadItems(GridTemplate, GridTemplate.Count, RoundCount);
        inputIndex = 0;
        int start = 0;
        int end = GridTemplate.Count - 1;
        CurrRound = 0;

        yield return new WaitForSeconds(RoundTime);
        
        StartCoroutine(ShowItems(start + (CurrRound * GridTemplate.Count), end + (CurrRound * GridTemplate.Count)));
    }

    IEnumerator ShowItems(int start,int end)
    {
        if (CurrRound >= 0)
        {
            Debug.Log("show item");
            yield return new WaitForSeconds(RoundTime);
            Debug.Log("hide item");
            StartInput();
            yield return new WaitForSeconds(InputTimeout);
            EndInput(true);

            CurrRound++;
            if (CurrRound < RoundCount)
            {
                StartCoroutine(ShowItems(start + (CurrRound * GridTemplate.Count), end + (CurrRound * GridTemplate.Count)));
            }
        }
    }

    void StartInput()
    {
        isGetInput = true;
        Debug.Log("start input");
    }

    void EndInput(bool fromTimeout)
    {
        if (isGetInput && fromTimeout)
        {
            EndGame();
        }
        else
        {
            isGetInput = false;
        }
    }

    void ProcessInput(int value)
    {
        int indexResult = value + (inputIndex * GridTemplate.Count);
        Debug.Log("your input : " + Grids[indexResult].Name + " state " + Grids[indexResult].State.ToString());

        ResultProcess(Grids[indexResult].State);

        inputIndex++;
        EndInput(false);
    }

    void ResultProcess(GridState result)
    {
        if (result == GridState.True)
        {
            if (CurrRound >= RoundCount - 1)
                Debug.Log("you win");
            else
                Debug.Log("next round");
        }
        else
        {
            EndGame();            
        }
    }

    void EndGame()
    {
        Debug.Log("you lose");
        CurrRound = -1;
        StopAllCoroutines();
    }

    void Update()
    {
        if(isGetInput)
        {
            if (InputManager.GetKeyDown(KeyCode.Alpha1))
            {
                ProcessInput(0);
            }
            else if (InputManager.GetKeyDown(KeyCode.Alpha2))
            {
                ProcessInput(1);
            }
            else if (InputManager.GetKeyDown(KeyCode.Alpha3))
            {
                ProcessInput(2);
            }
            else if (InputManager.GetKeyDown(KeyCode.Alpha4))
            {
                ProcessInput(3);
            }
        }
    }
}
