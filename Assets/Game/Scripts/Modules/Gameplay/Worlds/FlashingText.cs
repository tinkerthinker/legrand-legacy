﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Collections;

public class FlashingText : MonoBehaviour {

    public float flashingSpeed = 1f;
    public float lowestAlpha = 0.75f;
    private Text text;

    // Use this for initialization
    void Start () {
        text = GetComponent<Text>();      
	}

    void OnEnable()
    {
        StartCoroutine(DoFlashing());
    }

    IEnumerator DoFlashing()
    {
        while (true)
        {
            yield return text.DOFade(lowestAlpha, flashingSpeed).WaitForCompletion();
            yield return text.DOFade(1, flashingSpeed).WaitForCompletion();
        }
    }
}
