﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CitymapController : SubAreaController
{
    private Button[]        cityPoints;
    
    protected override void Awake()
    {
        base.Awake();
        cityPoints = GetComponentsInChildren<Button>(true);
    }

    void CityMapLoaded()
    {
        AreaController.OnSubAreaLoaded -= CityMapLoaded;
        SetCameraVerticalFOV();
        SetCityPointsActive();
        SetNavigation();
        HighlightCurrentCityPoint(AreaController.Instance.CurrPortal);
    }
	
    void OnEnable()
    {
        AreaController.OnSubAreaLoaded += CityMapLoaded;

        foreach (Button b in cityPoints)
        {
            bool clickedButton = b.GetComponent<MapPointSelection>().clicked;
            if (clickedButton)
                b.GetComponent<MapPointSelection>().clicked = false;

            if(b.navigation.mode == Navigation.Mode.None)
            {
                Navigation temp = b.navigation;
                temp.mode = Navigation.Mode.Explicit;
                b.navigation = temp;
            }
        }

        if ((!AreaController.Instance.DisableBGM && subArea.BGM != WwiseManager.BGM.NONE) && (WwiseManager.Instance.CurrentBGM == WwiseManager.BGM.NONE || WwiseManager.Instance.CurrentBGM != subArea.BGM) && PartyManager.Instance.State.currPhase != (int)PartyManager.StoryPhase.Prolog && AreaController.Instance.active)
        {
            WwiseManager.Instance.PlayBG(subArea.BGM);
        }
    }

    void OnDestroy()
    {
        AreaController.OnSubAreaLoaded -= CityMapLoaded;
    }

    Button GetCityPoints(string name)
    {
        foreach (Button b in cityPoints)
            if (b.name == name) return b;

        return null;
    }

    void SetNavigation()
    {
        foreach (Button b in cityPoints)
        {
            CitymapPointNavigator navigator = b.GetComponent<CitymapPointNavigator>();
            if(navigator != null)
            {
                navigator.SetNavigation();
            }
        }
    }

    public void SetCityPointsActive()
    {
        foreach (Button b in cityPoints)
        {
            CityPointInfo cpi = PartyManager.Instance.WorldInfo.UnlockedCityPoints.Find(item => item.name == b.gameObject.name);
            //FlashingText ft = b.GetComponentInChildren<FlashingText>();
            BlinkImageOverTime ft = b.GetComponentInChildren<BlinkImageOverTime>();
            
            if (cpi != null)
            {
                b.gameObject.SetActive(true);

                if (cpi.isVisited == true && ft != null)
                {
                    ft.gameObject.SetActive(false);
                }
            }
            if (AreaController.Instance != null && AreaController.Instance.UnlockAllPoint)
            {
                b.gameObject.SetActive(true);
            }
        }
    }

    public void HighlightCurrentCityPoint(string point)
    {
        Button currentPoint = GetCityPoints(point);
		EventSystem.current.SetSelectedGameObject (currentPoint.gameObject);
    }

    public void DisableNavigation(Button clickedButton)
    {
        //foreach(Button b in cityPoints)
        //{
        //    if(b != clickedButton)
        //       b.enabled = false;
        //}
        Navigation temp = clickedButton.navigation;
        temp.mode = Navigation.Mode.None;
        clickedButton.navigation = temp;
    }
}
