﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WeaponSlots : MonoBehaviour {
	public enum WeaponPosition
	{
		LeftHand,
		RightHand,
		LeftHip,
		RightHip,
		BackHip,
		Back,
	}

	public List<WeaponSlot> Slots;

	public List<GameObject> Weapons;


	public bool UseAtStart;
	public List<WeaponSlotHelper> DefaultPutWeaponLocation;

	[SerializeField]
	private List<GameObject> WeaponGameObjects;
	private List<Quaternion> WeaponDefaultRotation;

	void Awake()
	{
		WeaponGameObjects = new List<GameObject>();
		WeaponDefaultRotation = new List<Quaternion>();
		foreach (GameObject weapon in Weapons) 
		{
			GameObject g = Instantiate(weapon) as GameObject;
            ModelChanger modelChanger = LegrandUtility.GetComponentInChildren<ModelChanger>(g);
            if (modelChanger != null)
                modelChanger.ChangeModel(PartyManager.Instance.StoryProgression.PartyModels[int.Parse(modelChanger.ModelID)]);
            WeaponDefaultRotation.Add(g.transform.rotation);
			g.SetActive(false);
			g.transform.SetParent(this.transform, false);
			WeaponGameObjects.Add(g);
		}

		if (UseAtStart) 
		{
			PutWeaponDefault();
		}
	}

	public void SearchWeaponSlot()
	{
		Slots.Clear ();
		Search ("HLP_R_Hand", WeaponPosition.RightHand);
		Search ("HLP_L_Hand", WeaponPosition.LeftHand);
	}

	public void Search(string name, WeaponPosition position)
	// Initiate Weapon Position
	{
		GameObject g = transform.Search (name).gameObject;
		WeaponSlot tempSlot = new WeaponSlot ();
		tempSlot.Slot = g;
		tempSlot.Position = position;
		Slots.Add (tempSlot);
	}

	public void Clear()
	{
		Slots.Clear ();
	}

    public void UnequipAllWeapon()
    {
        for(int i=0;i<Slots.Count;i++)
        {
            if (Slots[i].Weapon != null)
            {
                Slots[i].Weapon.transform.SetParent(this.transform, false);
                Slots[i].Weapon.SetActive(false);
                Slots[i].Weapon = null;
            }
        }
    }

    public void Unequip(WeaponPosition position)
    {
        WeaponSlot slot = Slots.Find(x => x.Position == position);
        if (slot.Weapon != null)
        {
            slot.Weapon.transform.SetParent(this.transform, false);
            slot.Weapon.SetActive(false);
            slot.Weapon = null;
        }
    }

    public void PutWeaponDefault()
	{
		foreach(WeaponSlotHelper defaultPosition in DefaultPutWeaponLocation)
		{
			foreach(WeaponSlot slot in Slots)
			{
				if(slot.Position == defaultPosition.Position) PutWeapon(defaultPosition.WeaponIndex, slot.Position);
			}
		}
	}

	public void PutWeapon(int index, WeaponPosition position)
	{
		if (index < WeaponGameObjects.Count && index >= 0) {
			if(WeaponGameObjects[index] == null) WeaponGameObjects[index] = Instantiate(Weapons[index]) as GameObject;

			for(int i = 0; i < Slots.Count; i++)
			{

				if(Slots[i].Weapon == WeaponGameObjects[index]) Slots[i].Weapon = null;

				if(Slots[i].Position == position) 
				{ 
					Slots[i].Equip(WeaponGameObjects[index], WeaponDefaultRotation[index]);
				}
			}
		}
	}

	[System.Serializable]
	public class WeaponSlotHelper
	{
		public WeaponSlots.WeaponPosition Position;
		public int WeaponIndex;
	}
}

[System.Serializable]
public class WeaponSlot
{
	public WeaponSlots.WeaponPosition Position;
	public GameObject Slot;
	public GameObject Weapon;

	public void Equip(GameObject g, Quaternion defaultRotation)
	{
		Unequip ();
		Weapon = g;

		g.transform.parent = Slot.transform;
		g.transform.localPosition = new Vector3 (0f,0f,0f);
		g.transform.localEulerAngles = new Vector3 (0f, 0f, 0f);
		g.SetActive (true);
	}

	public void Unequip()
	{
		if (Weapon != null) Weapon.SetActive(false);
		Weapon = null;
	}
}


