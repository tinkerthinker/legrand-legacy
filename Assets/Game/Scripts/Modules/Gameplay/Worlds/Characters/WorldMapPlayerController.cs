﻿using UnityEngine;
using System.Collections;
using TeamUtility.IO;

public class WorldMapPlayerController : MonoBehaviour {

	public enum WorldMapPlayerControllerState
	{
		Normal,
		Transition
	}
	public WorldMapPlayerControllerState controllerState;

	public float    moveSpeed = 1.5f;
	private float   turnSmoothing = 15f;
	private float   _blendAnimTime;

    private Vector3 _targetMovement;
    private Rigidbody2D _rigidBody2d;
    private CircleCollider2D _circleCollider2d;
    private Animator _anim;
    private GameObject _collider3dObject;

    void OnEnable()
    {
        if (_rigidBody2d == null)
        {
            _rigidBody2d = gameObject.AddComponent<Rigidbody2D>();
            _rigidBody2d.gravityScale = 0f;
            _rigidBody2d.freezeRotation = true;

            _circleCollider2d = gameObject.AddComponent<CircleCollider2D>();
            _circleCollider2d.radius = 0.4f;

            _collider3dObject = new GameObject("Collider3d");
            _collider3dObject.transform.parent = transform;
            _collider3dObject.transform.localPosition = Vector3.zero;
            _collider3dObject.transform.localRotation = Quaternion.identity;
            _collider3dObject.tag = "PartyLeader";

            Rigidbody _rigidBody3d = _collider3dObject.AddComponent<Rigidbody>();
            _rigidBody3d.useGravity = false;
            _rigidBody3d.freezeRotation = true;

            CapsuleCollider _capsuleCollider = _collider3dObject.AddComponent<CapsuleCollider>();
            _capsuleCollider.center = new Vector3(0f, 0.85f, 0f);
            _capsuleCollider.radius = 0.4f;
            _capsuleCollider.height = 1.7f;
            _capsuleCollider.direction = 1;
        }
    }

    void OnDisable()
    {
        if(_rigidBody2d != null && _circleCollider2d != null && _collider3dObject != null)
        {
            // Remove 2d colliders
            Destroy(_rigidBody2d);
            Destroy(_circleCollider2d);

            // Remove 3d colliders
            Destroy(_collider3dObject);
        }
    }

	void Awake()
	{
		_anim = GetComponent<Animator>();
        _rigidBody2d = GetComponent<Rigidbody2D>();
        _circleCollider2d = GetComponent<CircleCollider2D>();
        controllerState = WorldMapPlayerControllerState.Normal;
	}

	void FixedUpdate()
	{
		if (controllerState == WorldMapPlayerControllerState.Normal) 
		{
            float hm = InputManager.GetAxis ("Horizontal");
            float vm = InputManager.GetAxis ("Vertical");

            float ha = InputManager.GetAxis ("AltHorizontal");
            float va = InputManager.GetAxis ("AltVertical");

            float h = (Mathf.Abs(hm) > Mathf.Abs(ha)) ? hm : ha;
            float v = (Mathf.Abs(vm) > Mathf.Abs(va)) ? vm : va;

			_blendAnimTime = Mathf.Clamp01 (new Vector2 (h, v).magnitude);
            Move(h, v);
		} 
		else 
		{
			_anim.SetFloat ("Speed", 0, 0.15f, Time.deltaTime);
		}
	}

    void Update()
    {
        if (controllerState == WorldMapPlayerControllerState.Normal && GlobalGameStatus.Instance.IsStateNormal())
        {
            if (InputManager.GetButtonDown("Confirm"))
            //if (GameplayInputManager.Instance.GetButton(ControlType.Confirm))
                ActivatePortal();

			if (InputManager.GetButtonDown("Menu"))
            //if (GameplayInputManager.Instance.GetButton(ControlType.Menu))
				EventManager.Instance.TriggerEvent(new MenuInGameEvent(true));
        }
    }

    void Move(float horizontal, float vertical)
    {
        _targetMovement.Set(horizontal, vertical, 0f);
        _targetMovement = Vector3.ClampMagnitude(_targetMovement, 1);

        if (horizontal != 0 || vertical != 0)
        {
            _rigidBody2d.MovePosition(transform.position + _targetMovement * moveSpeed * Time.deltaTime);
            //_rigidBody2d.velocity = _targetMovement * moveSpeed;
            Rotate(_targetMovement);
            _anim.SetFloat("Speed", _blendAnimTime * moveSpeed);
        }
        else
        {
            _anim.SetFloat("Speed", 0);
        }
    }

	void ActivatePortal()
	{
		EventManager.Instance.TriggerEvent(new EnterTrigger()); 
	}
	
	void Rotate(Vector3 direction)
	{
		direction = transform.InverseTransformDirection(direction);
		float turnAmount = Mathf.Atan2(direction.x, direction.z);
		transform.Rotate(0f, turnAmount * turnSmoothing, 0f);
	}

	void GetPointPath(out Vector3 dir, GameObject dest)
	{
		dir = Vector3.zero;

		if(dest != null)
			dir = dest.transform.position;
	}

	bool CheckPlayerRange(GameObject currentPoint)
	{
		if(Vector3.Distance(transform.position, currentPoint.transform.position) <= 0.1f)
			return true;

		return false;
	}
}
