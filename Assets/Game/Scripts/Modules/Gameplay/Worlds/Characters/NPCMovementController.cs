﻿using UnityEngine;
using System.Collections;
using TeamUtility.IO;
using DG.Tweening;

public class NPCMovementController : MonoBehaviour
{

    private Vector2 _targetPosition;
    private bool _isMove;

    private bool _isLookForward;

    private Vector3 _targetMovement;
    private Rigidbody _rigidBody;
    private CapsuleCollider _capsuleCollider;
    
    private float _moveSpeed;
    private float _turnSmoothing = 15f;

    private RaycastHit _hitInfo;
    private Transform _BIP;

    private const float _DefaultDistanceStop = 0.04f;
    private float _DistanceStop;

    public delegate void MoveDone();
    public MoveDone Done;

    private Tween _RotationTween;
    void Awake()
    {
        _rigidBody = GetComponent<Rigidbody>();
        _capsuleCollider = GetComponent<CapsuleCollider>();
        _isMove = false;

        GetBIP();
    }

    void OnEnable()
    {
        if (_rigidBody == null && _capsuleCollider == null)
        {
            _rigidBody = gameObject.AddComponent<Rigidbody>();
            _rigidBody.freezeRotation = true;

            _capsuleCollider = gameObject.AddComponent<CapsuleCollider>();
            _capsuleCollider.center = new Vector3(0f, 0.85f, 0f);
            _capsuleCollider.radius = 0.4f;
            _capsuleCollider.height = 1.7f;
            _capsuleCollider.direction = 1;
        }
    }

    public void ClearEndMovementListener()
    {
        Done = null;
    }

    public void SetMovement(Vector2 target, float speed, bool isLookForward)
    {
        _targetPosition = target;
        _isMove = true;
        _moveSpeed = speed;
        _isLookForward = isLookForward;
        _DistanceStop = _DefaultDistanceStop;
    }

    public void SetMovement(Vector2 target, float speed, bool isLookForward, float distanceStop)
    {
        _targetPosition = target;
        _isMove = true;
        _moveSpeed = speed;
        _isLookForward = isLookForward;
        _DistanceStop = distanceStop;
    }

    public void SetLookAt(Vector3 target, float time)
    {
        StopMove();
        if (_RotationTween != null)
        {
            _RotationTween.Kill();
            _RotationTween = null;
        }
        _RotationTween = transform.DOLookAt(target, time, AxisConstraint.Y);
    }

    public void SetRotation(float Y, float speed)
    {
        StopMove();
        Vector3 getRotationVector = transform.rotation.eulerAngles;
        getRotationVector.y = Y;
        if (_RotationTween != null)
        {
            _RotationTween.Kill();
            _RotationTween = null;
        }

        _RotationTween = transform.DORotate(getRotationVector, 360f / speed);
    }

    void OnDisable()
    {
        if (_rigidBody != null && _capsuleCollider != null)
        {
            Done = null;
            Destroy(_rigidBody);
            Destroy(_capsuleCollider);
        }
    }

    public void GetBIP()
    {
        _BIP = transform.Search("Bip001");
    }

    public void StopMove()
    {
        _isMove = false;
        if (Done != null)
        {
            Done();
            Done = null;
        }
    }

    void FixedUpdate()
    {

        if (!_isMove)
        {
            return;
        }
        
        

        Vector2 heading = _targetPosition - new Vector2(transform.position.x, transform.position.z);
        float distance = heading.magnitude;
        if (distance < _DistanceStop)   // Distance approximaion
        {
            StopMove();
            return;
        }
        Vector2 direcion = heading / distance;
        float y = 0;

        // Handle player physics when going down steep ground
        if (_BIP != null && !Physics.Raycast(new Ray(_BIP.position, Vector3.down), out _hitInfo, 1f, LayerMask.GetMask("Ground")) && !Physics.Raycast(new Ray(transform.position, transform.forward), out _hitInfo, 2f, LayerMask.GetMask("Ground")))
            {
                y -= 40f * Time.deltaTime;
            }
            else if(_BIP != null && Physics.Raycast(new Ray(_BIP.position, Vector3.down), out _hitInfo, 1f, LayerMask.GetMask("Ground")))
            {
                _rigidBody.velocity = Vector3.zero;
            }
            Move(direcion.x, y, direcion.y);
    }

    void Move(float horizontalX, float verticalY, float horizontalZ)
    {
        _targetMovement.Set(horizontalX, verticalY, horizontalZ);
        _targetMovement = Vector3.ClampMagnitude(_targetMovement, 1);

        if (horizontalX != 0 || horizontalZ != 0)
        {
            if (MoveAgainstWall(_BIP.position, new Vector3(_targetMovement.x, 0f, _targetMovement.z), 1f))
                _rigidBody.velocity = _targetMovement * _moveSpeed;
            else
                _rigidBody.MovePosition(transform.position + _targetMovement * _moveSpeed * Time.deltaTime);

            if (_isLookForward)
            {
                if (_RotationTween != null)
                {
                    _RotationTween.Kill();
                    _RotationTween = null;
                }
                Rotate(new Vector3(_targetMovement.x, 0f, _targetMovement.z));
            }
        }
    }

    void Rotate(Vector3 direction)
    {
        Quaternion targetRotation = Quaternion.LookRotation(direction);
        Quaternion newRotation = Quaternion.Lerp(transform.rotation, targetRotation, _turnSmoothing * Time.deltaTime);
        transform.rotation = newRotation;
    }
    
    bool MoveAgainstWall(Vector3 position, Vector3 desiredDirection, float distance)
    {
        Ray myRay = new Ray(position, desiredDirection); // cast a Ray from the position of our gameObject into our desired direction.
        RaycastHit hit;

        return Physics.Raycast(myRay, out hit, distance, LayerMask.GetMask("Wall"));
    }
}