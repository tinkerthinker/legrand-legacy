﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerEncounterController : MonoBehaviour
{
	void Start()
    {
        EventManager.Instance.AddListener<MonsterEncounterEvent>(MonsterEncountered);
    }

	void OnDestroy()
	{
		if (EventManager.Instance != null)
        {
			EventManager.Instance.RemoveListener<MonsterEncounterEvent> (MonsterEncountered);
		}
	}
    

	void MonsterEncountered(MonsterEncounterEvent e)
	{
        List<string> encounterID = new List<string>();
        foreach(DungeonMonster dm in AreaController.Instance.CurrSubAreaData.dungeonMonsters)
        {
            for(int i=0;i<dm.probabilityMultiplier;i++)
            {
                encounterID.Add(dm.monsterID);
            }
        }
		GameObject monsterObject = e.MonsterEncounterObject;
        string encounterPartyId = encounterID[LegrandUtility.Random(0, encounterID.Count)];
        string encounterArea = AreaController.Instance.CurrAreaData.areaName;
		TransitionData data = new TransitionData ();
		data.AfterFinishedGoTo = GameplayType.World;
        WorldSceneManager.TransWorldToBattle (AreaController.Instance.CurrPrefab, this.gameObject, monsterObject, "Battle/Arena " + encounterArea, encounterPartyId, data, e.AmbushAdvantage);
	}
}