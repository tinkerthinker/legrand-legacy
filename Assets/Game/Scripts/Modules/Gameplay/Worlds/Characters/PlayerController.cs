﻿using UnityEngine;
using System.Collections;
using TeamUtility.IO;

public class PlayerController : MonoBehaviour
{
    public enum PlayerControllerState
    {
        Normal,
        Sequence,
        Pause,
        Cutscene
    }

    public float ZoomOutTime = 10f;
    public float walkSpeed;
    public float runSpeed;
    public bool ForceWalk = false;
    private float _moveSpeed;
    private float _blendAnimTime;

    private Vector3 _targetMovement;
    private Rigidbody _rigidBody;
    private CapsuleCollider _capsuleCollider;
    private Animator _anim;

    private float _turnSmoothing = 15f;
    private bool _togglePortalButton;
    private PlayerControllerState PreviousControllerState;
    private PlayerControllerState _ControllerState;
    public PlayerControllerState ControllerState
    {
        get { return _ControllerState; }
        set { PreviousControllerState = _ControllerState; _ControllerState = value; }
    }

    public delegate void _onMoving(float steps);
    public event _onMoving onMoving;

    private RaycastHit _hitInfo;
    private Transform _BIP;
    
    void Awake()
    {
        _anim = GetComponent<Animator>();
        _rigidBody = GetComponent<Rigidbody>();
        _capsuleCollider = GetComponent<CapsuleCollider>();
        ControllerState = PlayerControllerState.Normal;
        EventManager.Instance.AddListener<TalkToPlayerEvent>(HearNPCTalking);
        EventManager.Instance.AddListener<SequenceEvent>(PlaySequence);
        EventManager.Instance.AddListener<MenuInGameEvent>(InGameMenu);
        EventManager.Instance.AddListener<PauseEvent>(Pause);
        EventManager.Instance.AddListener<UnPauseEvent>(UnPause);
        GetBIP();
    }

    void OnEnable()
    {
        if (_rigidBody == null && _capsuleCollider == null)
        {
            _rigidBody = gameObject.AddComponent<Rigidbody>();
            _rigidBody.freezeRotation = true;

            _capsuleCollider = gameObject.AddComponent<CapsuleCollider>();
            _capsuleCollider.center = new Vector3(0f, 0.85f, 0f);
            _capsuleCollider.radius = 0.4f;
            _capsuleCollider.height = 1.7f;
            _capsuleCollider.direction = 1;
        }
        EventManager.Instance.TriggerEvent(new CheckEncumberedEvent());
    }

    void OnDisable()
    {
        if(EventManager.Instance!=null)
            EventManager.Instance.TriggerEvent(new SetEncumberedEvent(false));
        if (_rigidBody != null && _capsuleCollider != null)
        {
            Destroy(_rigidBody);
            Destroy(_capsuleCollider);
        }
    }

    void OnDestroy()
    {
        if (EventManager.Instance)
        {
            EventManager.Instance.RemoveListener<TalkToPlayerEvent>(HearNPCTalking);
            EventManager.Instance.RemoveListener<SequenceEvent>(PlaySequence);
            EventManager.Instance.RemoveListener<MenuInGameEvent>(InGameMenu);
            EventManager.Instance.RemoveListener<PauseEvent>(Pause);
            EventManager.Instance.RemoveListener<UnPauseEvent>(UnPause);
        }
    }

    public void GetBIP()
    {
        _BIP = transform.Search("Bip001");
    }

    void FixedUpdate()
    {
		if (!GlobalGameStatus.Instance.IsStateNormal())
		{
			_anim.SetFloat("Speed", 0, 0.15f, Time.deltaTime);
			return;
		}

        if (ControllerState == PlayerControllerState.Normal)
        {
            float hm = InputManager.GetAxis ("Horizontal");
            float vm = InputManager.GetAxis ("Vertical");

            float ha = InputManager.GetAxis ("AltHorizontal");
            float va = InputManager.GetAxis ("AltVertical");

            float x = (Mathf.Abs(hm) > Mathf.Abs(ha)) ? hm : ha;
            float y = 0f;
            float z = (Mathf.Abs(vm) > Mathf.Abs(va)) ? vm : va;

            _blendAnimTime = Mathf.Clamp01(new Vector2(x, z).magnitude);

            if ((InputManager.GetKey(KeyCode.LeftShift) || PartyManager.Instance.Inventory.IsOverWeight) || ForceWalk)
                _moveSpeed = Mathf.Lerp(_moveSpeed, walkSpeed, runSpeed * Time.deltaTime);
            else
                _moveSpeed = Mathf.Lerp(_moveSpeed, runSpeed, runSpeed * Time.deltaTime);

            // Handle player physics when going down steep ground
            if (_BIP != null && !Physics.Raycast(new Ray(_BIP.position, Vector3.down), out _hitInfo, 1f, LayerMask.GetMask("Ground")) && !Physics.Raycast(new Ray(transform.position, transform.forward), out _hitInfo, 2f, LayerMask.GetMask("Ground")))
            {
                y -= 40f * Time.deltaTime;
            }
            else if(_BIP != null && Physics.Raycast(new Ray(_BIP.position, Vector3.down), out _hitInfo, 1f, LayerMask.GetMask("Ground")))
            {
                _rigidBody.velocity = Vector3.zero;
            }
            Move(x, y, z);
        }
        else if (ControllerState != PlayerControllerState.Cutscene)
        {
            _anim.SetFloat("Speed", 0, 0.15f, Time.deltaTime);
        }
    }

    void Move(float horizontalX, float verticalY, float horizontalZ)
    {
        _targetMovement.Set(horizontalX, verticalY, horizontalZ);
        _targetMovement = Vector3.ClampMagnitude(_targetMovement, 1);

        if (horizontalX != 0 || horizontalZ != 0)
        {
            if (MoveAgainstWall(_BIP.position, new Vector3(_targetMovement.x, 0f, _targetMovement.z), 1f))
                _rigidBody.velocity = _targetMovement * _moveSpeed;
            else
                _rigidBody.MovePosition(transform.position + _targetMovement * _moveSpeed * Time.deltaTime);

            Rotate(new Vector3(_targetMovement.x, 0f, _targetMovement.z));
            _anim.SetFloat("Speed", _blendAnimTime * _moveSpeed);
            if (onMoving != null)
                onMoving(_moveSpeed * Time.deltaTime);

            EventManager.Instance.TriggerEvent(new FirstStep());
        }
        else
        {
            if(AreaController.Instance && !AreaController.Instance.DisableZoomOut)
                EventManager.Instance.TriggerEvent(new StartTimerEvent(ZoomOutTime));
            _anim.SetFloat("Speed", 0);
        }
    }

    void Rotate(Vector3 direction)
    {
        Quaternion targetRotation = Quaternion.LookRotation(direction);
        Quaternion newRotation = Quaternion.Lerp(transform.rotation, targetRotation, _turnSmoothing * Time.deltaTime);
        transform.rotation = newRotation;
    }

    void Update()
    {
        if (ControllerState == PlayerControllerState.Normal && GlobalGameStatus.Instance.IsStateNormal())
        {
            #if UNITY_EDITOR
            if (Input.GetMouseButtonDown(1))
            {
                RaycastHit hit;
                Ray DEBUG_RAY = GlobalGameStatus.Instance.MainCamera.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(DEBUG_RAY, out hit, Mathf.Infinity,1 << LayerMask.NameToLayer("Ground")))
                    transform.position = hit.point;
            }

            if (Input.GetKey(KeyCode.Semicolon))
            {
                EventManager.Instance.TriggerEvent(new FastTravelUIEvent(true));
            }

            if (Input.GetKey(KeyCode.Backslash))
            {
                EventManager.Instance.TriggerEvent(new StorageUIEvent(true));
            }
            #endif

            if (InputManager.GetButtonDown("Confirm"))
            {
                EventManager.Instance.TriggerEvent(new Interact(this.gameObject));
                EventManager.Instance.TriggerEvent(new EnterTrigger());
            }

            if (InputManager.GetButtonDown("Menu"))
            {
                EventManager.Instance.TriggerEvent(new MenuInGameEvent(true));
            }

            if (InputManager.GetButtonDown("Portal Toggle"))
            {
                _togglePortalButton = !_togglePortalButton;
                EventManager.Instance.TriggerEvent(new TogglePortalIndicatorEvent(_togglePortalButton));
            }
        }
    }

    void HearNPCTalking(TalkToPlayerEvent e)
    {
        if (e.FinishTalking)
            ControllerState = PlayerControllerState.Normal;
        else
            ControllerState = PlayerControllerState.Sequence;
    }

    void PlaySequence(SequenceEvent e)
    {
        if (e.IsSequenceFinished) ControllerState = PlayerControllerState.Normal;
        else ControllerState = PlayerControllerState.Sequence;
    }

    void Pause(PauseEvent p)
    {
        _anim.enabled = false;
        ControllerState = PlayerControllerState.Pause;
    }

    void UnPause(UnPauseEvent p)
    {
        _anim.enabled = true;
        ControllerState = PreviousControllerState;
    }

    void InGameMenu(MenuInGameEvent p)
    {
        if (p.isOpen)
            ControllerState = PlayerControllerState.Pause;
        else
            ControllerState = PreviousControllerState;
    }

    public void MyAnimationEventCallback(AnimationEvent evt)
    {
        if (evt.animatorClipInfo.weight > 0.5)
        {
            // Do handle animation event
        }
    }

    bool MoveAgainstWall(Vector3 position, Vector3 desiredDirection, float distance)
    {
        Ray myRay = new Ray(position, desiredDirection); // cast a Ray from the position of our gameObject into our desired direction.
        RaycastHit hit;

        return Physics.Raycast(myRay, out hit, distance, LayerMask.GetMask("Wall"));
    }

	// This is for sequencer call function
	void SetNormalState()
	{
		ControllerState = PlayerControllerState.Normal;
	}
	void SetSequenceState()
	{
		ControllerState = PlayerControllerState.Sequence;
	}
	void SetCutsceneState()
	{
		ControllerState = PlayerControllerState.Cutscene;
	}

    public IEnumerator PortalWalking(WorldTrigger trigger)
    {
        Quaternion from = transform.rotation;
        Quaternion to = transform.rotation;

        float t = 0.1f;

        to = Quaternion.Euler(transform.rotation.x, trigger.transform.rotation.eulerAngles.y + 180f, transform.rotation.z);

        int i = 0;
        while (true)
        {
            transform.rotation = Quaternion.Slerp(from, to, t);
            t += 0.05f;
            transform.Translate(Vector3.forward * Time.deltaTime * _moveSpeed, Space.Self);
            _anim.SetFloat("Speed", _moveSpeed);
            yield return new WaitForFixedUpdate();
        }
    }
}