﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(WeaponSlots))]
public class WeaponSlotsEditor : Editor {
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
		if (GUILayout.Button("Search Weapon Slot"))
		{
			WeaponSlots script = (WeaponSlots)target;
			script.SearchWeaponSlot();
		}
	}


}
#endif