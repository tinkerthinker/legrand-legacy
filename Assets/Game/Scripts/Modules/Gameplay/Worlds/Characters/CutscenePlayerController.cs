﻿using UnityEngine;
using System.Collections;
using TeamUtility.IO;
using System.Collections.Generic;
public class CutscenePlayerController : MonoBehaviour
{
	[System.Serializable]
	public class CutscenePlayerData
	{
		public Vector3 Movement;
		public float Speed;

		public CutscenePlayerData(Vector3 move, float speed)
		{
			Movement = move;
			Speed = speed;
		}
	}

    public float walkSpeed;
    public float runSpeed;
	public List<CutscenePlayerData> MovementData;
	public bool PlayAutomatic = false;

	private float _moveSpeed;
    private float _blendAnimTime;

    private Vector3 _targetMovement;
    private Rigidbody _rigidBody;
    private CapsuleCollider _capsuleCollider;
    private Animator _anim;

    private float _turnSmoothing = 15f;
    
    private RaycastHit _hitInfo;
    private Transform _BIP;
    
	private int _CurrentFrameIndex = 0; 
	[SerializeField]
	private bool _CutsceneStarted = false;

    void Awake()
    {
        _anim = GetComponent<Animator>();
        _rigidBody = GetComponent<Rigidbody>();
        _capsuleCollider = GetComponent<CapsuleCollider>();
        GetBIP();

    }

    void OnEnable()
    {
        if (_rigidBody == null && _capsuleCollider == null)
        {
            _rigidBody = gameObject.AddComponent<Rigidbody>();
            _rigidBody.freezeRotation = true;

            _capsuleCollider = gameObject.AddComponent<CapsuleCollider>();
            _capsuleCollider.center = new Vector3(0f, 0.85f, 0f);
            _capsuleCollider.radius = 0.4f;
            _capsuleCollider.height = 1.7f;
            _capsuleCollider.direction = 1;
        }
    }

    void OnDisable()
    {
        if (_rigidBody != null && _capsuleCollider != null)
        {
            Destroy(_rigidBody);
            Destroy(_capsuleCollider);
        }
    }

    public void GetBIP()
    {
        _BIP = transform.Search("Bip001");
    }

    void FixedUpdate()
    {
		if (!PlayAutomatic) {
			float x = InputManager.GetAxis ("Horizontal");
			float y = 0;
			float z = InputManager.GetAxis ("Vertical");

			if (InputManager.GetKey (KeyCode.LeftShift))
				_moveSpeed = Mathf.Lerp (_moveSpeed, walkSpeed, runSpeed * Time.deltaTime);
			else
				_moveSpeed = Mathf.Lerp (_moveSpeed, runSpeed, runSpeed * Time.deltaTime);
			;

			// Handle player physics when going down steep ground
			if (_BIP != null && !Physics.Raycast (new Ray (_BIP.position, Vector3.down), out _hitInfo, 1f, LayerMask.GetMask ("Ground")) && !Physics.Raycast (new Ray (transform.position, transform.forward), out _hitInfo, 2f, LayerMask.GetMask ("Ground"))) {
				print ("not touching ground");
				y -= 40f * Time.deltaTime;
			}
			Move (x, y, z);
		} else 
		{
			if (_CutsceneStarted) 
			{
				if (_CurrentFrameIndex >= MovementData.Count)
					return;
				
				_moveSpeed = MovementData [_CurrentFrameIndex].Speed;
				Move (MovementData [_CurrentFrameIndex].Movement.x, MovementData [_CurrentFrameIndex].Movement.y, MovementData [_CurrentFrameIndex].Movement.z);
				_CurrentFrameIndex++;
			}
		}
    }

    void Move(float horizontalX, float verticalY, float horizontalZ)
    {
        _targetMovement.Set(horizontalX, verticalY, horizontalZ);
        _targetMovement = Vector3.ClampMagnitude(_targetMovement, 1);

        if (horizontalX != 0 || horizontalZ != 0)
        {
			if (!PlayAutomatic)
				MovementData.Add (new CutscenePlayerData (new Vector3 (horizontalX, verticalY, horizontalZ), _moveSpeed));

            if (MoveAgainstWall(_BIP.position, new Vector3(_targetMovement.x, 0f, _targetMovement.z), 1f))
                _rigidBody.velocity = _targetMovement * _moveSpeed;
            else
                _rigidBody.MovePosition(transform.position + _targetMovement * _moveSpeed * Time.deltaTime);

            Rotate(new Vector3(_targetMovement.x, 0f, _targetMovement.z));
        }
    }

    void Rotate(Vector3 direction)
    {
        Quaternion targetRotation = Quaternion.LookRotation(direction);
        Quaternion newRotation = Quaternion.Lerp(transform.rotation, targetRotation, _turnSmoothing * Time.deltaTime);
        transform.rotation = newRotation;
    }
		

    bool MoveAgainstWall(Vector3 position, Vector3 desiredDirection, float distance)
    {
        Ray myRay = new Ray(position, desiredDirection); // cast a Ray from the position of our gameObject into our desired direction.
        RaycastHit hit;

        return Physics.Raycast(myRay, out hit, distance, LayerMask.GetMask("Wall"));
    }

	public void StartCutscene()
	{
		_CutsceneStarted = true;
	}
}