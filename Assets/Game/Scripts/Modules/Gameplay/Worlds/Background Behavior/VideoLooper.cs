﻿using UnityEngine;
using System.Collections;
using RenderHeads.Media.AVProVideo;

public class VideoLooper : MonoBehaviour {

    public MediaPlayer _media;

    void Update()
    {
        if(_media.Control.GetCurrentTimeMs() >= _media.Info.GetDurationMs()-100)
        {
            _media.Rewind(false);
        }
    }
}
