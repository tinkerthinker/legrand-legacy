﻿using UnityEngine;
using UnityEngine.UI;

public class CitymapPointNavigator : MonoBehaviour
{
    public Selectable[] Up;
    public Selectable[] Down;
    public Selectable[] Right;
    public Selectable[] Left;

    Selectable GetSelectable(Selectable[] selectable)
    {
        for(int i=0;i<selectable.Length;i++)
        {
            if(selectable[i].gameObject.activeInHierarchy)
            {
                return selectable[i];
            }
        }
        return null;
    }

    public void SetNavigation()
    {
        Button button = GetComponent<Button>();
        Navigation navi = button.navigation;
        navi.mode = Navigation.Mode.Explicit;
        navi.selectOnUp = GetSelectable(Up);
        navi.selectOnDown = GetSelectable(Down);
        navi.selectOnRight = GetSelectable(Right);
        navi.selectOnLeft = GetSelectable(Left);
        button.navigation = navi;
    }
}
