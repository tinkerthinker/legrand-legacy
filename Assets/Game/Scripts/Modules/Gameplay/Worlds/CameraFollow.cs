using System;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
	private float 	vertExt;
	private float 	horExt;

	private float 	_MinXPosition;
	private float 	_MaxXPosition;
	private float 	_MinYPosition;
	private float 	_MaxYPosition;

	private Vector3	velocity 	= Vector3.zero;
	private float	smoothTime 	= 0.2f;

	void Start()
	{
		GameObject clusterObject = AreaController.Instance.CurrPrefab;
        Bounds worldMapBoundaries = clusterObject.GetComponentInChildren<Renderer>().bounds;
        
        vertExt = GetComponent<Camera>().orthographicSize;
        horExt = vertExt * Screen.width / Screen.height;

        _MinXPosition = worldMapBoundaries.min.x + horExt;
        _MaxXPosition = worldMapBoundaries.max.x - horExt;

        _MinYPosition = worldMapBoundaries.min.y + vertExt;
        _MaxYPosition = worldMapBoundaries.max.y - vertExt;
    }

    void LateUpdate()
    {
		Vector3 targetFollow = AreaController.Instance.CurrPlayer.transform.position;
		targetFollow.z = transform.position.z;
        targetFollow.x = Mathf.Clamp(targetFollow.x, _MinXPosition, _MaxXPosition);
        targetFollow.y = Mathf.Clamp(targetFollow.y, _MinYPosition, _MaxYPosition);

        transform.position = targetFollow;
    }
}

