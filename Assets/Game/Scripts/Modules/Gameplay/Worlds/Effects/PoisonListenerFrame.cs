﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;

public class PoisonListenerFrame : MonoBehaviour
{
    public Image Frame;
    public float FadeSpeed;
    public Color MaxColor;
    public float ClearTime;

    void Start ()
    {
        EventManager.Instance.AddListener<PoisonEvent>(ActivatePoison);
	}

	void OnDestroy()
	{
		if (EventManager.Instance != null)
			EventManager.Instance.RemoveListener<PoisonEvent> (ActivatePoison);
	}

    void ActivatePoison(PoisonEvent e)
    {
        if (!AreaController.Instance.insideHealPoint)
        {
            Frame.DOColor(MaxColor, FadeSpeed).SetDelay(0);
            Invoke("FadeClear", ClearTime + FadeSpeed);
        }
    }
    
    void FadeClear()
    {
        Frame.DOColor(Color.clear, FadeSpeed).SetDelay(0);
    }
}
