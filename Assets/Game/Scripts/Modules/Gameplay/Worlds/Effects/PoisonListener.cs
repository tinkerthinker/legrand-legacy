﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class PoisonListener : MonoBehaviour
{
    Renderer meshRenderer;
    Color rimColor;
    float timer;

	// Use this for initialization
	void Start () {
        meshRenderer = GetComponent<Renderer>();
        rimColor = meshRenderer.material.GetColor("_RimColor");
        EventManager.Instance.AddListener<PoisonEvent>(ActivatePoison);
	}

	void OnDestroy()
	{
		if (EventManager.Instance != null)
			EventManager.Instance.RemoveListener<PoisonEvent> (ActivatePoison);
	}

	void ActivatePoison(PoisonEvent e)
	{
        StartCoroutine(FlashingRed());
	}

    IEnumerator FlashingRed()
    {
        while(timer <= 1)
        {
            timer += Time.deltaTime / 0.5f;
            Color c = Color.Lerp(rimColor, Color.red, timer);
            meshRenderer.material.SetColor("_RimColor", c);

            yield return null;
        }
        timer = 0f;

        while (timer <= 1)
        {
            timer += Time.deltaTime / 0.5f;
            Color c = Color.Lerp(Color.red, rimColor, timer);
            meshRenderer.material.SetColor("_RimColor", c);

            yield return null;
        }
        timer = 0f;
    }
}
