﻿using UnityEngine;
using System.Collections;

public class EyeStoneA : MonoBehaviour
{
    public GameObject Obj;

    public void ActivateObject()
    {
        Obj.SetActive(true);
    }

    public void DisableObject()
    {
        Obj.SetActive(false);
    }
}
