﻿using UnityEngine;
using UnityEngine.UI;
using Legrand.core;
using System.Collections.Generic;
using DG.Tweening;
using System.Collections;

public class FrostBiteEffect : MonoBehaviour
{
    public bool DisplayFrameSpeedEffect = true;
    public bool DisplayFrameDamageEffect = true;
    public Image Frame;

    void Start ()
    {
        EventManager.Instance.AddListener<FrostBiteEvent>(FrostBiteAction);
        EventManager.Instance.AddListener<FrostBiteResetEvent>(ResetFrame);
    }

	void OnDestroy()
	{
        if (EventManager.Instance != null)
        {
            EventManager.Instance.RemoveListener<FrostBiteEvent>(FrostBiteAction);
            EventManager.Instance.RemoveListener<FrostBiteResetEvent>(ResetFrame);
        }
	}

    void ResetFrame(FrostBiteResetEvent e)
    {
        Frame.DOColor(Color.clear, e.FadeTime).SetDelay(0);
    }

    void EffectFrame(float alpha,float fadeTime)
    {
        Color initialColor = Frame.color;
        Color maxColor = initialColor;
        maxColor.a = (byte)alpha;
        Debug.Log("Frost bite frame, max color " + maxColor + " fade time " + fadeTime);
        Frame.DOColor(maxColor, fadeTime).SetDelay(0);
        StartCoroutine(FlipColor(initialColor, fadeTime));
    }

    IEnumerator FlipColor(Color color,float fadeTime)
    {
        yield return new WaitForSeconds(fadeTime);
        Debug.Log("Frost bite frame flip, fade time " + fadeTime.ToString());
        Frame.DOFade(0, fadeTime).SetDelay(0);
    }

    void FrostBiteAction(FrostBiteEvent e)
    {
        WwiseManager.Instance.PlaySFX(WwiseManager.WorldFX.Poisoned);
        
        if (e.IsDamage)
        {
            Debug.Log("frost bite damage");

            if(DisplayFrameDamageEffect)
                EffectFrame(e.Effect.AlphaColor, e.Effect.FadeTime);

            List<MainCharacter> partyList = PartyManager.Instance.CharacterParty;

            for (int i = 0; i < partyList.Count; i++)
            {
                float maxHealth = partyList[0].GetHealth().MaxValue;
                float decreaseAmount = maxHealth * (e.Effect.DamageValue / 100);

                partyList[i].GetHealth().Decrease(decreaseAmount);

                if (partyList[i].GetHealth().Value <= 1)
                    partyList[i].GetHealth().ChangeCurrentValue(1);
            }
        }
        else if(!e.Effect.ForceWalk)
        {
            Debug.Log("frost bite decrease run speed");

            if (DisplayFrameSpeedEffect)
                EffectFrame(e.Effect.AlphaColor, e.Effect.FadeTime);

            PlayerController controller = AreaController.Instance.CurrPlayer.GetComponent<PlayerController>();
            DOTween.To(x => controller.runSpeed = x, controller.runSpeed, controller.walkSpeed, e.Effect.Time);
        }
        else if(e.Effect.ForceWalk)
        {
            Debug.Log("frost bite force walk");

            PlayerController controller = AreaController.Instance.CurrPlayer.GetComponent<PlayerController>();
            controller.ForceWalk = true;
        }
    }
}
