﻿using UnityEngine;
using System.Collections;

public class ParallaxCamera : MonoBehaviour
{
    [SerializeField]
    private Vector2 min;
    [SerializeField]
    private Vector2 max;
    
    private GameObject player;

    // Use this for initialization
    void Start () {
        player = (AreaController.Instance != null) ? AreaController.Instance.CurrPlayer : GameObject.Find("PartyLeader");
    }
	
	// Update is called once per frame
	void Update () {
        Vector3 camPos = player.transform.position;
        camPos.x = Mathf.Clamp(camPos.x, min.x, max.x);
        camPos.y = Mathf.Clamp(camPos.y, min.y, max.y);
        camPos.z = transform.position.z;

        transform.position = camPos;
	}
}
