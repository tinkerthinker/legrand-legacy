﻿using UnityEngine;
using System.Collections;

public class ParallaxBackground : MonoBehaviour
{
    public Camera cam;
    public Vector2 minMaxOffsetX;
    public Vector2 minMaxOffsetY;

    public Vector2 minMaxScreenHorizontal;
    public Vector2 minMaxScreenVertical;

    public Vector3 playerScreenPos;

    private GameObject player;
    private Material mat;

    private float rangeHorizontal;
    private float rangeOffsetX;
    private float rangeVertical;
    private float rangeOffsetY;

    private float playerOnScreenPosX;
    private float playerOnScreenPosY;

    private float x,y = 0.0f;

    // Use this for initialization
    void Start () {
        player = GameObject.Find("PartyLeader");
        mat = GetComponent<Renderer>().material;

        rangeHorizontal = minMaxScreenHorizontal.y - minMaxScreenHorizontal.x;
        rangeOffsetX = minMaxOffsetX.y - minMaxOffsetX.x;

        rangeVertical = minMaxScreenVertical.y - minMaxScreenVertical.x;
        rangeOffsetY = minMaxOffsetY.y - minMaxOffsetY.x;
    }
	
	// Update is called once per frame
	void Update () {
        playerOnScreenPosX = cam.WorldToScreenPoint(player.transform.position).x / Screen.width;
        playerOnScreenPosY = cam.WorldToScreenPoint(player.transform.position).y / Screen.height;

        //float ratioX = (playerOnScreenPosX - minMaxScreenHorizontal.x) / rangeHorizontal;
        //x = (ratioX * rangeOffsetX) + minMaxOffsetX.x;
        float ratioX = (playerOnScreenPosY - minMaxScreenVertical.x) / rangeVertical;
        x = (ratioX * rangeOffsetX) + minMaxOffsetX.x;

        float ratioY = (playerOnScreenPosY - minMaxScreenVertical.x) / rangeVertical;
        y = (ratioY * rangeOffsetY) + minMaxOffsetY.x;

        x = Mathf.Clamp(x, minMaxOffsetX.x, minMaxOffsetX.y);
        y = Mathf.Clamp(y, minMaxOffsetY.x, minMaxOffsetY.y);

        mat.SetTextureOffset("_MainTex", new Vector2(x, 0f));

        playerScreenPos = cam.WorldToScreenPoint(player.transform.position);
        playerScreenPos = new Vector3(playerScreenPos.x / Screen.width, playerScreenPos.y / Screen.height, playerScreenPos.z);
    }
}
