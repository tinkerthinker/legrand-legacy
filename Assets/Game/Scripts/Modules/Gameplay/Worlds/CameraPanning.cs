﻿using UnityEngine;
using System.Collections;

public class CameraPanning : MonoBehaviour
{
    private GameObject _player;
    private Camera _cam;

    public Camera secondaryCamera;
    public Texture2D aTexture;
    
    public Vector2 minMaxPanX;
    public Vector2 minMaxPanY;

    public Vector2 minMaxScreenHorizontal;
    public Vector2 minMaxScreenVertical;

    private float _playerOnScreenPosX;
    private float _playerOnScreenPosY;

    private float _rangeHorizontal;
    private float _rangePanX;
    private float _rangeVertical;
    private float _rangePanY;

    public float testX = 0.0f;
    public float testY = 0.0f;
    public Vector3 playerScreenPos;

    private Coroutine _coroutine;
    private float _currPanX, _currPanY = 0f;
    private float _xAxis, _yAxis = 0f;
    public bool zoomedOut;
    public bool onZooming;

    public bool calculating;
    

    void Awake()
    {
        _cam = GetComponent<Camera>();
    }

    // Use this for initialization
    void Start()
    {
        _player = (AreaController.Instance != null) ? AreaController.Instance.CurrPlayer : GameObject.Find("PartyLeader");
        
        _rangeHorizontal = minMaxScreenHorizontal.y - minMaxScreenHorizontal.x;
        _rangePanX = minMaxPanX.y - minMaxPanX.x;

        _rangeVertical = minMaxScreenVertical.y - minMaxScreenVertical.x;
        _rangePanY = minMaxPanY.y - minMaxPanY.x;
    }

    //public void OnCameraEnable()
    //{
    //    EventManager.Instance.AddListener<FirstStep>(StartZooming);
    //    zoomedOut = true;
    //}

    //public void OnCameraDisable()
    //{
    //    EventManager.Instance.RemoveListener<FirstStep>(StartZooming);
    //    ResetProjectionMatrix();
    //}

    /// <summary>
    /// Reset camera properties in case enter battle while zooming.
    /// Field of View is set in roomcontroller.
    /// </summary>
    void OnDisable()
    {
        zoomedOut = false;
        onZooming = false;
        ResetProjectionMatrix();
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if(!calculating)
        {
            _playerOnScreenPosX = secondaryCamera.WorldToScreenPoint(_player.transform.position).x / Screen.width;
            _playerOnScreenPosY = secondaryCamera.WorldToScreenPoint(_player.transform.position).y / Screen.height;

            float ratioX = (_playerOnScreenPosX - minMaxScreenHorizontal.x) / _rangeHorizontal;
            _currPanX = (ratioX * _rangePanX) + minMaxPanX.x;

            float ratioY = (_playerOnScreenPosY - minMaxScreenVertical.x) / _rangeVertical;
            _currPanY = (ratioY * _rangePanY) + minMaxPanY.x;

            _currPanX = Mathf.Clamp(_currPanX, minMaxPanX.y, minMaxPanX.x);
            _currPanY = Mathf.Clamp(_currPanY, minMaxPanY.y, minMaxPanY.x);

            if (!onZooming && !zoomedOut)
                SetVanishingPoint(_cam, new Vector2(_currPanX, _currPanY));
        }
        else
        {
            SetVanishingPoint(_cam, new Vector2(testX, testY));

            playerScreenPos = secondaryCamera.WorldToScreenPoint(_player.transform.position);
            playerScreenPos = new Vector3(playerScreenPos.x / Screen.width, playerScreenPos.y / Screen.height, playerScreenPos.z);
        }
    }
    
    //void StartZooming(FirstStep e)
    //{
    //    EventManager.Instance.RemoveListener<FirstStep>(StartZooming);

    //    float fov = AreaController.Instance.CurrSubAreaData.mainHorizontalFOV;
    //    zoomedOut = false;
    //    StartCoroutine(Zooming(tempX, currPanX, tempY, currPanY, cam.fieldOfView, AreaController.Instance.CurrRoom.ConvertToVerticalFOV(fov), true));
    //}

    public void ZoomIn(float targetFOV)
    {
        if(zoomedOut)
        {
            zoomedOut = false;
            StopCoroutine(_coroutine);
            StartCoroutine(Zooming(_xAxis, _currPanX, _yAxis, _currPanY, _cam.fieldOfView, targetFOV, true));
        }
    }

    public void ZoomOut(float targetFOV)
    {
        zoomedOut = true;
        _coroutine = StartCoroutine(Zooming(_currPanX, 0f, _currPanY, 0f, _cam.fieldOfView, targetFOV));
    }

    IEnumerator Zooming(float xOri, float xDest, float yOri, float yDest, float fovOrigin, float fovFinal, bool zoomingIn = false)
    {
        float t = 0.0f;
        onZooming = true;

        while (t < 1.0f)
        {
            ResetProjectionMatrix();
            t += Time.deltaTime / 2f;
            _cam.fieldOfView = Mathf.Lerp(fovOrigin, fovFinal, t);

            if (zoomingIn)
            {
                xDest = _currPanX;
                yDest = _currPanY;
            }

            _xAxis = Mathf.Lerp(xOri, xDest, t);
            _yAxis = Mathf.Lerp(yOri, yDest, t);
            
            SetVanishingPoint(_cam, new Vector2(_xAxis, _yAxis));

            yield return null;
        }
        onZooming = false;
    }

    void SetVanishingPoint(Camera cam, Vector2 perspectiveOffset)
    {
        Matrix4x4 m = cam.projectionMatrix;
        float w = 2 * cam.nearClipPlane / m.m00;
        float h = 2 * cam.nearClipPlane / m.m11;

        float left = -w / 2 - perspectiveOffset.x;
        float right = left + w;
        float bottom = -h / 2 - perspectiveOffset.y;
        float top = bottom + h;

        cam.projectionMatrix = PerspectiveOffCenter(left, right, bottom, top, cam.nearClipPlane, cam.farClipPlane);
        //print("w : " + w + ", left : " + left + ", right : " + right);
    }

    static Matrix4x4 PerspectiveOffCenter(
        float left, float right,
        float bottom, float top,
        float near, float far)
    {
        float x = (2.0f * near) / (right - left);
        float y = (2.0f * near) / (top - bottom);
        float a = (right + left) / (right - left);
        float b = (top + bottom) / (top - bottom);
        float c = -(far + near) / (far - near);
        float d = -(2.0f * far * near) / (far - near);
        float e = -1.0f;

        Matrix4x4 m = new Matrix4x4();
        m[0, 0] = x; m[0, 1] = 0.0f; m[0, 2] = a; m[0, 3] = 0.0f;
        m[1, 0] = 0.0f; m[1, 1] = y; m[1, 2] = b; m[1, 3] = 0.0f;
        m[2, 0] = 0.0f; m[2, 1] = 0.0f; m[2, 2] = c; m[2, 3] = d;
        m[3, 0] = 0.0f; m[3, 1] = 0.0f; m[3, 2] = e; m[3, 3] = 0.0f;

        return m;
    }

    void ResetProjectionMatrix()
    {
        _cam.ResetProjectionMatrix();
    }

    void OnGUI()
    {
        if(calculating && aTexture != null)
        { 
            float xMin = (Screen.width / 2) - (aTexture.width / 2);
            float yMin = (Screen.height / 2) - (aTexture.height / 2);
            GUI.DrawTexture(new Rect(xMin, yMin, aTexture.width, aTexture.height), aTexture);
        }
    }
}
