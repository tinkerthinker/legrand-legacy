﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;
using System;

public class MapPointSelection : MonoBehaviour, ISelectHandler
{
    public Image cursor;
    [HideInInspector]
	public bool clicked = false;        // Prevent player from overclicking the button

    public void OnSelect(BaseEventData eventData)
    {
        cursor.transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y + 50, cursor.transform.localPosition.z);
    }

    public void SelectPoint()
    {
        if(!clicked)
        { 
            GameObject player = AreaController.Instance.CurrPlayer;
		    bool found = false;
				// Check event in dictionary to find is trigger match
				for (int i = 0; i < PartyManager.Instance.StoryProgression.ActiveEventID.Count && !found; i++) 
				{
					string storyDataID = PartyManager.Instance.StoryProgression.ActiveEventID[i];
					QuestWorldEvents worldEvent;
					if (LegrandBackend.Instance.WorldEventData.TryGetValue (storyDataID, out worldEvent)) 
					{
						if (LegrandUtility.CompareString (worldEvent.WorldTriggerID, this.gameObject.name)) 
						{
							AreaController.Instance.PlayingCutscene = worldEvent.WorldEvent;
							// Remove the cutscene event
							PartyManager.Instance.StoryProgression.ActiveEventID.Remove(storyDataID);
							found = true;
						}
					}
				}
		    

            int index = PartyManager.Instance.WorldInfo.UnlockedCityPoints.FindIndex(point => point.name == this.name);
            if(index > -1) PartyManager.Instance.WorldInfo.UnlockedCityPoints[index].isVisited = true;

            AreaController.Instance.CurrCitymap.DisableNavigation(GetComponent<Button>());
            AreaController.Instance.CurrCitymap.MovePlayer(this.gameObject.name, player);
            clicked = true;
        }
    }


}
