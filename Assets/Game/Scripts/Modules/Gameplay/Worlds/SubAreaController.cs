using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class SubAreaController : MonoBehaviour 
{
    public SubArea  subArea;
    public Camera[] cameras;

    protected virtual void Awake()
    {
        cameras = GetComponentsInChildren<Camera>(true);
    }
    
    public void MovePlayer(string origin, GameObject player,bool unlockByTrigger = false, string destination = "", string portalTo = "")
    {
        SubArea.Connection subAreaConnection = subArea.GetConnectionFrom(origin);
        if(subAreaConnection != null)
        {
            string targetPlace = destination.Equals("") ? subAreaConnection.destination : destination;
            string targetPortal = portalTo.Equals("") ? subAreaConnection.portalTo : portalTo;
            string[] targetUnlock = subAreaConnection.unlockPoint;
            string[] worldMapPointToUnlock = subAreaConnection.unlockWorldMapPoint;
            WwiseManager.WorldFX sfx = subAreaConnection.SFX;

            if (!unlockByTrigger && targetUnlock.Length > 0)
                EventManager.Instance.TriggerEvent(new UnlockCityPointsEvent(targetUnlock));

            if (!unlockByTrigger && worldMapPointToUnlock.Length > 0)
                EventManager.Instance.TriggerEvent(new UnlockWorldMapPointEvent(worldMapPointToUnlock));

            if (AreaController.Instance.PlayingCutscene == "")
           		player.GetComponent<PlayerController>().ControllerState = PlayerController.PlayerControllerState.Sequence;
			else
				player.GetComponent<PlayerController>().ControllerState = PlayerController.PlayerControllerState.Cutscene;
            
            //Debug.Log(targetRoom + " " + targetPortal);
            if (AreaController.Instance.IsSameArea(targetPlace))
            {
                AreaController.Instance.MoveToSubArea(player, targetPlace, targetPortal, targetUnlock, sfx);
            }
            else
            {
                //player.GetComponent<WorldMapPlayerController>().controllerState = WorldMapPlayerController.WorldMapPlayerControllerState.Transition;
                AreaController.Instance.ChangeArea(player, targetPlace, targetPortal);
            }
        }
    }

    public void SetCameraVerticalFOV()
    {
        foreach (Camera c in cameras)
        {
            if (c.tag == "SecondaryCamera")
                c.fieldOfView = ConvertToVerticalFOV(subArea.secondaryHorizontalFOV);
            else
                c.fieldOfView = ConvertToVerticalFOV(subArea.mainHorizontalFOV);
        }
    }

    public float ConvertToVerticalFOV(float hFOV)
    {
        float invertedRatio = (float)Screen.height / (float)Screen.width;
        float equation1 = Mathf.Tan(hFOV * Mathf.Deg2Rad / 2) * invertedRatio;
        float vFOV = 2 * Mathf.Atan(equation1) * Mathf.Rad2Deg;

        return vFOV;
    }
}
