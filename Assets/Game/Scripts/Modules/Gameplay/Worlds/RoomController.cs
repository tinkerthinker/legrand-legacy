﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


public class RoomController : SubAreaController
{
    private PortalTrigger[]         _portalTriggers;
    private WorldMapPortalTrigger[] _worldMapPortalTriggers;
    //private EncounterTrigger[]      _encounterTriggers;
    private MonsterTrigger[]        _monsterTriggers;
    private SequenceTrigger[]       _sequenceTriggers;
    private TerrainTrigger[]        _terrainTriggers;
    private TreasureTrigger[]       _treasureTriggers;
    private DialogTrigger[]         _dialogTriggers;
    private QueryTrigger[]          _queryTriggers;
    private NPCBehaviour[]          _roamingNPCs;
    
    private WorldCityInfo            _worldMapCityInfo;
    
    private List<GameObject>        _dynamicGameObjects = new List<GameObject>();
    private List<SpriteRenderer>    _portalIndicators = new List<SpriteRenderer>();
    public  List<ParticleSystem>    footParticles = new List<ParticleSystem>();

    //private int unloadDungeonCounter = 0;
    private float timer = 0;
    private bool timerStarted = false;

    public bool Initialized = false;

    protected override void Awake()
    {
        base.Awake();

        _portalTriggers = GetComponentsInChildren<PortalTrigger>(true);

        _worldMapPortalTriggers = GetComponentsInChildren<WorldMapPortalTrigger>(true);

        //_encounterTriggers = GetComponentsInChildren<EncounterTrigger>(true);
        //foreach (EncounterTrigger e in _encounterTriggers)
        //    e.OnTrigger += OnEncounterAreaTriggered;

        _monsterTriggers = GetComponentsInChildren<MonsterTrigger>(true);
        
        _sequenceTriggers = GetComponentsInChildren<SequenceTrigger>(true);

        _treasureTriggers = GetComponentsInChildren<TreasureTrigger>(true);

        _dialogTriggers = GetComponentsInChildren<DialogTrigger>(true);

        _queryTriggers = GetComponentsInChildren<QueryTrigger>(true);

        _roamingNPCs = GetComponentsInChildren<NPCBehaviour>(true);

        ParticleSystem[] particles = GetComponentsInChildren<ParticleSystem>(true);
        foreach (ParticleSystem p in particles)
        {
            if (p.tag == "FootParticle")
                footParticles.Add(p);
        }

        _worldMapCityInfo = LegrandUtility.GetComponentInChildren<WorldCityInfo>(this.gameObject);
        
        foreach (string name in subArea.dynamicObjectsName)
        {
            GameObject obj = transform.Search(name).gameObject;
            if (obj && !_dynamicGameObjects.Contains(obj)) _dynamicGameObjects.Add(obj);
        }

        SpriteRenderer[] sprites = GetComponentsInChildren<SpriteRenderer>(true);
        foreach(SpriteRenderer sr in sprites)
        {
            if (sr.tag == "PortalIndicator")
                _portalIndicators.Add(sr);
        }

        ResetMonsterState();
    }

    void RoomLoaded()
    {
        AreaController.OnSubAreaLoaded -= RoomLoaded;
        SetCameraVerticalFOV();
        RenderPortalIndicators(AreaController.Instance.portalIndicatorActive);
        CheckRoomChanges();
    }

    void RoomUnloaded()
    {
        AreaController.OnSubAreaUnloaded -= RoomUnloaded;
        ResetNPCState();
        if (AreaController.Instance.CurrAreaData.areaType == Area.AreaTypes.Dungeon)
        {
            //unloadDungeonCounter++;
            print("unload room");
            ResetMonsterState();
        }
    }

	void OnDestroy()
	{
        AreaController.OnSubAreaLoaded -= RoomLoaded;
        AreaController.OnSubAreaUnloaded -= RoomUnloaded;

        foreach (PortalTrigger trigger in _portalTriggers)
			if (trigger.CheckListener ()) trigger.OnTrigger -= OnPortalTriggered;

        foreach (WorldMapPortalTrigger trigger in _worldMapPortalTriggers)
        { 
            if (trigger.CheckListener())
            {
                trigger.OnTrigger -= OnWorldMapPortalTriggered;
                trigger.OffTrigger -= OffWorldMapPortalTriggered;
            }
        }

        foreach (SequenceTrigger trigger in _sequenceTriggers) 
			if(trigger.CheckListener()) trigger.OnTrigger -= OnSequenceTriggered;

		//foreach(EncounterTrigger trigger in _encounterTriggers)
		//	if(trigger.CheckListener()) trigger.OnTrigger -= OnEncounterAreaTriggered;

		foreach(MonsterTrigger trigger in _monsterTriggers)
			if(trigger.CheckListener()) trigger.OnTrigger -= OnMonsterEncounterTriggered;
			
		foreach(TreasureTrigger trigger in _treasureTriggers)
			if(trigger.CheckListener()) trigger.OnTrigger -= OnTreasureTaken;

        foreach (DialogTrigger trigger in _dialogTriggers)
            if (trigger.CheckListener()) trigger.OnTrigger -= OnDialogTriggered;

        foreach (QueryTrigger trigger in _queryTriggers)
            if (trigger.CheckListener()) trigger.OnTrigger -= OnQueryTriggered;
    }

    void OnEnable()
    {
        AreaController.OnSubAreaLoaded += RoomLoaded;
        AreaController.OnSubAreaUnloaded += RoomUnloaded;

        EventManager.Instance.AddListener<StartTimerEvent>(StartZoomOutTimer);
        EventManager.Instance.AddListener<FirstStep>(StartZoomIn);

        if ((!AreaController.Instance.DisableBGM && subArea.BGM != WwiseManager.BGM.NONE) && (WwiseManager.Instance.CurrentBGM == WwiseManager.BGM.NONE || WwiseManager.Instance.CurrentBGM != subArea.BGM) && PartyManager.Instance.State.currPhase != (int)PartyManager.StoryPhase.Prolog && AreaController.Instance.active)
        {
            WwiseManager.Instance.PlayBG(subArea.BGM);
            //SoundManager.PlayBackgroundMusic(AreaController.Instance.CurrClusterData.BGM,true);
        }

        if (Initialized)
            SetTriggersActive(true);

        GlobalGameStatus.Instance.SetCurrRoomSafety(IsRoomSafe());
    }

    void OnDisable()
    {
        AreaController.OnSubAreaLoaded -= RoomLoaded;
        AreaController.OnSubAreaUnloaded -= RoomUnloaded;

        if (EventManager.Instance)
        {
            EventManager.Instance.RemoveListener<StartTimerEvent>(StartZoomOutTimer);
            EventManager.Instance.RemoveListener<FirstStep>(StartZoomIn);
        }
        SetCameraVerticalFOV();
    }

    void StartZoomOutTimer(StartTimerEvent e)
    {
        if (!timerStarted && subArea.secondaryHorizontalFOV > 0)
            StartCoroutine("TimerRoutine", e.timer);
            //StartCoroutine(TimerRoutine(e.timer));
    }
    
    void StartZoomIn(FirstStep e)
    {
        if(timerStarted && subArea.secondaryHorizontalFOV > 0)
        {
            StopCoroutine("TimerRoutine");
            timerStarted = false;
            CameraPanning cp = cameras[0].GetComponent<CameraPanning>();
            cp.ZoomIn(ConvertToVerticalFOV(subArea.mainHorizontalFOV));
        }
    }

    IEnumerator TimerRoutine(float threshold)
    {
        timer = 0f;
        timerStarted = true;
        CameraPanning cp = cameras[0].GetComponent<CameraPanning>();
        while (timer <= threshold)
        {
            if (AreaController.Instance.CurrPlayer.GetComponent<PlayerController>().ControllerState != PlayerController.PlayerControllerState.Normal)
            {
                timerStarted = false;
                yield break;
            }
            
            timer += Time.deltaTime;
            if (timer >= threshold)
            {
                cp.ZoomOut(ConvertToVerticalFOV(subArea.secondaryHorizontalFOV));
            }

            yield return null;
        }
    }

    public void ResetMonsterState()
    {
        List<Transform> startingPositions = new List<Transform>();

        foreach (MonsterTrigger m in _monsterTriggers)
        {
			if(!m.CheckTriggerHasListener()) m.OnTrigger += OnMonsterEncounterTriggered;

            startingPositions.Clear();

            if (!m.gameObject.activeSelf /*&& unloadDungeonCounter == 2*/)
            {
                m.gameObject.SetActive(m.EnableMonster());
                //unloadDungeonCounter = 0;
            }

            if (!string.IsNullOrEmpty(m.monsterArea))
            {
                Transform navArea = transform.FindChild(m.monsterArea);
                foreach (Transform t in navArea)
                {
                    startingPositions.Add(t);
                }

                if (startingPositions.Count > 0)
                {
                    m.transform.position = startingPositions[LegrandUtility.Random(0, startingPositions.Count)].transform.position;
                    Ray r = new Ray(m.transform.position, -m.transform.up);
                    RaycastHit hit;
                    if(Physics.Raycast(r, out hit, LayerMask.NameToLayer("Ground")))
                    m.transform.position = new Vector3(m.transform.position.x, hit.point.y + 1f, m.transform.position.z);  
                }
                m.RestartAI();
            }
            else
                Debug.Log(m + " has no monsterArea specified");

            m.transform.rotation = Quaternion.Euler(m.transform.rotation.eulerAngles.x, LegrandUtility.Random(0f, 359f), m.transform.rotation.eulerAngles.z);
        }
    }

    public void ResetNPCState()
    {
        foreach (NPCBehaviour npc in _roamingNPCs)
        {
            if (!string.IsNullOrEmpty(npc.roamArea))
            {
                //Transform roamArea = transform.FindChild(npc.roamArea);
                //foreach (Transform t in roamArea)
                //{
                //    // Position and rotation too
                //    npc.transform.position = t.position;
                //    npc.transform.rotation = Quaternion.Euler(npc.transform.rotation.eulerAngles.x, Random.Range(0f, 359f), npc.transform.rotation.eulerAngles.z);
                //}
                npc.RestartAI();
            }
        }
    }

    public void SetTriggersActive(bool active)
    {
        foreach (SequenceTrigger s in _sequenceTriggers)
        {
            bool found = false;
            int i = 0;
            while (!found && i < PartyManager.Instance.StoryProgression.ActiveEventID.Count)
            {
				QuestWorldEvents worldEvent;
				if (LegrandBackend.Instance.WorldEventData.TryGetValue (PartyManager.Instance.StoryProgression.ActiveEventID[i], out worldEvent)) 
				{
					if (LegrandUtility.CompareString (worldEvent.WorldTriggerID, s.TriggerID)) 
					{
						s.EventData = new StoryData ();
						s.EventData.ID = PartyManager.Instance.StoryProgression.ActiveEventID [i];
						s.EventData.Scene = new QuestWorldEvents (worldEvent.WorldTriggerID, worldEvent.WorldEvent);
						found = true;
					}
				}
				i++;
			}

			if (found)
			{
				if (s.gameObject.GetComponent<Collider>() != null)
				{
					if (s.OnTrigger == null) s.OnTrigger += OnSequenceTriggered;
				    s.gameObject.GetComponent<Collider>().enabled = active;
				}
			}
		
        }		

        foreach (TreasureTrigger t in _treasureTriggers)
        {
            bool found = false;
            int i = 0;
            while (!found && i < PartyManager.Instance.WorldInfo.TakenTreasures.Count)
            {
				if (LegrandUtility.CompareString(t.ID, PartyManager.Instance.WorldInfo.TakenTreasures[i]))
                {
                    found = !found;
                }
                i++;
            }

            if (!found)
            {
               if (t.OnTrigger == null) t.OnTrigger += OnTreasureTaken;
                t.gameObject.GetComponent<Collider>().enabled = active;
            }
            else
            {
                t.gameObject.SetActive(false);
            }
        }

        foreach (PortalTrigger p in _portalTriggers)
        {
            if (p.gameObject.GetComponent<Collider>() != null)
            {
                if (p.OnTrigger == null) p.OnTrigger += OnPortalTriggered;

                if (p.gameObject.GetComponent<SequenceTrigger>() != null && p.gameObject.GetComponent<SequenceTrigger>().OnTrigger != null)
                {
                    p.OnTrigger -= OnPortalTriggered;
                }
                p.gameObject.GetComponent<Collider>().enabled = active;
            }
            //p.gameObject.SetActive(active);
        }

        foreach(WorldMapPortalTrigger wp in _worldMapPortalTriggers)
        {
            if (wp.gameObject.GetComponent<Collider>() != null)
            {
                if (wp.OnTrigger == null && wp.OffTrigger == null)
                {
                    wp.OnTrigger += OnWorldMapPortalTriggered;
                    wp.OffTrigger += OffWorldMapPortalTriggered;
                }
                if (wp.gameObject.GetComponent<SequenceTrigger>() != null && wp.gameObject.GetComponent<SequenceTrigger>().OnTrigger != null)
                {
                    wp.OnTrigger -= OnWorldMapPortalTriggered;
                    wp.OffTrigger -= OffWorldMapPortalTriggered;
                }
                wp.gameObject.GetComponent<Collider>().enabled = active;
            }
        }

        //foreach (EncounterTrigger e in _encounterTriggers)
        //{
        //    if (e.gameObject.GetComponent<Collider>() != null)
        //        e.gameObject.GetComponent<Collider>().enabled = active;
        //    //p.gameObject.SetActive(active);
        //}

        foreach (DialogTrigger d in _dialogTriggers)
        {
			d.OnTrigger = null;
			if (d.gameObject.GetComponent<Collider>() != null)
            {
                if (d.OnTrigger == null) d.OnTrigger += OnDialogTriggered;

                if (d.gameObject.GetComponent<SequenceTrigger>() != null && d.gameObject.GetComponent<SequenceTrigger>().OnTrigger != null)
                {
                    d.OnTrigger -= OnDialogTriggered;
                }
                d.gameObject.GetComponent<Collider>().enabled = active;
            }
        }

        foreach(QueryTrigger q in _queryTriggers)
        {
            if (q.gameObject.GetComponent<Collider>() != null)
            {
                if (q.OnTrigger == null) q.OnTrigger += OnQueryTriggered;

                if (q.gameObject.GetComponent<SequenceTrigger>() != null && q.gameObject.GetComponent<SequenceTrigger>().OnTrigger != null)
                {
                    q.OnTrigger -= OnQueryTriggered;
                }
                q.gameObject.GetComponent<Collider>().enabled = active;
            }
        }


        Initialized = true;
    }
    
    void OnPortalTriggered(WorldTrigger trigger, GameObject go)
    {
        trigger.OnTrigger -= OnPortalTriggered;
        trigger.Disable();

        PortalTrigger pt = (PortalTrigger)trigger;
        
        List<string> worldEventID = PartyManager.Instance.StoryProgression.ActiveEventID;
		bool found = false;
		for (int i=0; i<worldEventID.Count && !found;i++) {
			
			QuestWorldEvents worldEvent;
			if (LegrandBackend.Instance.WorldEventData.TryGetValue (worldEventID [i], out worldEvent)) 
			{
				if(LegrandUtility.CompareString(worldEvent.WorldTriggerID, trigger.TriggerID))
				{
					found = true;
					AreaController.Instance.PlayingCutscene = worldEvent.WorldEvent;
					PartyManager.SceneFinished (worldEventID [i]);
				}	
			}

		}
        if (pt.triggeredPortalWalking)
            StartCoroutine(go.GetComponent<PlayerController>().PortalWalking(trigger));

        if (pt.UnlockAndChangeDestinationByTrigger)
        {
            ResponsiveQuery responsiveQuery = new ResponsiveQuery();
            responsiveQuery.Add("Concept", LegrandUtility.GetUniqueSymbol("OnEnterPortal"));
            responsiveQuery.Add("PortalID", LegrandUtility.GetUniqueSymbol(pt.TriggerID));
            for (int i = 0; i < PartyManager.Instance.WorldInfo.LatestFacts.Count; i++)
            {
                responsiveQuery.Add(PartyManager.Instance.WorldInfo.LatestFacts[i].Name, PartyManager.Instance.WorldInfo.LatestFacts[i].Value);
            }
            if (responsiveQuery.Match())
            {
                RuleResult result = DynamicSystemController._Instance.RuleDB.GetLatestResult();
                string[] rawData = result.ResponseName.Split('|');
                string[] WorldMapPoints = rawData[0].Split(',');
                string[] CityPoints = rawData[1].Split(',');
                string[] destination = rawData[2].Split(',');

                EventManager.Instance.TriggerEvent(new UnlockWorldMapPointEvent(WorldMapPoints));
                EventManager.Instance.TriggerEvent(new UnlockCityPointsEvent(CityPoints));

                if(!destination[0].Equals("") && !destination[1].Equals(""))
                    MovePlayer(trigger.name, go, pt.UnlockAndChangeDestinationByTrigger,destination[0], destination[1]);
                else
                    MovePlayer(trigger.name, go, pt.UnlockAndChangeDestinationByTrigger);
            }
            else
            {
                MovePlayer(trigger.name, go, pt.UnlockAndChangeDestinationByTrigger);
            }
        }
        else
        {
            MovePlayer(trigger.name, go, pt.UnlockAndChangeDestinationByTrigger);
        }
    }


    void OnWorldMapPortalTriggered(WorldTrigger trigger, GameObject go)
    {
        //print("City Description Turned On");
        string destOnTrigger = subArea.GetConnectionFrom(trigger.name).destination;
        Area areaOnTrigger = AreaController.Instance.GetAreaDataBySubArea(destOnTrigger);

        _worldMapCityInfo.Set(areaOnTrigger.areaName, areaOnTrigger.areaName, areaOnTrigger.areaDescription);
    }

    void OffWorldMapPortalTriggered(WorldTrigger trigger, GameObject go)
    {
        //print("City Description Turned Off");
        _worldMapCityInfo.Disable();
    }

    public PortalTrigger GetPortalTrigger(string name)
    {
        foreach (PortalTrigger p in _portalTriggers)
            if (p.name == name) return p;

        return null;
    }

    public WorldMapPortalTrigger GetWorldMapPortalTrigger(string name)
    {
        foreach (WorldMapPortalTrigger wp in _worldMapPortalTriggers)
            if (wp.name == name) return wp;

        return null;
    }

    public void UpdateMonsterTrigger()
    {
        foreach (MonsterTrigger m in _monsterTriggers)
        {
            if (m == null)
            {
                _monsterTriggers = _monsterTriggers.Where(val => val != m).ToArray();
                //print ("NULL DETECTED");
            }
            else
            {
                m.StartAI();
                //print ("NULL NOT DETECTED");
            }
        }
    }

    //void OnEncounterAreaTriggered(WorldTrigger trigger, GameObject go)
    //{
    //    EncounterTrigger e = (EncounterTrigger)trigger;
    //    EventManager.Instance.TriggerEvent(new NewEncounterPartiesEvent(e.EncounterPartiesId));
    //}

    void OnMonsterEncounterTriggered(WorldTrigger trigger, GameObject go)
    {
        trigger.OnTrigger -= OnMonsterEncounterTriggered;

        WwiseManager.Instance.StopAll();
        WwiseManager.Instance.PlaySFX(WwiseManager.BattleFX.Battle_transition_wind);

        // Monster position relative to player
        Vector3 monsterRelativePoint = go.transform.InverseTransformPoint(trigger.transform.position);
        monsterRelativePoint.y = 0f;
        // Player position relative to monster
        Vector3 playerRelativePoint = trigger.transform.InverseTransformPoint(go.transform.position);
        playerRelativePoint.y = 0f;

        // Compare monster position to player's forward
        float dot = Vector3.Dot(monsterRelativePoint.normalized, Vector3.forward);
        // Compare player position to monster's forward
        float dot2 = Vector3.Dot(playerRelativePoint.normalized, Vector3.forward);

        MonsterEncounterEvent.AmbushStat ambush = MonsterEncounterEvent.AmbushStat.Neutral;
        // If monster is in front of player and player is facing it
        if ((dot >= 0.5f && dot <= 1f) && (dot2 <= -0.5f && dot2 >= -1f))
        {
           ambush = MonsterEncounterEvent.AmbushStat.Player;
        }
        // if player is in front of monster and monster is facing him
        else if ((dot2 >= 0.5f && dot2 <= 1f) && (dot <= -0.5f && dot >= -1f))
        {
            ambush = MonsterEncounterEvent.AmbushStat.Enemy;
        }

        EventManager.Instance.TriggerEvent(new MonsterEncounterEvent(trigger.gameObject, ambush));
        foreach (MonsterTrigger m in _monsterTriggers)
            m.StopAI();
    }

    void OnSequenceTriggered(WorldTrigger trigger, GameObject go)
    {
        SequenceTrigger s = (SequenceTrigger)trigger;
        s.OnTrigger -= OnSequenceTriggered;
        s.Play();
    }
      

    void OnTreasureTaken(WorldTrigger trigger, GameObject go)
    {
        trigger.OnTrigger -= OnTreasureTaken;
        TreasureTrigger t = (TreasureTrigger)trigger;

		t.TakeAllItems ();
//		foreach (VectorStringInteger Treasure in t.TreasureItems)
//        {
//            PartyManager.Instance.Inventory.AddItems(LegrandBackend.Instance.ItemData[Treasure.x], Treasure.y);
//        }
//		PartyManager.Instance.WorldInfo.TakenTreasures.Add(t.ID);
//        if (t.DestroyAfterTrigger) t.gameObject.SetActive(false);
    }

    void OnDialogTriggered(WorldTrigger trigger, GameObject go)
    {
        DialogTrigger d = (DialogTrigger)trigger;

        // Pop the dialog out
		d.StartDialog();
    }

    void OnQueryTriggered(WorldTrigger trigger, GameObject go)
    {
        QueryTrigger q = (QueryTrigger)trigger;
        ResponsiveQuery query = new ResponsiveQuery();

        // Check if query trigger requires item to activate
        if(q.ItemsRequired.Count > 0)
        {
            for (int i = 0; i < q.ItemsRequired.Count; i++)
            {
                Item item = LegrandBackend.Instance.ItemData[q.ItemsRequired[i]];
                if (!PartyManager.Instance.Inventory.Contains(item))
                    return;
            }
        }

        foreach (Memory data in q.QueryList)
            query.Add(data.Name, data.Value);
        foreach (Fact data in PartyManager.Instance.WorldInfo.LatestFacts)
            query.Add(data.Name, data.Value);
        
        // if all query are passed, pop dialogue mark if query needs activation, otherwise execute the rules right away
        if(query.Match())
        {
            if(q.NeedActivate)
                PopUpUI.CallDialogueMarkPopUp(trigger.gameObject, true);
            else
                DynamicSystemController._Instance.ResponseDB.ExecuteResponse(DynamicSystemController._Instance.RuleDB.Storing());
        }
    }

    public void RenderPortalIndicators(bool active)
    {
        foreach(SpriteRenderer sp in _portalIndicators)
        {
            sp.enabled = active;
        }
    }

  //  public void SetupRoomByVersion()
  //  {
		//List<SubAreaInfo> dynamicSubAreaList = PartyManager.Instance.WorldInfo.DynamicSubArea;

  //      foreach(SubAreaInfo sai in dynamicSubAreaList)
  //      {
  //          if(sai.name == subArea.prefabName)
  //          {
  //              int index = System.Array.FindIndex(subArea.roomVersion, row => row.version == sai.version);

  //              if(_background != null)
  //                  _background.GetComponent<Renderer>().material = subArea.roomVersion[index].modification.material;

  //              foreach (GameObject objPrefab in _objectsByVersion)
  //              {
  //                  if (System.Array.Find(subArea.roomVersion[index].modification.gameObjects, item => item.name == objPrefab.name) != null)
  //                      objPrefab.SetActive(true);
  //                  else
  //                      objPrefab.SetActive(false);
  //              }

  //              break;
  //          }
  //      }
  //  }

    public bool IsRoomSafe()
    {
        if (_monsterTriggers.Length > 0)
            return false;

        return true;
    }

    public void CheckRoomChanges()
    {
        EventManager.Instance.TriggerEvent(new RefreshRoomEvent());
        Fact[] facts = PartyManager.Instance.WorldInfo.LatestFacts.FindAll(fact => LegrandUtility.CompareString(fact.Owner, subArea.prefabName)).ToArray();
        foreach (Fact f in facts)
        {
            foreach(GameObject go in _dynamicGameObjects)
            {
                if (f.ActiveGameObjects.Find(item => LegrandUtility.CompareString(item, go.name)) != null)
                    go.SetActive(true);
                else if (f.InactiveGameObjects.Find(item => LegrandUtility.CompareString(item, go.name)) != null)
                    go.SetActive(false);
            }
        }
    }

#if UNITY_EDITOR
    void Update()
    {
        int input = -1;
        if (TeamUtility.IO.InputManager.anyKeyDown)
        {
            if (TeamUtility.IO.InputManager.GetKeyDown(KeyCode.Keypad0))
                input = 0;
            else if (TeamUtility.IO.InputManager.GetKeyDown(KeyCode.Keypad1))
                input = 1;
            else if (TeamUtility.IO.InputManager.GetKeyDown(KeyCode.Keypad2))
                input = 2;
            else if (TeamUtility.IO.InputManager.GetKeyDown(KeyCode.Keypad3))
                input = 3;
            else if (TeamUtility.IO.InputManager.GetKeyDown(KeyCode.Keypad4))
                input = 4;
            else if (TeamUtility.IO.InputManager.GetKeyDown(KeyCode.Keypad5))
                input = 5;
            else if (TeamUtility.IO.InputManager.GetKeyDown(KeyCode.Keypad6))
                input = 6;
            else if (TeamUtility.IO.InputManager.GetKeyDown(KeyCode.Keypad7))
                input = 7;
            else if (TeamUtility.IO.InputManager.GetKeyDown(KeyCode.Keypad8))
                input = 8;
            else if (TeamUtility.IO.InputManager.GetKeyDown(KeyCode.Keypad9))
                input = 9;
        }

        if (input >= 0 && input < _portalTriggers.Length)
            _portalTriggers[input].OnTrigger(_portalTriggers[input], AreaController.Instance.CurrPlayer);
        
    }

    void OnGUI()
    {
        int w = Screen.width, h = Screen.height;

        GUIStyle style = new GUIStyle();

        Rect rect = new Rect(20, 50, w, h * 2 / 100);
        style.alignment = TextAnchor.UpperLeft;
        style.fontSize = h * 2 / 100;
        style.normal.textColor = Color.white;
        string text = string.Empty;
        if(_portalTriggers != null)
        {
            for (int i=0;i<_portalTriggers.Length;i++)
                text += i+". "+_portalTriggers[i].gameObject.name + "\n";
        }
        GUI.Label(rect, text, style);
    }
#endif
}
