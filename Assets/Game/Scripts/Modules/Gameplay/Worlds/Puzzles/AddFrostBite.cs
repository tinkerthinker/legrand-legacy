﻿using UnityEngine;
using System.Collections;

public class AddFrostBite : MonoBehaviour
{
    public float Delay;
    public FrostBite.FrostEffect[] Effects;

    void OnEnable()
    {
        if(transform.parent != null && GetComponentInParent<FrostBite>() == null)
        {
            FrostBite frostBite = transform.parent.gameObject.AddComponent<FrostBite>();
            frostBite.Effects = Effects;
            frostBite.StartFrostBite(Delay);
        }
    }
}
