﻿using UnityEngine;
using System.Collections;

public class Quicksand : MonoBehaviour
{    
    [SerializeField]
    private float verticalSpeed;
    [SerializeField]
    private float horizontalSpeed;
    
    private float time;
    private Collider sphereCollider;

	// Use this for initialization
	void Start () {
        sphereCollider = GetComponent<Collider>();
	}

    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "PartyLeader")
        {
            col.GetComponent<Rigidbody>().useGravity = false;
        }
    }

    void OnTriggerStay(Collider col)
    {        
        if(col.tag == "PartyLeader")
        {
            //col.transform.position = Vector3.MoveTowards(col.transform.position, transform.position, verticalSpeed * Time.deltaTime);
            col.transform.position = Vector3.MoveTowards(col.transform.position, new Vector3(col.transform.position.x, transform.position.y, col.transform.position.z), verticalSpeed * Time.deltaTime);
            col.transform.position = Vector3.MoveTowards(col.transform.position, new Vector3(transform.position.x, col.transform.position.y, transform.position.z), horizontalSpeed * Time.deltaTime);
        }
    }

    void OnTriggerExit(Collider col)
    {
        if(col.tag == "PartyLeader")
        {
            col.GetComponent<Rigidbody>().useGravity = true;
        }
    }
}
