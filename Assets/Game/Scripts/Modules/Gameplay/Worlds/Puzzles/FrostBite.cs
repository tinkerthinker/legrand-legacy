﻿using UnityEngine;
using System.Collections;

public class FrostBite : MonoBehaviour
{
    [System.Serializable]
    public class FrostEffect
    {
        public float Time;
        public bool DamageAtEnd;
        public bool ForceWalk;
        public float DamageValue;
        public float FadeTime;
        [Range(0f, 255f)]
        public float AlphaColor;
    }
    public FrostEffect[] Effects;
    int CurrentIndex;
    PlayerController controller;
    float _runSpeed;
    float _walkSpeed;
    void Awake()
    {
        controller = AreaController.Instance.CurrPlayer.GetComponent<PlayerController>();
        _runSpeed = controller.runSpeed;
        _walkSpeed = controller.walkSpeed;
    }

    public void StartFrostBite(float Delay = 0f, int index = 0)
    {
        StartCoroutine(StartEffect(Delay,index));
    }

    IEnumerator StartEffect(float Delay, int index = 0)
    {
        yield return new WaitForSeconds(Delay);

        CurrentIndex = index;
        DoEffect();
    }

    void DoDamage()
    {
        if (!AreaController.Instance.insideHealPoint)
        {
            EventManager.Instance.TriggerEvent(new FrostBiteEvent(Effects[CurrentIndex], true));
            if (CurrentIndex >= Effects.Length - 1)
                Invoke("DoEffect", Effects[CurrentIndex].Time);
        }
    }

    void DoEffect()
    {
        ExecuteEffect();

        if (Effects[CurrentIndex].DamageAtEnd)
            Invoke("DoDamage", Effects[CurrentIndex].Time);
        else
            Invoke("ChangeEffect", Effects[CurrentIndex].Time);
    }

    void ChangeEffect()
    {
        if(CurrentIndex < Effects.Length - 1)
            CurrentIndex++;
        
            DoEffect();
    }

    public void Stop()
    {
        CurrentIndex = 0;

        CancelInvoke("DoDamage");
        CancelInvoke("ChangeEffect");
        CancelInvoke("DoEffect");

        controller.walkSpeed = _walkSpeed;
        controller.runSpeed = _runSpeed;

        controller.ForceWalk = false;
    }

    void ExecuteEffect()
    {
        if (!AreaController.Instance.insideHealPoint)
        {
            EventManager.Instance.TriggerEvent(new FrostBiteEvent(Effects[CurrentIndex],false));
        }
    }
}
