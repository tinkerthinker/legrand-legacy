﻿using UnityEngine;
using System.Collections;

public class SnowSurface : MonoBehaviour
{
    [SerializeField]
    private float horizontalSpeed;
    [SerializeField]
    private GameObject Point;
    private GameObject Player;
    bool active = false;

    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "PartyLeader")
        {
            active = true;
            Player = col.gameObject;
        }
    }

    void Update()
    {
        if(active && Player != null && Point.transform.position.y < Player.transform.position.y)
        {
            Player.transform.position = Vector3.MoveTowards(Player.transform.position, new Vector3(Point.transform.position.x, Point.transform.position.y, Point.transform.position.z), horizontalSpeed * Time.deltaTime);
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.tag == "PartyLeader")
        {
            active = false;
        }
    }
}
