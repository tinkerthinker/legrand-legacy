﻿using UnityEngine;
using System.Collections;

public class QuicksandAnimation : MonoBehaviour
{
    public float sandSpeed = 0.15f;
    private float offset;

	// Update is called once per frame
	void Update () {
        offset += sandSpeed * Time.deltaTime;
        GetComponent<Renderer>().material.SetTextureOffset("_MainTex", new Vector2(offset, 0));
    }
}
