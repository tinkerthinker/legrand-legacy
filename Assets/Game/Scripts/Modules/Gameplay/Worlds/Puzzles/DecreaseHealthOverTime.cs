﻿using UnityEngine;
using System.Collections.Generic;
using Legrand.core;

public class DecreaseHealthOverTime : MonoBehaviour {

    public float initialCooldown = 20f;
    public float nextCooldown = 10f;
    public float decreasePercentage = 1;
    
	void OnEnable ()
    {
        InvokeRepeating("DecreaseHealth", initialCooldown, nextCooldown);
	}

    void DecreaseHealth()
    {
        if (!AreaController.Instance.insideHealPoint)
        {
            WwiseManager.Instance.PlaySFX(WwiseManager.WorldFX.Poisoned);

            List<MainCharacter> partyList = PartyManager.Instance.CharacterParty;

            EventManager.Instance.TriggerEvent(new PoisonEvent());

            for (int i = 0; i < partyList.Count; i++)
            {
                float maxHealth = partyList[0].GetHealth().MaxValue;
                float decreaseAmount = maxHealth * (decreasePercentage / 100);

                partyList[i].GetHealth().Decrease(decreaseAmount);

                if (partyList[i].GetHealth().Value <= 1)
                    partyList[i].GetHealth().ChangeCurrentValue(1);
            }
        }
    }

    void OnDisable()
    {
        CancelInvoke();
    }
}
