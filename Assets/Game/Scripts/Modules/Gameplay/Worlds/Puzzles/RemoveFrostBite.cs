﻿using UnityEngine;
using System.Collections;

public class RemoveFrostBite : MonoBehaviour
{
    void OnEnable()
    {
        Remove();
    }
    void Remove()
    {
        FrostBite frostBite = GetComponentInParent<FrostBite>();
        if (frostBite != null)
        {
            frostBite.Stop();
            Destroy(frostBite);
        }
    }
}
