﻿    using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class QueryTrigger : WorldTrigger
{
	public List<Memory> QueryList;
    public List<string> ItemsRequired;

    void OnTriggerEnter(Collider coll)
    {
        if (coll.tag == "Interaction" && OnTrigger != null)
        {
            _ObjectOnTrigger = coll.gameObject;
             OnTrigger(this, _ObjectOnTrigger);

            if (NeedActivate)
            {
                _MarkPopUpHelper = coll.GetComponent<DialogueHelperReference>().helper;
                PopUpUI.CallDialogueMarkPopUp(_MarkPopUpHelper, true);
                EventManager.Instance.AddListener<Interact>(QueryActivated);
            }
        }
    }

    void OnTriggerExit(Collider coll)
    {
        if (coll.tag == "Interaction" && OnTrigger != null)
        {
            if (OffTrigger != null)
                OffTrigger(this, _ObjectOnTrigger);

            _ObjectOnTrigger = null;

            if (NeedActivate)
            {
                PopUpUI.CallDialogueMarkPopUp(null, false);
                _MarkPopUpHelper = null;
                EventManager.Instance.RemoveListener<Interact>(QueryActivated);
            }
        }
    }

    void QueryActivated(Interact e)
    {
        EventManager.Instance.RemoveListener<Interact>(QueryActivated);
        PopUpUI.CallDialogueMarkPopUp(null, false);
        DynamicSystemController._Instance.ResponseDB.ExecuteResponse(DynamicSystemController._Instance.RuleDB.Storing());
    }
}
