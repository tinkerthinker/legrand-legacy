﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TreasureTrigger : WorldTrigger 
{
	[ContextMenuItem("Randomize ID", "Randomize")] [Tooltip("Right Click To Select Randomize")]
	public string ID;

	[SerializeField] [Header("X for item ID and Y for item amount")]
	public List<VectorStringInteger> TreasureItems;

	private bool itemTaken;
	
	void Randomize()
	{
		ID = System.Guid.NewGuid ().ToString ();
	}

	void OnTriggerEnter(Collider coll)
	{
        if(coll.tag == "Interaction" && OnTrigger != null)
        {
            _ObjectOnTrigger = coll.gameObject;

            if (!NeedActivate)
            {
                OnTrigger(this, _ObjectOnTrigger);
            }
            else
            {
                _MarkPopUpHelper = coll.GetComponent<DialogueHelperReference>().helper;
                //if (helper != null)
                PopUpUI.CallDialogueMarkPopUp(_MarkPopUpHelper, true);
                //else
                   //PopUpUI.CallDialogueMarkPopUp(gameObject, true);
                EventManager.Instance.AddListener<Interact>(TakeTreasure);
            }
        }		
	}

    void OnDisable()
    {
        if (_MarkPopUpHelper != null)
        {
            PopUpUI.CallDialogueMarkPopUp(null, false);
            _MarkPopUpHelper = null;
        }

        if (EventManager.Instance)
            EventManager.Instance.RemoveListener<Interact>(TakeTreasure);
        else
            Debug.LogError("Event manager tidak ada");
    }

    void OnTriggerExit(Collider coll)
    {
        if (coll.tag == "Interaction")
        {
            if (NeedActivate)
            {
				//if (helper != null)
                    PopUpUI.CallDialogueMarkPopUp(null, false);
                //else
                    //PopUpUI.CallDialogueMarkPopUp(gameObject, false);
                EventManager.Instance.RemoveListener<Interact>(TakeTreasure);
                _MarkPopUpHelper = null;
            }

            _ObjectOnTrigger = null;
        }	
	}
	
	void TakeTreasure(Interact i)
	{
		Activate ();
	}

	public void TakeAllItems()
	{
		StartCoroutine (ReceiveItemOneByOne ());
	}

	IEnumerator ReceiveItemOneByOne()
	{
		foreach (VectorStringInteger Treasure in TreasureItems)
		{
			itemTaken = false;
			PartyManager.Instance.Inventory.AddItems(LegrandBackend.Instance.ItemData[Treasure.x], Treasure.y);
			WwiseManager.Instance.PlaySFX (WwiseManager.WorldFX.Treasure);
			PopUpUI.CallNotifPopUp("Received <color=#ff0000>"+ Treasure.y +" "+LegrandBackend.Instance.ItemData[Treasure.x].Name+"</color>", ConfirmedTakenItem);
			while(!itemTaken)
			{
				yield return null;
			}
		}
		PartyManager.Instance.WorldInfo.TakenTreasures.Add(ID);
		if (DestroyAfterTrigger) gameObject.SetActive(false);
	}

	void ConfirmedTakenItem()
	{
		itemTaken = true;
	}
}
