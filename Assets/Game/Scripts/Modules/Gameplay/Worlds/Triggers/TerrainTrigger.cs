﻿using UnityEngine;
using System.Collections;

public class TerrainTrigger : WorldTrigger {
	public TerrainType Type;
    public ParticleSystem[] Particles;
    public GameObject TerrainPos;

    public TerrainTrigger AreaTerrain;

    void OnTriggerEnter(Collider coll)
    {
        _ObjectOnTrigger = coll.gameObject;
        OnNewTerrain(_ObjectOnTrigger);
        if (AreaTerrain != null)
            AreaTerrain.OnTriggerExit(coll);
    }

    void OnTriggerExit(Collider coll)
    {
        if (coll.gameObject == _ObjectOnTrigger)
        {
            OnTerrainExit();
            _ObjectOnTrigger = null;
            if(AreaTerrain != null)
                AreaTerrain.OnTriggerEnter(coll);
        }
    }

    void OnNewTerrain(GameObject go)
    {
        foreach (ParticleSystem p in Particles)
        {
            if(p != null)
                p.gameObject.SetActive(true);
        }
            
        if(go.GetComponent<StepListener>() != null)
        {
            go.GetComponent<StepListener>().ChangeTerrainPos(TerrainPos);
            go.GetComponent<StepListener>().ChangeTerrain(Type);
            go.GetComponent<StepListener>().SetupParticle(Particles);
        }
            
    }

    void OnTerrainExit()
    {
        foreach (ParticleSystem p in Particles)
        {
            if (p != null)
                p.gameObject.SetActive(false);
        }

    }
}
