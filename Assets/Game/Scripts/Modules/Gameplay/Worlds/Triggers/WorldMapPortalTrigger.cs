﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WorldMapPortalTrigger : WorldTrigger  {

    //public enum Facing
    //{
    //    Up,
    //    Down,
    //    Left,
    //    Right
    //}
    //public Facing arriveDirection;
    public Vector3 arriveOffset;

    void OnTriggerEnter(Collider coll)
	{
		if (coll.tag == "PartyLeader" && OnTrigger != null)
        {
            _ObjectOnTrigger = coll.gameObject;
            OnTrigger(this, _ObjectOnTrigger);

            if(NeedActivate)
                EventManager.Instance.AddListener<EnterTrigger>(EnterNewArea);
        }
	}
	
	void OnTriggerExit(Collider coll)
	{
        if(coll.tag == "PartyLeader" && OffTrigger != null)
        {
            OffTrigger(this, _ObjectOnTrigger);
            _ObjectOnTrigger = null;

            if(NeedActivate)
                EventManager.Instance.RemoveListener<EnterTrigger>(EnterNewArea);
        }
	}
    
    void EnterNewArea(EnterTrigger e)
    {
        EventManager.Instance.RemoveListener<EnterTrigger>(EnterNewArea);

		// Check if there's is any cutscene with this trigger
		List<string> worldEventID = PartyManager.Instance.StoryProgression.ActiveEventID;
		bool found = false;
		for (int i=0; i<worldEventID.Count && !found;i++) {

			QuestWorldEvents worldEvent;
			if (LegrandBackend.Instance.WorldEventData.TryGetValue (worldEventID [i], out worldEvent)) 
			{
				if(LegrandUtility.CompareString(worldEvent.WorldTriggerID, TriggerID))
				{
					found = true;
					AreaController.Instance.PlayingCutscene = worldEvent.WorldEvent;
					PartyManager.SceneFinished (worldEventID [i]);
				}	
			}

		}

        int index = PartyManager.Instance.WorldInfo.UnlockedWorldMapPoint.FindIndex(point => point.name == this.name);
        if (index > -1) PartyManager.Instance.WorldInfo.UnlockedWorldMapPoint[index].isVisited = true;

        AreaController.Instance.CurrRoom.MovePlayer(this.name, _ObjectOnTrigger);
        //AreaController.Instance.CurrRoom.MovePlayer(this.name, _ObjectOnTrigger.transform.parent.gameObject);
    }

    //public Vector3 GetRotation(Facing dir)
    //{
    //    Vector3 rot = Vector3.zero;

    //    return rot;

    //    switch (dir)
    //    {
    //        case Facing.Up:
    //        rot.Set(315, 0f, 0f);
    //        break;
    //        case Facing.Down:
    //        rot.Set(45f, 180f, 0f);
    //        break;
    //        case Facing.Left:
    //        rot.Set(0f, 270f, 45f);
    //        break;
    //        case Facing.Right:
    //        rot.Set(0f, 90f, 315f);
    //        break;
    //    }

    //    return rot;
    //}
}
