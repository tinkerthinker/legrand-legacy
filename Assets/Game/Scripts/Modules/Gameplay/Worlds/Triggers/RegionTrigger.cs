﻿using UnityEngine;
using System.Collections;

public class RegionTrigger : MonoBehaviour
{
    public string RegionName;

    void OnTriggerEnter(Collider coll)
    {
        if(AreaController.Instance != null)
            AreaController.Instance.CurrentRegion = RegionName;
    }
}
