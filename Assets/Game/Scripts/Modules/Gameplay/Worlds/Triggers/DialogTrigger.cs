﻿using UnityEngine;
using System.Collections;

public class DialogTrigger : WorldTrigger
{
	public VectorStringDouble[] Query;
    

	public void StartDialog()
	{
        ResponsiveQuery q = new ResponsiveQuery();
        q.Add("Concept", LegrandUtility.GetUniqueSymbol("OnDialogTriggerEnter"));
        q.Add("TriggerID", LegrandUtility.GetUniqueSymbol(TriggerID));
        foreach (Memory m in PartyManager.Instance.WorldInfo.LatestFacts)
            q.Add(m.Name, m.Value);

        if (q.Match())
        {
            GlobalGameStatus.Instance.StateStatus[(int)GlobalGameStatus.GameState.WorldDialog] = true;
            EventManager.Instance.AddListener<DialogWindowClosed>(DialogueEnd);
            EventManager.Instance.TriggerEvent(new TalkToPlayerEvent(false));
            LegrandUtility.StartDialogue(DynamicSystemController._Instance.RuleDB.GetLatestResult().ResponseName);
            DynamicSystemController._Instance.ResponseDB.ExecuteResponse(DynamicSystemController._Instance.RuleDB.Storing());
        }

        if (DestroyAfterTrigger) this.gameObject.SetActive(false);
	}

	private void DialogueEnd (DialogWindowClosed e)
	{
        GlobalGameStatus.Instance.StateStatus[(int)GlobalGameStatus.GameState.WorldDialog] = false;
        EventManager.Instance.RemoveListener<DialogWindowClosed> (DialogueEnd);
        EventManager.Instance.TriggerEvent(new TalkToPlayerEvent(true));
    }
}
