﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class SequenceTrigger : WorldTrigger {
	private StoryData _EventData;
	public StoryData EventData
	{
		get{ return _EventData; }
		set{ _EventData = value; }
	}

    public void Play()
    {
		WorldSceneManager.TransWorldToSequence(AreaController.Instance.CurrPrefab, AreaController.Instance.CurrPlayer, _EventData.Scene.WorldEvent);
		PartyManager.SceneFinished(_EventData.ID);
        if(DestroyAfterTrigger)GameObject.Destroy(this.gameObject);
    }

}
