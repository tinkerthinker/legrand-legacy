﻿using UnityEngine;
using System.Collections;

public class PortalTrigger : WorldTrigger {

    // Player position when coming through this portal
    // Offset from current transform position
    public Vector3 arriveOffset;
    public bool triggeredPortalWalking = false;

    public bool UnlockAndChangeDestinationByTrigger = false;
    
    //void OnTriggerEnter(Collider coll)
    //{
    //    if (!NeedActivate && OnTrigger != null && _ObjectOnTrigger.tag == "PartyLeader")
    //    {
    //        _ObjectOnTrigger = coll.gameObject;
    //        OnTrigger(this, _ObjectOnTrigger);
    //    }
    //}
}
