﻿using UnityEngine;
using System.Collections;

public class FrostBiteResetTrigger : WorldTriggerParticle
{
    protected override void OnTriggerEnter(Collider coll)
    {
        base.OnTriggerEnter(coll);
    }

    protected override void OnTriggerExit(Collider coll)
    {
        base.OnTriggerExit(coll);
    }

    protected override void PreAction()
    {
        SetFrostBite(false);
    }

    protected override void PostAction()
    {
        SetFrostBite(true);
    }

    void SetFrostBite(bool replay)
    {
        if(transform.parent != null && transform.parent.parent != null)
        {
            FrostBite frostBite = transform.parent.parent.gameObject.GetComponent<FrostBite>();
            if(frostBite)
            {
                if (replay)
                    frostBite.StartFrostBite();
                else
                {
                    frostBite.Stop();
                    EventManager.Instance.TriggerEvent(new FrostBiteResetEvent(1f));
                }
            }
        }
    }
    
    protected override void Activated(EnterTrigger e)
    {
        SetFrostBite(false);
        EventManager.Instance.RemoveListener<EnterTrigger>(Activated);
    }
}
