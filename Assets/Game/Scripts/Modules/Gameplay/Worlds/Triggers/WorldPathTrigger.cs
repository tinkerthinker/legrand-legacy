﻿using UnityEngine;
using System.Collections;

public class WorldPathTrigger : WorldTrigger {

	// Direction player will be going 
	// for each point in the world map
	public GameObject WestPoint;
	public GameObject EastPoint;
	public GameObject NorthPoint;
	public GameObject SouthPoint;
}
