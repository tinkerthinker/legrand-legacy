﻿using UnityEngine;
using System.Collections;
using RAIN.Core;

public class MonsterTrigger : WorldTrigger 
{
	public string monsterArea;

    public bool Death;
    public int CountLimit = 1;
    private int Count = 0;

    public void OnDeath()
    {
        Death = true;
        Count = 0;
    }

    public bool EnableMonster()
    {
        if(Death)
            Count++;
        if(Count >= CountLimit)
        {
            Death = false;
            return true;
        }
        return false;
    }

    void OnTriggerEnter(Collider coll)
    {
    }

    void OnCollisionEnter(Collision coll)
	{
        if((AreaController.Instance != null && !AreaController.Instance.DisableBattle) && (coll.gameObject.tag == "PartyLeader" && OnTrigger != null))
        {
            _ObjectOnTrigger = coll.gameObject;

            if (!NeedActivate && _ObjectOnTrigger.GetComponent<PlayerController>().ControllerState == PlayerController.PlayerControllerState.Normal)
            {
                EventManager.Instance.TriggerEvent(new LastPositionCheckEvent(_ObjectOnTrigger.transform.position, _ObjectOnTrigger.transform.eulerAngles));
                OnTrigger(this, _ObjectOnTrigger);
                OnDeath();
            }
        }

		//Debug.Log("[Trigger] " + _ObjectOnTrigger.name + " entered " + gameObject.name);
	}

	public void StartAI()
	{
        if (gameObject.activeSelf)
        {
            EncounterBehavior encBehavior = LegrandUtility.GetComponentInChildren<EncounterBehavior>(this.gameObject);
            encBehavior.IsActive = true;
        }
	}

	public void StopAI()
	{
        if(gameObject.activeSelf)
        {
            EncounterBehavior encBehavior = LegrandUtility.GetComponentInChildren<EncounterBehavior>(this.gameObject);
            encBehavior.IsActive = false;
        }
    }

    public void RestartAI()
    {
        foreach (Transform child in transform)
        {
            EncounterBehavior encBehavior = LegrandUtility.GetComponentInChildren<EncounterBehavior>(this.gameObject);
            if (encBehavior != null)
            {
                encBehavior.Init();
                encBehavior.IsActive = true;
            }
        }
    }
}
