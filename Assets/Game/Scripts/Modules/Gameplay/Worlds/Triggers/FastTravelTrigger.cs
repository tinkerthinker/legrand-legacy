﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using TeamUtility.IO;

public class FastTravelTrigger : WorldTrigger {

    public GameObject btnPrefab;
    private GameObject canvas;
    private List<GameObject> btnFastTravel = new List<GameObject>();

    void Awake()
    {
        canvas = transform.parent.GetComponentInChildren<Canvas>().gameObject;
        //if(PartyManager.Instance.WorldInfo.UnlockedFastTravelPoints.Count <= 0)
        //{
        //    PartyManager.Instance.WorldInfo.UnlockedFastTravelPoints.Add(new FastTravelAttribute("Dumville", "Dumville_Entrance"));
        //    PartyManager.Instance.WorldInfo.UnlockedFastTravelPoints.Add(new FastTravelAttribute("Dunabad Cave", "Dunabad_Cave_Entrance"));
        //}
    }


    void OnEnable()
    {
        canvas.SetActive(true);
        List<FastTravelAttribute> unlockedFastTravel = PartyManager.Instance.WorldInfo.UnlockedFastTravelPoints;

        for (int i = 0; i < canvas.transform.GetChild(0).childCount; i++)
            Destroy(canvas.transform.GetChild(0).GetChild(i).gameObject);
        
        btnFastTravel.Clear();

        //for (int i = 0; i < unlockedFastTravel.Count; i++)
        //{
        //    if (!AreaController.Instance.CurrSubAreaData.prefabName.Equals(unlockedFastTravel[i].PrefabName))
        //    {
        //        GameObject temp = Instantiate(btnPrefab);
        //        temp.GetComponent<FastTravelAction>().FastTravelAttribute = unlockedFastTravel[i];
        //        temp.transform.SetParent(canvas.transform.GetChild(0).transform);
        //        temp.transform.GetChild(0).GetComponent<Text>().text = unlockedFastTravel[i].Name;
        //        temp.transform.localScale = new Vector3(1f, 1f, 1f);
        //        btnFastTravel.Add(temp);       
        //    }
        //}
        if(btnFastTravel.Count > 0)
            EventSystem.current.firstSelectedGameObject = btnFastTravel[0];
        canvas.SetActive(false);
    }


    void OnTriggerEnter(Collider coll)
    {
        if (coll.tag == "PartyLeader")
        {
            _ObjectOnTrigger = coll.gameObject;
           
            if (NeedActivate)
            {
                EventManager.Instance.AddListener<EnterTrigger>(ActivateFastTravelUI);
                EventManager.Instance.AddListener<ExitTrigger>(DeactivateFastTravelUI);
            }
                
        }
        
    }

    void OnTriggerExit(Collider coll)
    {
        if (coll.tag == "PartyLeader")
        {
            EventManager.Instance.RemoveListener<EnterTrigger>(ActivateFastTravelUI);
            EventManager.Instance.RemoveListener<ExitTrigger>(DeactivateFastTravelUI);
            canvas.SetActive(false);
        }
    }

    void OnDestroy()
    {
        EventManager.Instance.RemoveListener<EnterTrigger>(ActivateFastTravelUI);
        EventManager.Instance.RemoveListener<ExitTrigger>(DeactivateFastTravelUI);
    }

    void ActivateFastTravelUI(EnterTrigger e)
    {
        if (!canvas.activeSelf)
        {
            canvas.SetActive(true);
        
        }
            
    }

    void DeactivateFastTravelUI(ExitTrigger e)
    {
        if (canvas.activeSelf)
            canvas.SetActive(false);
    }
}
