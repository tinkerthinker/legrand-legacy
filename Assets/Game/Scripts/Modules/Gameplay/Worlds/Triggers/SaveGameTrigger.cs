﻿using UnityEngine;
using System.Collections;

public class SaveGameTrigger : WorldTrigger {
	public Vector2 arriveCameraPan;

	void OnTriggerEnter(Collider coll)
	{
		_ObjectOnTrigger = coll.gameObject;
		if (!NeedActivate && OnTrigger != null && _ObjectOnTrigger.tag == "PartyLeader")
		{
			OnTrigger(this, _ObjectOnTrigger);
		}
		
		if (NeedActivate)
		{
//			SoundManager.PlaySoundEffect("Cutscenes/Save Mode");
			EventManager.Instance.AddListener<Interact>(Save);
		}
	}
	
	void OnTriggerExit(Collider coll)
	{
		if (NeedActivate)
		{
			EventManager.Instance.RemoveListener<Interact>(Save);
		}
		
		_ObjectOnTrigger = null;
	}
	
	void Save(Interact i)
	{
		Activate ();
	}
}
