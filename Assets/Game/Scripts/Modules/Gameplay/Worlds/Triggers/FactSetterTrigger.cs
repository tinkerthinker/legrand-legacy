﻿using UnityEngine;
using System.Collections.Generic;

public class FactSetterTrigger : MonoBehaviour {
    public bool CheckFactFirst;
    public CriterionStatic[] RequirementFact;
    public VectorStringDouble[] NewFact;

    void OnTriggerEnter(Collider coll)
    {
        if (coll.tag == "PartyLeader")
        {
            if(CheckFactFirst)
            {
                for (int i = 0; i < RequirementFact.Length; i++)
                {
                    double value = 0;
                    Fact f = PartyManager.Instance.WorldInfo.GetFact(RequirementFact[i].Criterion);
                    if (f != null) value = f.Value;

                    if(!RequirementFact[i].Compare(value))
                        return;
                }
            }

            for (int i = 0; i < NewFact.Length; i++)
                PartyManager.Instance.WorldInfo.NewFact(NewFact[i].x, NewFact[i].y);
            
        }
    }
}
