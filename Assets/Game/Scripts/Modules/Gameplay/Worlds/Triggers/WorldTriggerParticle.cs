﻿using UnityEngine;
using System.Collections;

public class WorldTriggerParticle : WorldTrigger
{
    public ParticleSystem preEffect;
    public ParticleSystem postEffect;

    public WwiseManager.WorldFX EffectSound = WwiseManager.WorldFX.NONE;

    protected virtual void PreAction()
    { }

    protected virtual void PostAction()
    { }

    protected virtual void OnTriggerEnter(Collider coll)
    {
        if (coll.tag == "PartyLeader")
        {
            AreaController.Instance.insideHealPoint = true;
            if (NeedActivate)
            {
                _MarkPopUpHelper = coll.GetComponentInChildren<DialogueHelperReference>().helper;
                PopUpUI.CallDialogueMarkPopUp(_MarkPopUpHelper, true);
                EventManager.Instance.AddListener<EnterTrigger>(Activated);
            }
            else
            {
                WwiseManager.Instance.PlaySFX(EffectSound);
                preEffect.Stop();
                postEffect.Play();
                PreAction();
            }
        }
    }

    protected virtual void OnTriggerExit(Collider coll)
    {
        if (coll.tag == "PartyLeader")
        {
            AreaController.Instance.insideHealPoint = false;
            if (NeedActivate)
            {
                PopUpUI.CallDialogueMarkPopUp(null, false);
                _MarkPopUpHelper = null;
                EventManager.Instance.RemoveListener<EnterTrigger>(Activated);
            }
            else
            {
                postEffect.Stop();
                preEffect.Play();
                PostAction();
            }
        }
    }
}
