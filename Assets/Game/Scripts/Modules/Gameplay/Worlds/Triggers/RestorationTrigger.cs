﻿using UnityEngine;
using Legrand.core;

public class RestorationTrigger : WorldTriggerParticle
{
    protected override void PreAction()
    {
        foreach (MainCharacter mc in PartyManager.Instance.CharacterParty)
        {
            mc.Health.ChangeCurrentValue(mc.Health.MaxValue);
        }
    }

    protected override void OnTriggerEnter(Collider coll)
    {
        base.OnTriggerEnter(coll);
    }

    protected override void OnTriggerExit(Collider coll)
    {
        base.OnTriggerExit(coll);
    }

    protected override void Activated(EnterTrigger e)
    {
        foreach (MainCharacter mc in PartyManager.Instance.CharacterParty)
        {
            mc.Health.ChangeCurrentValue(mc.Health.MaxValue);
        }
    }
}
