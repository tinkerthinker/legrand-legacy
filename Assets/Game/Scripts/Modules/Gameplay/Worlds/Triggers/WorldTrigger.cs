﻿using UnityEngine;
using System.Collections;

// Representing a trigger in the world. Best to inherit this for all trigger interactions.
public class WorldTrigger : MonoBehaviour 
{
	public string TriggerID = "-1";
    // Will call the delegate when the trigger is activated.
    public delegate void onTrigger(WorldTrigger trigger, GameObject objectOnTrigger);
    public onTrigger OnTrigger;
    public onTrigger OffTrigger;
    
    // Do we need to "Activate" the trigger or we can just walk through it?
    public bool NeedActivate = false;
    public bool DestroyAfterTrigger = true;
    
    protected GameObject _ObjectOnTrigger;

    protected GameObject _MarkPopUpHelper;

    public void Enable()
    {
        if(GetComponent<Collider>()!=null)
            GetComponent<Collider>().enabled = true;
    }
    
    public void Disable()
    {
        if (GetComponent<Collider>() != null)
            GetComponent<Collider>().enabled = false;

        OnTrigger = null;
        OffTrigger = null;
        EventManager.Instance.RemoveListener<EnterTrigger>(Activated);
        if (_MarkPopUpHelper != null)
        {
            PopUpUI.CallDialogueMarkPopUp(null, false);
            _MarkPopUpHelper = null;
        }
    }

    void OnDestroy()
    {
        if (_MarkPopUpHelper != null)
        {
            PopUpUI.CallDialogueMarkPopUp(null, false);
            _MarkPopUpHelper = null;
        }
    }
    
	public bool CheckTriggerHasListener()
	{
		if(OnTrigger != null) return true;
		return false;
	}

    public void Activate()
    {
        if (NeedActivate && OnTrigger != null && _ObjectOnTrigger != null) 
		{
			EventManager.Instance.TriggerEvent(new LastPositionCheckEvent(_ObjectOnTrigger.transform.position, _ObjectOnTrigger.transform.eulerAngles));
			OnTrigger (this, _ObjectOnTrigger);
		}
        //Debug.Log("[Trigger] " + _ObjectOnTrigger.name + " activated " + gameObject.name);
    }
    
    void OnTriggerEnter(Collider coll)
    {
        if(coll.tag == "PartyLeader" && OnTrigger != null)
        {
            _ObjectOnTrigger = coll.gameObject;

            if (!NeedActivate)
            {
                EventManager.Instance.TriggerEvent(new LastPositionCheckEvent(_ObjectOnTrigger.transform.position, _ObjectOnTrigger.transform.eulerAngles));
                OnTrigger(this, _ObjectOnTrigger);
            }
            else
            {
                EventManager.Instance.AddListener<EnterTrigger>(Activated);
                _MarkPopUpHelper = coll.GetComponentInChildren<DialogueHelperReference>().helper;
                PopUpUI.CallDialogueMarkPopUp(_MarkPopUpHelper, true);
            }
        }

        //Debug.Log("[Trigger] " + _ObjectOnTrigger.name + " entered " + gameObject.name);
    }
    
    void OnTriggerExit(Collider coll)
    {
        //Debug.Log("[Trigger] " + _ObjectOnTrigger.name + " exited " + gameObject.name);

        if(coll.tag == "PartyLeader" && OnTrigger != null)
        {
            if(OffTrigger != null)
                OffTrigger(this, _ObjectOnTrigger);

            _ObjectOnTrigger = null;

            if (NeedActivate)
            {
                EventManager.Instance.RemoveListener<EnterTrigger>(Activated);
                PopUpUI.CallDialogueMarkPopUp(null, false);
                _MarkPopUpHelper = null;
            }
        }
    }   

	protected virtual void Activated(EnterTrigger e)
	{
		EventManager.Instance.RemoveListener<EnterTrigger> (Activated);
		Activate ();
	}

	public bool CheckListener()
	{
		if(OnTrigger != null || OffTrigger != null) return true;
		return false;
	}
}
