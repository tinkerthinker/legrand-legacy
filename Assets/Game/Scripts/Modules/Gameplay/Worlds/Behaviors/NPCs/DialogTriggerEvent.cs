﻿using UnityEngine;
using System.Collections;

public class DialogTriggerEvent : MonoBehaviour {
    public string ID;
    public GameObject DialogHelper;
    public string DialogueIdNames;
    private bool isTalking = false;

    void OnTriggerEnter(Collider C)
    {
        if (C.tag == "Interaction")
        {
            if (!EventManager.Instance.HasListener<Interact>(TalkToNPC))
                EventManager.Instance.AddListener<Interact>(TalkToNPC);

            PopUpUI.CallDialogueMarkPopUp(DialogHelper ? DialogHelper : this.gameObject, true, true);
        }

    }

    void OnDisable()
    {
        if (EventManager.Instance)
        {
            EventManager.Instance.RemoveListener<Interact>(TalkToNPC);
            EventManager.Instance.RemoveListener<CutsceneStartedEvent>(CutsceneStarted);
            EventManager.Instance.RemoveListener<DialogWindowClosed>(DialogueEnd);
            PopUpUI.CallDialogueMarkPopUp(DialogHelper ? DialogHelper : this.gameObject, false);
        }
        else
            Debug.LogError("Event manager tidak ada");
    }

    void OnTriggerExit(Collider C)
    {
        if (C.tag == "Interaction")
        {
            EventManager.Instance.RemoveListener<Interact>(TalkToNPC);
            PopUpUI.CallDialogueMarkPopUp(DialogHelper ? DialogHelper : this.gameObject, false);
        }
    }

    public void TalkToNPC(Interact e)
    {
        if (isTalking) return;
        if (DialogueIdNames == "" || DialogueIdNames == string.Empty) return;

        PopUpUI.CallDialogueMarkPopUp(DialogHelper ? DialogHelper : this.gameObject, false);
        isTalking = true;
        

        EventManager.Instance.TriggerEvent(new TalkToPlayerEvent(false));
        EventManager.Instance.AddListener<CutsceneStartedEvent>(CutsceneStarted);
        EventManager.Instance.AddListener<DialogWindowClosed>(DialogueEnd);

        if (!Dialoguer.GetGlobalBoolean(2))
        {
                if (DialogHelper != null && DialogHelper.GetComponent<RegObjDialogue>() != null)
                    DialogHelper.GetComponent<RegObjDialogue>().ReRegister();
                LegrandUtility.StartDialogue(DialogueIdNames);
        }
        
    }

    private void CutsceneStarted(CutsceneStartedEvent e)
    {
        EventManager.Instance.RemoveListener<CutsceneStartedEvent>(CutsceneStarted);
        EventManager.Instance.RemoveListener<DialogWindowClosed>(DialogueEnd);
    }

    protected void DialogueEnd(DialogWindowClosed e)
    {
        StartCoroutine(WaitToIdle());
        PopUpUI.CallDialogueMarkPopUp(DialogHelper ? DialogHelper : this.gameObject, true, true);
        EventManager.Instance.TriggerEvent(new TalkToPlayerEvent(true));
        EventManager.Instance.RemoveListener<CutsceneStartedEvent>(CutsceneStarted);
        EventManager.Instance.RemoveListener<DialogWindowClosed>(DialogueEnd);
    }

    IEnumerator WaitToIdle()
    {
        yield return new WaitForEndOfFrame();
        isTalking = false;
    }
}
