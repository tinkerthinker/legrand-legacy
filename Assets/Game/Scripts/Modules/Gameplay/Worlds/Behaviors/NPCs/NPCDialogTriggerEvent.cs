﻿using UnityEngine;
using System.Collections;
using RAIN.Core;

public class NPCDialogTriggerEvent : MonoBehaviour {
	private NPCBehaviour _NPCBehaviour;
    private bool _IsCallDialogueMark = false;

	void Start()
	{
		_NPCBehaviour = gameObject.GetComponentInParent<NPCBehaviour> ();
	}

	void OnTriggerEnter(Collider C)
	{
		if (C.tag == "Interaction")
		{
            if(!EventManager.Instance.HasListener<Interact>(TalkToNPC))
                EventManager.Instance.AddListener<Interact>(TalkToNPC);

            if (_NPCBehaviour._StateNPC != NPC.NPCState.Talking)
			{
				PopUpUI.CallDialogueMarkPopUp (C.GetComponent<DialogueHelperReference>().helper, true, true);
                _IsCallDialogueMark = true;
			}
		}
	}

    void OnDisable()
    {
        if (EventManager.Instance)
            EventManager.Instance.RemoveListener<Interact>(TalkToNPC);
        else
            Debug.LogError("Event manager tidak ada");
        if (_IsCallDialogueMark)
        {
            PopUpUI.CallDialogueMarkPopUp(null, false);
            _IsCallDialogueMark = false;
        }
    }

	void OnTriggerExit(Collider C)
	{
		if (C.tag == "Interaction")
		{
			EventManager.Instance.RemoveListener<Interact>(TalkToNPC);
			PopUpUI.CallDialogueMarkPopUp (null, false);
            _IsCallDialogueMark = false;
		}
	}
	
	public void TalkToNPC(Interact e)
	{
		if (_NPCBehaviour.State == NPC.NPCState.Talking)
			return;
		_NPCBehaviour.InterruptAIActivity();
		_NPCBehaviour.talkedCharacter = e.TalkedCharacter;
		_NPCBehaviour.State = NPC.NPCState.Talking;
        EventManager.Instance.TriggerEvent(new RespondInteract(_NPCBehaviour.gameObject));
	}
}
