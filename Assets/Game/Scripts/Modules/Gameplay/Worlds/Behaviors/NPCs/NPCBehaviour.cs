﻿using UnityEngine;
using System.Collections;
using RAIN.Core;
using DG.Tweening;

public class NPCBehaviour : NPC
{
    public NPC.NPCState State
    {
        get { return _StateNPC; }
        set
        {
            if (_StateNPC != value)
            {
                _StateNPC = value;
                if (value == NPCState.Talking)
                {
                    TalkingEvent();
                }
            }
        }
    }

    protected Vector3 Central;

    [HideInInspector]
    public Vector3
        TargetPosition;
    [HideInInspector]
    public Vector3
        TargetRotation;

    public float MaxMoveRadius = 5f;
    public float MoveDuration = 3;
    public float RestDuration = 1;
    public bool IsRunning = false;

    protected bool finishRotating;

    protected GameObject _TalkedCharacter;
    public GameObject talkedCharacter
    {
        get { return _TalkedCharacter; }
        set
        {
            _TalkedCharacter = value;
            TargetPosition = _TalkedCharacter.transform.position;
        }
    }

    protected bool isMoveableNPC = true;

    public string roamArea;
    protected Animator _Anim;
    protected AIRig RainRig;


    void Awake()
    {
        Central = transform.localPosition;
        _Anim = GetComponent<Animator>();
        RainRig = GetComponentInChildren<AIRig>();
        EventManager.Instance.AddListener<PauseEvent>(Pause);
        EventManager.Instance.AddListener<UnPauseEvent>(UnPause);
    }

    void OnDestroy()
    {
        if (EventManager.Instance != null)
        {
            EventManager.Instance.RemoveListener<PauseEvent>(Pause);
            EventManager.Instance.RemoveListener<UnPauseEvent>(UnPause);
        }
    }

    protected virtual void OnEnable()
    {
        if (IsShoutNPC)
        {
            if (ShoutsDialogue.Count > 0)
                StartCoroutine(Shouting());
        }
    }

    void OnDisable()
    {
        if (EventManager.Instance != null)
            EventManager.Instance.RemoveListener<DialogWindowClosed>(DialogueEnd);
        else
            Debug.LogWarning("Event Manager Tidak Ada");

        if(IsShoutNPC)
            _IsShouting = false;
    }

    protected virtual IEnumerator Rotate(Vector3 direction)
    {
        finishRotating = false;

        direction.x = transform.rotation.x;
        direction.z = transform.rotation.z;

        float t = 0.0f;
        while (t < 1.0f)
        {
            t += Time.deltaTime;
            transform.eulerAngles = Vector3.Slerp(transform.eulerAngles, direction, t / 3);
            yield return null;
        }
        transform.eulerAngles = direction;
        finishRotating = true;
    }

    public void InterruptAIActivity()
    {
            if (RainRig == null)
                RainRig = GetComponentInChildren<AIRig>();

            if (RainRig != null)
                RainRig.AI.WorkingMemory.SetItem("isTalking", true);
    }

    public void RestartAI()
    {
        if (RainRig == null)
            RainRig = GetComponentInChildren<AIRig>();
        if (RainRig != null)
            RainRig.AI.Mind.AIInit();
    }

    // <summary>
    /// Override this if NPC is a roaming NPC
    // </summary>
    protected virtual IEnumerator NewHeading()
    {
        yield return null;
    }

    /// <summary>
    /// Override this if Want to Add another feature when talking event
    /// </summary>A
    protected virtual void TalkingEvent()
    {
        if(IsShoutNPC)
        {
            _IsShouting = false;
            EventManager.Instance.TriggerEvent(new CloseShoutPopUpEvent(_SelectedShout));
        }
        if (DialogueIdNames.Count > 0 && !DialogByQuery)
        {
            for (int i = 0; i < DialogueIdNames.Count; i++)
                DialogueIdNames[i] = DialogueIdNames[i].Replace(" ", "_");

            EventManager.Instance.TriggerEvent(new TalkToPlayerEvent(false));
            if (!Dialoguer.GetGlobalBoolean(2))
            {
                //				PopUpUI.CallDialoguePopUp("Here i talk to npc" + _TalkedCharacter.name + " - with ID - " + ID, gameObject);
                if (DialogueHelper != null && DialogueHelper.GetComponent<RegObjDialogue>() != null)
                    DialogueHelper.GetComponent<RegObjDialogue>().ReRegister();

                //if (LegrandUtility.GetDialogueID(DialogueIdNames[currentDialogueIndex]) != -1)
                //    Dialoguer.StartDialogue((DialoguerDialogues)LegrandUtility.GetDialogueID(DialogueIdNames[currentDialogueIndex]));
                _ChoosenDialogueId = LegrandUtility.GetDialogueID(DialogueIdNames[currentDialogueIndex]);
            }

            PopUpUI.CallDialogueMarkPopUp(null, false, true);
            SetRotationTarget(TargetPosition, true);

            EventManager.Instance.AddListener<DialogWindowClosed>(DialogueEnd);
        }
        else if (DialogByQuery)
        {
            ResponsiveQuery query = new ResponsiveQuery();
            query.Add("Concept", LegrandUtility.GetUniqueSymbol("NPCDialog"));
            query.Add("Where", LegrandUtility.GetUniqueSymbol(AreaController.Instance.CurrPrefab.name));
            query.Add(QueryData);
            query.Add(PartyManager.Instance.WorldInfo.LatestFacts);
            if(UsingInventoryFact)
            {
                query.Add(PartyManager.Instance.Inventory.GetInventoryFacts(Item.Type.Key));
                query.Add(PartyManager.Instance.Inventory.GetInventoryFacts(Item.Type.CraftingMaterial));
                query.Add(PartyManager.Instance.Inventory.GetInventoryFacts(Item.Type.Consumable));
            }
            if (MonsterKilledFact)
                query.Add(PartyManager.Instance.WorldInfo.MonsterKilledFacts);

            if (query.Match())
            {
                EventManager.Instance.TriggerEvent(new TalkToPlayerEvent(false));
                //Dialoguer.StartDialogue(LegrandUtility.GetDialogueID(DynamicSystemController._Instance.RuleDB.GetLatestResult().ResponseName));
                RuleResult result = DynamicSystemController._Instance.RuleDB.GetLatestResult();
                _ChoosenDialogueId = LegrandUtility.GetDialogueID(result.ResponseName);
                for(int i=0;i< result.Facts.Count;i++)
                {
                    PartyManager.Instance.WorldInfo.NewFact(result.Facts[i]);
                }
                EventManager.Instance.AddListener<DialogWindowClosed>(DialogueEnd);

                PopUpUI.CallDialogueMarkPopUp(null, false, true);
                SetRotationTarget(TargetPosition, true);
            }
            else State = NPCState.Idle;

        }
        else
        {
            State = NPCState.Idle;
        }
    }

    protected virtual void TalkingEventRoutine()
    {
        if (_ChoosenDialogueId != -1)
        {
            LegrandUtility.StartDialogue(_ChoosenDialogueId);
            _ChoosenDialogueId = -1;
        }
    }

    /// <summary>
    /// Override this if want to change algorithm to rotate or add some feature in it
    /// </summary>
    protected virtual void SetRotationTarget(Vector3 TargetPosition, bool isRotateImmediately)
    {
        Vector3 currentRotation = this.transform.eulerAngles;
        transform.LookAt(TargetPosition);
        TargetRotation = this.transform.eulerAngles;
        TargetRotation.x = currentRotation.x;
        TargetRotation.z = currentRotation.z;

        this.transform.eulerAngles = currentRotation;

        if (isRotateImmediately)
            this.transform.DORotate(TargetRotation, 0.5f, RotateMode.Fast).OnComplete(TalkingEventRoutine);
    }

    /// <summary>
    /// Override this if want to change algorithm or add some feature when dialoguer close its window
    /// </summary>
    protected virtual void DialogueEnd(DialogWindowClosed e)
    {
        EventManager.Instance.TriggerEvent(new TalkToPlayerEvent(true));
        EventManager.Instance.RemoveListener<DialogWindowClosed>(DialogueEnd);
        if (RainRig == null)
            RainRig = GetComponentInChildren<AIRig>();

        if (RainRig != null)
            RainRig.AI.WorkingMemory.SetItem("isTalking", false);

        Invoke("WaitToIdle", 1f);

        if(IsShoutNPC)
        {
            if (!StopShoutAfterDialogue)
                StartCoroutine(WaitToShout(1f));
        }
    }

    private void WaitToIdle()
    {
        State = NPCState.Idle;
    }

    IEnumerator WaitToShout(float time)
    {
        yield return WaitFor.RealTime(time);
        StartCoroutine(Shouting());
    }

    IEnumerator Shouting()
    {
        _IsShouting = true;
        while (_IsShouting)
        {
            yield return new WaitForSeconds(CoolDown / 2f);
            //untuk sementara dibikin balonnya selalu shout
            if (_IsShouting)
            {
                if (IsShoutRandom)
                    _SelectedShout = ShoutsDialogue[LegrandUtility.Random(0, ShoutsDialogue.Count)];
                else
                {
                    _SelectedShout = ShoutsDialogue[_IndexShout];
                    if (++_IndexShout >= ShoutsDialogue.Count)
                        _IndexShout = 0;
                }
                string translate = LegrandBackend.Instance.GetLanguageData(_SelectedShout);
                _SelectedShout = string.IsNullOrEmpty(translate) ? _SelectedShout : translate;
                PopUpUI.CallDialoguePopUp("", _SelectedShout, DialoguePopUp.BalloonType.Shout, DialogueHelper ? DialogueHelper : gameObject, LiveTime);
            }
            yield return new WaitForSeconds(LiveTime + CoolDown / 2f);
        }
    }

    void Pause(PauseEvent p)
    {
        if (RainRig == null)
            RainRig = GetComponentInChildren<AIRig>();

        if (RainRig != null)
            RainRig.AI.WorkingMemory.SetItem("moveSpeed", 0f);
        if(_Anim)
            _Anim.enabled = false;
    }

    void UnPause(UnPauseEvent p)
    {
        if (RainRig == null)
            RainRig = GetComponentInChildren<AIRig>();

        if (RainRig != null)
            RainRig.AI.WorkingMemory.SetItem("moveSpeed", 1.5f);
        if (_Anim)
            _Anim.enabled = true;
    }
}
