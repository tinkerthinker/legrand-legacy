﻿using UnityEngine;
using System.Collections;

public class MiniGameNPC : StaticNPC
{
    public string Message;
    public GameObject MiniGameObject;

    protected override void DialogueEnd(DialogWindowClosed e)
    {
        base.DialogueEnd(e);
        PopUpUI.CallChoicePopUp(Message, OnYes, OnNo);
    }

    void OnYes()
    {
        Debug.Log("player main mini game");
        MiniGameObject.SetActive(true);
    }

    void OnNo()
    {
        Debug.Log("player tidak main mini game");
        MiniGameObject.SetActive(false);
    }
}
