﻿using UnityEngine;
using System.Collections;
using Legrand.core;

public class QuestChecker : MonoBehaviour
{
	public string QuestID;

	void OnEnable ()
	{
		Quest quest = QuestManager.Instance.GetQuestByKey (QuestID);
		if (quest == null) {
			this.gameObject.SetActive (false);
		}
	}
}
