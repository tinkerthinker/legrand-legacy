﻿using UnityEngine;
using System.Collections;

public class QuestNPC : NPCBehaviour {
	void Start () {
		_Type = NPCType.Quest;
		State = NPC.NPCState.Idle;
		isMoveableNPC = false;
	}
	
	void OnEnable()
	{
		State = NPCState.Idle;
	}
}
