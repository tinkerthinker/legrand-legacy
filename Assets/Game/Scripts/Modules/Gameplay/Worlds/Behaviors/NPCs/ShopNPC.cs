﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
public class ShopNPC : NPCBehaviour {
    public enum ShopType
    {
        ItemVendor,
        Alchemy,
        Blacksmith,
        FastTravel,
        Storage
    }

    public ShopType Type;
	public List<int> DialogueCloseShopIds;
    [Header("For Item Vendor")]
	public string shopModelId;
    [Header("For Alchemy and Blacksmith")]
    public int ShopLevel;

	public Vector3 startingRotation;
    public GameObject ShopIcon;

	void Start()
	{
		_Anim = GetComponent<Animator> ();
		startingRotation = transform.eulerAngles;
		isMoveableNPC = false;
		State = NPCState.Idle;
		_Type = NPCType.Shop;

        if (ShopIcon)
            ShopIcon.transform.LookAt(GlobalGameStatus.Instance.MainCamera.transform);
    }

	protected override void TalkingEvent ()
	{
//		_Anim.CrossFade ("Talk", 0f);
		SetRotationTarget (TargetPosition, false);
		EventManager.Instance.TriggerEvent(new TalkToPlayerEvent(false));

        PopUpUI.CallDialogueMarkPopUp(null, false, true);
        this.transform.DORotate(TargetRotation, 1f, RotateMode.Fast).OnComplete(TalkingEventRoutine);
        /*
		if (DialogueIdNames.Count > 0) 
		{
			EventManager.Instance.AddListener<DialogWindowClosed> (DialogueEnd);
            //Dialoguer.StartDialogue ((DialoguerDialogues)DialogueIds [currentDialogueIndex]);
            Dialoguer.StartDialogue((DialoguerDialogues)LegrandUtility.GetDialogueID(DialogueIdNames[currentDialogueIndex]));
            this.transform.DORotate (TargetRotation, 1f, RotateMode.Fast);
		}
		else this.transform.DORotate (TargetRotation, 1f, RotateMode.Fast).OnComplete(TalkingEventRoutine);
         */
        //EventManager.Instance.AddListener<PauseEvent>(ShopMenu);
    }

	void TalkingEventRoutine()
	{
		//if there are no dialogue before open shop, then open it immediately

        if (DialogByQuery)
        {
            ResponsiveQuery query = new ResponsiveQuery();
            query.Add("Concept", LegrandUtility.GetUniqueSymbol("NPCDialog"));
            query.Add("Where", LegrandUtility.GetUniqueSymbol(AreaController.Instance.CurrPrefab.name));
            query.Add(QueryData);
            query.Add(PartyManager.Instance.WorldInfo.LatestFacts);
            if (UsingInventoryFact)
                query.Add(PartyManager.Instance.Inventory.GetInventoryFacts(Item.Type.Key));
            if (MonsterKilledFact)
                query.Add(PartyManager.Instance.WorldInfo.MonsterKilledFacts);

            if (query.Match())
            {
                RuleResult result = DynamicSystemController._Instance.RuleDB.GetLatestResult();
                for (int i = 0; i < result.Facts.Count; i++)
                {
                    PartyManager.Instance.WorldInfo.NewFact(result.Facts[i]);
                }
                EventManager.Instance.AddListener<DialogWindowClosed>(DialogueEnd);

                InterruptAIActivity();

                LegrandUtility.StartDialogue(LegrandUtility.GetDialogueID(result.ResponseName));
                return; 
            }
        }

		if (DialogueIdNames.Count == 0)
		{
			OpenShop();
			EventManager.Instance.RemoveListener<DialogWindowClosed>(DialogueEnd);
		}
        else
        {

            EventManager.Instance.AddListener<DialogWindowClosed>(DialogueEnd);
            InterruptAIActivity();
            LegrandUtility.StartDialogue(DialogueIdNames[currentDialogueIndex]);
        }
	}

	protected override void DialogueEnd (DialogWindowClosed e)
	{
		base.DialogueEnd (e);
		OpenShop();
	}

	void OpenShop()
	{
		if (_TalkedCharacter.GetComponent<PlayerController> () != null)
		{
            switch(Type)
            {
                case ShopType.ItemVendor:
                    EventManager.Instance.TriggerEvent(new ShopEvent(true, shopModelId));
                    EventManager.Instance.AddListener<ShopEvent>(CloseItemShop);			        
                    break;
                case ShopType.Alchemy:
                    EventManager.Instance.TriggerEvent(new AlchemistShopEvent(true, ShopLevel));
                    EventManager.Instance.AddListener<AlchemistShopEvent>(CloseAlchemyShop);
                    break;
                case ShopType.Blacksmith:
                    EventManager.Instance.TriggerEvent(new CraftingShopEvent(true, ShopLevel));
                    EventManager.Instance.AddListener<CraftingShopEvent>(CloseBlacksmithShop);
                    break;
                case ShopType.FastTravel:
                    EventManager.Instance.TriggerEvent(new FastTravelUIEvent(true));
                    EventManager.Instance.AddListener<FastTravelUIEvent>(CloseFastTravel);
                    break;
                case ShopType.Storage:
                    EventManager.Instance.TriggerEvent(new StorageUIEvent(true));
                    EventManager.Instance.AddListener<StorageUIEvent>(CloseStorage);
                    break;
            }
			
		}
	}

    void CloseItemShop(ShopEvent e)
    {
        if(!e.isOpen)
        {
            EventManager.Instance.RemoveListener<ShopEvent>(CloseItemShop);
            CloseShop();
        }        
    }

    void CloseAlchemyShop(AlchemistShopEvent e)
    {
        if (!e.isOpen)
        {
            EventManager.Instance.RemoveListener<AlchemistShopEvent>(CloseAlchemyShop);
            CloseShop();
        }
    }

    void CloseBlacksmithShop(CraftingShopEvent e)
    {
        if (!e.isOpen)
        {
            EventManager.Instance.RemoveListener<CraftingShopEvent>(CloseBlacksmithShop);
            CloseShop();
        }
    }

    void CloseFastTravel(FastTravelUIEvent e)
    {
        if (!e.isOpen)
        {
            EventManager.Instance.RemoveListener<FastTravelUIEvent>(CloseFastTravel);
            CloseShop();
        }
    }

    void CloseStorage(StorageUIEvent e)
    {
        if (!e.isOpen)
        {
            EventManager.Instance.RemoveListener<StorageUIEvent>(CloseStorage);
            CloseShop();
        }
    }

	void CloseShop()
	{
        if (DialogueIdNames.Count > 0 && DialogueCloseShopIds.Count > 0)
        {
            LegrandUtility.StartDialogue(DialogueCloseShopIds[Mathf.Clamp(currentDialogueIndex, 0, DialogueCloseShopIds.Count - 1)]);
            EventManager.Instance.AddListener<DialogWindowClosed>(CloseShopDialogueEnd);
        }
        else
        {
            EventManager.Instance.TriggerEvent(new TalkToPlayerEvent(true));
            this.transform.DORotate(startingRotation, 0.5f, RotateMode.Fast).OnComplete(ShopBackToPosition);
        }
	}

	void CloseShopDialogueEnd(DialogWindowClosed e)
	{
		EventManager.Instance.RemoveListener<DialogWindowClosed>(CloseShopDialogueEnd);
		this.transform.DORotate (startingRotation, 0.5f, RotateMode.Fast).OnComplete(ShopBackToPosition);
	}

	void ShopBackToPosition()
	{
		State = NPCState.Idle;
	}
}
