﻿using UnityEngine;
using System.Collections;

public class NPCController : MonoBehaviour {

    public float walkSpeed = 4f;
    public float runSpeed = 4f;
    float turnSmoothing = 15f;
    Vector3 targetMovement;
    Rigidbody rigidBody;
    
    Animator anim;
    
    void Awake()
    {
        anim = GetComponent<Animator>();
        rigidBody = GetComponent<Rigidbody>();
    }
    
    void FixedUpdate () {
        rigidBody.AddForce(0, -100, 0);
    }

    public void Stop()
    {
        anim.SetBool("Walking", false);
        anim.SetBool("Running", false);
    }

    public void Walk(float horizontal, float vertical)
    {
        targetMovement.Set(horizontal, 0f, vertical);
        
        if(horizontal != 0 || vertical != 0)
        {
            anim.SetBool("Walking", true);
            anim.SetBool("Running", false);

            transform.position += targetMovement.normalized * walkSpeed * Time.deltaTime;
            Rotate(targetMovement);

        }
        else
        {
            anim.SetBool("Running", false);
            anim.SetBool("Walking", false);
        }
    }

    public void Run(float horizontal, float vertical)
    {
        targetMovement.Set(horizontal, 0f, vertical);
        
        if(horizontal != 0 || vertical != 0)
        {
            anim.SetBool("Running", true);
            anim.SetBool("Walking", false);
            transform.position += targetMovement.normalized * runSpeed * Time.deltaTime;
            Rotate(targetMovement);

        }
        else
        {
            anim.SetBool("Walking", false);
            anim.SetBool("Running", false);
        }
    }

    void Rotate(Vector3 direction)
    {
        Quaternion targetRotation = Quaternion.LookRotation(direction);
        Quaternion newRotation = Quaternion.Lerp(transform.rotation, targetRotation, turnSmoothing * Time.deltaTime);
        transform.rotation = newRotation;
    }
}
