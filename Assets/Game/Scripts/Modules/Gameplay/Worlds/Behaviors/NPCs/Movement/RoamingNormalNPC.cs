﻿using UnityEngine;
using System.Collections;

public class RoamingNormalNPC : NPCBehaviour {
	private float ChangeInterval;

	// Use this for initialization
	void Start () {
		_Type = NPCType.Roaming;

		TargetPosition = new Vector3(LegrandUtility.Random(Central.x - MaxMoveRadius, Central.x + MaxMoveRadius)
		                             ,0f
		                             , LegrandUtility.Random(Central.z - MaxMoveRadius, Central.z + MaxMoveRadius)
		                             );
		
		SetRotationTarget(TargetPosition, true);

		State = NPC.NPCState.Move;
	}

	void OnEnable()
	{
		State = NPC.NPCState.Move;
	}

	protected override IEnumerator NewHeading ()
	{
		SetInterval();
		yield return new WaitForSeconds(ChangeInterval);
		NewRoutine();
	}

	void NewRoutine()
	{
		if (State == NPC.NPCState.Idle)
		{
			TargetPosition = new Vector3(LegrandUtility.Random(Central.x - MaxMoveRadius, Central.x + MaxMoveRadius)
			                             ,0f
			                             , LegrandUtility.Random(Central.z - MaxMoveRadius, Central.z + MaxMoveRadius)
			                             );
			
			SetRotationTarget(TargetPosition, true);
			
			State = NPC.NPCState.Move;
		}
		else if (State == NPC.NPCState.Move)
		{
			State = NPC.NPCState.Idle;
		}
	}
	
	void SetInterval()
	{
		if (State == NPC.NPCState.Idle)
		{
			ChangeInterval = RestDuration;
			
		} 
		else if((State == NPC.NPCState.Move))
		{
			ChangeInterval =  MoveDuration;            
		}
	}
}
