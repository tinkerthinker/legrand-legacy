﻿using UnityEngine;
using System.Collections;
using DialoguerCore;
using RAIN.Core;

public class RoamingNPC : NPC {
    public enum RoamingNPCState
    {
        Idle,
        Move,
        Talking
    }
    private RoamingNPCState _State;
    public RoamingNPCState State
    {
        get{ return _State; }
        set{ 
            _State = value;
            if(value != RoamingNPCState.Talking) StartCoroutine(NewHeading());
            else
            {
                if(DialogueIdNames.Count > 0)
                {
                    SetRotationTarget(TargetPosition);
                    EventManager.Instance.TriggerEvent(new TalkToPlayerEvent(false));
                    //Dialoguer.StartDialogue((DialoguerDialogues)DialogueIds[currentDialogueIndex]);
                    LegrandUtility.StartDialogue(DialogueIdNames[currentDialogueIndex]);
                    EventManager.Instance.AddListener<DialogWindowClosed>(DialogueEnd);
                }
                else
                {
                    State = RoamingNPCState.Idle;
                }
            }
        }
    }
    private NPCController Controller;
    private Vector3 Central;
    [HideInInspector]
    public Vector3 TargetPosition;
    [HideInInspector]
    public Vector3 TargetRotation;

    public float MaxMoveRadius = 5f;
    public float MoveDuration = 3;
    public float RestDuration = 1;
    public bool IsRunning = false;
    private float ChangeInterval;
   


    void Awake()
    {
        Controller = GetComponent<NPCController>();
        Central = transform.localPosition;

        // Set random rotation


    }

    void Start()
    {
        State = RoamingNPCState.Move;
    }

	void OnEnable()
	{
		State = RoamingNPCState.Move;
	}

    void Update()
    {
		if(State == RoamingNPCState.Talking)
        {
            Controller.Stop();
            transform.eulerAngles = Vector3.Slerp(transform.eulerAngles, TargetRotation, Time.deltaTime * MoveDuration);
        }
    }

    IEnumerator NewHeading()
    {
            SetInterval();
            yield return new WaitForSeconds(ChangeInterval);
            NewRoutine();
    }

    void NewRoutine()
    {
        if (State == RoamingNPCState.Idle)
        {
            TargetPosition = new Vector3(LegrandUtility.Random(Central.x - MaxMoveRadius, Central.x + MaxMoveRadius)
                                         ,0f
                                         , LegrandUtility.Random(Central.z - MaxMoveRadius, Central.z + MaxMoveRadius)
                                         );

            SetRotationTarget(TargetPosition);

            State = RoamingNPCState.Move;
        }
        else if (State == RoamingNPCState.Move)
        {
            Controller.Stop();
            State = RoamingNPCState.Idle;
        }
    }

    void SetInterval()
    {
        if (State == RoamingNPCState.Idle)
        {
            ChangeInterval = RestDuration;
            
        } 
        else if((State == RoamingNPCState.Move))
        {
            ChangeInterval =  MoveDuration;            
        }
    }

    void SetRotationTarget(Vector3 TargetPosition)
    {
        Vector3 currentRotation = this.transform.eulerAngles;
        transform.LookAt(TargetPosition);
        TargetRotation = this.transform.eulerAngles;
        this.transform.eulerAngles = currentRotation;
    }

    void DialogueEnd(DialogWindowClosed e)
    {
        EventManager.Instance.TriggerEvent(new TalkToPlayerEvent(true));
        EventManager.Instance.RemoveListener<DialogWindowClosed>(DialogueEnd);
        State = RoamingNPCState.Idle;
    }
}
