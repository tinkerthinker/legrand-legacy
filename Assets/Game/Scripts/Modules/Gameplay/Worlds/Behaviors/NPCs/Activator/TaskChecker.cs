﻿using UnityEngine;
using System.Collections;
using Legrand.core;

public class TaskChecker : MonoBehaviour
{
	public string TaskID;

	void OnEnable ()
	{
		Task task = QuestManager.Instance.GetTaskByKey (TaskID);

		if (task == null || task.Status == QuestEnums.Status.Done) {
			this.gameObject.SetActive (false);
		}
	}
}
