﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class StaticNPC : NPCBehaviour {
    public bool FixedPosition = false;

    private Vector3 initialRotation;

	void Start () {
		_Type = NPCType.Static;
		isMoveableNPC = false;

		State = NPC.NPCState.Idle;

        initialRotation = transform.rotation.eulerAngles;
	}

	protected override void OnEnable()
	{
		State = NPCState.Idle;
        Ray r = new Ray(transform.position, -transform.up);
        RaycastHit hit;
        if (Physics.Raycast(r, out hit, LayerMask.NameToLayer("Ground")))
        {
            if (hit.collider.gameObject.tag != "Moving Ground")
                transform.position = new Vector3(transform.position.x, hit.point.y, transform.position.z);
        }
        base.OnEnable();
	}

    protected override void SetRotationTarget(Vector3 TargetPosition, bool isRotateImmediately)
    {
        if (!FixedPosition)
            base.SetRotationTarget(TargetPosition, isRotateImmediately);
        else
            TalkingEventRoutine();
    }

    protected override void DialogueEnd(DialogWindowClosed e)
    {
        base.DialogueEnd(e);
        if (transform.gameObject.activeSelf)
            transform.DORotate(initialRotation, 1f, RotateMode.Fast);
    }
}
