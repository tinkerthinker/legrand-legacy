﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
public class CraftingNPC : NPCBehaviour
{
    public int Level;

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Interaction")
            OpenShop();
    }

	void OpenShop()
	{
        EventManager.Instance.AddListener<CraftingShopEvent>(CloseShop);
		EventManager.Instance.TriggerEvent (new CraftingShopEvent(true,Level));
	}

	void CloseShop(CraftingShopEvent p)
	{
		if (!p.isOpen)
		{
			EventManager.Instance.RemoveListener<CraftingShopEvent> (CloseShop);
		}
	}
}
