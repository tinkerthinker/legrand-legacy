﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NPC : MonoBehaviour {
	public enum NPCState
	{
		Idle,
		Move,
		Talking
	}
	public enum NPCType
	{
		Roaming,
		Rounding,
		Routing,
		Shop,
		Static,
		Quest
	}

    public string ID;
    public List<string> DialogueIdNames;
    protected int currentDialogueIndex = 0; //used to know which index of dialogue is now selected
    protected int _ChoosenDialogueId = -1;   // used to know which Id of dialogue is now selected
    public bool DialogByQuery = false;
    public bool UsingInventoryFact = false;
    public bool MonsterKilledFact = false;
    public List<VectorStringDouble> QueryData;

    public NPCState _StateNPC;
	public GameObject DialogueHelper;
	protected NPCType _Type;

    #region  Shout NPC
    public bool IsShoutNPC;
    protected bool _IsShouting;
    [Header("Shouting NPC use this dialogue")]
    public List<string> ShoutsDialogue;
    public bool IsShoutRandom;
    protected int _IndexShout = 0;
    public float LiveTime = 3f;
    public float CoolDown = 6f;
    public bool StopShoutAfterDialogue = false;

    protected string _SelectedShout;
    #endregion   
}
