﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShoutNPC : NPCBehaviour {
    //private bool _IsShouting;
    //[Header("Shouting NPC use this dialogue")]
    //public List<string> ShoutsDialogue;
    //public bool IsShoutRandom;
    //private int _IndexShout = 0;
    //public float LiveTime = 3f;
    //public float CoolDown = 6f;
    //public bool StopShoutAfterDialogue = false;

    //string _SelectedShout;

	void Start () {
		_Type = NPCType.Static;
		isMoveableNPC = false;
		
		State = NPC.NPCState.Idle;
	}
	
	void OnEnable()
	{
		State = NPCState.Idle;

		if(ShoutsDialogue.Count >0)
			StartCoroutine (Shouting ());
	}

	void OnDisable()
	{
		_IsShouting = false;
	}

	protected override void TalkingEvent ()
	{
		_IsShouting = false;
		EventManager.Instance.TriggerEvent (new CloseShoutPopUpEvent (_SelectedShout));
		base.TalkingEvent ();
	}

    protected override void DialogueEnd(DialogWindowClosed e)
    {
        base.DialogueEnd(e);
        if(!StopShoutAfterDialogue)
            StartCoroutine(WaitToShout(1f));
    }

    IEnumerator WaitToShout(float time)
    {
        yield return WaitFor.RealTime(time);
        StartCoroutine(Shouting());
    }

	IEnumerator Shouting()
	{
        _IsShouting = true;
		while (_IsShouting) {
			yield return new WaitForSeconds(CoolDown/2f);
			//untuk sementara dibikin balonnya selalu shout
			if (_IsShouting) {
				if (IsShoutRandom)
					_SelectedShout = ShoutsDialogue [LegrandUtility.Random(0, ShoutsDialogue.Count)];
				else {
                    _SelectedShout = ShoutsDialogue[_IndexShout];
                    if (++_IndexShout >= ShoutsDialogue.Count)
						_IndexShout = 0;
				}
                string translate = LegrandBackend.Instance.GetLanguageData(_SelectedShout);
                _SelectedShout = string.IsNullOrEmpty(translate)?_SelectedShout:translate;
				PopUpUI.CallDialoguePopUp ("", _SelectedShout, DialoguePopUp.BalloonType.Shout, DialogueHelper ? DialogueHelper : gameObject, LiveTime);
			}
			yield return new WaitForSeconds(LiveTime+CoolDown/2f);
		}
	}
}
