﻿using UnityEngine;
using System.Collections.Generic;

public class FactChecker : MonoBehaviour {
    public FactCheckerValidator[] Validators;
    public bool DefaultActive = false;
    private List<GameObject> _Childs;
    void Awake()
    {
        EventManager.Instance.AddListener<RefreshRoomEvent>(Check);
        _Childs = new List<GameObject>();
        foreach(Transform t in transform)
            if (t.parent == this.transform) _Childs.Add(t.gameObject);
    }

    void Start()
    {
        Check(null);
    }

    void OnEnable()
    {
        Check(null);
    }

    void OnDestroy()
    {
        if(EventManager.Instance != null)
        EventManager.Instance.RemoveListener<RefreshRoomEvent>(Check);
    }

    void Check(RefreshRoomEvent e)
    {
        if (!gameObject.activeInHierarchy) return;

        bool found = false;
        List<Fact> latestFact = PartyManager.Instance.WorldInfo.LatestFacts;
        for (int i = 0; i < Validators.Length && !found; i++)
        {
            FactCheckerValidator validator = Validators[i];
            bool validatorFail = false;
            for (int j = 0; j < validator.Criterion.Length && !validatorFail; j++)
            {
                double value = 0;
                Fact f = latestFact.Find(x => x.Name == validator.Criterion[j].Criterion);
                if (f != null) value = f.Value;
                if (!validator.Criterion[j].Compare(value)) validatorFail = true;
            }
            if(!validatorFail)
            {
                SetActiveChild(validator.SetActiveIfSuccess);
                found = true;
            }
        }
        if (!found) SetActiveChild(DefaultActive);
    }

    void SetActiveChild(bool active)
    {
        foreach(GameObject g in _Childs)
           g.SetActive(active);
    }
}

[System.Serializable]
public class FactCheckerValidator
{
    public CriterionStatic[] Criterion;
    public bool SetActiveIfSuccess = true;
}
