﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
public class AlchemistNPC : NPCBehaviour
{
    public int Level;

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Interaction")
            OpenShop();
    }

	void OpenShop()
	{
        EventManager.Instance.AddListener<AlchemistShopEvent>(CloseShop);
		EventManager.Instance.TriggerEvent (new AlchemistShopEvent(true,Level));
	}

	void CloseShop(AlchemistShopEvent p)
	{
		if (!p.isOpen)
		{
			EventManager.Instance.RemoveListener<AlchemistShopEvent> (CloseShop);
		}
	}
}
