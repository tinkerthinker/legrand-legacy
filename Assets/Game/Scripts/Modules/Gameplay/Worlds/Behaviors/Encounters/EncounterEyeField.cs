﻿using UnityEngine;
using System.Collections;

public class EncounterEyeField : MonoBehaviour {
    public delegate void onTrigger(Transform objectOnTrigger);
    public onTrigger TriggerEnter;
    public onTrigger TriggerExit;

    void OnDestroy()
    {
        TriggerEnter = null;
        TriggerExit = null;
    }

    void OnTriggerEnter(Collider c)
    {
        if(TriggerEnter != null)
        {
            TriggerEnter(c.transform);
        }
    }

    void OnTriggerExit(Collider c)
    {
        if (TriggerEnter != null)
        {
            TriggerExit(c.transform);
        }
    }
}
