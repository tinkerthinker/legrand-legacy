﻿using UnityEngine;
using System.Collections;

public class MovingEncounterBehavior : EncounterBehavior {
    enum MovingEncounterState
    {
        Init,
        Moving,
        Follow,
        RestFromFollow
    }

    private MovingEncounterState _State;
    private IEnumerator _ActionTimer;

    private Vector3 Wanderarget;
    private Transform PersonOfInterest;
    private Transform FollowTarget;

    void OnEnable()
    {
        Init();
    }

    void Update()
    {
        if (!IsActive) return;

        if(_State == MovingEncounterState.Moving)
        {
            Vector3 target = new Vector3(Wanderarget.x, transform.position.y, Wanderarget.z);
            Ray myRay = new Ray(transform.position, transform.forward); // cast a Ray from the position of our gameObject into our desired direction.
            RaycastHit hit;

            if (Vector3.Distance(target, transform.position) > 2f)
            {
                Rotate(target);
                if(!Physics.Raycast(myRay, out hit, 1f, LayerMask.GetMask("Wall")))
                   transform.position += transform.forward * _MoveSpeed * Time.deltaTime;
            }
        }
        else if(_State == MovingEncounterState.Follow && FollowTarget != null)
        {
            Vector3 target = new Vector3(FollowTarget.position.x, transform.position.y, FollowTarget.position.z);
            Rotate(target);
            Vector3 savePosition = transform.position;
            transform.position += transform.forward * _FollowSpeed * Time.deltaTime;
            if(!NavArea.ContainsPoint(transform.position))
            {
                transform.position = savePosition;
            }
        }
    }


    public override void Init()
    {
        if (gameObject.activeInHierarchy)
        {
            _State = MovingEncounterState.Init;
            Rest(LegrandUtility.Random(0f, 1f));
        }
    }

    public void Rest(float time)
    {
        FollowTarget = null;
        _State = MovingEncounterState.Init;
        if (_ActionTimer != null)
        {
            StopCoroutine(_ActionTimer);
            _ActionTimer = null;
        }
        _ActionTimer = WaitToStart(time);
        StartCoroutine(_ActionTimer);
    }

    public void RestFromFollow(float time)
    {
        FollowTarget = null;
        _State = MovingEncounterState.RestFromFollow;
        if (_ActionTimer != null)
        {
            StopCoroutine(_ActionTimer);
            _ActionTimer = null;
        }
        _ActionTimer = WaitToStart(time);
        StartCoroutine(_ActionTimer);
    }

    public override void SeePlayer(Transform Player)
    {
        if (Player.tag != "PartyLeader") return;
        PersonOfInterest = Player;
        FollowTarget = PersonOfInterest;
        if(_State != MovingEncounterState.RestFromFollow)
           ChangeToFollowState();
    }

    public override void LostPlayer(Transform Player)
    {
        if (Player.tag != "PartyLeader") return;
        PersonOfInterest = null;
    }

    IEnumerator WaitToStart(float time)
    {
        yield return new WaitForSeconds(time);

        if (PersonOfInterest == null)
            ChangeToMovingState();
        else
        {
            FollowTarget = PersonOfInterest;
            ChangeToFollowState();
        }
    }

    IEnumerator WaitToRest(float time)
    {
        yield return new WaitForSeconds(time);
        Rest(LegrandUtility.Random(2f,3f));
    }

    IEnumerator WaitToRestFromFollow(float time)
    {
        yield return new WaitForSeconds(time);
        RestFromFollow(3f);
    }

    public void ChangeToFollowState()
    {
        _State = MovingEncounterState.Follow;
        if (_ActionTimer != null)
        {
            StopCoroutine(_ActionTimer);
            _ActionTimer = null;
        }
        _ActionTimer = WaitToRestFromFollow(3f);
        StartCoroutine(_ActionTimer);
    }

    public void ChangeToMovingState()
    {
        Vector3 v = NavArea.coords[LegrandUtility.Random(0, NavArea.coords.Count)];
        Wanderarget = LegrandUtility.Random(0f, 1f) * v + NavArea.transform.position;

        _State = MovingEncounterState.Moving;
        if (_ActionTimer != null)
        {
            StopCoroutine(_ActionTimer);
            _ActionTimer = null;
        }
        _ActionTimer = WaitToRest(4f);
        StartCoroutine(_ActionTimer);
    }
}
