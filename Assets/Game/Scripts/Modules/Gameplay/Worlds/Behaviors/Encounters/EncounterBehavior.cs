﻿using UnityEngine;
using System.Collections;
using RAIN.Core;

public class EncounterBehavior : MonoBehaviour {

    public bool IsActive;
    public float DefaultMoveSpeed = 2f;
    public float DefaultFollowSpeed = 4.3f;
    protected float _turnSmoothing = 15f;
    public AreaCollider NavArea;
    public float _MoveSpeed;
    public float _FollowSpeed;
    protected bool IsStop;

    protected EncounterEyeField Eye;

    Animator Anim;
	//private AIRig RainRig;

	void Awake()
	{
		Anim = GetComponent<Animator> ();
        _MoveSpeed = DefaultMoveSpeed;
        _FollowSpeed = DefaultFollowSpeed;
		EventManager.Instance.AddListener<PauseEvent> (Pause);
		EventManager.Instance.AddListener<UnPauseEvent> (UnPause);
        Eye = LegrandUtility.GetComponentInChildren<EncounterEyeField>(this.gameObject);
        if (Eye != null)
        {
            Eye.TriggerEnter += SeePlayer;
            Eye.TriggerExit += LostPlayer;
        }
	}

	void OnDestroy()
	{
		if (EventManager.Instance != null) {
			EventManager.Instance.RemoveListener<PauseEvent> (Pause);
			EventManager.Instance.RemoveListener<UnPauseEvent> (UnPause);
		}
	}

	void Pause(PauseEvent e)
	{
        //if(RainRig == null) RainRig = GetComponentInChildren<AIRig> ();

        _FollowSpeed = 0;
        _MoveSpeed = 0;

		//if (RainRig != null) {
		//	RainRig.AI.WorkingMemory.SetItem ("moveSpeed", 0f);
		//	RainRig.AI.WorkingMemory.SetItem ("followSpeed", 0f);
		//}


		if(Anim != null) Anim.enabled = false;
	}

	void UnPause(UnPauseEvent e)
	{
        //if(RainRig == null) RainRig = GetComponentInChildren<AIRig> ();

        _FollowSpeed = DefaultFollowSpeed;
        _MoveSpeed = DefaultMoveSpeed;

        //if (RainRig != null) {
        //	RainRig.AI.WorkingMemory.SetItem ("moveSpeed", 2f);
        //	RainRig.AI.WorkingMemory.SetItem ("followSpeed", 3.5f);
        //}

        if (Anim != null) Anim.enabled = true;
	}

    public virtual void Init()
    {

    }

    public virtual void SeePlayer(Transform Player)
    {
        if (Player.tag != "PartyLeader") return;
    }

    public virtual void LostPlayer(Transform Player)
    {
        if (Player.tag != "PartyLeader") return;
    }

    protected void Rotate(Vector3 direction)
    {
        Quaternion rotation = Quaternion.LookRotation(direction - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * _turnSmoothing);
        transform.eulerAngles = new Vector3(0f,transform.eulerAngles.y,0f);
    }
}
