﻿using UnityEngine;
using System.Collections;

public class WorldMapInfo : MonoBehaviour
{
    public GameObject[] PointsToUnlock;
    void OnEnable()
    {
        for (int i = 0; i < PointsToUnlock.Length; i++)
        {
            if (AreaController.Instance != null && !AreaController.Instance.UnlockAllPoint)
            {
                int index = -1;
                for (int ii = 0; ii < PartyManager.Instance.WorldInfo.UnlockedWorldMapPoint.Count; ii++)
                {
                    if (PartyManager.Instance.WorldInfo.UnlockedWorldMapPoint[ii].name.Equals(PointsToUnlock[i].name))
                    {
                        index = ii;
                        break;
                    }
                }
                if (index > -1)
                {
                    PointsToUnlock[i].transform.parent.gameObject.SetActive(true);
                    if (!PartyManager.Instance.WorldInfo.UnlockedWorldMapPoint[i].isVisited)
                    {
                        Debug.Log(PointsToUnlock[i].name + " is new");
                    }
                }
                else
                {
                    PointsToUnlock[i].transform.parent.gameObject.SetActive(false);
                }
            }
            else
                PointsToUnlock[i].transform.parent.gameObject.SetActive(true);
        }
    }
}
