﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

public class AreaController : MonoBehaviour 
{
    #region static fields
    
    public static readonly string CITY_RESOURCE_PATH = "Worlds/Area/Cities/";
    public static readonly string DUNGEON_RESOURCE_PATH = "Worlds/Area/Dungeons/";
    public static readonly string WORLDMAP_RESOURCES_PATH = "Worlds/Area/World Map/";
    public static readonly string MONSTER_RESOURCES_PATH = "Characters/Encounters/";
    
    #endregion

    #region singleton
    
    private static AreaController _instance;    
    public static AreaController Instance
    {
        get
        {
            if(_instance == null)
                _instance = GameObject.FindObjectOfType<AreaController>();
            
            return _instance;
        }
    }
    
    #endregion

    #region all area data

    public Area[] areaData;

    #endregion

    public ScreenFader ScreenFader;

    private Area _currAreaData;
    public Area CurrAreaData
    {
        get { return _currAreaData; }
    }

    private SubArea _currSubAreaData;
	public SubArea CurrSubAreaData 
	{
		get { return _currSubAreaData; }
	}
    private SubArea _prevSubAreaData;

	private string _currPortal;
    public string CurrPortal
    {
        get { return _currPortal; }
    }

    private string[] _unlockPoint;
    private WwiseManager.WorldFX _currSFX;
    private GameObject _areaContainer, _monsterContainer;

    private GameObject _currPlayer;
	public GameObject CurrPlayer 
	{
		get { return _currPlayer; }
	}   
   	
	private GameObject _currPrefab;
	public GameObject CurrPrefab
	{
		get { return _currPrefab; }
	}

	private RoomController _currRoom;
	public RoomController CurrRoom 
	{
		get { return _currRoom; }
	}

    private GameObject _fencingMinigameObject;

    private CitymapController _currCitymap;
    public CitymapController CurrCitymap
    {
        get { return _currCitymap; }
    }
    private Dictionary<string, string> FastTravelData;

    // Global setting for portal indicator
    [HideInInspector]
    public bool portalIndicatorActive = false;

	public bool active = true;
    
    public bool UnlockAllPoint = false;

    public bool DisableBattle = false;

    public bool DisableBGM = false;

    public bool DisableZoomOut = false;

	public string PlayingCutscene;

    public bool insideHealPoint;

    public string CurrentRegion;

	public MapDataConverter mapData;

    public delegate void AreaDelegate();
    public static event AreaDelegate OnSubAreaLoaded;
    public static event AreaDelegate OnSubAreaUnloaded;

    void Awake()
    {
		areaData = mapData.GetData();
        FastTravelData = mapData.GetFastTravelData();

        _currPlayer = GameObject.FindGameObjectWithTag("PartyLeader");

        _areaContainer = new GameObject("Areas");
        _areaContainer.transform.parent = transform;
        _monsterContainer = new GameObject("Monsters");
        _monsterContainer.transform.parent = transform;
	}
    
    public string GetFastTravelDestination(string areaName)
    {
        return FastTravelData[areaName];
    }

    /// <summary>
    /// Get an area by its name
    /// </summary>
    /// <param name="pAreaName">Area name to look for</param>
    Area GetAreaData(string pAreaName)
    {
        foreach (Area area in areaData)
            if(area.areaName == pAreaName) return area;     
        
        return null;
    }

    /// <summary>
    /// Get an area by its sub area name
    /// </summary>
    /// <param name="pSubAreaName">Sub Area name that resides within corresponding area</param>
    public Area GetAreaDataBySubArea(string pSubAreaName)
    {
        for (int i = 0; i < areaData.Length; i++)
        {
            for (int j = 0; j < areaData[i].subArea.Length; j++)
            {
                if(LegrandUtility.CompareString(areaData[i].subArea[j].prefabName, pSubAreaName))
                    return areaData[i];
            }
        }

        return null;
    }

    public void StartWorld()
    {
        _currAreaData = GetAreaData (PartyManager.Instance.State.gameArea);
		_currSubAreaData = _currAreaData.GetSubAreasByName (PartyManager.Instance.State.gameSubArea);
		_currPortal = PartyManager.Instance.State.gamePortal; //untuk save state jika gameover

        EnableListener();
        StartCoroutine(StartWorldRoutine());
    }

    void EnableListener()
    {
        EventManager.Instance.AddListener<UnlockCityPointsEvent>(UnlockCityPoints);
        EventManager.Instance.AddListener<UnlockWorldMapPointEvent>(UnlockWorldMapPoints);
        EventManager.Instance.AddListener<TogglePortalIndicatorEvent>(TogglePortalIndicators);
        EventManager.Instance.AddListener<ModRoomVersionEvent>(ModRoomVersion);
        EventManager.Instance.AddListener<TeleportPlayerEvent>(TeleportPlayer);
        EventManager.Instance.AddListener<MinigameNpcEvent>(AssignMinigameId);
        DynamicSystemController._Instance.RuleDB.AddChangeAreaListener(CurrAreaData);
    }

    IEnumerator StartWorldRoutine()
    {
        while (PoolingSystem.Instance == null)
            yield return null;

        if (PartyManager.Instance.State.currPhase == (int)PartyManager.StoryPhase.World)
        {
			ScreenFader.TransToBlack();
			StartCoroutine(LoadSavedGameSubAreasRoutine());
        }
        else
        {
			StartCoroutine(LoadAllSubAreasRoutine());
        }
    }

    /// <summary>
    /// Load all prefabs in current area
    /// </summary>
    void LoadAllSubAreas()
    {
        if (ScreenFader.OnFadeOut != null)
            ScreenFader.OnFadeOut -= LoadAllSubAreas; //delegate function

        EventManager.Instance.TriggerEvent(new LoadAreaEvent(_currAreaData));

        StartCoroutine(LoadAllSubAreasRoutine());
    }

    IEnumerator LoadAllSubAreasRoutine()
    {
        DestroyOldSubAreas();
		InstantiateSubAreas ();
		yield return null;

        DrawSubAreaFromPool();
        if (_currSubAreaData.subAreaType == SubArea.Types.Room)
            SpawnPlayerAtPortal();
        else
            ScreenFader.StartScene();

        StartCoroutine(ChangePlayerController());
    }

    IEnumerator LoadSavedGameSubAreasRoutine()
    {
        DestroyOldSubAreas();
		InstantiateSubAreas ();
		yield return null;

        DrawSubAreaFromPool();
        SpawnPlayerAtSavePortal();

        StartCoroutine(ChangePlayerController());
    }
    
    void ActivateNewSubArea()
    {
        if (ScreenFader.OnFadeOut != null)
            ScreenFader.OnFadeOut -= ActivateNewSubArea;

        if (_currSubAreaData.subAreaType == SubArea.Types.CityMap)
            StartCoroutine(ActivateCityMapRoutine());
        else if (_currSubAreaData.subAreaType == SubArea.Types.Room)
            StartCoroutine(ActivateNewRoomRoutine());
    }

    IEnumerator ActivateCityMapRoutine()
    {
        EnablePlayerGravity(false);
        ReturnSubAreaToPool();
        yield return null;

        DrawSubAreaFromPool();
        ScreenFader.StartScene();
    }

    IEnumerator ActivateNewRoomRoutine()
    {
        ReturnSubAreaToPool();
        yield return null;
        
        DrawSubAreaFromPool();
        WwiseManager.Instance.PlaySFX(_currSFX);
        EnablePlayerGravity(true);
        SpawnPlayerAtPortal();
    }

    void DestroyOldSubAreas()
    {
        if (PoolingSystem.Instance.pooledItems == null || PoolingSystem.Instance.pooledItems.Length == 0)
            return;

        for (int i = 0; i < PoolingSystem.Instance.pooledItems.Length; i++)
            Destroy(PoolingSystem.Instance.pooledItems[i][0].gameObject);

        Array.Clear(PoolingSystem.Instance.poolingItems, 0, PoolingSystem.Instance.poolingItems.Length);
        Array.Clear(PoolingSystem.Instance.pooledItems, 0, PoolingSystem.Instance.pooledItems.Length);
    }

    /// <summary>
    /// Pool all sub areas in current area
    /// </summary>
    void InstantiateSubAreas()
    {
        //PoolingSystem.Instance.poolingItems = new PoolingSystem.PoolingItems[_currAreaData.subArea.Length + _currAreaData.dungeonMonsters.Length];
        PoolingSystem.Instance.poolingItems = new PoolingSystem.PoolingItems[_currAreaData.subArea.Length];

        for (int i = 0; i < _currAreaData.subArea.Length; i++)
        {
            PoolingSystem.Instance.poolingItems[i] = new PoolingSystem.PoolingItems();
			PoolingSystem.Instance.poolingItems[i].prefab = Resources.Load<GameObject>(GenerateResourcePath(i));
            PoolingSystem.Instance.poolingItems[i].parent = _areaContainer;
            PoolingSystem.Instance.poolingItems[i].amount = 1;
        }

   //     int monsterIndex = 0;
   //     for (int i = _currAreaData.subArea.Length; i < PoolingSystem.Instance.poolingItems.Length; i++)
   //     {
   //         PoolingSystem.Instance.poolingItems[i] = new PoolingSystem.PoolingItems();
			//PoolingSystem.Instance.poolingItems[i].prefab = Resources.Load<GameObject>(MONSTER_RESOURCES_PATH + _currAreaData.dungeonMonsters[monsterIndex].monsterID);
   //         PoolingSystem.Instance.poolingItems[i].parent = _monsterContainer;
   //         PoolingSystem.Instance.poolingItems[i].amount = 1;
   //         monsterIndex++;
   //     }

        PoolingSystem.Instance.Init();

        for (int j = 0; j < _currAreaData.subArea.Length; j++)
        {
            SubArea tempSubAreaData = _currAreaData.subArea[j];
            GameObject tempSubAreaObject = PoolingSystem.Instance.pooledItems[j][0];//untuk ambil prefab dari area
            PoolingSystem.Instance.pooledItems[j][0].name = tempSubAreaData.prefabName;
            InitializeSubAreasObject(tempSubAreaData, tempSubAreaObject);
        }
    }

    void InitializeSubAreasObject(SubArea clData, GameObject clObject)
    {
        SubAreaController tempSubAreaController;

        if (clData.subAreaType == SubArea.Types.CityMap)
            tempSubAreaController = clObject.AddComponent<CitymapController>();
        else
            tempSubAreaController = clObject.AddComponent<RoomController>();

        tempSubAreaController.subArea = clData;

        if (!active) clObject.SetActive(false); 
    }
    
    void DrawSubAreaFromPool()
    {
        _currPrefab = PoolingSystem.Instance.InstantiateAPS(_currSubAreaData.prefabName);
        if (OnSubAreaLoaded != null)
            OnSubAreaLoaded();

        if (_currSubAreaData.subAreaType == SubArea.Types.Room)
        {
            if (_currPrefab.gameObject.GetComponent<FencingSetupArea>() != null)
            {
                _fencingMinigameObject = _currPrefab.gameObject.GetComponent<FencingSetupArea>().MinigameBattle;
            }
            _currRoom = _currPrefab.GetComponent<RoomController>();
        }
        else if (_currSubAreaData.subAreaType == SubArea.Types.CityMap)
            _currCitymap = _currPrefab.GetComponent<CitymapController>();

		if(!active) _currPrefab.SetActive(false);
    }
    
    void ReturnSubAreaToPool()
    {
        if(OnSubAreaUnloaded != null)
            OnSubAreaUnloaded();
        PoolingSystem.DestroyAPS(_currPrefab);
        _currPrefab = null;
    }

    void SpawnPlayerAtPortal()
    {
        PortalTrigger portalTrigger = _currRoom.GetPortalTrigger(_currPortal);
        WorldMapPortalTrigger worldMapPortalTrigger = _currRoom.GetWorldMapPortalTrigger(_currPortal);

        if (worldMapPortalTrigger != null)
        {
            _currPlayer.transform.position = worldMapPortalTrigger.transform.position + worldMapPortalTrigger.arriveOffset;
            _currPlayer.transform.rotation = worldMapPortalTrigger.transform.rotation;
            
            CameraWorldMap worldMapCam = CurrPrefab.GetComponentInChildren<CameraWorldMap>();
            if (worldMapCam != null)
                worldMapCam.SetStartPosition();
        }
        if (portalTrigger != null)
        {
            _currPlayer.transform.position = portalTrigger.transform.position + portalTrigger.arriveOffset;
            _currPlayer.transform.rotation = portalTrigger.transform.rotation;
        }

        Ray r = new Ray(_currPlayer.transform.position, -_currPlayer.transform.up);
        RaycastHit hit;
        if (Physics.Raycast(r, out hit, LayerMask.NameToLayer("Ground")))
        {
            if(hit.collider.gameObject.tag != "Moving Ground")
              _currPlayer.transform.position = new Vector3(_currPlayer.transform.position.x, hit.point.y, _currPlayer.transform.position.z);
        }

        EventManager.Instance.TriggerEvent (new LastPositionCheckEvent (_currPlayer.transform.position, _currPlayer.transform.eulerAngles));           

		if (active)
			_currRoom.SetTriggersActive (true);
		else 
		{
			_currPlayer.SetActive (false);
			_currRoom.Initialized = true;
		}
		EventManager.Instance.TriggerEvent (new PlayerMoveAreaEvent (_currAreaData, _currSubAreaData, _currPortal));
        // Fade in when player is already at the position

		if (PlayingCutscene != "") 
		{
			/*
			Dalam hal ini harus dicek apakah cutscene tersebut memakai tempat background sekarang 
			atau background yang berbeda.
			Untuk mengetahui itu, cutscene yang memakai background yang berbeda 
			memiliki kelas sequence player dan sistem perlu menonaktifkan current background.
			Caranya adalah dengan mengubah status game menjadi cutscene dengan menggunakan worldscenemanager
			*/
			WorldSceneManager._Instance.ScreenFader.OnFadeOut += ScreenFaderClear;
			WorldSceneManager.TransWorldToSequence(_currPrefab, _currPlayer, PlayingCutscene);
			PlayingCutscene = "";
		}

		if (active)
        {
            ScreenFader.OnFadeIn += EnablePlayers;
            ScreenFader.StartScene ();
		}
    }

	void SpawnPlayerAtSavePortal()
	{
//		SaveGameTrigger saveTrigger = _currSubArea.GetSaveTrigger(_currPortal);
//		_currPlayer.transform.position = saveTrigger.transform.position;
		_currPlayer.transform.position = new Vector3 (LegrandBackend.Instance.State.PosX,LegrandBackend.Instance.State.PosY,LegrandBackend.Instance.State.PosZ);
		_currPlayer.transform.rotation = new Quaternion (LegrandBackend.Instance.State.RotX,LegrandBackend.Instance.State.RotY,LegrandBackend.Instance.State.RotZ,LegrandBackend.Instance.State.RotW);

		if (active)
			_currRoom.SetTriggersActive (true);
		else {
			_currPlayer.SetActive (false);
			_currRoom.Initialized = true;
		}
		EventManager.Instance.TriggerEvent (new PlayerMoveAreaEvent (_currAreaData, _currSubAreaData, _currPortal));
		// Fade in when player is already at the position
		if (active) {
            ScreenFader.OnFadeIn += EnablePlayers; //ini kenapa di += ?
            ScreenFader.StartScene ();
		}
	}

    void EnablePlayers()
    {
        ScreenFader.OnFadeIn -= EnablePlayers; //menghilangkan fungsi OnFadeIn di pooling delegasi
		// Check if need to play cutscene
		if (PlayingCutscene == "")
		{
			_currPlayer.GetComponent<PlayerController> ().ControllerState = PlayerController.PlayerControllerState.Normal;
            //_currPlayer.GetComponent<WorldMapPlayerController>().controllerState = WorldMapPlayerController.WorldMapPlayerControllerState.Normal;
        }
        else 
		{
			_currPlayer.GetComponent<PlayerController> ().ControllerState = PlayerController.PlayerControllerState.Cutscene;
			PlayingCutscene = "";
		}
    }  
	
    /// <summary>
    /// Get prefab's path separated by area type
    /// </summary>
    /// <param name="index">Index of sub area</param>
    /// <returns>Prefab's path</returns>
	string GenerateResourcePath(int index)
    {
        string path = "";

        if(_currAreaData.areaType == Area.AreaTypes.City)
            path = CITY_RESOURCE_PATH;
        else if(_currAreaData.areaType == Area.AreaTypes.Dungeon)
            path = DUNGEON_RESOURCE_PATH;
        else if(_currAreaData.areaType == Area.AreaTypes.WorldMap)
            path = WORLDMAP_RESOURCES_PATH;
        
        return path + _currAreaData.areaName + "/" + _currAreaData.subArea[index].prefabName;
    }

    /// <summary>
    /// Called when player moves from one sub area to another within the same area.
    /// </summary>
    /// <param name="pPlayer">Player that moves through the portal</param>
    /// <param name="pSubAreaDestination">Sub Area to which the portal connects</param>
    /// <param name="pPortalDestination">Name of the portal player arrives at</param>
    /// <param name="pUnlockPoint">Citymap points to unlock</param>
    /// <param name="sfx">Sound effect to play right after colliding with portal</param>
    public void MoveToSubArea(GameObject pPlayer, string pSubAreaDestination, string pPortalDestination, string[] pUnlockPoint = null, WwiseManager.WorldFX sfx = WwiseManager.WorldFX.NONE)
    {
        _prevSubAreaData = _currSubAreaData;
        _currPlayer = pPlayer;
        _currSubAreaData = _currAreaData.GetSubAreasByName(pSubAreaDestination);
        _currPortal = pPortalDestination;
        _currSFX = sfx;

        if (active) {
			ScreenFader.EndScene ();
			ScreenFader.OnFadeOut += ActivateNewSubArea;
		} else
            ActivateNewSubArea();
    }
            
    public bool IsSameArea(string destination)
    {
        SubArea[] allSubAreas = _currAreaData.subArea;
        
        foreach (SubArea subArea in allSubAreas)
            if(subArea.prefabName == destination) return true;
        
        return false;
    }

    /// <summary>
    /// Called when player moves from one area to another.
    /// e.g. Tel Harran -> Rahas Desert, Shapur -> World Map
    /// </summary>
    /// <param name="pPlayer">Player that moves through the portal</param>
    /// <param name="pSubAreaDestination">Sub Area to which the portal connects</param>
    /// <param name="pPortalDestination">Name of the portal player arrives at</param>
    public void ChangeArea(GameObject pPlayer, string pSubAreaDestination, string pPortalDestination)
    {
        _currPlayer = pPlayer;
        _currAreaData = GetAreaDataBySubArea(pSubAreaDestination);
        _currSubAreaData = _currAreaData.GetSubAreasByName(pSubAreaDestination);
        _currPortal = pPortalDestination;
        
        if (active)
        {
            ScreenFader.EndScene();
            ScreenFader.OnFadeOut += LoadAllSubAreas;
        }
        else
        {
            LoadAllSubAreas();
        }

        if (FastTravelData.ContainsKey(_currAreaData.areaName))
        {
            if (PartyManager.Instance.WorldInfo.UnlockedFastTravelPoints.Find(x => x.Name.Equals(_currAreaData.areaName)) == null)
            {
                PartyManager.Instance.WorldInfo.UnlockedFastTravelPoints.Add(new FastTravelAttribute( _currAreaData.areaName, _currAreaData.areaType));
            }
        }
    }   

    void EnablePlayerGravity(bool value)
    {
		if (_currPlayer != null)
			LegrandUtility.GetComponentInChildren<Rigidbody> (_currPlayer).useGravity = value;
    }

    // Wait for a frame to finish to prevent OnDisable and OnEnable from being executed at the same time
	IEnumerator ChangePlayerController()
	{
        _currPlayer.GetComponent<PlayerController>().enabled = true;

        if (_currSubAreaData.subAreaType == SubArea.Types.CityMap)
            EnablePlayerGravity(false);
        else
            EnablePlayerGravity(true);

        yield return null;


        //if(_currAreaData.areaType == Area.AreaTypes.WorldMap)
        //{
        //	//_currPlayer.GetComponent<PlayerController> ().enabled = false;
        //          yield return null;
        //	//_currPlayer.GetComponent<WorldMapPlayerController> ().enabled = true;
        //}
        //else
        //      {
        //          //_currPlayer.GetComponent<WorldMapPlayerController>().enabled = false;
        //          yield return null;
        //          _currPlayer.GetComponent<PlayerController> ().enabled = true;

        //          if (_currAreaData.areaType != Area.AreaTypes.WorldMap)
        //          {
        //              // If player arrives in a citymap, disable gravity. Else, enable gravity
        //              if (_currSubAreaData.subAreaType == SubArea.Types.CityMap)
        //                  EnablePlayerGravity(false);
        //              else
        //                  EnablePlayerGravity(true);
        //          }
        //      }
    }
    
    void UnlockCityPoints(UnlockCityPointsEvent e)
    {
		List<CityPointInfo> cityPointList = PartyManager.Instance.WorldInfo.UnlockedCityPoints;

        if(e.pointsToUnlock.Length > 0)
        {
            foreach(string pointName in e.pointsToUnlock)
            {
                if(pointName.Length > 0 && cityPointList.Find(item => item.name == pointName) == null)
                    cityPointList.Add(new CityPointInfo(pointName));
            }
        }
    }

    void UnlockWorldMapPoints(UnlockWorldMapPointEvent e)
    {
        List<WorldMapPointInfo> worldMapPointList = PartyManager.Instance.WorldInfo.UnlockedWorldMapPoint;

        if (e.pointsToUnlock.Length > 0)
        {
            foreach (string pointName in e.pointsToUnlock)
            {
                if (pointName.Length > 0 && worldMapPointList.Find(item => item.name == pointName) == null)
                    worldMapPointList.Add(new WorldMapPointInfo(pointName));
            }
        }
    }

    void ScreenFaderClear()
	//	Clear Screen fader setelah transisi ke background cutscene
	{
		WorldSceneManager._Instance.ScreenFader.OnFadeOut -= ScreenFaderClear;
		ScreenFader.TransToClear ();
	}

    void TogglePortalIndicators(TogglePortalIndicatorEvent e)
    {
        portalIndicatorActive = e.isRendered;
        _currRoom.RenderPortalIndicators(e.isRendered);
    }
    
    // Obsolete function, no longer used
    void ModRoomVersion(ModRoomVersionEvent e)
    {
		List<SubAreaInfo> dynamicSubAreaList = PartyManager.Instance.WorldInfo.DynamicSubArea;
        
        if (e.modInfo.name.Length > 0)
        {
            SubAreaInfo sai = dynamicSubAreaList.Find(item => LegrandUtility.CompareString(item.name, e.modInfo.name));

            if (sai != null)
                sai.version = e.modInfo.version;
            else
                dynamicSubAreaList.Add(e.modInfo);
        }

        if (e.modCurrentRoom)
            EventManager.Instance.TriggerEvent(new TeleportPlayerEvent(CurrSubAreaData.prefabName));//yang dipanggil buat fast travel
    }
    
    void TeleportPlayer(TeleportPlayerEvent e)
    {
        if (IsSameArea(e.targetPlace))
        {
            MoveToSubArea(CurrPlayer, e.targetPlace, e.targetPortal);
        }
        else
        {
            ChangeArea(CurrPlayer, e.targetPlace, e.targetPortal);
        }
    }

    void AssignMinigameId(MinigameNpcEvent e)
    {
        if(_fencingMinigameObject != null)
        {
            FencingController controller = _fencingMinigameObject.GetComponent<FencingController>();
            controller.Enemy.GetComponent<FencingParticipantStatus>().ParticipantId = e.npcID;
            controller.Enemy.GetComponent<FencingParticipantStatus>().InitData();
        }
        
    }
}