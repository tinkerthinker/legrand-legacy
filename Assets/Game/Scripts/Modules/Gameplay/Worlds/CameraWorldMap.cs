﻿using UnityEngine;
using System.Collections;

public class CameraWorldMap : MonoBehaviour
{
    public Vector2 MinMaxHorizontal;
    public Vector2 MinMaxVertical;

    public float YValue;
    public float Zvalue;

    public void SetStartPosition()
    {
        Vector3 targetFollow = AreaController.Instance.CurrPlayer.transform.position;
        Vector3 targetPosition = new Vector3();

        if (targetFollow.z < MinMaxVertical.x)
            targetPosition.z = MinMaxVertical.x + Zvalue;
        else if (targetFollow.z > MinMaxVertical.y)
            targetPosition.z = MinMaxVertical.y + Zvalue;
        else
            targetPosition.z = Zvalue;

        if (targetFollow.x < MinMaxHorizontal.x)
            targetPosition.x = MinMaxHorizontal.x;
        else if (targetFollow.x > MinMaxHorizontal.y)
            targetPosition.x = MinMaxHorizontal.y;

        targetPosition.y = targetFollow.y + YValue;

        transform.position = targetPosition;
    }

    void LateUpdate()
    {
        Vector3 targetFollow = AreaController.Instance.CurrPlayer.transform.position;
        Vector3 targetPosition = new Vector3();

        if (targetFollow.z > MinMaxVertical.x && targetFollow.z < MinMaxVertical.y)
            targetPosition.z = targetFollow.z + Zvalue;
        else
        {
            targetPosition.z = transform.position.z;
        }

        if (targetFollow.x > MinMaxHorizontal.x && targetFollow.x < MinMaxHorizontal.y)
            targetPosition.x = targetFollow.x;
        else
        {
            targetPosition.x = transform.position.x;
        }

        targetPosition.y = targetFollow.y + YValue;

        transform.position = targetPosition;
    }
}