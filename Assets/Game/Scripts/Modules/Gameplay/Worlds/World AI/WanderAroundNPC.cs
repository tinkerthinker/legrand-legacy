﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WanderAroundNPC : NPCAI {
    enum MovingEncounterState
    {
        Init,
        Moving,
    }

    
    public float DefaultMoveSpeed = 2f;
    public float MovingTime = 2f;
    public Vector2 RestTime;

    public AreaCollider NavArea;
    
    private MovingEncounterState _State;
    private IEnumerator _ActionTimer;
    private Vector3 _Wanderarget;
    private List<Transform> _BlockingCharacter;

    protected override void Awake()
    {
        base.Awake();
        _BlockingCharacter = new List<Transform>();
    }

    protected override void OnEnable()
    {
        _BlockingCharacter.Clear();
        _MoveSpeed = DefaultMoveSpeed;
        Init();
    }

    void Update()
    {
        if (_State == MovingEncounterState.Moving)
        {
            Vector3 target = new Vector3(_Wanderarget.x, transform.position.y, _Wanderarget.z);
            Ray myRay = new Ray(transform.position, transform.forward); // cast a Ray from the position of our gameObject into our desired direction.
            RaycastHit hit;

            if (Vector3.Distance(target, transform.position) > 2f)
            {
                Rotate(target);
                if (!Physics.Raycast(myRay, out hit, 1f, LayerMask.GetMask("Wall")))
                {
                    if (_MoveSpeed > 0 && _Animation.GetInteger("Action") != MoveAnimation)
                        _Animation.SetInteger("Action", MoveAnimation);
                    else if (_MoveSpeed == 0 && _Animation.GetInteger("Action") != IdleAnimation)
                        _Animation.SetInteger("Action", IdleAnimation);

                    transform.position += transform.forward * _MoveSpeed * Time.deltaTime;
                    
                }
            }
            else
            {
                if (_Animation.GetInteger("Action") != IdleAnimation)
                    _Animation.SetInteger("Action", IdleAnimation);
            }
        }
        if (_State == MovingEncounterState.Init && _Animation.GetInteger("Action") != IdleAnimation)
        {
            _Animation.SetInteger("Action", IdleAnimation);
        }
    }


    public override void Init()
    {
        if (gameObject.activeInHierarchy)
        {
            _State = MovingEncounterState.Init;
            Rest(LegrandUtility.Random(0f, 1f));
        }
    }

    public void Rest(float time)
    {
        _State = MovingEncounterState.Init;
        if (_ActionTimer != null)
        {
            StopCoroutine(_ActionTimer);
            _ActionTimer = null;
        }
        _ActionTimer = WaitToStart(time);
        StartCoroutine(_ActionTimer);
    }

    IEnumerator WaitToStart(float time)
    {
        yield return new WaitForSeconds(time);
        ChangeToMovingState();
    }

    public override void SeePlayer(Transform Player)
    {
        if (Player.tag == "NPC" || Player.tag == "PartyLeader")
        {
            _BlockingCharacter.Add(Player);
            _MoveSpeed = 0f;
        }
    }

    public override void LostPlayer(Transform Player)
    {
        if (Player.tag == "NPC" || Player.tag == "PartyLeader")
        {
            _BlockingCharacter.Remove(Player);
            if(_BlockingCharacter.Count == 0)
                _MoveSpeed = DefaultMoveSpeed;
        }
    }

    IEnumerator WaitToRest(float time)
    {
        yield return new WaitForSeconds(time);
        Rest(LegrandUtility.Random(RestTime.x, RestTime.y));
    }

    public void ChangeToMovingState()
    {
        Vector3 v = NavArea.coords[LegrandUtility.Random(0, NavArea.coords.Count)];
        _Wanderarget = LegrandUtility.Random(0f, 1f) * v + NavArea.transform.position;

        _State = MovingEncounterState.Moving;
        if (_ActionTimer != null)
        {
            StopCoroutine(_ActionTimer);
            _ActionTimer = null;
        }
        _ActionTimer = WaitToRest(MovingTime);
        StartCoroutine(_ActionTimer);
    }
}
