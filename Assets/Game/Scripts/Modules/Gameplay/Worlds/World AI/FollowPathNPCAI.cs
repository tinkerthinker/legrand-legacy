﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FollowPathNPCAI : NPCAI {
    enum MovingEncounterState
    {
        Init,
        Moving,
    }

    public int PointStartIndex;
    public float DefaultMoveSpeed = 2f;
    public Vector2 RestTime;
    public float NearTargetRange = 1f;

    public AreaCollider NavArea;

    private int _CurrentPointIndex;
    private MovingEncounterState _State;
    private IEnumerator _ActionTimer;
    private Vector3 _Wanderarget;
    private List<Transform> _BlockingCharacter;

    protected override void Awake()
    {
        base.Awake();
        _CurrentPointIndex = PointStartIndex;
        _BlockingCharacter = new List<Transform>();
    }

    protected override void OnEnable()
    {
        _BlockingCharacter.Clear();
        _MoveSpeed = DefaultMoveSpeed;
        Init();
    }

    void Update()
    {
        if (_State == MovingEncounterState.Moving)
        {
            Vector3 target = new Vector3(_Wanderarget.x, transform.position.y, _Wanderarget.z);
            Ray myRay = new Ray(transform.position, transform.forward); // cast a Ray from the position of our gameObject into our desired direction.
            RaycastHit hit;

            if (Vector3.Distance(target, transform.position) > NearTargetRange)
            {
                Rotate(target);
                if (!Physics.Raycast(myRay, out hit, 1f, LayerMask.GetMask("Wall")))
                {
                    if (_MoveSpeed > 0 && _Animation.GetInteger("Action") != MoveAnimation)
                        _Animation.SetInteger("Action", MoveAnimation);
                    else if (_MoveSpeed == 0 && _Animation.GetInteger("Action") != IdleAnimation)
                        _Animation.SetInteger("Action", IdleAnimation);

                    transform.position += transform.forward * _MoveSpeed * Time.deltaTime;
                    
                }
            }
            else
            {
                ChangeToNextPoint();
            }
        }
        if (_State == MovingEncounterState.Init && _Animation.GetInteger("Action") != IdleAnimation)
        {
            _Animation.SetInteger("Action", IdleAnimation);
        }
    }


    public override void Init()
    {
        if (gameObject.activeInHierarchy)
        {
            _State = MovingEncounterState.Init;
            Rest(LegrandUtility.Random(0f, 1f));
        }
    }

    public void Rest(float time)
    {
        _State = MovingEncounterState.Init;
        if (_ActionTimer != null)
        {
            StopCoroutine(_ActionTimer);
            _ActionTimer = null;
        }

        if (time > 0f)
        {
            _ActionTimer = WaitToStart(time);
            StartCoroutine(_ActionTimer);
        }
        else
        {
            ChangeToMovingState();
        }
    }

    IEnumerator WaitToStart(float time)
    {
        yield return new WaitForSeconds(time);
        ChangeToMovingState();
    }

    public override void SeePlayer(Transform Player)
    {
        if (Player.tag == "NPC" || Player.tag == "PartyLeader")
        {
            _BlockingCharacter.Add(Player);
            _MoveSpeed = 0f;
        }
    }

    public override void LostPlayer(Transform Player)
    {
        if (Player.tag == "NPC" || Player.tag == "PartyLeader")
        {
            _BlockingCharacter.Remove(Player);
            if(_BlockingCharacter.Count == 0)
                _MoveSpeed = DefaultMoveSpeed;
        }
    }

    IEnumerator WaitToRest(float time)
    {
        yield return new WaitForSeconds(time);
        Rest(LegrandUtility.Random(RestTime.x, RestTime.y));
    }

    public void ChangeToMovingState()
    {
        Vector3 v = NavArea.coords[_CurrentPointIndex];
        _Wanderarget = v + NavArea.transform.position;

        _State = MovingEncounterState.Moving;
    }

    public void ChangeToNextPoint()
    {
        _CurrentPointIndex++;
        if (_CurrentPointIndex == NavArea.coords.Count)
            _CurrentPointIndex = 0;

        Vector3 v = NavArea.coords[_CurrentPointIndex];
        _Wanderarget = v + NavArea.transform.position;
    }
}
