﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NPCAI : MonoBehaviour {

    public int IdleAnimation;
    public int MoveAnimation;

    protected float _turnSmoothing = 15f;
    protected EncounterEyeField Eye;

    protected float _MoveSpeed;
    protected Animator _Animation;

    protected virtual void Awake()
    {
        _Animation = LegrandUtility.GetComponentInChildren<Animator>(this.gameObject);
        Eye = LegrandUtility.GetComponentInChildren<EncounterEyeField>(this.gameObject);
        if (Eye != null)
        {
            Eye.TriggerEnter += SeePlayer;
            Eye.TriggerExit += LostPlayer;
        }
    }

    protected virtual void OnEnable()
    {
        Init();
    }


    public virtual void Init()
    {
    }

    public virtual void SeePlayer(Transform Player)
    {
    }

    public virtual void LostPlayer(Transform Player)
    {
    }

    protected void Rotate(Vector3 direction)
    {
        Quaternion rotation = Quaternion.LookRotation(direction - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * _turnSmoothing);
        transform.eulerAngles = new Vector3(0f, transform.eulerAngles.y, 0f);
    }
}
