﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class TransitionData{
	public GameplayType AfterFinishedGoTo;
	public string SequenceName;
	public int BattleId;
	public bool mustLose = false;
	public string TutorialToPlay = "";
	public int WarId;
	public string SceneName = "";
	public GameObject WorldCutscene; 
	public string WorldDialogName = "";
    public string WorldDestination = "";
}
