﻿using UnityEngine;

public class AnimRandomizer : StateMachineBehaviour
{
    public int Minimum;
    public int Maximum;
    public string AnimParam;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        int rand = Random.Range(Minimum, Maximum + 1);
        animator.SetInteger(AnimParam, rand);
    }
}