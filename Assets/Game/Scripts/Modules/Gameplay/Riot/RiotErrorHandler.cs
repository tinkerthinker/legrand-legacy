﻿using UnityEngine;
using System.Collections;

public class RiotErrorHandler : MonoBehaviour
{
    public GameObject HitParticle;

    public void OnStartAttackEvent()
    { }
    public void PlayBattleSFX(string sfxString)
    { }
    public void DoneAttackEvent()
    { }
    public void OnLastAttackEvent(string particleName)
    { }
    public void OnGuardEvent()
    {
        //if guard win/lose/draw do something
    }
    public void OnClashEvent()
    {
        //show particle clash
    }
    public void OnHeavyAttackEvent()
    {
        if (HitParticle) HitParticle.SetActive(true);
    }
    public void OnLightAttackEvent()
    {
        if (HitParticle) HitParticle.SetActive(true);
    }
}