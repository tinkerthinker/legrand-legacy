﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using TeamUtility.IO;

public class WishingWellAttribute : MonoBehaviour
{
	private int _Value;
	private PlayerController playerController;
	private WishingWellChance wishingWellChance;
	private bool pay = false;
	private string highlighted = "";

	public Button btn;
	public Image btnLeft;
	public Image btnRight;
	public Text money;
	public int minValue = 1;
	public int maxValue = 1000;
	public Sprite normal;
	public Sprite highlight;
	public List<WishingWellChance> Chance = new List<WishingWellChance> ();

	void OnEnable ()
	{
		pay = false;

		PopUpUI.OnEndPopUp += ChangePlayerState;

		maxValue = PartyManager.Instance.Inventory.Gold;
		playerController = GameObject.FindWithTag ("PartyLeader").GetComponent<PlayerController> ();

		playerController.ControllerState = PlayerController.PlayerControllerState.Sequence;

		_Value = minValue;

		UpdateUI ();

		btn.Select ();
        
	}

	void Update ()
	{
        
		if (isActiveAndEnabled) {
			UpdateUI ();
			if (InputManager.anyKey || InputManager.AnyInput ()) {
				TeamUtility.IO.StandaloneInputModule module = EventSystem.current.GetComponent<TeamUtility.IO.StandaloneInputModule> ();
				bool allow = module.allowMoveEvent;

				if (LegrandUtility.GetAxisRawHorizontal () > 0.1f && allow) {
					_Value += 10;
					highlighted = "right";
				} else if (LegrandUtility.GetAxisRawHorizontal () < -0.1f && allow) {
					_Value -= 10;
					highlighted = "left";
				} else if (LegrandUtility.GetAxisRawVertical () > 0.1f && allow) {
					_Value += 100;
                   
				} else if (LegrandUtility.GetAxisRawVertical () < -0.1f && allow) {
					_Value -= 100;
				} else {
					highlighted = "";
				}

				if (_Value > maxValue)
					_Value = maxValue;

				if (_Value < minValue)
					_Value = minValue;
			}
		}
	}

	void UpdateUI ()
	{
		if (_Value <= minValue)
			btnLeft.gameObject.SetActive (false);
		if (_Value >= maxValue)
			btnRight.gameObject.SetActive (false);
		if (_Value > minValue)
			btnLeft.gameObject.SetActive (true);
		if (_Value < maxValue)
			btnRight.gameObject.SetActive (true);

		money.GetComponent<Text> ().text = _Value.ToString () + " D";
       

		if (highlighted.Equals ("left")) {
			btnLeft.sprite = highlight;
			btnRight.sprite = normal;
		} else if (highlighted.Equals ("right")) {
			btnLeft.sprite = normal;
			btnRight.sprite = highlight;
		} else {
			btnLeft.sprite = normal;
			btnRight.sprite = normal;
		}
	}

	IEnumerator GetRandomItem ()
	{
		WorldSceneManager._Instance.ScreenFader.EndScene ();
		yield return new WaitForSeconds (1f);
		WwiseManager.Instance.PlaySFX (WwiseManager.WorldFX.Treasure);
		WorldSceneManager._Instance.ScreenFader.StartScene ();
		Close ();
		string itemID = GetItemID (_Value);
		PartyManager.Instance.Inventory.AddItems (LegrandBackend.Instance.ItemData [itemID], 1);
		PopUpUI.CallNotifPopUp ("You Received <color=#ff0000> 1 " + LegrandBackend.Instance.ItemData [itemID].Name + "</color>");
	}

	IEnumerator FailedGetItem ()
	{
		WorldSceneManager._Instance.ScreenFader.EndScene ();
		yield return new WaitForSeconds (1f);
		WorldSceneManager._Instance.ScreenFader.StartScene ();
		Close ();
		PopUpUI.CallNotifPopUp ("You don't have enough money.");
	}

	private string GetItemID (int value)
	{
		float startValue = 0;
		float lastValue = 0;

		float random = Random.Range (0, 100);

		int id = -1;

		for (int i = 0; i < Chance.Count; i++) {
			if (Chance [i].maxValue == -1) {
				//Debug.Log("dipanggil " + Chance[i].maxValue);
				if (_Value >= Chance [i].minValue && _Value <= PartyManager.Instance.Inventory.Gold) {
					wishingWellChance = Chance [i];
					break;
				}
			} else {
				if (_Value >= Chance [i].minValue && _Value <= Chance [i].maxValue) {
					wishingWellChance = Chance [i];
					break;
				}
			}
           
		}

		for (int i = 0; i < wishingWellChance.ItemChance.Count; i++) {
			lastValue = startValue + wishingWellChance.ItemChance [i].y;
			if (random >= startValue && random <= lastValue) {
				id = i;
				break;
			}
			startValue = lastValue;
		}

		PartyManager.Instance.Inventory.Gold -= _Value;

		if (id != -1)
			return wishingWellChance.ItemChance [id].x;
		else
			return "";
	}

	public void DoPay ()
	{
		if (PartyManager.Instance.Inventory.Gold < _Value) {
			StartCoroutine (FailedGetItem ());
		} else {
			StartCoroutine (GetRandomItem ());           
		}
		pay = true;
	}

	public void Close ()
	{
		transform.gameObject.SetActive (false);
		if (!pay)
			transform.parent.gameObject.GetComponent<WishingWellTrigger> ().StartCoroutine (transform.parent.gameObject.GetComponent<WishingWellTrigger> ().ChangeState ());
	}

	public void ChangePlayerState (bool value = true)
	{
		PopUpUI.OnEndPopUp -= ChangePlayerState;
		transform.parent.gameObject.GetComponent<WishingWellTrigger> ().StartCoroutine (transform.parent.gameObject.GetComponent<WishingWellTrigger> ().ChangeState ());
	}

}
