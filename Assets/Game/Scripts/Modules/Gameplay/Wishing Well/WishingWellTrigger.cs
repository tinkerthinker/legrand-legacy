﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class WishingWellTrigger : WorldTrigger {
    public GameObject helper;
    public GameObject canvas;
    //public GameObject btnprefab;
    //public List<int> listChoice = new List<int>();
    //public List<GameObject> listBtnWishingWell = new List<GameObject>();

    private GameObject player;
    //void OnEnable()
    //{
    //    for (int i = 0; i < listChoice.Count; i++)
    //    {
    //        GameObject temp = Instantiate(btnprefab);
    //        temp.GetComponentInChildren<Text>().text = listChoice[i] + " D";
    //        temp.GetComponent<WishingWellAttribute>().SetValue(listChoice[i]);
    //        temp.GetComponent<WishingWellAttribute>().SetCanvas(canvas);
    //        temp.transform.SetParent(canvas.transform.GetChild(0));
    //        temp.transform.localScale = new Vector3(1f, 1f, 1f);
    //        listBtnWishingWell.Add(temp);
    //    }

    //    if (listBtnWishingWell.Count > 0)
    //        EventSystem.current.firstSelectedGameObject = listBtnWishingWell[0];
    //}

    void OnTriggerEnter(Collider coll)
    {
        if (coll.tag == "PartyLeader")
        {
            if (NeedActivate)
            {
                player = coll.gameObject;
                helper = coll.GetComponentInChildren<DialogueHelperReference>().helper;
                if (helper != null)
                    PopUpUI.CallDialogueMarkPopUp(helper, true);
                else
                    PopUpUI.CallDialogueMarkPopUp(gameObject, true);
                EventManager.Instance.AddListener<EnterTrigger>(ActivateUI);
            }
        }
    }

    void OnTriggerExit(Collider coll)
    {
        if (coll.tag == "PartyLeader")
        {
            player = null;
            EventManager.Instance.RemoveListener<EnterTrigger>(ActivateUI);
            if (helper != null)
                PopUpUI.CallDialogueMarkPopUp(null, false);
            else
                PopUpUI.CallDialogueMarkPopUp(null, false);
            if (canvas.activeSelf)
                canvas.SetActive(false);
        }
    }

    void OnDisable()
    {
        if (EventManager.Instance != null)
            EventManager.Instance.RemoveListener<EnterTrigger>(ActivateUI);
        else
            Debug.LogWarning("Event Manager Tidak Ada");
    }

    void OnDestroy()
    {
        if (EventManager.Instance != null)
            EventManager.Instance.RemoveListener<EnterTrigger>(ActivateUI);
        else
            Debug.LogWarning("Event Manager Tidak Ada");
    }


    void ActivateUI(EnterTrigger e)
    {
        if (helper != null)
            PopUpUI.CallDialogueMarkPopUp(null, false);
        else
            PopUpUI.CallDialogueMarkPopUp(null, false);
        if (!canvas.activeSelf)
            canvas.SetActive(true);
    }

    public IEnumerator ChangeState()
    {
        yield return null;
        player.GetComponent<PlayerController>().ControllerState = PlayerController.PlayerControllerState.Normal;
    }
}
