﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[System.Serializable]
public class WishingWellChance {

    public int minValue;
    public int maxValue;
    //public float[] Chance = new float[Enum.GetNames(typeof(Item.Rarity)).Length];
    public List<VectorStringInteger> ItemChance = new List<VectorStringInteger>();

    public WishingWellChance()
    {
        minValue = 0;
        maxValue = 0;
    }
}
