﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class FencingAttribute
{
    [HideInInspector]
    public enum Type
    {
        Normal,
        Counter
    }

    [System.Serializable]
    public class FencingSkillAttribute
    {
        public int skillID;
        public string skillName;
        public string skillAltName;
        public int skillChance;
        public int skillDamage;
        public int skillDamageModifier;
        public int skillGainAP;
        public Type skillType;
        public List<int> skillStrengthID;
        public List<int> skillWeaknessID;
        public List<int> skillDrawID;

        public FencingSkillAttribute()
        {
            skillID = 0;
            skillName = "";
            skillAltName = "";
            skillChance = 0;
            skillDamage = 0;
            skillDamageModifier = 0;
            skillGainAP = 0;
            skillType = Type.Normal;
            skillStrengthID = new List<int>();
            skillWeaknessID = new List<int>();
            skillDrawID = new List<int>();

        }

        [System.Serializable]
        public class FencingSkillModifier
        {
            public int skillID;
        }
    }
}

