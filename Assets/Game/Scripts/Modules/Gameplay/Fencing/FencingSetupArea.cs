﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FencingSetupArea : MonoBehaviour {
    public GameObject MinigameEnvironment;
    public GameObject MinigameBattle;
    public GameObject PlayerLeader;
    public List<GameObject> ListDisabledNPC;

    void Start()
    {
        PlayerLeader = GameObject.Find("PartyLeader");
        //MinigameBattle.SetActive(false);
    }

    public void SetEnableNPC(bool enable) {
        for (int i = 0; i < ListDisabledNPC.Count; i++)
        {
            GameObject g = ListDisabledNPC[i];
            g.SetActive(enable);
        }
    }
}
