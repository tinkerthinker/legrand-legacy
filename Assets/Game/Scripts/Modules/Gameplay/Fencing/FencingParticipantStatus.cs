﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using Legrand.database;

public class FencingParticipantStatus : MonoBehaviour
{
    [HideInInspector]
    public string ParticipantId;
    public FencingController Controller;
    public bool SuperSkill = false;
    [HideInInspector]
    public int Hp;
    [HideInInspector]
    public int Ap;

    public GameObject HpBar;
    public GameObject ApBar;
    //public GameObject CommandPanel;
    //public GameObject ButtonSkillPrefab;
    public List<KeyCode> ListButtonSkill = new List<KeyCode>();
    public List<GameObject> ListHighlightButton;

    private List<FencingAttribute.FencingSkillAttribute> skillAttribute;
    private List<GameObject> ButtonSkill = new List<GameObject>();
    private int Choice;
    private FencingParticipantStatusData data;

    public List<FencingAttribute.FencingSkillAttribute> SkillAttribute
    {
        get
        {
            return skillAttribute;
        }
    }

    public void InitData()
    {
        foreach (GameObject b in ButtonSkill)
        {
            Destroy(b);
        }

        ButtonSkill.Clear();

        for (int i = 0; i < Controller.Database.Count; i++)
        {
            if (LegrandUtility.CompareString(Controller.Database.Get(i).ID, ParticipantId))
            {
                data = Controller.Database.Get(i);
            }
        }

        Hp = data.Hp;
        Ap = data.Ap;

        BarScript hpBarScript = HpBar.GetComponent<BarScript>();
        hpBarScript.maxValue = Hp;
        hpBarScript.currValue = Hp;
        skillAttribute = data.SkillAttribute;

        //foreach (var sa in SkillAttribute)
        //{
        //    GameObject temp = Instantiate(ButtonSkillPrefab);
        //    temp.transform.SetParent(CommandPanel.transform);
        //    temp.GetComponentInChildren<Text>().text = sa.skillName;
        //    temp.transform.localScale = new Vector3(1f, 1f, 1f);
        //    temp.name = "Button " + sa.skillName;
        //    ButtonSkill.Add(temp);
        //}
    }

    void Update()
    {
        if (Ap >= 100)
            SuperSkill = true;
        else
            SuperSkill = false;

        //if (SuperSkill)
        //{
        //    for (int i = 0; i < ButtonSkill.Count; i++)
        //    {
        //        if (ListButtonSkill.Count > 0)
        //            ButtonSkill[i].GetComponentInChildren<Text>().text = ListButtonSkill[i].ToString() + " - " + SkillAttribute[i].skillAltName;
        //    }
        //}
        //else
        //{
        //    for (int i = 0; i < ButtonSkill.Count; i++)
        //    {
        //        if (ListButtonSkill.Count > 0)
        //            ButtonSkill[i].GetComponentInChildren<Text>().text = ListButtonSkill[i].ToString() + " - " + SkillAttribute[i].skillName;
        //    }
        //}

        HpBar.GetComponent<BarScript>().setCurrValue(Hp);
        ApBar.GetComponent<BarScript>().setCurrValue(Ap);
    }

    public void GainAp(int value)
    {
        Ap += value;
    }

    public int GetGold()
    {
        return data.GoldDrop;
    }

    public void ReceiveDamage(int value)
    {
        Hp -= value;
        if (value > 0)
            Ap += 5;
    }

    //public void setChoice(int value)
    //{
    //    Choice = value;
    //}

    public string getSkillName(int index)
    {
        if (SuperSkill)
            return SkillAttribute[index].skillAltName;
        else
            return SkillAttribute[index].skillName;
    }

    public int getAttackChoice()
    {
        int choice = -1;

        int maxValue = 0;
        int indexOfMaxValue = 0;

        float value = Random.Range(0, SkillAttribute.Count - 1);
        float startValue = 0;
        float lastValue = 0;

        int total = 0;

        for (int i = 0; i < SkillAttribute.Count; i++)
        {
            if (SkillAttribute[i].skillChance > maxValue)
            {
                maxValue = SkillAttribute[i].skillChance;
                indexOfMaxValue = i;
            }
            total += SkillAttribute[i].skillChance;
        }

        for (int i = 0; i < SkillAttribute.Count; i++)
        {
            lastValue = startValue + (float)SkillAttribute[i].skillChance / total;
            if (value >= startValue && value <= lastValue)
            {
                choice = i;
                break;
            }
            startValue = lastValue;
        }

        if (choice == -1)
            choice = indexOfMaxValue;

        return choice;
    }
}
