﻿using UnityEngine;
using System.Collections;
using TeamUtility.IO;

public class FencingController : MonoBehaviour
{
    public GameObject TimeBar;
    public GameOverBar GameOverBar;
    public GameObject Battler;
    public FencingParticipantStatusDatabase Database;
    public float PreparationTime;
    public GameObject Player;
    public GameObject Enemy;
    public ScreenFader FencingScreenFader;

    private int enemyAttack = 0;
    private int playerAttack = 0;

    private bool battleCalculation = false;
    private bool enemyAttackChoiced = false;

    private FencingParticipantStatus playerStatus;
    private FencingParticipantStatus enemyStatus;

    private Animator playerAnim;
    private Animator enemyAnim;

    private int turn = 0;
    private float tempTime;
    private float maxWidthTimeBar;

    private GameObject currentNPCBattler;
    public GameObject CurrentNPCBattler
    {
        set
        {
            currentNPCBattler = value;
        }
    }

    private enum BattleState
    {
        Preparation,
        Battle,
        Finish
    }

    BattleState currState;

    void OnEnable()
    {
        playerStatus = Player.GetComponent<FencingParticipantStatus>();
        enemyStatus = Enemy.GetComponent<FencingParticipantStatus>();
        playerStatus.ParticipantId = "Player";
        playerStatus.InitData();
        playerAnim = Player.GetComponent<Animator>();
        enemyAnim = Enemy.GetComponent<Animator>();

        currState = BattleState.Preparation;
        tempTime = PreparationTime;

        RectTransform rect = TimeBar.GetComponent<RectTransform>();
        maxWidthTimeBar = RectTransformExtensions.GetWidth(rect);

        CameraPanning cp = AreaController.Instance.CurrRoom.cameras[0].GetComponent<CameraPanning>();
        cp.ZoomIn(AreaController.Instance.CurrRoom.ConvertToVerticalFOV(AreaController.Instance.CurrRoom.subArea.mainHorizontalFOV));
    }

    void Update()
    {
        if (currState == BattleState.Finish)
            return;
        if (playerStatus.Hp <= 0 || enemyStatus.Hp <= 0)
            currState = BattleState.Finish;

        if (currState == BattleState.Preparation)
        {
            PreparationTime -= Time.deltaTime;
            RectTransform rect = TimeBar.GetComponent<RectTransform>();
            float decrease = maxWidthTimeBar * PreparationTime / tempTime;
            RectTransformExtensions.SetWidth(rect, decrease);

            StartCoroutine("WaitForInputTimeout", PreparationTime);

            battleCalculation = false;

            if (!enemyAttackChoiced)
            {
                enemyAttack = enemyStatus.getAttackChoice();
                enemyAttackChoiced = true;
            }
            for (int i = 0; i < playerStatus.ListButtonSkill.Count; i++)
            {
                playerStatus.ListHighlightButton[i].SetActive(false);
                if (Input.GetKeyDown(playerStatus.ListButtonSkill[i]))
                {
                    currState = BattleState.Battle;
                    playerStatus.ListHighlightButton[i].SetActive(true);
                    playerAttack = i;
                }
            }
            for (int idBtn = 0; idBtn < enemyStatus.ListHighlightButton.Count; idBtn++)
            {
                enemyStatus.ListHighlightButton[idBtn].SetActive(false);
            }
        }
        else if (currState == BattleState.Battle)
        {
            PreparationTime = tempTime;

            enemyAttackChoiced = false;

            if (!battleCalculation)
            {
                StopAllCoroutines();
                battleCalculation = true;
                turn++;
                string winner = "draw";

                FencingAttribute.FencingSkillAttribute enemySkillAttribute = enemyStatus.SkillAttribute[enemyAttack];

                enemyStatus.ListHighlightButton[enemyAttack].SetActive(true);

                //Debug.Log("Player Choose skill " + playerAttack);
                //Debug.Log("Enemy Choose skill " + enemyAttack);
                if (playerAttack != -1)
                {
                    FencingAttribute.FencingSkillAttribute playerSkillAttribute = playerStatus.SkillAttribute[playerAttack];

                    for (int i = 0; i < playerSkillAttribute.skillStrengthID.Count; i++)
                    {
                        if (playerSkillAttribute.skillStrengthID[i] == enemyAttack)
                            winner = "player";
                    }

                    for (int i = 0; i < playerSkillAttribute.skillWeaknessID.Count; i++)
                    {
                        if (playerSkillAttribute.skillWeaknessID[i] == enemyAttack)
                            winner = "enemy";
                    }

                    //if (playerSkillAttribute.skillType == MinigameAttribute.Type.Counter && enemySkillAttribute.skillType == MinigameAttribute.Type.Counter)
                    //    winner = "none";

                    if (winner == "player")
                    {
                        playerAnim.SetInteger("State", playerAttack + 1);
                        enemyAnim.SetInteger("State", 5);
                        if (playerStatus.SuperSkill)
                        {
                            if (playerSkillAttribute.skillType == FencingAttribute.Type.Normal)
                                enemyStatus.ReceiveDamage(playerSkillAttribute.skillDamage * playerSkillAttribute.skillDamageModifier / 100);
                            else
                                enemyStatus.ReceiveDamage(enemySkillAttribute.skillDamage * enemySkillAttribute.skillDamageModifier / 100);
                        }
                        else
                        {
                            if (playerSkillAttribute.skillType == FencingAttribute.Type.Normal)
                                enemyStatus.ReceiveDamage(playerSkillAttribute.skillDamage);
                            else
                                enemyStatus.ReceiveDamage(enemySkillAttribute.skillDamage);
                        }
                        playerStatus.GainAp(playerSkillAttribute.skillGainAP);
                    }

                    if (winner == "enemy")
                    {
                        playerAnim.SetInteger("State", 5);
                        enemyAnim.SetInteger("State", enemyAttack + 1);
                        if (enemyStatus.SuperSkill)
                        {
                            if (enemySkillAttribute.skillType == FencingAttribute.Type.Normal)
                                playerStatus.ReceiveDamage(enemySkillAttribute.skillDamage * enemySkillAttribute.skillDamageModifier / 100);
                            else
                                playerStatus.ReceiveDamage(playerSkillAttribute.skillDamage * playerSkillAttribute.skillDamageModifier / 100);
                        }
                        else
                        {
                            if (enemySkillAttribute.skillType == FencingAttribute.Type.Normal)
                                playerStatus.ReceiveDamage(enemySkillAttribute.skillDamage);
                            else
                                playerStatus.ReceiveDamage(playerSkillAttribute.skillDamage);
                        }
                        enemyStatus.GainAp(enemySkillAttribute.skillGainAP);
                    }

                    if (winner == "draw")
                    {
                        playerAnim.SetInteger("State", 4);
                        enemyAnim.SetInteger("State", 4);
                        //draw state ~ no one got damage.
                        //    if (playerStatus.SuperSkill)
                        //    {
                        //        enemyStatus.ReceiveDamage(playerSkillAttribute.skillDamage * playerSkillAttribute.skillDamageModifier / 200);
                        //    }
                        //    else
                        //        enemyStatus.ReceiveDamage(playerSkillAttribute.skillDamage / 2);

                        //    if (enemyStatus.SuperSkill)
                        //    {
                        //        playerStatus.ReceiveDamage(enemySkillAttribute.skillDamage * enemySkillAttribute.skillDamageModifier / 200);
                        //    }
                        //    else
                        //        playerStatus.ReceiveDamage(enemySkillAttribute.skillDamage / 2);
                    }
                }
                else
                {
                    if (enemyStatus.SuperSkill)
                    {
                        if (enemySkillAttribute.skillType == FencingAttribute.Type.Normal)
                            playerStatus.ReceiveDamage(enemySkillAttribute.skillDamage * enemySkillAttribute.skillDamageModifier / 100);
                    }
                    else
                    {
                        if (enemySkillAttribute.skillType == FencingAttribute.Type.Normal)
                            playerStatus.ReceiveDamage(enemySkillAttribute.skillDamage);
                    }
                    enemyStatus.GainAp(enemySkillAttribute.skillGainAP);
                }

                if (playerStatus.SuperSkill) playerStatus.Ap = 0;
                if (enemyStatus.SuperSkill) enemyStatus.Ap = 0;

                StartCoroutine("WaitForAnimation");
            }
        }
        else
        {
            StopCoroutine("WaitForAnimation");
            StopCoroutine("WaitForInputTimeout");
            if (playerStatus.Hp <= 0)
            {
                //Debug.Log("Lose");
                FencingScreenFader.OnFadeOut += TransToTitle;
                FencingScreenFader.EndScene();
                GameOverBar.FadeInText();
                enemyAnim.Play("EnemyWinBattle");
                playerAnim.Play("PlayerDead");
                StartCoroutine(gameOverScreenHandler());
            }
            else
            {
                //Debug.Log("Win");]
                playerAnim.Play("PlayerWinBattle");
                enemyAnim.Play("EnemyDead");
                PopUpUI.CallNotifPopUp("You Win and Got " + enemyStatus.GetGold() + " Danaar ", NotifEvents);
                currentNPCBattler.GetComponent<FencingEnemyController>().enabled = true;
                currentNPCBattler.GetComponent<SphereCollider>().enabled = false;
                FencingTrigger batTrainer = currentNPCBattler.GetComponent<FencingTrigger>();
                batTrainer.AIRig.SetActive(false);
                batTrainer.ExecuteDeadAnimation();
            }
        }
    }

    private IEnumerator gameOverScreenHandler()
    {
        bool changeState = false;

        while (!changeState)
        {
            if (InputManager.GetButtonUp("Confirm"))
            {
                TransToTitle();
                changeState = true;
            }
            yield return null;
        }
    }

    private void TransToTitle()
    {
        FencingScreenFader.OnFadeOut -= TransToTitle;
        SceneManager.SwitchScene("TitleScene");
    }

    private void NotifEvents()
    {
        playerAnim.SetInteger("State", 0);
        enemyAnim.SetInteger("State", 0);
        turn = 0;
        FencingSetupArea setupArea = transform.parent.GetComponent<FencingSetupArea>();
        setupArea.MinigameEnvironment.SetActive(true);
        setupArea.MinigameBattle.SetActive(false);
        setupArea.SetEnableNPC(true);
        setupArea.PlayerLeader.SetActive(true);
        AreaController.Instance.CurrPlayer.GetComponent<PlayerController>().ControllerState = PlayerController.PlayerControllerState.Normal;
        PartyManager.Instance.Inventory.Gold += enemyStatus.GetGold();
    }

    IEnumerator WaitForAnimation()
    {
        yield return new WaitForSeconds(1f);

        currState = BattleState.Preparation;
        playerAnim.SetInteger("State", 0);
        enemyAnim.SetInteger("State", 0);
    }

    IEnumerator WaitForInputTimeout(float value)
    {
        yield return new WaitForSeconds(value);

        playerAttack = -1;
        enemyAttack = LegrandUtility.Random(0, 2);

        playerAnim.SetInteger("State", 0);
        enemyAnim.SetInteger("State", enemyAttack + 1);
        currState = BattleState.Battle;
    }
}