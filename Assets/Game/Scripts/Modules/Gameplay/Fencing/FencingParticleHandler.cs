﻿using UnityEngine;

public class FencingParticleHandler : MonoBehaviour {

    private ParticleSystem ps;
    // Use this for initialization
    void Start()
    {
        ps = GetComponent<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        if (ps)
        {
            if (!ps.IsAlive())
            {
                DisableThis();
            }
        }
    }

    public void DisableThis()
    {
        this.gameObject.SetActive(false);
    }
}
