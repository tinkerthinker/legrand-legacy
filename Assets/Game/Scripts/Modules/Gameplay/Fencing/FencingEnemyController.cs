﻿using UnityEngine;
using System.Collections;

public class FencingEnemyController : MonoBehaviour {
    private void OnEnable() {
        gameObject.GetComponent<FencingTrigger>().ActivateTrigger = false;
        gameObject.GetComponent<Animator>().SetInteger("State", 2);
    }
}
