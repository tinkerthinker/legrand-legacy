﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class FencingTrigger : WorldTrigger
{
    public GameObject Portal;
    public GameObject AIRig;
    [HideInInspector]
    public bool ActivateTrigger = true;
    private GameObject canvas;
    private List<GameObject> btnBattleTrainer = new List<GameObject>();
    public string npcID;

    void OnTriggerEnter(Collider coll)
    {
        if (!ActivateTrigger)
            return;
        if (coll.tag == "PartyLeader")
        {
            _ObjectOnTrigger = coll.gameObject;

            if (NeedActivate)
            {
                EventManager.Instance.AddListener<EnterTrigger>(EnterBattle);
                EventManager.Instance.AddListener<ExitTrigger>(DeactivateBattleTrainerUI);
            }
            else
            {
                WorldSceneManager._Instance.ScreenFader.EndScene();
                StartCoroutine(WaitForFadeOut());
            }
        }
    }

    void OnTriggerExit(Collider coll)
    {
        if (!ActivateTrigger)
            return;
        if (coll.tag == "PartyLeader")
        {
            EventManager.Instance.RemoveListener<EnterTrigger>(EnterBattle);
            EventManager.Instance.RemoveListener<ExitTrigger>(DeactivateBattleTrainerUI);
        }
    }

    void EnterBattle(EnterTrigger e)
    {
        Transform masterParent = transform.parent.parent.parent;
        masterParent.GetComponent<FencingSetupArea>().PlayerLeader.SetActive(false);
        masterParent.GetComponent<FencingSetupArea>().MinigameBattle.SetActive(true);
        masterParent.GetComponent<FencingSetupArea>().MinigameEnvironment.SetActive(false);
        EventManager.Instance.TriggerEvent(new MinigameNpcEvent(npcID));
    }

    void EnterBattle()
    {
        Transform masterParent = transform.parent.parent.parent;
        GameObject playerleader = masterParent.GetComponent<FencingSetupArea>().PlayerLeader;
        playerleader.transform.position = Portal.transform.position;
        FencingController controller = masterParent.GetComponent<FencingSetupArea>().MinigameBattle.GetComponent<FencingController>();
        controller.Battler.transform.position = Portal.transform.position; controller.CurrentNPCBattler = gameObject;
        masterParent.GetComponent<FencingSetupArea>().PlayerLeader.SetActive(false);
        masterParent.GetComponent<FencingSetupArea>().MinigameBattle.SetActive(true);
        AreaController.Instance.CurrPlayer.GetComponent<PlayerController>().ControllerState = PlayerController.PlayerControllerState.Sequence;
        masterParent.GetComponent<FencingSetupArea>().SetEnableNPC(false);
        EventManager.Instance.TriggerEvent(new MinigameNpcEvent(npcID));
    }

    void DeactivateBattleTrainerUI(ExitTrigger e)
    {
        //if (canvas.activeSelf)
        //    canvas.SetActive(false);
    }

    private IEnumerator WaitForFadeOut()
    {
        yield return new WaitForSeconds(1f);
        WorldSceneManager._Instance.ScreenFader.StartScene();
        EnterBattle();
    }

    public void ExecuteDeadAnimation()
    {
        ActivateTrigger = false;
        gameObject.GetComponent<Animator>().SetInteger("State", 1);
    }
}