using UnityEngine;

public class SplineWalker : MonoBehaviour
{
    public GameObject Target;

    public BezierSpline spline;

    public bool UseDuration;
    public float duration;

    public bool lookForward;
    public float speed = 1;

    public SplineWalkerMode mode;

    private float progress;
    private bool goingForward = true;

    private void Start()
    {
    }

    private void Update()
    {
        if (goingForward)
        {
            if (UseDuration)
                progress += speed * Time.deltaTime / duration;
            else
                progress += speed * Time.deltaTime;
            if (progress > 1f)
            {
                if (mode == SplineWalkerMode.Once)
                {
                    progress = 1f;
                }
                else if (mode == SplineWalkerMode.Loop)
                {
                    progress -= 1f;
                }
                else
                {
                    progress = 2f - progress;
                    goingForward = false;
                }
            }
        }
        else
        {
            if (UseDuration)
                progress -= speed * Time.deltaTime / duration;
            else
                progress -= speed * Time.deltaTime;
            if (progress < 0f)
            {
                progress = -progress;
                goingForward = true;
            }
        }

        if (spline != null)
        {
            Vector3 position = spline.GetPoint(progress);
            transform.localPosition = position;
            if (lookForward)
            {
                if (goingForward)
                    transform.LookAt(position + spline.GetDirection(progress));
                else
                    transform.LookAt(position - spline.GetDirection(progress));
            }
        }
        if (Target != null)
            gameObject.transform.LookAt(Target.transform);
    }

    void OnDisable()
    {
        goingForward = true;
    }

}