﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;
using Legrand.core;
using TeamUtility.IO;

public enum TimedHitState{
	None,
	Miss,
	Good,
	Perfect,
}
public enum TimedType{
	Hit,
	Def,
}
public class TimedHitController : MonoBehaviour {
	public int hitButtonId;
    public Sprite[] TimeHitSprites;
	public CameraFacing cameraFacingModule;

	public RectTransform HitIndicator;
	public Image FirstGoodIndicator;
	public Image SecondGoodIndicator;
	public float GoodIndicatorWidth = 0.075f;
	public Image PerfectIndicator;
	public float PerfectIndicatorWidth = 0.075f;
    public Image BackgroundImage;
    public Image buttonImage;

	public BattleInput battleInput;
	private BattleCommand command;

	private Battler Attacker;
	private Battler[] Defenders;

	private TimedType timedType;
    private TimedHitState _AttackHitState;
    public TimedHitState AttackHitState
    {
        get { return _AttackHitState; }
        set { _AttackHitState = value; }
    }
    private TimedHitState _DefendHitState;
    public TimedHitState DefendHitState
    {
        get { return _DefendHitState; }
        set { _DefendHitState = value; }
    }
	private bool onTimed = false;
	public bool OnTimed{
		get {return onTimed;}
		set {onTimed = value;}
	}
	private Animator ani;

	public delegate void onTimedDef(Battler battler, TimedHitState hitState, int id);
	public event onTimedDef OnTimedDef;

	public delegate void onTimedHit(BattleCommand command, TimedHitState hitState, int id);
	public event onTimedHit OnTimedHit;

	public Vector2 PerfectMinMaxRotation = new Vector2(126f, 288f);

	private Vector2 _PerfectArea;
	private Vector2 _FirstGoodArea;
	private Vector2 _SecondGoodArea;

	private Tweener _PlayingTween;

	private int Phase = 0;
	private bool KeyHasPressed = false;
	private const float _TimedHitLength = 1f;

    public TimeHitPopUp PopUp;
    public Text ConfirmObj;
	void Awake(){
		ani = GetComponent<Animator> ();
		_AttackHitState = TimedHitState.None;
        _DefendHitState = TimedHitState.None;

        PerfectIndicator.fillAmount = PerfectIndicatorWidth;
		FirstGoodIndicator.fillAmount = GoodIndicatorWidth;
		SecondGoodIndicator.fillAmount = GoodIndicatorWidth;
	}

    // O Top, 1 Right, 2 Bottom, 3 Left
    public void setHit(Battler attacker, Battler[] defenders ,BattleCommand command , int btnId){
		this.command 	= command;
		this.Attacker   = attacker;
		this.Defenders 	= defenders;
        timedType = TimedType.Hit;

        if (!GameSetting.OneButtonAct)
        {
            hitButtonId = btnId;
            setButtonImage(GetButtonImage((CommandPos)hitButtonId));
        }
        else
        { 
            hitButtonId = 4;
            setButtonImage(LegrandBackend.Instance.GetSpriteData("TimedHitPlain"));
            ConfirmObj.DOColor(new Color(ConfirmObj.color.r, ConfirmObj.color.g, ConfirmObj.color.b, 1), 0f);
        }
	}

	public void setDef(Battler attacker, Battler defender , int btnId){
		this.Attacker   = attacker;
		this.Defenders = new Battler[1];
		this.Defenders[0] = defender;
        timedType = TimedType.Def;

        if (!GameSetting.OneButtonAct)
        {
            hitButtonId     = btnId;
            setButtonImage(GetButtonImage((CommandPos)hitButtonId));
        }
        else
        { 
            hitButtonId = 4;
            setButtonImage(LegrandBackend.Instance.GetSpriteData("TimedHitPlain"));
            ConfirmObj.DOColor(new Color(ConfirmObj.color.r, ConfirmObj.color.g, ConfirmObj.color.b, 1), 0f);
        }
	}

	public void startTimedHit()
    {
        float PerfectLocation = 200;
        battleInput.isTimeHit = true;

        onTimed 		= true;

        // First randomize perfect area
        if (GameSetting.RandomActPosition)
        {
            float location = 180f;
            if (timedType == TimedType.Hit)
                location = LegrandUtility.Random(PerfectMinMaxRotation.x, PerfectMinMaxRotation.y);
            if (timedType == TimedType.Def)
                location = 360f- LegrandUtility.Random(PerfectMinMaxRotation.x, PerfectMinMaxRotation.y);
            PerfectLocation = location;
        }
        else
            PerfectLocation = 180f;  

        if (timedType == TimedType.Hit)
            PerfectIndicatorWidth = LegrandUtility.Random(0.015f, 0.01525f + (Attacker.GetAttribute(Attribute.LUCK) * 0.00025f));
        if (timedType == TimedType.Def)
                PerfectIndicatorWidth = LegrandUtility.Random(0.015f, 0.01525f + (Defenders[0].GetAttribute(Attribute.LUCK) * 0.00025f));

        PerfectIndicator.fillAmount = PerfectIndicatorWidth;
        // Crete fill point helper
        float PerfectIndicatorUIPoint = PerfectLocation / 360f;
		_PerfectArea = new Vector2 (PerfectIndicatorUIPoint * 360f, (PerfectIndicatorUIPoint + PerfectIndicatorWidth) * 360f);
		_FirstGoodArea = new Vector2 ((PerfectIndicatorUIPoint - GoodIndicatorWidth) * 360f, PerfectIndicatorUIPoint * 360f);
		_SecondGoodArea = new Vector2 ((PerfectIndicatorUIPoint + PerfectIndicatorWidth) * 360f, (PerfectIndicatorUIPoint + (GoodIndicatorWidth + PerfectIndicatorWidth)) * 360f);

		// Transform point into UI
		PerfectIndicator.rectTransform.eulerAngles = new Vector3 (0f,0f, 360f-PerfectLocation);
		FirstGoodIndicator.rectTransform.eulerAngles = new Vector3 (0f,0f,360f-_FirstGoodArea.x);
		SecondGoodIndicator.rectTransform.eulerAngles = new Vector3 (0f,0f,360f-_SecondGoodArea.x);

		// Play the animation
		ani.ResetTrigger ("HitPerfect");
		ani.ResetTrigger ("HitTrue");
		ani.ResetTrigger ("HitFalse");

		HitIndicator.eulerAngles = new Vector3 (0f,0f,0);

        if (timedType == TimedType.Hit)
            _AttackHitState = TimedHitState.Good;
        else
            _DefendHitState = TimedHitState.Good;
		KeyHasPressed = false;
		battleInput.onReqInput += HandleonReqInput;

		EventManager.Instance.TriggerEvent (new SetTimeHitStatusEvent(Attacker, Defenders, true));
        if (timedType == TimedType.Hit)
        {
            BackgroundImage.sprite = TimeHitSprites[0];
            ani.SetTrigger("Hit");
        }
        else if (timedType == TimedType.Def)
        {
            BackgroundImage.sprite = TimeHitSprites[1];
            ani.SetTrigger("Def");
        }
		StartCoroutine (AnimateTimeHit());
	}


	IEnumerator AnimateTimeHit()
	{
		// Phase 1 is time hit play animation
		yield return StartCoroutine(LegrandUtility.WaitForRealTime (0.5f));

		Phase = 1;
		float timePassed = 0;

		float rotateTo = 0f;
		if (timedType == TimedType.Hit)
			rotateTo = -360f;
		else if (timedType == TimedType.Def)
			rotateTo = 360f;
		
		while (Phase > 0 && !KeyHasPressed) 
		{
			yield return null;
			if(GlobalGameStatus.Instance.StateStatus[(int)GlobalGameStatus.GameState.Pause] == false) timePassed += Time.unscaledDeltaTime;
			if (Phase == 1)
				DoPhaseOne (timePassed, rotateTo);
		}

        battleInput.isTimeHit = false;
	}

	private void HandleonReqInput (int key)
    {
        if (GameSetting.OneButtonAct && key != 4)
            return;
        if (!GameSetting.OneButtonAct && key > 3)
            return;

		if (onTimed) {
			KeyHasPressed = true;
			if (hitButtonId == key) {

				float hitPoint = 360f-HitIndicator.eulerAngles.z;

                if ((hitPoint >= _FirstGoodArea.x && hitPoint < _FirstGoodArea.y) || (hitPoint >= _SecondGoodArea.x && hitPoint < _SecondGoodArea.y))
                {
                    PopUp.TimeHitPop.Play("Good");
                    if (timedType == TimedType.Hit)
                    {
                        _AttackHitState = TimedHitState.Good;
                        Attacker.GoodTimeHit += 1;
                    }
                    else
                    {
                        _DefendHitState = TimedHitState.Good;
                        for (int i = 0; i < Defenders.Length; i++)
                            Defenders[i].GoodTimeHit += 1;
                    }

                }
                else if (hitPoint >= _PerfectArea.x && hitPoint < _PerfectArea.y)
                {
                    PopUp.TimeHitPop.Play("Perfect");
                    if (timedType == TimedType.Hit)
                    {
                        _AttackHitState = TimedHitState.Perfect;
                        Attacker.PrefectTimeHit += 1;
                    }
                    else
                    {
                        _DefendHitState = TimedHitState.Perfect;
                        for (int i = 0; i < Defenders.Length; i++)
                            Defenders[i].PrefectTimeHit += 1;
                    }
                }
                else
                {
                    PopUp.TimeHitPop.Play("Miss");
                    if (timedType == TimedType.Hit)
                    {
                        _AttackHitState = TimedHitState.Miss;
                        Attacker.MissTimeHit += 1;
                    }
                    else
                    {
                        _DefendHitState = TimedHitState.Miss;
                        for (int i = 0; i < Defenders.Length; i++)
                            Defenders[i].MissTimeHit += 1;
                    }
                }

                endHit ();
            } else {
                PopUp.TimeHitPop.Play("Miss");
                if (timedType == TimedType.Hit)
                    _AttackHitState = TimedHitState.Miss;
                else
                    _DefendHitState = TimedHitState.Miss;
                endHit ();
			}
		}
	}
	private void setButtonImage(Sprite s){
		buttonImage.sprite = s;
	}

	#region Anim Event Func

	public void endHit(){
        Phase = 0;
        ConfirmObj.DOColor(new Color(ConfirmObj.color.r, ConfirmObj.color.g, ConfirmObj.color.b, 0), 0.05f);
		EventManager.Instance.TriggerEvent (new SetTimeHitStatusEvent(Attacker, Defenders,false));

        TimedHitState hitState;
        if (timedType == TimedType.Hit)
            hitState = _AttackHitState;
        else
            hitState = _DefendHitState;

        switch (hitState) {
		case TimedHitState.Perfect :
            WwiseManager.Instance.PlaySFX(WwiseManager.BattleFX.Time_Hit_Perfect);
            ani.SetTrigger("HitPerfect");
			break;
		case TimedHitState.Good :
            WwiseManager.Instance.PlaySFX(WwiseManager.BattleFX.Time_Hit_Good);
            ani.SetTrigger("HitTrue");
			break;
		case TimedHitState.Miss :
            WwiseManager.Instance.PlaySFX(WwiseManager.BattleFX.Time_Hit_Fail);
            ani.SetTrigger("HitFalse");
			break;
		}

		EventManager.Instance.TriggerEvent (new TimedHitFinishedEvent(Attacker, Defenders));
		if (timedType == TimedType.Hit) {
			if (OnTimedHit != null) {
				OnTimedHit (command, _AttackHitState, hitButtonId);
				Attacker.ActionBar.DestroyActionBar ();
			}
		} else {
			if (OnTimedDef != null) {
				OnTimedDef (Defenders[0], _DefendHitState, hitButtonId);
				Attacker.ActionBar.DestroyActionBar ();
			}
		}

        battleInput.onReqInput -= HandleonReqInput;
        onTimed = false;

        //battler.isTimeHit = false;
    }
	#endregion

	void DoPhaseOne(float currentTime, float rotateTo = -360f)
	{
        if (currentTime > _TimedHitLength && !KeyHasPressed)
        {
            PopUp.TimeHitPop.Play("Miss");
            if (timedType == TimedType.Hit)
                _AttackHitState = TimedHitState.Miss;
            else
                _DefendHitState = TimedHitState.Miss;
            endHit();
        }
		float rotation = (currentTime/_TimedHitLength) * rotateTo;
		HitIndicator.eulerAngles = new Vector3(0f,0f,rotation);

//		if (HitIndicator.localEulerAngles.z > 260f)
//			Phase = 2;
	}

    Sprite GetButtonImage(CommandPos direction)
    {
        if (InputManager.PlayerOneConfiguration.name == "KeyboardAndMouse")
        {
            if (direction == CommandPos.Up)
                return LegrandBackend.Instance.GetSpriteData("ControllerKeyboardW");
            else if (direction == CommandPos.Right)
                return LegrandBackend.Instance.GetSpriteData("ControllerKeyboardD");
            else if (direction == CommandPos.Down)
                return LegrandBackend.Instance.GetSpriteData("ControllerKeyboardS");
            else
                return LegrandBackend.Instance.GetSpriteData("ControllerKeyboardA");
        }
        else
        {
            if (direction == CommandPos.Up)
                return LegrandBackend.Instance.GetSpriteData("ControllerJoystickButton3");
            else if (direction == CommandPos.Right)
                return LegrandBackend.Instance.GetSpriteData("ControllerJoystickButton1");
            else if (direction == CommandPos.Down)
                return LegrandBackend.Instance.GetSpriteData("ControllerJoystickButton0");
            else
                return LegrandBackend.Instance.GetSpriteData("ControllerJoystickButton2");
        }
    }

    #if UNITY_EDITOR
    void Update (){
        if (onTimed)
        {
            if (Input.GetKeyDown(KeyCode.F6))
            {
                PopUp.TimeHitPop.Play("Perfect");
                if (timedType == TimedType.Hit)
                {
                    _AttackHitState = TimedHitState.Perfect;
                    Attacker.PrefectTimeHit += 1;
                }
                else
                {
                    _DefendHitState = TimedHitState.Perfect;
                    for (int i = 0; i < Defenders.Length; i++)
                        Defenders[i].PrefectTimeHit += 1;
                }

                endHit ();
            }
        }
    }
    #endif
//
//	void DoPhaseTwo()
//	{
//		HitIndicator.Rotate(new Vector3 (0f,0f, -6f));
//		if (HitIndicator.localEulerAngles.z < 100f)
//			Phase = 3;
//	}
//
//	void DoPhaseThree()
//	{
//		HitIndicator.Rotate(new Vector3 (0f,0f, 10f));
//		if (HitIndicator.localEulerAngles.z > 180f)
//			endHit();
//	}
}
