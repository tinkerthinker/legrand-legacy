﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

public class CameraSecondFader : MonoBehaviour {
	public float FadeDelay = 0f;
	public float FadeSpeed = 0.5f;

	public RawImage imageTexture;

	public delegate void TextureDelegateVoid();
	public TextureDelegateVoid OnFadeIn;
	public TextureDelegateVoid FadeInPreview;

    private Tween FadeTween;
    private Coroutine _StartCoroutine;
	public void TransToClear (float delay, float speed){
        KillTween();
        FadeTween = imageTexture.DOColor (new Color (1,1,1,0), speed).SetDelay (delay);
	}
	public void TransToTexture (float delay, float speed){
        KillTween();
        FadeTween = imageTexture.DOColor (new Color (1,1,1,1), speed).SetDelay (delay);
	}
	public void FadeTextureToClear (float delay){
		ActiveSecondCamera (true);
		imageTexture.DOColor (new Color (1,1,1,1), FadeSpeed).SetDelay (FadeDelay).OnComplete(ClearTexture);
	}
	public void ClearTexture (){
		imageTexture.DOColor (new Color (1,1,1,0), 0f);
	}

	public void ActiveTexture (){
		ActiveSecondCamera (true);
		imageTexture.DOColor (new Color (1,1,1,1), 0);
	}
	public void StartSecondCamera()
	{
		if (_StartCoroutine != null)
			StopCoroutine(_StartCoroutine);

		imageTexture.DOColor (new Color (1,1,1,1), FadeSpeed).OnComplete(StartTextureComplete).SetDelay(FadeDelay);
	}
	void StartTextureComplete()
	{
		imageTexture.color = new Color (1,1,1,0);
		if (OnFadeIn != null) OnFadeIn();
	}

	public void ActiveSecondCamera(bool b){
		this.gameObject.SetActive (b);
	}
    void KillTween(){
        if (FadeTween != null)
            FadeTween.Kill(false);
    }
}
