﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(BattleHelper))]
public class BattleHelperEditor : Editor {
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
		if (GUILayout.Button("Search Helper"))
		{
			BattleHelper script = (BattleHelper)target;
			script.Search();
		}
	}
	
	
}

[CustomEditor(typeof(BattleHelperBoss))]
public class BattleEditorHelper : Editor{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        if (GUILayout.Button("Search Helper"))
        {
            BattleHelperBoss script = (BattleHelperBoss)target;
            script.Search();
        }
    }
}
#endif