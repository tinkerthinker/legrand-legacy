﻿using UnityEngine;
using System.Collections;

public class BattlerInfo : MonoBehaviour {
	public string AttributeInfoText;
	public string InfoText;

	void OnMouseDown() {
		EventManager.Instance.TriggerEvent (new UpdateInfoTextEvent(AttributeInfoText + InfoText));
	}
}
