﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;

public class BattleHelperBoss : BattleHelper {

    public Transform PreviewAnimation;
    public Transform DeadCinematic;
	// Use this for initialization
    public override void Search()
    {
        HitArea = transform.Search("HLP_Hit_Area");
        HitRange = transform.Search("HLP_Hit_Range");
        PointerFocus = transform.Search("HLP_Focus_Camera");
        InfoHelper = transform.Search("HLP_Info");
        PreviewAnimation = transform.Search("HLP_Camera_Preview");
        DeadCinematic = transform.Search("HLP_Camera_Dead");

        if(HitRange == null) Debug.LogError(gameObject.name + " Hit Area Not Found!");
        if(HitRange == null) Debug.LogError(gameObject.name + " Hit Range Not Found!");
        if(PointerFocus == null) Debug.LogError(gameObject.name + " Focus Helper Not Found!");
        if(InfoHelper == null) Debug.LogError(gameObject.name + " Info Helper Not Found!");
        if(PreviewAnimation == null) Debug.LogError(gameObject.name + " Preview Helper Not Found!");
        if(DeadCinematic == null) Debug.LogError(gameObject.name + " Cinematic Helper Not Found!");
    }

    public override bool IsEmpty()
    {
        if(HitArea == null || HitRange == null || PointerFocus == null || InfoHelper == null || PreviewAnimation == null)
            return true;
        return false;
    }
}
