﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BattlerInfoPanel : MonoBehaviour {
	public Text Text;
	public GameObject Panel;
	void Start()
	{
		StartCoroutine (WaitUntilGameReady());
	}

	private IEnumerator WaitUntilGameReady()
	{
		while (!PartyManager.Instance._ReadyToPlay)
			yield return null;
		EventManager.Instance.AddListener<UpdateInfoTextEvent> (UpdateInfo);
	}

	void UpdateInfo(UpdateInfoTextEvent e)
	{
		this.Text.text = e.NewInfo;
	}

	void Update()
	{
		if (Input.GetKeyDown (KeyCode.Tab)) 
			Panel.SetActive(!Panel.activeSelf);
	}
}
