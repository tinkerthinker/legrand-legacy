﻿using UnityEngine;
using System.Collections;

public class BattleHelper : MonoBehaviour {
	public Transform HitArea;
	public Transform HitRange;
	public Transform PointerFocus;
	public Transform InfoHelper;
	public Vector2 SelectorSize;

	virtual public void Search()
	{
		HitArea = transform.Search("HLP_Hit_Area");
		HitRange = transform.Search("HLP_Hit_Range");
		PointerFocus = transform.Search("HLP_Focus_Camera");
		InfoHelper = transform.Search("HLP_Info");

		if(HitRange == null) Debug.LogError(gameObject.name + " Hit Area Not Found!");
		if(HitRange == null) Debug.LogError(gameObject.name + " Hit Range Not Found!");
		if(PointerFocus == null) Debug.LogError(gameObject.name + " Focus Helper Not Found!");
		if(InfoHelper == null) Debug.LogError(gameObject.name + " Info Helper Not Found!");
	}

	virtual public bool IsEmpty()
	{
		if(HitArea == null || HitRange == null || PointerFocus == null || InfoHelper == null)
			return true;
		return false;
	}
}
