﻿using UnityEngine;
using System.Collections;

public class CameraPreviewListener : MonoBehaviour {
    public delegate void PreviewEvent(CameraPreviewListener c);
    public event PreviewEvent DonePreview;
    public Battler battler;
    void Start (){
        battler = GetComponentInParent<Battler>();
    }
    void DonePreviewEvent (){
        if (DonePreview != null)
        {
            DonePreview(this);
        }
    }

    public void ClearPreviewEvent (){
        DonePreview = null;
    }
}
