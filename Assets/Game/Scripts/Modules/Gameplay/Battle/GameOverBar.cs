﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;

public class GameOverBar : MonoBehaviour {
	public Text GameOverText; 
	// Use this for initialization
	void Start () {
		GameOverText.gameObject.SetActive (false);
	}
	
	public void FadeInText (){
		GameOverText.gameObject.SetActive (true);
		GameOverText.DOColor (new Color (0, 0, 0, 1), 5f);
	}

    void OnDisable (){
        GameOverText.gameObject.SetActive (false);
        GameOverText.DOColor (new Color (0, 0, 0, 0), 0);
    }

}
