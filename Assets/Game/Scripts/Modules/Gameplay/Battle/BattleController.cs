﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Analytics;
using Legrand.core;
using TeamUtility.IO;

public enum InitBuff
{
    Normal,
    EnemyParalyzed,
    CharacterParalyzed
}

public enum BattlePhase
{
    Init,
    Command,
    NonBattleCommand,
    AICommand,
    Battle,
    EndWin,
    EndLose,
    WinFare,
    CalculateResult,
    ResultScreen,
    ForceEnd,
    PreviewEncounter,
    TutorialBattle
}

public class BattleController : MonoBehaviour
{

    private BattlePoolingSystem poolingSystem;
    //private GameObject pointer;

    private List<Battler> battlerList = new List<Battler>();
    private List<Battler> charList = new List<Battler>();
    private List<Battler> encounterList = new List<Battler>();
    private List<Battler> selectedTarget = new List<Battler>();

    private List<BattleCommand> commandList = new List<BattleCommand>();
    public List<BattleCommand> CommandList
    {
        get { return commandList; }
    }
    private List<BattleCommand> nonBattleCom = new List<BattleCommand>();
    private List<CommandData> encounterCommand = new List<CommandData>();
    private List<Vector2> _GridTarget = new List<Vector2>();

    public BattlePhase phase;

    private Battle battle;
    private BattleAICommand AICommandData;
    private CommandData commandData;

    private CommandInputController commandControl;
    private TargetController targetController;
    private TurnController turnController;

    private BattleEventListener eventListener;
    private TimedHitListener timedHitListener;
    [HideInInspector]
    public BattleInput battleInput;

    private int iChar = 0, iEnemy = 0, iCommand = 0, iTurn = 0;
    private bool onPrep = false, onInput = false, onBattle = false, fleeAble = false, collected = false, onDialog = false, onChangeCamera = false; //not used yet
    [HideInInspector]
    public TransitionData transData;

    private bool _BattleEnd;

    public ResultScreenController ResultScreen;
    //	public GameOverBar GameOverScreen;
    public ScreenFader BattleFader;

    private CollectDamage _collectDamage;
    private GameObject _CharacterSelectPointer;

    [HideInInspector]
    public BattlePosition PlayerBattlePos;
    [HideInInspector]
    public BattlePosition EnemyBattlePos;

    [HideInInspector]
    public bool CameraFirstMove; //wait command till camera end preview on first battle

    [HideInInspector]
    public bool OnTutorial = false;

    private string _TutorialName;

    public bool PrevSequence = false, waitForCinematic = false;

    private bool _CalledOnce = false;
    private bool _OnCheck = false;

    private Battler batCombination;
    private Battler BattlerTemp;

    [HideInInspector]
    public List<Battler> EncounterFirstMeet = new List<Battler>();

    [HideInInspector]
    public int ExperienceGain;

    public List<BaseAction> GlobalActionSeq = new List<BaseAction>();

    private Dictionary<string, int> _PlayersItems;

    private Dictionary<string, double> _BattlerData;

    private IDictionary<string, object> _AnalyticQuery;

    private List<Fact> ListOfFact;

    public InitBuff StatusInitBattle;

    private float _BattleStartTime;

    public WwiseManager.BGM BattleSong;
    // Use this for initialization
    void Awake()
    {
        _AnalyticQuery = new Dictionary<string, object>();
        AICommandData = GetComponent<BattleAICommand>();
        //		battleInput 		= GetComponent<BattleInput> ();
        timedHitListener = GetComponent<TimedHitListener>();

        //dummy AI command
        encounterCommand = AICommandData.AICommandList;
    }

#if UNITY_EDITOR
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F11))
        {
            for (int i = 0; i < charList.Count; i++)
            {
                charList[i].Character.ReceiveDamage(99999f);
            }
        }

        if (Input.GetKeyDown(KeyCode.F12))
        {
            for (int i = 0; i < encounterList.Count; i++)
            {
                encounterList[i].Character.ReceiveDamage(99999f);
            }
        }

        if (Input.GetKeyDown(KeyCode.F10))
        {
            for (int i = 0; i < charList.Count; i++)
            {
                charList[i].Health.Increase(charList[i].Health.MaxValue);
            }
        }

        if (Input.GetKeyDown(KeyCode.F9))
        {
            for (int i = 0; i < charList.Count; i++)
            {
                charList[i].GaugeCharge = 100;
                charList[i].UpdateGauge(100);
            }
        }
        if (Input.GetKeyDown(KeyCode.F8))
        {
            PartyManager.Instance.Inventory.AddItem(LegrandBackend.Instance.ItemData["IB-02"]);
        }
    }
#endif

    void OnDestroy()
    {
        if (EventManager.Instance != null)
            EventManager.Instance.RemoveListener<MoveBattlerEvent>(HandleMoveBattler);

        if (_CharacterSelectPointer != null) _CharacterSelectPointer.DestroyAPS();
    }

    #region init
    public void initBattle()
    {
        battle = Battle.Instance;
        _BattleStartTime = Time.unscaledTime;

        this.battlerList = battle.battlerList;
        this.transData = battle.transData;
        this.fleeAble = battle.fleeAble;
        this.charList = battle.charList;
        this.encounterList = battle.encounterList;

        PlayerBattlePos = new BattlePosition();
        EnemyBattlePos = new BattlePosition();

        poolingSystem = BattlePoolingSystem.Instance;

        battleInput = battle.BattleInputController;
        ResultScreen = battle.ResultScreen.GetComponentInChildren<ResultScreenController>();
        BattleFader = ResultScreen.ScreenFade;
 //       BattleFader = battle.ResultScreen.GetComponentInChildren<ScreenFader>();

        battleInput.startListen();
        timedHitListener.startListener();

        commandControl = new CommandInputController(battleInput);
        targetController = new TargetController(battleInput);
        eventListener = new BattleEventListener();
        turnController = new TurnController();

        _CharacterSelectPointer = BattlePoolingSystem.Instance.InstantiateAPS("CharacterCursor");
        _CharacterSelectPointer.SetActive(false);
        //		eventListener.initListener (this);
        ResponsiveQuery q = new ResponsiveQuery();
        q.Add("Concept", LegrandUtility.GetUniqueSymbol("TutorialEvent"));
        q.Add("IdParty", LegrandUtility.GetUniqueSymbol(battle.encounterPartyID));

        if (q.Match())
        {
            RuleResult result = DynamicSystemController._Instance.RuleDB.GetLatestResult();
            OnTutorial = true;
            _TutorialName = result.ResponseName;
            EventManager.Instance.AddListenerOnce<BattleTransitionDone>(CallPopUp);
        }

        phase = BattlePhase.TutorialBattle;
        turnController.onInited += turnControllerInited;
        turnController.onDone += DoneInitSpeed;
        turnController.initSpeed(battlerList);
        
        EventManager.Instance.AddListener<BattlerBuffEvent>(HandleBattlerBuffEvent);
    }

    private void turnControllerInited()
    {
        StartCoroutine(turnController.updateSpeed());
    }
    private void DoneInitSpeed(List<Battler> list)
    {
        turnController.onDone -= DoneInitSpeed;
        battlerList = list;
        EventManager.Instance.AddListener<OpenBattleEvent>(OpenBattle);
        EventManager.Instance.TriggerEvent(new BattleDonePrepared());
    }
    private void OpenBattle(OpenBattleEvent e)
    {
        onPrep = true;
        WwiseManager.Instance.PlayBG(BattleSong);
        EventManager.Instance.RemoveListener<OpenBattleEvent>(OpenBattle);
        StartCoroutine(callSwicther());
    }

    #endregion

    #region MainCoroutine
    IEnumerator callSwicther()
    {
        while (onPrep && !onBattle)
        {
            phaseSwitcher();
            yield return new WaitForSeconds(0.2f);
        }
    }

    private void phaseSwitcher()
    {
        switch (phase)
        {
            case BattlePhase.Init:
                initPhaseHandler();
                break;
            case BattlePhase.Command: //insert command player
                commandPhaseHandler();
                break;
            case BattlePhase.NonBattleCommand:
                nonBattleHandler();
                break;
            case BattlePhase.AICommand:
                AICommandHandler();
                break;
            case BattlePhase.Battle:
                battlePhaseHandler();
                break;
            case BattlePhase.EndWin:
                endPhaseHandler();
                break;
            case BattlePhase.EndLose:
                endPhaseHandler();
                break;
            case BattlePhase.ForceEnd:
                endPhaseHandler();
                break;
            case BattlePhase.PreviewEncounter:
                previewHandler();
                break;
            case BattlePhase.TutorialBattle:
                tutorialHandler();
                break;
        }
    }
    #endregion
    #region tutorial Handler
    void tutorialHandler()
    {
        if (!OnTutorial)
            phase = BattlePhase.PreviewEncounter;
    }
    private void CallPopUp(BattleTransitionDone b)
    {
        EventManager.Instance.RemoveListener<BattleTransitionDone>(CallPopUp);
        PopUpUI.OnEndPopUp += HandleOnEndPopUp;
        PopUpUI.CallTutorialPopUp(_TutorialName);
    }
    private void HandleOnEndPopUp(bool p)
    {
        PopUpUI.OnEndPopUp -= HandleOnEndPopUp;
        //		EventManager.Instance.TriggerEvent (new CameraChangeView());
        //		phase = BattlePhase.Command;
        //		Invoke ("DelayOnInput", 0.5f);
        //		onInput = true;
        //		phase = BattlePhase.PreviewEncounter;
        OnTutorial = false;
    }

    #endregion
    #region preview handler
    public void previewHandler()
    {
        //search apakah monster di list sudah pernah ditemui atau belum
        if (!PrevSequence)
        {
            PrevSequence = true;
            if (EncounterFirstMeet.Count > 0)
            {
                EventManager.Instance.TriggerEvent(new CameraPreviewEncounter());
            }
            else
            {
                battle.BattleCameraController._SecondCamera.SetActive(false);
                phase = BattlePhase.Init;
            }
        }
    }
    #endregion
    #region Phase Handler
    private void initPhaseHandler()
    {
        iChar = iEnemy = iCommand = 0;
        collected = false;
        onChangeCamera = false;
        _PlayersItems = new Dictionary<string, int>();
        Battler[] allPlayerBattler = charList.Concat(battle.reserveList).ToArray();
        for (int i = 0; i < allPlayerBattler.Length; i++)
        {
            MainCharacter mc = allPlayerBattler[i].Character as MainCharacter;
            for (int j = 0; j < mc.Items.Count; j++)
            {
                if (mc.Items[j] != null && mc.Items[j].ItemMaster != null && !_PlayersItems.ContainsKey(mc.Items[j].ItemMaster._ID))
                    _PlayersItems.Add(mc.Items[j].ItemMaster._ID, PartyManager.Instance.Inventory.GetStackItem(mc.Items[j].ItemMaster));//mc.Items[j].Stack);
            }
        }
        //check onTurnStart event list

        if (!OnTutorial && !_CalledOnce)
        {
            _CalledOnce = true;
            EventManager.Instance.TriggerEvent(new CameraStartBattle());
        }

        if (CameraFirstMove && phase != BattlePhase.ForceEnd)
        {
            phase = BattlePhase.Command;
            CheckStatusBattler();
            if (!charList[iChar].Health.IsDead && !charList[iChar].IsSkip && !onDialog)
            {
                //				Invoke ("DelayOnInput", 0.25f);
                onInput = true;
            }
        }
    }


    private void commandPhaseHandler()
    {
        charList[iChar].ItemEvent = BuffEvent.ON_TURN_START;
        if (!charList[iChar].Health.IsDead && !charList[iChar].IsSkip)
        {
            BattleChecker();
            showCommandPanel();
        }
        else
        {
            nextChar();
        }
    }
    private void nonBattleHandler()
    {
        execNonBattleCommand();
        battle.BattleCameraController.StopCommandCharacter();
    }
    private void AICommandHandler()
    {
        if (!collected)
        {
            collected = true;
            for (int i = 0; i < encounterList.Count; i++)
            {
                encounterList[i].ItemEvent = BuffEvent.ON_TURN_START;
                if (encounterList[i].BattlerState != BattlerState.Dead && !encounterList[i].IsSkip)
                {
                    //	getAITarget (encBat);
                    AICommandData.Decision(encounterList[i]);
                }
                else if (encounterList[i].IsSkip)
                {
                    //encounterList[i].PopUp("Paralyzed Effect", (int)BattleTextPopUp.BattleTextType.Debuff, false);
                    encounterList[i].IsSkip = true;
                    ItemController._Instance.ItemActionSkip(encounterList[i], 100);
                }
            }
        }

        phase = BattlePhase.Battle;
    }
    private void battlePhaseHandler()
    {
        //check onBattleStart event list
        _OnCheck = false;
        checkCondition();
    }
    private void endPhaseHandler()
    {
        commandControl.hideAll();
        foreach (Battler bat in charList.Concat(battle.reserveList))
        {
            bat.HealthBar.gameObject.SetActive(false);
        }
        if (phase == BattlePhase.EndLose)
            waitForCinematic = false;

        if (!waitForCinematic)
            checkBattleEnd();
    }
    #endregion
    #region collectData Battle
    private void sendBattlerData(Battler bat)
    {
        //        RuleResult ruleResult = new RuleResult();
        if (!_BattlerData.ContainsKey(bat.Character._Name))
        {
            _BattlerData["Is" + bat.Character._Name] = 1f;
            _BattlerData[bat.Character._Name + "Health"] = bat.Health.Value;
            _BattlerData[bat.Character._Name + "PositionX"] = bat.BattlePosition.x;
            _BattlerData[bat.Character._Name + "PositionY"] = bat.BattlePosition.y;

            if (bat.BuffDebuffStat.Count > 0)
            {
                for (int i = 0; i < bat.BuffDebuffStat.Count; i++)
                {
                    _BattlerData[bat.Character._Name + "Is" + bat.BuffDebuffStat[i].Name] = LegrandUtility.GetUniqueSymbol(bat.BuffDebuffStat[i].Name);
                }
            }
            if (bat.BattlerType == BattlerType.Enemy)
                _BattlerData["EnemyPartyID"] = LegrandUtility.GetUniqueSymbol((bat.Character as Encounter).PartyID);
        }

    }
    private void sendReserveData(Battler bat)
    {
        MainCharacter mainChar = (bat.Character as MainCharacter);
        if (!_BattlerData.ContainsKey(bat.Character._Name))
        {
            _BattlerData["Is" + bat.Character._Name] = -1f;
            _BattlerData[bat.Character._Name + "Health"] = mainChar.Health.Value;
            _BattlerData[bat.Character._Name + "PositionX"] = -1f;
            _BattlerData[bat.Character._Name + "PositionY"] = -1f;

        }

    }
    #endregion
    #region Turn Handler	
    private void nextChar()
    {
        //		EventManager.Instance.TriggerEvent (new CameraRandomView ());
        charList[iChar].HealthBar.unSelected();

//        charList.ForEach(x => x.StopShaderSequence());
        for (int i = 0; i < charList.Count; i++)
            charList[i].StopShaderSequence();
        _CharacterSelectPointer.SetActive(false);
        iChar++;

        if (iChar < charList.Count)
        {
            //			Invoke ("DelayOnInput", 0.5f);
            onInput = true;
        }
        else
        {
            onInput = false;
            if (!onChangeCamera)
            {
                onChangeCamera = true;
                battle.BattleCameraController.StopCommandCharacter();
            }
            commandControl.hideAll();
            phase = BattlePhase.NonBattleCommand;
        }

        //		onSelect = true;
    }
    private void prevChar()
    {
        //pointer.DestroyAPS ();
        if (BattlerTemp != null)
            BattlerTemp.transform.position = BattlerTemp.MyPlane.transform.position;

        //        if(_CurrentTargetPosition){
        //			_CurrentTargetPosition.placeAble = true;
        commandControl.hideAll();
        commandData = null;
        charList[iChar].HealthBar.unSelected();
//        charList.ForEach(x => x.StopShaderSequence());
        for (int i = 0; i < charList.Count; i++)
            charList[i].StopShaderSequence();
        _CharacterSelectPointer.SetActive(false);
        iChar--;
        if (iChar < 0)
        {
            iChar = 0;
        }

        if (charList[iChar].TargetPosition != null)
        {
            charList[iChar].TargetPosition.placeAble = true;
            charList[iChar].TargetPosition = null;
        }
        onInput = true;
        //		Invoke ("DelayOnInput", 0.5f);
        if (charList[iChar].BattlerState == BattlerState.Dead && iChar != 0 && charList[iChar].IsSkip == false)
        {
            prevChar();
        }

    }
    #endregion

    #region command handler
    private void showCommandPanel()
    {
        //input character command presets
        if (onInput)
        {
            onInput = false;
            charList[iChar].HealthBar.selected();

            if (BattlerTemp != null)
                BattlerTemp.transform.position = BattlerTemp.MyPlane.transform.position;

            battle.BattleCameraController.OnCommandCharacter(charList[iChar]);

            BattlerTemp = charList[iChar].GetAllyBehind(charList[iChar], true);
//            if (BattlerTemp != null)
//                BattlerTemp.transform.position = new Vector3(0, -10, 0);

            if (_CharacterSelectPointer != null)
            {
                _CharacterSelectPointer.SetActive(true);
                _CharacterSelectPointer.transform.position = charList[iChar].transform.position;
            }

            Transform batTrans = charList[iChar].GetComponent<Transform>();
            MainCharacter mainChar = charList[iChar].Character as MainCharacter;

            bool attackAble = (targetController.getAttackTarget(charList[iChar]).Length > 0) ? true : false;
            //commandControl = new CommandInputController(battleInput);
            removeOnInputCommand(charList[iChar].BattleID);

            commandControl.showMainPanel(attackAble, fleeAble, charList[iChar], _PlayersItems);
            //delegates command control
            commandControl.OnCommandInput += GetCommand;
            commandControl.OnCommandCancel += CancelCommand;
        }
    }

    private void GetCommand(CommandButton commandButton)
    {
        removeCommandHandler();
        Battler attacker = charList[iChar];
        MainCharacter mc = (attacker.Character as MainCharacter);
        commandData = CommandData.Copy(commandButton.commandButtonData);

        targetController.ClearOnSelectTarget();
        targetController.ClearOnCancel();
        targetController.OnSelectTarget += HandleOnSelectTarget;
        targetController.OnCancel += HandleOnCancel;

        switch ((CommandTypes)commandData.CommandType)
        {
            case CommandTypes.Attack:
                //turn 3
                attacker.BattleTurn = 3;
                targetController.selectAttackTarget(attacker);
                EventManager.Instance.TriggerEvent(new CameraRandomView());
                break;
            case CommandTypes.Magic:
                attacker.isCasting = false;
                attacker.BattleTurn = 2;
                targetController.selectMagicTarget(attacker, commandData.CommandValue);
                EventManager.Instance.TriggerEvent(new CameraRandomView());
                break;
            case CommandTypes.SkillUlti:
                //turn 1
                attacker.BattleTurn = 1;
                targetController.selectSkillUltiTarget(attacker, mc.BattleOffensiveSkill.SkillType);
                EventManager.Instance.TriggerEvent(new CameraRandomView());
                break;
            case CommandTypes.Item:
                bool onEnemy = false;
                //turn 0
                attacker.BattleTurn = 0;
                //check target and set state animation buff/debuff
                Consumable item = attacker.GetItem(commandData.CommandValue) as Consumable;
                if (item.ConsumableType != Consumable.ConsumeType.Healing)
                    onEnemy = ((item as ConsumableMagic).TargetFormation == TargetFormation.Enemy) ? true : false;


                if (onEnemy)
                    attacker.BattleStateCondition = BattleStateCondition.ItemDebuff;
                else
                    attacker.BattleStateCondition = BattleStateCondition.ItemBuff;

                targetController.ClearOnSelectItemTarget();
                targetController.OnSelectItemTarget += HandleOnItemSelected;
                targetController.selectItemTarget(attacker, commandData.CommandValue);
                EventManager.Instance.TriggerEvent(new CameraRandomView());
                break;
            case CommandTypes.Move:
                //turn 0
                attacker.BattleTurn = 0;
                targetController.OnSelectPos += HandleOnSelectPos;
                targetController.selectMoveTarget(attacker);
                EventManager.Instance.TriggerEvent(new CameraRandomView());
                break;
            case CommandTypes.Guard:
                //turn 0
                attacker.BattleTurn = 0;
                //			removeTargetHandler ();
                doneCommand();
                break;
            case CommandTypes.Flee:
                //turn -1
                attacker.BattleTurn = 8;
                //			removeTargetHandler ();
                doneCommand();
                break;
            case CommandTypes.Change:
                //turn 0
                attacker.BattleTurn = 0;
                targetController.OnSelectReserve += HandleOnReserve;
                targetController.ChanceCharacter(attacker);
                break;
            case CommandTypes.Skill:
                attacker.BattleTurn = 0;
                targetController.selectNormalSkillTarget(attacker, commandData.CommandValue);
                EventManager.Instance.TriggerEvent(new CameraRandomView());
                //               prevChar ();
                //                removeCommandHandler ();
                break;
            default:
                CancelCommand();
                break;
        }
    }
    private void removeCommandHandler()
    {
        commandControl.OnCommandInput -= GetCommand;
        commandControl.OnCommandCancel -= CancelCommand;
    }
    private void removeTargetHandler()
    {
        targetController.OnSelectTarget -= HandleOnSelectTarget;
        targetController.OnSelectPos -= HandleOnSelectPos;
        targetController.OnCancel -= HandleOnCancel;
        targetController.OnSelectReserve -= HandleOnReserve;
    }

    private void CancelCommand()
    {
        if (iChar != 0)
        {
            prevChar();
            removeCommandHandler();
        }
    }
    private void HandleOnReserve(Battler batReserve, int resvId)
    {

        commandData = null;
        //		batReserve.inReserve = false;
        //		charList [iChar].inReserve = true;

        //		EventManager.Instance.TriggerEvent (new CameraRandomView ());
        charList[iChar].HealthBar.unSelected();
        _CharacterSelectPointer.SetActive(false);
        //set reserve on battler
        batReserve.ReserveOnBattle(charList[iChar]);
        charList[iChar].SwitchCharacter();

        //set battle position
        PlayerBattlePos.Position[(int)charList[iChar].BattlePosition.x, (int)charList[iChar].BattlePosition.y] = batReserve;

        battle.swapCharacter(charList[iChar], iChar, batReserve, resvId);

        batReserve.OnDoneChange += DoneChange;

        battlerList = battle.battlerList;
        charList = battle.charList;

        batReserve.onChangeCommand = true;

        removeTargetHandler();
        print(iChar);
    }
    private void DoneChange()
    {
        charList[iChar].OnDoneChange -= DoneChange;
        turnController.SetBattlerList(battlerList);
        onInput = true;
    }
    private void HandleOnCancel()
    {
        removeTargetHandler();
        //		Invoke ("DelayOnInput", 0.3f);
        onInput = true;
    }


    void HandleOnSelectPos(BattlePlane plane)
    {
        removeTargetHandler();
        charList[iChar].TargetPosition = plane;
        charList[iChar].TargetPosition.placeAble = false;
        //		_CurrentTargetPosition = plane;
        //		_CurrentTargetPosition.placeAble = false;
        doneCommand();
    }

    void HandleOnSelectTarget(List<Battler> lists)
    {
        removeTargetHandler();
        selectedTarget.Clear();
        _GridTarget.Clear();
        selectedTarget = lists;


        //		print(lists[0]);

        if (selectedTarget.Count > 0)
        {
            for (int i = 0; i < selectedTarget.Count; i++)
                _GridTarget.Add(selectedTarget[i].BattlePosition);

            doneCommand();
        }
    }

    void HandleOnItemSelected(string itemID)
    {
        targetController.OnSelectItemTarget -= HandleOnItemSelected;
        _PlayersItems[itemID] = _PlayersItems[itemID] - 1;
    }

    private void doneCommand()
    {
        if (iChar < charList.Count)
        {
            collectCommand(charList[iChar]);
            nextChar();
        }
    }
    #endregion

    #region Collect command
    public void collectCommand(Battler battler)
    {
        BattleCommand batCommand = new BattleCommand();
        ItemController._Instance.CheckConfuseEffect(battler);
        if (!battler.isConfuse)
        {
            batCommand.attacker = battler;
            batCommand.commandTurn = battler.BattleTurn;
            batCommand.target = new List<Battler>(selectedTarget);
            batCommand.targetPos = battler.TargetPosition;
            batCommand.isCasting = battler.isCasting;
            batCommand.batPartner = batCombination;
            batCommand.commandData = commandData;

            for (int i = 0; i < _GridTarget.Count; i++)
                batCommand.GridTarget.Add(_GridTarget[i]);
        }
        else
        {
            batCommand = ItemController._Instance.ConfuseCommand(battler);
        }


        commandList.Add(batCommand);

        //		if (batCombination != null) {
        //			charList [iChar].StateController.BattlerCombo = batCombination;
        //			batCombination.StateController.BattlerCombo = charList [iChar];
        //		}


        batCombination = null;
        selectedTarget.Clear();
        _GridTarget.Clear();
        commandData = null;
    }
    #endregion

    #region Non-Battle Command
    private void execNonBattleCommand()
    {
        onPrep = false;
        int[] ct = new int[] { (int)CommandTypes.Guard, (int)CommandTypes.Move, (int)CommandTypes.Change };
        nonBattleCom.Clear();
        nonBattleCom = commandList.FindAll(x => ct.Contains(x.commandData.CommandType));

        //		int i = 0;
        if (nonBattleCom.Count > 0)
        {
            for (int j = 0; j < nonBattleCom.Count; j++)
            {
                executeCommand(nonBattleCom[j], 1);
                //				i++;
            }
        }
        else
        {
            if (charList.Count > 0 && charList.All(x => x.BattlerState == BattlerState.Dead))
                phase = BattlePhase.EndLose;
            else
            {
                phase = BattlePhase.AICommand;
            }

            onPrep = true;
            StartCoroutine(callSwicther());
        }
    }
    #endregion
    #region Dummy AI
    public void AddSelectedTarget(Battler enc, List<Battler> targetsList)
    {
        List<Battler> targets = targetsList;
        selectedTarget = targets;
        //		selectedTarget.Add (target);
    }
    public void SetAICommand(Battler enc, CommandData getAICommand)
    {
        commandData = getAICommand;
    }
    #endregion

    #region check Item status
    private void CheckStatusBattler()
    {
        if (!_OnCheck)
        {
            _OnCheck = true;
            iTurn++;
            _BattlerData = new Dictionary<string, double>();
            _BattlerData["PartyID"] = LegrandUtility.GetUniqueSymbol(battle.encounterPartyID);
            _BattlerData["BattleTurn"] = iTurn;
            for (int i = 0; i < battlerList.Count; i++)
            {
                battlerList[i].ItemEvent = BuffEvent.ON_INIT;
                battlerList[i].ActivateBattler(true);
                if (battlerList[i].ParticleGuard != null)
                    battlerList[i].ParticleGuard.DestroyBattleAPS(); ;
                if (battlerList[i].BattlerType == BattlerType.Player)
                {
                    PlayerBattlePos.Position[(int)battlerList[i].BattlePosition.x, (int)battlerList[i].BattlePosition.y] = null;
                    PlayerBattlePos.Add(battlerList[i].BattlePosition, battlerList[i]);
                    if (StatusInitBattle == InitBuff.CharacterParalyzed)
                        SetParalyzed(battlerList[i]);
                    battlerList[i].HealthBar.ResetScaleBar();
                }
                else
                {
                    EnemyBattlePos.Add(battlerList[i].BattlePosition, battlerList[i]);
                    if (StatusInitBattle == InitBuff.EnemyParalyzed)
                        SetParalyzed(battlerList[i]);
                }

                battlerList[i].CheckBossOnEachTurn();

                if (battlerList[i].BattlerState != BattlerState.Dead && battlerList[i].BattlerState != BattlerState.Preview)
                    battlerList[i].BattlerState = BattlerState.Idle;

                if (battlerList[i].TimedHit != null)
                {
                    battlerList[i].TimedHit.gameObject.SetActive(false);
                    battlerList[i].TimedHit.DefendHitState = TimedHitState.None;
                    battlerList[i].TimedHit.AttackHitState = TimedHitState.None;
                }

                if (battlerList[i].BuffDebuffStat.Count > 0)
                {
                    ItemController._Instance.CheckBuffCondition(battlerList[i]);
                    ItemController._Instance.ExecBuff(battlerList[i], battlerList[i].ItemEvent);
                }

                if (battlerList[i].MyPlane != null)
                {
                    battlerList[i].MyPlane.placeAble = false;
                }

                if (battlerList[i].SkipChance > 0f && LegrandUtility.Random(0f, 100f) < battlerList[i].SkipChance)
                {
                    if (StatusInitBattle == InitBuff.Normal)
                    {
                        battlerList[i].IsSkip = true;
                        ItemController._Instance.ItemActionSkip(battlerList[i], 100);
                    }
                    //                    if (battlerList[i].BattlerType == BattlerType.Player)
                    //                        battlerList[i].PopUp("Paralyzed Effect", (int)BattleTextPopUp.BattleTextType.Debuff, false);
                }
                else
                    battlerList[i].IsSkip = false;

                if (battlerList[i].isInterrupt && !PartyManager.Instance.TutorialProgress.ContainsKey("Interrupt"))
                {
                    PopUpUI.CallTutorialPopUp("Interrupt");
                }

                battlerList[i].isInterrupt = false;
                battlerList[i].onChangeCommand = false;
                battlerList[i].isCombination = false;
                battlerList[i].BattleStateCondition = BattleStateCondition.None;

                battlerList[i].isTimeHit = false;

                //                if (battlerList[i].Character._ID == "CR-09" && battlerList[i].BattlerState != BattlerState.Dead && iTurn > 3)
                //                {
                //                    phase = BattlePhase.ForceEnd;
                //                }
                //                if (battlerList[i].BattlerType == BattlerType.Player || battlerList[i].Character.name == "RampoksLeader")
                sendBattlerData(battlerList[i]);

            }

            for (int x = 0; x < battle.reserveList.Count; x++)
            {
                sendReserveData(battle.reserveList[x]);
            }

            ResponsiveQuery query = new ResponsiveQuery();
            query.Add(_BattlerData);
            CheckDialogQuery(query);

            StatusInitBattle = InitBuff.Normal;

        }
    }

    void SetParalyzed(Battler bat)
    {
        bool OutBool = false;
        Buff buff = new Buff();
        buff._ID = 3;
        buff.Name = "Paralyzed";
        buff.Type = BuffType.ACTION_SKIP;
        buff.EventType = BuffEvent.ON_TURN_START;
        buff.Turns = 2;
        buff.value = 100;
        buff.ActionsBuffEnum.Add(CommandTypes.Attack);
        buff.StatusBuff = Status.Paralyze;
        buff.StatusType = StatusType.Debuff;

        bat.BuffDebuffStat.Add(buff);

        bat.IsSkip = true;

        ItemController._Instance.SearchBuffAction(bat, buff, out OutBool);
    }
    #endregion

    #region Pre Battle
    private void checkCondition()
    {
        if (encounterList.All(b => b.BattlerState == BattlerState.Dead && b.Character._ID != "CB-01"))
        {
            //win condition
            RemoveParticleActive();
            phase = BattlePhase.EndWin;
        }
        else if (charList.All(b => b.BattlerState == BattlerState.Dead))
        {
            //lose condition
            RemoveParticleActive();
            phase = BattlePhase.EndLose;
        }
        else
        {
            checkCommands();
        }
    }
    #endregion
    private void RemoveParticleActive()
    {
        if (commandList.Count > 0)
        {
            for (int i = 0; i < commandList.Count; i++)
            { // each (BattleCommand bc in commandList) {
                if (commandList[i].magicCasted != null)
                {
                    commandList[i].magicCasted.DestroyBattleAPS();
                    //					}
                }
            }
        }
    }
    #region Battle
    private void checkCommands()
    {
        if (commandList.Count > 0)
        {
            //iCommand = 0; nonBattle
            //icommand = 1; skill; one by one;
            //icommand = 2; magic; cast animation
            //icommand = 3; attack; basic melee and range attack; single magic;
            //icommand = 4,5,6; prior on range magic multi target;
            //icommand = 4,5,6; after all range executed, melee magic multi target;
            if (commandList.Any(x => x.commandTurn == iCommand))
            {
                onBattle = true;
                int i = 0;
                List<BattleCommand> cTemp = commandList.Where(x => x.commandTurn == iCommand && x.isExecuted == false).ToList();

                if (cTemp.Count > 0)
                {
                    switch (iCommand)
                    {
                        case 0:
                        case 2:
                        case 3:
//                            foreach (BattleCommand bc in cTemp)
//                            {
//                                executeCommand(bc, i);
//                                i++;
//                            }
                            for (int x = 0; x < cTemp.Count; x++)
                            {
                                executeCommand(cTemp[x], i);
                                i++;
                            }
                            break;
                        case 1:
                            executeCommand(cTemp.First(), 0);
                            break;
                        case 4:
                        case 5:
                        case 6:
                        case 7:
                        case 8:
                            executeCommand(cTemp.First(), 0);
                            break;
                    }
                }
                else
                {
                    nextCommand();
                }
            }
            else
            {
                nextCommand();
            }
        }
        else
        {
            turnController.regenerateSpeed(battlerList);
            turnController.onDone += reInitSpeed;
            //end battle phase
        }
    }
    private void executeCommand(BattleCommand bc, int prior)
    {
        bc.attacker.ItemEvent = BuffEvent.ON_BATTLE_START;

        for (int i = 0; i < battlerList.Count; i++)
        {
            if (battlerList[i].TimedHit != null) battlerList[i].TimedHit.gameObject.SetActive(true);
        }

        if (!bc.attacker.BattlerState.Equals(BattlerState.Dead) && !bc.isExecuted && !bc.attacker.IsSkip)
        {
            bc.isExecuted = true;

            bc.attacker.OnDead += HandleOnDead;
            bc.attacker.OnShiftAction += HandleOnShiftAction;
            bc.attacker.OnDoneAction += HandleOnDoneAction;
            bc.attacker.ActionController.initAct(bc, prior);

        }
        else if (bc.attacker.BattlerState.Equals(BattlerState.Dead) && !bc.isExecuted || bc.attacker.IsSkip)
        {
            if (bc.magicCasted != null)
            {
                bc.magicCasted.DestroyBattleAPS();
                //				PKFxFX fx = bc.magicCasted.GetComponentInChildren<PKFxFX> ();
                //				if (fx) {
                //					fx.StopEffect ();
                //					bc.magicCasted.DestroyBattleAPS ();
                //				}
            }
            removeCommand(bc);
        }
    }

    void HandleOnShiftAction(Battler battler)
    {
        battler.OnShiftAction -= HandleOnShiftAction;
        BattleCommand bc = getCommandByID(battler.BattleID);
        shiftCasterCommand(bc);
    }
    void HandleOnDoneAction(Battler battler)
    {
        BattleCommand bc = getCommandByID(battler.BattleID);
        if (bc != null)
        {
            if (bc.isExecuted)
            {
                //				AllMagicTarget(bc);
            }
        }
        else
            //print("BC NULL");

            battler.OnDead -= HandleOnDead;
        battler.OnShiftAction -= HandleOnShiftAction;
        battler.OnDoneAction -= HandleOnDoneAction;


        if (bc != null)
            removeCommand(bc);

    }
    void HandleOnDead(Battler battler)
    {
        battler.OnDead -= HandleOnDead;
        battler.OnShiftAction -= HandleOnShiftAction;
        battler.OnDoneAction -= HandleOnDoneAction;

        RemoveTargetedBy(battler);

        BattleCommand bc = getCommandByID(battler.BattleID);
        if (bc != null)
            removeCommand(bc);
    }

    #endregion
    public void cameraShake(bool crit)
    {
        CameraShakeInstance shakeInstance;
        if (crit)
        {
            shakeInstance = CameraShaker.Instance.ShakeOnce(13f, 9f, 0.8f, 1f); //+3
                                                                                //BattleCam.blurCrit();
        }
        else
        {
            shakeInstance = CameraShaker.Instance.ShakeOnce(5f, 5f, 0.7f, 0.9f); //+5
        }
    }
    #region Post Battle
    private void reInitSpeed(List<Battler> list)
    {
        turnController.onDone -= reInitSpeed;
        battlerList = list;
        for (int i = 0; i < battlerList.Count; i++)
        {
            if (battlerList[i].Health.IsDead)
                battlerList[i].BattlerState = BattlerState.Dead;
        }

        if (encounterList.All(b => b.BattlerState == BattlerState.Dead && b.Character._ID != "CB-01"))
        {
            phase = BattlePhase.EndWin;
        }
        else if (charList.All(b => b.BattlerState == BattlerState.Dead))
        {
            phase = BattlePhase.EndLose;
        }
        else
        {
            phase = BattlePhase.Init;
        }

        BattleChecker();
    }

    private void BattleChecker()
    {
        if (encounterList.All(b => b.BattlerState == BattlerState.Dead && b.Character._ID != "CB-01"))
        {
            phase = BattlePhase.EndWin;
        }
        else if (charList.All(b => b.BattlerState == BattlerState.Dead))
        {
            phase = BattlePhase.EndLose;
        }

    }
    #endregion

    #region Command modifier
    private BattleCommand getCommandByID(int battleID)
    {
        return commandList.Find(x => x.attacker.BattleID == battleID);
    }
    private void removeOnInputCommand(int battleID)
    {
        BattleCommand bc = commandList.Find(x => x.attacker.BattleID == battleID);
        if (bc != null)
        {
            // If item, return item to temp
            if (bc.commandData.CommandType == (int)CommandTypes.Item)
            {
                string itemID = bc.attacker.GetItem(bc.commandData.CommandValue)._ID;
                _PlayersItems[itemID] = _PlayersItems[itemID] + 1;
            }
            removeCommand(bc);
        }
    }
    public void shiftCasterCommand(BattleCommand bc)
    {
        string MagicEffect = bc.attacker.GetMagicEffect(bc, true);
        Vector3 position = new Vector3(bc.attacker.transform.position.x, bc.attacker.transform.position.y, bc.attacker.transform.position.z);
        if (MagicEffect == "" || MagicEffect == "Ruin")
            MagicEffect = "OrbCasting";

        GameObject magicCast = poolingSystem.InstantiateAPS(MagicEffect, position, new Quaternion()) as GameObject;
//        magicCast.transform.position = new Vector3(bc.attacker.transform.position.x, magicCast.transform.position.y, bc.attacker.transform.position.z);
        TargetType magicTarget = bc.attacker.GetMagicTarget(bc.commandData.CommandValue);

        //		PKFxFX fx = magicCast.GetComponentInChildren<PKFxFX> ();
        //		if (fx)
        //			fx.StartEffect ();

        switch (magicTarget)
        {
            case TargetType.Single:
                bc.commandTurn = 4;
                break;
            case TargetType.Row:
                bc.commandTurn = 5;
                break;
            case TargetType.Column:
                bc.commandTurn = 6;
                break;
            case TargetType.Square:
                bc.commandTurn = 7;
                break;
            case TargetType.All:
                bc.commandTurn = 7;
                break;
        }

        bc.magicCasted = magicCast;
        //		bc.attacker.CastingFX = fx;
        bc.isExecuted = false;
        bc.isCasting = true;
        bc.attacker.isCasting = true;
        checkShiftedCommands();
    }
    public void removeCommand(BattleCommand bc)
    {
        commandList.Remove(bc);

        if (nonBattleCom.Count > 0)
        {
            nonBattleCom.Remove(bc);
        }
        checkCurrentCommands();
    }
    private void checkShiftedCommands()
    {
        if (commandList.Where(x => x.commandData.CommandType == (int)CommandTypes.Magic).All(x => x.isCasting))
        {
            nextCommand();

            onPrep = true;
            onBattle = false;
            StartCoroutine(callSwicther());
        }
    }
    private void checkCurrentCommands()
    {
        if (nonBattleCom.Count <= 0 && phase == BattlePhase.NonBattleCommand)
        {
            phase = BattlePhase.AICommand;
            onPrep = true;
            onBattle = false;
            StartCoroutine(callSwicther());

        }
        else if (phase == BattlePhase.Battle)
        {
            if (commandList.Count > 0)
            {
                if (commandList.Where(x => x.commandTurn == iCommand).All(x => x.isDone))
                {
                    nextCommand();
                    onPrep = true;
                    onBattle = false;
                    StartCoroutine(callSwicther());
                }
                else if (commandList.Where(x => x.commandTurn == iCommand).All(x => !x.isExecuted))
                {
                    onPrep = true;
                    onBattle = false;
                    StartCoroutine(callSwicther());
                }
            }
            else
            {
                onPrep = true;
                onBattle = false;
                StartCoroutine(callSwicther());
            }
        }
    }

    private void nextCommand()
    {
        iCommand++;
    }
    //check on clash melee attack (always single target);
    public bool CheckTargetCommand(Battler checker, Battler target)
    {
        for (int i = 0; i < commandList.Count; i++)
        { // each (BattleCommand command in commandList) {
            if (commandList[i].attacker.BattleID.Equals(target.BattleID) && !target.isCasting)
            {
                if (commandList[i].target.Count > 0 && commandList[i].target[0].BattleID.Equals(checker.BattleID))
                {
                    return true;
                }
            }
        }
        return false;
    }
    public void RemoveTargetedBy(Battler attacker)
    {
        for (int i = 0; i < battlerList.Count; i++)
        { // each (Battler battler in battlerList) {
            if (battlerList[i].Targeted.Contains(attacker))
            {
                battlerList[i].Targeted.Remove(attacker);
            }
        }
    }
    public Battler GetBattlerByFormation(BattlerType type, Vector2 pos)
    {
        List<Battler> lists = new List<Battler>();

        if (type == BattlerType.Player) lists = charList;
        else lists = encounterList;


        return lists.Find(x => x.BattlePosition.Equals(pos) && x.BattlerState != BattlerState.Dead);


    }
    public List<Battler> GetRowBattlers(Battler attacker, BattlerType type, bool front)
    {
        List<Battler> lists = new List<Battler>();
        Vector2 pos = (front) ? new Vector2(0, 1) : new Vector2(1, 0);

        if (type == BattlerType.Player) lists = charList;
        else lists = encounterList;

        return lists.FindAll(x => x.BattlePosition.y.Equals(pos.y) && x.BattlerState != BattlerState.Dead && x != attacker);
    }
    public List<Battler> GetRowBattlers(Battler target)
    {
        List<Battler> lists = new List<Battler>();
        lists = encounterList;
        if (target.BattlePosition.y == 0)
            return lists.FindAll(x => x.BattlePosition.y.Equals(1) && x.BattlerState != BattlerState.Dead);
        else
            return null;
    }

    #endregion

    #region Battle End
    public void fleePrompt()
    {
        //create prompt popup
        //return bool value
        bool fleeNow = true;
        if (fleeNow && !_BattleEnd)
        {
            onPrep = false;

            foreach (Battler mainBat in charList.Concat(battle.reserveList))
            {
                mainBat.HealthBar.gameObject.SetActive(false);
                mainBat.isFlee = true;
                mainBat.inReserve = false;
                mainBat.RemoveController();
            }
            ResultScreen.OnLoseBattle();

            _AnalyticQuery.Clear();
            _AnalyticQuery["encounter_party_id"] = battle.encounterPartyID;
            float time_elapsed = Time.unscaledTime - _BattleStartTime;
            _AnalyticQuery["battle_time"] = ((int)time_elapsed / 3600).ToString("00") + "." + (((int)time_elapsed % 3600) / 60).ToString("00") + "." + (((int)time_elapsed % 3600) % 60).ToString("00");
            int index = 0;
            for (int i = 0; i < charList.Count; i++)
            {
                string s = string.Empty;
                MainCharacter mc = charList[i].Character as MainCharacter;
                index = i + 1;
                _AnalyticQuery["party_" + index + "_name"] = mc._Name;
                _AnalyticQuery["party_" + index + "_level"] = mc.Level;
                _AnalyticQuery["party_" + index + "stat"] = mc.GetAttribute(Attribute.STR) + "_" + mc.GetAttribute(Attribute.VIT) + "_" + mc.GetAttribute(Attribute.INT) + "_" + mc.GetAttribute(Attribute.AGI) + "_" + mc.GetAttribute(Attribute.LUCK);
                _AnalyticQuery["party_" + index + "_equipment"] = mc.Equipment().Name;
                _AnalyticQuery["party_" + index + "_health"] = mc.Health.Value + "/" + mc.Health.MaxValue;
                _AnalyticQuery["party_" + index + "_misstimedhit"] = charList[i].MissTimeHit;
                _AnalyticQuery["party_" + index + "_goodtimedhit"] = charList[i].GoodTimeHit;
                _AnalyticQuery["party_" + index + "_misstimedhit"] = charList[i].PrefectTimeHit;
            }
            Analytics.CustomEvent("fleebattle", _AnalyticQuery);

            BattleFader.OnFadeOut += BattleEnd;
            BattleFader.EndScene();
        }
    }
    private void checkBattleEnd()
    {
        if (phase == BattlePhase.EndWin)
        {
            battle.SetStatusActive(false);
            onPrep = false;
            phase = BattlePhase.CalculateResult;

            //			BattleFader.FadeSpeed = 0.5f;
            //			BattleFader.FadeToBlack (0);

            //			Invoke ("BattleWinningView", 1f);
            StartCoroutine(BattleWinningView());

            //			Invoke ("ShowResultScreen", 3f);
        }
        else if (transData != null && transData.mustLose)
        {
            if (!_BattleEnd)
            {
                onPrep = false;
                _BattleEnd = true;

                ResultScreen.OnLoseBattle();

                BattleFader.OnFadeOut += BattleEnd;
                BattleFader.EndScene();
            }
        }
        else
        {
            //SoundManager.StopBackgroundMusic(true);
            if (!_BattleEnd)
            {
                _BattleEnd = true;
                onPrep = false;

                WwiseManager.Instance.StopBG();

                if (phase == BattlePhase.ForceEnd)
                {
                    foreach (Battler mainBat in charList.Concat(battle.reserveList))
                    {
                        mainBat.ApplyExp(0);//(totalExp);
                    }

                    _AnalyticQuery.Clear();
                    _AnalyticQuery["encounter_party_id"] = battle.encounterPartyID;
                    float time_elapsed = Time.unscaledTime - _BattleStartTime;
                    _AnalyticQuery["battle_time"] = ((int)time_elapsed / 3600).ToString("00") + "." + (((int)time_elapsed % 3600) / 60).ToString("00") + "." + (((int)time_elapsed % 3600) % 60).ToString("00");
                    int index = 0;
                    for (int i = 0; i < charList.Count; i++)
                    {
                        string s = string.Empty;
                        MainCharacter mc = charList[i].Character as MainCharacter;
                        index = i + 1;
                        _AnalyticQuery["party_" + index+"_name"] = mc._Name;
                        _AnalyticQuery["party_" + index + "_level"] = mc.Level;
                        _AnalyticQuery["party_" + index + "stat"] = mc.GetAttribute(Attribute.STR) + "_" + mc.GetAttribute(Attribute.VIT) + "_" + mc.GetAttribute(Attribute.INT) + "_" + mc.GetAttribute(Attribute.AGI) + "_" + mc.GetAttribute(Attribute.LUCK);
                        _AnalyticQuery["party_" + index + "_equipment"] = mc.Equipment().Name;
                        _AnalyticQuery["party_" + index + "_health"] = mc.Health.Value + "/" + mc.Health.MaxValue;
                        _AnalyticQuery["party_" + index + "_misstimedhit"] = charList[i].MissTimeHit;
                        _AnalyticQuery["party_" + index + "_goodtimedhit"] = charList[i].GoodTimeHit;
                        _AnalyticQuery["party_" + index + "_misstimedhit"] = charList[i].PrefectTimeHit;
                    }
                    Analytics.CustomEvent("winbattle", _AnalyticQuery);

                    ResultScreen.OnLoseBattle();
                    BattleFader.OnFadeOut += BattleEnd;
                    BattleFader.EndScene();
                }
                else
                {
                    foreach (Battler bat in charList.Concat(battle.reserveList))
                    {
                        bat.inReserve = false;
                        bat.onBattle = false;
                    }

                    //					SceneManager.SwitchScene ("TitleScene");
                    BattleFader.FadeSpeed = 10;
                    BattleFader.OnFadeOut += TransToTittle;
                    BattleFader.EndScene();

                    _AnalyticQuery.Clear();
                    _AnalyticQuery["encounter_party_id"] = battle.encounterPartyID;
                    int index = 0;
                    for (int i = 0; i < charList.Count; i++)
                    {
                        string s = string.Empty;
                        MainCharacter mc = charList[i].Character as MainCharacter;
                        index = i + 1;
                        _AnalyticQuery["party_" + index + "_name"] = mc._Name;
                        _AnalyticQuery["party_" + index + "_level"] = mc.Level;
                        _AnalyticQuery["party_" + index + "stat"] = mc.GetAttribute(Attribute.STR)+"_"+ mc.GetAttribute(Attribute.VIT)+"_"+ mc.GetAttribute(Attribute.INT)+"_"+ mc.GetAttribute(Attribute.AGI)+"_"+ mc.GetAttribute(Attribute.LUCK);
                        _AnalyticQuery["party_" + index + "_equipment"] = mc.Equipment().Name;
                    }

                    Analytics.CustomEvent("battlegameover", _AnalyticQuery);
                    WwiseManager.Instance.PlayBG(WwiseManager.BGM.Game_Over);
                    EventManager.Instance.TriggerEvent(new CameraGameOver());
                    battle.GameOverScreen.FadeInText();
                    StartCoroutine(gameOverScreenHandler());
                }
            }
        }

        if (_CharacterSelectPointer != null) _CharacterSelectPointer.DestroyAPS();
    }

    public void TransToTittle()
    {
        BattleFader.OnFadeOut -= TransToTittle;
        SceneManager.SwitchScene("TitleScene");
    }
    private IEnumerator BattleWinningView()
    {
        _BattleEnd = true;
        WwiseManager.Instance.PlayBG(WwiseManager.BGM.Win_Battle);
        yield return new WaitForSeconds(1);
        EventManager.Instance.TriggerEvent(new CameraEndBattle());
        for (int i = 0; i < charList.Count; i++)
        {// each (Battler mainBat in charList) {
            charList[i].StopShaderSequence();
            if (charList[i].BattlerState != BattlerState.Dead)
            {
                charList[i].BattlerState = BattlerState.Winning;
                if (charList[i].ParticleGuard != null)
                    charList[i].ParticleGuard.DestroyBattleAPS();
            }

            if (charList[i].WeaponFX.Count > 0)
            {
                for (int x = 0; x < charList[i].WeaponFX.Count; x++)
                {
                    charList[i].WeaponFX[x].DeactiveParticle();
                }
            }
        }
        yield return new WaitForSeconds(3);
        ShowResultScreen();
    }
    public void ShowResultScreen()
    {

        _AnalyticQuery.Clear();
        if (transData != null & !transData.mustLose)
        { //transData != null && 
            //apply exp, loot, etc
            List<VectorStringInteger> retrievedlLoots = new List<VectorStringInteger>();
            int totalExp = 0;
            if (ExperienceGain != -1)
                totalExp = ExperienceGain; // experience gain, input data manual dari database

            int encounterLevel = Formula.GetAveragePartyLevel(encounterList);
            float lootBonus = Formula.GetPartyLootLuck(charList.ToArray());
            for (int y = 0; y < encounterList.Count; y++)//each(Encounter enc in encounterList.ConvertAll(x => x.Character))
            {
                Encounter enc = encounterList[y].Character as Encounter;
                
                if (enc.Level > 0) //jika ada monster yang level 0 maka pakai perhitungan
                    totalExp += enc.Exp;

                #region Quest Trigger Battle Event
                string monsterID = enc._ID;
                double monsterKilled = PartyManager.Instance.WorldInfo.GetMonsterKillCount(monsterID);
                PartyManager.Instance.WorldInfo.UpdateMonsterKilled(monsterID, monsterKilled + 1);
                #endregion

                enc.RetrieveLoots(lootBonus, ref retrievedlLoots);
            }

            // New total exp, it's for new party member
            float bonusExpSetting = 0f;
            if (!GameSetting.OneButtonAct) bonusExpSetting += 0.1f;
            if (GameSetting.RandomActPosition) bonusExpSetting += 0.1f;
            totalExp = Mathf.RoundToInt(totalExp * (1f + bonusExpSetting));

            PartyManager.Instance.TotalEXP += totalExp;

            // Put list of loot into result screen
            ResultScreen.Loots = new List<VectorStringInteger>(retrievedlLoots);

            string s = string.Empty;
            for (int i = 0; i < retrievedlLoots.Count; i++)
            {
                s += i > 0 ? "," : string.Empty;
                s += retrievedlLoots[i].x + retrievedlLoots[i].y;
            }
            _AnalyticQuery["loot"] = s;

            ResultScreen.EXP = totalExp;// totalExp;
            foreach (Battler mainBat in charList.Concat(battle.reserveList))
            {
                mainBat.ApplyExp(Mathf.RoundToInt((totalExp * Formula.ExpBonus(mainBat.PrefectTimeHit, mainBat.GoodTimeHit, mainBat.MissTimeHit))
                                                 +
                                                   (totalExp * Formula.ExpLevelDifferencesBonus((mainBat.Character as MainCharacter).Level, encounterLevel))
                                  ));
            }
            // Non active character
            for(int i=0;i<PartyManager.Instance.NonActiveParty.Count;i++)
            {
                MainCharacter nonActiveChar = PartyManager.Instance.NonActiveParty[i];
                nonActiveChar.IncreaseEXP(totalExp + Mathf.RoundToInt(totalExp * Formula.ExpLevelDifferencesBonus(nonActiveChar.Level, encounterLevel)));
            }
        }

        _AnalyticQuery["encounter_party_id"] = battle.encounterPartyID;
        float time_elapsed = Time.unscaledTime - _BattleStartTime;
        _AnalyticQuery["battle_time"] = ((int)time_elapsed / 3600).ToString("00") + "." + (((int)time_elapsed % 3600) / 60).ToString("00") + "." + (((int)time_elapsed % 3600) % 60).ToString("00");
        int index = 0;
        for (int i = 0; i < charList.Count; i++)
        {
            string s = string.Empty;
            MainCharacter mc = charList[i].Character as MainCharacter;
            index = i + 1;
            _AnalyticQuery["party_" + index + "_name"] = mc._Name;
            _AnalyticQuery["party_" + index + "_level"] = mc.Level;
            _AnalyticQuery["party_" + index + "stat"] = mc.GetAttribute(Attribute.STR) + "_" + mc.GetAttribute(Attribute.VIT) + "_" + mc.GetAttribute(Attribute.INT) + "_" + mc.GetAttribute(Attribute.AGI) + "_" + mc.GetAttribute(Attribute.LUCK);
            _AnalyticQuery["party_" + index + "_equipment"] = mc.Equipment().Name;
            _AnalyticQuery["party_" + index + "_health"] = mc.Health.Value + "/" + mc.Health.MaxValue;
            _AnalyticQuery["party_" + index + "_misstimedhit"] = charList[i].MissTimeHit;
            _AnalyticQuery["party_" + index + "_goodtimedhit"] = charList[i].GoodTimeHit;
            _AnalyticQuery["party_" + index + "_misstimedhit"] = charList[i].PrefectTimeHit;
        }
        Analytics.CustomEvent("winbattle", _AnalyticQuery);

        phase = BattlePhase.ResultScreen;
        StartCoroutine(resultScreenHandler());
    }

    private IEnumerator resultScreenHandler()
    {
        ResultScreen.ResultScreenStart(charList, battle.reserveList);
        while (!ResultScreen.IsAnimationFinished())
        {
            if (InputManager.GetButtonDown("Confirm"))
            {
                ResultScreen.ForceFinish();
            }
            yield return null;
        }

        StartCoroutine(ResultScreenIdle());
    }

    private IEnumerator ResultScreenIdle()
    {
        bool changeState = false;

        while (!changeState)
        {
            yield return null;
            if (InputManager.GetButtonDown("Confirm"))
            {
                if (ResultScreen.IsAnyCharacterLeveledUp())
                {
                    //open panel character level up
                    ResultScreen.LevelUpScreenStart();
                    //then wait till all character finish leveling up
                    while (ResultScreen.IsAnyCharacterLeveledUp())
                    {

                        yield return null;
                    }
                }
                
                BattleFader.OnFadeOut += BattleEnd;
                BattleFader.EndScene();
                changeState = true;
            }            
        }
    }

    private IEnumerator gameOverScreenHandler()
    {
        bool changeState = false;

        while (!changeState)
        {
            if (InputManager.GetButtonUp("Confirm"))
            {
                TransToTittle();
                changeState = true;
            }
            yield return null;
        }
    }
    private void BattleEnd()
    {
        if (BattleFader.OnFadeOut != null)
            BattleFader.OnFadeOut -= BattleEnd;
        onPrep = false;
        timedHitListener.stopListener();

        foreach (Battler mainBat in charList.Concat(battle.reserveList))
        {
            mainBat.inReserve = false;
            mainBat.onBattle = false;
            mainBat.RemoveController();
        }
        //remove this object
        for(int i = 0; i<PartyManager.Instance.CharacterParty.Count;i++) //each (MainCharacter MC in PartyManager.Instance.CharacterParty)
        {
            MainCharacter MC = PartyManager.Instance.CharacterParty[i];
            //Revive yang mati
            MC.Health.Revive(1);

            Battler cBattler = LegrandUtility.GetComponentInChildren<Battler>(MC.gameObject);
            BattlerActionController cBattlerActionController = LegrandUtility.GetComponentInChildren<BattlerActionController>(MC.gameObject);
            BattlerStateController cBattlerStateController = LegrandUtility.GetComponentInChildren<BattlerStateController>(MC.gameObject);

            if (cBattler != null) Destroy(cBattler);
            if (cBattlerActionController != null) Destroy(cBattlerActionController);
            if (cBattlerStateController != null) Destroy(cBattlerStateController);
            MC.gameObject.SetActive(false);
            MC.transform.SetParent(PartyManager.Instance.transform);
        }
        //backtoworld
        //SoundManager.StopBackgroundMusic(true);
        WwiseManager.Instance.StopBG();

        turnController.onInited -= turnControllerInited;
        turnController.onDone -= DoneInitSpeed;
        //set false status bar

        if (transData.AfterFinishedGoTo == GameplayType.World)
        {
            if (transData.WorldDialogName != "" || transData.WorldDestination != "")
                WorldSceneManager.TransToWorld(transData);
            else
                WorldSceneManager.TransToWorld(null);
        }
        else if (transData.AfterFinishedGoTo == GameplayType.Sequence)
            WorldSceneManager.TransNonWorldToSequence(transData.SequenceName);
        else if (transData.AfterFinishedGoTo == GameplayType.UnityScene)
            EventManager.Instance.TriggerEvent(new BattleEndEvent());
        
        EventManager.Instance.RemoveListener<BattlerBuffEvent>(HandleBattlerBuffEvent);

        //        battle.ResultScreen.DestroyBattleAPS();
    }
    #endregion

    private void HandleMoveBattler(MoveBattlerEvent m)
    {
        for (int i = 0; i < battlerList.Count; i++) //  each (Battler battler in battlerList)
            battlerList[i].transform.position += m.Distance;
    }

    private void HandleOnDialogFinish(DialogWindowClosed e)
    {
        onDialog = false;
        onInput = true;
        EventManager.Instance.RemoveListener<DialogWindowClosed>(HandleOnDialogFinish);
    }

    private void HandleBattlerBuffEvent(BattlerBuffEvent e)
    {
        List<Battler> allBattler = battlerList.Concat(Battle.Instance.reserveList).ToList();
        List<Battler> foundBattler = new List<Battler>(); ;

        for (int i = 0; i < e.CharacterNameBuffed.Length; i++)
            foundBattler.AddRange(allBattler.FindAll(battler => battler.name == e.CharacterNameBuffed[i]));

        for (int i = 0; i < foundBattler.Count; i++)
        {
            for (int j = 0; j < e.BattlerBuff.Length; j++)
            {
                Buff buff = Buff.Copy(e.BattlerBuff[j]);
                ItemController._Instance.SendTargetBuffDebuff(foundBattler[i], buff);
            }
        }
    }

    private void CheckDialogQuery(ResponsiveQuery q)
    {
        for (int i = 0; ListOfFact != null && i < ListOfFact.Count; i++)
        {
            q.Add(ListOfFact[i].Name, ListOfFact[i].Value);
        }
        q.Add("Concept", LegrandUtility.GetUniqueSymbol("BattleDialog"));

        if (q.Match())
        {
            onDialog = true;
            EventManager.Instance.AddListener<DialogWindowClosed>(HandleOnDialogFinish);

            RuleResult result = DynamicSystemController._Instance.RuleDB.GetLatestResult();
            LegrandUtility.StartDialogue(result.ResponseName);
            //if (ListOfFact == null)
            //    ListOfFact = new List<Fact>();


            //ListOfFact.AddRange(result.Facts);
            //DynamicSystemController._Instance.ResponseDB.ExecuteResponse(result.ResponseName);

        }
        else
            onInput = true;

    }
}