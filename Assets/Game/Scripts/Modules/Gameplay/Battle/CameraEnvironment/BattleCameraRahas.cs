﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Legrand.core;
using UnityStandardAssets.ImageEffects;
using System.Reflection;

public class BattleCameraRahas : BattleCameraController {

	private Vector3 _DOFOriPos;
	public Transform DOFCenter;

	// Use this for initialization
	public override void initCamera (){
		_DOFOriPos = DOFCenter.position;
	}
    public override void resetBlinkCamera (){
		DOFCenter.position = _DOFOriPos;

		_ThisCamera.fieldOfView = 30;
        ResetUICamera();
		KillTween ();
		setLocation = false;
		targetPos = null;
		changePerspective = false;
		firstSelect = true;
		//		_SelectTweenMove = camTrans.DOMove (oriPos, 0f).OnComplete (CorrectLookAt);
		//		_SelectTweenRot = camTrans.DORotate (oriRot.eulerAngles, 0f, RotateMode.Fast);
		camTrans.transform.position = oriPos;
		if (isMassive)
			camTrans.LookAt (MassiveCenter);
		else
			camTrans.LookAt (NormalCenter);
//		CorrectLookAt ();
	}
    public override void OnCommandCharacter(Battler battler)
    {
        _SecondCamera.SetActive(true);
        battler.transform.eulerAngles = battler.MyPlane.transform.eulerAngles;
        Battle.Instance.encounterList.ForEach(x=>x.ActivateBattler(true));
//        Battle.Instance.charList.ForEach(x=>x.transform.DORotate(new Vector3(0,0,0),1f,RotateMode.Fast));
        float xMod = (battler.BattlePosition.x - 1) * -0.75f;
        firstSelect = true;
        DOFCenter.position = battler.HitRange.position;
        _Camera2.fieldOfView = 50;

        _CamFader.TransToClear(0, 0);
        _SecondCameraController.KillTween(); 

        if (battler.BattlePosition.x == 1)
            xMod = 0.75f;

        if (battler.Character._ID != "5")
            _SecondCamera.transform.position = new Vector3(battler.transform.position.x+xMod, 1.3f, battler.transform.position.z-1.7f);
        else
            _SecondCamera.transform.position = new Vector3(battler.transform.position.x+xMod, 1.3f, battler.transform.position.z-2.5f);
        
        _SecondCamera.SetActive(true);
        _SecondCameraController.StartTweenView();

        if (!isFirst)
        {
            isFirst = true;
            _CamFader.TransToTexture(0, 0.5f);
            StartCoroutine (ChangeCamera(true, 0.5f));
        }
        else
        {
            _CamFader.TransToTexture(0, 0);
            StartCoroutine (ChangeCamera(true, 0f));
        }

    }
	public override void StartPreview(){
		//pindah fokus dof sementara (karena blur effect)

		List <Battler> battlers = Battle.Instance.Controller.EncounterFirstMeet;
        battlers [0].BattlerState = BattlerState.Preview;
        DOFCenter.position = battlers[0].CameraPointingLocation.position;
		Vector3 newPos = new Vector3 ();

		if (isMassive)
			newPos = new Vector3 (battlers [0].CameraPointingLocation.position.x, battlers [0].CameraPointingLocation.position.y, battlers [0].CameraPointingLocation.position.z - 3);
		else
			newPos = new Vector3 (battlers [0].CameraPointingLocation.position.x, battlers [0].CameraPointingLocation.position.y+2.5f, battlers [0].CameraPointingLocation.position.z - 3);
		_SecondCamera.transform.position = newPos;
		_CamFader.TransToTexture (0, 0.5f);
//		StartCoroutine(DoFade(newPos, battlers [0]));
		previewLookAt = true;
		_SecondCamera.transform.DOMove(new Vector3(newPos.x, newPos.y-1, newPos.z-2.5f),1.5f).OnComplete(()=>RemoveBattlers(battlers[0]));
	}
    public override void pointerView (Battler target, bool isSingle){
        StopAllCoroutines();
        StartCoroutine (ChangeCamera(false, 0f));
        DOFCenter.position = target.HitArea.position;
        if (isMassive)
            _ThisCamera.fieldOfView = 40;
        else
            _ThisCamera.fieldOfView = 40;

        ResetUICamera();

        if (!setLocation) {
            currentPos = camTrans.transform.position;
            currentRot = camTrans.transform.rotation;
            setLocation = true;
        }
        isIdle = false;
        changePerspective = false;
        KillTween ();
        // Kamera dipindahkan ke posisi akhir kemudian dicatat posisi dan rotasinya => dibalikin ke posisi awal => dottween ke posisi dan rotasi yang dicatat
        if (isSingle){
            Encounter enc = (target.Character as Encounter);
            Vector3 currentRotation = camTrans.transform.eulerAngles;
            Vector3 currentPosition = camTrans.transform.position;
            Transform targetFocusTransform = target.CameraPointingLocation;

            float xMod = 0;//(target.BattlePosition.x - 1) * -3f;//LegrandUtility.Random(0f, -5f);
            float zMod = target.BattlePosition.y - 4.5f;//((target.BattlePosition.y - 1) * 2.5f) - 2.5f;

            Vector3 newPos = new Vector3 (targetFocusTransform.transform.position.x + xMod, 1.75f, targetFocusTransform.transform.position.z + zMod);
            camTrans.transform.position = newPos;
            camTrans.transform.LookAt (targetFocusTransform.transform);

            Vector3 targetRotation = camTrans.transform.eulerAngles;

            if (isMassive)
            {
                newPos = new Vector3(targetFocusTransform.transform.position.x + xMod, 2.8f, -7f);
                targetRotation = new Vector3(355, 0, 0);
            }

            camTrans.transform.eulerAngles = currentRotation;
            camTrans.transform.position = currentPosition;
            if (firstSelect){
                _SelectTweenMove = camTrans.DOMove (newPos, 0f);
                _SelectTweenRot = camTrans.DORotate (targetRotation, 0f, RotateMode.Fast);
            }
            else{
                _SelectTweenMove = camTrans.DOMove (newPos, 0.5f).SetEase(Ease.OutQuad).SetUpdate(true);
                _SelectTweenRot = camTrans.DORotate (targetRotation, 0.5f, RotateMode.Fast).SetUpdate(true);
            }
        } else {
            Encounter enc = (target.Character as Encounter);
            Vector3 currentRotation = camTrans.transform.eulerAngles;//camTrans.transform.eulerAngles;
            Vector3 currentPosition = camTrans.transform.position;

            if (enc.MonsterSize == EncounterSize.Massive){
                Vector3 newPos = new Vector3 (0, 2.8f, -7);
                Vector3 targetRotation = new Vector3(355,0,0);

                if (firstSelect){
                    _SelectTweenMove = camTrans.DOMove (newPos, 0f);
                    _SelectTweenRot = camTrans.DORotate (targetRotation, 0f, RotateMode.Fast);
                }
                else{
                    _SelectTweenMove = camTrans.DOMove (newPos, 0.5f).SetEase (Ease.OutQuad).SetUpdate(true);
                    _SelectTweenRot = camTrans.DORotate (targetRotation, 0.5f, RotateMode.Fast).SetUpdate(true);
                }
            }
            else{
                Vector3 newPos = new Vector3 (0, target.transform.lossyScale.y+2f, target.transform.localPosition.z-9f);
                Vector3 targetRotation = new Vector3(13,0,0);
                if (firstSelect){
                    _SelectTweenMove = camTrans.DOMove (newPos, 0f);
                    _SelectTweenRot = camTrans.DORotate (targetRotation, 0f, RotateMode.Fast);
                }
                else{
                    _SelectTweenMove = camTrans.DOMove (newPos, 0.5f).SetEase (Ease.OutQuad).SetUpdate(true);
                    _SelectTweenRot = camTrans.DORotate (targetRotation, 0.5f, RotateMode.Fast).SetUpdate(true);
                }
            }
        }
        if (isSelected)
            firstSelect = false;

        _CamFader.TransToClear(0, 0);
        _SecondCameraController.KillTween();    
    }

    public override void resetDOF(){
        DOFCenter.position = _DOFOriPos;
    }

    public override Transform GetDOF(){
        return DOFCenter;
    }
}
