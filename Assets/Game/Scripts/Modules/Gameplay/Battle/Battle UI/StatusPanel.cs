﻿using UnityEngine;
using System.Collections;

public class StatusPanel : MonoBehaviour {

    public GameObject StatusBarGroup;
    public GameObject ReserveBarGroup;
    public GameObject TimeHitGroup;
    public ActionPopUp ActionBar;
    public GameOverBar GameOverScreen;
    public GameObject FleeUI;
}
