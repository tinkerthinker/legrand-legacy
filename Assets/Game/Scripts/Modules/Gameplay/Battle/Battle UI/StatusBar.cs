﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;
using Legrand.core;

public class StatusBar : MonoBehaviour {
    public Battler battler;
	public Image HPBar;
	public Image GaugeBar;
	public Image Avatar;
	public Image BgActive;
	public Text HPText;
    public Text HPShadow;
    public Text APText;
    public BuffBar BuffBar;
    public Image ElementCharacter;
    public Image APFull;

	private float _HpMax;

    private Tween _SelectedTween;
    private Tween _APFullTween;
    private bool OnSelected = false;

    public void initStatusBar(MainCharacter mainChar, bool isReserve, BuffBar buffBar){
        
        float currentHP = Mathf.FloorToInt(mainChar.Health.Value); 
        float maxHP = Mathf.FloorToInt(mainChar.Health.MaxValue);
        int currentGauge =  mainChar.Gauge;
        Sprite characterAvatar = mainChar.Avatar[PartyManager.Instance.StoryProgression.PartyModels[int.Parse(mainChar._ID)]];

        HPText.text = Mathf.Round(currentHP).ToString();
		//HPShadow.text = Mathf.Round(currentHP).ToString();

        BuffBar = buffBar;
		_HpMax = maxHP;
		HPBar.fillAmount = (currentHP / maxHP);
		GaugeBar.fillAmount = (currentGauge / 100f);
        if (!isReserve)
        {
            battler = mainChar.GetComponent<Battler>();
            APText.text = currentGauge.ToString()+"/100";
            UpdateGauge(currentGauge, 100);
            BgActive.color = Color.clear;
            ElementCharacter.sprite = LegrandBackend.Instance.GetSpriteData("Element" +mainChar.CharacterElements.Elements[0].ToString());
        }
        
        Avatar.sprite = characterAvatar;

        CheckBuffBar();
	}
	public void UpdateHP(float current, float max){
		if (current > max)
			current = max;
		float percent = (current / max);
		if(HPBar.fillAmount > percent)
			this.transform.DOPunchScale (new Vector3 (0.2f, 0.2f), 0.1f, 2).SetAutoKill(true).SetUpdate(true);

		HPBar.DOFillAmount (percent, 2f).SetUpdate (true).OnUpdate(UpdateText);
	}
    public void UpdateAP (){
        APText.text = Mathf.Round(GaugeBar.fillAmount * 100).ToString()+"/100";
    }
    public void ResetScaleBar(){
        this.transform.localScale = new Vector3(1, 1, 1);
    }
	void UpdateText()
	{
        HPText.text = Mathf.Round(HPBar.fillAmount * _HpMax).ToString();
		//HPShadow.text = HPText.text;
	}
    void CheckBuffBar ()
    {
        if (battler != null && !battler.inReserve)
        {
            battler.CheckBuffBlink();
        }
    }
	public void UpdateGauge(int current, int max){
		if (current > max)
			current = max;
		float percent = ((float)current / (float)max);
        GaugeBar.DOFillAmount (percent, 0.25f).SetUpdate (true).OnUpdate(UpdateAP);

        if (_APFullTween != null)
            _APFullTween.Kill ();
        if (current >= 100)
        {
            _APFullTween = APFull.DOColor(new Color(1, 1, 0.6f, 1f), 0.75f).SetUpdate(true).SetLoops(-1, LoopType.Yoyo);
        }
        else
            APFull.DOColor(new Color(1, 1, 1, 0), 0f).SetUpdate(true);
	}
	public void PredictCost(float current, float max){
		float percent = (current / max);
		HPBar.DOFillAmount (percent, 0.5f).SetUpdate (true);
	}
	public void UnPredictCost(float current, float max){
		float percent = (current / max);
		HPBar.DOFillAmount (percent, 0.5f).SetUpdate (true);
	}
	public void selected(){

		if (_SelectedTween != null)
			_SelectedTween.Kill ();
		BgActive.color = Color.white;
		//_SelectedTween = BgActive.DOFade (0.5f, 0.7f).SetUpdate(true).SetLoops(-1, LoopType.Yoyo);
        _SelectedTween = BgActive.DOColor (new Color (0.39f,0.34f,0.32f,1) , 0.7f).SetUpdate(true).SetLoops(-1, LoopType.Yoyo);

        if (!OnSelected)
        {
            battler.OnSelected();
            OnSelected = true;
        }
	}

    public void reserveSelect(){
        if (_SelectedTween != null)
            _SelectedTween.Kill ();
        //BgActive.color = Color.white;
        //_SelectedTween = BgActive.DOFade (0.5f, 0.7f).SetUpdate(true).SetLoops(-1, LoopType.Yoyo);
        _SelectedTween = BgActive.DOColor (new Color (1f,1f,1f,0.5f) , 0.7f).SetUpdate(true).SetLoops(-1, LoopType.Yoyo);
    }
	public void unSelected(){
		_SelectedTween.Pause ();
		BgActive.DOFade (0f,0.1f);

        if (OnSelected)
        {
            battler.StopShaderSequence();
            OnSelected = false;
        }
	}

    public void unSelectedReserve(){
        _SelectedTween.Kill (true);
        BgActive.DOColor(new Color(1f, 1f, 1f, 1f), 0.1f);
    }
}
