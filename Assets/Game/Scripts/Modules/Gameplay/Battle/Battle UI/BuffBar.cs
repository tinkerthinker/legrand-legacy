﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using DG.Tweening;

public class BuffBar : MonoBehaviour {
    public GameObject BuffImagePrefab;
    [SerializeField]
    public List<BuffImage> BuffImageList;

    public Image BuffImage;
//    public Text NameBuff;
    public Image TextImage;
    void Start()
    {
        BuffImageList = new List<BuffImage>();
    }

    public void AddBuff(string searchingKey, Sprite sprite, string point)
    {
        BuffImage buffImage = new BuffImage(searchingKey, sprite, point);
//        buffImage.Sprite.DOColor(Color.white, 1f);
        BuffImageList.Add(buffImage);
    }

    public void ReplaceBuffSprite(string searchingKey, Sprite sprite)
    {
        for (int i = 0; i < BuffImageList.Count; i++)
        {
            if (LegrandUtility.CompareString(BuffImageList[i].SourceKey, searchingKey))
            {
                BuffImageList[i].Sprite = sprite;
                return;
            }
        }
    }

    public void RemoveBuff(string Key)
    {
        string searchingKey = Key.Trim(new char[2] { '-', '+' });
        
        for (int i = 0; i < BuffImageList.Count; i++)
        {
            if (LegrandUtility.CompareString(BuffImageList[i].SourceKey, searchingKey))
            {
                BuffImage buffImage = BuffImageList[i];
                BuffImageList.Remove(buffImage);
                return;
            }
        }
    }

    public void OnDisable (){
        
    }


}

[System.Serializable]
public class BuffImage
{
    public string SourceKey;
    public Sprite Sprite;
    public string Point;

    public BuffImage(string sourceKey, Sprite sprite, string point)
    {
        SourceKey = sourceKey;
        Sprite = sprite;
        Point = point;
    }
}
