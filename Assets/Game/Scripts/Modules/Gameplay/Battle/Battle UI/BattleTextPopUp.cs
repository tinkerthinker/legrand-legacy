﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class BattleTextPopUp : MonoBehaviour {
    public enum BattleTextType
    {
        Player,
        Enemy,
        Heal,
        Poison,
        Buff,
        Debuff
    }

    [SerializeField]
    private List<Color> PopUpColors;

    [SerializeField]
    private Text TextComponent;

    void OnDisable()
    {
    }

    public void SetPopUp(string text, BattlerType battlerType, bool isCritical, Transform targetTransform)
    {
        int type = 0;
        if (battlerType == BattlerType.Player) type = (int)BattleTextType.Player;
        else type = (int)BattleTextType.Enemy;

        SetPopUp(text, type, isCritical, targetTransform);
    }

    public void SetPopUp(string text, int type, bool isCritical, Transform targetTransform)
    {
        TextComponent.text = text;
        TextComponent.color = PopUpColors[type];
        if(isCritical)
            GetComponent<Animator>().CrossFade("Critical", 0f);
        else
            GetComponent<Animator>().CrossFade("Normal", 0f);
        StartCoroutine(FollowTarget(targetTransform));
    }


    IEnumerator FollowTarget(Transform target)
    {
        while(true)
        {
            if (target != null)
                this.transform.position = new Vector3(target.position.x, this.transform.position.y, target.position.z);
            yield return null;
        }
    }
}

public class BattleTextPopUpData
{
    public int ClassType = 0;

    public string Text;
    public int Type;
    public bool Crit;
    public BattlerType BatterType;

    public BattleTextPopUpData(string text, int type, bool crit)
    {
        Text = text;
        Type = type;
        Crit = crit;
        ClassType = 0;
    }

    public BattleTextPopUpData(string text, BattlerType type, bool crit)
    {
        Text = text;
        BatterType = type;
        Crit = crit;

        ClassType = 1;
    }
}