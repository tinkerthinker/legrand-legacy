﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;
using Legrand.core;

public class ActionPopUp : MonoBehaviour {
	public Text Name;


	public void ShowActionBar (string nameAction){
		this.gameObject.SetActive (true);
		Name.text = nameAction;
	}

	public void DestroyActionBar(){
		gameObject.SetActive (false);
	}
}
