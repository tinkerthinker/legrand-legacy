﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;

public class BattleParticleEffect
{

	private static string[] CastingMagicName = {"FireCasting","EarthCasting","WaterCasting","LightingCasting","OrbCasting","DarkCasting","TornadoCasting"};
	private static string[] MeleeMagicName = {"FireMagic","EarthMagic","WaterMagic","LightingMagic","LightMagic","DarkMagic","AirMagic"};
	private static string[] RangeMagicName = {"FireArrow","EarthArrow","WaterArrow","LightingArrow","LightArrow","DarkArrow","AirArrow"};

	public void GetMagicPooling (Magic magicChar)
	{
		for (int i = 0; i < LegrandBackend.Instance.LegrandDatabase.BattleEffectData.Count; i++) {
			if (LegrandBackend.Instance.LegrandDatabase.BattleEffectData.Get (i).Name.Equals (magicChar.Name)) {

			}
		}
//		LegrandBackend.Instance.LegrandDatabase.BattleEffectData.Get;
	}

	public static string GenerateMeleeMagicCharacterEffect(Element element, TargetType targetMagic)
	{
		string magicName;
		switch (element) {
		case Element.Fire:
			return magicName = MeleeMagicName[0]+GenerateMagicByTarget(targetMagic);
		case Element.Earth:
			return magicName = MeleeMagicName[1]+GenerateMagicByTarget(targetMagic);
		case Element.Water:
			return magicName = MeleeMagicName[2]+GenerateMagicByTarget(targetMagic);
		case Element.Lighting:
			return magicName = MeleeMagicName[3]+GenerateMagicByTarget(targetMagic);
		case Element.Light:
			return magicName = MeleeMagicName[4]+GenerateMagicByTarget(targetMagic);
		case Element.Dark:
			return magicName = MeleeMagicName[5]+GenerateMagicByTarget(targetMagic);
		case Element.Air:
			return magicName = MeleeMagicName[6]+GenerateMagicByTarget(targetMagic);
		default:
			return "Ruin";
		}
	}

	static string GenerateMagicByTarget(TargetType targetMagic)
	{
		if (targetMagic == TargetType.Single) {
			return "";
		} else {
			return "";
		}
	}

	public static string GenerateRangeMagicEffect(Element element, TargetType targetMagic)
	{
		string magicName;
		switch (element) {
		case Element.Fire:
			return magicName = RangeMagicName[0]+GenerateMagicByTarget(targetMagic);
		case Element.Earth:
			return magicName = RangeMagicName[1]+GenerateMagicByTarget(targetMagic);
		case Element.Water:
			return magicName = RangeMagicName[2]+GenerateMagicByTarget(targetMagic);
		case Element.Lighting:
			return magicName = RangeMagicName[3]+GenerateMagicByTarget(targetMagic);
		case Element.Light:
			return magicName = RangeMagicName[4]+GenerateMagicByTarget(targetMagic);
		case Element.Dark:
			return magicName = RangeMagicName[5]+GenerateMagicByTarget(targetMagic);
		case Element.Air:
			return magicName = RangeMagicName[6]+GenerateMagicByTarget(targetMagic);
		default:
			return "Ruin";
		}
	}

	public static string GenerateMeleeCastingMagicCharacterEffect(Element element, TargetType targetMagic)
	{
		string magicName;
		switch (element) {
		case Element.Fire:
			return magicName = CastingMagicName[0];
		case Element.Earth:
			return magicName = CastingMagicName[1];
		case Element.Water:
			return magicName = CastingMagicName[2];
		case Element.Lighting:
			return magicName = CastingMagicName[3];
		case Element.Light:
			return magicName = CastingMagicName[4];
		case Element.Dark:
			return magicName = CastingMagicName[5];
		case Element.Air:
			return magicName = CastingMagicName[6];
		default:
			return "Ruin";
		}
	}
}
