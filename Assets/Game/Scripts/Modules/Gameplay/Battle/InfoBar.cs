﻿using UnityEngine;
using UnityEngine.UI;
using Legrand.core;
using System.Collections;
using System.Collections.Generic;

public class InfoBar : MonoBehaviour {
	public Outline Outline;
	public Text Name;
	public Image Element;
    public Image WeaponType;

    public List<PercentageColor> PercentColors;
    public float timeToDestroy = 2f;

//    public List<BuffImage> BuffImageList;
//    public Image BuffImages;
//    public Text BuffName;
    public GameObject MagicStrongInfo;
    public GameObject MagicWeaknessInfo;
    public GameObject WeaponStrongInfo;
    public GameObject WeaponWeaknessInfo;

    private float _HpMax;
    public BuffBar BuffArea;
	void OnEnable (){
//		StartCoroutine (DestroyAps ());
	}

    public void Set(Battler target, Battler attacker,bool isPhysical, bool isMagic, Element magicElement)
    {
        //set character data

        //set encounter data
        Name.text = (target.Character).name;
        string element = (target.Character as Encounter).EncounterElements.Elements[0].ToString();
		Element.sprite = LegrandBackend.Instance.GetSpriteData("Element"+element);

        WeaponType.sprite = LegrandBackend.Instance.GetSpriteData("Weapon" + (target.Character as Encounter).BaseWeapon.Type.ToString());

        bool found = false;
        for(int i = 0; i < PercentColors.Count && found == false; i++)
        {
            if((target.Health.Value/target.Health.MaxValue) <= PercentColors[i].Percentage)
            {
                Outline.effectColor = PercentColors[i].Color;
                found = true;
            }
        }

//        int j = 0;
//        if (target.BuffDebuffSprite != null)
//        {
//            foreach (KeyValuePair<string, Sprite> keyValue in target.BuffDebuffSprite)
//            {
//                if (j == BuffImageList.Count) 
//                    break;
//
//                BuffImageList[j].Sprite = keyValue.Value;
//                j++;
//            }
//        }

        MagicStrongInfo.SetActive(false);
        MagicWeaknessInfo.SetActive(false);
        WeaponStrongInfo.SetActive(false);
        WeaponWeaknessInfo.SetActive(false);

        if (isMagic)
            SetMagicWeakness(magicElement, target);
        else if (isPhysical)
           SetPhysicalWeakness(target, attacker);
    }

    public void SetMagicWeakness(Element magicElement, Battler target)
    {
        int elementPower = Formula.CompareElement(magicElement, target.Character);
        if(elementPower == 1)
            MagicStrongInfo.SetActive(true);
        else if (elementPower == -1)
            MagicWeaknessInfo.SetActive(true);
    }

    public void SetPhysicalWeakness(Battler target, Battler attacker)
    {
        int weaponPower = Formula.WeaponTriangle(attacker.Character, target.Character);
        if (weaponPower == 1)
            WeaponStrongInfo.SetActive(true);
        else if (weaponPower == -1)
            WeaponWeaknessInfo.SetActive(true);
    }

    private IEnumerator DestroyAps (){
		yield return new WaitForSeconds(timeToDestroy);
		this.gameObject.DestroyBattleAPS ();
	}

    [System.Serializable]
    public class PercentageColor
    {
        public float Percentage;
        public Color Color;
    }
}
