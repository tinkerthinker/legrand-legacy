﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class BattleCameraEditorUpdate : MonoBehaviour {

	public Transform look;
	// Update is called once per frame
	void Update () {
		this.transform.LookAt(look);
	}
}
