﻿using UnityEngine;
using System.Collections;

public class BattlePlane : MonoBehaviour {
	[System.Serializable]
	public enum PlayerType{
		Player,
		Enemy,
	};
	public PlayerType playerType;

	public bool placeAble = true;
	public Vector2 planePos;

	public MeshRenderer mesh;

	void Start(){
		mesh = GetComponent<MeshRenderer> ();
	}
}
