﻿using UnityEngine;
using System.Collections;

public class MainPanelDesc : MonoBehaviour {
	public int Index;
	public string Name;
	public string Description;

    public Transform SelectedParents;
}
