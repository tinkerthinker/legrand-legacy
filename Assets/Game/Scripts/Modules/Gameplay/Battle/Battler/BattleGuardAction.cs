﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;

public class BattleGuardAction {

    public void ExecGuardAction(Battler battler, Buff buff){
        switch (battler.Character._Name)
        {
            case "Finn":
                //counter reflect damage x 3
                battler.ReflectPercentage = buff.value;
                battler.PopUp(buff.Name, (int)BattleTextPopUp.BattleTextType.Buff, false);
                break;
            case "Eris":
                //heal allies 5% damage 
                break;
            case "Kael":
                //chance 50% evasion 100%
                break;
            case "Azzam":
                //increase sp 5 allies
                break;
            case "Scatia":
                //counter enemy allies 100%
                break;
        }
    }
}
