﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Legrand.core;
using System.Linq;

public class RangeAttack : BaseAction
{
    Battler targetBattler;
    Transform trans;
    Vector3 oriPos;
    Vector3 oriRot;
    private bool onStartAttack;
    private BattlerState _AttackType;
    private string NameProjectile;
    private GameObject hitEffect;
    private List<Battler> targets;
    private List<Battler> TempTarget;

    private Animator anim;
    public override void initAction(BattleCommand com, float speed, Vector3 oriPos, Quaternion oriRot)
    {
        base.initAction(com, speed, oriPos, oriRot);
        battler = com.attacker;
        DestroyListener();
        targetBattler = com.target[0];
        targets = com.target;
        this.oriPos = oriPos;
        this.oriRot = battler.transform.eulerAngles;

        trans = battler.transform;
        onStartAttack = false;
        anim = battler.GetComponent<Animator>();
        TempTarget = new List<Battler>();

        CheckTarget();
        onUpdate = true;
        onDone = false;
        battler.StateController.timeTransition = 0.5f;
        //		if (battler.BattlerType == BattlerType.Player){
        //			if (battler.AttackType == AttackType.Range) {
        //				NameProjectile = "Arrow2";
        //				speed = 80f;
        //			} else {
        //				NameProjectile = "Orb";
        //				speed = 30f;
        //			}
        //		}else {
        //			Encounter enc = (battler.Character as Encounter);
        //			if (enc.Projectile == ProjectileType.None)
        //				NameProjectile = "Arrow2";
        //			else
        //				NameProjectile = enc.Projectile.ToString ();
        //			speed = 40f;
        //		}
    }
    private void CheckTarget()
    {
        if (!battler.IsEnemyDeadAll())
        {
            if (targets.TrueForAll(x => x.BattlerState != BattlerState.Dead))
            {
                for (int i = 0; i < targets.Count; i++)
                {
                    if (targets[i].BattlerState != BattlerState.Dead)
                        targetBattler = targets[i];
                }
            }
            else
            {
                targetBattler = FindAnothertarget();
                targets = new List<Battler>();
                command.target = new List<Battler>();
                targets.Add(targetBattler);
                command.target.Add(targetBattler);
            }

            if (command.attacker.BattlerType == BattlerType.Player && targets.Any(x => x.BattlerType != BattlerType.Player))
            {
                EventManager.Instance.TriggerEvent(new TimedHitEvent(command.attacker, targets, command));
            }
        }
        else
        {
            onDone = true;
        }
    }
    void DestroyListener()
    {
        if (battler.StateController.IsOnAttackEvent())
            battler.StateController.ClearOnAttackEvent();

        if (battler.StateController.IsOnLastAttackEvent())
            battler.StateController.ClearOnLastAttackEvent();
    }

    void aimTarget()
    {
        if (!onExecuted)
        {
            onExecuted = true;
            
            if (targetBattler.BattlerState != BattlerState.Dead)
            {
                trans.LookAt(targetBattler.transform);
                if (battler.TimedHit != null)
                {
                    if (battler.TimedHit.AttackHitState == TimedHitState.Perfect || battler.TimedHit.AttackHitState == TimedHitState.Good)
                    {
                        _AttackType = BattlerState.Combo;
                        if (battler.BattlerState != BattlerState.Hit)
                            battler.BattlerState = BattlerState.Combo;
                    }
                    else
                    {
                        _AttackType = BattlerState.Attack;
                        if (battler.BattlerState != BattlerState.Hit)
                            battler.BattlerState = BattlerState.Attack;
                    }
                }
                else
                {
                    _AttackType = BattlerState.Attack;

//                    EventManager.Instance.TriggerEvent(new TimedDefEvent(battler, targetBattler));
//                    for (int i = 0; i < targets.Count; i++)
//                    {
//                        if (targets[i].TimedHit != null) {
//                            targets[i].TimedHit.DefendHitState = targetBattler.TimedHit.DefendHitState;
//                        }
//                    }

                    if (battler.BattlerState != BattlerState.Hit)
                        battler.BattlerState = BattlerState.Attack;
                }
                battler.StateController.OnAttack += HandleOnAttack;
                battler.StateController.OnLastAttack += HandleOnLastAttack;
            }
            else
            {
                onDone = true;
            }
        }

    }



    void HandleOnAttack(string nameParticle)
    {
        string gParticle = GetParticleName(nameParticle);
        if (gParticle == "")
            HandleProjectileOnHit(null);
        else
        {
            int x = 0;
            for (int i = 0; i < targets.Count; i++)
            {
                string particleName = GetParticleName(nameParticle);
                GameObject p = BattlePoolingSystem.Instance.InstantiateAPS(particleName);//GameObject gBattleParticle = GameObject.Instantiate<GameObject>(gParticle);
                p.transform.position = battler.HitRange.position;
                BattleParticle bp = p.GetComponent<BattleParticle>();

                if (x == 0)
                {
                    bp.ClearOnParticle();

                    bp.OnParticleDone += HandleProjectileOnHit;
                    x++;
                }

                bp.Initiate(battler.HitRange.gameObject, targets[i].HitRange.gameObject);
            }
        }
    }
    void HandleOnLastAttack(string nameParticle)
    {
        string gParticle = GetParticleName(nameParticle);
        if (gParticle == "")
        {
            HandleLastProjectileOnHit(null);
            for (int i = 0; i < targets.Count; i++)
                targets[i].Targeted.Remove(battler);
        }
        else
        {
            int x = 0;
            for (int i = 0; i < targets.Count; i++)
            {

                string particleName = GetParticleName(nameParticle);
                GameObject p = BattlePoolingSystem.Instance.InstantiateAPS(particleName);//GameObject gBattleParticle = GameObject.Instantiate<GameObject>(gParticle);
                p.transform.position = battler.HitRange.position;
                BattleParticle bp = p.GetComponent<BattleParticle>();

                if (x == 0)
                {
                    bp.ClearOnParticle();

                    bp.OnParticleDone += HandleLastProjectileOnHit;
                    x++;
                }

                bp.Initiate(battler.HitRange.gameObject, targets[i].HitRange.gameObject);
            }
        }

//        battler.BattlerState = BattlerState.Idle;
    }
    void HandleProjectileOnHit(BattleParticle bp)
    {
        if (bp != null)
            bp.OnParticleDone -= HandleProjectileOnHit;
        foreach (Battler b in targets)
        {
            if (b.BattlerState != BattlerState.Dead)
            {
                InstantiateAPS(b);
                b.HitState();
            }
        }
        WwiseManager.Instance.PlaySFX(WwiseManager.BattleFX.Blast_Hit);
    }
    void HandleLastProjectileOnHit(BattleParticle bp)
    {
        if (bp != null)
            bp.OnParticleDone -= HandleLastProjectileOnHit;

        foreach (Battler b in targets)
        {
            if (b.BattlerState != BattlerState.Dead)
            {
                InstantiateAPS(b);
                //                b.HitState();
                battler.Attack(b, battler);
                if (b.Targeted.Contains(battler))
                    b.Targeted.Remove(battler);
            }
        }

        onDone = true;
        WwiseManager.Instance.PlaySFX(WwiseManager.BattleFX.Blast_Hit);
        //targetBattler.Targeted.Remove (battler);
    }

    void InstantiateAPS(Battler b)
    {
        hitEffect = BattlePoolingSystem.Instance.InstantiateAPS(battler.GetParticleHit(), new Vector3(b.HitRange.position.x, b.HitRange.position.y, b.HitRange.position.z), new Quaternion(0, 0, 0, 0));
    }

//    void FindAnothertarget()
//    {
//        List<Battler> listTarget = new List<Battler>();
//        if (battler.BattlerType == BattlerType.Player)
//            listTarget = Battle.Instance.encounterList;
//        else
//            listTarget = Battle.Instance.charList;
//        for (int i = 0; i < listTarget.Count; i++)
//        {
//            if (listTarget[i].BattlerState != BattlerState.Dead)
//                TempTarget.Add(listTarget[i]);
//        }
//    }
    private Battler FindAnothertarget()
    {
        List<Battler> listTarget = new List<Battler>();
        if (battler.BattlerType == BattlerType.Player)
            listTarget = Battle.Instance.encounterList;
        else
            listTarget = Battle.Instance.charList;

        for (int i = 0; i < listTarget.Count; i++)
        {
            if (listTarget[i].BattlerState != BattlerState.Dead)
                TempTarget.Add(listTarget[i]);
        }
        if (TempTarget.Count > 0)
            return TempTarget[0];
        else
            return null;
    }
    public override IEnumerator Update()
    {
        while (onUpdate)
        {
            if (!onDone)
            {
                if (targets.TrueForAll(x => x.BattlerState == BattlerState.Dead) && battler.BattlerState != BattlerState.Attack && battler.BattlerState != BattlerState.Combo)
                {
                    onDone = true;
                }
                else if (battler.BattlerState == BattlerState.Dead)
                {
                    onDone = true;
                }
                else if (!onExecuted)
                {
                    yield return new WaitForSeconds(0.5f);
                    aimTarget();
                }
                else if ((onExecuted && !onStartAttack) || (onStartAttack && !onDone))
                {
                    yield return new WaitForSeconds(0.5f);
                    if (_AttackType == BattlerState.Attack)
                    {
                        battler.BattlerState = BattlerState.Attack;
                    }
                    else if (_AttackType == BattlerState.Combo)
                    {
                        battler.BattlerState = BattlerState.Combo;
                    }
                    yield return null;
                }
            }
            else
            {
                sendDoneAct(true);
                break;
            }
            yield return null;
        }
        DestroyListener();
        //		battler.transform.DORotate (oriRot, 0.5f, RotateMode.Fast);
        //		battler.transform.eulerAngles = oriRot;
        sendDoneAct(true);
    }

    private string GetParticleName(string particleData)
    {
        string[] particleName = particleData.Split('/');
        return particleName[1];
    }
}
