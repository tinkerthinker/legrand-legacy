﻿using UnityEngine;
using System.Collections;
[System.Serializable]
public class BaseAction{
	protected BattleCommand command;
	protected Battler battler;
                                                                                                                                                                                                                                                                                                                                                                                                                        
	protected bool onUpdate = false;
	protected bool onExecuted = false;
	protected bool onDone = false;
	private bool _doneEventSended = false;

	public delegate void OnDoneAct (bool resetState);
	public event OnDoneAct onDoneAct;

    public virtual void initAction(BattleCommand com){
        onExecuted = false;
        command = com;
        battler = com.attacker;
		onUpdate = true;
		onDone = false;
		_doneEventSended = false;
	}
    public virtual void initAction(BattleCommand com, float speed){
        onExecuted = false;
        command = com;
        battler = com.attacker;
        onUpdate = true;
		onDone = false;
		_doneEventSended = false;
	}
    public virtual void initAction(BattleCommand com, float speed, Vector3 oriPos, Quaternion oriRot){
        onExecuted = false;
        command = com;
        battler = com.attacker;
		onUpdate = true;
		onDone = false;
		_doneEventSended = false;
	}
    public virtual void initAction(Battler reviver){
        onExecuted = false;
		onUpdate = true;
		onDone = false;
		_doneEventSended = false;
	}
	public virtual IEnumerator Update(){
		yield return null;
	}
	protected void sendDoneAct(bool resetState){
		if(!_doneEventSended)
		{
			_doneEventSended = true;
			if(onDoneAct != null)
				onDoneAct(resetState);
		}
	}
}
