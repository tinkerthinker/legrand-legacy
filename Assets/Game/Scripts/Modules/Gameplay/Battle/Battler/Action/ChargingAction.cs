﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;

public class ChargingAction : BaseAction {

    private Encounter encounter;
    private GameObject magicCast;

    public override void initAction (BattleCommand com)
    {
        base.initAction (com);
        battler = command.attacker;
    }
    private void chargeAction (){
        if (!onExecuted)
        {
            onExecuted = true;

            string MagicEffect = (battler.Character as Encounter).EncounterElements.Elements[0].ToString() + "Casting";
            Vector3 position = new Vector3(battler.transform.position.x, battler.transform.position.y, battler.transform.position.z);
            if (MagicEffect == "" || MagicEffect == "Ruin" || MagicEffect == "Casting")
                MagicEffect = "OrbCasting";

            magicCast = BattlePoolingSystem.Instance.InstantiateAPS(MagicEffect, position, new Quaternion()) as GameObject;
            battler.PopUp("CHARGING", 1, false);

            float healing = battler.Health.MaxValue*((float)2/100f);
            battler.Health.Increase(battler.Health.MaxValue*((float)2/100f));
            battler.PopUp(Mathf.RoundToInt(healing).ToString(), (int)BattleTextPopUp.BattleTextType.Heal);
        }
    }
    private void DoneCharge(){
        magicCast.DestroyBattleAPS();
    }
    public override IEnumerator Update ()
    {
        while (onUpdate) {
            if (!onDone) {
                if (!onExecuted) {
                    //                  yield return new WaitForSeconds (3f);
                    chargeAction ();
                    yield return new WaitForSeconds (2f);
                    onDone = true;
                }
            } else {
                yield return new WaitForSeconds (2f);
                DoneCharge();
                sendDoneAct (true);
            }
            yield return null;
        }
    }
}
