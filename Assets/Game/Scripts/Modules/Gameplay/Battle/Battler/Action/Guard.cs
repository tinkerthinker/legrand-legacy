﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;

public class Guard : BaseAction
{
    private MainCharacter _MainChar;
    private GuardBuff _GuardBuff;
    private BattleGuardAction _BattleGuard = new BattleGuardAction();
    public override void initAction(BattleCommand com)
    {
        _GuardBuff = null;
        command = com;
        base.initAction(com);
        battler = com.attacker;

        if (battler.BattlerType == BattlerType.Player)
        {
            _MainChar = (battler.Character as MainCharacter);

            if (_MainChar != null && _MainChar.GuardSkill != null)
                _GuardBuff = _MainChar.GuardSkill;
        }
    }

    private void guard()
    {
        if (!onExecuted)
        {
            if (command.attacker.BattlerType == BattlerType.Player                                                                                                                                                                          )
            {
                EventManager.Instance.TriggerEvent(new TimedDefEvent(command.attacker, command.attacker));
            }
            onExecuted = true;
            int fillGauge = 0;
            if (battler.BattlerType == BattlerType.Player)
            {
                fillGauge = fillGauge + (battler.Character as MainCharacter).GetGaugeSpeed(GaugeType.Guard);
                battler.ChangeGauge(fillGauge);
            }
            battler.BattlerState = BattlerState.Guard;

            if (battler.BattlerType == BattlerType.Player)
            {
                if (_GuardBuff != null && LegrandUtility.Random(0, 100) < _GuardBuff.BuffPercentage)
                {
                    battler.ParticleGuard = BattlePoolingSystem.Instance.InstantiateAPS("FinnGuard");
                    battler.ParticleGuard.transform.position = new Vector3(battler.transform.position.x, battler.transform.position.y + 0.5f, battler.transform.position.z);
                    GuardAction();
                }
            }
        }
    }
    private void GuardAction ()
    {
        if (_GuardBuff != null)
        {
            ItemController._Instance.SetTargets(battler, _GuardBuff.Target, _GuardBuff.TargetFormations);

            for (int i = 0; i < _GuardBuff.Buffs.Count ; i++)
            {
                Buff buff = Buff.Copy(_GuardBuff.Buffs[i]);
                ItemController._Instance.SendTargetBuffDebuff(battler, buff);
            }
        }
    }

    public override IEnumerator Update()
    {
        while (onUpdate)
        {
            if (onExecuted)
            {
                yield return new WaitForSeconds(0.5f);
                sendDoneAct(false);
                yield break;
            }
            else
            {
                guard();
            }
            yield return null;
        }
    }
}
