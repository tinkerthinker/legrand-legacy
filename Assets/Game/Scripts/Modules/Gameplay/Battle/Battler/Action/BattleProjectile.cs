﻿using UnityEngine;
using System.Collections;

public class BattleProjectile : MonoBehaviour {
	public Transform target;
	public float speed;

	bool shoot = false;
	Transform trans;

	public delegate void OnDone(BattleProjectile bp);
	public event OnDone onDone;
	// Use this for initialization
	void Awake () {
		trans = transform;
	}
	public void setTarget(Transform t){

		target = t;
		shoot = true;
	}
	private void shooting(){
		float distance = Vector3.Distance(trans.position, target.position);

		if (distance <= 0.5f) {
			shoot = false;

			if (onDone != null) {
				onDone (this);
				this.enabled = false;
			}
		} else {
			trans.position = Vector3.MoveTowards(trans.position, target.position, Time.smoothDeltaTime * speed);
		}
	}
	// Update is called once per frame
	void Update () {
		if (shoot) {
			shooting();
		}
	}
}
