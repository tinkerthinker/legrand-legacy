﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using DG.Tweening;

public class Flee : BaseAction {
    private List<Battler> battlerFleeList;
    private BattleController battleController;
    private Image FailUI;
    private Text TextUI;
	public override void initAction (BattleCommand com)
	{
		base.initAction (com);
		battler = com.attacker;
        battleController = Battle.Instance.Controller;
        battlerFleeList = new List<Battler>();

        for (int i = 0; i < battleController.CommandList.Count; i++)
        {
            BattleCommand bc = battleController.CommandList[i];
            if (bc.attacker.BattlerType == BattlerType.Player && bc.commandData.CommandType == (int)CommandTypes.Flee && bc.attacker != battler)
                battlerFleeList.Add(bc.attacker);
        }
        FailUI = Battle.Instance.StatusPanelGroup.FleeUI.GetComponentInChildren<Image>();
        TextUI = Battle.Instance.StatusPanelGroup.FleeUI.GetComponentInChildren<Text>();
	}
	
    void FailToFlee(){
        if (!onExecuted)
        {
            onExecuted = true;
            if (battlerFleeList.Count == 0)
            { //Battle.Instance.StatusPanelGroup.FleeUI.SetActive(true);
                FailUI.DOColor(new Color(1, 1, 1, 1), 0.5f).OnComplete(FalseUI);
                TextUI.DOColor(new Color(0, 0, 0, 1), 0.5f);
            }
        }
    }

    void FalseUI(){
        FailUI.DOColor(new Color(1, 1, 1, 0), 0.5f).SetDelay(1f);
        TextUI.DOColor (new Color(1, 1, 1, 0), 0.5f).SetDelay(1f);
    }

	public override IEnumerator Update ()
	{
        if (LegrandUtility.Random(0,100) < battler.FleeChance && battler.BattlerState != BattlerState.Dead) {
            if (!battler.isFlee){
                yield return new WaitForSeconds (0.5f);
				Debug.Log (battler+" isFlee");
				EventManager.Instance.TriggerEvent(new FleeEvent());
				yield return new WaitForSeconds (0.25f);
                battleController.fleePrompt ();
			}else{
				Debug.Log("Other Player is Flee");
			}
		}
		else{
			Debug.Log (battler+" can't Flee");
            FailToFlee();
            sendDoneAct(true);
		}
	}
}
