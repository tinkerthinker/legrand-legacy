﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;

public class EncounterSkillAction : BaseAction {

	private List<Battler> targets;

    private ScreenFader fader;

	private GameObject hitEffect;
    private OffensiveBattleSkill _Skill;

    private BattleCameraController _CameraAnim;
    private Transform _SkillCamera;
    private Camera _camera;
    private GameObject gSkill;
    private BattlerSkillController encSkill;
    private Battler targetBattler;
    GameObject particle;
    GameObject effectParticle;

    bool skillDone = false;

	public override void initAction (BattleCommand com)
	{
		base.initAction (com);
        battler = com.attacker;
        DestroyListener();
		targets = com.target;
		onExecuted = false;
		onDone = false;
		DestroyListener ();
        _Skill = battler.Character.GetOffensiveSkill(com.commandData.CommandValue);
        fader = Battle.Instance.ScreenFader;
        skillDone = false;

        _CameraAnim = Battle.Instance.BattleCameraController;
        _SkillCamera = _CameraAnim.SkillCamera;
        _camera = _CameraAnim.GetComponent<Camera> ();
        _camera.fieldOfView = 28;
        for (int i = 0; i < targets.Count; i++)
        {
            if (targets[i].BattlerState != BattlerState.Dead)
            {
                targetBattler = targets[i];
                i = targets.Count;
            }
                
        }
        battler.StateController.timeTransition = 0f;
    }

    private void castSkill (){
        if (!onExecuted) {
            onExecuted = true;
            List<GameObject> templist = new List<GameObject>();
            bool OnCastSkill = false;
            //          battler.SkillController.initSkill(_Targets, Battle.Instance.ScreenFader);
            //          battler.SkillController.OnSkillDone += HandleOnSkillDone;
            for (int i = 0; i < targets.Count; i++){
                Battler bat = targets[i];
                if (bat.BattlerState != BattlerState.Dead && targets.Count > 0 && !OnCastSkill) {
                    OnCastSkill = true;
                    fader.OnFadeOut += StartSkill;
                    fader.FadeSpeed = 0.1f;
                    fader.EndScene ();
//                    StartSkill();

                } 
            }

            for (int i = 0; battler.WeaponFX.Count > 0 && i < battler.WeaponFX.Count; i++)
            {
                battler.WeaponFX[i].ActiveParticle();
            }

            if (targets.TrueForAll (x=>x.BattlerState == BattlerState.Dead)){
                onDone = true;
            }

        }
    }
    private void initEncounterSkill(){
        
    }
    private void StartSkill (){
        fader.OnFadeOut -= StartSkill;
//        fader.StartScene ();

        //      _CameraAnim.SetDOF (32f);
//        battler.StateController.OnAttack += HandleOnAttack;
//        battler.StateController.OnLastAttack += HandleOnLastAttack;
        battler.StateController.OnDoneSkill += HandleOnSkillDone;
        battler.StateController.OnTimeHit += HandleOnTimeDef;

        battler.transform.position = new Vector3(0,0,0);
        battler.transform.eulerAngles = new Vector3(0,180,0);


        gSkill = GameObject.Instantiate<GameObject>(Resources.Load<GameObject>("Battle/SkillBoss/"+_Skill.Name));
        gSkill.name = "LimitBreak";
        gSkill.transform.position = new Vector3 (0, 0, 0);
        encSkill = gSkill.GetComponent<BattlerSkillController>();//<BattleEncounterSkillControl>();
        encSkill.init (_Skill);
        fader.transform.parent.parent = null; // SetParent(encSkill.transform);


        _CameraAnim.onSkill = true;
        _SkillCamera.gameObject.SetActive (true);
        _CameraAnim.MainCamera.enabled = false;
        foreach (Battler bat in targets) {
            if (bat.BattlerState != BattlerState.Dead)
                encSkill.ChangeLayer (bat, "LimitBreak");
        }

        encSkill.ChangeLayer (battler, "LimitBreak");
//        battler.BattlerState = BattlerState.Skill;

        encSkill.PlayCameraAnimation(battler, targets);
    }
    void HandleOnTimeDef(){
        battler.StateController.OnTimeHit -= HandleOnTimeDef;
        if (targetBattler != null && targetBattler.TimedHit != null)
        {
            EventManager.Instance.TriggerEvent(new TimedDefEvent(command.attacker, targetBattler));

            for (int i = 0; i < targets.Count; i++)
                targets[i].TimedHit.DefendHitState = targetBattler.TimedHit.DefendHitState;
        }
        
        battler.ActionBar.ShowActionBar (_Skill.Name);
    }
    void HandleOnEffect(string nameParticle)
    {
        string[] particles = nameParticle.Split('/');
        effectParticle = BattlePoolingSystem.Instance.InstantiateAPS(particles[1]);//GameObject.Instantiate<GameObject>(Resources.Load<GameObject>("Battle/Battle Particles Resources/" + nameParticle));
        effectParticle.transform.position = new Vector3(battler.transform.position.x, battler.transform.position.y, battler.transform.position.z);
    }
    void HandleOnAttack(string nameParticle)
    {
        string[] particles = nameParticle.Split('/');
        for (int i=0;i<targets.Count;i++)// each (Battler b in targets)
        {
            if (nameParticle != "")
            {
                particle = BattlePoolingSystem.Instance.InstantiateAPS(particles[1]);//GameObject.Instantiate<GameObject>(Resources.Load<GameObject>("Battle/Battle Particles Resources/" + nameParticle));
                particle.transform.position = new Vector3(targets[i].transform.position.x, targets[i].transform.position.y+1f, targets[i].transform.position.z);
                HitTarget(targets[i]);
            }
        }
    }
    public void HandleOnLastAttack(string nameParticle){
        string[] particles = nameParticle.Split('/');
        if (nameParticle != "")
        {
            particle = BattlePoolingSystem.Instance.InstantiateAPS(particles[1]);//GameObject.Instantiate<GameObject>(Resources.Load<GameObject>("Battle/Battle Particles Resources/" + nameParticle));
            particle.transform.position = new Vector3(targets[0].transform.position.x, targets[0].transform.position.y, targets[0].transform.position.z);
            HitTarget(targets[0]);
        }
    }
    void HandleOnSkillDone ()
    {
        battler.StateController.OnDoneSkill -= HandleOnSkillDone;
        onFadeComplete();
    }

    void onFadeComplete(){
        fader.transform.parent.SetParent(Battle.Instance.transform);
        encSkill.ChangeLayer (battler, "Default");
        battler.transform.SetParent(Battle.Instance.Controller.transform);
        battler.ActivateBattler(true);
        battler.transform.eulerAngles = battler.MyPlane.transform.eulerAngles;
        battler.BattlerState = BattlerState.Idle;

        _SkillCamera.transform.SetParent (_CameraAnim.camTrans);
        _CameraAnim.onSkill = false;
        _CameraAnim.MainCamera.enabled = true;
        _SkillCamera.gameObject.SetActive (false);

        _CameraAnim.ResetUICamera();
        for (int i =0; i<targets.Count;i++)
        {
            targets[i].transform.SetParent(Battle.Instance.Controller.transform);
            targets[i].transform.position = targets[i].MyPlane.transform.position;
            targets[i].transform.eulerAngles = targets[i].MyPlane.transform.eulerAngles;
            encSkill.ChangeLayer (targets[i], "Default");
            battler.SkillOffenseAttack (targets[i], command.commandData.CommandValue);
        }

        for (int i = 0; battler.WeaponFX.Count > 0 && i < battler.WeaponFX.Count; i++)
        {
            battler.WeaponFX[i].DeactiveParticle();
        }

        battler.StateController.timeTransition = 0f;
        onDone = true;
        fader.StartScene ();
        
        skillDone = true;
        GameObject.Destroy(gSkill, 0.5f);
    }

    void InstantiateAPS (Battler target){
        BattlePoolingSystem.Instance.InstantiateAPS (battler.GetParticleHit(), new Vector3 (target.transform.position.x, target.transform.position.y + 1.5f, target.transform.position.z), new Quaternion (0, 0, 0, 0));
    }

	void DestroyListener (){
        if (battler.StateController.IsOnAttackEvent())
            battler.StateController.ClearOnAttackEvent();

        if (battler.StateController.IsOnLastAttackEvent())
            battler.StateController.ClearOnLastAttackEvent();

        if (battler.StateController.IsDoneAttackEvent())
            battler.StateController.ClearOnDoneAttackEvent();
        
        if (battler.StateController.IsOnEffectEvent())
            battler.StateController.ClearEffectEvent();
    }
	public override IEnumerator Update ()
	{
		while (onUpdate) {
			if(!onDone){
                castSkill();

                if (skillDone)
                {
                    yield return new WaitForSeconds(0.25f);
                    battler.BattlerState = BattlerState.Idle;
                }
            }else{
                yield return new WaitForSeconds(0.5f);
                sendDoneAct (true);
				yield break;
			}
			yield return null;
		}
		DestroyListener ();
	}
    void HitTarget (Battler target){
        if (target.BattlerState != BattlerState.Guard)
            target.BattlerState = BattlerState.Hit;
    }
}
