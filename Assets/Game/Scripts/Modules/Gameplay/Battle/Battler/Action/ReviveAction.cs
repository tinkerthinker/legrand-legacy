﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;
using System.Linq;


public class ReviveAction : BaseAction {

	Battler battler;
	Transform trans;
	Transform target;
	BattlePlane targetPlane;
	Quaternion oriRot;
	float speed;

	public override void initAction (BattleCommand com, float speed)
	{
		base.initAction (com, speed);
		battler = com.target[0];
		this.speed = speed;
		this.oriRot = oriRot;
		trans = com.target[0].transform;
		targetPlane = battler.MyPlane;
		target = targetPlane.transform;
		onDone = false;

	}
	private void Move ()
	{
        float distance = Vector3.Distance (battler.transform.position, target.position);
		
		if (distance < 0.25f) {
			battler.BattlePosition = targetPlane.planePos;
			
			trans.position = target.position;
			trans.rotation = oriRot;
			onDone = true;
			battler.BattlerState = BattlerState.Idle;
        } else {
            
            if(battler.BattlerState != BattlerState.Move){
                battler.BattlerState = BattlerState.Move;
            }
            trans.position = Vector3.MoveTowards (battler.transform.position, target.position, Time.deltaTime * speed);
			trans.LookAt (target);
		}
	}
//	private BattlePlane filterPos(){
//		List<BattlePlane> targetPlanes = battleData.playerPlanes.Where(x => x.placeAble == true).ToList();
//		int rand = Random.Range(0,targetPlanes.Count-1);
//		return targetPlanes[0];
//	}

	public override IEnumerator Update ()
	{
		while (onUpdate) {
			if (!onDone){
				Move();
			}
			else{
				yield return new WaitForSeconds(0.5f);
				sendDoneAct(true);
			}
			yield return null;
		}
	}
}
