﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SkillCombo : BaseAction {
	private List<Battler> _Targets;
	private bool onDone;
	private ScreenFader fader;
	private Vector3 targetOriPos;
	private BattleCameraController _CameraAnim;
	private Transform _SkillCamera;
	private Camera _camera;
	public Battler batPartner;
	private BattleSkillCombination _SkillCombo;



	public override void initAction (BattleCommand com)
	{
		base.initAction (com);
		battler = command.attacker;
		onDone = false;
		_Targets = command.target;
		fader = Battle.Instance.ScreenFader;
		targetOriPos = _Targets [0].transform.position;
		_CameraAnim = Battle.Instance.BattleCameraController;
		_SkillCamera = _CameraAnim.SkillCamera;
		_camera = _CameraAnim.GetComponent<Camera> ();
		batPartner = com.batPartner;
		battler.StateController.BattlerCombo = batPartner;
		batPartner.StateController.BattlerCombo = battler;
		_SkillCombo = battler.SkillCombination.Find (x => x.IdPartner.Equals(batPartner.Character._ID));
	}
	private void castSkill (){
		if (!onExecuted) {
			onExecuted = true;
			List<GameObject> templist = new List<GameObject>();
			if (_Targets [0].BattlerState != BattlerState.Dead && _Targets.Count > 0 && batPartner != null && _SkillCombo != null) {
				fader.OnFadeOut += StartSkill;
				fader.FadeSpeed = 0.2f;
				fader.EndScene ();
			} else {
				onDone = true;
			}
		}
	}
	private void StartSkill (){
		fader.OnFadeOut -= StartSkill;
		fader.StartScene ();
		battler.StateController.OnAttack += HandleOnAttack;
		battler.StateController.OnDoneSkill += HandleOnSkillDone;
		battler.transform.position = battler.SkillController.transform.position;
		batPartner.transform.position = battler.SkillController.transform.position;

//		_camera.cullingMask ^= (1 << LayerMask.NameToLayer ("Default"));
		_SkillCombo.ChangeLayer (_Targets [0], "LimitBreak");
		_SkillCombo.ChangeLayer (battler, "LimitBreak");
		_SkillCombo.ChangeLayer (batPartner, "LimitBreak");

		ActiveWeaponParticle ();
		_SkillCamera.gameObject.SetActive (true);
		_SkillCombo.PlayCameraAnimation (battler, _Targets [0]);
		battler.BattlerState = BattlerState.Combination;
		batPartner.BattlerState = BattlerState.Combination;
	}

	void HandleOnSkillDone ()
	{
		battler.StateController.OnAttack -= HandleOnAttack;
		battler.StateController.OnDoneSkill -= HandleOnSkillDone;
		fader.OnFadeOut += onFadeComplete;
		fader.FadeSpeed = 0.2f;
		fader.EndScene ();
		//		battler.SkillController.OnSkillDone -= HandleOnSkillDone;
		//		battler.SkillAttack(_Targets);
	}
	void onFadeComplete(){
		battler.transform.position = battler.MyPlane.transform.position; //mengembalikan posisi character ke tempat asal
		batPartner.transform.position = batPartner.MyPlane.transform.position; //mengembalikan posisi character ke tempat asal
		_SkillCamera.transform.SetParent (_CameraAnim.camTrans); //mengembalikan letak parent kamera
		_Targets [0].transform.position = targetOriPos; //mengembalikan posisi target ke tempat semula
		DisableWeaponParticle();
//		_camera.cullingMask ^= (1 << LayerMask.NameToLayer ("Default")); //culling mask default aktif

		//set layer dari limitbreak ke default
		_SkillCombo.ChangeLayer (_Targets [0], "Default");
		_SkillCombo.ChangeLayer (battler, "Default");
		_SkillCombo.ChangeLayer (batPartner, "Default");

		battler.Anim.CrossFade ("BATTLESTANCE", 0);
		batPartner.Anim.CrossFade ("BATTLESTANCE", 0);

		fader.OnFadeOut -= onFadeComplete;
		onDone = true;
		fader.StartScene ();
		battler.Attack (_Targets[0], battler);
		//		battler.SkillAttack (_Targets);
		_SkillCamera.gameObject.SetActive (false);
	}

    void HandleOnAttack (string nameParticle){
		foreach (Battler b in _Targets) {
            b.HitState ();
		}
		InstantiateAPS ();
	}
	void InstantiateAPS (){
        BattlePoolingSystem.Instance.InstantiateAPS (battler.GetParticleHit(), new Vector3 (_Targets[0].transform.position.x, _Targets[0].transform.position.y + 1.5f, _Targets[0].transform.position.z), new Quaternion (0, 0, 0, 0));
	}

	public override IEnumerator Update ()
	{
		while (onUpdate) {
			castSkill();
			if (onDone){
				yield return new WaitForSeconds(1f);
				sendDoneAct(true);
			}

			yield return null;
		}
	}
	void ActiveWeaponParticle (){
        for (int i = 0; battler.WeaponFX.Count > 0 && i < battler.WeaponFX.Count; i++)
        {
            battler.WeaponFX[i].ActiveParticle();
        }
        for (int i = 0; batPartner.WeaponFX.Count > 0 && i < batPartner.WeaponFX.Count; i++)
        {
            batPartner.WeaponFX[i].ActiveParticle();
        }
	}
	void DisableWeaponParticle (){
        for (int i = 0; battler.WeaponFX.Count > 0 && i < battler.WeaponFX.Count; i++)
        {
            battler.WeaponFX[i].DeactiveParticle();
        }
        for (int i = 0; batPartner.WeaponFX.Count > 0 && i < batPartner.WeaponFX.Count; i++)
        {
            batPartner.WeaponFX[i].DeactiveParticle();
        }
	}
}
