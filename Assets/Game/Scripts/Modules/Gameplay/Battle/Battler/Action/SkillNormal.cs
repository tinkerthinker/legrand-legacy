﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;

public class SkillNormal : BaseAction {
    List<Battler> targets; // target sebelum ada kondisi2 yang ada
    private Battler battlerTarget; //target yang ada di reserve diganti sesuai battle id
    int index = 0; //variable to check iteration
    private Battler TargetBattler;
    private NormalSkill skill;
    private List <Battler> FixTarget; // target setelah ada beberapa kondisi yang paling mungkin diserang
    List<Buff> normalSkillBuffs;

    public override void initAction (BattleCommand com)
    {
        index = 0;
        base.initAction (com);
        battler = command.attacker;
        targets = command.target;
        TargetBattler = targets[0];
        skill = (battler.Character as MainCharacter).GetNormalSkill(command.commandData.CommandValue);
        FixTarget = new List<Battler>();
        normalSkillBuffs = new List<Buff>();
    }
    private void useSkill (){
        if (!onExecuted)
        {
            onExecuted = true;
            for (int x=0;x<skill.BuffsChance.Count;x++){
                normalSkillBuffs.Add(Buff.Copy(skill.BuffsChance[x].BuffEffect));
            }
            GameObject pNewParticle = Resources.Load<GameObject>("Battle/Skill/NormalSkill/"+skill.NormalSkillParticle);
            //must check item type
            for (int i = 0; i < targets.Count; i++)
            {// each(Battler target in targets){
                if (!targets[i].inReserve)
                    battlerTarget = targets[i];
                else
                    battlerTarget = Battle.Instance.charList[targets[i].BattleID];

                SendParticle(pNewParticle);
                FixTarget.Add(battlerTarget);

                for (int x=0;x<normalSkillBuffs.Count;x++)
                    ItemController._Instance.SendTargetBuffDebuff(battlerTarget, normalSkillBuffs[x]);
//                ItemController._Instance.SendTargetBuffDebuff(battlerTarget, normalSkillBuffs);
            }

            if (pNewParticle == null)
                particleDone(null);
        }

        battler.BattlerState = BattlerState.UseItem;
    }

    void SendParticle (GameObject pNewParticle)
    {
        if (pNewParticle != null)
        {
            GameObject newParticle = GameObject.Instantiate<GameObject>(pNewParticle);
            BattleParticle battleParticle = newParticle.GetComponent<BattleParticle>();
            if (index == 0)
            {
                if (battleParticle != null)
                    battleParticle.OnParticleDone -= particleDone;

                battleParticle.OnParticleDone += particleDone;
                index++;
            }
            battleParticle.Initiate(battler.gameObject, battlerTarget.gameObject);
        }
    }
    void particleDone(BattleParticle particle)
    {
        battler.UseItem(command.commandData.CommandValue, FixTarget);

//        for (int i = 0; i < targets.Count;i++) //each (Battler b in targets)
//        {
//            Battler b = targets[i];
//            if (b.BattlerState == BattlerState.Dead && !b.Health.IsDead)
//            {
//                b.SetLockedState(false);
//                b.BattlerState = BattlerState.Revived;
//            }
//        }

        onDone = true;
    }

    public override IEnumerator Update ()
    {
        while (onUpdate) {
            if (!onDone) {
                if (!onExecuted) {
//                  yield return new WaitForSeconds (3f);
                    useSkill();
                }
            } else {
                yield return new WaitForSeconds (0.5f);
                sendDoneAct (true);
            }
            yield return null;
        }
    }
}
