﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;
using System.Linq;

public class MeleeMagic : BaseAction
{
    private Battler targetBattler;
    private float speed;
    private Transform battlerTrans;

    private Magic magic;
    private List<Battler> targets = new List<Battler>(); //default targets
    private List<Battler> TempTarget; //targets possible temp

    private GameObject hitEffect;

    private int index = 0;

    public override void initAction(BattleCommand com, float speed)
    {
        index = 0;
        base.initAction(com);
        this.speed = speed;
        command = com;
        battlerTrans = com.attacker.transform;
        battler = com.attacker;
        DestroyListener();
        targets = com.target;
        TempTarget = new List<Battler>();

        onDone = false;
        onExecuted = false;

        if (command.magicCasted != null)
            command.magicCasted.DestroyBattleAPS();

        if (!battler.IsEnemyDeadAll())
        {
            if (targets.Any(x => x.BattlerState != BattlerState.Dead))
            {
                for (int i = 0; i < targets.Count; i++)
                {
                    if (targets[i].BattlerState != BattlerState.Dead)
                        targetBattler = targets[i];
                }
            }
            else
            {
                targetBattler = FindAnothertarget();
                targets = new List<Battler>();
                command.target = new List<Battler>();
                targets.Add(targetBattler);
                command.target.Add(targetBattler);
            }


//            if (command.target.Any(x => x.BattlerType != BattlerType.Player))
//            {
//                EventManager.Instance.TriggerEvent(new TimedHitEvent(command.attacker, command.target, command));
//                battler.ActionBar.ShowActionBar(command.commandData.CommandName);
//            }
        }
        else
        {
            onDone = true;
        }
    }

    void SetNewTarget()
    {

    }
    void DestroyListener()
    {
        if (battler.StateController.IsOnAttackEvent())
            battler.StateController.ClearOnAttackEvent();

        if (battler.StateController.IsOnLastAttackEvent())
            battler.StateController.ClearOnLastAttackEvent();

        if (battler.StateController.IsDoneAttackEvent())
            battler.StateController.ClearOnDoneAttackEvent();
    }

    private void moveToTarget()
    {
        Vector3 tempDefenderHitArea = new Vector3(targetBattler.HitRange.position.x, targetBattler.transform.position.y, targetBattler.HitRange.position.z);
        battlerTrans.LookAt(tempDefenderHitArea);
        Vector3 tempAttackerHitRange = new Vector3(battler.HitRange.position.x, battler.transform.position.y, battler.HitRange.position.z);
        float distance = Vector3.Distance(tempAttackerHitRange, tempDefenderHitArea);

        if (distance < 0.5f)
        {
            attack();
        }
        else
        {
            if (battler.BattlerState != BattlerState.Move)
                battler.BattlerState = BattlerState.Move;

            Vector3 newLocation = Vector3.MoveTowards(tempAttackerHitRange, tempDefenderHitArea, Time.deltaTime * speed);
            Vector3 diff = newLocation - tempAttackerHitRange;

            battlerTrans.position = battlerTrans.position + diff;
        }
    }



    private void attack()
    {
        if (!onExecuted)
        {
            onExecuted = true;
            battlerTrans.LookAt(targetBattler.transform);

//            if (battler.BattlerType == BattlerType.Enemy && targetBattler.TimedHit != null)
//            {
//                EventManager.Instance.TriggerEvent(new TimedDefEvent(battler, targetBattler));
//
//                for (int i = 0; i < targets.Count; i++)
//                    targets[i].TimedHit.DefendHitState = targetBattler.TimedHit.DefendHitState;
//                
//                battler.ActionBar.ShowActionBar(command.commandData.CommandName);
//            }
            battler.BattlerState = BattlerState.Magic;

            battler.StateController.OnAttack += HandleOnAttack;
            battler.StateController.OnLastAttack += HandleOnLastAttack;
            battler.StateController.OnDoneAttack += HandleOnDoneAttack;
        }
    }

    void HandleOnAttack(string nameParticle)
    {
    }

    void HandleOnLastAttack(string nameParticle)
    {
        string gParticle = projectile(nameParticle);
        if (gParticle == "")
            onDone = true;
        else
        {
            for (int i = 0; i < targets.Count; i++)//each (Battler b in targets)
            {
                if (targets[i].BattlerState != BattlerState.Dead)
                {
                    //GameObject gBattleParticle = GameObject.Instantiate<GameObject>(gParticle);
                    GameObject gBattleParticle = BattlePoolingSystem.Instance.InstantiateAPS(gParticle);
                    gBattleParticle.transform.position = targets[i].HitArea.position;
                    BattleParticle bp = gBattleParticle.GetComponent<BattleParticle>();
                    if (index == 0)
                    {
                        if (bp.OnParticleDone != null)
                            bp.OnParticleDone -= HandleOnLastMagicHit;

                        bp.OnParticleDone += HandleOnLastMagicHit;
                        index++;
                    }
                    bp.Initiate(battler.HitRange.gameObject, targets[i].HitArea.gameObject);
                    targets[i].Targeted.Remove(battler);

                    WwiseManager.Instance.PlaySFX(WwiseManager.BattleFX.Blast_Hit);
                }
            }

        }
    }
    void HandleOnLastMagicHit(BattleParticle bp)
    {
        battler.MagicAttack(command.commandData.CommandValue, targets);

        if (bp.OnParticleDone != null)
            bp.OnParticleDone -= HandleOnLastMagicHit;


    }
    void HandleOnDoneAttack()
    {
        for (int i = 0; battler.WeaponFX.Count > 0 && i < battler.WeaponFX.Count; i++)
        {
            battler.WeaponFX[i].DeactiveParticle();
        }
        battler.StateController.OnAttack -= HandleOnAttack;
        battler.StateController.OnLastAttack -= HandleOnLastAttack;
        battler.StateController.OnDoneAttack -= HandleOnDoneAttack;
        onDone = true;
    }

    private Battler FindAnothertarget()
    {
        List<Battler> listTarget = new List<Battler>();
        if (battler.BattlerType == BattlerType.Player)
            listTarget = Battle.Instance.encounterList;
        else
            listTarget = Battle.Instance.charList;

        for (int i = 0; i < listTarget.Count; i++)
        {
            if (listTarget[i].BattlerState != BattlerState.Dead)
                TempTarget.Add(listTarget[i]);
        }
        if (TempTarget.Count > 0)
            return TempTarget[0];
        else
            return null;
    }
    void InstantiateAPS(Battler b)
    {
        hitEffect = BattlePoolingSystem.Instance.InstantiateAPS(battler.GetParticleHit(), new Vector3(b.HitRange.transform.position.x, b.HitRange.transform.position.y, b.HitRange.transform.position.z), new Quaternion(0, 0, 0, 0));
    }
    public override IEnumerator Update()
    {
        while (onUpdate)
        {
            if (!onDone)
            {
                if (command.target.TrueForAll(x => x.BattlerState == BattlerState.Dead))
                {
                    sendDoneAct(true);
                }
                else if (!onExecuted && battler.BattlerState != BattlerState.Hit)
                {
                    moveToTarget();
                }
            }
            else
            {
                //yield return new WaitForSeconds(0.5f);
                sendDoneAct(true);
                yield break;
            }
            yield return null;
        }
        DestroyListener();
    }


    public string projectile(string nameParticle)
    {
        string[] p = nameParticle.Split('/');
        //GameObject p = Resources.Load<GameObject>("Battle/Battle Particles Resources/" + nameParticle);
        return p [1];
    }
}
