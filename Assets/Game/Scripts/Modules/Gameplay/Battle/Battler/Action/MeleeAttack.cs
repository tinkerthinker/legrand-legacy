using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;
using System.Linq;

public class MeleeAttack : BaseAction{
	private Battler targetBattler;

	private bool onClash;
	private bool onStartAttack;
	private bool targetedByTarget;
	private float range = 0.5f;
	private float speed;
	private Transform battlerTrans;
	private bool _Interupted;
	private GameObject hitEffect;
	private List<Battler> targets;

    private List <Battler> TempTarget;


	public override void initAction (BattleCommand com, float speed)
    {
		base.initAction (com);
        this.speed = speed;
		targetedByTarget = false;
        battlerTrans = command.attacker.transform;

        battler = command.attacker;
        targets = command.target;

        DestroyListener();
		onStartAttack = false;
		onClash = false;
		onDone = false;
		onExecuted = false;

        TempTarget = new List<Battler>();

        CheckTarget();
        battler.StateController.timeTransition = 0.5f;
        battler.BattlerState = BattlerState.Idle;

	}
    private void CheckTarget()
    {
        if (!battler.IsEnemyDeadAll())
        {
            if (targets.TrueForAll(x => x.BattlerState != BattlerState.Dead))
            {
                for (int i = 0; i < targets.Count; i++)
                {
                    if (targets[i].BattlerState != BattlerState.Dead)
                        targetBattler = targets[i];

                    targetedByTarget = Battle.Instance.Controller.CheckTargetCommand (battler, targetBattler);
                }
            }
            else
            {
                targetBattler = FindAnothertarget();
                targets = new List<Battler>();
                command.target = new List<Battler>();
                targets.Add(targetBattler);
                command.target.Add(targetBattler);
            }

            if (command.attacker.BattlerType == BattlerType.Player && targets.Any(x => x.BattlerType != BattlerType.Player))
            {
                EventManager.Instance.TriggerEvent(new TimedHitEvent(command.attacker, targets, command));
            }
        }
        else
        {
            onDone = true;
        }
    }

    private void SeeTarget()
    {
        Vector3 tempDefenderHitArea = new Vector3(targetBattler.HitArea.position.x, 0f, targetBattler.HitArea.position.z);
        battlerTrans.LookAt(tempDefenderHitArea);
    }

	private void moveToTarget () {
        SeeTarget();
		Vector3 tempDefenderHitArea = new Vector3 (targetBattler.HitArea.position.x, 0f, targetBattler.HitArea.position.z);
		Vector3 tempAttackerHitRange = new Vector3 (battler.HitRange.position.x, 0f, battler.HitRange.position.z);
		float distance = Vector3.Distance (tempAttackerHitRange, tempDefenderHitArea);
		
		if (distance < range && battler.BattlerState != BattlerState.Dead && !onDone) {
//			battler.BattlerState = BattlerState.Idle;
            battler.transform.LookAt(targetBattler.transform);
			checkReactSpeed();
		} else {
			if(battler.BattlerState != BattlerState.Move)
				battler.BattlerState = BattlerState.Move;

            //            if (targetBattler.BattlerState == BattlerState.Dead)
            //            {
            //                FindAnothertarget();
            //                if (TempTarget.Count > 0 && !GetTempTarget)
            //                {
            //                    GetTempTarget = true;
            //                    targetBattler = TempTarget[0];
            //                    tempDefenderHitArea = new Vector3 (targetBattler.HitArea.position.x, 0f, targetBattler.HitArea.position.z);
            //                }
            //            }

            if (battler.CheckState("RUN"))
            {
                Vector3 newLocation = Vector3.MoveTowards(tempAttackerHitRange, tempDefenderHitArea, Time.deltaTime * speed);
                Vector3 diff = newLocation - tempAttackerHitRange;
                battlerTrans.position = battlerTrans.position + diff;
            }
		}
	}

	private void checkReactSpeed(){
        if (targetedByTarget && targetBattler.AttackType == AttackType.Melee) {
            onClash = true;

            if (battler.BattlerType == BattlerType.Player)
            {
                if (!targetBattler.isInterrupt)
                    CalculateInterrupt(battler, targetBattler);
            }
            else
                if (!battler.isInterrupt)
                    CalculateInterrupt(targetBattler, battler);
                

			if(!targetBattler.Targeted.Contains(battler)){
				targetBattler.Targeted.Add(battler);
			} 
//			else if(!battler.Targeted.Contains(targetBattler)){
//				battler.Targeted.Add(targetBattler);
//			} 

            if (battler.ReactSpeed >= targetBattler.ReactSpeed || battler.TimedHit != null && battler.TimedHit.AttackHitState == TimedHitState.Perfect) {
				AttackProcces();
			} 
			else {
				AttackProcces();
			}
		} else {
			onClash = false;
			AttackProcces();
		}
	}

    void CalculateInterrupt (Battler characters, Battler encounter){
        if (characters.TimedHit != null)
        {
            int chance = (int)Formula.CalculateInterrupt(characters, encounter);
            int rand = (int)LegrandUtility.Random(0f, 100f);
            if(chance>0)
            {
                chance = Mathf.Abs(chance);
                if (rand < chance)
                {
                    characters.isInterrupt = false;
                    encounter.isInterrupt = true;
                }
                else
                {
                    characters.isInterrupt = false;
                    encounter.isInterrupt = false;
                }
            }
            else if(chance<0)
            {
                chance = Mathf.Abs(chance);
                if (rand < chance)
                {
                    characters.isInterrupt = true;
                    encounter.isInterrupt = false;
                }
                else
                {
                    characters.isInterrupt = false;
                    encounter.isInterrupt = false;
                }
            }
        }
    }
    private void AttackProcces(){
        battler.StateController.OnStartAttack -= HandleOnStartAttack;
        battler.StateController.OnAttack -= HandleOnAttack;
        battler.StateController.OnLastAttack -= HandleOnLastAttack;
		battler.StateController.OnStartAttack += HandleOnStartAttack;
		battler.StateController.OnAttack += HandleOnAttack;
		battler.StateController.OnLastAttack += HandleOnLastAttack;

		onExecuted = true;
        battlerTrans.LookAt (targetBattler.transform);
		//EventManager.Instance.TriggerEvent (new StartAttackEvent (command));

        for (int i = 0; battler.WeaponFX.Count > 0 && i < battler.WeaponFX.Count; i++)
        {
            battler.WeaponFX[i].ActiveParticle();
        }
		if (battler.TimedHit != null) {
			switch(battler.TimedHit.AttackHitState){
                case TimedHitState.Perfect:
                    if (onClash && battler.isInterrupt)
                        InterruptAction();
                    else
                        battler.BattlerState = BattlerState.Combo;
    				break;
    			case TimedHitState.Good :
                    if(onClash && battler.isInterrupt){
                        InterruptAction();
    				}else{
    					if (targetBattler.AttackType == AttackType.Melee)
    						battler.BattlerState = BattlerState.Combo;
    					else
    						battler.BattlerState = BattlerState.Attack;
    				}
    				break;
    			case TimedHitState.Miss :
                    if(onClash && battler.isInterrupt){
                        InterruptAction();
    				}else{
    					battler.BattlerState = BattlerState.Attack;
    				}
				break;
                default:
                    battler.BattlerState = BattlerState.Attack;
                    break;
			}
		} else {
            if (onClash && targetBattler.AttackType == AttackType.Melee && battler.isInterrupt)
            {
                battler.ActionController.mAttackAct.Interupt();
                battler.PopUp("Interrupted", battler.BattlerType);
                if (targetBattler.Targeted.Contains(battler))
                    targetBattler.Targeted.Remove(battler);
            }
            else {
                battler.BattlerState = BattlerState.Attack;
//                if (!targetBattler.isTimeHit)
//                {
//                    targetBattler.isTimeHit = true;
//                    if (targets.Count == 1)
//                        EventManager.Instance.TriggerEvent(new TimedDefEvent(battler, targetBattler));
//                }
            }
		}
		
		//battler.isInterrupt = true;
	}
    private void InterruptAction(){
        if (battler.BattlerState != BattlerState.Dead)
            battler.BattlerState = BattlerState.Idle;

        if (targetBattler.BattlerState != BattlerState.Dead)
            battler.PopUp("Interrupted", battler.BattlerType);

        targetBattler.Targeted.Remove (battler);
        DoneAttack();
        Interupt();
    }
	public void DoneAttack (){
//		onDone = true;
		_Interupted = false;
        for (int i = 0; battler.WeaponFX.Count > 0 && i < battler.WeaponFX.Count; i++)
        {
            battler.WeaponFX[i].DeactiveParticle();
        }

		
		battler.StateController.OnStartAttack -= HandleOnStartAttack;
		battler.StateController.OnAttack -= HandleOnAttack;
        battler.StateController.OnLastAttack -= HandleOnLastAttack;
		sendDoneAct (true);


	}

	public void Interupt()
	{
		_Interupted = true;
	}

	void HandleOnStartAttack (){
		onStartAttack = true;
	}
    void HandleOnLastAttack (string nameParticle){
		InstantiateAPS ();
//		targetBattler.HitState ();
		if (battler.BattlerState != BattlerState.Dead)
			battler.BattlerState = BattlerState.Idle;
		if(targetBattler.BattlerState != BattlerState.Dead)
			battler.Attack (targetBattler, battler);

        if (targetBattler.Targeted.Contains(battler))
            targetBattler.Targeted.Remove(battler);
        if (onClash)
        {
            if (battler.Targeted.Contains(targetBattler))
            battler.Targeted.Remove(targetBattler);
        }

        onDone = true;
	}
    void HandleOnAttack (string nameParticle){
        targetBattler.HitState ();
		InstantiateAPS ();
	}

	void InstantiateAPS (){
        hitEffect = BattlePoolingSystem.Instance.InstantiateAPS (battler.GetParticleHit(), new Vector3 (targetBattler.transform.position.x, targetBattler.transform.position.y + 1.5f, targetBattler.transform.position.z), new Quaternion (0, 0, 0, 0));
	}
	void DestroyListener (){
        if (battler.StateController.IsOnStartAttackEvent())
            battler.StateController.ClearAttackEvent();

        if (battler.StateController.IsOnAttackEvent())
            battler.StateController.ClearOnAttackEvent();
		
        if (battler.StateController.IsOnLastAttackEvent())
            battler.StateController.ClearOnLastAttackEvent();
	}

//    void FindAnothertarget ()
//    {
//        List <Battler> listTarget = new List<Battler>();
//        if (battler.BattlerType == BattlerType.Player)
//            listTarget = Battle.Instance.encounterList;
//        else
//            listTarget = Battle.Instance.charList;
//        for (int i = 0; i < listTarget.Count; i++)
//        {
//            if (listTarget[i].BattlerState != BattlerState.Dead)
//                TempTarget.Add(listTarget[i]);
//        }
//    }
    private Battler FindAnothertarget()
    {
        List<Battler> listTarget = new List<Battler>();
        if (battler.BattlerType == BattlerType.Player)
            listTarget = Battle.Instance.encounterList;
        else
            listTarget = Battle.Instance.charList;

        for (int i = 0; i < listTarget.Count; i++)
        {
            if (listTarget[i].BattlerState != BattlerState.Dead)
                TempTarget.Add(listTarget[i]);
        }
        if (TempTarget.Count > 0)
            return TempTarget[0];
        else
            return null;
    }

//	void CheckMoveBattlers(){
//		if (battler.BattlerType == BattlerType.Enemy) {
//			Encounter e = (battler.Character as Encounter);
//			if (e.MonsterSize == EncounterSize.Massive) {
//				AttackProcces ();
//			}
//		} else {
//			moveToTarget ();
//		}
//	}
	public override IEnumerator Update ()
	{
		while (onUpdate) {
			if(!onDone && !_Interupted){
				if (targetBattler.BattlerState == BattlerState.Dead) {
					sendDoneAct (true);
				} else if (battler.BattlerState == BattlerState.Dead) {
					sendDoneAct (true);
				} else if (!onStartAttack && battler.Targeted.Count <= 0 && battler.BattlerState != BattlerState.Hit && battler.BattlerState != BattlerState.Dead) {
					moveToTarget ();
//					CheckMoveBattlers();
				} 
//                else if (onStartAttack) {
//					// Always see target while attack
//					Vector3 tempDefenderHitArea = new Vector3 (targetBattler.HitArea.position.x, targetBattler.transform.position.y, targetBattler.HitArea.position.z);
//					battlerTrans.LookAt (tempDefenderHitArea);
//				} 
                else if (!onExecuted && battler.Targeted.Count > 0 && battler.BattlerState != BattlerState.Hit && battler.BattlerState != BattlerState.Dead) {
					battler.BattlerState = BattlerState.Idle;
					yield return new WaitForSeconds (0.5f);
					if (!onDone && battler.BattlerState == BattlerState.Idle) {
						onExecuted = false;
						onStartAttack = false;
					}
				} else if (onExecuted && onStartAttack && !onDone && battler.BattlerState == BattlerState.Idle) {
					onExecuted = false;
					onStartAttack = false;
					moveToTarget ();
//					CheckMoveBattlers();
				}
                else if (onExecuted && onStartAttack && !onDone && (battler.BattlerState == BattlerState.Attack || battler.BattlerState == BattlerState.Combo)) //Sedang Attack selalu lihat target
                    SeeTarget();
            }
            else{
				yield return new WaitForSeconds(0.3f);

				if ((battler.Targeted.Count <= 0 && battler.BattlerState != BattlerState.Hit) || battler.BattlerState == BattlerState.Dead){
					//				battler.BattlerState = BattlerState.Idle;
					DoneAttack ();
					break;
				}
			}
			yield return null;
		}
		DestroyListener();

	}
}
