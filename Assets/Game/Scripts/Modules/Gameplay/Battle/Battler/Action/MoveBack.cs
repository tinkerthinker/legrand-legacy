﻿using UnityEngine;
using System.Collections;

public class MoveBack : BaseAction {
	Battler battler;
	Transform trans;
	Vector3 oriPos;
	Quaternion oriRot;
	float speed;

	public override void initAction (BattleCommand com, float speed, Vector3 oriPos, Quaternion oriRot)
	{
		base.initAction (com, speed, oriPos, oriRot);
		battler = com.attacker;
		this.speed = speed;
		this.oriPos = oriPos;
		this.oriRot = oriRot;
		trans = battler.transform;
	}
	private void move () {
		float distance = Vector3.Distance (trans.position, oriPos);
		
		if (distance <= 0.1f) {
			battler.SetLockedState(false);
			trans.position = oriPos;
			trans.rotation = oriRot;
			sendDoneAct (true);
		} else {
//            if (!battler.StateController.Anim.GetCurrentAnimatorStateInfo(0).IsName("RUN"))
//            {
//                battler.SetLockedState(false);
				battler.BattlerState = BattlerState.Move;
            //				battler.SetLockedState(true);
            //			}

            if (battler.CheckState("RUN"))
            {
                trans.position = Vector3.MoveTowards(trans.position, oriPos, Time.deltaTime * speed);
                trans.LookAt(oriPos);
            }
		}
	}
	
	public override IEnumerator Update ()
	{
		while (onUpdate) {
			if (battler.BattlerState != BattlerState.Hit) {
				if (battler.Targeted.Count <= 0)
					move ();
			} 
			else {
                battler.SetLockedState(false);
                battler.RecheckState ();
                battler.SetLockedState(true);
            }
			yield return null;
		}
	}
}
