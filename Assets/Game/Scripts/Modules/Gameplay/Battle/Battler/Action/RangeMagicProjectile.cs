﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;
using System.Linq;

public class RangeMagicProjectile : BaseAction {
	Transform trans;
    private Battler targetBattler;
    private string _ElementMagic;
    private List<Battler> targets; //default targets
    private List <Battler> TempTarget; //targets possible temp
    bool OnAttack = false;
    bool OnLastAttack = false;
    private Magic magic;
    private string gParticle;
	public override void initAction (BattleCommand com)
    {
        OnAttack = false;
        OnLastAttack = false;
		base.initAction (com);
		command = com;
		battler = com.attacker;
        DestroyListener();
		onUpdate = true;
        trans = battler.transform;
        targets = com.target;
		onDone = false;

        TempTarget = new List<Battler>();

		if (command.magicCasted != null) 
            command.magicCasted.DestroyBattleAPS ();

        if (battler.BattlerType == BattlerType.Player)
        {
            _ElementMagic = battler.GetMagicEffect(command, false);
            magic = (battler.Character as MainCharacter).GetMagic(command.commandData.CommandValue);
        }
        else
        {   _ElementMagic = battler.GetMagicEffect(command, false);
            magic = (battler.Character as Encounter).GetMagic(command.commandData.CommandValue);
        }

        CheckTarget();
        battler.StateController.timeTransition = 0.5f;
	}
    private void CheckTarget()
    {
        if (!battler.IsEnemyDeadAll())
        {
            if (targets.Any(x => x.BattlerState != BattlerState.Dead))
            {
                for (int i = 0; i < targets.Count; i++)
                {
                    if (targets[i].BattlerState != BattlerState.Dead)
                        targetBattler = targets[i];
                }
            }
            else
            {
                targetBattler = FindAnothertarget();
                targets = new List<Battler>();
                command.target = new List<Battler>();
                targets.Add(targetBattler);
                command.target.Add(targetBattler);
            }

//            if (command.attacker.BattlerType == BattlerType.Player && targets.Any(x => x.BattlerType != BattlerType.Player))
//            {
//                EventManager.Instance.TriggerEvent(new TimedHitEvent(command.attacker, targets, command));
//                battler.ActionBar.ShowActionBar (command.commandData.CommandName);
//            }
        }
        else
        {
            onDone = true;
        }
    }
	void castAction(){
		if (!onExecuted) {
            onExecuted = true;

            trans.LookAt (targetBattler.transform);
			
//            if (targetBattler.BattlerType == BattlerType.Player && targetBattler.TimedHit != null) {
//                EventManager.Instance.TriggerEvent(new TimedDefEvent(battler, targetBattler));
//
//                for (int i = 0; i < targets.Count; i++)
//                    targets[i].TimedHit.DefendHitState = targetBattler.TimedHit.DefendHitState;
//
//				battler.ActionBar.ShowActionBar (command.commandData.CommandName);
//			}

			if(battler.BattlerState != BattlerState.Hit)
				battler.BattlerState = BattlerState.Magic;

            battler.StateController.OnAttack += HandleOnAttack;
            battler.StateController.OnLastAttack += HandleOnLastMagic;
		}
	}

    void DestroyListener (){
        if (battler.StateController.IsOnAttackEvent())
            battler.StateController.ClearOnAttackEvent();

        if (battler.StateController.IsOnLastAttackEvent())
            battler.StateController.ClearOnLastAttackEvent();
    }

    void HandleOnAttack(string nameParticle){

        if (battler.BattlerType == BattlerType.Player)
            gParticle = projectile(nameParticle, true);
        else
            gParticle = projectile(nameParticle, false);

        if (gParticle == "")
            onDone = true;
        else
        { 
            if (targets.TrueForAll(x => x.BattlerState == BattlerState.Dead))
                onDone = true;
            else
            {
                for (int i = 0; i < targets.Count; i++)//each (Battler b in targets)
                {
                    if (targets[i].BattlerState != BattlerState.Dead)
                    {
                        //                  GameObject gBattleParticle = GameObject.Instantiate<GameObject>(gParticle);
                        GameObject gBattleParticle = BattlePoolingSystem.Instance.InstantiateAPS(gParticle);
                        gBattleParticle.transform.position = battler.HitRange.position;
                        BattleParticle bp = gBattleParticle.GetComponent<BattleParticle>();

                        if (!OnAttack)
                        {
                            if (bp.OnParticleDone != null)
                                bp.OnParticleDone -= HandleOnMagicHit;

                            bp.OnParticleDone += HandleOnMagicHit;
                            OnAttack = true;
                        }

                        bp.Initiate(battler.HitRange.gameObject, targets[i].HitRange.gameObject);

                    }
                }
            }
        }
    }
    void HandleOnMagicHit(BattleParticle bp){
        if (bp.OnParticleDone != null)
            bp.OnParticleDone -= HandleOnMagicHit;

        WwiseManager.Instance.PlaySFX(WwiseManager.BattleFX.Blast_Hit);
    }
    void HandleOnLastMagic(string nameParticle){
        if (battler.BattlerType == BattlerType.Player)
            gParticle = projectile(nameParticle, true);
        else
            gParticle = projectile(nameParticle, false);
        
        if (gParticle == "")
            onDone = true;
        else
        { 
            if (targets.TrueForAll(x => x.BattlerState == BattlerState.Dead))
                onDone = true;
            else
            {
                for (int i = 0; i < targets.Count; i++)//each (Battler b in targets)
                {
                    if (targets[i].BattlerState != BattlerState.Dead)
                    {
//                  GameObject gBattleParticle = GameObject.Instantiate<GameObject>(gParticle);
                        GameObject gBattleParticle = BattlePoolingSystem.Instance.InstantiateAPS(gParticle);
                        gBattleParticle.transform.position = battler.HitRange.position;
                        BattleParticle bp = gBattleParticle.GetComponent<BattleParticle>();

                        if (!OnLastAttack)
                        {
                            if (bp.OnParticleDone != null)
                                bp.OnParticleDone -= HandleOnLastMagicHit;

                            bp.OnParticleDone += HandleOnLastMagicHit;
                            OnLastAttack = true;
                        }

                        bp.Initiate(battler.HitRange.gameObject, targets[i].HitRange.gameObject);

                    }
                }
            }

        }
//        battler.BattlerState = BattlerState.Idle;
	}
    void HandleOnLastMagicHit (BattleParticle bp)
    {   
        battler.MagicAttack (command.commandData.CommandValue, targets);

        onDone = true;
        //targetBattler.Targeted.Remove (battler);
        if (bp.OnParticleDone != null)
            bp.OnParticleDone -= HandleOnLastMagicHit;
        
        WwiseManager.Instance.PlaySFX(WwiseManager.BattleFX.Blast_Hit);


    }

    private Battler FindAnothertarget ()
    {
        List <Battler> listTarget = new List<Battler>();
        if (battler.BattlerType == BattlerType.Player)
            listTarget = Battle.Instance.encounterList;
        else
            listTarget = Battle.Instance.charList;

        for (int i = 0; i < listTarget.Count; i++)
        {
            if (listTarget[i].BattlerState != BattlerState.Dead)
                TempTarget.Add(listTarget[i]);
        }
        if (TempTarget.Count > 0)
            return TempTarget[0];
        else
            return null;
    }
	public override IEnumerator Update ()
	{
		while (onUpdate) {
			if(!onDone){
//                if(targets.TrueForAll (x=>x.BattlerState == BattlerState.Dead)){
//                    sendDoneAct (true);
//                }else 
                if(!onExecuted){
                    yield return new WaitForSeconds(0.25f);
					castAction();
				}else if(onExecuted)
				{
//					yield return new WaitForSeconds(0.5f);
//					Animator anim = battler.GetComponent<Animator>();
//					if(!anim.GetCurrentAnimatorStateInfo(0).IsName("MAGIC"))
//					{
//						battler.BattlerState = BattlerState.Magic;
//					}
				}
			}else{	
                yield return new WaitForSeconds(1f);
                sendDoneAct(true);
				break;
			}
			yield return null;
		}
		DestroyListener();
	}
    public string projectile (string nameParticle, bool isPlayer){
        string[] particleName = nameParticle.Split('/');
        if (_ElementMagic == "None" || _ElementMagic == "")
            _ElementMagic = "Water";
        if (isPlayer)
            return particleName[1] + _ElementMagic;
        else
        {
            return particleName[1] + _ElementMagic +command.commandData.CommandValue;
        }
//        GameObject p;
//        if (isPlayer)
//            p = Resources.Load<GameObject>("Battle/Battle Particles Resources/" + nameParticle +_ElementMagic);
//        else
//            p = Resources.Load<GameObject>("Battle/Battle Particles Resources/" + nameParticle + command.commandData.CommandValue);
//        return p;
    }
}
