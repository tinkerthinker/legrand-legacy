﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Legrand.core;
using System.Linq;


public class CasterAttack : BaseAction
{
    Battler targetBattler;
    Transform trans;
    Vector3 oriPos;
    Vector3 oriRot;
    private bool onStartAttack;
    private BattlerState _AttackType;
    private GameObject hitEffect;
    private List<Battler> targets;
    private List<Battler> TempTarget;
    private Transform hitRange;

    private Animator anim;
    public override void initAction(BattleCommand com, float speed, Vector3 oriPos, Quaternion oriRot)
    {
        base.initAction(com, speed, oriPos, oriRot);
        battler = command.attacker;
        DestroyListener();
        targetBattler = command.target[0];
        targets = command.target;
        this.oriPos = oriPos;
        this.oriRot = battler.transform.eulerAngles;
        trans = battler.transform;
        onStartAttack = false;
        anim = battler.GetComponent<Animator>();
        hitRange = battler.HitRange;
        //        if (battler.BattlerType == BattlerType.Player){
        //            if (battler.AttackType == AttackType.Range) {
        //           
        //                NameProjectile = "Orb";
        //                speed = 30f;
        //            }
        //        }else {
        //            Encounter enc = (battler.Character as Encounter);
        //            if (enc.Projectile == ProjectileType.None)
        //                NameProjectile = "Arrow2";
        //            else
        //                NameProjectile = enc.Projectile.ToString ();
        //            speed = 40f;
        //        }
        TempTarget = new List<Battler>();
        CheckTarget();

        Debug.Log(command);
    }
    private void CheckTarget()
    {
        if (!battler.IsEnemyDeadAll())
        {
            if (targets.TrueForAll(x => x.BattlerState != BattlerState.Dead))
            {
                for (int i = 0; i < targets.Count; i++)
                {
                    if (targets[i].BattlerState != BattlerState.Dead)
                        targetBattler = targets[i];
                }
            }
            else
            {
                targetBattler = FindAnothertarget();
                targets = new List<Battler>();
                command.target = new List<Battler>();
                targets.Add(targetBattler);
                command.target.Add(targetBattler);
            }

            if (command.attacker.BattlerType == BattlerType.Player && targets.Any(x => x.BattlerType != BattlerType.Player))
            {
                EventManager.Instance.TriggerEvent(new TimedHitEvent(command.attacker, targets, command));
            }
        }
        else
        {
            onDone = true;
        }
    }
    void DestroyListener()
    {
        if (battler.StateController.IsOnAttackEvent())
            battler.StateController.ClearOnAttackEvent();

        if (battler.StateController.IsOnLastAttackEvent())
            battler.StateController.ClearOnLastAttackEvent();
    }

    void aimTarget()
    {
        if (!onExecuted)
        {
            onExecuted = true;

//            if (targetBattler.BattlerState == BattlerState.Dead)
//            {
//                FindAnothertarget();
//                if (TempTarget.Count > 0 && !GetTempTarget)
//                    targetBattler = TempTarget[0];
//            }

            if (targetBattler.BattlerState != BattlerState.Dead)
            {
                battler.StateController.OnAttack += HandleOnAttack;
                battler.StateController.OnLastAttack += HandleOnLastAttack;
                trans.LookAt(targetBattler.transform);
                if (battler.TimedHit != null)
                {
                    if (battler.TimedHit.AttackHitState == TimedHitState.Perfect || battler.TimedHit.AttackHitState == TimedHitState.Good)
                    {
                        _AttackType = BattlerState.Combo;
                        if (battler.BattlerState != BattlerState.Hit)
                            battler.BattlerState = BattlerState.Combo;
                    }
                    else
                    {
                        _AttackType = BattlerState.Attack;
                        if (battler.BattlerState != BattlerState.Hit)
                            battler.BattlerState = BattlerState.Attack;
                    }
                }
                else
                {
                    _AttackType = BattlerState.Attack;
                    if (targets.Count == 1)
                        EventManager.Instance.TriggerEvent(new TimedDefEvent(battler, targetBattler));

                    if (battler.BattlerState != BattlerState.Hit)
                        battler.BattlerState = BattlerState.Attack;
                }
            }
            else
            {
                onDone = true;
            }
        }

    }

    void HandleOnAttack(string particleData)
    {
        int i = 0;
        int direction = 0;
        string[] particleDatas = particleData.Split(',');
        foreach (Battler target in targets)
        {

            if (particleDatas.Length > 1)
                direction = int.Parse(particleDatas[1]);

            Vector3 dir = new Vector3();

            if (direction == 0)
                dir = new Vector3(0f, 0f, 0f);
            else if (direction == 1)
            {
                dir = battler.transform.TransformDirection(1f, 0f, 0f);
                if (dir.x > 0f)
                    dir = new Vector3(0, 0, 1f);
                else if (dir.x < 0f)
                    dir = new Vector3(0, 0, 2f);
            }
            else if (direction == 2)
            {
                dir = battler.transform.TransformDirection(-1f, 0f, 0f);
                if (dir.x > 0f)
                    dir = new Vector3(0, 0, 1f);
                else if (dir.x < 0f)
                    dir = new Vector3(0, 0, 2f);
            }
            else if (direction == 3) dir = new Vector3(0f, 0f, 3f);

            //            GameObject p = GameObject.Instantiate<GameObject>(Resources.Load<GameObject>("Battle/Battle Particles Resources/" + particleDatas[0]));
            string particleName = GetParticleName(particleDatas[0]);
            GameObject p = BattlePoolingSystem.Instance.InstantiateAPS(particleName);

            p.transform.position = hitRange.position;
            BattleParticle battleParticle = p.GetComponent<BattleParticle>();

            if (i == 0)
            {
                battleParticle.ClearOnParticle();
                battleParticle.OnParticleDone += HandleProjectileHit;
                i++;
            }

            if (battleParticle.DataHelper.ContainsKey("Direction"))
                battleParticle.DataHelper.Remove("Direction");

            battleParticle.AddData("Direction", Mathf.RoundToInt(dir.z));
            battleParticle.Initiate(hitRange.gameObject, target.HitRange.gameObject);
        }
        //bp.setTarget (targetBattler.HitArea.transform);
    }

    void HandleOnLastAttack(string particleData)
    {
        int i = 0;
        int direction = 0;
        string[] particleDatas = particleData.Split(',');
        foreach (Battler target in targets)
        {
            target.Targeted.Remove(battler);

            if (particleDatas.Length > 1)
                direction = int.Parse(particleDatas[1]);

            Vector3 dir = new Vector3();

            if (direction == 0)
                dir = new Vector3(0f, 0f, 0f);
            else if (direction == 1)
            {
                dir = battler.transform.TransformDirection(1f, 0f, 0f);
                if (dir.x > 0f)
                    dir = new Vector3(0, 0, 1f);
                else if (dir.x < 0f)
                    dir = new Vector3(0, 0, 2f);
            }
            else if (direction == 2)
            {
                dir = battler.transform.TransformDirection(-1f, 0f, 0f);
                if (dir.x > 0f)
                    dir = new Vector3(0, 0, 1f);
                else if (dir.x < 0f)
                    dir = new Vector3(0, 0, 2f);
            }
            else if (direction == 3) dir = new Vector3(0f, 0f, 3f);

            //            GameObject p = GameObject.Instantiate<GameObject>(Resources.Load<GameObject>("Battle/Battle Particles Resources/" + particleDatas[0]));

            string particleName = GetParticleName(particleDatas[0]);
            GameObject p = BattlePoolingSystem.Instance.InstantiateAPS(particleName);

            p.transform.position = hitRange.position;
            BattleParticle battleParticle = p.GetComponent<BattleParticle>();

            if (i == 0)
            {
                battleParticle.ClearOnParticle();
                battleParticle.OnParticleDone += HandleProjectileLastHit;
                i++;
            }

            if (battleParticle.DataHelper.ContainsKey("Direction"))
                battleParticle.DataHelper.Remove("Direction");

            battleParticle.AddData("Direction", Mathf.RoundToInt(dir.z));
            battleParticle.Initiate(hitRange.gameObject, target.HitRange.gameObject);
        }
        //bp.setTarget (targetBattler.HitArea.transform);

        //targetBattler.Targeted.Remove (battler);

        DestroyListener();
    }

    void InstantiateAPS(Battler b)
    {
        hitEffect = BattlePoolingSystem.Instance.InstantiateAPS(battler.GetParticleHit(), new Vector3(b.transform.position.x, b.transform.position.y + 1.5f, b.transform.position.z), new Quaternion(0, 0, 0, 0));
    }

    private Battler FindAnothertarget()
    {
        List<Battler> listTarget = new List<Battler>();
        if (battler.BattlerType == BattlerType.Player)
            listTarget = Battle.Instance.encounterList;
        else
            listTarget = Battle.Instance.charList;

        for (int i = 0; i < listTarget.Count; i++)
        {
            if (listTarget[i].BattlerState != BattlerState.Dead)
                TempTarget.Add(listTarget[i]);
        }
        if (TempTarget.Count > 0)
            return TempTarget[0];
        else
            return null;
    }
    public override IEnumerator Update()
    {
        while (onUpdate)
        {
            if (!onDone)
            {
                if (targetBattler.BattlerState == BattlerState.Dead && battler.BattlerState != BattlerState.Attack && battler.BattlerState != BattlerState.Combo)
                {
                    onDone = true;
                }
                else if (battler.BattlerState == BattlerState.Dead)
                {
                    onDone = true;
                }
                else if (!onExecuted)
                {
                    yield return new WaitForSeconds(0.5f);
                    aimTarget();
                }
                else if ((onExecuted && !onStartAttack) || (onStartAttack && !onDone))
                {
                    yield return new WaitForSeconds(0.5f);
                    if (_AttackType == BattlerState.Attack)
                    {
                        battler.BattlerState = BattlerState.Attack;
                    }
                    else if (_AttackType == BattlerState.Combo)
                    {
                        battler.BattlerState = BattlerState.Combo;
                    }
                    yield return null;
                }
            }
            else
            {
                sendDoneAct(true);
                break;
            }
            yield return null;
        }
        DestroyListener();
        //      battler.transform.DORotate (oriRot, 0.5f, RotateMode.Fast);
        //      battler.transform.eulerAngles = oriRot;
        sendDoneAct(true);
    }
    #region particle

    void HandleProjectileHit(BattleParticle bp)
    {

        foreach (Battler b in targets)
        {
            if (b.BattlerState != BattlerState.Dead)
            {
                b.HitState();
            }
        }

        WwiseManager.Instance.PlaySFX(WwiseManager.BattleFX.Blast_Hit);
    }
    void HandleProjectileLastHit(BattleParticle bp)
    {

        foreach (Battler b in targets)
        {
            if (b.BattlerState != BattlerState.Dead)
            {
                //                b.HitState();
                battler.Attack(b, battler);
            }
        }

        bp.OnParticleDone -= HandleProjectileHit;
        bp.OnParticleDone -= HandleProjectileLastHit;
        WwiseManager.Instance.PlaySFX(WwiseManager.BattleFX.Blast_Hit);
        onDone = true;
    }
    #endregion

    private string GetParticleName(string particleData)
    {
        string[] particleName = particleData.Split('/');
        return particleName[1];
    }
}
