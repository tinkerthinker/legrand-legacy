﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;
using System.Linq;

public class UseItem : BaseAction {
	List<Battler> targets; // target sebelum ada kondisi2 yang ada
	private Battler battlerTarget; //target yang ada di reserve diganti sesuai battle id
    int index = 0; //variable to check iteration
    private Battler TargetBattler;
    private List <Battler> TempBattler; // target cadangan apabila target utama tidak bisa
    private Consumable item;
    private List <Battler> FixTarget; // target setelah ada beberapa kondisi yang paling mungkin diserang

    private List <Battler> listOfTarget = new List<Battler>();

	public override void initAction (BattleCommand com)
	{
        index = 0;
		base.initAction (com);
		battler = command.attacker;
		targets = command.target;
        TargetBattler = targets[0];
        TempBattler = new List<Battler>();
        item = battler.GetItem(command.commandData.CommandValue) as Consumable;
        FixTarget = new List<Battler>();

        if (TargetBattler.BattlerType == BattlerType.Player)
            listOfTarget = Battle.Instance.charList;
        else
            listOfTarget = Battle.Instance.encounterList;


        battler.StateController.timeTransition = 0.5f;
	}
	private void useItem (){
		if (!onExecuted) {
			onExecuted = true;
            string[] itemParticle = item.ItemParticle.Split('/');
//            GameObject pNewParticle = BattlePoolingSystem.Instance.InstantiateAPS(item.ItemParticle);
            List<Battler> bats = new List<Battler>();
            //must check item type
            if (battler.BattlerType == BattlerType.Player)
            {
                switch (item.Target)
                {
                    case TargetType.Single:
                        if (battler.BattlerType == BattlerType.Enemy)
                            CheckMagicItem(item);
                
                        if (!TargetBattler.inReserve)
                            battlerTarget = TargetBattler;
                        else
                            battlerTarget = Battle.Instance.charList[TargetBattler.BattleID];

                        FixTarget.Add(battlerTarget);
                        SendParticle(itemParticle[3]);
                        break;
                    case TargetType.Column:
                        bats = listOfTarget.FindAll(x => x.BattlePosition.x.Equals(command.GridTarget[0].x));
                        for (int i = 0; bats.Count > 0 && i < bats.Count; i++)
                        {
                            battlerTarget = bats[i];
                            SendParticle(itemParticle[3]);
                            FixTarget.Add(battlerTarget);
                        }
                        break;
                    case TargetType.Row:
                        bats = listOfTarget.FindAll(x => x.BattlePosition.y.Equals(command.GridTarget[0].y));
                        for (int i = 0; bats.Count > 0 && i < bats.Count; i++)
                        {
                            battlerTarget = bats[i];
                            SendParticle(itemParticle[3]);
                            FixTarget.Add(battlerTarget);
                        }
                        break;
                    default:
                        for (int i = 0; i < targets.Count; i++)
                        {
                            if (!targets[i].inReserve)
                                battlerTarget = targets[i];
                            else
                                battlerTarget = listOfTarget[targets[i].BattleID];
                       
                            SendParticle(itemParticle[3]);
                            FixTarget.Add(battlerTarget);
                        }
                        break;
                }
                battler.BattlerState = BattlerState.UseItem;
            }
            else
            {
                if (targets.TrueForAll(x => x.BattlerState != BattlerState.Dead))
                {
                    for (int i = 0; i < targets.Count; i++)
                    {
                        if (!targets[i].inReserve)
                            battlerTarget = targets[i];
                        else
                            battlerTarget = listOfTarget[targets[i].BattleID];

                        FixTarget.Add(battlerTarget);
                        SendParticle(itemParticle[3]);
                    }

                    battler.BattlerState = BattlerState.UseItem;
                }
                else
                {
                    onDone = true;
                }
            }
            if(item.ItemParticle == "")
                particleDone(null);
		}
	}

    void SendParticle (string pNewParticle)
    {
        if (pNewParticle != null)
        {
//          GameObject newParticle = GameObject.Instantiate<GameObject>(pNewParticle);
            GameObject newParticle = BattlePoolingSystem.Instance.InstantiateAPS(pNewParticle);
            BattleParticle battleParticle = newParticle.GetComponent<BattleParticle>();
            if (index == 0)
            {
                if (battleParticle != null)
                    battleParticle.OnParticleDone -= particleDone;

                battleParticle.OnParticleDone += particleDone;
                index++;
            }
            battleParticle.Initiate(battler.gameObject, battlerTarget.gameObject);

            if (item.ConsumableType == Consumable.ConsumeType.Healing)
            {
                ConsumableHealing healingItem = (ConsumableHealing) item;
                if (healingItem.CuredStatuses.Contains(Status.Dead))
                    WwiseManager.Instance.PlaySFX(WwiseManager.ItemGrimoireFX.Crossbone);
                else
                    WwiseManager.Instance.PlaySFX(WwiseManager.ItemGrimoireFX.Bloodstone);
            }
            else
            {
                WwiseManager.Instance.PlaySFX(WwiseManager.BattleFX.Eris_Force_Misille_Journey);
            }
        }
    }
    void particleDone(BattleParticle particle)
    {
        battler.UseItem(command.commandData.CommandValue, FixTarget);
        for (int i = 0; i < FixTarget.Count; i++)
        {
            if (FixTarget[i].Targeted.Count > 0 && FixTarget[i].Targeted.Contains(battler))
            {
                FixTarget[i].Targeted.Remove(battler);
            }
        }
        if (item.ConsumableType == Consumable.ConsumeType.Magic)
        {
            ConsumableMagic consMag = (ConsumableMagic)item;
            if (consMag.Buff.BuffSecondary.Count > 0 && consMag.Buff.BuffSecondary.Any (x=>x.AttributeType == Attribute.StatusResistance)){
                for (int i=0; i < FixTarget.Count;i++){
//                    ItemController._Instance.ItemCleanseEffect(FixTarget[i]);
                }
            }
        }
        particle.OnParticleDone -= particleDone;
        onDone = true;
    }
    //debuff cant spamming, if there same debuff change the target random
    void CheckMagicItem (Consumable consumable)
    {
        if (consumable.ConsumableType == Consumable.ConsumeType.Magic)
        {
            ConsumableMagic magicItem = (ConsumableMagic) consumable;
            if (TargetBattler.BuffDebuffStat.Count > 0)
            {
                for (int i = 0; i < TargetBattler.BuffDebuffStat.Count; i++)
                {
                    if (TargetBattler.BuffDebuffStat[i]._ID == magicItem.Buff._ID)
                    {
                        FindAnothertarget();
                    }
                }
            }
        }
    }
    void FindAnothertarget ()
    {
        for (int i = 0; i < listOfTarget.Count; i++)
        {
            if (listOfTarget[i].BattlerState != BattlerState.Dead && listOfTarget[i].BattleID != TargetBattler.BattleID)
                TempBattler.Add(listOfTarget[i]);
        }
        if (TempBattler.Count > 0)
            TargetBattler = TempBattler[0];
    }
    public override IEnumerator Update ()
	{
		while (onUpdate) {
			if (!onDone) {
				if (!onExecuted) {
//					yield return new WaitForSeconds (3f);
                    if (battler.BattlerState != BattlerState.Dead)
                        useItem();
                    else
                        onDone = true;
				}
			} else {
                yield return new WaitForSeconds (2f);
				sendDoneAct (true);
			}
			yield return null;
		}
	}
}
