﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

using Legrand.core;

public class SkillAttack : BaseAction
{
    private List<Battler> targets = new List<Battler>();
    private ScreenFader fader;
    //	private Vector3 targetOriPos;
    private BattleCameraController _CameraAnim;
    private Transform _SkillCamera;
    private GameObject hitEffect;
    //	private GameObject auraEffect;
    private MainCharacter mainChar;

    private List<Battler> TempTarget; //targets possible temp
    private Battler targetBattler;
    private OffensiveBattleSkill offensiveSkill;

    BattlerSkillController skillController;

    Transform dofTrans;
    public override void initAction(BattleCommand com)
    {
        base.initAction(com);
        battler = command.attacker;
        onDone = false;
        targets = command.target;
        fader = Battle.Instance.ScreenFader;

        _CameraAnim = Battle.Instance.BattleCameraController;
        _SkillCamera = _CameraAnim.SkillCamera;
        TempTarget = new List<Battler>();
        offensiveSkill = (battler.Character as MainCharacter).BattleOffensiveSkill;
        dofTrans = _CameraAnim.GetDOF();
        if (!battler.IsEnemyDeadAll())
        {
            if (targets.TrueForAll(x => x.BattlerState == BattlerState.Dead))
            {
                targetBattler = FindAnothertarget();
                targets = new List<Battler>();
                targets.Add(targetBattler);
            }
        }
        else
        {
            onDone = true;
        }
        battler.StateController.timeTransition = 0f;
    }

    private void initSkillCharacter (){
        MainCharacter mc =  battler.Character as MainCharacter;

        if (mc.BattleOffensiveSkill.Name != "" && mc.BattleOffensiveSkill.Name != "None") {
            GameObject skillContainer = GameObject.Instantiate(Resources.Load("Battle/LimitBreak/"+offensiveSkill.Name)) as GameObject;//BattlePoolingSystem.Instance.InstantiateAPS (mc.BattleOffensiveSkill.Name, new Vector3 (0, 0, 0), new Quaternion (0, 0, 0, 0));
            skillContainer.name = "LimitBreak";
            skillController = skillContainer.GetComponent<BattlerSkillController> ();
            skillController.init (offensiveSkill);
            fader.transform.parent.parent = null;

        }
    }
    private void castSkill()
    {
        if (!onExecuted)
        {
            battler.ChangeGauge(-offensiveSkill.Cost);
            onExecuted = true;
            List<GameObject> templist = new List<GameObject>();
            bool OnCastSkill = false;

            for (int i = 0; i < targets.Count; i++)
            {
                Battler bat = targets[i];
                if (bat.BattlerState != BattlerState.Dead && targets.Count > 0 && !OnCastSkill)
                {
                    OnCastSkill = true;
                    fader.OnFadeOut += StartSkill;
                    fader.FadeSpeed = 0.1f;
                    fader.EndScene();
//                    StartSkill();
                }
            }

            for (int i = 0; battler.WeaponFX.Count > 0 && i < battler.WeaponFX.Count; i++)
            {
                battler.WeaponFX[i].ActiveParticle();
            }

            if (targets.TrueForAll(x => x.BattlerState == BattlerState.Dead))
            {
                onDone = true;
            }
        }
    }
    private void StartSkill()
    {
        fader.OnFadeOut -= StartSkill;
        initSkillCharacter();

        battler.StateController.OnDoneSkill += HandleOnSkillDone;
        battler.StateController.OnTimeHit += HandleOnTimeHit;
        _CameraAnim.onSkill = true;
        if (dofTrans != null)
        {
            dofTrans.SetParent(battler.transform.FindChild("Bip001").transform);
            dofTrans.transform.localPosition = new Vector3(0, 0, 0);
        }
        _SkillCamera.gameObject.SetActive(true);
        _CameraAnim.MainCamera.enabled = false;

        for (int i = 0; i < targets.Count; i++)
        {
            if (targets[i].BattlerState != BattlerState.Dead)
                LegrandUtility.SetLayer(targets[i].gameObject, LayerMask.NameToLayer("LimitBreak"));
        }

        LegrandUtility.SetLayer(battler.gameObject, LayerMask.NameToLayer("LimitBreak"));
        skillController.PlayCameraAnimation(battler, targets);
    }
    void HandleOnTimeHit()
    {
        battler.StateController.OnTimeHit -= HandleOnTimeHit;
        EventManager.Instance.TriggerEvent(new TimedHitEvent(command.attacker, command.target, command));
        battler.ActionBar.ShowActionBar(command.commandData.CommandName);
    }
    void HandleOnSkillDone()
    {
        battler.StateController.OnDoneSkill -= HandleOnSkillDone;
        battler.BattlerState = BattlerState.Idle;
        onFadeComplete();
    }
    void InstantiateAPS(Battler target)
    {
        BattlePoolingSystem.Instance.InstantiateAPS(battler.GetParticleHit(), new Vector3(target.transform.position.x, target.transform.position.y + 1.5f, target.transform.position.z), new Quaternion(0, 0, 0, 0));
    }

    void onFadeComplete()
    {
        fader.transform.parent.SetParent(Battle.Instance.transform);
        battler.transform.SetParent(Battle.Instance.Controller.transform);
        battler.transform.position = battler.MyPlane.transform.position;
        LegrandUtility.SetLayer(battler.gameObject, LayerMask.NameToLayer("Default"));
        battler.Anim.Play("BATTLESTANCE");
        _SkillCamera.transform.SetParent(_CameraAnim.camTrans);
        _SkillCamera.gameObject.SetActive(false);
        _CameraAnim.onSkill = false;
        _CameraAnim.MainCamera.enabled = true;

        if (dofTrans != null)
        {
            dofTrans.SetParent(_CameraAnim.camTrans);
            _CameraAnim.resetDOF();
        }
        //		_camera.cullingMask ^= (1 << LayerMask.NameToLayer ("Default"));
        //		_CameraAnim.SetDOF (9f);
        for (int i = 0; i < targets.Count; i++)
        {// Battler bat in _Targets) {
            if (targets[i].BattlerState != BattlerState.Dead)
            {
                targets[i].transform.SetParent(Battle.Instance.Controller.transform);
                targets[i].transform.position = targets[i].MyPlane.transform.position;
                LegrandUtility.SetLayer(targets[i].gameObject, LayerMask.NameToLayer("Default"));
                battler.SkillOffenseAttack(targets[i], command.commandData.CommandValue);
                targets[i].HitState();
            }
        }

        for (int i = 0; battler.WeaponFX.Count > 0 && i < battler.WeaponFX.Count; i++)
        {
            battler.WeaponFX[i].DeactiveParticle();
        }
        GameObject.Destroy(skillController.gameObject);
        onDone = true;
        fader.StartScene();

    }

    private Battler FindAnothertarget()
    {
        List<Battler> listTarget = new List<Battler>();
        if (battler.BattlerType == BattlerType.Player)
            listTarget = Battle.Instance.encounterList;
        else
            listTarget = Battle.Instance.charList;

        for (int i = 0; i < listTarget.Count; i++)
        {
            if (listTarget[i].BattlerState != BattlerState.Dead)
                TempTarget.Add(listTarget[i]);
        }
        if (TempTarget.Count > 0)
            return TempTarget[0];
        else
            return null;
    }

    public override IEnumerator Update()
    {
        while (onUpdate)
        {
            castSkill();
            if (onDone)
            {
                yield return new WaitForSeconds(1f);
                sendDoneAct(true);
            }

            yield return null;
        }
    }

    void HitTarget (Battler target){
        if (target.BattlerState != BattlerState.Guard && (target.Character as Encounter).MonsterSize != EncounterSize.Massive)
            target.BattlerState = BattlerState.Hit;
    }
}
