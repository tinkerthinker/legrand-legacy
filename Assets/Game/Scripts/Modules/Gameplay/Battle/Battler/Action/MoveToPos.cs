﻿using UnityEngine;
using System.Collections;

public class MoveToPos : BaseAction{
	Battler battler;
	Transform trans;
	Transform target;
	BattlePlane targetPlane;
	Vector3 oriPos;
	Quaternion oriRot;
	float range;
	float speed;

	public override void initAction (BattleCommand com, float speed)
	{
		base.initAction (com, speed);
		battler = com.attacker;
		this.speed = speed;
		this.oriPos = oriPos;
		this.oriRot = oriRot;
		trans = com.attacker.transform;
		target = com.targetPos.transform;
		targetPlane = com.targetPos;
	}
	private void move () {
		onExecuted = true;
		if(battler.BattlerState != BattlerState.Move)
			battler.BattlerState = BattlerState.Move;

		float distance = Vector3.Distance (trans.position, target.position);
	
		if (distance < 0.1f) {
			Battle.Instance.playerPlanes.Find(x => x.planePos.Equals(battler.BattlePosition)).placeAble = true;
			battler.BattlePosition = targetPlane.planePos;

			trans.position = target.position;
			trans.rotation = oriRot;
			battler.MyPlane = targetPlane;
			sendDoneAct (true);
		} else {
			trans.position = Vector3.MoveTowards (trans.position, target.position, Time.deltaTime * speed);
			trans.LookAt (target);
		}
	}

	public override IEnumerator Update ()
	{
		while (onUpdate) {
			move ();
			yield return null;
		}
	}
}
