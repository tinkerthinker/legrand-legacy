using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using Legrand.core;

public enum BattlerState{
	Idle, 
	Move,
	Attack,
	Skill,
	Combo,
	UseItem,
	Casting,
	Magic,
	Guard,
	Hit,
	Dead,
	Status,
	Revived,
	Winning,
	Channeling,
	Flee,
	Preview,
	Combination
};
public enum BattlerType{
	Player,
	Enemy,
};
public enum AttackType{
	Melee,
	Range,
	Caster,
};
public enum BattleStateCondition{
	None,
	ItemBuff,
	ItemDebuff,
	SkillOffensive,
	SkillDefensive,
	SkillCombination,
	GuardHit,
	GuardEnd,
	EnemySkillFirst,
	EnemySkillSecond
};

public class Battler : MonoBehaviour {
	public delegate void OnState(BattlerState state);
	public event OnState OnStateChange;

	public delegate void LockState(bool state);
	public event LockState OnLockState;

	[SerializeField]
	protected BattlerState battlerState;
	public BattlerState BattlerState {
		get {return battlerState;}
		set {setState(value);}
	}

	protected BattleStateCondition _BattleStateCondition; 
	public BattleStateCondition BattleStateCondition{
		get {return _BattleStateCondition;}
		set {_BattleStateCondition = value;}
	}

	protected Character character;
	public Character Character{
		get { return character;}
		set { character = value;}
	}

	protected BattlerType battlerType;
	public BattlerType BattlerType {
		get {return battlerType;}
		set {battlerType = value;}
	}

	protected AttackType attackType;
	public AttackType AttackType {
		get {return attackType;}
		set {attackType = value;}
	}

	[SerializeField]
	protected Vector2 battlePosition;
	public Vector2 BattlePosition {
		get {return battlePosition;}
		set {battlePosition = value;}
	}

	protected int battleID;
	public int BattleID {
		get {return battleID;}
		set {battleID = value;}
	}

	protected int battleTurn;
	public int BattleTurn {
		get {return battleTurn;}
		set {battleTurn = value;}
	}

	protected int reactSpeed;
	public int ReactSpeed {
		get {return reactSpeed;}
		set {reactSpeed = value;}
	}

	protected List<Battler> targeted = new List<Battler>();
	public List<Battler> Targeted {
		get {return targeted;}
		set {targeted = value;}
	}

	protected StatusBar healthBar;
	public StatusBar HealthBar {
		get {return healthBar;}
		set {healthBar = value;}
	}
	[SerializeField]
	protected TimedHitController timedHit;
	public TimedHitController TimedHit {
		get {return timedHit;}
		set {timedHit = value;}
	}

	protected int gaugeCharge = 0;
	public int GaugeCharge {
		get{ return gaugeCharge;}
		set{ gaugeCharge = value ; UpdateGauge(0); }
	}
    public void ChangeGauge(int value)
    {
        UpdateGauge(value);
    }

	protected int maxGauge = 100;

	protected BattlerActionController actionController;
	public BattlerActionController ActionController {
		get {return actionController;}
	}
	protected BattlerStateController stateController;
	public BattlerStateController StateController {
		get {return stateController;}
	}
	[SerializeField]
	protected BattlerSkillController skillController;
	public BattlerSkillController SkillController {
		get {return skillController;}
	}
	[SerializeField]
	protected List <BattleSkillCombination> _SkillCombination;
	public List <BattleSkillCombination> SkillCombination {
		get {return _SkillCombination;}
	}
	protected GameObject skillContainer;
	protected GameObject _SkillContainerCombo;

	protected Health health;
	public Health Health {
		get {return health;}
	}
    protected List<WeaponTrail> weaponFX = new List<WeaponTrail>();
    public List<WeaponTrail> WeaponFX {
		get {return weaponFX;}
	}
	protected Battle battleData;

	protected Transform battlerTrans;
	public Transform BattlerTrans {
		get {return battlerTrans;}
	}

	[SerializeField]
	protected BuffEvent _ItemEvent;
	public BuffEvent ItemEvent{
		get {return _ItemEvent;}
		set{_ItemEvent = value;}
	}

	protected BattleHelper _BattleHelper;
	public BattleHelper BattleHelper
	{
		get { return _BattleHelper;}
		set { _BattleHelper = value;}
	}

	public Transform HitArea{
		get {return BattleHelper.HitArea;}
		set {BattleHelper.HitArea = value;}
	}

	public Transform HitRange{
		get {return BattleHelper.HitRange;}
		set{BattleHelper.HitRange = value;}
	}
	public Transform InfoHelper{
		get {return BattleHelper.InfoHelper;}
		set{BattleHelper.InfoHelper = value;}
	}

	public Transform CameraPointingLocation{
		get {return BattleHelper.PointerFocus;}
		set{BattleHelper.PointerFocus = value;}
	}

	public Vector2 SelectorSize{
		get {return BattleHelper.SelectorSize;}
		set{BattleHelper.SelectorSize = value;}
	}

	public int FleeChance;
	public bool isFlee;

	protected Material[] _BattlerShaders;

//	private Sequence mySequence;
	protected Tween [] _TweenShader;

	#region Editor
	public BattlerInfo BattlerInfo;
	#endregion

	#region Buff Handler
	public bool IsSkip = false;
    public bool isBlinkBuff = false;

	[SerializeField]
    protected List<Buff> _BuffDebuffStat = new List<Buff>();
	public List<Buff> BuffDebuffStat{
		get {return _BuffDebuffStat;}
		set {_BuffDebuffStat = value;}
	}

    private Dictionary<string, Sprite> _BuffDebuffSprite;
    public Dictionary<string, Sprite> BuffDebuffSprite
    {
        get { return _BuffDebuffSprite; }
        set { _BuffDebuffSprite = value; }
    }

    [SerializeField]
    private List <BuffImage> _BuffSprite = new List<BuffImage>();
    public List<BuffImage> BuffSprite
    {
        get { return _BuffSprite; }
        set { _BuffSprite = value; }
    }

	private bool _NoMagicCost = false;
	public bool NoMagicCost
	{
		get{ return _NoMagicCost;}
		set{ _NoMagicCost = value;}
	}
    protected bool _isConfuse = false;
    public bool isConfuse {
        get{ return _isConfuse;}
        set{ _isConfuse = value;}
    }
	protected float _ConfuseChance;
	public float ConfuseChance
	{
		get { return _ConfuseChance; }
		set { _ConfuseChance = value; }
	}

	protected float _SkipChance;
	public float SkipChance
	{
		get { return _SkipChance;}
		set { _SkipChance = value;}
	}
	public List<CommandTypes> NegatedAction;

	protected float _ReflectPercentage;
	public float ReflectPercentage
	{
		get {return _ReflectPercentage;}
		set {_ReflectPercentage = value;}
	}
    protected float _APDrain;
    public float APDrain
    {
        get {return _APDrain;}
        set {_APDrain = value;}
    }
    protected float _HPDrain;
    public float HPDrain
    {
        get {return _HPDrain;}
        set {_HPDrain = value;}
    }
	protected float _DrainPercentage;
	public float DrainPercentage
	{
		get {return _DrainPercentage;}
		set {_DrainPercentage = value;}
	}

    protected Dictionary<Attribute, float> _BattlerAttribute;
    public Dictionary<Attribute, float> BattlerAttribute
    {
        get { return _BattlerAttribute; }
    }

    protected Dictionary<Attribute, float> _PrimaryBuff;
	public Dictionary<Attribute, float> PrimaryBuff
	{
		get {return _PrimaryBuff;}
	}
	protected Dictionary<Attribute, float> _SecondaryBuff;
	public Dictionary<Attribute, float> SecondaryBuff
	{
		get {return _SecondaryBuff;}
	}
	#endregion
	[SerializeField]
	protected BattlePlane _MyPlane;
	public BattlePlane MyPlane {
		get{return _MyPlane;}
		set{_MyPlane = value;}
	}
	[HideInInspector]
	public bool isCasting = false;
	[HideInInspector]
	public bool isTimeHit = false;
	[HideInInspector]
	public bool isInterrupt = false;
	[HideInInspector]
	public bool inReserve = false;
	[HideInInspector]
	public bool onChangeCommand = false;
	[HideInInspector]
	public bool onBattle = false;
	[HideInInspector]
    public bool isCombination = false;
    [HideInInspector]
    public bool OnDamage = false;
    private bool _StopBlink = false;

    [HideInInspector]
    public BattlePlane TargetPosition;
	
//	public List<AttackDataTemp> AttackData = new List<AttackDataTemp>();
	public delegate void ChangeEvent();
	public event ChangeEvent OnDoneChange;

	public delegate void ActionEvent (Battler battler);
	public event ActionEvent OnShiftAction;
	public event ActionEvent OnDoneAction;
	public event ActionEvent OnDead;
    public event ActionEvent OnHit;


	public Animator Anim;
    [HideInInspector]
    public GameObject ParticleGuard;

//    [HideInInspector]
//	public GameObject InfoBarEncounter;

    private int _BonusEXP;
    public int BonusExp
    {
        get { return _BonusEXP; }
    }

    List<SkinnedMeshRenderer> _SkinRenderer;
	List <MeshRenderer> _MeshRenderer;

	protected GameObject _HitParticle;

	[HideInInspector]
	public ActionPopUp ActionBar;

	protected int popUpType = 0;

    Queue<BattleTextPopUpData> PopUpQueue;

    [HideInInspector]
    public BuffBar _BuffBar;

    public int PrefectTimeHit = 0, GoodTimeHit = 0, MissTimeHit = 0;
    private Tween _buffTween;

    public List <Vector2Integer> CommandImportants;
    private Color highLightColor;

	public void initBattler () {
		battleData = Battle.Instance;
		battlerTrans = this.transform;

        _BuffSprite = new List<BuffImage>();
		_SkinRenderer = new List<SkinnedMeshRenderer>();
		_MeshRenderer = new List<MeshRenderer>();
        PopUpQueue = new Queue<BattleTextPopUpData>();
        StartCoroutine(PopUpQueueChecker());

        // Get all material from battler
        //		SkinnedMeshRenderer[] _SkinRenderer = LegrandUtility.GetComponentsInChildren<SkinnedMeshRenderer>(this.gameObject);
        //		MeshRenderer [] _MeshRender = LegrandUtility.GetComponentsInChildren<MeshRenderer>(this.gameObject);

        foreach (Transform child in this.GetComponentsInChildren<Transform>()) {
			//Add skin renderer body, head, dll
			SkinnedMeshRenderer skinMeshRender = child.gameObject.GetComponent<SkinnedMeshRenderer> ();
			if (skinMeshRender!=null)
				_SkinRenderer.Add(skinMeshRender);
			//add mesh renderer sword
			MeshRenderer meshRender = child.gameObject.GetComponent<MeshRenderer> ();
			if (meshRender!=null)
				_MeshRenderer.Add(meshRender);
		}
		//_BattlerShader = this.gameObject.GetComponentInChildren<SkinnedMeshRenderer> ().material;
		List<Material> materials = new List<Material> ();
        for (int i=0;i<_SkinRenderer.Count;i++)//each (SkinnedMeshRenderer renderer in _SkinRenderer) 
		{
            materials.AddRange (_SkinRenderer[i].materials);
		}
        for (int i=0;i<_MeshRenderer.Count;i++) //each (MeshRenderer renderer in _MeshRenderer) 
		{
            materials.AddRange (_MeshRenderer[i].materials);
		}

		_BattlerShaders = materials.ToArray ();

		setState (BattlerState.Idle);
		health = character.GetHealth ();

		if (!onBattle) {
			healthAddListener ();
			stateController = this.gameObject.AddComponent<BattlerStateController> ();
			actionController = this.gameObject.AddComponent<BattlerActionController> ();

			foreach (Transform child in this.GetComponentsInChildren<Transform>()) {
                WeaponTrail e = child.gameObject.GetComponent<WeaponTrail> ();
				if (e != null)
					weaponFX.Add (e);
			}
		}

		_BuffDebuffStat = new List<Buff>();
		NegatedAction = new List<CommandTypes>();
		_PrimaryBuff = new Dictionary<Attribute, float>();
		_SecondaryBuff = new Dictionary<Attribute, float>();

        RecalculateAttribute();

		BossOnInit ();

        if (character._Type == 0 && !onBattle) 
        {
            GaugeCharge = (character as MainCharacter).Gauge; //100
            EventManager.Instance.AddListener<FleeEvent>(RunAway);
//          Invoke ("initSkill", 1f);
            initSkill();
            onBattle = true;
        }
	}
	private void healthAddListener(){
		health.OnIncreased 	+= HandleOnIncreased;
		health.OnReduced 	+= HandleOnReduced;
		health.OnZero 		+= HandleOnZero;
		health.OnDead 		+= HandleOnDead;
		health.OnRevive 	+= HandleOnRevive;
	}
	public void healthRemoveListener(){
        if (health != null && health.IsOnIncreased())
            health.OnIncreased 	-= HandleOnIncreased;
        if (health != null && health.IsOnReduced())
            health.OnReduced 	-= HandleOnReduced;
        if (health != null && health.IsOnZero())
            health.OnZero 		-= HandleOnZero;
        if (health != null && health.IsOnDead())
            health.OnDead 		-= HandleOnDead;
        if (health != null && health.IsOnRevive())
            health.OnRevive 	-= HandleOnRevive;
	}

	public void initSkill(){
//		if (this.character._Type == (int)Character.CharacterType.Main) {
//			MainCharacter mc =  this.character as MainCharacter;
//
//            if (mc.BattleOffensiveSkill.Name != "" && mc.BattleOffensiveSkill.Name != "None") {
//				skillContainer = BattlePoolingSystem.Instance.InstantiateAPS (mc.BattleOffensiveSkill.Name, new Vector3 (0, 0, 0), new Quaternion (0, 0, 0, 0));
//				skillController = skillContainer.GetComponent<BattlerSkillController> ();
//				skillController.init ();
//			}
//
//			if (this.battlerType == BattlerType.Player && mc.BattleComboSkill.Count > 0) {
//				_SkillCombination = new List<BattleSkillCombination> ();
//				BattleSkillCombination bc = new BattleSkillCombination ();
//                for (int i = 0;i < mc.BattleComboSkill.Count; i++){
//                    CombinationSkill cs = mc.BattleComboSkill[i];
//                    if (cs.Name != "None")
//                    {
//                        _SkillContainerCombo = BattlePoolingSystem.Instance.InstantiateAPS(cs.Name, new Vector3(0, 0, 0), new Quaternion(0, 0, 0, 0));
//                        bc = _SkillContainerCombo.GetComponent<BattleSkillCombination>();
//                        bc.IdPartner = cs.PartnerID;
//                        _SkillCombination.Add(bc);
//                        bc.init();
//                    }
//				}
//			}
//		}
	}


	public void initGaugeCharge()
	{

	}

    #region Battler Attribute
    public void RecalculateAttribute()
    {
        _BattlerAttribute = new Dictionary<Attribute, float>();
        _BattlerAttribute.Add(Attribute.PhysicalAttack, Formula.CalculateTotalDamage(this.character, this._PrimaryBuff, null));
        _BattlerAttribute.Add(Attribute.PhysicalDefense, Formula.CalculatePhysicalDefense(this.character, this._PrimaryBuff, null));
        _BattlerAttribute.Add(Attribute.MagicAttack, Formula.CalculateINTMagicDamage(this.character, this._PrimaryBuff, null));
        _BattlerAttribute.Add(Attribute.CriticalRate, Formula.GetCritRate(this.character, this._PrimaryBuff, null));
        _BattlerAttribute.Add(Attribute.EvasionRate, Formula.GetEvaRate(this.character, this._PrimaryBuff, null));
        _BattlerAttribute.Add(Attribute.MagicDefense, Formula.CalculateMagicDefense(this.character, this._PrimaryBuff, null));
        _BattlerAttribute.Add(Attribute.HitRate, Formula.GetAccRate(this.character, this._PrimaryBuff, null));
    }
    #endregion

    #region Health Handler
    void HandleOnIncreased (float percent) {
		if(healthBar != null) healthBar.UpdateHP (health.Value,health.MaxValue);
	}
	void HandleOnReduced (float percent){
		//arkwora AI
		if (healthBar != null) {
			healthBar.UpdateHP (health.Value, health.MaxValue);
		}
		if (this.battlerState != BattlerState.Magic){
			if (this.character._ID != "CB-01" || this.character._ID != "CB-02"){
                HitState ();
			}else { Debug.Log(this); }
		}
	}
	void HandleOnZero (){
		if (healthBar != null) healthBar.UpdateHP (health.Value,health.MaxValue);
        if (OnDead != null && this.character._ID != "CB-01") OnDead (this);
		setState (BattlerState.Dead);

//        if (character._ID != "CB-01") // (battlerType == BattlerType.Enemy && (character as Encounter).enemyType == EnemyType.BossUnique)
//        {
            CheckBossOnDeadCinematic();
//        }
	}
	void HandleOnRevive (GameObject g){
		if(healthBar != null) healthBar.UpdateHP (health.Value,health.MaxValue);
//		setState (BattlerState.Revived);
	}
	void HandleOnDead (GameObject g){
//		healthRemoveListener ();
        for (int i=0; i<weaponFX.Count;i++){// each (PKFxFX e in weaponFX) {
            weaponFX[i].DeactiveParticle();
		}

        ParticleSystem[] particles = this.gameObject.GetComponentsInChildren<ParticleSystem>();
        for(int i=0; i < particles.Length; i++)
        {
            particles[i].gameObject.SetActive(false);
        }
        if (ParticleGuard != null)
            ParticleGuard.DestroyBattleAPS();
        
        health.IsDead = true;
        resetAll();
        IsSkip = false;
        _ConfuseChance = 0;
        _SkipChance = 0;
        isConfuse = false;
	}
	public void UpdateGauge(int point){
        gaugeCharge += point;

        if (gaugeCharge < 0) gaugeCharge = 0;
		if (gaugeCharge > maxGauge) gaugeCharge = maxGauge;
		
        (character as MainCharacter).Gauge = gaugeCharge;
		healthBar.UpdateGauge(gaugeCharge, maxGauge);
	}
	#endregion

	#region State Setter
	void setState(BattlerState nextState){
		battlerState = nextState;
		if(OnStateChange != null) OnStateChange(battlerState);
	}

	public void RecheckState()
	{
		battlerState = stateController.RecheckAnimation ();
	}

    public bool CheckState(string name)
    {
        return stateController.CheckAnimation(name);
    }

    public void SetLockedState(bool state)
	{
		if (OnLockState != null)
			OnLockState (state);
	}
	#endregion

	#region Public Method
    virtual public void HitState()
	{
		if (OnHit != null)
            OnHit (this);
	}
	public void ShiftAction()
	{
		if (OnShiftAction != null)
			OnShiftAction (this);
	}
	public void DoneAction()
	{
		if (OnDoneAction != null)
			OnDoneAction (this);
	}
	public void UseItem(int itemID, List<Battler> targets)
	{
//		List<int> targetIDs = targets.ConvertAll (x => x.character._ID).ToList();
//		List<int> battlerIDs = targets.ConvertAll (x => x.battleID).ToList();

		switch (battlerType){
		case BattlerType.Player:
			MainCharacter mc = (character as MainCharacter);
			bool used = mc.UseItem (itemID, targets, this);
			break;
		case BattlerType.Enemy:
			Encounter enc = (character as Encounter);		
			bool use = enc.UseItem (itemID, targets, this);
			break;
		}
	}

    public float GetAttribute(Attribute attrib)
    {
        float totalAttributeValue = (float)this.character.GetAttribute(attrib);
        if (PrimaryBuff != null && PrimaryBuff.ContainsKey(attrib)) totalAttributeValue = Mathf.Round(totalAttributeValue + (totalAttributeValue * PrimaryBuff[attrib] / 100f));
        return totalAttributeValue;
    }

    public bool RollEvadeFrom(Battler attacker, float bonusHitRate, float evaChance)
	{
		float roll = rollRandom();
		float attackerHitRate = this.RollHit(attacker, bonusHitRate, evaChance);


		return (roll >= attackerHitRate) ? true : false;
	}

    public bool RollEvademagic(Battler attacker, float magicAcc,float bonusHitRate, float evaChance)
    {
        float roll = rollRandom();
        float magicHitRate = this.RollHitMagic(attacker, magicAcc, bonusHitRate, evaChance);


        return (roll >= magicHitRate) ? true : false;
    }

    public bool RollCrit(Battler att, Battler def)
	{
		float roll = rollRandom ();
		float critRate = (float) Formula.GetTotalCritRate (att._BattlerAttribute[Attribute.CriticalRate], att._SecondaryBuff);
		
		return (roll <= critRate)? true : false;
	}
	public float RollHit(Battler attacker, float bonusHitRate, float bonusEva)
	{
		return Formula.GetHitRate (attacker.character, this.character, bonusHitRate,bonusEva, attacker._BattlerAttribute[Attribute.HitRate], attacker.SecondaryBuff, _BattlerAttribute[Attribute.EvasionRate], SecondaryBuff);
	}
    public float RollHitMagic(Battler attacker, float magicAcc,float bonusHitRate, float bonusEva)
    {
        return Formula.GetMagicHitRate(magicAcc, bonusHitRate, bonusEva, attacker._BattlerAttribute[Attribute.HitRate], attacker.SecondaryBuff, _BattlerAttribute[Attribute.EvasionRate], SecondaryBuff);
    }
    public float PhysicDamage(Battler target, float attackerModifer = 1f, float defenderModifier = 1f)
	{
        float finalPhysicalAttack = this._BattlerAttribute[Attribute.PhysicalAttack] * attackerModifer;
        float finalPhysicalDefend = this._BattlerAttribute[Attribute.PhysicalDefense] * defenderModifier;

        float damage = Formula.GetPhyDmg (finalPhysicalAttack, this.SecondaryBuff, finalPhysicalDefend, target.SecondaryBuff);
		return Mathf.Round (damage);
	}
    /*
	public float PhysicDamage(Battler target, out string log)
	{ 
		float damage = Formula.GetPhyDmg (this.character, target.character, this.PrimaryBuff, this.SecondaryBuff, target.PrimaryBuff, target.SecondaryBuff, out log);
		return Mathf.Ceil (damage);
	}
    */
	public float MagicDamage(Battler target, Magic magic)
	{
		float damage = Formula.GetMagDmg (this._BattlerAttribute[Attribute.MagicAttack], this._SecondaryBuff, target._BattlerAttribute[Attribute.MagicDefense], target._SecondaryBuff, magic);
		return Mathf.Ceil (damage);
	}
	public float SkillDamage(Battler target)
	{
		float damage = PhysicDamage (target);
//		if (this.BattlerType == BattlerType.Enemy){
//			Encounter enc = (this.character as Encounter);
//			return Mathf.Ceil (damage * 
//		else
		return Mathf.Ceil (damage * 1.5f);
	}
	public TargetType GetMagicTarget(int MagicPresetID)
	{
		Magic magic = character.GetMagic(MagicPresetID);
		return magic.MagicTargetType;
	}
    public Magic GetMagic(int MagicPresetID)
    {
        return character.GetMagic(MagicPresetID);
    }
    public Item GetItem(int ItemID){
		return character.GetItem (ItemID);
	}

    public void CalculateInterrupt (Battler encounter){
        if (this.TimedHit != null)
        {
            int chance = (int)Formula.CalculateInterrupt(this, encounter);
            int rand = (int)LegrandUtility.Random(0f, 100f);
            if(chance>0)
            {
                chance = Mathf.Abs(chance);
                if (rand < chance)
                {
                    this.isInterrupt = false;
                    encounter.isInterrupt = true;
                }
                else
                {
                    this.isInterrupt = false;
                    encounter.isInterrupt = false;
                }
            }
            else if(chance<0)
            {
                chance = Mathf.Abs(chance);
                if (rand < chance)
                {
                    this.isInterrupt = true;
                    encounter.isInterrupt = false;
                }
                else
                {
                    this.isInterrupt = false;
                    encounter.isInterrupt = false;
                }
            }
        }
    }

	public void Attack(Battler target, Battler attacker){
		//attack value
		bool critical = false;
		float bonusDamage = Formula.GoodDamage / 100f;
        float baseDamage = 0f;
		//def value
		float bonusEvaRate = 0f;
        float bonusHitRate = Formula.GoodHitRate;
        float timedDefDamage = 100f;
        
		//target is player
		if (target.TimedHit != null) {

            if (target.timedHit.DefendHitState == TimedHitState.None)
                target.timedHit.DefendHitState = target.timedHit.AttackHitState;

			if(target.TimedHit.DefendHitState == TimedHitState.Perfect){
				timedDefDamage = Formula.TimedDefPerfectDamage;
				bonusEvaRate = Formula.PerfectEvaRate;
            }
            else if (target.TimedHit.DefendHitState == TimedHitState.Good)
                bonusEvaRate = Formula.GoodEvaRate;
            else if(target.TimedHit.DefendHitState == TimedHitState.Miss)
				timedDefDamage = Formula.TimedDefMissDamage;
		}

        if (attacker.battlerType == BattlerType.Player)
        {
            MainCharacter mc = null;
            int FillGauge = 0;

            mc = attacker.character as MainCharacter;
            FillGauge = FillGauge + mc.GetGaugeSpeed(GaugeType.BaseAttack);

            if (attacker.TimedHit.AttackHitState == TimedHitState.Perfect)
            {
                if (mc != null)
                    FillGauge = FillGauge + mc.GetGaugeSpeed(GaugeType.PerfectAttack);
            }
            else if (attacker.TimedHit.AttackHitState == TimedHitState.Good)
            {
                if (mc != null)
                    FillGauge = FillGauge + mc.GetGaugeSpeed(GaugeType.GoodAttack);
            }
            else
            {
                bonusHitRate = Formula.BadHitRate;
            }
            attacker.ChangeGauge(FillGauge);
        }

        bool evade = false;
        //attacker is player so count quick event

        critical = RollCrit(attacker, target);
        evade = target.RollEvadeFrom(attacker , bonusHitRate, bonusEvaRate);
        if (attacker.TimedHit != null)
        {
            if (attacker.TimedHit.AttackHitState == TimedHitState.Perfect)
            {
                evade = false;
                critical = true;
            }
            else if (attacker.TimedHit.AttackHitState == TimedHitState.Miss)
                bonusDamage = Formula.BadDamage / 100f;
        }

        // Check if target evade or not
		if (!evade) {
            // Calculate if critical or not
            // Add bonus from time hit

            //check bonus damage by position
            float damageModifier = 1f;
            float defenseModifier = 1f;
            if (attacker.battlerType == BattlerType.Player && attacker.battlePosition.y == 1)
                damageModifier = 1.1f;
            if (target.battlerType == BattlerType.Player && target.battlePosition.y == 0)
                defenseModifier = 1.1f;


            baseDamage = attacker.PhysicDamage (target, damageModifier, defenseModifier);
            baseDamage = Formula.WeaponTriangle(attacker.character, target.character, baseDamage);
			// Add critical damage
			if (critical) {
				baseDamage = Formula.GetCriticalDmg (baseDamage, attacker.SecondaryBuff);
				battleData.Controller.cameraShake (true);
			}

            baseDamage = baseDamage * bonusDamage;
            baseDamage = (timedDefDamage / 100f) * baseDamage;

			if (target.character.IsAntiPhysical)
                baseDamage = Formula.CalculateArmoredPenalty(baseDamage);
			Damage damage = new Damage (critical, baseDamage);
			attacker.ItemEvent = BuffEvent.ON_ATTACK;
            
            target.ReceiveDamage (damage, attacker, false);
            
			target.ItemEvent = BuffEvent.ON_HITTED;
            target.OnDamage = true;
            target.HitState();
            target.actionController.OnCheckInterrupt(this);
            target.OnHitShader();
		} else {
            attacker.PopUp("Miss", (int)attacker.BattlerType);
		}
	}
    public void MagicAttack(int MagicID, List <Battler> targets){
        float bonusDamage = 1f;
        float bonusHitRate = Formula.GoodHitRate;
        
        Magic magic = character.GetMagic (MagicID);
        bool isCritical = (LegrandUtility.Random(0f, 100f) <= magic.CritPercentage) ? true : false;
        TimedHitState attackerHitState = TimedHitState.Good;
        if (this.TimedHit != null)
            attackerHitState = this.TimedHit.AttackHitState;
        
        if (this.battlerType == BattlerType.Player)
        {
            MainCharacter mc = null;
            int FillGauge = 0;

            mc = this.character as MainCharacter;
            FillGauge = FillGauge + mc.GetGaugeSpeed(GaugeType.BaseMagic);

            if (attackerHitState == TimedHitState.Perfect)
            {
                if (mc != null)
                    FillGauge = FillGauge + mc.GetGaugeSpeed(GaugeType.PerfectMagic);
            }
            else if (attackerHitState == TimedHitState.Good)
            {
                if (mc != null)
                    FillGauge = FillGauge + mc.GetGaugeSpeed(GaugeType.GoodMagic);
            }

            this.ChangeGauge(FillGauge);
        }
        //attacker is player so count quick event
        if (this.TimedHit != null)
        {
            if (this.TimedHit.AttackHitState == TimedHitState.Perfect)
                isCritical = true;
            else if (this.TimedHit.AttackHitState == TimedHitState.Good)
            {
                bonusDamage = Formula.GoodDamage / 100f;
            }
            else
            {
                bonusDamage = Formula.BadDamage / 100f;
                bonusHitRate = Formula.BadHitRate;
            }
        }
        else if (attackerHitState == TimedHitState.Good)
            bonusDamage = Formula.GoodDamage / 100f;
        else
        {
            bonusDamage = Formula.BadDamage / 100f;
            bonusHitRate = Formula.BadHitRate;
        }
        float attackerMagicAttack = Formula.GetMagAttack(this._BattlerAttribute[Attribute.MagicAttack], this._SecondaryBuff);

        //check bonus damage by position
        if (this.battlerType == BattlerType.Player && this.battlePosition.y == 1){
            attackerMagicAttack *= 1.1f;
        }

        float timedDefDamage = 100f;

        for (int i=0;i< targets.Count; i++)
        {
            if (targets[i].BattlerState != BattlerState.Dead)
            {
                float bonusEvaRate = 0f;
                if (targets[i].TimedHit != null)
                {
                    if (targets[i].timedHit.DefendHitState == TimedHitState.None)
                        targets[i].timedHit.DefendHitState = targets[i].timedHit.AttackHitState;
                    
                    if (targets[i].TimedHit.DefendHitState == TimedHitState.Perfect)
                        bonusEvaRate = Formula.PerfectEvaRate;
                    if (targets[i].TimedHit.DefendHitState == TimedHitState.Good)
                        bonusEvaRate = Formula.GoodEvaRate;
                }

                if (attackerHitState != TimedHitState.Perfect && targets[i].RollEvademagic(this, magic.HitPercentage, bonusHitRate, bonusEvaRate))
                {
                    targets[i].PopUp("Miss", (int)this.BattlerType);
                    continue;
                }
                Battler target = targets[i];
                float defenderMagicDefense = Formula.GetmagDef(target.BattlerAttribute[Attribute.MagicDefense], target.SecondaryBuff);
                if (this.battlerType == BattlerType.Player && this.battlePosition.y == 0)
                {
                    defenderMagicDefense *= 1.1f;
                }

                float finalDamage = Formula.GetMagDmg(attackerMagicAttack, defenderMagicDefense, magic);
                finalDamage = Formula.Elements (magic.Element, target.character, finalDamage);
                if (isCritical) finalDamage = Formula.GetCriticalDmg(finalDamage, this.SecondaryBuff);

                // Time Hit Bonus
                finalDamage = finalDamage * bonusDamage;
                //target is player
                if (target.TimedHit != null)
                    {
                    if (target.TimedHit.DefendHitState == TimedHitState.Perfect)
                    {
                        timedDefDamage = Formula.TimedDefPerfectDamage;
                    }
                    else if (target.TimedHit.DefendHitState == TimedHitState.Miss)
                    {
                        timedDefDamage = Formula.TimedDefMissDamage;
                    }
                }


                finalDamage = (timedDefDamage / 100f) * finalDamage;
                // Finally check if monster is anti magic.
                if (target.character.IsAntiMagic)
                    finalDamage = Formula.CalculateAntiMagicPenalty(finalDamage);
                Damage magicDamage = new Damage(false , finalDamage);

                target.ReceiveDamage(magicDamage, this, false);
                target.OnDamage = true;
                target.HitState();
                target.actionController.OnCheckInterrupt(this);
                target.OnHitShader();
                for(int j=0;j<magic.BuffEffect.Count;j++)
                    ItemController._Instance.SetMagicSkillDebuff(target,magic.BuffEffect[j].BuffEffect, magic.BuffEffect[j].Chance);
            }
		}
    }
	public void SkillOffenseAttack(Battler target, int idSkill){
		float bonusDamage = 1f;
        float baseDamage = 0f;

		OffensiveBattleSkill skill = this.character.GetOffensiveSkill(idSkill);

		string info = "";

		if (this.TimedHit != null) {
			if(this.TimedHit.AttackHitState == TimedHitState.Perfect){
				bonusDamage = Formula.PerfectDamage / 100f;
			}else if(this.TimedHit.AttackHitState == TimedHitState.Good){
				bonusDamage = Formula.GoodDamage / 100f;
			}else{
				bonusDamage = Formula.BadDamage / 100f;
			}
		}

        baseDamage = Formula.OffenseSkillDamage(skill,this,target);

        baseDamage = Mathf.Round(baseDamage * bonusDamage);
        Damage damage = new Damage (false, baseDamage);
		this.ItemEvent = BuffEvent.ON_ATTACK;

        target.ReceiveDamage (damage, this, true);
        for(int j=0;j<skill.BuffsChance.Count;j++)
            ItemController._Instance.SetMagicSkillDebuff(target,skill.BuffsChance[j].BuffEffect, skill.BuffsChance[j].Chance);
        
        target.ItemEvent = BuffEvent.ON_HITTED;
        target.OnHitShader();

		UpdateBattlerActionInfo (info);
	}

    IEnumerator PopUpQueueChecker()
    {
        while (true)
        {
            if (PopUpQueue.Count > 0)
            {
                PopUp(PopUpQueue.Dequeue());
                yield return new WaitForSeconds(0.4f);
            }
            else
                yield return null;
        }
    }

    private void PopUp(BattleTextPopUpData popUpData)
    {
        battlerTrans = this.gameObject.transform;
        GameObject popUp = BattlePoolingSystem.Instance.InstantiateAPS("DamagePopUp", battlerTrans.position, battlerTrans.rotation);
        if(popUpData.ClassType == 0)
            popUp.GetComponent<BattleTextPopUp>().SetPopUp(popUpData.Text, popUpData.Type, popUpData.Crit, battlerTrans);
        else
            popUp.GetComponent<BattleTextPopUp>().SetPopUp(popUpData.Text, popUpData.BatterType, popUpData.Crit, battlerTrans);
    }

    public void PopUp (string text, int type, bool critical = false){
        PopUpQueue.Enqueue(new BattleTextPopUpData(text,type, critical));
    }

    public void PopUp(string text, BattlerType battlerType, bool critical = false)
    {
        PopUpQueue.Enqueue(new BattleTextPopUpData(text, battlerType, critical));
    }

    public void InfoPopUp (GameObject poolingObj, Battler attacker, bool IsPhysical, bool IsMagic, Element magicElement){
        StopCoroutine("InfoBarBuff");
		InfoBar _InfoPopUp = poolingObj.GetComponent<InfoBar> ();
		if (this.BattlerType == BattlerType.Enemy) {
            _BuffBar = _InfoPopUp.BuffArea;
            _BuffBar.BuffImageList = new List<BuffImage>();
            _BuffBar.BuffImageList = _BuffSprite;
            StartCoroutine("InfoBarBuff");
            _InfoPopUp.Set(this, attacker, IsPhysical, IsMagic, magicElement);
            battlerTrans = this.gameObject.transform;
		}
	}
	#endregion


	private float rollRandom(){
		return LegrandUtility.Random(0f,100f);
	}
	#region Flee
	void RunAway(FleeEvent f)
	{
		if (this.BattlerState != BattlerState.Dead){
			transform.Rotate (0f, 180f, 0f);
			if(OnStateChange != null)
				OnStateChange (BattlerState.Flee);
			transform.DOMove(new Vector3(transform.position.x, transform.position.y, transform.position.z - 10f), 3f);
		}
	}
	#endregion

	#region Change Character
	public void SwitchCharacter (){
		inReserve = true;
		transform.Rotate (0f, 180f, 0f);

		if (OnStateChange != null)
			OnStateChange (BattlerState.Flee);
		transform.DOMove (new Vector3 (transform.position.x, transform.position.y, transform.position.z - 10f), 2.5f).OnComplete (FadeCharacter);

	}
	void FadeCharacter (){
		if (inReserve) {
			this.gameObject.SetActive (false);
		} else {
			if (OnDoneChange != null)
				OnDoneChange ();
		}

		Anim.CrossFade ("BATTLESTANCE", 0.5f);
	}
	public void ReserveOnBattle (Battler main){
		this.gameObject.SetActive (true);
		transform.eulerAngles = main.transform.eulerAngles;
		inReserve = false;
		Transform resvTrans = this.transform;
		Transform mainTrans = main.transform;
		Vector3 startPos = new Vector3 (mainTrans.position.x, mainTrans.position.y, mainTrans.position.z - 10f);

		resvTrans.position = startPos;
		Anim.CrossFade ("RUN",0.5f);
		transform.DOMove (mainTrans.position, 2.5f).OnComplete (FadeCharacter);
	}
	#endregion
	#region GetMagic
	public string GetMagicEffect(BattleCommand command, bool CastingEffect){
		Magic magic = new Magic ();
		if (this.BattlerType == BattlerType.Player) {
			MainCharacter mainChar = (character as MainCharacter);
			magic = mainChar.Magics.GetMagicPreset (command.commandData.CommandValue).MagicPreset;
		} else {
			Encounter enc = (character as Encounter);
			magic = enc.Magics.GetMagicPreset (command.commandData.CommandValue).MagicPreset;
		}

		if (CastingEffect)
			return magic.CastingMagicEffect;
		else
            return magic.Element.ToString();
	}
	#endregion
	#region checkBuff
	public bool IsActionNegated(CommandTypes executionCommand)
	// Check if wanna be executed action is negated
	{
        for (int i = 0; i < NegatedAction.Count; i++)// each(CommandTypes command in NegatedAction)
            if(executionCommand == NegatedAction[i]) return true;
		return false;
	}
	#endregion
	#region RecieveDamage
	
    virtual public void ReceiveDamage(Damage damage, Battler attacker, bool isSkill){
        float bonusDefense = 0;

		if (battlerState == BattlerState.Guard)
			damage.BaseDamage = damage.BaseDamage / 2f;

        if (!isSkill) //!attacker.CheckUniqueBehaviour() && 
            ItemController._Instance.ExecBuffOnHittedAndAttack(this, attacker, Mathf.RoundToInt(damage.BaseDamage));

        character.ReceiveDamage (damage.BaseDamage);
        this.PopUp(Mathf.RoundToInt(damage.BaseDamage).ToString(), attacker.BattlerType, damage.IsCritical);
	}
	#endregion

//	#region Collect Damage
//	public void GetDamage (float damage, Battler attacker, Battler target){
//		BattleController bc = (BattleController)GetComponentInParent(typeof(BattleController));
//		bc.CollectDamage(attacker.battleID, attacker, target.battleID, target, damage);
//	}
//	#endregion
	#region End Battle Handler

	public void ApplyExp(int bonusExperience){
		_BonusEXP = bonusExperience;
		(character as MainCharacter).Gauge = gaugeCharge;
	}

	public void RemoveController(){
		if (character._Type == 0) 
		{
			EventManager.Instance.RemoveListener<FleeEvent>(RunAway);
		}
		
		healthRemoveListener ();

		if (actionController != null)
			Destroy (actionController);
		if (stateController != null)
			Destroy (stateController);
		
		if (this != null && !inReserve)
			Destroy (this);
//		StartCoroutine (DesroyState());
	}
//	public void CollectingData (int idAttacker, Battler Attacker, int AttackerType,int IDTarget,
//	                            Battler Target,float ReceiveDamage){
//
//	}
	#endregion

	public void UpdateBattlerInfo()
	{
		if (BattlerInfo != null) 
		{
			BattlerInfo.AttributeInfoText = "";
			BattlerInfo.AttributeInfoText += character._Name + "\n";
			BattlerInfo.AttributeInfoText += this.battlePosition + "\n";
			BattlerInfo.AttributeInfoText += "STR\t:" + character.GetAttribute(Attribute.STR) + "\t\t";
			BattlerInfo.AttributeInfoText += "VIT\t:" + character.GetAttribute(Attribute.VIT) + "\t\t";
			BattlerInfo.AttributeInfoText += "INT\t:" + character.GetAttribute(Attribute.INT) + "\t\t";
			BattlerInfo.AttributeInfoText += "LUC\t:" + character.GetAttribute(Attribute.LUCK) + "\t\t";
			BattlerInfo.AttributeInfoText += "AGI\t:" + character.GetAttribute(Attribute.AGI) + "\n";
		}
	}
    public Battler GetAllyInFront (Battler bat, bool isCharacter){
        Battler ally = new Battler();
        if (battleData == null)
            battleData = Battle.Instance;
        if (bat.BattlePosition.y == 0)
        {
            Vector2 pos = new Vector2(bat.BattlePosition.x, 1);
            if (isCharacter)
                ally = battleData.charList.Find(x => x.BattlePosition.Equals(pos) && x.BattlerState != BattlerState.Dead);
            else
                ally = battleData.encounterList.Find(x => x.BattlePosition.Equals(pos) && x.BattlerState != BattlerState.Dead);
        }
        return ally;
    }
    public Battler GetAllyBehind (Battler bat, bool isCharacter){
        Battler ally = new Battler();
        if (battleData == null)
            battleData = Battle.Instance;
        if (bat.BattlePosition.y == 1)
        {
            Vector2 pos = new Vector2(bat.BattlePosition.x, 0);
            if (isCharacter)
                ally = battleData.charList.Find(x => x.BattlePosition.Equals(pos) && x.BattlerState != BattlerState.Dead);
            else
                ally = battleData.encounterList.Find(x => x.BattlePosition.Equals(pos) && x.BattlerState != BattlerState.Dead);
        }
        return ally;
    }
    public void ActivateBattler (bool Active){
        if (battlerState != BattlerState.Dead)
        {
            if (Active)
                transform.position = MyPlane.transform.position;
            else
                transform.position = new Vector3(MyPlane.transform.position.x, -30, MyPlane.transform.position.y);
        }
    }
	public void UpdateBattlerActionInfo(string info)
	{
		if (BattlerInfo != null) 
		{
			BattlerInfo.InfoText = "\n";
			BattlerInfo.InfoText += info;
		}
	}
    public void BattlerDeadShader (){
        for (int i = 0; i < _MeshRenderer.Count; i++)
            _MeshRenderer[i].shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;

        for (int i = 0; i < _SkinRenderer.Count; i++)
            _SkinRenderer[i].shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;

        for (int i = 0; _BattlerShaders.Length > 0 && i < _BattlerShaders.Length; i++)
        { // each (Material battleShader in _BattlerShaders) 
            if ((character as Encounter).enemyType != EnemyType.BossUnique)
            {
                _BattlerShaders[i].SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                _BattlerShaders[i].SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                _BattlerShaders[i].SetInt("_ZWrite", 0);
                _BattlerShaders[i].DisableKeyword("_ALPHATEST_ON");
                _BattlerShaders[i].DisableKeyword("_ALPHABLEND_ON");
                _BattlerShaders[i].EnableKeyword("_ALPHAPREMULTIPLY_ON");
                _BattlerShaders[i].renderQueue = 3000;
                _BattlerShaders[i].DOFloat(1, "_SliceAmount", 3f).SetDelay(1f);
            }
        }
	}
    public void OnSelected (){
		//		mySequence = DOTween.Sequence ();
        List <Tween> tweenList = new List<Tween>();
        for (int i = 0;_BattlerShaders.Length > 0 && i < _BattlerShaders.Length; i++)
        {
            tweenList.Add(_BattlerShaders[i].DOColor(Color.white, "_RimColor", 0.5f).SetLoops(-1, LoopType.Yoyo));
//            tweenList.Add(_BattlerShaders[i].DOColor(new Color (1,1,1,0.5f), "_OutlineColor", 0.5f).SetLoops(-1, LoopType.Yoyo));
//            if (battlerType == BattlerType.Player)
//            {
//                if (character._ID != "1")
//                    tweenList.Add(_BattlerShaders[i].DOFloat(0.8f, "_Outline", 0.5f).SetLoops(-1, LoopType.Yoyo));
//                else
//                    tweenList.Add(_BattlerShaders[i].DOFloat(0.1f, "_Outline", 0.5f).SetLoops(-1, LoopType.Yoyo));
//                    
//            }
//            else
//                tweenList.Add(_BattlerShaders[i].DOFloat(2, "_Outline", 0.5f).SetLoops(-1, LoopType.Yoyo));
                
        }
        _TweenShader = tweenList.ToArray ();
	}
    public void OnHitShader (){
        for (int i = 0;_BattlerShaders.Length > 0 && i < _BattlerShaders.Length; i++)
        {
            _BattlerShaders[i].DOFloat(1, "_RimPower", 0);
            _BattlerShaders[i].DOColor(Color.red, "_RimColor", 0.1f).SetLoops(5, LoopType.Yoyo).OnComplete(StopShaderSequence);
        }
    }
    void Update (){
    
    }
	public void ChangeRimColor(Color newColor, float rimPower = 3f)
	{
        for (int i=0;_BattlerShaders.Length > 0 && i<_BattlerShaders.Length;i++){
            _BattlerShaders[i].DOColor(newColor, "_RimColor", 0); 
            _BattlerShaders[i].DOFloat(1, "_RimPower", 0);
//            _BattlerShaders[i].DOColor(newColor, "_OutlineColor", 0); 
//            if (battlerType == BattlerType.Player)
//                _BattlerShaders[i].DOFloat(1.5f, "_Outline", 0f);
//            else
//                _BattlerShaders[i].DOFloat(2.5f, "_Outline", 0f);
		}
	}

	public void StopShaderSequence (){
//		mySequence.Kill();
        if (_TweenShader != null && _TweenShader.Length > 0) {
            for (int i=0;i<_TweenShader.Length;i++)
                _TweenShader[i].Kill ();
		}

        for (int i = 0; _BattlerShaders.Length > 0 && i < _BattlerShaders.Length; i++)
        {
            _BattlerShaders[i].DOFloat(3, "_RimPower", 0);
            _BattlerShaders[i].DOColor(Color.black, "_RimColor", 0);

//            _BattlerShaders[i].DOColor(new Color (0,0,0,0), "_OutlineColor", 0f);
//            _BattlerShaders[i].DOFloat(0, "_Outline", 0f);
        }
	}
	void OnDestroy (){

	}

    #region inheritanceBattler
	virtual public void BossOnInit(){}

	virtual public void BossBehaviour (){}

	virtual public void CheckBossOnEachTurn () {}

    virtual public void NormalUniqueBehaviour () {}

    virtual public void CheckBossOnDeadCinematic (){}

    virtual public bool CheckUniqueBehaviour(){ return false; }

    virtual public Transform GetPreviewHelper(){ return null; }
    #endregion

    public void AddBuffSprite(string Key, string point)
    {
        string searchingKey = Key.Trim(new char[2] { '-', '+' });
        Sprite sprite = LegrandBackend.Instance.GetSpriteData(Key);

        if (_BuffDebuffSprite == null) _BuffDebuffSprite = new Dictionary<string, Sprite>();


        bool exist = false;
        if (_BuffDebuffSprite.ContainsKey(searchingKey))
        {
            _BuffDebuffSprite[searchingKey] = sprite;
            exist = true;
        }
        else
        {
            _BuffDebuffSprite.Add(searchingKey, sprite);
            BuffImage bi = new BuffImage(searchingKey, sprite, point);
            _BuffSprite.Add(bi);
        }

        if (this.BattlerType == BattlerType.Player)
        {
            if (exist)
                _BuffBar.ReplaceBuffSprite(searchingKey, _BuffDebuffSprite[searchingKey]);
            else
            {
                _BuffBar.AddBuff(searchingKey, _BuffDebuffSprite[searchingKey], point);
            }
        }
    }
    public void DisableBuffBlink()
    {
        KillBuffTween();
        StopCoroutine("BlinkBuff");
        _StopBlink = true;
        if (_BuffBar != null)
            _BuffBar.GetComponent<CanvasGroup>().DOFade(0, 0f);
    }
    public void CheckBuffBlink(){
        DisableBuffBlink();
        StartCoroutine(BlinkBuff());
    }
    public IEnumerator BlinkBuff ()
    {
        yield return new WaitForSeconds(1);
        if (_BuffBar != null && _BuffBar.BuffImageList != null && _BuffBar.BuffImageList.Count > 0)
            _BuffBar.GetComponent<CanvasGroup>().DOFade(1, 0f);
        else
            if (_BuffBar != null)
                _BuffBar.GetComponent<CanvasGroup>().DOFade(0, 0f);

        if (_BuffSprite != null && _BuffSprite.Count > 1 && battlerState != BattlerState.Dead)
        {
            _StopBlink = false;
            while (_BuffBar.BuffImageList != null && _BuffBar.BuffImageList.Count > 1 && battlerState != BattlerState.Dead && !_StopBlink)
            {
                for (int i = 0; i < _BuffBar.BuffImageList.Count; i++)
                {
                    BuffImage buffObj = _BuffBar.BuffImageList[i];
                    _BuffBar.BuffImage.sprite = buffObj.Sprite;
//                    _BuffBar.NameBuff.text = SourceNameConverter (buffObj.SourceKey) + " " + buffObj.Point;
                    _BuffBar.TextImage.sprite = LegrandBackend.Instance.GetSpriteData(buffObj.SourceKey + "font");
                    KillBuffTween();
                    _buffTween = _BuffBar.GetComponent<CanvasGroup>().DOFade(1, 0);
                    yield return new WaitForSeconds(1f); //coroutine n tween clash jadi timingnya g pas / ada pause waktu time hit
//                    KillBuffTween();
//                    _buffTween = _BuffBar.GetComponent<CanvasGroup>().DOFade(0, 0);
//                    yield return new WaitForSeconds(1f);
                }
            }
        }
        else if (_BuffSprite != null && _BuffSprite.Count == 1 && battlerState != BattlerState.Dead)
        {
            KillBuffTween();
            if (_BuffBar.BuffImageList != null && _BuffBar.BuffImageList.Count > 0)
            {
                _BuffBar.BuffImage.sprite = _BuffBar.BuffImageList[0].Sprite;
                _BuffBar.TextImage.sprite = LegrandBackend.Instance.GetSpriteData(_BuffBar.BuffImageList[0].SourceKey + "font");
                _buffTween = _BuffBar.GetComponent<CanvasGroup>().DOFade(1, 0f);
            }
        }
        else
        {
            DisableBuffBlink();
        }
    }
    private void KillBuffTween(){
        if (_buffTween != null)
            _buffTween.Kill(false);
    }
    IEnumerator InfoBarBuff(){
        if (_BuffBar.BuffImageList != null && _BuffBar.BuffImageList.Count > 0)
            _BuffBar.GetComponent<CanvasGroup>().alpha = 1;
        else
            _BuffBar.GetComponent<CanvasGroup>().alpha = 0;

//        if (_BuffSprite.Count > 1)
            while (_BuffBar.BuffImageList.Count > 0)
            {
                for (int i = 0; i < _BuffBar.BuffImageList.Count; i++)
                {
                    BuffImage buffObj = _BuffBar.BuffImageList[i];
                    _BuffBar.BuffImage.sprite = buffObj.Sprite;
//                    _BuffBar.NameBuff.text = buffObj.SourceKey + " " + buffObj.Point;
                    _BuffBar.TextImage.sprite = LegrandBackend.Instance.GetSpriteData(buffObj.SourceKey+"font");
                    _BuffBar.GetComponent<CanvasGroup>().alpha = 1;
                    yield return new WaitForSeconds(1);
                }
            }
//        else if (_BuffSprite.Count == 1)
//        {
//            BuffImage buffObj = _BuffBar.BuffImageList[0];
//            _BuffBar.BuffImage.sprite = buffObj.Sprite;
//            _BuffBar.NameBuff.text = buffObj.SourceKey + " " + buffObj.Point;
//            _BuffBar.GetComponent<CanvasGroup>().alpha = 1;
//        }
    }

    public void RemoveBuffSprite(string Key)
    {
        string searchingKey = Key.Trim(new char[2] { '-', '+' });
        List <BuffImage> buffImageTemp = new List<BuffImage>();
        _BuffDebuffSprite.Remove(searchingKey);

        if (this.BattlerType == BattlerType.Player)
            _BuffBar.RemoveBuff(Key);

        for (int i = 0; i < _BuffSprite.Count; i++)
        {
            if (_BuffSprite[i].SourceKey == searchingKey)
                _BuffSprite.Remove(_BuffSprite[i]);
        }

    }

    public string GetParticleHit (){
        Weapon weapon = character.Weapon();
        string particle = "";
        switch (weapon.Type){
            case Weapon.DamageType.Pierce:
                particle = "PierceHit";
                break;
            case Weapon.DamageType.Impact:
                particle = "ImpactHit";
                break;
            case Weapon.DamageType.Slash:
                particle = "SlashHit";
                break;
        }

        return particle;
    }

    public bool IsEnemyDeadAll()
    {
        List<Battler> listTarget = new List<Battler>();
        if (battlerType == BattlerType.Player)
            listTarget = Battle.Instance.encounterList;
        else
            listTarget = Battle.Instance.charList;

        if (listTarget.TrueForAll(x => x.BattlerState == BattlerState.Dead))
            return true;
        else
            return false;
    }

    public void HitTarget (){
        if (battlerState != BattlerState.Guard)
            battlerState = BattlerState.Hit;
    }

    public void resetAll (){
        StopShaderSequence();
        _BuffDebuffStat = new List<Buff>();
        StopShaderSequence();
        if (_BuffBar != null)
        {
            _BuffBar.BuffImageList = new List<BuffImage>();
            _BuffBar.GetComponent<CanvasGroup>().DOFade(0, 0f);
        }
        if (_BuffDebuffSprite != null)
        _BuffDebuffSprite = new Dictionary<string, Sprite>();
        _BuffSprite = new List<BuffImage>();
        CheckBuffBlink();
    }

    public void CommandImportantCheck()
    {
        
    }
}