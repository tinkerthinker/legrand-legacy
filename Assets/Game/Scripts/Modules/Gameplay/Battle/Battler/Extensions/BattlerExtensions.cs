﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;

public static class BattlerExtensions {
//	public static Vector3 GetPivot(this Transform battlerTrans){
//		if (battlerTrans.FindChild ("FakePivot")) {
//			Transform ft = battlerTrans.FindChild ("FakePivot");
//			return ft.position - ft.localPosition;
//		} else {
//			return battlerTrans.position;
//		}
//	}
//
//	public static void Attack(this Battler attacker, Battler target){
//		Battle battle = Battle.Instance;
//		//attack value
//		bool critical = false;
//		float bonusCritical = 1f;
//		float bonusDamage = 1f;
//		float effectiveHit = 1f;
//		float baseDamage = 0f;
//		//def value
//		float bonusEvaRate = 0f;
//		float timedDefMissDamage = 0f;
//
//		//target is player
//		if (target.TimedHit != null) {
//			if(target.TimedDefState == TimedHitState.Perfect){
//				bonusEvaRate = battle.BattleFormula.BonusEvaRate / 100f;
//			}else if(target.TimedDefState == TimedHitState.Miss){
//				timedDefMissDamage = battle.BattleFormula.TimedDefMissDamage / 100f;
//			}
//		}
//
//		bool evade = target.RollEvadeFrom (attacker, bonusEvaRate);
//
//		if (!evade) {
//			//attacker is player
//			if (attacker.TimedHit != null) {
//				if(attacker.TimedHitState == TimedHitState.Perfect){
//					bonusDamage = battle.BattleFormula.PerfectDamage / 100f;
//				}else if(attacker.TimedHitState == TimedHitState.Good){
//					bonusDamage = battle.BattleFormula.GoodDamage / 100f;
//				}
//			}
//			critical = attacker.RollCrit (target);
//			baseDamage = attacker.PhysicDamage (target) * bonusDamage;
//			baseDamage = baseDamage + (baseDamage * timedDefMissDamage);
//
//			attacker.AddSTR_EXP(target);
//			attacker.AddAGI_EXP(target,"Hit");
//			if(critical) {
//				attacker.AddAGI_EXP(target, "Crit");
//			}
//
//			battle.Controller.cameraShake(critical);
//
//			Damage damage = new Damage (critical, baseDamage);
//			target.ReceiveDamage (damage);
//
//			target.AddVIT_EXP(attacker);
//			DamagePopUp(target.transform, Mathf.FloorToInt(damage.BaseDamage).ToString());
//		} else {
//			DamagePopUp(target.transform, "Miss");
//			target.AddAGI_EXP(target, "Eva");
//		}
//	}
//	public static void MagicAttack(this Battler attacker, List<Battler> targets, int MagicID){
//		foreach (Battler target in targets) {
//			if(target.BattlerState != BattlerState.Dead){
//				float magicDamage = attacker.MagicDamage (target, MagicID);
//
//				Damage MagicDamage = new Damage(false ,magicDamage);
//				target.ReceiveDamage (MagicDamage);
//
//				target.AddWIS_EXP(attacker);
//				attacker.AddINT_EXP(target);
//
//				DamagePopUp(target.transform, Mathf.FloorToInt(MagicDamage.BaseDamage).ToString());
//			}
//		}
//
//		float costDamage = attacker.CostDamage (MagicID);
//		Damage CostDamage = new Damage(false, costDamage);
//
//		attacker.ReceiveDamage (CostDamage);
//		DamagePopUp(attacker.transform, Mathf.FloorToInt(CostDamage.BaseDamage).ToString());
//	}
//	public static void SkillAttack(this Battler attacker, Battler target){
//		float skillDamage = attacker.SkillDamage (target);
//		Damage SkillDamage = new Damage(false, skillDamage);
//		target.ReceiveDamage(SkillDamage);
//
//		DamagePopUp(target.transform, SkillDamage.BaseDamage.ToString());
//	}
//	private static void DamagePopUp(Transform trans, string text){
//		GameObject popUp = PoolingSystem.Instance.InstantiateAPS("DamagePopUp", trans.position, trans.rotation);
//		popUp.GetComponent<Text> ().text = text;
//	}
//	public static void DamagePopUp(this Battler battlerTrans, string text){
//		GameObject popUp = PoolingSystem.Instance.InstantiateAPS("DamagePopUp", battlerTrans.transform.position, battlerTrans.transform.rotation);
//		popUp.GetComponent<Text> ().text = text;
//	}
}
