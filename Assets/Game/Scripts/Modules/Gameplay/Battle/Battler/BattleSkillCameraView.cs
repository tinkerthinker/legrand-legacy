﻿using UnityEngine;
using System.Collections;

public class BattleSkillCameraView : MonoBehaviour {
	public delegate void ChangeView(int viewIndex);
	public event ChangeView OnChangeView;

	public delegate void SkillDone();
	public event SkillDone OnSkillDone;

	private Animator _ViewAnimator;

	void Start()
	{
		_ViewAnimator = GetComponent<Animator> ();
	}

	public void Play(string animationName)
	{
		_ViewAnimator.Play (animationName);
	}

	public void ChangeViewTrigger(int i)
	{
		if (OnChangeView != null)
			OnChangeView (i);
	}

	public void SkillDoneTrigger()
	{
		if (OnSkillDone != null)
			OnSkillDone ();
	}
}
