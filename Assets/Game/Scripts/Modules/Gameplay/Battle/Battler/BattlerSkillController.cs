﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Legrand.core;
using Flux;

public class BattlerSkillController : MonoBehaviour {
	private BattleCameraController _BattleCamera;
	private Transform _CameraTrans;
    public Animator Anim;
    public Transform CasterPos;
    public GameObject Sequencer;

    private Camera _camera;
    public List <Transform> TargetPositions;
    public OffensiveBattleSkill skillUltimate;
    private FSequence SequencerScript;
    public string[] ActivateObjectPathsAtStart;

    public bool ActivatePartyLeader = false;
    public bool ActivateCurrentBG = false;
    private GameObject SequencerGameObject;
    public Transform FocusCamera;

    public string ObjectName;
    // Use this for initialization 
    void Start()
    {
       
    }

    public void init (OffensiveBattleSkill skill) {
		DOTween.Init ();
		_BattleCamera = Battle.Instance.BattleCameraController;
		_CameraTrans = _BattleCamera.SkillCamera.transform;
        _camera = _CameraTrans.GetComponent<Camera>();
        skillUltimate = skill;
	}
	public void PlayCameraAnimation (Battler battler,List <Battler> targets){
		_CameraTrans.transform.position = new Vector3 (0, 0, 0);
        _CameraTrans.SetParent (Anim.transform);
        _camera.fieldOfView = 35;

        battler.transform.SetParent(CasterPos);
        battler.transform.localPosition = new Vector3(0, 0, 0);

//        Anim.CrossFade("START", 1f);
        if (skillUltimate.SkillType == SkillTypes.UltimateOffense)
        {
            switch (skillUltimate.Target)
            {
                case TargetType.Single:
                    SetSingleTarget(targets);
                    break;
                case TargetType.Column: //horizontal
                    int x = 2;
                    if (targets.Count > 1)
                    {
                        for (int i = 0; i < targets.Count; i++)
                        {
                            if (targets[i].BattlerState != BattlerState.Dead)
                            {
                                targets[i].transform.SetParent(TargetPositions[x]);
                                targets[i].transform.localPosition = new Vector3(0, 0, 0);
                                x += 3;
                            }
                        }
                    }
                    else
                        SetSingleTarget(targets);
                    break;
                case TargetType.Row:
                    if (targets.Count > 1)
                    {
                        for (int i = 0; i < targets.Count; i++)
                        {
                            if (targets[i].BattlerState != BattlerState.Dead)
                            {
                                targets[i].transform.SetParent(TargetPositions[i]);
                                targets[i].transform.localPosition = new Vector3(0, 0, 0);
                            }
                        }
                    }
                    else
                        SetSingleTarget(targets);
                    break;
                case TargetType.All:
                    if (targets.Count > 1)
                    {
                        for (int i = 0; i < targets.Count; i++)
                        {
                            if (targets[i].BattlerState != BattlerState.Dead)
                            {
                                for (int j = 0; j < TargetPositions.Count; j++)
                                {
                                    Vector2 planePos = TargetPositions[j].GetComponent<BattlePlane>().planePos;
                                    if (planePos == targets[i].MyPlane.planePos)
                                    {
                                        targets[i].transform.SetParent(TargetPositions[j]);
                                        targets[i].transform.localPosition = new Vector3(0, 0, 0);
                                    }
                                }
                            }
                        }
                    }
                    else
                        SetSingleTarget(targets);
                    break;
                default:
                    break;
            }
        }

        PlayFlux();
	}

    void SetSingleTarget (List<Battler> targetBattler){
        if (targetBattler[0].BattlerState != BattlerState.Dead)
        {
            targetBattler[0].transform.SetParent(TargetPositions[1]);
            targetBattler[0].transform.localPosition = new Vector3(0, 0, 0);
        }
    }
    void PlayFlux()
    {
//        foreach (string objectPath in ActivateObjectPathsAtStart)
//            GameObject.Find(objectPath).SetActive (true);
//
//        if (ActivatePartyLeader && AreaController.Instance != null)
//            AreaController.Instance.CurrPlayer.SetActive (true);
//        if (ActivateCurrentBG && AreaController.Instance != null)
//            AreaController.Instance.CurrPrefab.SetActive (true);

        FluxResume FRes = GetComponent<FluxResume>();
        SequencerGameObject = (GameObject)Instantiate(Sequencer);
        SequencerScript = SequencerGameObject.GetComponent<FSequence>();
        if (FRes && SequencerScript)
        {
            FRes.CurrentSequence = SequencerScript;
        }

        StartCoroutine(PlayScene(SequencerScript));
    }
    IEnumerator PlayScene(FSequence SequenceToPlay)
    {
        yield return new WaitForSeconds(0f);
        SequenceToPlay.Play();
    }
    void OnDestroy (){
        Destroy(SequencerGameObject);
    }

    public void ChangeLayer (Battler trans, string layerName)
    {
        foreach (Transform child in trans.GetComponentsInChildren<Transform>()) 
        {
            child.gameObject.layer = LayerMask.NameToLayer (layerName);
        }
    }
}
