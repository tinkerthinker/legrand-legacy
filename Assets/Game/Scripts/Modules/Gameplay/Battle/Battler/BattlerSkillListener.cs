﻿using UnityEngine;
using System.Collections;

public class BattlerSkillListener : MonoBehaviour {
	public delegate void OnEvent();
	public event OnEvent OnHit;
	public event OnEvent OnDone;
	// Use this for initialization
	void Start () {
	
	}
	public void HitEvent(string audio){
		if (OnHit != null) 
		{
			WwiseManager.BattleFX parsed_enum = (WwiseManager.BattleFX) System.Enum.Parse( typeof( WwiseManager.BattleFX ), audio );
			WwiseManager.Instance.PlaySFX(parsed_enum);
			OnHit ();
		}
	}

	public void DoneEvent(){
		if (OnDone != null)
			OnDone ();
	}

	public void Sound(string audio)
	{
		WwiseManager.BattleFX parsed_enum = (WwiseManager.BattleFX) System.Enum.Parse( typeof( WwiseManager.BattleFX ), audio );
		WwiseManager.Instance.PlaySFX(parsed_enum);
	}
}
