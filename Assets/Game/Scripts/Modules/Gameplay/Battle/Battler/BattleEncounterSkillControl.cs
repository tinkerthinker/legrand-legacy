﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BattleEncounterSkillControl : MonoBehaviour {
    public Transform Target;
    private Animator _Anim;
    private BattleCameraController _BattleCamera;
    private Transform _CameraTrans;
    public Transform _AnimTransform;
    public string BossName;
	// Use this for initialization
    void Awake (){
        _Anim = gameObject.GetComponent<Animator>();
    }
	void Start () {
	    
	}
	
    public void init () {
        _BattleCamera = Battle.Instance.BattleCameraController;
        _CameraTrans = _BattleCamera.SkillCamera.transform;
        _Anim = _AnimTransform.gameObject.GetComponent<Animator> ();
    }

    public void PlayCameraAnimation (Battler battler,List <Battler> targets){
        init();
        _CameraTrans.transform.position = new Vector3 (0, 0, 0);
        _CameraTrans.SetParent (_AnimTransform);
        _Anim.CrossFade("START", 0f);
        float x = 0;

        if (targets.Count > 1)
        {
            for (int i = 0; i < targets.Count; i++)
            {//foreach (Battler bat in targets) {
                if (targets[i].BattlerState != BattlerState.Dead)
                {
                    targets[i].transform.position = new Vector3(targets[i].transform.position.x, Target.position.y, Target.position.z + targets[i].BattlePosition.y + 1);
                    //bat.transform.eulerAngles = new Vector3 (0,180,0);
                    x += 1.75f;
                }
            }
        }
        else
        {
            targets[0].transform.position = Target.position;
        }
    }
    public void ChangeLayer (Battler trans, string layerName)
    {
        foreach (Transform child in trans.GetComponentsInChildren<Transform>()) 
        {
            child.gameObject.layer = LayerMask.NameToLayer (layerName);
        }
    }
}
