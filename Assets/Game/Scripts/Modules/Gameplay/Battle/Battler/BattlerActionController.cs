using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using Legrand.core;

public class BattlerActionController : MonoBehaviour
{
    private BattleCommand command;
    private Battler battler;

    private Vector3 oriPos;
    private Quaternion oriRot;
    private BattlePoolingSystem pooling;

    private BattlerStateController stateController;
    private IEnumerator actionCoroutine;
    private BattleController _Controller;

    [SerializeField]
    public List<BaseAction> actionSequence = new List<BaseAction>();
    private MoveToPos moveToPos = new MoveToPos();
    private MoveBack moveBack = new MoveBack();
    private CasterAttack cAttackAct = new CasterAttack();
    public MeleeAttack mAttackAct = new MeleeAttack();
    private MeleeMagic mMagicAct = new MeleeMagic();
    private RangeAttack rAttackAct = new RangeAttack();
    private RangeMagicProjectile rMagicActProj = new RangeMagicProjectile();
    private Guard guardAct = new Guard();
    private UseItem useItemAct = new UseItem();
    private SkillAttack skillAct = new SkillAttack();
    private SkillCombo skillCom = new SkillCombo();
    private SkillDefense skillDef = new SkillDefense();
    private SkillNormal skillNor = new SkillNormal();
    private Flee fleeAct = new Flee();
    private ReviveAction reviveAct = new ReviveAction();
    private EncounterSkillAction encSkillAct = new EncounterSkillAction();
    private ChargingAction chargeAct = new ChargingAction();

    private int iSeq = 0;
    private int priority = 0;
    //	private float basicSpeed = 3f;
    private float moveSpeed = 6f;
    private float delay = 0.25f;

    // Use this for initialization
    void Start()
    {
        pooling = BattlePoolingSystem.Instance;
        battler = GetComponent<Battler>();
        stateController = GetComponent<BattlerStateController>();
        _Controller = GetComponentInParent<BattleController>();
        initBattlerValue();
    }

    void initBattlerValue()
    {
        oriPos = transform.position;
        oriRot = transform.rotation;

        battler.OnDead += OnDeadhandler;
        battler.OnHit += OnHitHandler;
    }

    void OnDestroy()
    {
        battler.OnDead -= OnDeadhandler;
        battler.OnHit -= OnHitHandler;
    }

    public void initAct(BattleCommand cmd, int prior)
    {
        command = cmd;
        priority = prior;
        oriPos = transform.position;
        oriRot = transform.rotation;
        actionSequence.Clear();

        if (command != null)
        {

            if (command.target.Count > 0 &&
                command.commandData.CommandType != (int)CommandTypes.Item &&
                command.target.TrueForAll(x => x.BattlerState == BattlerState.Dead &&
                command.commandData.CommandType != (int)CommandTypes.Magic && 

                command.commandData.CommandType != (int)CommandTypes.Attack && 
                command.commandData.CommandType != (int)CommandTypes.SkillUlti))
            { 
                command.isDone = true;
                battler.DoneAction();

                if (command.magicCasted != null)
                    command.magicCasted.DestroyBattleAPS();
            }
            else
            {

                switch (command.commandData.CommandType)
                {
                    case (int)CommandTypes.Attack:
                        attackAction();
                        break;

                    case (int)CommandTypes.Move:
                        moveAction();
                        break;

                    case (int)CommandTypes.Magic:
                        if (command.isCasting)
                        {
                            if (command.attacker.BattlerType == BattlerType.Player)
                            {
                                battler.isCasting = false;
                                command.isCasting = false;
                            }

                            if (command.magicCasted != null)
                            {
                                command.magicCasted.DestroyBattleAPS();
                            }

                            if (battler.AttackType == AttackType.Melee)
                            {
                                meleeMagicAtion();
                            }
                            else
                            {
                                rangeMagicAction();
                            }
                        }
                        else
                        {
                            if (command.attacker.BattlerType == BattlerType.Player && command.target.Any(x => x.BattlerType != BattlerType.Player))
                            {
                                EventManager.Instance.TriggerEvent(new TimedHitEvent(command.attacker, command.target, command));
                                battler.ActionBar.ShowActionBar (command.commandData.CommandName);
                            }
                            battler.BattlerState = BattlerState.Channeling;
                            battler.ShiftAction();
                        }
                        break;

                    case (int)CommandTypes.SkillUlti:
                        if (battler.BattlerType == BattlerType.Player)
                        {
                            MainCharacter mc = (battler.Character as MainCharacter);
                            if (mc.BattleOffensiveSkill.SkillType == SkillTypes.UltimateOffense)
                            {
                                skillAction();
                                battler.BattleStateCondition = BattleStateCondition.SkillOffensive;
                            }
                            else
                            {
                                defSkillAction();
                            }
//                            else if (command.commandData.SlotPos == 2)
//                            {
//                                defSkillAction();
//                                battler.BattleStateCondition = BattleStateCondition.SkillDefensive;
//                            }
//                            else if (command.commandData.SlotPos == 3)
//                            {
//                                BattleCommand batCom = _Controller.CommandList.Find(x => x.attacker != command.attacker && x.commandData.PanelPos == command.commandData.PanelPos && x.commandData.SlotPos == command.commandData.SlotPos);
//                                battler.BattleStateCondition = BattleStateCondition.SkillCombination;
//
//
//                                if (batCom != null)
//                                {
//                                    command.batPartner = batCom.attacker;
//                                    _Controller.CommandList.Remove(batCom);
//                                    comboSkillAction();
//                                }
//                                else
//                                {
//                                    command.commandData.PanelPos = 0;
//                                    command.commandData.SlotPos = 1;
//                                    if (battler.BattlerType == BattlerType.Player && command.target.Any(x => x.BattlerType != BattlerType.Player))
//                                        EventManager.Instance.TriggerEvent(new TimedHitEvent(command.attacker, command.target, command));
//
//                                    attackAction();
//                                }
//                            }
                        }
                        else
                            encSkillAction();
                        break;

                    case (int)CommandTypes.Item:
                        useItemAction();
                        reviveAction();
                        break;

                    case (int)CommandTypes.Guard:
                        guardAction();
                        break;
                    case (int)CommandTypes.Flee:
                        fleeAction();
                        //fleePrompt();
                        break;
                    case (int)CommandTypes.Skill:
                        normalSkillAction();
                        break;
                    case (int)CommandTypes.Charging:
                        chargeAction();
                        break;
                }
            }
        }
        else
        {
            battler.DoneAction();
        }
    }
    private void chargeAction()
    {
        chargeAct.initAction(command);
        actionSequence.Add(chargeAct);
        execSequence();
    }
    private void moveAction()
    {
        moveToPos.initAction(command, moveSpeed);
        actionSequence.Add(moveToPos);
        execSequence();
    }
    private void attackAction()
    {
        if (battler.AttackType == AttackType.Melee)
        {
            mAttackAct.initAction(command, moveSpeed);
            moveBack.initAction(command, moveSpeed, oriPos, oriRot);

            actionSequence.Add(mAttackAct);
            actionSequence.Add(moveBack);
            execSequence();
        }
        else if (battler.AttackType == AttackType.Range)
        {
            rAttackAct.initAction(command, moveSpeed, oriPos, oriRot);
            actionSequence.Add(rAttackAct);
            execSequence();
        }
        else if (battler.AttackType == AttackType.Caster)
        {
            cAttackAct.initAction(command, moveSpeed, oriPos, oriRot);
            actionSequence.Add(cAttackAct);
            execSequence();
        }
    }
    private void rangeMagicAction()
    {
        TargetType magicTarget = (TargetType)battler.GetMagicTarget(command.commandData.CommandValue);

        rMagicActProj.initAction(command);
        actionSequence.Add(rMagicActProj);
        execSequence();
    }
    private void meleeMagicAtion()
    {
        mMagicAct.initAction(command, moveSpeed);
        moveBack.initAction(command, moveSpeed, oriPos, oriRot);

        actionSequence.Add(mMagicAct);
        actionSequence.Add(moveBack);
        execSequence();
    }
    private void useItemAction()
    {
        useItemAct.initAction(command);
        actionSequence.Add(useItemAct);
        execSequence();
    }
    private void guardAction()
    {
        guardAct.initAction(command);
        actionSequence.Add(guardAct);
        execSequence();
    }

    private void fleeAction()
    {
        fleeAct.initAction(command);
        actionSequence.Add(fleeAct);
        execSequence();
    }

    private void skillAction()
    {
        skillAct.initAction(command);
        actionSequence.Add(skillAct);
        execSequence();
    }
    private void encSkillAction()
    {
        encSkillAct.initAction(command);
        actionSequence.Add(encSkillAct);
        execSequence();
    }
    public void reviveAction()
    {
        if (command.target[0].BattlerState == BattlerState.Dead)
        {
            reviveAct.initAction(command, moveSpeed);
            actionSequence.Add(reviveAct);
            execSequence();
        }
    }
    public void comboSkillAction()
    {
        skillCom.initAction(command);
        actionSequence.Add(skillCom);
        execSequence();
    }
    private void defSkillAction()
    {
//        List<Buff> ultiDefBuffs = (battler.Character as MainCharacter).BattleDefensiveSkill.Buffs;
//        for (int i = 0; i < command.target.Count; i++)
//            ItemController._Instance.SendTargetBuffDebuff(command.target[i], ultiDefBuffs);
        skillDef.initAction(command);
        actionSequence.Add(skillDef);
        execSequence();
    }
    private void normalSkillAction()
    {
        skillNor.initAction(command);
        actionSequence.Add(skillNor);
        execSequence();
    }
    public void OnCheckInterrupt (Battler attacker){
        checkIsCasting(attacker);
    }
    private void OnHitHandler(Battler bat)
    {
        if (battler.BattlerState != BattlerState.Dead && battler.OnDamage)
        {
            if (actionSequence.Count <= 0)
            {
                if (battler.BattlerState != BattlerState.Guard &&
                    battler.BattlerState != BattlerState.Channeling &&
                    battler.BattlerState != BattlerState.Magic &&
                    battler.BattlerState != BattlerState.Dead)
                {
                    battler.BattlerState = BattlerState.Hit;
                }
            }
            else
            {
                if (battler.BattlerState != BattlerState.Guard &&
                    battler.BattlerState != BattlerState.Attack &&
                    battler.BattlerState != BattlerState.Combo &&
                    battler.BattlerState != BattlerState.Magic &&
                    battler.BattlerState != BattlerState.Channeling &&
                    battler.BattlerState != BattlerState.Guard &&
                    battler.BattlerState != BattlerState.Dead)
                {
                    battler.BattlerState = BattlerState.Hit;
                }
            }
        }

    }
    private void OnDeadhandler(Battler b)
    {
        if (actionCoroutine != null)
        {
            StopCoroutine(actionCoroutine);
            actionCoroutine = null;
        }
        actionSequence.ForEach(x => x.onDoneAct -= HandleonDoneAct);
        actionSequence.Clear();
        iSeq = 0;
        if (command != null && command.isCasting && command.magicCasted != null)
        {
            command.magicCasted.DestroyBattleAPS();
        }
    }
    private void checkIsCasting(Battler attacker)
    {
        if (command != null && command.isCasting && command.isDone != true && battler.BattlerState != BattlerState.Dead)
        {
            float rand = LegrandUtility.Random(1f, 100f);
            float interruptChance = 0;
            Magic castedMagic = battler.GetMagic(command.commandData.CommandValue);
            if(castedMagic != null)
            {
                if (attacker != null) // dicek lagi, karena hit juga bisa disebabkan poison each turn attacker nya null) attacker = penyerang, battler = hitted = this battler access
                    interruptChance = Formula.CalculateInterruptModifier(attacker, battler, castedMagic.InteruptChance); //battler = defender
            }
            if (interruptChance > 0 && LegrandUtility.Random(0f,100f) < interruptChance)
            {
                if (command != null && command.isCasting && command.magicCasted != null)
                {
                    command.magicCasted.DestroyBattleAPS();
                    //					}
                    battler.isCasting = false;
                    command.isCasting = false;
                    battler.PopUp("Interrupted", battler.BattlerType);
                    battler.BattlerState = BattlerState.Hit;
                    StopAllCoroutines();
                    actionSequence.Clear();
                    battler.DoneAction();
                }
            }

        }
    }
    #region sequence executor
    private void execSequence()
    {
        foreach (BaseAction bs in actionSequence)
        {
            _Controller.GlobalActionSeq.Add(bs);
        }
        if (actionSequence.Count > 0)
        {
            StartCoroutine("DelayExecSequence", ((priority / 12f) + (delay * priority)));
        }
    }
    private void coroutineSequence()
    {
        StopCoroutine("DelayExecSequence");
        if (actionSequence.Count > 0)
        {
            actionSequence[iSeq].onDoneAct += HandleonDoneAct;

            actionCoroutine = actionSequence[iSeq].Update();
            StartCoroutine(actionCoroutine);
        }
    }

    void HandleonDoneAct(bool resetState)
    {
        if (actionCoroutine != null)
        {
            StopCoroutine(actionCoroutine);
            actionCoroutine = null;
            actionSequence[iSeq].onDoneAct -= HandleonDoneAct;

            if (resetState && battler.BattlerState != BattlerState.Dead)
            {
                battler.BattlerState = BattlerState.Idle;
            }
            iSeq++;

            if (iSeq <= actionSequence.Count - 1)
            {
                execSequence();
            }
            else
            {
                iSeq = 0;
                actionSequence.Clear();
                command.isDone = true;
                battler.DoneAction();
                battler.isCasting = false;
                command = null;
            }
        }
    }
    IEnumerator DelayExecSequence(float delay)
    {
        while (true)
        {
            yield return new WaitForSeconds(delay);
            coroutineSequence();
            yield break;
        }
    }
    IEnumerator DelayNextSequence()
    {
        while (true)
        {
            yield return new WaitForSeconds(0f);
            execSequence();
            yield break;
        }
    }
    #endregion


}
