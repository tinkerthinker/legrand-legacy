﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class BattleSkillCombination : MonoBehaviour {
	[SerializeField]
	private Transform _TargetPos;
	public Animation ThisAnimations;
	private BattleCameraController _BattleCamera;
	private Transform _CameraTrans;
	private ScreenFader fader;
	public Battler Battler;
	public string IdPartner;


	public delegate void OnSkill();
	public event OnSkill OnSkillDone;

	private Vector3 _Dif;
	private List<Battler> _Targets;
	// Use this for initialization
	public void init () {
		DOTween.Init ();
		_BattleCamera = Battle.Instance.BattleCameraController;
		_CameraTrans = _BattleCamera.SkillCamera;
		ThisAnimations = GetComponentInChildren<Animation> ();
	}
	public void PlayCameraAnimation (Battler battler,Battler target){
		_CameraTrans.SetParent (ThisAnimations.transform);
		target.transform.position = _TargetPos.position;
		ThisAnimations.Play ();
	}
	public void ChangeLayer (Battler trans, string layerName)
	{
		foreach (Transform child in trans.GetComponentsInChildren<Transform>()) 
		{
			child.gameObject.layer = LayerMask.NameToLayer (layerName);
		}
	}
}
