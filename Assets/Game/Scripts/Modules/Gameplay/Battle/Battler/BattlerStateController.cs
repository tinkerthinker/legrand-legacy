using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;
public class BattlerStateController : MonoBehaviour {
	private Battler battler;
	private Animator _Anim;
	public Animator Anim {
		get {return _Anim;}
		set {_Anim = value;}
	}

	private AnimatorStateInfo animState;
	private BattlerState lastState, nextState;

	public Battler target;

	[HideInInspector]
	public Battler BattlerCombo;

	public delegate void AnimEvent();
	public event AnimEvent OnStartAttack;
	public event AnimEvent OnDoneAttack;
	public event AnimEvent OnMagic;
	public event AnimEvent OnDoneMagic;
	public event AnimEvent OnDoneHit;

	public event AnimEvent OnDoneSkill;

	public event AnimEvent OnLastEffect;

	public event AnimEvent OnTimeHit;

    public delegate void AttackEvent (string particleName);
    public event AttackEvent OnAttack;
    public event AttackEvent OnLastAttack;
    public event AttackEvent EffectParticle;
    public event AttackEvent OnSkillEffect;


	public bool StateLocked = false;
	public float timeTransition = 0.2f;
	// Use this for initialization
	void Start () {
		_Anim = GetComponentInChildren<Animator> ();
		battler = GetComponent<Battler> ();

		battler.OnStateChange += HandleOnStateChange;
		battler.OnLockState += HandleLockState;
		animState = _Anim.GetCurrentAnimatorStateInfo (0);
	}
	#region State Listener
	void HandleOnStateChange  (BattlerState state)
	{
		if (state != BattlerState.Dead && StateLocked)
			return;
        
		
		switch (state) {
		case BattlerState.Attack:
			_Anim.CrossFade("ATTACK",0f);
			break;
		case BattlerState.Skill:
			if (battler.BattlerType == BattlerType.Player){
				if (_Anim.HasState (0, Animator.StringToHash ("OFFENSE_SKILL")) && battler.BattleStateCondition == BattleStateCondition.SkillOffensive) {
					_Anim.CrossFade ("SKILL_ULTI", 0.1f);
				}
//				else if (_Anim.HasState (0, Animator.StringToHash ("DEFENSIVE_SKILL")) && battler.BattleStateCondition == BattleStateCondition.SkillDefensive) {
//					_Anim.CrossFade ("DEFENSIVE_SKILL", 0.1f);
//					StartCoroutine("resetState",2f);
//				}
//				else if (_Anim.HasState (0, Animator.StringToHash ("COMBINATION")) && battler.BattleStateCondition == BattleStateCondition.SkillCombination) {
//					_Anim.CrossFade ("COMBINATION_SKILL", 0.1f);
//				}
			}
			else {
				if (battler.BattleStateCondition == BattleStateCondition.EnemySkillFirst)
				_Anim.CrossFade("SKILL_FIRST", 0.1F);
				else if (battler.BattleStateCondition == BattleStateCondition.EnemySkillSecond)
				_Anim.CrossFade("SKILL_SECOND", 0.1F);
			}
			break;
        case BattlerState.Move:
                _Anim.Play("RUN");
			break;
		case BattlerState.Combo:
                _Anim.Play ("COMBO");
			break;
		case BattlerState.Casting:
			break;
		case BattlerState.UseItem:
			if(_Anim.HasState(0, Animator.StringToHash("ITEMBUFF")) && battler.BattleStateCondition == BattleStateCondition.ItemBuff)
			_Anim.CrossFade("ITEMBUFF",0.1f);
			else if (_Anim.HasState(0, Animator.StringToHash("ITEMDEBUFF")) && battler.BattleStateCondition == BattleStateCondition.ItemDebuff)
			_Anim.CrossFade("ITEMDEBUFF",0.1f);

			StartCoroutine("resetState",2f);
			break;
		case BattlerState.Magic:
			if(_Anim.HasState(0, Animator.StringToHash("MAGIC"))){
				_Anim.CrossFade("MAGIC",0.1f);
			}
			break;
		case BattlerState.Guard:
			if(_Anim.HasState(0, Animator.StringToHash("GUARD")) ){ //&& battler.BattleStateCondition == BattleStateCondition.None
				_Anim.CrossFade("GUARD",0.1f);
				battler.BattleStateCondition = BattleStateCondition.GuardEnd;
			}
//			else if (anim.HasState(0, Animator.StringToHash("GUARD_HIT")) && battler.BattleStateCondition == BattleStateCondition.GuardHit){
//				anim.CrossFade("GUARD_HIT",0.1f);
//				battler.BattleStateCondition = BattleStateCondition.GuardEnd;
//			}
			break;
            case BattlerState.Hit:
                if (battler.BattlerType == BattlerType.Enemy)
                {
                    Encounter enc = battler.Character as Encounter;
                    if (enc.MonsterSize != EncounterSize.Massive)
                    {
                        _Anim.Play("HIT");
                        if (!battler.Health.IsDead)
                            StartCoroutine("resetState", 0.1f);
                    }
                }
                else{
                    _Anim.Play("HIT");
        			if (!battler.Health.IsDead)
        				StartCoroutine("resetState",0.1f);
                }
			break;
		case BattlerState.Dead:
			StopAllCoroutines ();
			_Anim.CrossFade ("DEATH", 0.25f);
			if (battler.BattlerType == BattlerType.Enemy)
				battler.BattlerDeadShader ();
			break;
		case BattlerState.Idle:
			if (battler.BattleStateCondition == BattleStateCondition.GuardEnd) {
				_Anim.CrossFade ("GUARD_END", 0f);
                    timeTransition = 0.5f;
			} else {

				if (battler.Health.Value < battler.Health.MaxValue * 20 / 100) {
					if (!_Anim.GetAnimatorTransitionInfo (0).anyState)
                            _Anim.CrossFade ("BATTLESTANCE_LOW", timeTransition);
				} else {
					if (!_Anim.GetAnimatorTransitionInfo (0).anyState)
                            _Anim.CrossFade ("BATTLESTANCE", timeTransition);
				}
			}
			break;
		case BattlerState.Revived:
			_Anim.CrossFade ("REVIVE", 0.1f);
                timeTransition = 1f;
			break;
		case BattlerState.Winning:
			_Anim.CrossFade("WINNING",0.5f);
			break;
		case BattlerState.Channeling:
			_Anim.CrossFade("CHANNELING_START", 0.2f);
			break;
		case BattlerState.Flee:
			_Anim.CrossFade("FLEE", 0f);
			break;
		case BattlerState.Preview:
			_Anim.CrossFade("PREVIEW", 0.2f);
			break;
		case BattlerState.Combination:
			if (BattlerCombo != null) {
				MainCharacter main = (BattlerCombo.Character as MainCharacter);
				if (BattlerCombo != null) {
					_Anim.CrossFade ("COMBO_" + main._ID, 0f);
				}
			}
			break;
		}
	}

	void HandleLockState(bool state)
	{
		StateLocked = state;
	}

	IEnumerator resetState(float delay){
		while(true){
			if(battler.BattlerState != BattlerState.Dead){
				yield return new WaitForSeconds(delay);
				battler.BattlerState = BattlerState.Idle;
				StateLocked = false;
			}
			yield break;
		}
	}
	public void OnStartAttackEvent(){
		if (OnStartAttack != null)
			OnStartAttack();
	}
    public void OnSkillEffectEvent(string particleName){
        if (OnSkillEffect != null)
            OnSkillEffect(particleName);
    }
    public void OnAttackEvent(string particleName){
		if (OnAttack != null)
            OnAttack (particleName);
	}
    public void OnLastAttackEvent(string particleName){
		if (OnLastAttack != null)
            OnLastAttack (particleName);
	}
    public void OnEffectEvent (string particleName){
        if (EffectParticle != null)
            EffectParticle(particleName);
    }
	public void DoneAttackEvent(){
		if (OnDoneAttack != null)
			OnDoneAttack ();
	}
	public void OnMagicEvent(){
		if (OnMagic != null)
			OnMagic ();
	}
	public void DoneMagicEvent(){
		if (OnDoneMagic != null)
			OnDoneMagic ();
	}
	public void DoneOneShotEvent(){
		if (OnDoneHit != null)
			OnDoneHit ();
	}
	public void DoneSkillEvent (){
		if (OnDoneSkill != null)
			OnDoneSkill ();
	}
	void OnDestroy(){
		StopAllCoroutines ();
		battler.OnStateChange -= HandleOnStateChange;
		battler.OnLockState -= HandleLockState;
	}
	public void OnTimeHitEvent(){
		if (OnTimeHit != null)
			OnTimeHit ();
	}

	public void OnLastEffectEvent(){
		if (OnLastEffect != null)
			OnLastEffect ();
	}

	public bool IsOnStartAttackEvent (){
		if (OnStartAttack != null)
			return true;
		return false;
	}
    public void ClearAttackEvent()
    {
        OnStartAttack = null;
    }

    public bool IsOnAttackEvent (){
		if (OnAttack != null)
			return true;
		return false;
	}
    public void ClearOnAttackEvent()
    {
        OnAttack = null;
    }

    public bool IsOnLastAttackEvent(){
		if (OnLastAttack != null)
			return true;
		return false;
	}
    public void ClearOnLastAttackEvent()
    {
        OnLastAttack = null;
    }
    public bool IsOnEffectEvent(){
        if (EffectParticle != null)
            return true;
        return false;
    }
    public void ClearEffectEvent()
    {
        EffectParticle = null;
    }
    public bool IsDoneAttackEvent(){
		if (OnDoneAttack != null)
			return true;
		return false;
	}
    public void ClearOnDoneAttackEvent()
    {
        OnDoneAttack = null;
    }

    public bool IsOnMagicEvent(){
		if (OnMagic != null)
			return true;
		return false;
	}
    public void ClearOnMagicEvent()
    {
        OnMagic = null;
    }

    public bool IsDoneMagicEvent(){
		if (OnDoneMagic != null)
			return true;
		return false;
	}
    public void ClearDoneMagicEvent()
    {
        OnDoneMagic = null;
    }

    public bool IsDoneOneShotEvent(){
		if (OnDoneHit != null)
			return true;
		return false;
	}
    public void ClearDoneOneShotEvent()
    {
        OnDoneHit = null;
    }
    #endregion

    public BattlerState RecheckAnimation()
	{
		AnimatorStateInfo currentState = _Anim.GetCurrentAnimatorStateInfo (0);

		if (currentState.IsName ("ATTACK"))
			return BattlerState.Attack;
		else if(currentState.IsName ("RUN"))
			return BattlerState.Move;
		else if(currentState.IsName ("BATTLESTANCE") && currentState.IsName ("BATTLESTANCE_LOW"))
			return BattlerState.Idle;

		return BattlerState.Idle;
	}

    public bool CheckAnimation(string name)
    {
        AnimatorStateInfo currentState = _Anim.GetCurrentAnimatorStateInfo(0);

        return currentState.IsName(name);
    }
}
