using UnityEngine;

public class SelectTargetData {
	//offset data
	public Vector2[] single = 
	{
		new Vector2(0,0)
	};
	public Vector2[] column =
	{
		new Vector2 (0, 0),
		new Vector2 (0, 1)
	};
	public Vector2[] row =
	{
		new Vector2 (0, 0),
		new Vector2 (1, 0),
		new Vector2 (2, 0),
	};
	public Vector2[] square =
	{
		new Vector2 (0, 0),
		new Vector2 (1, 0),
		new Vector2 (0, 1),
		new Vector2 (1, 1)
	};
	public Vector2[] all =
	{
		new Vector2 (0, 0),
		new Vector2 (1, 0),
		new Vector2 (2, 0),
		new Vector2 (0, 1),
		new Vector2 (1, 1),
		new Vector2 (2, 1),
	};
}



