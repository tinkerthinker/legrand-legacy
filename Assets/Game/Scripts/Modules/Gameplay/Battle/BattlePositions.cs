﻿using UnityEngine;
using System.Collections;
using Legrand.core;

public class BattlePosition {
	public Battler [,] Position;
	
	public BattlePosition (){
		Position =  new Battler [3,2];
	}
	public void Add (Vector2 pos, Battler battler){
		Position[(int)pos.x,(int)pos.y] = battler;
	}
	public Battler GetBattlerByPosition (int x, int y){
		return Position[x,y];
	}
}
