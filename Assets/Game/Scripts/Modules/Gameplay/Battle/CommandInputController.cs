﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using TeamUtility.IO;
using DG.Tweening;

using Legrand.core;
[System.Serializable]
public class CommandInputController {
	//public AudioSource sfx;
	private GameObject commandPanel;
	private GameObject _MainPanel;
	private GameObject _UpPanel;
	private GameObject _RightPanel;
	private GameObject _DownPanel;
	private GameObject _LeftPanel;

	private Animator commandAni;
	private CanvasGroup commandGroup;
	private CommandButton button;
	private BattleInput battleInput;
	private ItemController _ItemController = new ItemController();
	private TargetController _TargetController;

	private List<CommandButton> buttonArr = new List<CommandButton>();

	private bool onGetInput = false;
	private int dir = -1;

	public delegate void CommandInput(CommandButton commandButton);
	public event CommandInput OnCommandInput;

	public delegate void CommandCancel();
	public event CommandCancel OnCommandCancel;
	private int _Key;
	private int _SelectKey;

	private Tween _OverlayTween;
	private Tween _LeftButton;

	private ButtonOverlay _ButtonHelper;
	private string _CommandDesc;
	private bool _confirmMainPanel = false;
    private bool isDescription = false;

    private List <CommandData> ImportantCommand = new List<CommandData>();
	// Use this for initialization
	public CommandInputController (BattleInput input) {
		commandPanel 	= Battle.Instance.CommandPanelGroup;
		commandGroup 	= commandPanel.GetComponent<CanvasGroup> ();
		commandAni 		= commandPanel.GetComponent<Animator> ();
		battleInput 	= input;
		_TargetController = new TargetController (battleInput);
		_ButtonHelper = commandPanel.GetComponent<ButtonOverlay> ();

		buttonArr.Clear ();
		buttonArr.AddRange (commandPanel.GetComponentsInChildren<CommandButton> ());

		_MainPanel = commandPanel.transform.GetChild(0).gameObject;
		_UpPanel = commandPanel.transform.GetChild(1).gameObject;
		_RightPanel = commandPanel.transform.GetChild(2).gameObject;
		_DownPanel = commandPanel.transform.GetChild(3).gameObject;
		_LeftPanel = commandPanel.transform.GetChild(4).gameObject;

	}
	public void showMainPanel(bool attackAble, bool fleeAble, Battler battler, Dictionary<string, int> PlayersItems)
    {
        _ButtonHelper.DescriptionPanel.SetActive (false);
        _ButtonHelper.SelectCommand.SetActive (false);
		MainCharacter mainChar = battler.Character as MainCharacter;
        int gaugeDifference = mainChar.Gauge - battler.GaugeCharge;

        if (gaugeDifference != 0)
        {
            battler.GaugeCharge = mainChar.Gauge;
            battler.UpdateGauge(gaugeDifference);
        }

        List<CommandData> commands 	= mainChar.CommandPresets;
		setAllPanel(true);
        onGetInput = false;
        ImportantCommand = new List<CommandData>();;
		setAllFalse ();
		commandGroup.alpha = 1;
		dir = -1;
		_SelectKey = -1;

        for (int i = 0; i < buttonArr.Count; i++){
            for (int y = 0; y < commands.Count; y++){ 
                CommandButton but = buttonArr[i];
                CommandData dat = commands[y];

				if(but.commandButtonData.PanelPos.Equals(dat.PanelPos) && 
				   but.commandButtonData.SlotPos.Equals(dat.SlotPos)){

					but.commandButtonData.CharacterID		= dat.CharacterID;
					but.commandButtonData.CommandType 		= dat.CommandType;
					but.commandButtonData.CommandValue 		= dat.CommandValue;
					but.commandButtonData.PanelPos			= dat.PanelPos;
					but.commandButtonData.SlotPos			= dat.SlotPos;

                    Text buttonText 						= but.ButtonText;
					Button btn 								= but.gameObject.GetComponentInChildren<Button>();

					but.commandButtonData.CommandName		= ((CommandTypes)dat.CommandType).ToString("g");
                    buttonText.text 						= ((CommandTypes)dat.CommandType).ToString("g");

                    if (but.CommandAOE == null)
                        but.CommandAOE = but.image.GetComponentInChildren<Text>();

                    but.CommandAOE.text = "";
                    but.CommandAOE.gameObject.SetActive(false);

					switch(but.commandButtonData.CommandType){
					
                        case (int)CommandTypes.None: 
                            btn.interactable = false;
                            but.commandButtonData.CommandName = "None";
                            buttonText.text = "None";
                            DisabledButton(buttonText, true);
                            but.image.sprite = _ButtonHelper.DefaultTransparant;
//                            SetDefaultIcon(but);
    						break;
    					case (int)CommandTypes.Attack:
    						if(!battler.IsActionNegated((CommandTypes)but.commandButtonData.CommandType)){
    							if (attackAble) {
							    	if (battler.AttackType == AttackType.Melee && _TargetController.checkAllyFrontBattler (battler)) {
                                        btn.interactable = false;
    									but.commandButtonData.Description	= "Unable to attack on THIS POSITION";
                                        DisabledButton(buttonText, true);
                                       } 
                                    else {
    									btn.interactable = true;
                                        but.commandButtonData.Description	= "Normal attack with single target";
                                        DisabledButton(buttonText, false);
								    }
							    } else {
    								btn.interactable = false;
                                    but.commandButtonData.Description	= "Unable to attack";
                                    DisabledButton(buttonText, true);
							    }
						    }
                            else 
                            {
                                battler.PopUp("Negated ATTACK", (int)BattleTextPopUp.BattleTextType.Debuff, false);
    							btn.interactable = false;
                                but.commandButtonData.Description	= "DEBUFFED Unable to attack";
                                DisabledButton(buttonText, true);
    						}
						break;
                        case (int)CommandTypes.Change : 
                            but.commandButtonData.Description   = "Swap Party Members";
    						if(Battle.Instance.reserveList.Count > 0 && !battler.onChangeCommand && !battler.isCombination){
                                btn.interactable = true;
                                DisabledButton(buttonText, false);
                                //check important
    						}else{
                                btn.interactable = false;
                                DisabledButton(buttonText, true);
    						}
    						break;
    					case (int)CommandTypes.Flee: 
    						if (!fleeAble) {
                                btn.interactable = false;
                                DisabledButton(buttonText, true);
                                but.commandButtonData.Description	= "Can't escape";
    						} else {
                                btn.interactable = true;
                                DisabledButton(buttonText, false);
    							but.commandButtonData.Description	= "Flee From Battle";
    						}
    						break;
                        case (int)CommandTypes.Skill:
                            NormalSkill skill = mainChar.GetNormalSkill(dat.CommandValue);

                            if (skill != null)
                            {
                                SetNormalSkillIcon(but, skill.SkillType, false);
                                SetCommandAOE(skill.Target, but);
                                but.commandButtonData.CommandName = skill.Name;
                                buttonText.text = skill.Name;
                                but.commandButtonData.Description = skill.Description;
                                btn.interactable = true;
                                DisabledButton(buttonText, false);
                            }
                            else
                            {
                                but.commandButtonData.CommandName = "None";
                                buttonText.text = "None";
                                but.commandButtonData.Description = "None";
                                btn.interactable = false;
                                DisabledButton(buttonText, true);
                                but.image.sprite = _ButtonHelper.DefaultTransparant;
                            }
                        break;
                        case (int)CommandTypes.Magic: 
                            if (mainChar.Magics.MagicPresets.Count > 0)
                            {
                                CharacterMagicPreset cmp = mainChar.Magics.GetMagicPreset(dat.CommandValue);
                                if (cmp != null)
                                {
                                    Magic magic = cmp.MagicPreset;
                                    but.commandButtonData.CommandName = magic.Name;
                                    buttonText.text = magic.Name;
                                    but.commandButtonData.Description	= magic.Description;

                                    SetCommandAOE(magic.MagicTargetType, but);
                                    if (!battler.IsActionNegated((CommandTypes)but.commandButtonData.CommandType))
                                    {
                                        if (battler.AttackType == AttackType.Melee && _TargetController.checkAllyFrontBattler(battler))
                                        {
                                            SetMagicIcon(but, magic.Element, true);
                                            btn.interactable = false;
                                            DisabledButton(buttonText, true);
                                        }
                                        else
                                        { 
                                            SetMagicIcon(but, magic.Element, false);
                                            btn.interactable = true; 
                                            DisabledButton(buttonText, false);
                                        }
                                    }
                                    else
                                    {
                                        battler.PopUp("Negated MAGIC", (int)BattleTextPopUp.BattleTextType.Debuff, false);
                                        btn.interactable = false;
                                        SetMagicIcon(but, magic.Element, true);
                                        DisabledButton(buttonText, true);
                                    }
                                }
                                else
                                {
                                    btn.interactable = false;
                                    but.image.sprite = _ButtonHelper.DefaultTransparant;
                                    but.commandButtonData.CommandName = "None";
                                    buttonText.text = "None";
                                    DisabledButton(buttonText, true);
                                }
                            }
                            else
                            {
                                btn.interactable = false;
                                but.commandButtonData.CommandName = "None";
                                buttonText.text = "None";
                                DisabledButton(buttonText, true);
                            }
    					break;
                        case (int)CommandTypes.Item: 
                            ItemSlot item = mainChar.Items[dat.CommandValue];
                            int stack = 0; 
                            if (item != null && item.ItemMaster != null)
                            {
                                PlayersItems.TryGetValue(item.ItemMaster._ID, out stack);
                            
//                            item.Stack = stack;
                                Consumable items = battler.GetItem(but.commandButtonData.CommandValue) as Consumable;
                                if (items != null)
                                    SetCommandAOE(items.Target, but);
                                if (stack == 0)
                                {
                                    btn.interactable = false;
                                    DisabledButton(buttonText, true);
                                }
                                else
                                {
                                    btn.interactable = true;
                                    DisabledButton(buttonText, false);
                                }

                                but.commandButtonData.CommandName = item.ItemMaster.Name + " x " + stack;
                                but.commandButtonData.Description = item.ItemMaster.Description;
                              
                                buttonText.text = but.commandButtonData.CommandName;
                            }
                            else
                            {
                                btn.interactable = false;
                                DisabledButton(buttonText, true);
                                but.commandButtonData.CommandName = "None";
                                but.commandButtonData.Description = "";
                            }
                            break;
                        case (int)CommandTypes.SkillUlti: 
                            if (dat.SlotPos == 0 && mainChar.BattleOffensiveSkill.Name != "None")
                            {
                                buttonText.text = mainChar.BattleOffensiveSkill.Name;
                                but.commandButtonData.CommandName = mainChar.BattleOffensiveSkill.Name;
                                but.commandButtonData.Description	= mainChar.BattleOffensiveSkill.Description;
                                but.commandButtonData.CommandValue = mainChar.BattleOffensiveSkill.CommandValue;

                                if (battler.GaugeCharge >= mainChar.BattleOffensiveSkill.Cost )//&& battler.SkillController != null
                                {
                                    if (battler.AttackType == AttackType.Melee && _TargetController.checkAllyFrontBattler(battler))
                                    {
                                        btn.interactable = false; 
                                        DisabledButton(buttonText, true);
                                    }
                                    else
                                    {
                                        btn.interactable = true; 
                                        DisabledButton(buttonText, false);
                                    }
                                }
                                else
                                {
                                    btn.interactable = false;
                                    DisabledButton(buttonText, true);
                                }     
                                but.image.sprite = _ButtonHelper.DefaultAttack;       
                            }
//                            else if (dat.SlotPos == 2 && mainChar.BattleDefensiveSkill.Name != "None")
//                            {
//                                buttonText.text = mainChar.BattleDefensiveSkill.Name;
//                                but.commandButtonData.CommandName = mainChar.BattleDefensiveSkill.Name;
//                                but.commandButtonData.Description	= mainChar.BattleDefensiveSkill.Description;
//
//                                if (mainChar.BattleDefensiveSkill.Cost <= battler.GaugeCharge)
//                                {
//                                    btn.interactable = true;
//                                    DisabledButton(buttonText, false); 
//                                }
//                                else
//                                {
//                                    btn.interactable = false;
//                                    DisabledButton(buttonText, true);
//                                }
//
//                                but.image.sprite = _ButtonHelper.DefaultAttack;   
//                            }
//                            else if (dat.SlotPos == 3 && mainChar.BattleComboSkill.Count > 0)
//                            {
//                                buttonText.text = "Combination Skill";
//                                but.commandButtonData.CommandName = "Combination Skill";
//                                but.commandButtonData.Description = "Combination Skill Attack";
//
//                                if (battler.GaugeCharge == 100)
//                                {
//                                    btn.interactable = true; 
//                                    DisabledButton(buttonText, false);
//                                }
//                                else
//                                {
//                                    btn.GetComponent<Image>().DOColor(new Color(1, 0, 0, 1f), 0f);
//                                    btn.interactable = false;
//                                    DisabledButton(buttonText, true);
//                                }
//                            }
                            else
                            {
                                buttonText.text = "None";
                                but.commandButtonData.CommandName = "None";
                                but.commandButtonData.Description   = "None";
                                but.image.sprite = _ButtonHelper.DefaultTransparant;
                                btn.interactable = false;
                                DisabledButton(buttonText, true);
                            }
    						break;
    					case (int)CommandTypes.Move:
                            btn.interactable = true;
                            DisabledButton(buttonText, false);
                            but.commandButtonData.Description	= "Move Position Battle";
    						break;
                        case (int)CommandTypes.Guard:
                            btn.interactable = true;
                            DisabledButton(buttonText, false);
                            if (mainChar.GuardSkill.Description != "")
                                but.commandButtonData.Description = mainChar.GuardSkill.Description;
                            else
                                but.commandButtonData.Description	= "Reduce Incoming Damage and Charge AP";

    						break;
    					default :
                            btn.interactable = true;
                            DisabledButton(buttonText, false);
						break;
					}

                    if (dat.Important)
                        ImportantCommand.Add (dat);

                    if (!dat.Active)
                    {
                        btn.interactable = false;
                        DisabledButton(buttonText, true);
                    }
                }
            }
		}
		setAllPanel(false);
		_MainPanel.SetActive(true);
		commandAni.SetBool("ShowMain", true);
        CommandImportantCheck(true);
		reqInput (true);
	}
    void CommandImportantCheck(bool MainPanelShow, bool isHideAll = false){
        _ButtonHelper.up.SetActive(false);
        _ButtonHelper.right.SetActive(false);
        _ButtonHelper.down.SetActive(false);
        _ButtonHelper.left.SetActive(false);
        if (isHideAll)
        {
            return;
        }
        if (ImportantCommand!=null && ImportantCommand.Count > 0)
        {
            for (int i = 0; i < ImportantCommand.Count; i++)
            {
                if (MainPanelShow)
                {
                    switch (ImportantCommand[i].PanelPos)
                    {
                        case 0:
                            _ButtonHelper.up.SetActive(true);
                            break;
                        case 1:
                            _ButtonHelper.right.SetActive(true);
                            break;
                        case 2:
                            _ButtonHelper.down.SetActive(true);
                            break;
                        case 3:
                            _ButtonHelper.left.SetActive(true);
                            break;
                    }
                }
                else
                {
                    switch (ImportantCommand[i].SlotPos)
                    {
                        case 0:
                            _ButtonHelper.up.SetActive(true);
                            break;
                        case 1:
                            _ButtonHelper.right.SetActive(true);
                            break;
                        case 2:
                            _ButtonHelper.down.SetActive(true);
                            break;
                        case 3:
                            _ButtonHelper.left.SetActive(true);
                            break;
                    }
                }
            }
        }
    }
    void SetCommandAOE(TargetType targetType, CommandButton commandButton)
    {
        if (commandButton.CommandAOE == null)
            commandButton.CommandAOE = commandButton.image.GetComponentInChildren<Text>();
        commandButton.CommandAOE.gameObject.SetActive(false);
        switch (targetType)
        {
            case TargetType.Single:
                commandButton.CommandAOE.gameObject.SetActive(true);
                commandButton.CommandAOE.text = "S";
                break;
            case TargetType.Row:
                commandButton.CommandAOE.gameObject.SetActive(true);
                commandButton.CommandAOE.text = "R";
                break;
            case TargetType.Column:
                commandButton.CommandAOE.gameObject.SetActive(true);
                commandButton.CommandAOE.text = "C";
                break;
            case TargetType.All:
                commandButton.CommandAOE.gameObject.SetActive(true);
                commandButton.CommandAOE.text = "A";
                break;
        }
    }
    void DisabledButton(Text btn, bool isDisabled){
        if (isDisabled)
            btn.color = new Color(1, 1, 1, 0.5f);
        else
            btn.color = new Color(1, 1, 1, 1);
            
    }
	public void hideAll(){
		KillTween ();
		_confirmMainPanel = false;
		_ButtonHelper.DescriptionPanel.SetActive (false);
		onGetInput = false;
		setAllFalse ();
		reqInput (false);
		dir = -1;
		_SelectKey = -1;
		commandAni.SetBool("HideAll", true);



	}

	private void setAllFalse(){
		commandAni.SetBool("ShowUp" ,false);
		commandAni.SetBool("ShowRight" ,false);
		commandAni.SetBool("ShowDown" ,false);
		commandAni.SetBool("ShowLeft" ,false);
		commandAni.SetBool("ShowMain" ,false);

	}

	public void setAllPanel (bool value){
		_MainPanel.SetActive(value);
		_UpPanel.SetActive(value);
		_RightPanel.SetActive(value);
		_DownPanel.SetActive(value);
		_LeftPanel.SetActive(value);

	}

	private void reqInput(bool req){
		if(req)
			battleInput.onReqInput += HandleonReqInput;
		else
			battleInput.onReqInput -= HandleonReqInput;
	}

	private void HandleonReqInput (int key)
	{
		if (key == (int)BattleInputKey.CANCEL) {
			if(dir >= 0){
				WwiseManager.Instance.PlaySFX(WwiseManager.UIFX.Cancel);
				hideAll();
				reqInput(true);
				setAllPanel(false);
				_MainPanel.SetActive(true);
				commandAni.SetBool("ShowMain", true); 
                CommandImportantCheck(true);
//				HandleonReqInput (1);
			}else{
				if(OnCommandCancel != null){
					WwiseManager.Instance.PlaySFX(WwiseManager.UIFX.Cancel);
                    OnCommandCancel();
//                    CommandImportantCheck(true, true);
				}
			}
		}else{
			setCommand(key);
		}
	}

	private void setCommand(int key){
		KillTween ();
		if (key <= 3) {
			WwiseManager.Instance.PlaySFX (WwiseManager.UIFX.Cursor_Moving);
//			dir = key;
			switch (key) {
			case 0:
				_OverlayTween = _ButtonHelper.UpOverlay.DOColor (new Color (1, 1, 1, 0.5f), 0.5f).SetLoops (-1, LoopType.Yoyo);
				break;
			case 1:
				_OverlayTween = _ButtonHelper.RightOverlay.DOColor(new Color (1, 1, 1, 0.5f), 0.5f).SetLoops(-1, LoopType.Yoyo);
				break;
			case 2:
				_OverlayTween = _ButtonHelper.DownOverlay.DOColor(new Color (1, 1, 1, 0.5f), 0.5f).SetLoops(-1, LoopType.Yoyo);
				break;
			case 3:
				_OverlayTween = _ButtonHelper.LeftOverlay.DOColor(new Color (1, 1, 1, 0.5f), 0.5f).SetLoops(-1, LoopType.Yoyo);
				break;
			}

			if (key == _SelectKey)
				HandleonReqInput (4);
			
			if (_confirmMainPanel)
				getDesciption (dir, key);
			else
				GetMainPanelDesc (key);
			
			onGetInput = true;

			
			_SelectKey = key;

		} 
		else if (key == 4 && onGetInput){
			_confirmMainPanel = true;
			if (dir < 0) {
				dir = _SelectKey;
				commandAni.SetBool ("ShowMain", false);
				commandAni.SetBool ("HideAll", false);
				switch (_SelectKey) {
                    case 0:
                        _MainPanel.SetActive(false);
                        _UpPanel.SetActive(true);
                        commandAni.SetBool("ShowUp", true);
					break;
				case 1:
					_MainPanel.SetActive (false);
					_RightPanel.SetActive (true);
                    commandAni.SetBool ("ShowRight", true);
					break;
				case 2:
					_MainPanel.SetActive (false);
					_DownPanel.SetActive (true);
                    commandAni.SetBool ("ShowDown", true);
					break;
				case 3:
					_MainPanel.SetActive (false);
					_LeftPanel.SetActive (true);
                    commandAni.SetBool ("ShowLeft", true);
					break;
				}
                if (ImportantCommand != null && ImportantCommand.Count > 0 && ImportantCommand[0].PanelPos == dir)
                    CommandImportantCheck(false);
                else
                    CommandImportantCheck(true, true);
                    
				onGetInput = false;
				_SelectKey = -1;
				HandleonReqInput (dir);
			} else {
				getButton (dir, _SelectKey);
			}
//			KillTween ();
            if (!isDescription)
			    _ButtonHelper.DescriptionPanel.SetActive (false);
		}
	}

	private void GetMainPanelDesc (int key){
		MainPanelDesc[] mainPanelDesc = _MainPanel.GetComponentsInChildren<MainPanelDesc> ();

        for (int i = 0; i < mainPanelDesc.Length; i++){ 
            if (mainPanelDesc[i].Index == key) {
				_ButtonHelper.DescriptionPanel.SetActive (true);
                _ButtonHelper.Description.text = mainPanelDesc[i].Description;
                _ButtonHelper.SelectCommand.SetActive(true);
                _ButtonHelper.SelectCommand.transform.SetParent(mainPanelDesc[i].SelectedParents, false);
			}
		}
	}
	private void getButton(int panel, int slot){
		button = buttonArr.Find (x => x.commandButtonData.PanelPos == panel && x.commandButtonData.SlotPos == slot);
		
		CommandTypes commandType 	= (CommandTypes)button.commandButtonData.CommandType;
		Button btn 					= button.gameObject.GetComponentInChildren<Button> ();

		if (!commandType.Equals (CommandTypes.None) && btn.interactable) {
            hideAll ();
            CommandImportantCheck(true, true);

			if (OnCommandInput != null) {
				OnCommandInput (button);
				button = null;
			}
		}
	}

	private void getDesciption (int panel, int slot){
		CommandButton but = buttonArr.Find (x=> x.commandButtonData.PanelPos == panel && x.commandButtonData.SlotPos == slot);
		CommandTypes commandType 	= (CommandTypes)but.commandButtonData.CommandType;
		Button btn 					= but.gameObject.GetComponentInChildren<Button> ();

        _ButtonHelper.SelectCommand.SetActive(true);
        _ButtonHelper.SelectCommand.transform.SetParent(but.SelectedParents, false);

        if (but.commandButtonData.Description != "" && !commandType.Equals (CommandTypes.None)) {
            _ButtonHelper.DescriptionPanel.SetActive (true);
			_ButtonHelper.Description.text = but.commandButtonData.Description;
            if (btn.interactable == false)
                KillTween();
            isDescription = true;
        } else {
            KillTween();
			_ButtonHelper.DescriptionPanel.SetActive (false);
            isDescription = false;
		}
	}


    private void KillTween(){
		commandPanel.GetComponent<ButtonOverlay>().UpOverlay.DOColor(new Color (1,1,1,0), 0f);
		commandPanel.GetComponent<ButtonOverlay>().RightOverlay.DOColor(new Color  (1,1,1,0), 0f);
		commandPanel.GetComponent<ButtonOverlay>().LeftOverlay.DOColor(new Color (1,1,1,0), 0f);
		commandPanel.GetComponent<ButtonOverlay>().DownOverlay.DOColor(new Color (1,1,1,0), 0f);
		if (_OverlayTween != null)
			_OverlayTween.Kill (true);
	} 

    private void SetMagicIcon (CommandButton but, Element element, bool disable){
        if (element != null)
        {
            Sprite sprite = LegrandBackend.Instance.GetSpriteData("Element" + element.ToString());
            but.image.sprite = sprite;
            if (!disable)
                but.image.color = new Color(1, 1, 1, 1);
            else
                but.image.color = new Color(1, 1, 1, 0.5f);
        }
    }
    private void SetNormalSkillIcon (CommandButton but, SkillTypes skillType, bool disable){
        if (skillType != null)
        {
            switch (skillType)
            {
                case SkillTypes.NormalDefense:
                    Sprite buffSprite = LegrandBackend.Instance.GetSpriteData("SkillBuff");
                    but.image.sprite = buffSprite;
                    break;
                case SkillTypes.NormalOffense:
                    Sprite debuffSprite = LegrandBackend.Instance.GetSpriteData("SkillDebuff");
                    but.image.sprite = debuffSprite;
                    break;
            }
            if (!disable)
                but.image.color = new Color(1, 1, 1, 1);
            else
                but.image.color = new Color(1, 1, 1, 0.5f);
        }
    }
    private void SetDefaultIcon(CommandButton but){
        Mask x = but.GetComponent<Mask>();
        if (x != null)
        {
            but.GetComponent<Mask>().enabled = false;
            but.image.gameObject.SetActive(false);
        }
    }
}
