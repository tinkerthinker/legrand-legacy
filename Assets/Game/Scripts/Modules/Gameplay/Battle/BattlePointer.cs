﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BattlePointer : MonoBehaviour {
	[Range(0f,100f)]
	public float MidPercentage = 50f;

	[Range(0f,100f)]
	public float LowPercentage = 20f;

	public SpriteRenderer CrystalImage;

	public Sprite NormalSprite;
	public Sprite MidSprite;
	public Sprite LowSprite;

	public void InitPointer(float currentHealth, float maxHealth)
	{
		SetCystalSprite((currentHealth/maxHealth) *100f);
	}

	private void SetCystalSprite(float healthPercentage)
	{
		if(healthPercentage <= LowPercentage) CrystalImage.sprite = LowSprite;			// value <= 20%
		else if(healthPercentage > MidPercentage) CrystalImage.sprite = NormalSprite;	// value > 50%
		else CrystalImage.sprite = MidSprite;											// 20% < value <= 50%
	}
}
