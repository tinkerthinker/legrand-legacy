﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TimedHitEvent : GameEvent{
	public Battler Attacker;
	public Battler[] Defenders;
	public BattleCommand command;

	public TimedHitEvent(Battler attacker, List<Battler> defenders, BattleCommand c){
		this.Attacker = attacker;
		this.Defenders = defenders.ToArray();
		this.command = c;
	}
}
public class TimedDefEvent : GameEvent{
	public Battler Attacker;
	public Battler Defender;
	
	public TimedDefEvent(Battler attacker, Battler defender){
		this.Attacker = attacker;
        this.Defender = defender;
        Attacker.isTimeHit = true;
	}
}

public class TimedHitFinishedEvent : GameEvent{
	public Battler Attacker;
	public Battler[] Defenders;
	
	public TimedHitFinishedEvent(Battler attacker, Battler[] defenders){
		this.Attacker = attacker;
		this.Defenders = defenders;
	}
}
