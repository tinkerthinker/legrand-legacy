﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GenerateTurnEvent : GameEvent {
	public List<Battler> battlers;
	
	public GenerateTurnEvent(List<Battler> b){
		this.battlers = b;
	}
}
public class InitTurnEvent : GameEvent {
	public List<Battler> battlers;
	
	public InitTurnEvent(List<Battler> b){
		this.battlers = b;
	}
}