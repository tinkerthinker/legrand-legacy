﻿using UnityEngine;
using System.Collections;

public class CastEvent : GameEvent {
	public BattleCommand command;

	public CastEvent(BattleCommand c){
		this.command = c;
	}
}
public class StartCastEvent : GameEvent {
	public BattleCommand command;
	
	public StartCastEvent(BattleCommand c){
		this.command = c;
	}
}
public class EndCastEvent : GameEvent {
	public BattleCommand command;
	public GameObject magic;
	
	public EndCastEvent(BattleCommand c, GameObject m){
		this.command = c;
		this.magic = m;
	}
}
