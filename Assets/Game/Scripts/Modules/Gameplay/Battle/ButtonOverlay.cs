﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ButtonOverlay : MonoBehaviour {
	public Image UpOverlay;
	public Image RightOverlay;
	public Image DownOverlay;
	public Image LeftOverlay;

	public GameObject DescriptionPanel;
	public Text Description;
    public Sprite DefaultTransparant;
    public GameObject SelectCommand;
//    public Animator ImportantButton;
    public Sprite DefaultAttack;

    public GameObject up;
    public GameObject right;
    public GameObject down;
    public GameObject left;
}
