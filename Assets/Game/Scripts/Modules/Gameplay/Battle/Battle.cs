﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using Legrand.core;
using Legrand.database;

public class Battle : MonoBehaviour {
	#region Singleton Handling
	private static Battle _Instance;
	public static Battle Instance
	{
		get
		{
			if (_Instance == null)
			{
				_Instance = GameObject.FindObjectOfType<Battle>();
			}
			return _Instance;
		}
	}
	#endregion
	private string monsPath = "Characters/Encounters/";
	private List<Battler> _battlerList 		= new List<Battler>();
	private List<Battler> _charList 		= new List<Battler>();
	private List<Battler> _reserveList 		= new List<Battler>();
	private List<Battler> _encounterList 	= new List<Battler>();

	public List<Battler> battlerList
	{
		get {return _battlerList;}
		set 
		{
			_battlerList = value;
			_charList			= _battlerList.Where(x => x.BattlerType == BattlerType.Player).ToList();
			_encounterList 		= _battlerList.Where(x => x.BattlerType == BattlerType.Enemy).ToList();
		}
	}
	public List<Battler> charList{
		get {return _charList;}
	}
	public List<Battler> reserveList{
		get {return _reserveList;}
	}
	public List<Battler> encounterList{
		get {return _encounterList;}
	}
	private string _encounterPartyID = "";
	public string encounterPartyID
	{
		get {return _encounterPartyID;}
		set {_encounterPartyID = value;}
	}
	private int battleID = 0;

	private List<BattlePlane> _playerPlanes = new List<BattlePlane>();
	public List<BattlePlane> playerPlanes
	{
		get { return _playerPlanes;}
	}
	private List<BattlePlane> _enemyPlanes = new List<BattlePlane>();
	public List<BattlePlane> enemyPlanes
	{
		get { return _enemyPlanes;}
	}

	private TransitionData _transData;
	public TransitionData transData
	{
		get {return _transData;}
		set {_transData = value;}
	}
	private bool _fleeAble;
	public bool fleeAble
	{
		get {return _fleeAble;}
	}




	[SerializeField]
	public BattleCameraController BattleCameraController;
    public BattleInput BattleInputController;
	private LegrandBackend backend;
	private PartyManager partyManager;
	private Database dB;

	[SerializeField]
	private GameObject PlayerPlaneGroup;
	[SerializeField]
	private GameObject EnemyPlaneGroup;
	[SerializeField]
	private ScreenFader screenFader;
	public ScreenFader ScreenFader {
		get {return screenFader;}
	}

	private GameObject _CommandPanelGroup;
	public GameObject CommandPanelGroup {
		get {return _CommandPanelGroup;}
	}

//    [SerializeField]
    [HideInInspector]
    public StatusPanel StatusPanelGroup;
//    [SerializeField]
    private GameObject TimedHitPanelGroup;
//    [SerializeField]
    private GameObject MainStatusGroup;
//    [SerializeField]
    private GameObject ReserveStatusGroup;
    [HideInInspector]
	public ActionPopUp ActionBar;

	private BattleController controller;
	public BattleController Controller {
		get {return controller;}
	}
	private BattleAICommand _BattleAICommand;
	public BattleAICommand battleAICommand{
		get {return _BattleAICommand;}
	}
	private Transform controllerTrans;

	private List<StatusBar> mainHealthBar = new List<StatusBar>();
	private List<StatusBar> reserveHealthBar = new List<StatusBar>();

	private List<TimedHitController> timedHits = new List<TimedHitController>();

	private EncounterInfo[] _EncounterEditor;

    public delegate void BattleReady();
    public event BattleReady OnStateChange;

    [HideInInspector]
    public GameOverBar GameOverScreen;
    [HideInInspector]
    public GameObject ResultScreen;
    // Use this for initialization
    public void StartBattle (string partyID, TransitionData transData, int ambushStats) {
		_Instance = this;
	//		BattleCameraController = GameObject.FindGameObjectWithTag ("BattleCamera").GetComponent<BattleCameraController> ();
	//		BattleCameraController = gameObject.GetComponentInChildren<BattleCameraController>();
		backend = LegrandBackend.Instance;
		partyManager = PartyManager.Instance;
		dB = backend.LegrandDatabase;
		_encounterPartyID = partyID;
		_transData = transData;

        if (StatusPanelGroup == null)
        {
            GameObject g = GameObject.Instantiate<GameObject>(Resources.Load<GameObject>("Battle/BattleUI/StatusPanel"));
            g.transform.SetParent(this.transform);

            StatusPanelGroup = g.GetComponent<StatusPanel>();

            TimedHitPanelGroup = StatusPanelGroup.TimeHitGroup;
            MainStatusGroup = StatusPanelGroup.StatusBarGroup;
            ReserveStatusGroup = StatusPanelGroup.ReserveBarGroup;
            ActionBar = StatusPanelGroup.ActionBar;
            GameOverScreen = StatusPanelGroup.GameOverScreen;
        }
        if (_CommandPanelGroup == null)
        {
            _CommandPanelGroup = GameObject.Instantiate<GameObject>(Resources.Load<GameObject>("Battle/BattleUI/CommandPanel"));
            _CommandPanelGroup.transform.SetParent(this.transform);
        }

		_playerPlanes = PlayerPlaneGroup.GetComponentsInChildren<BattlePlane> ().ToList();

		//TODO: Defined from party ID plane group prefab
		_enemyPlanes = EnemyPlaneGroup.GetComponentsInChildren<BattlePlane> ().ToList();

		mainHealthBar = MainStatusGroup.GetComponentsInChildren<StatusBar> ().ToList ().FindAll (x => x.transform.parent.Equals(MainStatusGroup.transform));
//		mainHealthBar.ForEach (x => x.gameObject.SetActive (false));
        for (int i = 0; i < mainHealthBar.Count; i++)
            mainHealthBar[i].gameObject.SetActive(false);
		reserveHealthBar = ReserveStatusGroup.GetComponentsInChildren<StatusBar> ().ToList ().FindAll (x => x.transform.parent.Equals(ReserveStatusGroup.transform));
//		reserveHealthBar.ForEach (x => x.gameObject.SetActive (false));
        for (int i = 0; i < reserveHealthBar.Count; i++)
            reserveHealthBar[i].gameObject.SetActive(false);

		timedHits = TimedHitPanelGroup.GetComponentsInChildren<TimedHitController> ().ToList ();

        for (int i = 0; i < timedHits.Count; i++)
        {
            timedHits[i].battleInput = BattleInputController;
            timedHits[i].gameObject.SetActive(false);
        }

		controller = GetComponentInChildren<BattleController> ();
		_BattleAICommand = GetComponentInChildren<BattleAICommand> ();
		controllerTrans = controller.transform;
        controller.StatusInitBattle = (InitBuff)ambushStats;

//        GameObject resultScreen = Instantiate(ResultScreen, ResultScreen.transform.position, Quaternion.identity) as GameObject;
//        resultScreen.transform.SetParent(this.transform);

        if (ResultScreen == null)
        {
            ResultScreen = GameObject.Instantiate<GameObject>(Resources.Load<GameObject>("Battle/BattleUI/ResultScreen"));
            ResultScreen.transform.SetParent(this.transform);
        }

		initBattle();
	}

	public void StartBattle (string partyID, TransitionData transData, EncounterInfo[] encounterEdit) {
		_EncounterEditor = encounterEdit;
		StartBattle (partyID, transData, 0);
	}

	private void initBattle(){
		bool found = false;
		int index = 0;
		
		while (!found && index < dB.EncounterPartyData.Count)
		{
			if (dB.EncounterPartyData.Get(index)._ID.Equals(encounterPartyID, System.StringComparison.OrdinalIgnoreCase))
			{
				controller.BattleSong = dB.EncounterPartyData.Get(index).Music;
				int viewIndex = (int)dB.EncounterPartyData.Get(index).View;
				if (viewIndex == 0) {
					BattleCameraController.Views = BattleCameraController.NormalViews;
					BattleCameraController.isMassive = false;
				} else if (viewIndex == 1) {
					BattleCameraController.Views = BattleCameraController.MassiveViews;
					BattleCameraController.isMassive = true;
				}
				_fleeAble = dB.EncounterPartyData.Get(index).CanFlee;
				controller.ExperienceGain = dB.EncounterPartyData.Get (index).ExperiencePoint;
				BattleCameraController.PartyType = viewIndex;
				found = true;
			}

			if (!found)
				index++;
		}

		if (found) {
			loadBattler(index);
		} else {
			Debug.LogError ("Party "+ encounterPartyID +" ID not found");
		}
	}
	//Set atribute @scene
	private void loadBattler(int encounterPartyDBIndex){
		//sorting mainchart in party manager by position
		//		partyManager.CharacterParty = SortMainCharacter(partyManager.CharacterParty);
		LoadCharacter();
		LoadEncounter(encounterPartyDBIndex);

		_encounterList = SortByPosition (_encounterList);

		int averagePartyLevel = Formula.GetAveragePartyLevel (_charList);
		int averageEncounterLevel = Formula.GetAveragePartyLevel (_encounterList);

		int fleeChance = 50;
		if (averagePartyLevel - averageEncounterLevel < -5)
			fleeChance = 20;
		else if (averagePartyLevel - averageEncounterLevel > 5)
			fleeChance = 80;

        for (int i = 0; i < _battlerList.Count; i++) // each (Battler battler in _battlerList)
            _battlerList[i].FleeChance = fleeChance;

		Invoke("initController", 1f);
//        initController();
	}

	private void LoadCharacter()
	{
		//	partyManager.CharacterParty = SortMainCharacter(partyManager.CharacterParty);
		List <MainCharacter> newCharList = new List<MainCharacter>(partyManager.CharacterParty); 
		newCharList = SortMainCharacter(newCharList);
		int iMain = 0;
		int iResv = 0;

        for (int i = 0; i < newCharList.Count; i++){ // each (MainCharacter mainChar in newCharList) {
            MainCharacter mainChar = newCharList[i];
			GameObject charGo = mainChar.gameObject;


			Battler battler 		= charGo.AddComponent<Battler> ();

			battler.Character 		= mainChar;
			battler.BattlerType 	= BattlerType.Player;
			battler.Anim			= battler.GetComponentInChildren<Animator> ();
			battler.ActionBar 		= ActionBar;
			//???
			//battler.AttackType 		= (mainChar._BattleClass.ClassType == BattleClass.Archer) ? AttackType.Range : AttackType.Melee;

			if (mainChar._BattleClass.ClassType == BattleClass.Archer || mainChar._BattleClass.ClassType == BattleClass.Warrior || mainChar._BattleClass.ClassType == BattleClass.Warlord){
				battler.AttackType = AttackType.Range;
			}
			else if (mainChar._BattleClass.ClassType == BattleClass.Fighter || mainChar._BattleClass.ClassType == BattleClass.Warrior || mainChar._BattleClass.ClassType == BattleClass.Warlord){
				battler.AttackType = AttackType.Melee;
			}
			else if (mainChar._BattleClass.ClassType == BattleClass.Apprentice || mainChar._BattleClass.ClassType == BattleClass.Mage || mainChar._BattleClass.ClassType == BattleClass.Sorceress){
				battler.AttackType = AttackType.Caster;
			}

			BattleFormation form = partyManager.BattleFormations.Formations.Find (p => p.Character.Equals (mainChar));

			if(form != null){
				battler.BattleID 		= battleID;
				battler.TimedHit 		= timedHits [battleID];

				BattlePlane plane 		= _playerPlanes.Find (p => p.planePos.Equals (form.Position));
				plane.placeAble 		= false;

				Transform planeTrans 	= plane.transform;
				Transform charTrans 	= mainChar.transform;

				charGo.SetActive (true);

				battler.HealthBar 		= mainHealthBar [iMain];
                battler._BuffBar = battler.HealthBar.BuffBar;
				mainHealthBar [iMain].gameObject.SetActive (true);
                mainHealthBar [iMain].initStatusBar (mainChar, false, battler._BuffBar);

				battler.BattlePosition 	= form.Position;

//				charTrans.position 		= planeTrans.position;
				charTrans.rotation 		= planeTrans.rotation;

				battler.BattleHelper = battler.GetComponent<BattleHelper>();
				if(battler.BattleHelper == null) Debug.LogError("Battle Helper Not Found!");
				else if(battler.BattleHelper.IsEmpty()) battler.BattleHelper.Search();

				Vector3 diff = planeTrans.position - battler.HitArea.position;
				diff.y = 0f;
				charTrans.position = charTrans.position + diff;

				charTrans.SetParent (controllerTrans);

				battler.initBattler ();

				_charList.Add (battler);
				_battlerList.Add (battler);
				battler.MyPlane = plane;
                battler.ActivateBattler(true);
				iMain++;
				battleID++;
			} else {
				battler.inReserve = true;
                battler.HealthBar 		= reserveHealthBar [iResv];
                battler._BuffBar = battler.HealthBar.BuffBar;
				//				reserveHealthBar [iResv].gameObject.SetActive (true);
                reserveHealthBar [iResv].initStatusBar (mainChar, true, null);

				reserveHealthBar [iResv].gameObject.SetActive (false);
				_reserveList.Add(battler);
				iResv++;
			}

			battler.BattlerInfo = battler.GetComponent<BattlerInfo>();
			battler.UpdateBattlerInfo();

		}
	}

	private void LoadEncounter(int dbIndex)
	{

		if (_EncounterEditor != null)
		{
			
            for (int i = 0; i < dB.EncounterPartyData.Get(dbIndex).Formations.Count; i++)// each (EncounterFormationTest Eform in dB.EncounterPartyData.Get(dbIndex).Formations)
			{
                EncounterFormationTest Eform = dB.EncounterPartyData.Get(dbIndex).Formations[i];
                for (int j = 0; j < _EncounterEditor.Length; j++) // each (EncounterInfo info in _EncounterEditor)
				{
                    EncounterInfo info = _EncounterEditor[j];
					if (Eform.Position.x == info.Formation.x && Eform.Position.y == info.Formation.y)
					{
						Eform.MonsterLvl = info.Level;
						break;
					}
				}
			}
			_EncounterEditor = null;
		}

        for (int i = 0; i < dB.EncounterPartyData.Get(dbIndex).Formations.Count; i++) { // each (EncounterFormationTest Eform in dB.EncounterPartyData.Get(dbIndex).Formations) {
            EncounterFormationTest Eform = dB.EncounterPartyData.Get(dbIndex).Formations[i];
            string partyID          = dB.EncounterPartyData.Get(dbIndex)._ID;
			BattlePlane Eplane      = _enemyPlanes.Find(p => p.planePos.Equals(Eform.Position));
			Eplane.placeAble        = false;
			Transform EplaneTrans   = Eplane.transform;
            //get data from resources character/encounter/enc0/(get monster id)
            string monsterPrefabName = "";
            if (string.IsNullOrEmpty(Eform.Monster.PrefabName)) monsterPrefabName = Eform.Monster.ID;
            else monsterPrefabName = Eform.Monster.PrefabName;
            Encounter e             = Resources.Load<Encounter>(monsPath + monsterPrefabName);

    		Encounter encounter     = Instantiate<Encounter>(e);
			Transform eTrans        = encounter.transform;
			GameObject eObject      = encounter.gameObject;

			//Instantiate monster 
            encounter.init (Eform.MonsterLvl, Eform, Eform.Monster.ID, partyID);

			eTrans.position         = EplaneTrans.position;
			eTrans.rotation         = EplaneTrans.rotation;

			eTrans.SetParent(controller.transform);
			Battler batEnc = new Battler ();
			if (encounter.enemyType == EnemyType.BossUnique) {
				// add battler script to enemy and set value
				switch (encounter._ID){
    				case "CB-01":
    					batEnc = eObject.AddComponent<KlahmaranBattler> ();
    					break;
    				case "CB-02":
    					batEnc = eObject.AddComponent<ArkworaBattler> ();
                        break;
                    case "HB-01":
                        batEnc = eObject.AddComponent<GladiatorBattler> ();
                        break;
                    case "HB-04-A-1":
                        batEnc = eObject.AddComponent<RogarBattler>();
                        break;
                    case "HB-07-A-1":
                        batEnc = eObject.AddComponent<SirCallahanBattler>();
                        break;
                    case "HB-03-A-1":
                        batEnc = eObject.AddComponent<GumbaladBattler>();
                        break;
                    case "HB-08-A-1":
                        batEnc = eObject.AddComponent<TheProtegeBattler>();
                        break;
                    default:
                        batEnc = eObject.AddComponent<Battler> ();
                        break;
				}

				batEnc.BattleID = battleID;
				batEnc.Character = encounter;
				batEnc.BattlerType = BattlerType.Enemy;
				batEnc.ActionBar = ActionBar;
				batEnc.MyPlane = Eplane;

				switch (encounter._EncounterClass) {
				case CharacterAttackType.Range:
					batEnc.AttackType = AttackType.Range;
					break;
				case CharacterAttackType.Melee:
					batEnc.AttackType = AttackType.Melee;
					break;
				case CharacterAttackType.Caster:
					batEnc.AttackType = AttackType.Caster;
					break;
				}
//					batEnc.AttackType = (encounter._EncounterClass == CharacterAttackType.Range) ? AttackType.Range : AttackType.Melee;
				batEnc.BattlePosition = Eform.Position;

				batEnc.BattleHelper = batEnc.GetComponent<BattleHelper> ();
				if (batEnc.BattleHelper == null)
					Debug.LogError ("Battle Helper Not Found!");
				else if (batEnc.BattleHelper.IsEmpty ())
					batEnc.BattleHelper.Search ();

				Vector3 diff = EplaneTrans.position - batEnc.HitArea.position;
				diff.y = 0f;
				EplaneTrans.position = EplaneTrans.position; //+diff

				batEnc.initBattler ();

				_encounterList.Add (batEnc);

				_battlerList.Add (batEnc);
				battleID++;

				batEnc.BattlerInfo = batEnc.GetComponent<BattlerInfo> ();
				batEnc.UpdateBattlerInfo ();

				if (!partyManager.WorldInfo.ExistMonsterKilled (Eform.Monster.ID)) {
					partyManager.WorldInfo.UpdateMonsterKilled (Eform.Monster.ID, 0);
					controller.EncounterFirstMeet.Add (batEnc);
				}
			} 
			else 
			{
                Battler battler = new Battler();
				// add battler script to enemy and set value
                if (encounter.enemyType == EnemyType.NormalUnique){
                    switch (encounter._ID){
                        case "HB-05-A-1":
                            battler = eObject.AddComponent<KaelBattler> ();
                            break;
                        case "HB-06-A-1":
                            battler = eObject.AddComponent<AzzamBattler> ();
                            break;
                        default:
                            battler = eObject.AddComponent<Battler> ();
                            break;
                    }
                } else
				    battler = eObject.AddComponent<Battler> ();
                
				battler.BattleID = battleID;
				battler.Character = encounter;
				battler.BattlerType = BattlerType.Enemy;
				battler.ActionBar = ActionBar;
				battler.MyPlane = Eplane;

				switch (encounter._EncounterClass) {
				case CharacterAttackType.Range:
					battler.AttackType = AttackType.Range;
					break;
				case CharacterAttackType.Melee:
					battler.AttackType = AttackType.Melee;
					break;
				case CharacterAttackType.Caster:
					battler.AttackType = AttackType.Caster;
					break;
				}
//					battler.AttackType = (encounter._EncounterClass == CharacterAttackType.Range) ? AttackType.Range : AttackType.Melee;
				battler.BattlePosition = Eform.Position;

				battler.BattleHelper = battler.GetComponent<BattleHelper> ();
				if (battler.BattleHelper == null)
					Debug.LogError ("Battle Helper Not Found!");
				else if (battler.BattleHelper.IsEmpty ())
					battler.BattleHelper.Search ();

				Vector3 diff = EplaneTrans.position - battler.HitArea.position;
				diff.y = 0f;
				EplaneTrans.position = EplaneTrans.position; //+diff

				battler.initBattler ();

				_encounterList.Add (battler);

				_battlerList.Add (battler);
				battleID++;

				battler.BattlerInfo = battler.GetComponent<BattlerInfo> ();
				battler.UpdateBattlerInfo ();

                if (!partyManager.WorldInfo.ExistMonsterKilled(Eform.Monster.ID))
                {
                    partyManager.WorldInfo.UpdateMonsterKilled(Eform.Monster.ID, 0);
                    controller.EncounterFirstMeet.Add(battler);
                }
            }
		}
	}
	private void initController(){
		controller.initBattle ();
	}
	//blm dipanggil
	public void swapCharacter(Battler main, int mainID, Battler reserve, int resvIndex){
		MainCharacter resvMain = _reserveList [resvIndex].Character as MainCharacter;
		MainCharacter swappedMain = main.Character as MainCharacter;

		//		reserve.gameObject.SetActive (true);
		reserve.MyPlane = main.MyPlane;
		reserve.BattleHelper = reserve.GetComponent<BattleHelper>();
		reserve.BattleID = main.BattleID;
		reserve.BattlePosition 	= main.BattlePosition;
		reserve.TimedHit = main.TimedHit;
        reserve._BuffBar = main._BuffBar;
        reserve._BuffBar.BuffImageList = reserve.BuffSprite;
        if (reserve.BuffSprite.Count == 0)
            reserve._BuffBar.BuffImageList = new List<BuffImage>();

		if(reserve.BattleHelper == null) Debug.LogError("Battle Helper Not Found!");
		else if(reserve.BattleHelper.IsEmpty()) reserve.BattleHelper.Search();


		StatusBar tempBar = _reserveList[resvIndex].HealthBar;

		_reserveList[resvIndex].HealthBar = main.HealthBar;
        _reserveList [resvIndex].HealthBar.initStatusBar (resvMain, false, main.HealthBar.BuffBar);

		main.HealthBar = tempBar;
        main.HealthBar.initStatusBar (swappedMain, true, null);

		//		resvTrans.position = startPos.position;
		reserve.transform.SetParent (controller.transform);

		main.transform.SetParent (partyManager.transform);

		reserve.initBattler ();
		//		reserve.inReserve = false;
		//		main.inReserve = true;
		reserve.onChangeCommand = true;

		//		_battlerList.Remove (main);
		//		_battlerList.Add (_reserveList [resvIndex]);

		//		_reserveList.Remove (_reserveList [resvIndex]);
		//		_reserveList.Add (main);

		_battlerList [mainID] 	 = reserve;
		_charList [mainID] 		 = reserve;
		_reserveList [resvIndex] = main;

		//		main.RemoveController ();
		//		main.gameObject.SetActive (false);
	}
	//Health bar etc set active false from battle controller
	public void SetStatusActive(bool active)
	{
		MainStatusGroup.SetActive (active);
	}

	public List<Battler> SortByPosition (List<Battler> battlerCharacter){
		List<Battler> battlerTemp = new List<Battler>();
		for (float y = 1; y >=0; y--){
			for (float x = 0; x <=2; x++){
				for (int index=0; index < battlerCharacter.Count;index++){
					if (battlerCharacter[index].BattlePosition.x == x
						&& battlerCharacter[index].BattlePosition.y == y){
						battlerTemp.Add (battlerCharacter[index]);
					}
				}
			}
		}

		return battlerTemp;

	}

	public List<MainCharacter> SortMainCharacter (List<MainCharacter> mainChars){
		List <MainCharacter> mainCharTemp = new List<MainCharacter>();
		for (float y = 1; y >=0; y--){
			for (float x = 0; x <=2; x++){
				for (int index=0; index < mainChars.Count;index++){
					BattleFormation form = partyManager.BattleFormations.Formations.Find (p => p.Character.Equals (mainChars[index]));
					if (form != null) {
						if (form.Position.x == x && form.Position.y == y) {
							mainCharTemp.Add (mainChars [index]);
						} 
					} else {
						if (!mainCharTemp.Contains (mainChars [index]))
							mainCharTemp.Add (mainChars [index]);
					}
				}
			}
		}

		return mainCharTemp;
	}
}