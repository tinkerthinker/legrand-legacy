﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class TimedHitListener : MonoBehaviour {
	private List<BattleCommand> timeHitCommands = new List<BattleCommand> ();
	private List<Battler> timedDefBattler = new List<Battler> ();

	private ArrayList timedButtonId = new ArrayList(){0, 1, 2, 3};
	private bool onTimeHit = false;
	public BattleCameraController battleCam;

	void Start(){
//		GameObject g = GameObject.FindGameObjectWithTag ("BattleCamera");
//		battleCam = GameObject.FindGameObjectWithTag ("BattleCamera").GetComponent<BattleCameraController> ();
//		battleCam = Battle.Instance.BattleCameraController;
	}

	public void startListener(){
		EventManager.Instance.AddListener <TimedHitEvent> (timedHitListener);
		EventManager.Instance.AddListener <TimedDefEvent> (timedDefListener);
	}
	public void stopListener(){
		EventManager.Instance.RemoveListener <TimedHitEvent> (timedHitListener);
		EventManager.Instance.RemoveListener <TimedDefEvent> (timedDefListener);
	}
	private int randomTimedButton(){
		int randNum = LegrandUtility.Random(0, timedButtonId.Count);
		int choosen = (int) timedButtonId [randNum];
		
		timedButtonId.RemoveAt(randNum);		
		return choosen;
	}
	private void timedDefListener(TimedDefEvent evt){
        if(evt.Defender.TimedHit.OnTimed) return;

        int num = randomTimedButton ();
		evt.Defender.TimedHit.setDef (evt.Attacker , evt.Defender, num);
		evt.Defender.TimedHit.OnTimedDef += HandleOnTimedDef;
		timedDefBattler.Add (evt.Defender);
		if (!onTimeHit) {
			onTimeHit = true;
			StartCoroutine(CoUpdateDef());
		}
	}
	void HandleOnTimedDef (Battler battler, TimedHitState hitState, int id)
	{
		battler.TimedHit.OnTimedDef -= HandleOnTimedDef;
		battler.TimedHit.OnTimed = false;
//        battler.isTimeHit = false;
        battler.StopShaderSequence();
        foreach (Battler b in timedDefBattler)
        {
            b.StopShaderSequence();
        }
		timedButtonId.Add (id);
		
		onTimeHit = false;
		StopAllCoroutines ();
		timedDefBattler.Remove (battler);
		StartCoroutine(DelayNextDef());
	}
	private void nextBattlerDef(){
		StopAllCoroutines ();
		if (timedDefBattler.Count > 0) {
			onTimeHit = true;
			StartCoroutine(CoUpdateDef());
		} else {
            battleCam.resetTarget(false);
			Time.timeScale = 1f;
		}
	}
	IEnumerator DelayNextDef() {
		while (true) {
			yield return StartCoroutine(WaitForRealSeconds(1f));
			nextBattlerDef ();
		}
	}
	IEnumerator CoUpdateDef() {
		while (onTimeHit) {
			if (!timedDefBattler.First().TimedHit.OnTimed) {
				timedDefBattler.First().TimedHit.startTimedHit ();
				if (battleCam.PartyType == 0){
					battleCam.focusTarget (timedDefBattler.First().transform, timedDefBattler.First().transform);
				}
				else{
					battleCam.focusTarget (timedDefBattler.First().HitArea, timedDefBattler.First().transform);
				}
			}
			Time.timeScale = 0.05f;
			yield return StartCoroutine(WaitForRealSeconds(0.5f));
		}
	}
	//======================================================================================
	private void timedHitListener(TimedHitEvent evt){
        if (evt.command.attacker.TimedHit.OnTimed) return;
		int num = randomTimedButton ();
		evt.command.onTimedHit 			= true;
		evt.command.attacker.TimedHit.setHit (evt.Attacker, evt.Defenders, evt.command, num);
		evt.command.attacker.TimedHit.OnTimedHit += HandleOntimedHit;

		timeHitCommands.Add (evt.command);

		if (!onTimeHit) {
			onTimeHit = true;
			StartCoroutine(CoUpdateHit());
		}
	}
	private void HandleOntimedHit (BattleCommand command, TimedHitState hitState, int id)
	{
		command.attacker.TimedHit.OnTimedHit -= HandleOntimedHit;
		command.attacker.TimedHit.OnTimed = false;

		timedButtonId.Add (id);

		onTimeHit = false;
		StopAllCoroutines ();
		timeHitCommands.Remove (command);
		StartCoroutine(DelayNextHit());
	}
	private void nextCommandHit(){
		StopAllCoroutines ();
		if (timeHitCommands.Count > 0) {
			onTimeHit = true;
			StartCoroutine(CoUpdateHit());
		} else {
            battleCam.resetTarget(false);
			Time.timeScale = 1f;
		}
	}
	IEnumerator DelayNextHit() {
		while (true) {
			yield return StartCoroutine(WaitForRealSeconds(0.5f));
			nextCommandHit ();
		}
	}
	IEnumerator CoUpdateHit() {
		while (onTimeHit) {
			if (!timeHitCommands.First().attacker.TimedHit.OnTimed) {
				timeHitCommands.First().attacker.TimedHit.startTimedHit ();
				if(timeHitCommands.First().attacker.AttackType == AttackType.Range || timeHitCommands.First().attacker.AttackType == AttackType.Caster){
					battleCam.focusTarget (timeHitCommands.First().attacker.transform, timeHitCommands.First().target[0].transform);
				}else{
					battleCam.focusTarget (timeHitCommands.First().attacker.transform, timeHitCommands.First().target[0].transform);
				}
			}
			Time.timeScale = 0.05f;
			yield return StartCoroutine(WaitForRealSeconds(0.5f));
		}
	}
	IEnumerator WaitForRealSeconds(float time)
	{
		float start = Time.realtimeSinceStartup;
		while (Time.realtimeSinceStartup < start + time)
		{
			yield return null;
		}
	}
}
