﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Legrand.core;
using UnityStandardAssets.ImageEffects;
using System.Reflection;

public class BattleCameraController : MonoBehaviour {
    public Transform camTrans;
    public Transform targetPos;
    public Transform targetLook;

    protected Vector3 oriPos;
    protected Quaternion oriRot;
    protected Vector3 currentPos;
    protected Quaternion currentRot;

    protected bool onFocus = false, onCrit = false, onReset = false;

    public float speed = 5f;
    public Transform[] Views;
    public Transform[] MassiveViews;
    public Transform[] NormalViews;
    public enum CameraView
    {
        NormalViews,
        MassiveViews
    }
    protected CameraFilterPack_Blur_Focus _Focus;
    protected CC_HueSaturationValue _HueSaturation;


    protected CC_HueSaturationValue _HueSaturationSkillCamera;

    protected Transform _FollowedObject = null;

    public bool changePerspective = false;
    public bool firstSelect = false;
    protected int _PartyType;
    public int PartyType{
        get {return _PartyType;}
        set {_PartyType = value;}
    }

    protected Tween _FocusTween;
    protected Tween _SelectTweenMove;
    protected Tween _SelectTweenRot;
    protected Tween _IdleMoveTween;
    protected Tween _IdleRotTween;

    public Color PlayerTimedHitColor = Color.blue;
    public Color EnemyTimedHitColor = Color.red;

    public Transform MassiveCenter;
    public Transform NormalCenter;

    protected ScreenFader _ScreenFade;
    protected CameraSecondFader _CamFader;

    protected Animator _Anim;
    public GameObject _SecondCamera;

    [HideInInspector]
    public Battler BattlerFocusCamera;

    protected bool setLocation;
    protected bool isIdle;
    protected bool previewLookAt = false;
    public bool isMassive;

    protected CameraEventListener _CamEvent;

    [HideInInspector]
    public bool isSelected = false;
    protected Camera _ThisCamera;
    public Camera MainCamera{
        get { return _ThisCamera; }
    }

    public Transform SkillCamera;

    protected Camera _Camera2;
    public float fadeTime = 1f;


    protected bool inProgress = false, isFirst = false;
    protected bool swap = false;

    private int _CameraTurn = 0;

    public Camera _UICamera;

    public Transform LockedTimeHitPos;
    protected BattleSecondCameraController _SecondCameraController;
    public bool onSkill;

    void Start () {
        _ScreenFade = Battle.Instance.ScreenFader;
        _SecondCamera.SetActive (true);
        _CamFader = _SecondCamera.GetComponent<CameraSecondFader> ();
        _SecondCameraController = _SecondCamera.GetComponent<BattleSecondCameraController>();
        //set ori pos dof, yang blur ketika preview

        camTrans = transform.parent;
        oriPos = camTrans.position;
        oriRot = camTrans.rotation;
        _Anim = camTrans.GetComponent<Animator> ();
        _CamEvent = camTrans.GetComponent<CameraEventListener> ();
        _Anim.enabled = false;
        _Focus = GetComponent<CameraFilterPack_Blur_Focus> ();
        _HueSaturation = GetComponent<CC_HueSaturationValue> ();
        _HueSaturationSkillCamera = SkillCamera.GetComponent<CC_HueSaturationValue> ();
        _ThisCamera = GetComponent<Camera> ();
        EventManager.Instance.AddListener<CameraFollowEvent> (CameraFollowHandler);
        EventManager.Instance.AddListener<SetTimeHitStatusEvent> (TimeHitHandler);
        DOTween.Init(true, true, LogBehaviour.Verbose).SetCapacity(200, 10);

        EventManager.Instance.AddListener<CameraPreviewEncounter> (HandleOnPreviewCamera);
        EventManager.Instance.AddListener<CameraStartBattle> (HandleOnStartView);
        EventManager.Instance.AddListener<CameraEndBattle> (HandleEndBattle);
        EventManager.Instance.AddListener<CameraRandomView> (HandleRandomCamera);
        EventManager.Instance.AddListener<CameraGameOver> (HandleGameOverCamera);
        EventManager.Instance.AddListener<CameraDeadCinematic>(HandleDeadCinematicBoss);

        _Camera2 = _SecondCamera.GetComponent<Camera> ();
        //      components = this.GetComponents<Component>();
        //      foreach (Component c in components) {
        //          Debug.Log(c.GetType());
        //      }
        //
        //      depthOfField = this.GetComponent<UnityStandardAssets.CinematicEffects.DepthOfField> ().enabled = false;
        //      Debug.Log (depthOfField);
        //      DOFEffect = GetComponent<Component>();
        initCamera();
    }

    protected void OnDestroy()
    {
        if (EventManager.Instance != null) {
            EventManager.Instance.RemoveListener<CameraFollowEvent> (CameraFollowHandler);
            EventManager.Instance.RemoveListener<SetTimeHitStatusEvent> (TimeHitHandler);
            EventManager.Instance.RemoveListener<CameraEndBattle> (HandleEndBattle);
            EventManager.Instance.RemoveListener<CameraRandomView> (HandleRandomCamera);
            EventManager.Instance.RemoveListener<CameraPreviewEncounter> (HandleOnPreviewCamera);
            EventManager.Instance.RemoveListener<CameraGameOver> (HandleGameOverCamera);
            EventManager.Instance.RemoveListener<CameraDeadCinematic> (HandleDeadCinematicBoss);
        }
    }
    virtual public void resetBlinkCamera (){
        _ThisCamera.fieldOfView = 30;
        ResetUICamera();
        KillTween ();
        setLocation = false;
        targetPos = null;
        changePerspective = false;
        firstSelect = true;
        camTrans.transform.position = oriPos;
        if (isMassive)
            camTrans.LookAt (MassiveCenter);
        else
            camTrans.LookAt (NormalCenter);
//        CorrectLookAt ();
    }
    protected void IdleCamera (){
        if (isMassive)
            _SelectTweenRot = camTrans.DOLookAt (MassiveCenter.position, 5f, AxisConstraint.None, Vector3.up).OnComplete (CorrectLookAt);
        else
            _SelectTweenRot = camTrans.DOLookAt (NormalCenter.position, 0.25f, AxisConstraint.None, Vector3.up).OnComplete (CorrectLookAt);
    }
    protected void CorrectLookAt(){
        KillTween ();
        isIdle = true;
        _SelectTweenMove = camTrans.DOMove (new Vector3(oriPos.x + 1f, oriPos.y + 1f, oriPos.z + 1f), 9f).SetLoops (-1, LoopType.Yoyo).SetEase(Ease.InOutQuad);
    }
    virtual public void pointerView (Battler target, bool isSingle){
        StopAllCoroutines();
        StartCoroutine (ChangeCamera(false, 0f));
        if (isMassive)
            _ThisCamera.fieldOfView = 40;
        else
            _ThisCamera.fieldOfView = 40;

        ResetUICamera();

        if (!setLocation) {
            currentPos = camTrans.transform.position;
            currentRot = camTrans.transform.rotation;
            setLocation = true;
        }
        isIdle = false;
        changePerspective = false;
        KillTween ();
        // Kamera dipindahkan ke posisi akhir kemudian dicatat posisi dan rotasinya => dibalikin ke posisi awal => dottween ke posisi dan rotasi yang dicatat
        if (isSingle){
            Encounter enc = (target.Character as Encounter);
            Vector3 currentRotation = camTrans.transform.eulerAngles;
            Vector3 currentPosition = camTrans.transform.position;
            Transform targetFocusTransform = target.CameraPointingLocation;

            float xMod = 0;//(target.BattlePosition.x - 1) * -3f;//LegrandUtility.Random(0f, -5f);
            float zMod = target.BattlePosition.y - 4.5f;//((target.BattlePosition.y - 1) * 2.5f) - 2.5f;

            Vector3 newPos = new Vector3 (targetFocusTransform.transform.position.x + xMod, 1.75f, targetFocusTransform.transform.position.z + zMod);
            camTrans.transform.position = newPos;
            camTrans.transform.LookAt (targetFocusTransform.transform);

            Vector3 targetRotation = camTrans.transform.eulerAngles;

            if (isMassive)
            {
                newPos = new Vector3(targetFocusTransform.transform.position.x + xMod, 2.8f, -7f);
                targetRotation = new Vector3(355, 0, 0);
            }

            camTrans.transform.eulerAngles = currentRotation;
            camTrans.transform.position = currentPosition;
            if (firstSelect){
                _SelectTweenMove = camTrans.DOMove (newPos, 0f);
                _SelectTweenRot = camTrans.DORotate (targetRotation, 0f, RotateMode.Fast);
            }
            else{
                _SelectTweenMove = camTrans.DOMove (newPos, 0.5f).SetEase(Ease.OutQuad).SetUpdate(true);
                _SelectTweenRot = camTrans.DORotate (targetRotation, 0.5f, RotateMode.Fast).SetUpdate(true);
            }
        } else {
            Encounter enc = (target.Character as Encounter);
            Vector3 currentRotation = camTrans.transform.eulerAngles;//camTrans.transform.eulerAngles;
            Vector3 currentPosition = camTrans.transform.position;

            if (enc.MonsterSize == EncounterSize.Massive){
                Vector3 newPos = new Vector3 (0, 2.8f, -7);
                Vector3 targetRotation = new Vector3(355,0,0);

                if (firstSelect){
                    _SelectTweenMove = camTrans.DOMove (newPos, 0f);
                    _SelectTweenRot = camTrans.DORotate (targetRotation, 0f, RotateMode.Fast);
                }
                else{
                    _SelectTweenMove = camTrans.DOMove (newPos, 0.5f).SetEase (Ease.OutQuad).SetUpdate(true);
                    _SelectTweenRot = camTrans.DORotate (targetRotation, 0.5f, RotateMode.Fast).SetUpdate(true);
                }
            }
            else{
                Vector3 newPos = new Vector3 (0, target.transform.lossyScale.y+2f, target.transform.localPosition.z-9f);
                Vector3 targetRotation = new Vector3(13,0,0);
                if (firstSelect){
                    _SelectTweenMove = camTrans.DOMove (newPos, 0f);
                    _SelectTweenRot = camTrans.DORotate (targetRotation, 0f, RotateMode.Fast);
                }
                else{
                    _SelectTweenMove = camTrans.DOMove (newPos, 0.5f).SetEase (Ease.OutQuad).SetUpdate(true);
                    _SelectTweenRot = camTrans.DORotate (targetRotation, 0.5f, RotateMode.Fast).SetUpdate(true);
                }
            }
        }
        if (isSelected)
            firstSelect = false;

        _CamFader.TransToClear(0, 0);
        _SecondCameraController.KillTween();    
    }
    public void KillTween(){
        if (_SelectTweenMove != null)
            _SelectTweenMove.Kill (false);
        if (_SelectTweenRot != null)
            _SelectTweenRot.Kill (false);
    }
    public void characterSelectView(Battler battler, bool isSingle){
        StopAllCoroutines();
        StartCoroutine (ChangeCamera(false, 0f));
        _ThisCamera.fieldOfView = 40;
        ResetUICamera();
        if (!setLocation) {
            currentPos = camTrans.transform.position;
            currentRot = camTrans.transform.rotation;
            setLocation = true;
        }
        isIdle = false;
        changePerspective = true;
        KillTween ();
        if (isSingle){
            Vector3 currentRotation = camTrans.transform.eulerAngles;
            Vector3 currentPosition = camTrans.transform.position;
            Transform targetFocusTransform = battler.CameraPointingLocation;

            Vector3 newPos = new Vector3 (targetFocusTransform.transform.position.x/*+LegrandUtility.Random(-4,5)*/, targetFocusTransform.transform.position.y + 0.75f /*LegrandUtility.Random(0.5f,1f)*/, targetFocusTransform.transform.position.z+ 4f/*LegrandUtility.Random(4f,5f)*/);
            camTrans.transform.position = newPos;
            camTrans.transform.LookAt (targetFocusTransform.transform);

            Vector3 targetRotation = camTrans.transform.eulerAngles;
            camTrans.transform.eulerAngles = currentRotation;
            camTrans.transform.position = currentPosition;

            if (firstSelect){
                _SelectTweenMove = camTrans.DOMove (newPos, 0f);
                _SelectTweenRot = camTrans.DORotate (targetRotation, 0f, RotateMode.Fast);
            }else{
                _SelectTweenMove = camTrans.DOMove (newPos, 0.5f).SetEase (Ease.OutQuad).SetUpdate(true);
                _SelectTweenRot = camTrans.DORotate (targetRotation, 0.5f, RotateMode.Fast);
            }
        }else{
            changePerspective = true;
            Vector3 currentRotation = camTrans.transform.eulerAngles;
            Vector3 currentPosition = camTrans.transform.position;
            Vector3 newPos = new Vector3 (0, 3.5f, 6f);
            Vector3 targetRotation = new Vector3(20,180,0);

            _SelectTweenMove = camTrans.DOMove (newPos, 0f).SetEase (Ease.OutQuad);
            _SelectTweenRot = camTrans.DORotate (targetRotation, 0f, RotateMode.Fast);
        }
        if (isSelected)
            firstSelect = false;

        _CamFader.TransToClear(0, 0);
        _SecondCameraController.KillTween();
    }
    public void positioningCamera(){
        //      targetPos = null;
        changePerspective = false;
        oriRot = Views[0].rotation;
        oriPos = Views[0].position;

        currentPos = oriPos;
        currentRot = oriRot;

        resetTarget (false);

        Battle.Instance.Controller.CameraFirstMove = true;
    }

    public void resetTarget(bool afterTimeHit){
        KillTween ();
        setLocation = false;
        isIdle = false;
//        _SecondCamera.SetActive (false);

//        SetSkyBox(); 
        targetPos = null;
        changePerspective = false;
        firstSelect = true;

        if (afterTimeHit && LockedTimeHitPos != null)
        {
            oriPos = LockedTimeHitPos.position;
            oriRot = LockedTimeHitPos.rotation;
        }

        _SelectTweenMove = camTrans.DOMove (oriPos, 1f).SetEase (Ease.OutQuad).OnComplete (IdleCamera);
        _SelectTweenRot = camTrans.DORotate (oriRot.eulerAngles, 0.8f, RotateMode.Fast);

    }
    void SetSkyBox(){
        SkyboxSetter Skybox1 = _ThisCamera.GetComponent<SkyboxSetter>();
        SkyboxSetter Skybox2 = _SecondCamera.GetComponent<SkyboxSetter>();
        if (Skybox1 != null)
        {
            Skybox1.enabled = false;
            Skybox1.enabled = true;
        }
    }
    virtual public void OnCommandCharacter(Battler battler)
    {
        _SecondCamera.SetActive(true);
        Battle.Instance.encounterList.ForEach(x=>x.ActivateBattler(true));
        battler.transform.eulerAngles = battler.MyPlane.transform.eulerAngles;
//        Battle.Instance.charList.ForEach(x=>x.BattlerState != BattlerState.Dead && x.transform.DORotate(new Vector3(0,0,0),1f,RotateMode.Fast));
        float xMod = (battler.BattlePosition.x - 1) * -0.75f;
        firstSelect = true;
        _Camera2.fieldOfView = 50;

        _CamFader.TransToClear(0, 0);
        _SecondCameraController.KillTween(); 

        if (battler.BattlePosition.x == 1)
            xMod = 0.75f;

        if (battler.Character._ID != "5")
            _SecondCamera.transform.position = new Vector3(battler.transform.position.x+xMod, 1.3f, battler.transform.position.z-1.7f);
        else
            _SecondCamera.transform.position = new Vector3(battler.transform.position.x+xMod, 1.3f, battler.transform.position.z-2.5f);
        
        _SecondCameraController.StartTweenView();

        if (!isFirst)
        {
            isFirst = true;
            _CamFader.TransToTexture(0, 0.5f);
            StartCoroutine (ChangeCamera(true, 0.5f));
        }
        else
        {
            _CamFader.TransToTexture(0, 0);
            StartCoroutine (ChangeCamera(true, 0f));
        }
    }
    public void SwitchCamera2(){
        _CamFader.TransToTexture(0, 0.3f);
    }
    public void StopCommandCharacter(){
        StopAllCoroutines();
        StartCoroutine (ChangeCamera(false, 0));
        Battle.Instance.battlerList.ForEach(x=>x.ActivateBattler(true));
        _CamFader.TransToClear(0, 0);
        _SecondCameraController.KillTween();
        resetBlinkCamera();
    }
    public IEnumerator ChangeCamera (bool isCommand, float duration){
        yield return new WaitForSeconds(duration);
        if (isCommand){
            _ThisCamera.enabled = false;
        }else{
            if (!onSkill)
            {
                _ThisCamera.enabled = true;
                _CamFader.TransToClear(0, 0);
                _SecondCamera.SetActive(false);
            }
        }
    }
    protected void RandomView (){
        if (Views.Length > 0) {
            oriRot = Views [_CameraTurn].rotation;
            oriPos = Views [_CameraTurn].position;

            _CameraTurn++;
            if (_CameraTurn > Views.Length-1)
                _CameraTurn = 0;

            currentPos = oriPos;
            currentRot = oriRot;
        }
    }
    public void focusTarget(Transform pos, Transform look){
        StartCoroutine (ChangeCamera(false, 0f));
        isIdle = false;
        targetPos = pos;
        targetLook = look;
        onFocus = true;

        if (pos.Equals (look)) {
            Vector3 mid = GetPoint();
            camTrans.DOMove (new Vector3(mid.x, targetPos.position.y + 1.5f, targetPos.position.z-1.75f), 0.5f).SetUpdate(true);
            camTrans.DOLookAt(targetPos.position, 0.5f, AxisConstraint.Y).SetUpdate(true);
        } else {
            if (_PartyType == 0){
//                Battler battler = pos.gameObject.GetComponent<Battler>();
//                float xMod = (battler.BattlePosition.x - 1) * -0.1f;
                var zoomResult = new Vector3 (-0.25f,1.5f, -3f);
                var rotationResult = Quaternion.Euler (targetPos.position);

                var targetAdjustedPosition = rotationResult * zoomResult;

                camTrans.DOMove (targetPos.position + targetAdjustedPosition, 0.5f).SetUpdate(true);
            }
            else{
                var zoomResult = new Vector3 (0.25f, 1.5f, -4f);
                var rotationResult = Quaternion.Euler (targetPos.position);

                var targetAdjustedPosition = rotationResult * zoomResult;

                camTrans.DOMove (targetPos.position + targetAdjustedPosition, 0.5f).SetUpdate(true);
            }

            camTrans.DOLookAt(targetLook.position, 0.4f, AxisConstraint.Y).SetUpdate(true);
        }

    }
    public void focusCrit(Transform targetCrit){
        isIdle = false;
        targetPos = targetCrit;
        onCrit = true;
        camTrans.LookAt (targetPos);

        float step = speed * Time.deltaTime;

        Vector3 newPos = GetPoint (); // + new Vector3(0, targetPos.lossyScale.y ,0);
        camTrans.position = Vector3.MoveTowards (camTrans.position, newPos, step * 1.5f);
    }
    protected  Vector3 GetPoint(){
        //get the positions of our transforms

        Vector3 pos1 = oriPos ;
        Vector3 pos2 = targetPos.position;

        //get the direction between the two transforms -->
        Vector3 dir = (pos2 - pos1).normalized ;

        //get a direction that crosses our [dir] direction
        //NOTE! : this can be any of a buhgillion directions that cross our [dir] in 3D space
        //To alter which direction we're crossing in, assign another directional value to the 2nd parameter
        Vector3 perpDir = Vector3.Cross(dir, - Vector3.forward) ;

        //get our midway point
        Vector3 midPoint = (pos1 + pos2) / 2f ;

        //get the offset point
        //This is the point you're looking for.
        Vector3 offsetPoint = midPoint + (perpDir );//* offset) ;

        return offsetPoint ;
    }

    // Update is called once per frame
    protected void LateUpdate () {
        if (targetPos != null) {
            if(onFocus){
                focusTarget (targetPos, targetLook);
            }else if(onCrit){
                focusCrit(targetPos);
            }
        }
        if (_FollowedObject != null) {
            camTrans.position = _FollowedObject.position;
            camTrans.eulerAngles = _FollowedObject.eulerAngles;
        }
    }
    protected void Update (){
        if (isIdle) {
            if (isMassive)
                camTrans.LookAt (MassiveCenter);
            else
                camTrans.LookAt (NormalCenter);
        }

        if (previewLookAt)
            _SecondCamera.transform.LookAt (Battle.Instance.Controller.EncounterFirstMeet [0].CameraPointingLocation.transform);
    }

    protected void CameraFollowHandler(CameraFollowEvent c)
    {
        _FollowedObject = c.FollowedObject;
        if (_FollowedObject == null) 
        {
            camTrans.position = oriPos;
            camTrans.eulerAngles = oriRot.eulerAngles;
        }
    }

    protected void TimeHitHandler(SetTimeHitStatusEvent e)
    {
        isFirst = false;
        // Handle thing when start and stop timed hit
        if (e.IsTimeHit) {

            // Change player & enemy color while timed hit
            if (e.Attacker.BattlerType == BattlerType.Player) {
                //              foreach (Material battleShader in e.Attacker.BattleShaders)
                e.Attacker.ChangeRimColor (PlayerTimedHitColor, 1f);
            }
            else e.Attacker.ChangeRimColor(EnemyTimedHitColor, 1f);

            for (int i = 0;i < e.Defenders.Length; i++) // each (Battler battler in e.Defenders) 
            {
                Battler battler = e.Defenders[i];
                if (battler.BattlerType == BattlerType.Player) battler.ChangeRimColor (PlayerTimedHitColor, 1f);
                else battler.ChangeRimColor(EnemyTimedHitColor, 1f);
            }
            //

            WwiseManager.Instance.PlaySFX(WwiseManager.BattleFX.Zoom);

            // Change camera to greyscale and blurry side
            DOTween.To (x => _HueSaturation.saturation = x, _HueSaturation.saturation, -50f, 0.25f).SetUpdate (true);
            DOTween.To (x => _HueSaturationSkillCamera.saturation = x, _HueSaturationSkillCamera.saturation, -50f, 0.25f).SetUpdate (true);
            _FocusTween = DOTween.To (x => CameraFilterPack_Blur_Focus.ChangeSize = x, CameraFilterPack_Blur_Focus.ChangeSize, 4f, 1f).SetUpdate (true);
        } 
        else 
        {
            WwiseManager.Instance.StopSFX(WwiseManager.BattleFX.Zoom);

            // Change battler color to normal
            e.Attacker.StopShaderSequence();// ChangeRimColor (Color.black);
            foreach (Battler battler in e.Defenders) 
            {
                battler.StopShaderSequence();// ChangeRimColor (Color.black);
            }
            //

            // Turn Camera to normal
            DOTween.To (x => _HueSaturation.saturation = x, _HueSaturation.saturation, 0f, 0.25f).SetUpdate (true);
            DOTween.To (x => _HueSaturationSkillCamera.saturation = x, _HueSaturationSkillCamera.saturation, 0f, 0.25f).SetUpdate (true);
            _FocusTween.Kill (true);
            DOTween.To (x => CameraFilterPack_Blur_Focus.ChangeSize = x, CameraFilterPack_Blur_Focus.ChangeSize, 0.11f, 0.25f).SetUpdate (true);
        }
    }
    void HandleOnStartView (CameraStartBattle c){
        _Anim.enabled = true;
        if (_PartyType == 0) {
            _Anim.CrossFade ("CameraStart", 0.3f);
        } else {
            _Anim.CrossFade ("CameraStartMassive", 0.3f);
        }
        //      this.OnDoneCamera += HandleOnDoneCamera;
        EventManager.Instance.RemoveListener<CameraStartBattle> (HandleOnStartView);
    }
    void HandleRandomCamera (CameraRandomView c){
        RandomView ();
    }
    void HandleEndBattle (CameraEndBattle e){
        StopCommandCharacter();
        this.transform.SetParent(camTrans);
        this.transform.position = new Vector3(0, 0, 0);
        isIdle = false;
        int rand = (int)LegrandUtility.Random(1, 12);
        _ThisCamera.fieldOfView = 45f;
        _Anim.enabled = true;
        _Anim.CrossFade ("CameraEnd_"+ rand, 0.25f);
        resetCameraSettings();
        resetDOF();
    }
    void HandleOnPreviewCamera(CameraPreviewEncounter p){
        StartPreview ();
    }
    virtual public void StartPreview(){
        List <Battler> battlers = Battle.Instance.Controller.EncounterFirstMeet;
        battlers [0].BattlerState = BattlerState.Preview;
        Encounter enc = battlers[0].Character as Encounter;
        Vector3 newPos = new Vector3 ();

        if (enc.enemyType == EnemyType.BossUnique)
        {
            Transform previewPos = battlers[0].GetPreviewHelper();
            this.transform.SetParent(previewPos);
            CameraPreviewListener camListener = previewPos.GetComponent<CameraPreviewListener>();
            camListener.ClearPreviewEvent();
            camListener.DonePreview += HandleDonePreview;
        }
        else
        {
            _CamFader.TransToTexture (0, 0.5f);
            if (isMassive)
                newPos = new Vector3(battlers[0].CameraPointingLocation.position.x, battlers[0].CameraPointingLocation.position.y, battlers[0].CameraPointingLocation.position.z - 3);
            else
                newPos = new Vector3(battlers[0].CameraPointingLocation.position.x, battlers[0].CameraPointingLocation.position.y + 2.5f, battlers[0].CameraPointingLocation.position.z - 3);
            _SecondCamera.transform.position = newPos;
            //      StartCoroutine(DoFade(newPos, battlers [0]));
            previewLookAt = true;
            _SecondCamera.transform.DOMove(new Vector3(newPos.x, newPos.y - 1, newPos.z - 2.5f), 1.5f).OnComplete(() => RemoveBattlers(battlers[0]));
        }
    }
    void HandleDonePreview(CameraPreviewListener ce){
        Battle.Instance.Controller.EncounterFirstMeet.Remove (ce.battler);
        Battle.Instance.Controller.PrevSequence = false;
        this.transform.SetParent(camTrans);
        this.transform.position = new Vector3(0, 0, 0);
        EventManager.Instance.TriggerEvent (new CameraStartBattle ());
        ce.DonePreview -= HandleDonePreview;
    }
    public void RemoveBattlers (Battler bat){
        StartCoroutine (WaitPreview (bat));
    }

    IEnumerator WaitPreview (Battler bat){
        yield return new WaitForSeconds (1f);
        _CamFader.ClearTexture ();
        camTrans.position = _Camera2.transform.position;
        camTrans.eulerAngles = _Camera2.transform.eulerAngles;
        previewLookAt = false;
        Battle.Instance.Controller.EncounterFirstMeet.Remove (bat);
        Battle.Instance.Controller.PrevSequence = false;
        _ScreenFade.FadeSpeed = 1.5f;
        if (Battle.Instance.Controller.EncounterFirstMeet.Count == 0)
            EventManager.Instance.TriggerEvent (new CameraStartBattle ());
    }
    void HandleGameOverCamera (CameraGameOver c){
        _Anim.enabled = true;
        _Anim.CrossFade ("GameOverCam_1", 0.2f);
    }

    void HandleDeadCinematicBoss (CameraDeadCinematic c){
        StopCommandCharacter();
        Transform previewPos = c.deadTrans;
        this.transform.SetParent(previewPos);
        this.transform.position = new Vector3(0, 0, 0);
        c.bat.BattlerState = BattlerState.Dead;
        DeadCinematic camListener = previewPos.GetComponent<DeadCinematic>();
        camListener.ClearPreviewEvent();
        camListener.DoneCinematic += HandleDoneDeadCinematic;
        previewLookAt = false;
    }

    void HandleDoneDeadCinematic(DeadCinematic c, Battler b){
        c.DoneCinematic -= HandleDoneDeadCinematic;
        Battle.Instance.Controller.waitForCinematic = false;
    }

    virtual public void initCamera(){}


    public void ResetUICamera(){
        if (_UICamera != null)
            _UICamera.fieldOfView = _ThisCamera.fieldOfView;
    }

    private void resetCameraSettings(){
        isFirst = false;
    }
    virtual public void resetDOF(){}
    virtual public Transform GetDOF(){
        return null;
    }

}
