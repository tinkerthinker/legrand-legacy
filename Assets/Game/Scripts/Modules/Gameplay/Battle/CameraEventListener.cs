﻿using UnityEngine;
using System.Collections;

public class CameraEventListener : MonoBehaviour {

    public delegate void CameraEvent();
	public event CameraEvent OnDoneCamera;

	private Animator _Anim;
	public BattleCameraController batCam;

	void Start (){
		_Anim = GetComponent<Animator> ();
		OnDoneCamera += HandleOnDoneCamera;
	}


    public void HandleOnDoneCamera (){
		OnDoneCamera -= HandleOnDoneCamera;
		_Anim.CrossFade ("IdleCamera", 0f);
		_Anim.enabled = false;
		batCam.positioningCamera ();
//		Battle.Instance.Controller.previewHandler();
	}
	void OnDoneCameraEvent (){
		if (OnDoneCamera != null)
			OnDoneCamera ();
	}


}
