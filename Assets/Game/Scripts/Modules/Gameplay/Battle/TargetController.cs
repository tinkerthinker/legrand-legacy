using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TeamUtility.IO;

using Legrand.core;
[System.Serializable]
public class TargetController {
	private SelectTargetData targetData = new SelectTargetData();

	private bool ignorePos = false;
	private bool isMove = false;
	private bool isSingle = false;
	private bool onEnemy = false;
	private bool onSelf = false;
	private bool onDead = false;
	private bool isChange = false;
    private bool _IsPhysical = false;
    private bool _IsMagic = false;
    private Element _MagicElement = Element.None;
    public bool StopChangeCamera = false;
    private string _ItemId = "";

	private int xOffset = 0;
	private int yOffset = 0;
	private int xMax = 0;
	private int yMax = 0;
	private int xMin = 0;
	private int yMin = 0;
	private int mirror = -1;
	private int xOffsetTemp = 0;
	private int yOffsetTemp = 0;
	private int SelectLoop = 0;

	private List<GameObject> targetPointer = new List<GameObject>();
	private List<GameObject> infoBattler = new List<GameObject>();
    private List<Battler> targetList = new List<Battler>();
    private List<Battler> targetListTemp = new List<Battler>();
	private List<BattlePlane> selectedPlane = new List<BattlePlane>();
	private Vector2[] targetVecs;
	private List<Vector2> selectedVecs = new List<Vector2> ();
	private Battler battler = new Battler();
    private Battler disabledBattler = new Battler();
	private Battler targetTemp = new Battler();
	private TargetType _TargetType;

	private Battle battleData;
	private BattleController _BattleController;
	private BattlePoolingSystem poolingSystem;
	private BattleInput battleInput;

	public delegate void SelectedTarget(List<Battler> lists);
    public delegate void SelectedItemTarget(string id);
    public delegate void SelectedPos(BattlePlane plane);
	public delegate void SelectedReserve (Battler batReserve, int resvId);
	public delegate void SelectedBattlerCombination (Battler batCombine, List <Battler> target);

	public event SelectedTarget OnSelectTarget;
    public void ClearOnSelectTarget()
    {
        OnSelectTarget = null;
    }

    public event SelectedItemTarget OnSelectItemTarget;
    public void ClearOnSelectItemTarget()
    {
        OnSelectItemTarget = null;
    }
    public event SelectedPos OnSelectPos;
	public event SelectedReserve OnSelectReserve;

	public delegate void NoTarget ();
	public event NoTarget OnCancel;
    public void ClearOnCancel()
    {
        OnCancel = null;
    }

    public List<BattlePlane> targetPlane = new List<BattlePlane>();
	public List<Battler> filteredTarget = new List<Battler>();

	public TargetController(BattleInput input){
		poolingSystem 	= BattlePoolingSystem.Instance;
		battleData 		= Battle.Instance; 
		battleInput		= input;
		_BattleController = battleData.Controller;
	}

	#region Public Method
	public Vector2[] getAttackTarget(Battler attacker){
		onSelf = false;
		isSingle = true;
		onEnemy = true;
		ignorePos = false;

		Vector2[] filtered = filterTarget(attacker);
		return filtered;
  	}

    private void ItemSelected(List<Battler> battlers)
    {
        OnSelectTarget -= ItemSelected;
        if(OnSelectItemTarget != null)
           OnSelectItemTarget(_ItemId);
    }


    public void selectAttackTarget(Battler attacker){
        onDead = false;
        onSelf = false;
        isSingle = true;
        ignorePos = false;
        onEnemy = true;
        _IsPhysical = true;
        _IsMagic = false;

        targetVecs = getAttackTarget(attacker);
        battler = attacker;

        if (targetVecs.Length > 0) {
            startSelect ();
        } else {
            sendCancel();
        }

    }
    public void selectItemTarget(Battler attacker, int itemSlotId){
        // belum ada data target di item, default single
        battler = attacker;
        onSelf = false;
        isSingle = false;
        onEnemy = false;
        ignorePos = false;

        _IsPhysical = false;
        _IsMagic = false;

        OnSelectTarget -= ItemSelected;
        OnSelectTarget += ItemSelected;


        Consumable item = attacker.GetItem (itemSlotId) as Consumable;

        _ItemId = item._ID;

        if(item.ConsumableType == Consumable.ConsumeType.Magic){
            onEnemy = ((item as ConsumableMagic).TargetFormation == TargetFormation.Enemy)? true : false;
            ignorePos = (onEnemy)?true: false;
        }
        if(item.ConsumableType == Consumable.ConsumeType.Healing){
            onEnemy = false;
            ignorePos = true;
            attacker.BattleStateCondition = BattleStateCondition.ItemBuff;

            ConsumableHealing healingItem = (ConsumableHealing) item;
            if (healingItem.CuredStatuses.Contains(Status.Dead)){
                onDead = true;
            }
            else{
                onDead = false;
            }
        }

        //belum handle consumable misc

        if (item.Target == TargetType.Single) {
            isSingle = true;
            targetVecs = filterTarget (attacker);
        } else {
            targetVecs = getTargetTypeVec(item.Target);
        }

        if (targetVecs.Length > 0)
            startSelect ();
        else
            sendCancel ();
    }
    public void selectSkillUltiTarget (Battler attacker, SkillTypes skillType){
		MainCharacter mainChar = (attacker.Character as MainCharacter);
		List<Battler> targets = new List<Battler>();
		battler = attacker; 
		switch (skillType){
            case SkillTypes.UltimateOffense: //offensive skill
    			TargetType targetTypee = mainChar.BattleOffensiveSkill.Target;

    			onSelf = false;
    			isSingle = false;
    			onEnemy = true;
    			onDead = false;
    			isChange = false;
                    if (mainChar.BattleOffensiveSkill.DamageType == TypeDamage.Physical)
                    {
                        _IsPhysical = true;
                        _IsMagic = false;
                    }
                    else
                    {
                        _IsPhysical = false;
                        _IsMagic = true;
                        _MagicElement = mainChar.BattleOffensiveSkill.Element;
                    }

                    switch (targetTypee) {
            			case TargetType.Single:
            				isSingle = true;
            				targetVecs = filterTarget (attacker);
            				_TargetType = TargetType.Single;
            				break;
            			case TargetType.Row:
            				targetVecs = targetData.row;
            				_TargetType = TargetType.Row;
            				break;
            			case TargetType.Column:
            				targetVecs = targetData.column;
            				_TargetType = TargetType.Column;
            				break;
            			case TargetType.Square:
            				targetVecs = targetData.square;
            				_TargetType = TargetType.Square;
            				break;
            			case TargetType.All:
            				targetVecs = targetData.all;
            				_TargetType = TargetType.All;
            				break;
            			}

    			ignorePos = (attacker.AttackType == AttackType.Caster)? true: false;
    			battler.BattleStateCondition = BattleStateCondition.SkillOffensive;

    			startSelect();
			break;
            case SkillTypes.UltimateDefense: // defensive skill
                TargetType targetType = mainChar.BattleOffensiveSkill.Target; 

                onSelf = false;
                ignorePos = true;
                onEnemy = false;
                _IsPhysical = false;
                _IsMagic = false;

                switch (targetType)
                {
                    case TargetType.Single:
                        isSingle = true;
                        targetVecs = filterTarget(attacker);
                        break;
                    case TargetType.Row:
                        isSingle = false;
                        targetVecs = targetData.row;
                        break;
                    case TargetType.Column:
                        isSingle = false;
                        targetVecs = targetData.column;
                        break;
                    case TargetType.Square:
                        isSingle = false;
                        targetVecs = targetData.square;
                        break;
                    case TargetType.All:
                        isSingle = false;
                        targetVecs = targetData.all;
                        break;
                }

                _TargetType = targetType;
                ignorePos = true;
                startSelect();
//			if ((TargetFormation)mainChar.BattleDefensiveSkill.TargetFormationType == TargetFormation.Self)
//			{
//				targets.Add (attacker);
//				onSelf = true;
//				isSingle = true;
//				ignorePos = true;
//				onEnemy = false;
//                _IsPhysical = false;
//                _IsMagic = false;
//                targetVecs = filterTarget(attacker);
//			}
//			else if ((TargetFormation)mainChar.BattleDefensiveSkill.TargetFormationType == TargetFormation.Ally){
//                TargetType targetType = mainChar.BattleOffensiveSkill.Target; //(TargetType)mainChar.BattleDefensiveSkill.Target;
//
//				onSelf = false;
//				ignorePos = true;
//				onEnemy = false;
//                _IsPhysical = false;
//                _IsMagic = false;
//
//                switch (targetType){
//        			case TargetType.Single:
//        				isSingle = true;
//        				targetVecs = filterTarget (attacker);
//        				break;
//        			case TargetType.Row:
//        				isSingle = false;
//        				targetVecs = targetData.row;
//        				break;
//        			case TargetType.Column:
//        				isSingle = false;
//        				targetVecs = targetData.column;
//        				break;
//        			case TargetType.Square:
//        				isSingle = false;
//        				targetVecs = targetData.square;
//        				break;
//        			case TargetType.All:
//        				isSingle = false;
//        				targetVecs = targetData.all;
//        				break;
//        			}
//
//				_TargetType = targetType;
//
//			}
//			else if ((TargetFormation)mainChar.BattleDefensiveSkill.TargetFormationType == TargetFormation.All){
//				onSelf = false;
//				isSingle = false;
//				ignorePos = true;
//				onEnemy = false;
//                _IsPhysical = false;
//                _IsMagic = false;
//                targetVecs = targetData.all;
//			}
//			battler.BattleStateCondition = BattleStateCondition.SkillDefensive;
//			startSelect();
			break;
            default: // combination
                Debug.Log("NOPE !!");
//			isSelectedPartner = false;
//			batPartnerCombo = null;
//			onDead = false;
//			isSingle = true;
//			onEnemy = false;
//			isCombination = true;
//			targetVecs = SelectPartner (battler);
//			startSelect ();
//			battler.isCombination = true;
//			battler.BattleStateCondition = BattleStateCondition.SkillCombination;
//			selectAttackTarget(attacker);
			break;
		}
		onDead = false;
		isChange = false;
	}
	public void selectMagicTarget(Battler attacker, int magicId){
        onSelf = false;
        isSingle = false;
        onEnemy = true;
        onDead = false;
        isChange = false;
        _IsPhysical = false;
        _IsMagic = true;

        battler = attacker;
        Magic magic = attacker.GetMagic(magicId);
        _MagicElement = magic.Element;
        TargetType targetType = magic.MagicTargetType;
//		Debug.Log (targetType);

        switch (targetType)
        {
            case TargetType.Single:
                isSingle = true;
                targetVecs = filterTarget(attacker);
                break;
            case TargetType.Row:
                targetVecs = targetData.row;
                break;
            case TargetType.Column:
                targetVecs = targetData.column;
                break;
            case TargetType.Square:
                targetVecs = targetData.square;
                break;
            case TargetType.All:
                targetVecs = targetData.all;
                break;
        }
		
        _TargetType = targetType;
        ignorePos = (attacker.AttackType == AttackType.Caster) ? true : false;

        startSelect();
    }
    public void selectNormalSkillTarget(Battler attacker, int normalID)
    {
        battler = attacker;
        onSelf = false;
        onDead = false;
        isChange = false;
        ignorePos = false;

        _IsPhysical = false;
        _IsMagic = false;

        MainCharacter character = (attacker.Character as MainCharacter);
        NormalSkill normalSkill = character.GetNormalSkill(normalID);

        onEnemy = (normalSkill.TargetFormationType == TargetFormation.Enemy)? true : false;
        ignorePos = (onEnemy)?true: false;

        battler.BattleStateCondition = (onEnemy) ? BattleStateCondition.ItemDebuff : BattleStateCondition.ItemBuff;

        if (normalSkill.Target == TargetType.Single)
        {
            isSingle = true;
            targetVecs = filterTarget(attacker);
        }
        else
            targetVecs = getTargetTypeVec(normalSkill.Target);

        _TargetType = normalSkill.Target;
        if (targetVecs.Length > 0)
            startSelect ();
        else
            sendCancel ();
    }
	public void selectMoveTarget(Battler battler){
		this.battler = battler;
		targetVecs = filterPos ();
		_TargetType = TargetType.Single;
		onSelf = false;
		isSingle = true;
		onEnemy = false;
		isMove = true;
        _IsPhysical = false;
        _IsMagic = false;
		startSelect ();
	}
	#endregion


	private Vector2[] filterTarget(Battler attacker){
		_TargetType = TargetType.Single;
		List<Battler> targets = new List<Battler>();
		filteredTarget = new List<Battler>();

		BattlerType attackerType = attacker.BattlerType;
		
		if (onEnemy) {
			targets = (attackerType == BattlerType.Player)? 
				battleData.encounterList : battleData.charList;
		} else {
			targets = (attackerType == BattlerType.Player)? 
				battleData.charList : battleData.encounterList;
		}

		if (onSelf){
			filteredTarget.Add (attacker);
		}
		else
		{
			if (!onDead)
			{
				foreach(Battler target in targets.Where(x => x.BattlerState != BattlerState.Dead)){
					if(onEnemy){
						if(ignorePos){
							filteredTarget.Add(target);
						}else if(canAttack (attacker, target)){
							filteredTarget.Add(target);
						}
					}else{
						filteredTarget.Add(target);
					}
				}
			}
			else{
                if (targets.Any(x => x.BattlerState == BattlerState.Dead))
                {
                    foreach (Battler target in targets.Where(x => x.BattlerState == BattlerState.Dead))
                    {
                        if (onEnemy)
                        {
                            if (ignorePos)
                            {
                                filteredTarget.Add(target);
                            }
                            else if (canAttack(attacker, target))
                            {
                                filteredTarget.Add(target);
                            }
                        }
                        else
                        {
                            filteredTarget.Add(target);
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < targets.Count;i++) // each (Battler target in targets.Where(x => x.BattlerState == BattlerState.Dead))
                    {
                        if (onEnemy)
                        {
                            if (ignorePos)
                            {
                                filteredTarget.Add(targets[i]);
                            }
                            else if (canAttack(attacker, targets[i]))
                            {
                                filteredTarget.Add(targets[i]);
                            }
                        }
                        else
                        {
                            filteredTarget.Add(targets[i]);
                        }
                    }
                }

			}
		}
		return filteredTarget.ConvertAll(x => x.BattlePosition).ToArray();

	}

	private Vector2[] filterPos(){
		targetPlane = battleData.playerPlanes.Where(x => x.placeAble == true).ToList();

		return targetPlane.ConvertAll(x => x.planePos).ToArray();
	}
	#region chance
	public void ChanceCharacter (Battler batChange){
		targetList = new List<Battler>();
		filteredTarget = new List<Battler>();
		filteredTarget = battleData.reserveList;

		onEnemy = false;
		isSingle = true;
		onSelf = false;
		isMove = false;
		onDead = false;
        isChange = true;
        _IsPhysical = false;
        _IsMagic = false;
		battler = batChange;
		startSelect ();
	}
	#endregion
	private void startSelect(){

        battleData.battlerList.ForEach(x => x.transform.eulerAngles = x.MyPlane.transform.eulerAngles);
		if (!isChange) {
			mirror = (onEnemy) ? -1 : 1;
			targetTemp = null;
            targetListTemp = new List<Battler>();
			if (isSingle) {
				xOffset = 0;
				yOffset = 1;
			} else {
				xOffset = 0;
				yOffset = 0;
			}

			if (isSingle) {
				xMax = 2;
				yMax = Mathf.FloorToInt (targetVecs.Max (vec => vec.y));

				xMin = 0;
				yMin = Mathf.FloorToInt (targetVecs.Min (vec => vec.y));
			} else {
				xMax = Mathf.FloorToInt (targetVecs.Max (vec => vec.x));
				yMax = Mathf.FloorToInt (targetVecs.Max (vec => vec.y));

				xMin = Mathf.FloorToInt (targetVecs.Min (vec => vec.x));
				yMin = Mathf.FloorToInt (targetVecs.Min (vec => vec.y));
			}
			updateTarget ();
		} else {
//			targetList.Add (filteredTarget [0]);
			xOffset = 0;
			yOffset = 0;

			xMin = 0;
			xMax = filteredTarget.Count-1;
			yMin = 0;
			yMax = filteredTarget.Count-1;

            if (xMax < 0)
                xMax = 0;
             if (yMax < 0) 
                yMax = 0;

            for (int i = 0; i < filteredTarget.Count; i++){// each (Battler bat in filteredTarget) {
                filteredTarget[i].HealthBar.gameObject.SetActive (true);
			}

			changeTarget ();
		}
		battleInput.onReqInput += HandleonReqInput;
	}

	private void changeOffset(int x, int y){
		xOffset += x * mirror;
		yOffset += y * mirror;


        if (!isChange)
        {
            if (xOffset <= xMin) {
                xOffset = xMin;
            }
            if (yOffset <= yMin) {
                yOffset = yMin;
            }

            if (isMove){
                if (yOffset >= yMax)
                    yOffset = yMax;
            }

            if (isSingle) {
                if (xOffset >= xMax)
                    xOffset = xMax;

                if (yOffset >= yMax)
                    yOffset = yMax;

            } else {
                if(xOffset >= 2 - xMax){
                    xOffset = 2 - xMax;
                }
                if (yOffset >= 1 - yMax){
                    yOffset = 1 - yMax;
                }
            }
            updateTarget();
        }
        else
        {
            foreach (Battler bat in filteredTarget)
            {
                bat.HealthBar.unSelectedReserve();
            }

            xMin = 0;
            xMax = filteredTarget.Count - 1;
            yMin = 0;
            yMax = filteredTarget.Count - 1;

            if (yOffset < yMin)
                yOffset = yMax;
            if (yOffset > yMax)
                yOffset = yMin;

            changeTarget();
        }
	}
	private void changeTarget (){
        battleData.BattleCameraController.changePerspective = false;
		targetList = new List <Battler> ();
        filteredTarget [yOffset].HealthBar.reserveSelect ();
		targetList.Add (filteredTarget [yOffset]);
	}
	private void updateTarget(){
        for (int i = 0; i < battleData.charList.Count; i++)
        {
            Battler b = battleData.charList[i];
            if (b.BattlerState != BattlerState.Dead)
                b.transform.position = b.MyPlane.transform.position;
        }
        for (int i = 0; i < battleData.encounterList.Count; i++)
        {
            Battler b = battleData.encounterList[i];
            if (b.BattlerState != BattlerState.Dead)
            {
                b.StopShaderSequence();
                b.ActivateBattler(true);
            }
        }

		List <BattlePlane> selectedPlaneTemp = new List<BattlePlane>();

		if (targetVecs.Length > 0) {
			targetPointer.ForEach (x => x.DestroyBattleAPS ());
			targetPointer.Clear ();
			selectedVecs.Clear ();
			selectedPlane.Clear ();

			if (!isSingle) {
				foreach (Vector2 vec in targetVecs) {
					selectedVecs.Add (vec + new Vector2 (xOffset, yOffset));
				}
			}

			//handle if target lis (ally or enemy)
			if (onEnemy) {
				battleData.enemyPlanes.ForEach (x => x.mesh.enabled = false);

				if (isSingle){

				}
				else{
					targetList = battleData.encounterList.Where (x => selectedVecs.Contains (x.BattlePosition)).ToList ();
					selectedPlaneTemp = battleData.enemyPlanes.Where (x => selectedVecs.Contains (x.planePos)).ToList ();
					foreach (BattlePlane bp in selectedPlaneTemp){
						if (!bp.placeAble){
							selectedPlane.Add (bp);
						}
					}
				}
			} else {
				battleData.playerPlanes.ForEach (x => x.mesh.enabled = false);

				if (isSingle){

				}
				else{
					targetList = battleData.charList.Where (x => selectedVecs.Contains (x.BattlePosition)).ToList ();
					selectedPlaneTemp = battleData.playerPlanes.Where (x => selectedVecs.Contains (x.planePos)).ToList ();
					foreach (BattlePlane bp in selectedPlaneTemp){
						if (!bp.placeAble && !isMove){
							selectedPlane.Add (bp);
						}
					}
				}
			}

			//if command is Move
			if (isMove) {
                battleData.encounterList.ForEach(x => x.ActivateBattler(false));//.SetActive(false));
				battleData.BattleCameraController.changePerspective = true;
				Vector2 positionSelect = new Vector2 (xOffset, yOffset);
				BattlePlane plane = targetPlane.Find (x=>x.planePos == positionSelect);
				Battler bat = battleData.charList.Find (x=> x.BattlerType == BattlerType.Player);
				battleData.BattleCameraController.characterSelectView(bat, false);
				if (plane != null){
                    battleData.BattleCameraController.isSelected = true;
					SelectLoop = 0;
					//plane.mesh.enabled = true;

                    GameObject pointer = poolingSystem.InstantiateAPS ("Cursor", plane.transform.position + new Vector3 (0, 0.1f, 0), new Quaternion ());
                    //pointer.transform.localScale = plane.SelectorSize;
                    pointer.transform.eulerAngles = new Vector3(270f,0f,0f);
                    targetPointer.Add (pointer);

					selectedPlane.Add (plane);
					xOffsetTemp = xOffset;
					yOffsetTemp = yOffset;
				}
				else{
					SelectLoop++;
					if (SelectLoop <= 4){
						skipSelection();
					}
					else{
						if (yOffset == 0){
							SelectLoop = 0;
							HandleonReqInput(2);
						}
						else{
							SelectLoop = 0;
							HandleonReqInput(0);
						}
					}
				}
			} 
			//if command !isMove
			else 
            {
				if (isSingle) { //if target is single
					targetList = new List<Battler>();
					Vector2 positionSelect = new Vector2 (xOffset, yOffset);
					Battler target;


					if (onEnemy){ //if target is enemy
						battleData.BattleCameraController.changePerspective = false;
						target = _BattleController.EnemyBattlePos.GetBattlerByPosition((int)positionSelect.x, (int)positionSelect.y);
					}
					else{  //if target is ally
						battleData.BattleCameraController.changePerspective = true;
						target = _BattleController.PlayerBattlePos.GetBattlerByPosition((int)positionSelect.x, (int)positionSelect.y);
					}

					if (filteredTarget.Find(x=> x==target)){
						target = target;
					}else{
						target = null;
						target = GetBattlerSide();
					}


                    if (target != null){

						SelectLoop = 0;
						battleData.BattleCameraController.isSelected = true;
						GameObject pointer = poolingSystem.InstantiateAPS ("Cursor", target.transform.position + new Vector3 (0, 0.1f, 0), new Quaternion ());
						pointer.transform.eulerAngles = new Vector3(270f,0f,0f);
						pointer.transform.localScale = target.SelectorSize;
						targetPointer.Add (pointer);
						if (target != targetTemp){
                            if (disabledBattler != null)
                                disabledBattler.ActivateBattler(true);// MyPlane.transform.position;//disabledBattler.gameObject.SetActive(true);
                            disabledBattler = new Battler();

							if (onEnemy) {
                                disabledBattler = battler.GetAllyInFront(target, false);
								battleData.BattleCameraController.pointerView(target, true);
							}
                            else {
                                battleData.encounterList.ForEach(x=>x.ActivateBattler(false));
                                disabledBattler = battler.GetAllyInFront(target, true);
                                battleData.BattleCameraController.characterSelectView(target, true);
                            }

                                if (disabledBattler != null)
                                disabledBattler.ActivateBattler(false);// transform.position = new Vector3(disabledBattler.MyPlane.transform.position.x, -30, disabledBattler.MyPlane.transform.position.z);//gameObject.SetActive(false);
                        }else{
                            if (disabledBattler != null)
                                disabledBattler.ActivateBattler(false);
                            
                            if (!onEnemy) 
                                battleData.encounterList.ForEach(x=>x.ActivateBattler(false));
                        }

                        if (onEnemy && target != targetTemp) {
                            infoBattler.ForEach(x => x.DestroyBattleAPS());
                            infoBattler.Clear ();

                            GameObject infoBar = poolingSystem.InstantiateAPS ("InfoPopUp", target.InfoHelper.position, target.InfoHelper.rotation);
                            target.InfoPopUp(infoBar, battler, _IsPhysical, _IsMagic, _MagicElement);
                            infoBattler.Add (infoBar);

                        }

						targetList.Add(target);
						xOffsetTemp = xOffset;
						yOffsetTemp = yOffset;
						if (onEnemy)
							target.OnSelected ();

					}
					else {
						SelectLoop++;
						if (SelectLoop <= 4){
							skipSelection();
						}
						else{
							SelectLoop = 0;
							if (yOffset == 0){
								HandleonReqInput(2);
							}
							else{
								HandleonReqInput(0);
							}
						}
					}
                    if (target != null)
                        targetTemp = target;

				} else {
					//change view camera on target
					bool battlerSelected = false;
                    if (targetList.Count > 0 && !CanAttackNotSingleTarget()){
						battleData.BattleCameraController.isSelected = true;
                        for (int i = 0;i < targetList.Count; i++){ //  each (Battler b in targetList) {
							if (!battlerSelected){
								battlerSelected = true;
                                if (onEnemy) battleData.BattleCameraController.pointerView(targetList[i], false);
                                else battleData.BattleCameraController.characterSelectView(targetList[i], false);
							}
						}
					} 
                    else 
                    {
						if (onEnemy) {
							Battler bat = battleData.encounterList.Find (x=> x.BattlerType == BattlerType.Enemy);
							battleData.BattleCameraController.pointerView(bat, false);
                        }else{
							Battler bat = battleData.charList.Find (x=> x.BattlerType == BattlerType.Player);
							battleData.BattleCameraController.characterSelectView(bat, false);
						}
					}

                    if (selectedPlane.Count > 0 && targetList.Any (x=> x.BattlerState != BattlerState.Dead) && !CanAttackNotSingleTarget()){
						SelectLoop = 0;
                        battleData.BattleCameraController.isSelected = true;
                        if (!onEnemy)
                            battleData.encounterList.ForEach(x=>x.ActivateBattler(false));
                        
                        if (ClearInfoBar())
                        {
                            infoBattler.ForEach(x => x.DestroyBattleAPS());
                            infoBattler.Clear();
                        }

                        for (int i = 0; i < selectedPlane.Count; i++){ // foreach (BattlePlane bp in selectedPlane) {
                            for (int y = 0; y < targetList.Count; y++){ //each (Battler b in targetList) {
                                if(targetList[y].BattlePosition.Equals(selectedPlane[i].planePos) && targetList[y].BattlerState != BattlerState.Dead){

                                    //selectedPlane[i].mesh.enabled = true;
                                    GameObject pointer = poolingSystem.InstantiateAPS ("Cursor", targetList[y].transform.position + new Vector3 (0, 0.1f, 0), new Quaternion ());
                                    pointer.transform.localScale = targetList[y].SelectorSize;
									pointer.transform.eulerAngles = new Vector3(270f,0f,0f);
									targetPointer.Add (pointer);
									xOffsetTemp = xOffset;
									if (onEnemy) {
                                        GameObject infoBar = poolingSystem.InstantiateAPS ("InfoPopUp", targetList[y].InfoHelper.position, targetList[y].InfoHelper.rotation);
                                        targetList[y].InfoPopUp (infoBar, battler, _IsPhysical, _IsMagic, _MagicElement);
										infoBattler.Add (infoBar);
                                        targetList[y].OnSelected ();
									}
								}
							}
							
						}
					}else{
						SelectLoop++;
						skipSelection();
					}

                    targetListTemp = targetList;
				}
			}
		} else {
			sendCancel();
		}
	}

    private bool ClearInfoBar (){
        for (int i = 0; i < targetListTemp.Count; i++)
        {
            if (targetListTemp[i] != targetList[i])
            {
                return true;
            }
        }
        return false;
    }
	//pindah posisi yOffset jika target dari lokasi xOffset = null
 	private Battler GetBattlerSide (){
		Vector2 positionSelect = new Vector2 (xOffset, yOffset);
		Battler target = new Battler();

        if (onEnemy)
        {
            if (yOffset == 0)
            {
                target = _BattleController.EnemyBattlePos.GetBattlerByPosition((int)positionSelect.x, (int)positionSelect.y + 1);
            }
            else if (yOffset == 1)
            {
                target = _BattleController.EnemyBattlePos.GetBattlerByPosition((int)positionSelect.x, (int)positionSelect.y - 1);
            }
        }
        else
        {
            if (yOffset == 0)
            {
                target = _BattleController.PlayerBattlePos.GetBattlerByPosition((int)positionSelect.x, (int)positionSelect.y + 1);
            }
            else if (yOffset == 1)
            {
                target = _BattleController.PlayerBattlePos.GetBattlerByPosition((int)positionSelect.x, (int)positionSelect.y - 1);
            }
        }

		//cek apakah target yang ada di lokasi sesuai dengan target yang telah di filter melee/range dll
		if (filteredTarget.Find(x=> x==target) && target != targetTemp){
			target = target;
			xOffset = (int)target.BattlePosition.x;
			yOffset = (int)target.BattlePosition.y;
			return target;
		}else{
			target = null;
			return null;
		}
	}

	private void skipSelection (){
		switch (_TargetType){
		case TargetType.Column:
			if (xOffset == 0){
				xOffsetTemp = 0;
				HandleonReqInput(3);
			}else if (xOffset == 1){
				if (xOffsetTemp == 0){
					HandleonReqInput(3);
				}
				else {
					HandleonReqInput(1);
				}
			}else if (xOffset == 2) {
				xOffsetTemp = 2;
				HandleonReqInput(1);
			}
			break;
		case TargetType.Row:
			if (yOffset == 0){
				HandleonReqInput(2);
			}else{
				HandleonReqInput(0);
			}
			break;
		case TargetType.Single: 
			if (isSingle || isMove){ 
				if (xOffset == 0){  //cek posisi xOffset 0, jika target pada offset 0 = null
					xOffsetTemp = 0; 
					HandleonReqInput(3); 
				}else if (xOffset == 1){ 
					if (xOffsetTemp == 0){ // cek posisi xOffset terakhir yang di select
						HandleonReqInput(3);
					}
					else {
						HandleonReqInput(1);
					}
				}else if (xOffset == 2) {
					xOffsetTemp = 2;
					HandleonReqInput(1);
				}
			}
			break;
		}
	}
	//magic melee possibility
	//cek posisi jika berada pada posisi 1 (belum)
	public bool checkAllyFrontBattler (Battler battler){
		Vector2 pos = new Vector2 (0, 1);
		List <Battler> allyFrontBattler = new List<Battler>();
		allyFrontBattler = battleData.charList.FindAll(x => x.BattlePosition.y.Equals(pos.y) && x.BattlerState != BattlerState.Dead);

		if (battler.BattlePosition.y == 1){
			return false;
		}
		else{
			if (allyFrontBattler.Count > 0) return true;
			else return false;
		}
	}
    //mendapatkan ally didepan battel

	//attack possibility by battle position
	public bool canAttack(Battler attacker, Battler target){
		bool attackAble = false;

		Battler allyFrontBattler = 
			battleData.Controller.GetBattlerByFormation(attacker.BattlerType, new Vector2(attacker.BattlePosition.x, 1));
		Battler enemyFrontBattler = 
			battleData.Controller.GetBattlerByFormation(target.BattlerType, new Vector2(target.BattlePosition.x, 1));
		List<Battler> enemyFrontBattlers = 
			battleData.Controller.GetRowBattlers (target, target.BattlerType, true);
		List<Battler> allyFrontBattlers = 
			battleData.Controller.GetRowBattlers (attacker, attacker.BattlerType, true);


//cek kondisi enemy di posisi 0 & cek kondisi target canAttack / !canAttack
		if (target.BattlerState == BattlerState.Dead || target == null) {
			return false;
		} 
		//executed when ally front attacker is null or die, and position target in front
		else if (attacker.BattlePosition.y == 0 && (allyFrontBattlers.Count == 0 || allyFrontBattlers.All(x=> x.BattlerState == BattlerState.Dead)) && target.BattlePosition.y == 1){
			return true;
		} 
		//executed when ally front attacker is null or die, and front of enemy is null or die
		else if (attacker.BattlePosition.y == 0 && 
		          (allyFrontBattler == null || allyFrontBattler.BattlerState == BattlerState.Dead) && 
		          (enemyFrontBattler == null || enemyFrontBattler.BattlerState == BattlerState.Dead)){
			return true;
		} 


		switch (attacker.AttackType) {
		case AttackType.Melee:
			if(attacker.BattlePosition.y == 1){
				if(target.BattlePosition.y == 1){
					attackAble = true;
				}else{
					if (enemyFrontBattlers.Count == 0 || enemyFrontBattlers.All (x=>x.BattlerState == BattlerState.Dead)){
						attackAble = true;
					}
				}
			}
			break;
		case AttackType.Range: case AttackType.Caster:
			if(attacker.BattlePosition.y == 0){
				if(target.BattlePosition.y == 1){
					attackAble = true;
				}else{
//					if(enemyFrontBattler == null || enemyFrontBattler.BattlerState == BattlerState.Dead){
						attackAble = true;
//					}
				}
			}else{
				attackAble = true;
			};
			break;
		}
		
		return attackAble;
	}
    private bool CanAttackNotSingleTarget (){
        bool attackable = false;
        List<Battler> allyFront = new List<Battler>();

        if (targetList.Count > 0 && battler.AttackType == AttackType.Melee && onEnemy && !ignorePos)
        {
            if (_TargetType == TargetType.Row)
            {
                allyFront = battleData.Controller.GetRowBattlers(targetList[0]); 
            }
        }

        if (allyFront != null && allyFront.Count > 0)
            attackable = true;
        else
            attackable = false;  
        
        return attackable;

    }
	private Vector2[] getTargetTypeVec(TargetType type){
		_TargetType = type;
		switch (type) {
		case TargetType.Row:
			return targetData.row;
			break;
		case TargetType.Column:
			return targetData.column;
			break;
		case TargetType.Square:
			return targetData.square;
			break;
		case TargetType.All:
			return targetData.all;
			break;
		}
		return null;
	}
	private void sendData(){
		bool callOnce = false;
		SelectLoop = 0;
		battleInput.onReqInput -= HandleonReqInput;
		List <Battler> targets = targetList.FindAll(x => x.BattlerState != BattlerState.Dead);
		if (isMove) {
			if (OnSelectPos != null) {
				OnSelectPos (selectedPlane [0]);
			}
		} else {
			if (targets.Count > 0 && !onDead){
				if (OnSelectTarget != null && !isChange) {
					OnSelectTarget (targetList);
				}

				if (OnSelectReserve != null && isChange) {
					OnSelectReserve (targetList [0], yOffset);
				}
			}
            else if (targetList.Count > 0  && onDead){ //&&  targetList.All(x => x.BattlerState == BattlerState.Dead) 
				if (OnSelectTarget != null) {
					OnSelectTarget (targetList);
				}
			}
			else {
				sendCancel();
			}
		}
//		if (!isCombination)
			reset ();
	}
	private void sendCancel(){
		battleInput.onReqInput -= HandleonReqInput;
		if (OnCancel != null) {
			OnCancel();
		}
//		isCombination = false;
//		battler.isCombination = false;
		reset ();
	}
	private void reset(){
        for (int i = 0; i < battleData.reserveList.Count; i++){ // each (Battler bat in battleData.reserveList) {
            Battler bat = battleData.reserveList[i];
            bat.HealthBar.unSelectedReserve ();
			bat.HealthBar.gameObject.SetActive (false);
		}

        if (disabledBattler != null && disabledBattler.BattlerState != BattlerState.Dead)
            disabledBattler.transform.position = disabledBattler.MyPlane.transform.position;//disabledBattler.gameObject.SetActive(true);

        disabledBattler = new Battler();
		battleData.BattleCameraController.isSelected = false;
//		battleData.BattleCameraController.resetBlinkCamera();
        battleData.encounterList.ForEach (x => x.StopShaderSequence ());
        battleData.battlerList.ForEach (x => x.ActivateBattler (true));
		targetPointer.ForEach(x => x.DestroyBattleAPS());
		targetPointer.Clear ();
		infoBattler.ForEach(x => x.DestroyBattleAPS());
		infoBattler.Clear ();

		battleData.playerPlanes.ForEach (x => x.mesh.enabled = false);
		battleData.enemyPlanes.ForEach (x => x.mesh.enabled = false);

		ignorePos = false;
		isMove = false;
		isSingle = false;
		isChange = false;
		onEnemy = false;
		onDead = false;
		onSelf = false;
        _IsPhysical = false;
        _IsMagic = false;
        _MagicElement = Element.None;

        targetListTemp = new List<Battler>();
        targetTemp = null;

	}
	
	private void HandleonReqInput (int key){
		if (key == (int)BattleInputKey.CANCEL ) {
			WwiseManager.Instance.PlaySFX(WwiseManager.UIFX.Cancel );
			sendCancel();
		}else if (key == (int)BattleInputKey.CONFIRM ) {
			WwiseManager.Instance.PlaySFX(WwiseManager.UIFX.Confirm);
			sendData();
		}else if (key == (int)BattleInputKey.RIGHT ) {
			WwiseManager.Instance.PlaySFX(WwiseManager.UIFX.Cursor_Moving);
			//			changeOffset(1,0);
			if (battleData.BattleCameraController.changePerspective)
				changeOffset(-1,0);
			else
				changeOffset(1,0); //true
		}else if (key == (int)BattleInputKey.LEFT ) {
			WwiseManager.Instance.PlaySFX(WwiseManager.UIFX.Cursor_Moving);
			//			changeOffset(-1,0);
			if (battleData.BattleCameraController.changePerspective)
				changeOffset(1,0);
			else
				changeOffset(-1,0);
		}else if (key == (int)BattleInputKey.UP ) {
			WwiseManager.Instance.PlaySFX(WwiseManager.UIFX.Cursor_Moving);
			if (battleData.BattleCameraController.changePerspective)
				changeOffset(0,-1);
			else
				changeOffset(0,1);
		}else if (key == (int)BattleInputKey.DOWN) {
			WwiseManager.Instance.PlaySFX(WwiseManager.UIFX.Cursor_Moving);
			if (battleData.BattleCameraController.changePerspective)
				changeOffset(0,1);
			else
				changeOffset(0,-1);
		}
	}

	private Vector2[] filterNotSingleTarget (TargetType targetType){
		Vector2 [] targetLoc;
		List <BattlePlane> bp = new List<BattlePlane>();
		switch (targetType){
		case TargetType.All:
			break;
		case TargetType.Column:
			break;
		case TargetType.Row:
			break;
		}

		return null;

	}

}
