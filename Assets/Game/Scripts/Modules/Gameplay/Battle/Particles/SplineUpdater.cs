﻿using UnityEngine;
using System.Collections;

public class SplineUpdater : MonoBehaviour {
    public BezierSpline Spline;
    public Vector3 Offset;
	// Update is called once per frame
	void Update () {
        if(Spline != null)
            Spline.SetControlPoint(3, this.transform.position - Spline.transform.position);
            Spline.SetControlPoint(2, new Vector3(Offset.x, Offset.y, Spline.GetControlPoint(3).z));
    }
}
