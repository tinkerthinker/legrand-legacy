﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class ParticleLightFade : MonoBehaviour {

    public float DelayFade;
    public float FadeSpeed = 0.5f;
    public float DefaultIntensity = 2;
    private Light _Light;
	// Use this for initialization
    void Awake (){
        _Light = GetComponent<Light>();
    }
	void Start () {
        EndFade();
	}
//
//    private void StartFade (){
//        _Light.DOIntensity(DefaultIntensity, FadeSpeed).SetDelay(DelayFade).OnComplete(EndFade);
//    }

    private void EndFade (){
        _Light.DOIntensity(0, FadeSpeed);
    }
	
}
