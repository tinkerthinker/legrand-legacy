﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BattleParticleRangeProjectile : BattleParticle
{
    public List<GameObject> ActivateOnHit;
    public List<GameObject> DiactivateOnHit;
    private GameObject target;
    bool shoot = false;
    private GameObject trans;
    public float speed = 25f;
    public int time = 2;

    Transform targetPos;
    // Use this for initialization

    public override void Initiate(GameObject source, GameObject t)
    {
        trans = source;
        target = t;
        targetPos = t.transform;
        shoot = true;
    }
    private void shooting()
    {
        float distance = Vector3.Distance(transform.position, targetPos.position);
        if (distance <= 0.5f)
        {
            explodeParticle();
        }
        else
        {
            transform.position = Vector3.MoveTowards(transform.position, targetPos.position, Time.smoothDeltaTime * speed);
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (shoot)
        {
            shooting();
        }
    }

    public void explodeParticle()
    {
        shoot = false;
        foreach (GameObject g in DiactivateOnHit)
            if (g) g.SetActive(false);
        foreach (GameObject g in ActivateOnHit)
            if (g) g.SetActive(true);

        if (OnParticleDone != null)
        {
            OnParticleDone(this);
        }
        OnParticleDone = null;
        StartCoroutine(DestroyAPS());
    }

    IEnumerator DestroyAPS()
    {
        yield return new WaitForSeconds(time);
        for (int i = 0; i < ActivateOnHit.Count; i++)
        {
            ActivateOnHit[i].SetActive(false);
        }
        for (int i = 0; i < DiactivateOnHit.Count; i++)
        {
            DiactivateOnHit[i].SetActive(true);
        }

        this.gameObject.SetActive(false);
    }

    void OnDisable()
    {
        this.gameObject.transform.position = new Vector3(0, 0, 0);
        this.gameObject.transform.eulerAngles = new Vector3(0, 0, 0);
    }
}
