﻿using UnityEngine;
using System.Collections.Generic;

public class BattleParticleDestoryWaitForSecond : MonoBehaviour {
    public List<GameObject> DestroyOnDone;
    public float Time;
    // Use this for initialization
    void Start () {
        foreach (GameObject g in DestroyOnDone)
            Destroy(g, Time);
    }
}
