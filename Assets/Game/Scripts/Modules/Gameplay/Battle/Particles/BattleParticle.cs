﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BattleParticle : MonoBehaviour
{
    public delegate void ParticleDone(BattleParticle bp);
    public ParticleDone OnParticleDone;
    public Dictionary<object, object> DataHelper;
    
    void Awake()
    {
        DataHelper = new Dictionary<object, object>();
    }

    public void AddData(object key, object value)
    {
        DataHelper.Add(key, value);
    }

    public virtual void Initiate(GameObject source, GameObject target)
    {
    }

    public void ClearOnParticle()
    {
        OnParticleDone = null;
    }
}