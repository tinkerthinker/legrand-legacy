﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BattleParticleDestoryOnDone : MonoBehaviour {
    public List<GameObject> DestroyOnDone;
    private ParticleSystem ps;
    // Use this for initialization
    void Start () {
        ps = GetComponent<ParticleSystem> ();
    }

    // Update is called once per frame
    void Update () {
        if (ps) {
            if (!ps.IsAlive ()) {
                DestroyAPSThis ();
            }
        }
    }

    public void DestroyAPSThis(){
        for (int i = 0; i < DestroyOnDone.Count; i++)
        {
//            Destroy(DestroyOnDone[i]);
            DestroyOnDone[i].DestroyBattleAPS();
        }
    }
//    public float minDuration = 8;
//    public float maxDuration = 10;
//
//    private float m_MaxLifetime;
//    private bool m_EarlyStop;
//
//    private IEnumerator Start()
//    {
//        var systems = GetComponentsInChildren<ParticleSystem>();
//
//        // find out the maximum lifetime of any particles in this effect
//        foreach (var system in systems)
//        {
//            m_MaxLifetime = Mathf.Max(system.startLifetime, m_MaxLifetime);
//        }
//
//        // wait for random duration
//
//        float stopTime = Time.time + LegrandUtility.Random(minDuration, maxDuration);
//
//        while (Time.time < stopTime || m_EarlyStop)
//        {
//            yield return null;
//        }
//        Debug.Log("stopping " + name);
//
//        // turn off emission
//        foreach (var system in systems)
//        {
//            var emission = system.emission;
//            emission.enabled = false;
//        }
//        BroadcastMessage("Extinguish", SendMessageOptions.DontRequireReceiver);
//
//        // wait for any remaining particles to expire
//        yield return new WaitForSeconds(m_MaxLifetime);
//
//        foreach (GameObject g in DestroyOnDone)
//            Destroy(g);
//    }
//
//
//    public void Stop()
//    {
//        // stops the particle system early
//        m_EarlyStop = true;
//    }
}
