﻿using UnityEngine;
using System.Collections;

public class ParticleTester : MonoBehaviour {
    public GameObject Prefab;
    public GameObject Source;
    public GameObject Target;

    private Animator _Animator;

    void Start()
    {
        _Animator = GetComponent<Animator>();
    }
    // Update is called once per frame
	void Update () {
	    if(Input.GetKeyDown(KeyCode.Space))
        {
            _Animator.CrossFade("COMBO",0f);
        }
	}

    void OnAttackEvent(string particleData)
    {
        int direction = 0;
        string[] particleDatas = particleData.Split(',');

        if (particleDatas.Length > 1)
            direction = int.Parse(particleDatas[1]); 

        Vector3 dir = new Vector3();

        if (direction == 0)
            dir = new Vector3(0f, 0f, 0f);
        else if (direction == 1)
        {
            dir = transform.TransformDirection(1f, 0f, 0f);
            if (dir.x > 0f)
                dir = new Vector3(0,0,1f);
            else if(dir.x < 0f)
                dir = new Vector3(0,0,2f);
        }
        else if (direction == 2)
        {
            dir = transform.TransformDirection(-1f, 0f, 0f);
            if (dir.x > 0f)
                dir = new Vector3(0,0,1f);
            else if(dir.x < 0f)
                dir = new Vector3(0,0,2f);
        }
        else if (direction == 3) dir = new Vector3(0f,0f,3f);

        GameObject p = Instantiate<GameObject>(Prefab);
        p.transform.position = Source.transform.position;
        BattleParticle battleParticle = p.GetComponent<BattleParticle>();
        battleParticle.AddData("Direction", Mathf.RoundToInt(dir.z));
        battleParticle.Initiate(Source, Target);
    }

    void OnLastAttackEvent(string particleData)
    {
        int direction = 0;
        string[] particleDatas = particleData.Split(',');

        if (particleDatas.Length > 1)
            direction = int.Parse(particleDatas[1]);

        Vector3 dir = new Vector3();

        if (direction == 0)
            dir = new Vector3(0f, 0f, 0f);
        else if (direction == 1)
        {
            dir = transform.TransformDirection(1f, 0f, 0f);
            if (dir.x > 0f)
                dir = new Vector3(0, 0, 1f);
            else if (dir.x < 0f)
                dir = new Vector3(0, 0, 2f);
        }
        else if (direction == 2)
        {
            dir = transform.TransformDirection(-1f, 0f, 0f);
            if (dir.x > 0f)
                dir = new Vector3(0, 0, 1f);
            else if (dir.x < 0f)
                dir = new Vector3(0, 0, 2f);
        }
        else if (direction == 3) dir = new Vector3(0f, 0f, 3f);

        GameObject p = Instantiate<GameObject>(Prefab);
        p.transform.position = Source.transform.position;
        BattleParticle battleParticle = p.GetComponent<BattleParticle>();
        battleParticle.OnParticleDone += OnParticleDone;
        battleParticle.AddData("Direction", Mathf.RoundToInt(dir.z));
        battleParticle.Initiate(Source, Target);
    }

    void OnParticleDone(BattleParticle bp)
    {
        Debug.Log("DONE");
    }

    void DoneAttackEvent()
    {
        _Animator.CrossFade("BATTLESTANCE",0f);
    }
}
