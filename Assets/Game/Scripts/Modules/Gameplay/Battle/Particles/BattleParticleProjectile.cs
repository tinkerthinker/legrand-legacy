﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BattleParticleProjectile : BattleParticle
{
    public List<GameObject> ActivateOnHit;
    public List<GameObject> DiactivateOnHit;
    public GameObject Target;
    public GameObject TargetHelper;
    public BezierSpline Spline;
    private SplineWalker Walker;

    public GameObject gSpline;
    public bool stopShot;
    public int time = 2;
    void Start()
    {

    }

    public override void Initiate(GameObject source, GameObject target)
    {
        for (int i = 0; i < DiactivateOnHit.Count; i++)
            DiactivateOnHit[i].SetActive(true);
        for (int i = 0; i < ActivateOnHit.Count; i++)
            ActivateOnHit[i].SetActive(false);

        Walker = this.gameObject.AddComponent<SplineWalker>();
        //        if(Walker == null) Walker = GetComponent<SplineWalker>();
        Target = target;

        // Make The spline curve
        gSpline = BattlePoolingSystem.Instance.InstantiateAPS("Particle Spline"); //Instantiate(SplinePrefab); //
        gSpline.transform.position = source.transform.position;
        Spline = gSpline.GetComponent<BezierSpline>();
        Spline.Reset();
        Vector3 sourcePosition = Spline.GetControlPoint(0);
        Vector3 targetPosition = Spline.GetControlPoint(3);

        float zOffset = 0;
        if (!DataHelper.ContainsKey("Direction"))
        {
            zOffset = LegrandUtility.Random(-5f, 5f);
        }
        else
        {
            int iDirection = (int)DataHelper["Direction"];
            if (iDirection == 0) zOffset = LegrandUtility.Random(-5, 5f);
            else if (iDirection == 1) zOffset = LegrandUtility.Random(1f, 5f);
            else if (iDirection == 2) zOffset = LegrandUtility.Random(-5f, -1f);
            else if (iDirection == 3) zOffset = 0f;

        }

        float sourceOffsetY = LegrandUtility.Random(0f, 3f);
        Spline.SetControlPoint(1, new Vector3(zOffset, sourceOffsetY, sourcePosition.z));
        // Create target helper as new gameobject because helper maybe need more than one.
        Target = target;
        TargetHelper = new GameObject();
        TargetHelper.transform.SetParent(target.transform, false);
        SplineUpdater splineUpdater = TargetHelper.AddComponent<SplineUpdater>();
        splineUpdater.Spline = Spline;
        splineUpdater.Offset = new Vector3(zOffset, LegrandUtility.Random(0f, sourceOffsetY), targetPosition.z);

        Walker.spline = Spline;
        Walker.Target = TargetHelper;
    }
    void Update()
    {
        if (Target != null)
        {
            float distance = Vector3.Distance(transform.position, Target.transform.position);

            if (distance <= 0.5f)
            {
                explodeParticle();
            }
        }
    }
    public void explodeParticle()
    {
        if (!stopShot)
        {
            stopShot = true;
            Target = null;
            Destroy(Walker);
            //            Walker = null; 
            Destroy(TargetHelper);
            if (Spline != null)
                gSpline.DestroyBattleAPS();


            foreach (GameObject g in DiactivateOnHit)
                g.SetActive(false);
            foreach (GameObject g in ActivateOnHit)
                g.SetActive(true);

            if (OnParticleDone != null)
            {
                OnParticleDone(this);
            }
            OnParticleDone = null;
            //            gSpline = null;
            Spline = null;

            StartCoroutine(DestroyAPS());
        }
    }

    IEnumerator DestroyAPS()
    {
        yield return new WaitForSeconds(time);
        for (int i = 0; i < ActivateOnHit.Count; i++)
        {
            ActivateOnHit[i].SetActive(false);
        }
        for (int i = 0; i < DiactivateOnHit.Count; i++)
        {
            DiactivateOnHit[i].SetActive(true);
        }

        this.gameObject.SetActive(false);
    }

    void OnDisable()
    {
        stopShot = false;
        this.gameObject.transform.position = new Vector3(0, 0, 0);
        this.gameObject.transform.eulerAngles = new Vector3(0, 0, 0);
    }

}