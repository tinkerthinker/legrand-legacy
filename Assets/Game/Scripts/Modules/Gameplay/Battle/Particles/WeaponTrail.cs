﻿using UnityEngine;
using System.Collections;
using Xft;

public class WeaponTrail : MonoBehaviour {
    public GameObject [] ParticleObject;
    public XWeaponTrail XTrail;

	// Use this for initialization
    public void  ActiveParticle(){
        for (int i = 0; i < ParticleObject.Length; i++)
        {
            ParticleObject[i].SetActive(true);   
        }
        XTrail.enabled = true;
    }

    public void DeactiveParticle (){
        for (int i = 0; i < ParticleObject.Length; i++)
        {
            ParticleObject[i].SetActive(false);
        }
        XTrail.enabled = false;
    }
}
