﻿using UnityEngine;
using System.Collections;

public class BattleParticleStay : BattleParticle {
    [SerializeField]
    public float TimeToDone;

    public Vector3 Offset;

    public override void Initiate(GameObject source, GameObject target)
    {
//        SetParent(target.transform, false); //transform.position = new Vector3(target.tra);//
//        transform.localPosition = Offset;
        transform.position = target.transform.position;
        StartCoroutine(WaitForSendDone());
    }

    IEnumerator WaitForSendDone()
    {
        yield return new WaitForSeconds(TimeToDone);
        if (OnParticleDone != null) OnParticleDone(this);
        yield return new WaitForSeconds(TimeToDone);
//        transform.SetParent(BattlePoolingSystem.Instance.transform);
        gameObject.DestroyBattleAPS();
    }
}
