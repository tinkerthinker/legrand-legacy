using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BattleParticleDestroyAPSTarget : MonoBehaviour
{
    public List<GameObject> DestroyOnDone;
    public float Time;
    // Use this for initialization
    void OnEnable()
    {
        StartCoroutine(DestroyAPS());
    }

    IEnumerator DestroyAPS()
    {
        yield return new WaitForSeconds(Time);
        for (int i = 0; i < DestroyOnDone.Count; i++)
        {// each (GameObject g in DestroyOnDone)
            DestroyOnDone[i].DestroyBattleAPS();
        }

    }
}
