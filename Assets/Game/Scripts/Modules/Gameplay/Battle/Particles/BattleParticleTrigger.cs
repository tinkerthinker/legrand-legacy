﻿using UnityEngine;
using System.Collections;

public class BattleParticleTrigger : MonoBehaviour {
	public delegate void StartAttack();
	public StartAttack OnStartNormalAttack;
	public StartAttack OnStartComboAttack;

	void OnStartAttackEvent(string type)
	{
		if (LegrandUtility.CompareString (type, "normal") && OnStartNormalAttack != null) 
		{
			OnStartNormalAttack ();
		}
		else if (LegrandUtility.CompareString (type, "combo") && OnStartComboAttack != null) 
		{
			OnStartComboAttack ();
		}
	}
}
