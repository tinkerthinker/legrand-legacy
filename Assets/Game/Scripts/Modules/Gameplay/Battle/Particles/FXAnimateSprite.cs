﻿using UnityEngine;
using System.Collections;

public class FXAnimateSprite : MonoBehaviour
{
	//public int ColumnCount = 1;
	public int RowCount = 1;
	public int FPS = 30;

	private bool animateSprite;
	private Renderer rend;
	private float currentRow;
	private float uvSizeY;
	private Vector2 uvOffset;

	void Start ()
	{
		uvSizeY = 1.0f / RowCount;
		Vector2 uvSize = new Vector2 (1.0f, uvSizeY);
		uvOffset = new Vector2 (0.0f, 1.0f - uvSizeY);
		rend = GetComponent<Renderer> ();
		rend.material.SetTextureScale ("_MainTex", uvSize);
		rend.material.SetTextureOffset ("_MainTex", uvOffset);
	}

	void OnEnable ()
	{
		//animateSprite = true;
		//Debug.Log ("Berhasil");
	}

	void OnDisable ()
	{
		animateSprite = true;
	}

	void Update ()
	{
		if (animateSprite == true)
		{
			currentRow = currentRow + (Time.deltaTime * FPS);
			uvOffset = new Vector2 (0.0f, 1.0f - (Mathf.Ceil (currentRow) * uvSizeY));
			rend.material.SetTextureOffset ("_MainTex", uvOffset);
			if(Mathf.Ceil (currentRow) == RowCount)
			{
				animateSprite = false;
				currentRow = 0.0f;
			}
		}
	}

	public void PlayFX ()
	{
		//AnimateSprite = true;
	}
}
