﻿using UnityEngine;
using System.Collections;

public class BattleParticleListener : MonoBehaviour {
	Animator _Animator;
	BattleParticleTrigger _ParticleTrigger;

	void Start()
	{
		_Animator = GetComponent<Animator> ();
		_ParticleTrigger = GetComponentInParent<BattleParticleTrigger> ();
		if (_ParticleTrigger == null)
			Debug.LogError ("Particle Trigger Missing");
		else 
		{
			_ParticleTrigger.OnStartNormalAttack += PlayNormalParticle;
			_ParticleTrigger.OnStartComboAttack += PlayComboParticle;
		}
	}

	void OnDestroy()
	{
		if (_ParticleTrigger != null) {
			_ParticleTrigger.OnStartNormalAttack -= PlayNormalParticle;
			_ParticleTrigger.OnStartComboAttack -= PlayComboParticle;
		}
	}

	void PlayNormalParticle()
	{
		_Animator.CrossFade ("FX_Finn_Anim_Single", 0f);
	}

	void PlayComboParticle()
	{
		_Animator.CrossFade ("FX_Finn_Anim_Combo", 0f);
	}
}
