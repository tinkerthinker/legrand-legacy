﻿using UnityEngine;
using System.Collections;

public class DestroyAPSParticle : MonoBehaviour {
	public float TimeDestroy = 3.5f;
	// Use this for initialization
	void Start () {
		StartCoroutine(DestroyAPS());
	}
	
	IEnumerator DestroyAPS(){
		yield return new WaitForSeconds (TimeDestroy);
		this.gameObject.DestroyBattleAPS ();
	}
}
