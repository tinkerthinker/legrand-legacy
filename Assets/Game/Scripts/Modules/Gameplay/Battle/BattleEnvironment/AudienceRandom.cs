﻿using UnityEngine;
using System.Collections;

public class AudienceRandom : MonoBehaviour {
    public Sprite[] SpriteStartCandidate;
    public string[] AnimationCandidate;

    private Animator anim;

    private int _SelectedAnim;
    private SpriteRenderer _Renderer;
    private Animator _Animator;

    void Awake()
    {
        _SelectedAnim = LegrandUtility.Random(0, SpriteStartCandidate.Length);
        _Renderer = GetComponent<SpriteRenderer>();
        _Animator = GetComponent<Animator>();
        _Renderer.flipX = LegrandUtility.Random(0, 2) == 0 ? false: true ;
    }
    // Use this for initialization
    void Start ()
    {
        SetAudience();
    }

    void OnEnable()
    {
        SetAudience();
    }

    void SetAudience()
    {
        _Renderer.sprite = SpriteStartCandidate[_SelectedAnim];
        _Animator.Play(AnimationCandidate[_SelectedAnim], -1, LegrandUtility.Random(0.0f, 1.0f));
    }
}
