﻿using UnityEngine;
using System.Collections;

public class FXController : MonoBehaviour {
	public delegate void OnEvent(FXController fxController);
	public event OnEvent onEvent;

	public Animator anim;

	// Use this for initialization
	void Awake () {
		anim = GetComponent<Animator> ();
	}
	public void sendEvent(){
		if (onEvent != null)
			onEvent (this);
	}
	public void playFx(){
		anim.SetTrigger ("PlayFx");
	}	
	// Update is called once per frame
	void Update () {
	
	}
}
