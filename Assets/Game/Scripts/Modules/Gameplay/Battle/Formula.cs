﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;
using System;

public class Formula
{
    #region Execute Formula
    private static float ExecuteFormula(string formula)
    {
        return 1000;
//        return float.Parse(FormulaEval.EvaluateFormula(formula).ToString());
    }
    #endregion

    #region formula parameter
    public static string AddDeclaration(string formula, List<string> name, List<string> value)
    {
        string dec = "";
        for (int f = 0; f < name.Count; f++)
        {
            string val = value[f];
            if (val.Equals("False") || val.Equals("True"))
                val = val.ToLower();
            float Dum = 0f;
            if (float.TryParse(val, out Dum))
            {
                if (!val.Contains("."))
                {
                    val = val + ".0";
                }
            }
            else if (!val.Equals("true") && !val.Equals("false"))
            {
                val = "'" + val + "'";
            }

            dec = dec + "\nvar " + name[f] + " = " + val + ";";
        }
        return dec + "\n" + formula;
    }
    private static float FormulaControl(string formulaName, List<string> parameter, string CharName)
    {
        return 0f;
//        string formula = "";
//        List<string> name = new List<string>();
//        //langsung dari database, biar bisa edit dan langsung tes pas play
//#if UNITY_EDITOR
//        for (int f = 0; f < LegrandBackend.Instance.LegrandDatabase.formulaData.Count; f++)
//        {
//            if (LegrandBackend.Instance.LegrandDatabase.formulaData.Get(f).Name.Equals(formulaName))
//            {
//                formula = LegrandBackend.Instance.LegrandDatabase.formulaData.Get(f).Formula;
//                foreach (var par in LegrandBackend.Instance.LegrandDatabase.formulaData.Get(f).ParameterVariable)
//                {
//                    name.Add(par.Name);
//                }
//                break;
//            }
//        }
//#else
//        //dari backend, ambil dari dictionary biar cepet
//        LegrandBackend.Instance.GetFormula(formulaName,out formula,out name);
//#endif
//        formula = AddDeclaration(formula, name, parameter);
//        float result = ExecuteFormula(formula);
//#if UNITY_EDITOR
//        //        Debug.Log("Formula Name : " + formulaName + " ,From : {" + CharName + "}\n" + formula + "\n Result : " + result);
//#endif
//        return result;
    }
    #endregion

    #region Constant Percentage Value
    public static float CriticalDamage
    {
        get
        {
            return LegrandBackend.Instance.GetConstantValueFormula("CriticalDamage");
        }
    }
    public static float BadDamage
    {
        get
        {
            return LegrandBackend.Instance.GetConstantValueFormula("BadDamage");
        }
    }
    public static float GoodDamage
    {
        get
        {
            return LegrandBackend.Instance.GetConstantValueFormula("GoodDamage");
        }
    }
    public static float PerfectDamage
    {
        get
        {
            return LegrandBackend.Instance.GetConstantValueFormula("PerfectDamage");
        }
    }
    public static float DefReduceDamage
    {
        get
        {
            return LegrandBackend.Instance.GetConstantValueFormula("DefReduceDamage");
        }
    }

    public static float BadHitRate
    {
        get
        {
            return LegrandBackend.Instance.GetConstantValueFormula("BadHitRate");
        }
    }

    public static float GoodHitRate
    {
        get
        {
            return LegrandBackend.Instance.GetConstantValueFormula("GoodHitRate");
        }
    }

    public static float GoodEvaRate
    {
        get
        {
            return LegrandBackend.Instance.GetConstantValueFormula("GoodEvaRate");
        }
    }
    public static float PerfectEvaRate
    {
        get
        {
            return LegrandBackend.Instance.GetConstantValueFormula("PerfectEvaRate");
        }
    }

    public static float TimedDefPerfectDamage
    {
        get
        {
            return LegrandBackend.Instance.GetConstantValueFormula("TimedDefPerfectDamage");
        }
    }
    public static float TimedDefMissDamage
    {
        get
        {
            return LegrandBackend.Instance.GetConstantValueFormula("TimedDefMissDamage");
        }
    }

    private static float WeaponStrong
    {
        get
        {
            return LegrandBackend.Instance.GetConstantValueFormula("WeaponStrong");
        }
    }

    private static float WeaponWeak
    {
        get
        {
            return LegrandBackend.Instance.GetConstantValueFormula("WeaponWeak");
        }
    }

    private static float ElementStrong
    {
        get
        {
            return LegrandBackend.Instance.GetConstantValueFormula("ElementStrong");
        }
    }

    private static float ElementWeak
    {
        get
        {
            return LegrandBackend.Instance.GetConstantValueFormula("ElementWeak");
        }
    }
    private static float GuardBonus
    {
        get
        {
            return LegrandBackend.Instance.GetConstantValueFormula("GuardBonus");
        }
    }
    
    private static float FlyingAccBonus
    {
        get
        {
            return LegrandBackend.Instance.GetConstantValueFormula("FlyingAccBonus");
        }
    }
    private static float FlyingAccPenalty
    {
        get
        {
            return LegrandBackend.Instance.GetConstantValueFormula("FlyingAccPenalty");
        }
    }

    private static float AntiPhysicalPenalty
    {
        get
        {
            return LegrandBackend.Instance.GetConstantValueFormula("AntiPhysicalPenalty");
        }
    }

    private static float AntiMagicPenalty
    {
        get
        {
            return LegrandBackend.Instance.GetConstantValueFormula("AntiMagicPenalty");
        }
    }

    private const float _EulerNumber = 2.7182818284f;
    #endregion

    #region Status
    public static int CalculateHealth(Character c)
    {
        return CalculateHealth(c,(float)c.GetAttribute(Legrand.core.Attribute.VIT));
    }

    public static int CalculateHealth(Character c,float vit)
    {
        float result = 0f;
        if (vit <= 50f) result = vit * 65f;
        else result = 19998f / (1 + Mathf.Pow(_EulerNumber, (-0.0337f * (vit - 99))));
        return Mathf.RoundToInt(result * c.GetGrowth(Legrand.core.Attribute.Health));
    }
    #endregion

    #region Physical

    public static float CalculateTotalDamage(Character charBattler)
    {
        return CalculateTotalDamage(charBattler, null, null);
    }

    public static float CalculateTotalDamage(Character charBattler, float str, float agi)
    {
        return CalculateTotalDamage(charBattler, str, agi, null, null);
    }

    public static float CalculateTotalDamage(Character charBattler, Dictionary<Legrand.core.Attribute, float> primaryBuff, Dictionary<Legrand.core.Attribute, float> secondaryBuff)
    {
        float str = (float)charBattler.GetAttribute(Legrand.core.Attribute.STR);
        float agi = (float)charBattler.GetAttribute(Legrand.core.Attribute.AGI);
        return CalculateTotalDamage(charBattler, str, agi, primaryBuff, secondaryBuff);
    }

    public static float CalculateTotalDamage(Character charBattler, float str, float agi, Dictionary<Legrand.core.Attribute, float> primaryBuff, Dictionary<Legrand.core.Attribute, float> secondaryBuff)
    {
        float strDamage = CalculateBaseDamage(str, primaryBuff);
        float agiDamage = CalculateAgiDamage(agi, primaryBuff);
        float totalDamage = strDamage + agiDamage;
        totalDamage = totalDamage * charBattler.GetGrowth(Legrand.core.Attribute.PhysicalAttack);
        if (charBattler.Equipment() != null)
            totalDamage += charBattler.Equipment().GetEquipmentAttribute(Legrand.core.Attribute.PhysicalAttack);

        totalDamage = CalculateTotalDamage(totalDamage, secondaryBuff);

        return Mathf.Round(totalDamage);
    }

    public static float CalculateTotalDamage(float nonBuffedDamage,Dictionary<Legrand.core.Attribute, float> secondaryBuff)
    {
        float finalDamage = nonBuffedDamage;
        if (secondaryBuff != null && secondaryBuff.ContainsKey(Legrand.core.Attribute.PhysicalAttack)) finalDamage = Mathf.Round(finalDamage + (finalDamage * secondaryBuff[Legrand.core.Attribute.PhysicalAttack] / 100f));

        return Mathf.Round(finalDamage);
    }

    public static float CalculateBaseDamage(float str, Dictionary<Legrand.core.Attribute, float> primaryBuff)
    {
        if (primaryBuff != null && primaryBuff.ContainsKey(Legrand.core.Attribute.STR)) str = Mathf.Round(str + (str * primaryBuff[Legrand.core.Attribute.STR] / 100f));

        float baseDamage = 0f;
        if (str <= 50f) baseDamage = str * 10f;
        else baseDamage = 3000 / (1 + Mathf.Pow(_EulerNumber, -0.0325f * (str - 99)));

        return baseDamage;
    }

    public static float CalculateAgiDamage(Character charBattler, Dictionary<Legrand.core.Attribute, float> primaryBuff)
    {
        return CalculateAgiDamage((float)charBattler.GetAttribute(Legrand.core.Attribute.AGI), primaryBuff);
    }

    public static float CalculateAgiDamage(float agi, Dictionary<Legrand.core.Attribute, float> primaryBuff)
    {
        if (primaryBuff != null && primaryBuff.ContainsKey(Legrand.core.Attribute.AGI)) agi = Mathf.Round(agi + (agi * primaryBuff[Legrand.core.Attribute.AGI] / 100f));

        float agiDamage = 0f;

        if (agi <= 50f)
        {
            int n = (((int)agi) + 10) / 10;
            float Sn = (n / 2f) * ((n - 1) * 10f);
            float modifier = (float)(((int)agi % 10) * ((((int)agi) / 10) + 1));
            agiDamage = Sn + modifier;
        }
        else if (agi > 50f && agi <= 100f)
        {
            int n = (((int)agi) - 50) / 10;
            float Sn = (n / 2f) * (100f + ((n - 1) * -10f));
            float modifier = (float)((((int)agi) % 10) * (5 - (((int)agi - 50) / 10)));
            agiDamage = 150f + Sn + modifier;
        }
        else
        {
            agiDamage = 300f + (agi - 100f);
        }

        return agiDamage;
    }

    public static float CalculatePhysicalDefense(Character charBattler)
    {
        return CalculatePhysicalDefense(charBattler, null, null);
    }

    public static float CalculatePhysicalDefense(Character charBattler, float vit)
    {
        return CalculatePhysicalDefense(charBattler, vit, null, null);
    }

    public static float CalculatePhysicalDefense(Character charBattler, Dictionary<Legrand.core.Attribute, float> primaryBuff, Dictionary<Legrand.core.Attribute, float> secondaryBuff)
    {
        float vit = (float)charBattler.GetAttribute(Legrand.core.Attribute.VIT);
        return CalculatePhysicalDefense(charBattler, vit, primaryBuff, secondaryBuff);
    }

    public static float CalculatePhysicalDefense(Character charBattler, float vit, Dictionary<Legrand.core.Attribute, float> primaryBuff, Dictionary<Legrand.core.Attribute, float> secondaryBuff)
    {
        float baseDefense = CalculatePhysicalDefense(vit, primaryBuff) * charBattler.GetGrowth(Legrand.core.Attribute.PhysicalDefense);

        if (charBattler.Equipment() != null)
            baseDefense = baseDefense + charBattler.Equipment().GetEquipmentAttribute(Legrand.core.Attribute.PhysicalDefense);

        baseDefense = BuffPhysicalDefense(baseDefense, secondaryBuff);
       
        return Mathf.Round(baseDefense);
    }

    public static float BuffPhysicalDefense(float nonBuffedDefense, Dictionary<Legrand.core.Attribute, float> secondaryBuff)
    {
        float finalDefense = nonBuffedDefense;
        if (secondaryBuff != null && secondaryBuff.ContainsKey(Legrand.core.Attribute.PhysicalAttack)) finalDefense = Mathf.Round(finalDefense + (finalDefense * secondaryBuff[Legrand.core.Attribute.PhysicalDefense] / 100f));

        return Mathf.Round(finalDefense);
    }

    public static float CalculatePhysicalDefense(float vit, Dictionary<Legrand.core.Attribute, float> primaryBuff)
    {
        if (primaryBuff != null && primaryBuff.ContainsKey(Legrand.core.Attribute.VIT)) vit = Mathf.Round(vit + (vit * primaryBuff[Legrand.core.Attribute.VIT] / 100f));
        float result = (0.046f * Mathf.Pow(vit, 2)) + (vit);

        return Mathf.Round(result);
    }

    public static float GetPhyDmg(Character att, Character def, Dictionary<Legrand.core.Attribute, float> attPrimaryBuff, Dictionary<Legrand.core.Attribute, float> attSecondaryBuff, Dictionary<Legrand.core.Attribute, float> defPrimaryBuff, Dictionary<Legrand.core.Attribute, float> defSecondaryBuff)
    {
        float totalDamage = CalculateTotalDamage(att, attPrimaryBuff, attSecondaryBuff);
        float defenderBaseDefense = CalculatePhysicalDefense(def, defPrimaryBuff, defSecondaryBuff);

        return GetPhyDmg(totalDamage, defenderBaseDefense);
    }

    public static float GetPhyDmg(float pDamage, Dictionary<Legrand.core.Attribute, float> attSecondaryBuff, float pDefense, Dictionary<Legrand.core.Attribute, float> defSecondaryBuff)
    {
        float totalDamage = pDamage;
        if (attSecondaryBuff != null && attSecondaryBuff.ContainsKey(Legrand.core.Attribute.PhysicalDefense)) totalDamage = Mathf.Round(totalDamage + (totalDamage * attSecondaryBuff[Legrand.core.Attribute.PhysicalAttack] / 100f));
        float defenderBaseDefense = pDefense;
        if (defSecondaryBuff != null && defSecondaryBuff.ContainsKey(Legrand.core.Attribute.PhysicalDefense)) defenderBaseDefense = Mathf.Round(defenderBaseDefense + (defenderBaseDefense * defSecondaryBuff[Legrand.core.Attribute.PhysicalDefense] / 100f));

        return GetPhyDmg(totalDamage, defenderBaseDefense);
    }

    public static float GetPhyDmg(float damage, float defense)
    {
        float RandomedtotalDamage = damage * LegrandUtility.Random(0.9125f, 1.125f);

        float result = -defense + RandomedtotalDamage;
        result = Mathf.Clamp(result, damage * 0.1f, float.MaxValue);

        return Mathf.Round(result);
    }

    public static float GetPhyDmg(Character att, Character def, Dictionary<Legrand.core.Attribute, float> attPrimaryBuff, Dictionary<Legrand.core.Attribute, float> attSecondaryBuff, Dictionary<Legrand.core.Attribute, float> defPrimaryBuff, Dictionary<Legrand.core.Attribute, float> defSecondaryBuff, out string log)
    {
        log = "";
        return GetPhyDmg(att, def, attPrimaryBuff, attSecondaryBuff, defPrimaryBuff, defSecondaryBuff);
    }
    #endregion

    #region Magic
    public static float CalculateINTMagicDamage(Character charBattler)
    {
        return CalculateINTMagicDamage(charBattler, (float)charBattler.GetAttribute(Legrand.core.Attribute.INT));
    }

    public static float CalculateINTMagicDamage(Character charBattler, float intel)
    {
        return CalculateINTMagicDamage(charBattler, intel, null, null);
    }

    public static float CalculateINTMagicDamage(Character charBattler, Dictionary<Legrand.core.Attribute, float> primaryBuff, Dictionary<Legrand.core.Attribute, float> secondaryBuff)
    {
        return CalculateINTMagicDamage(charBattler, (float)charBattler.GetAttribute(Legrand.core.Attribute.INT), primaryBuff, secondaryBuff);
    }

    public static float CalculateINTMagicDamage(Character charBattler, float intel, Dictionary<Legrand.core.Attribute, float> primaryBuff, Dictionary<Legrand.core.Attribute, float> secondaryBuff)
    {
        float baseDamage = INTMagicDamage(intel, primaryBuff) * charBattler.GetGrowth(Legrand.core.Attribute.MagicAttack);

        if (charBattler.Equipment() != null)
            baseDamage += charBattler.Equipment().GetEquipmentAttribute(Legrand.core.Attribute.MagicAttack);

        if (secondaryBuff != null && secondaryBuff.ContainsKey(Legrand.core.Attribute.MagicAttack)) baseDamage = Mathf.Round(baseDamage + (baseDamage * secondaryBuff[Legrand.core.Attribute.MagicAttack] / 100f));

        return Mathf.Round(baseDamage);
    }

    public static float INTMagicDamage(float intel, Dictionary<Legrand.core.Attribute, float> primaryBuff)
    {
        if (primaryBuff != null && primaryBuff.ContainsKey(Legrand.core.Attribute.INT)) intel = Mathf.Round(intel + (intel * primaryBuff[Legrand.core.Attribute.INT] / 100f));

        float magicDamage = 0f;
        if (intel <= 50f) magicDamage = intel * 12f;
        else magicDamage = 3600f / (1 + Mathf.Pow(_EulerNumber, -0.0325f * (intel - 99)));

        return magicDamage;
    }

    public static float CalculateMagicDefense(Character charBattler)
    {
        return CalculateMagicDefense(charBattler, null, null);
    }

    public static float CalculateMagicDefense(Character charBattler, float intel)
    {
        return CalculateMagicDefense(charBattler, intel, null, null);
    }

    public static float CalculateMagicDefense(Character charBattler, Dictionary<Legrand.core.Attribute, float> primaryBuff, Dictionary<Legrand.core.Attribute, float> secondaryBuff)
    {
        return CalculateMagicDefense(charBattler, (float)charBattler.GetAttribute(Legrand.core.Attribute.INT), primaryBuff, secondaryBuff);
    }

    public static float CalculateMagicDefense(Character charBattler, float intel, Dictionary<Legrand.core.Attribute, float> primaryBuff, Dictionary<Legrand.core.Attribute, float> secondaryBuff)
    {
        float magicDefense = CalculateMagicDefense(intel, primaryBuff) * charBattler.GetGrowth(Legrand.core.Attribute.MagicDefense);

        if (charBattler.Equipment() != null)
            magicDefense += charBattler.Equipment().GetEquipmentAttribute(Legrand.core.Attribute.MagicDefense);

        magicDefense = BuffMagicDefense(magicDefense, secondaryBuff);
        return Mathf.Round(magicDefense);
    }

    public static float CalculateMagicDefense(float intel, Dictionary<Legrand.core.Attribute, float> primaryBuff)
    {
        if (primaryBuff != null && primaryBuff.ContainsKey(Legrand.core.Attribute.INT)) intel = Mathf.Round(intel + (intel * primaryBuff[Legrand.core.Attribute.INT] / 100f));

        float magicDefense = 0f;

        if (intel <= 90f)
        {
            int n = ((int)intel) / 10;
            float Sn = (n / 2f) * (40 + ((n - 1) * 10f));
            float modifier = (float)(((int)intel % 10) * ((((int)intel) / 10) + 2));
            magicDefense = Sn + modifier;
        }
        else if (intel > 91f && intel <= 100f)
            magicDefense = ((intel - 90f) * 1.1f) + 540f;
        else
            magicDefense = 550f + (intel - 99f) * 5f;

        return magicDefense;
    }

    public static float BuffMagicDefense(float baseDefense, Dictionary<Legrand.core.Attribute, float> secondaryBuff)
    {
        float finalDefense = baseDefense;

        if (secondaryBuff != null && secondaryBuff.ContainsKey(Legrand.core.Attribute.MagicDefense)) finalDefense = Mathf.Round(finalDefense + (finalDefense * secondaryBuff[Legrand.core.Attribute.MagicDefense] / 100f));

        return Mathf.Round(finalDefense);
    }

    public static float GetMagAttack(float attMagDamage, Dictionary<Legrand.core.Attribute, float> attSecondaryBuff)
    {
        float magDamage = attMagDamage;
        if (attSecondaryBuff != null && attSecondaryBuff.ContainsKey(Legrand.core.Attribute.MagicAttack))
            magDamage = Mathf.Round(magDamage + (magDamage * attSecondaryBuff[Legrand.core.Attribute.MagicAttack] / 100f));

        return magDamage;
    }

    public static float GetmagDef(float defMagDefense, Dictionary<Legrand.core.Attribute, float> defSecondaryBuff)
    {
        float magDefense = defMagDefense;
        if (defSecondaryBuff != null && defSecondaryBuff.ContainsKey(Legrand.core.Attribute.MagicDefense))
            magDefense = Mathf.Round(magDefense + (magDefense * defSecondaryBuff[Legrand.core.Attribute.MagicDefense] / 100f));

        return magDefense;
    }

    public static float GetMagDmg(float attMagDamage, Dictionary<Legrand.core.Attribute, float> attSecondaryBuff, float defMagDefense, Dictionary<Legrand.core.Attribute, float> defSecondaryBuff, Magic magic)
    {
        float magDamage = GetMagAttack(attMagDamage, attSecondaryBuff);
        
        float magDefense = GetmagDef(defMagDefense, defSecondaryBuff);
        
        return GetMagDmg(magDamage, magDefense, magic);
    }
    public static float GetMagDmg(float magDamage, float magDefense, Magic magic)
    {
        float magicPower = magic.MagicDamageModifier;
        float r1 = (magDamage * magicPower / 100f) - (magDefense / magDamage);
        float rnd = LegrandUtility.Random(32f, 34f);
        float r2 = magDamage + magDefense;
        float r3 = Mathf.Pow(rnd, 2f);
        float bonusDamage = (r1 * magDamage * magicPower) / (r2 * r3);
        return Mathf.Round(magicPower + bonusDamage);
    }
    public static float GetCostDmg(Character character, int magCostMod, TargetType target, Dictionary<Legrand.core.Attribute, float> primaryBuff, Dictionary<Legrand.core.Attribute, float> secondaryBuff)
    {
        return 0f;
        
    }

    //khusus magic interrupt
    /// <returns>The interrupt modifier / magic delay modifier for magic preset in percentage.</returns>
    public static float CalculateInterruptModifier(Battler attacker, Battler defender, float interruptChance)
    {
        float ChanceInterrupt = 0f;
        float timedHitBonus = LegrandBackend.Instance.GetConstantValueFormula("TimedHitGoodInterrupt");

        float r1 = (attacker.GetAttribute(Legrand.core.Attribute.LUCK) - defender.GetAttribute(Legrand.core.Attribute.LUCK)) * (interruptChance/ 5f);
        float r2 = (GetAccRate(attacker) / (GetEvaRate(defender) * 10f));
        float rnd = LegrandUtility.Random(-10f, 10f);

        if (defender.TimedHit != null) //player lagi defend nich
        {
            if (defender.TimedHit.DefendHitState == TimedHitState.Perfect)
                timedHitBonus = LegrandBackend.Instance.GetConstantValueFormula("TimedHitPerfectInterrupt");
            else if (defender.TimedHit.DefendHitState == TimedHitState.Miss)
                timedHitBonus = LegrandBackend.Instance.GetConstantValueFormula("TimedHitPoorInterrupt");
        }
        else if (attacker.TimedHit != null)//encounter lagi defend nich
        {
            if (attacker.TimedHit.AttackHitState == TimedHitState.Perfect) //kalau player timehitnya sukses, berarti chance interruptnya gede n masuk perhitungan timehitpoornya lawan
                timedHitBonus = LegrandBackend.Instance.GetConstantValueFormula("TimedHitPoorInterrupt");
            else if (attacker.TimedHit.AttackHitState == TimedHitState.Miss)
                timedHitBonus = LegrandBackend.Instance.GetConstantValueFormula("TimedHitPerfectInterrupt");
        }

        return (r1 * r2) - timedHitBonus + rnd; ;
    }
    #endregion

    #region Agility 

    public static float GetCritRate(Character character)
    {
        return GetCritRate(character, null, null);
    }

    public static float GetCritRate(Character character, float agi)
    {
        return GetCritRate(character, agi, null, null);
    }

    public static float GetCritRate(Character att, Dictionary<Legrand.core.Attribute, float> primaryBuff, Dictionary<Legrand.core.Attribute, float> secondaryBuff)
    {
        float agi = (float)att.GetAttribute(Legrand.core.Attribute.AGI);                                      //TotalPC.AGI = AGI
        return GetCritRate(att, agi, primaryBuff, secondaryBuff);
    }

    public static float GetCritRate(Character att, float agi, Dictionary<Legrand.core.Attribute, float> primaryBuff, Dictionary<Legrand.core.Attribute, float> secondaryBuff)
    {
        float result = GetCritRate(agi, primaryBuff) * att.GetGrowth(Legrand.core.Attribute.CriticalRate);

        if (att.Equipment() != null)
            result = result + att.Equipment().GetEquipmentAttribute(Legrand.core.Attribute.CriticalRate);

        if (secondaryBuff != null && secondaryBuff.ContainsKey(Legrand.core.Attribute.CriticalRate))
            result = result + secondaryBuff[Legrand.core.Attribute.CriticalRate];                        // result + %skillbuff

        return result;
    }

    public static float GetCritRate(float agi, Dictionary<Legrand.core.Attribute, float> primaryBuff)
    {                                   //TotalPC.AGI = AGI
        if (primaryBuff != null && primaryBuff.ContainsKey(Legrand.core.Attribute.AGI))
            agi = Mathf.RoundToInt(agi + (agi * primaryBuff[Legrand.core.Attribute.AGI] / 100f));    //TotalPC.AGI = (AGI + PC.ArmorType) + SkillBuff

        float result = (0.15f * agi) + 1.15f;	//	result = (TotalPC.AGI/2) + 5 in %

        return result;
    }

    public static float GetTotalCritRate(float critRate, Dictionary<Legrand.core.Attribute, float> secondaryBuff)
    {
        float result = critRate;
        if (secondaryBuff != null && secondaryBuff.ContainsKey(Legrand.core.Attribute.CriticalRate))
            result = result + secondaryBuff[Legrand.core.Attribute.CriticalRate];

        return result;
    }

    public static float GetAccRate(Character character)
    {
        return GetAccRate(character, null, null);
    }

    public static float GetAccRate(Battler character)
    {
        float acc = character.BattlerAttribute[Legrand.core.Attribute.HitRate];
        if (character.SecondaryBuff != null && character.SecondaryBuff.ContainsKey(Legrand.core.Attribute.HitRate))
            acc = acc + character.SecondaryBuff[Legrand.core.Attribute.HitRate];

        return acc;
    }

    public static float GetAccRate(Character character, float agi)
    {
        return GetAccRate(character, agi, null, null);
    }

    public static float GetAccRate(Character character, Dictionary<Legrand.core.Attribute, float> primaryBuff, Dictionary<Legrand.core.Attribute, float> secondaryBuff)
    {
        float agi = character.GetAttribute(Legrand.core.Attribute.AGI);
        return GetAccRate(character, agi, primaryBuff, secondaryBuff);
    }


    public static float GetAccRate(Character character, float agi, Dictionary<Legrand.core.Attribute, float> primaryBuff, Dictionary<Legrand.core.Attribute, float> secondaryBuff)
    {
        float result = GetAccRate(agi, primaryBuff) * character.GetGrowth(Legrand.core.Attribute.HitRate);
        if (character.Equipment() != null)
            result = result + character.Equipment().GetEquipmentAttribute(Legrand.core.Attribute.HitRate);

        if (secondaryBuff != null && secondaryBuff.ContainsKey(Legrand.core.Attribute.HitRate))
            result = result + secondaryBuff[Legrand.core.Attribute.HitRate];                      // result + buff %accurary

        return result;
    }

    public static float GetAccRate(float agi, Dictionary<Legrand.core.Attribute, float> primaryBuff)
    {
        if (primaryBuff != null && primaryBuff.ContainsKey(Legrand.core.Attribute.AGI))
            agi = Mathf.RoundToInt(agi + (agi * primaryBuff[Legrand.core.Attribute.AGI] / 100f));

        float result = (0.4f * agi) + 85.4f;

        return result;
    }

    public static float GetEvaRate(Battler character)
    {
        float eva = character.BattlerAttribute[Legrand.core.Attribute.EvasionRate];
        if (character.SecondaryBuff != null && character.SecondaryBuff.ContainsKey(Legrand.core.Attribute.EvasionRate))
            eva = eva + character.SecondaryBuff[Legrand.core.Attribute.EvasionRate];

        return eva;
    }

    public static float GetEvaRate(Character character)
    {
        return GetEvaRate(character, null, null);
    }

    public static float GetEvaRate(Character character, float agi)
    {
        return GetEvaRate(character, agi, null, null);
    }

    public static float GetEvaRate(Character character, Dictionary<Legrand.core.Attribute, float> primaryBuff, Dictionary<Legrand.core.Attribute, float> secondaryBuff)
    {
        float agi = (float)character.GetAttribute(Legrand.core.Attribute.AGI);
        return GetEvaRate(character, agi, primaryBuff, secondaryBuff);
    }

    public static float GetEvaRate(Character character, float agi, Dictionary<Legrand.core.Attribute, float> primaryBuff, Dictionary<Legrand.core.Attribute, float> secondaryBuff)
    {
        float result = GetEvaRate(agi, primaryBuff) * character.GetGrowth(Legrand.core.Attribute.EvasionRate);

        if (character.Equipment() != null)
            result += character.Equipment().GetEquipmentAttribute(Legrand.core.Attribute.EvasionRate);

        if (secondaryBuff != null && secondaryBuff.ContainsKey(Legrand.core.Attribute.EvasionRate))
            result = result + secondaryBuff[Legrand.core.Attribute.EvasionRate];                      // result + buff %evasion

        return result;
    }

    public static float GetEvaRate(float agi, Dictionary<Legrand.core.Attribute, float> primaryBuff)
    {
        if (primaryBuff != null && primaryBuff.ContainsKey(Legrand.core.Attribute.AGI))
            agi = Mathf.RoundToInt(agi + (agi * primaryBuff[Legrand.core.Attribute.AGI] / 100f));

        float result = (0.3f * agi) + 0.3f;

        return result;
    }

    public static float CalculateAntiMagicPenalty(float baseDamage)
    {
        return Mathf.Round(baseDamage + (baseDamage * (AntiMagicPenalty / 100f)));
    }

    public static float CalculateArmoredPenalty(float baseDamage)
    {
        return Mathf.Round(baseDamage + (baseDamage * (AntiPhysicalPenalty/100f)));
    }
    
    public static float GetHitRate(Character att, Character def, float bonusHitRate, float bonusEva, float attAcc, Dictionary<Legrand.core.Attribute, float> attSecondaryBuff, float defEva, Dictionary<Legrand.core.Attribute, float> defSecondaryBuff)
    {
        float finalAcc = attAcc;
        if (attSecondaryBuff != null && attSecondaryBuff.ContainsKey(Legrand.core.Attribute.HitRate))
            finalAcc = finalAcc + attSecondaryBuff[Legrand.core.Attribute.HitRate];

        float finalEva = defEva;
        if (defSecondaryBuff != null && defSecondaryBuff.ContainsKey(Legrand.core.Attribute.EvasionRate))
            finalEva = finalEva + defSecondaryBuff[Legrand.core.Attribute.EvasionRate];

        return GetHitRate(att, def, finalAcc, finalEva, bonusHitRate, bonusEva);
    }
    public static float GetHitRate(Character att, Character def, float acc, float eva, float bonusHitRate, float bonusEva)
    {
        float result = acc + bonusHitRate - eva - bonusEva;  //A.Accuracy - D.Evasion

        // Assumption player never flying
        float flyingModifier = 0f;
        if (def._Type == (int)Character.CharacterType.Encounter && (def as Encounter).IsFlying)
        {
            CharacterAttackType attackType = CharacterAttackType.Caster;    // For no bonus;
            if (att._Type == (int)Character.CharacterType.Main)
                attackType = (att as MainCharacter).CharacterAttackType;
            else if (att._Type == (int)Character.CharacterType.Encounter)
                attackType = (att as Encounter)._EncounterClass;

            if (attackType == CharacterAttackType.Melee)
                flyingModifier = FlyingAccPenalty;
            else if (attackType == CharacterAttackType.Range)
                flyingModifier = FlyingAccBonus;
        }
        result = result + flyingModifier;       // + Flying Modifier

        return result;
    }
    #endregion

    #region Magic Hit Rate
    public static float GetMagicHitRate(float magicAcc, float bonusHitRate, float bonusEva, float attAcc, Dictionary<Legrand.core.Attribute, float> attSecondaryBuff, float defEva, Dictionary<Legrand.core.Attribute, float> defSecondaryBuff)
    {
        float finalAcc = attAcc;
        if (attSecondaryBuff != null && attSecondaryBuff.ContainsKey(Legrand.core.Attribute.HitRate))
            finalAcc = finalAcc + attSecondaryBuff[Legrand.core.Attribute.HitRate];

        float finalEva = defEva;
        if (defSecondaryBuff != null && defSecondaryBuff.ContainsKey(Legrand.core.Attribute.EvasionRate))
            finalEva = finalEva + defSecondaryBuff[Legrand.core.Attribute.EvasionRate];

        return GetMagicHitRate(magicAcc, finalAcc, finalEva, bonusHitRate, bonusEva);
    }

    public static float GetMagicHitRate(float magicAcc, float acc, float eva, float bonusHitRate, float bonusEva)
    {
        float result = magicAcc + (acc / magicAcc) + bonusHitRate - eva - bonusEva;
        return result;
    }

    #endregion
    #region Misc Number
    public static float GetCriticalDmg(float baseDamage, Dictionary<Legrand.core.Attribute, float> secondaryBuff)
    {
        float finalCritDamagePercentage = CriticalDamage;
        // Check if battler has critical damage buff
        if (secondaryBuff != null && secondaryBuff.ContainsKey(Legrand.core.Attribute.CriticalDamage))
            finalCritDamagePercentage = finalCritDamagePercentage + secondaryBuff[Legrand.core.Attribute.CriticalDamage]; // +skillbuff%

        float result = (baseDamage * finalCritDamagePercentage / 100f);

        if (result < 0f)
            result = 0f;

        return result;
    }

    public static float GetStatusResistance(Character character, Dictionary<Legrand.core.Attribute, float> primaryBuff, Dictionary<Legrand.core.Attribute, float> secondaryBuff)
    {
        int intel = character.GetAttribute(Legrand.core.Attribute.INT);
		if (primaryBuff != null && primaryBuff.ContainsKey (Legrand.core.Attribute.INT))
			intel = Mathf.RoundToInt(intel + (intel * primaryBuff [Legrand.core.Attribute.INT] /100f));
		float result = (5f + (intel / 2f) );
		if(secondaryBuff != null && secondaryBuff.ContainsKey(Legrand.core.Attribute.StatusResistance)) 
			result = result + secondaryBuff[Legrand.core.Attribute.StatusResistance];
		return result;
    }

    public static int WeaponTriangle(Character att, Character def)
    {
        int strong = (int)att.Weapon().StrongerTo;
        int weak = (int)att.Weapon().WeakerTo;
        int defType = (int)def.Weapon().Type;

        if (strong == defType)
            return 1;
        else if (weak == defType)
            return -1;

        return 0;
    }

    public static float WeaponTriangle(Character att, Character def, float baseDamage)
    {
        float multiplier = 100f;
        // Sorry for my bad design
        int power = WeaponTriangle(att,def);

        if (power == 1)
            multiplier = WeaponStrong;
        else if (power == -1)
            multiplier = WeaponWeak;

        return baseDamage * multiplier / 100f;
    }

    public static int CompareElement(Element magicElement, Character def)
    {
        // magicElement power to def element
        // 1 magicElement is stronger
        // -1 magicElement is weaker
        // 0 magicElement is draw

        if (magicElement == Element.None) return 0;

        Element defWeakness = Element.None;
        Element defStrength = Element.None;
        CharacterElement defElementPower = null;

        if (def._Type == (int)Character.CharacterType.Main)
            defElementPower = (def as MainCharacter).CharacterElements;
        else if (def._Type == (int)Character.CharacterType.Encounter)
            defElementPower = (def as Encounter).EncounterElements;

        defWeakness = defElementPower.Weakness;
        defStrength = defElementPower.Stronger;

        if (defWeakness == magicElement)
            return 1;
        else if (defStrength == magicElement)
            return -1;

        return 0;
    }

    public static float Elements(Element magicElement, Character def, float baseDamage)
    {
        float multiplier = 100f;
        int ElementPower = CompareElement(magicElement, def);

        if (ElementPower == 1)
            multiplier = ElementStrong;
        else if (ElementPower == -1)
            multiplier = ElementWeak;

        return baseDamage * multiplier / 100f;
    }

    public static float ExpBonus(int perfect, int good, int bad)
    {
        float bonusPercentage = 0;
        float total = perfect + good + bad;
        if (total <= 0) return 0;

        bonusPercentage = 50f / (total / ((perfect * 0.5f) + (good * 0.2f) - (bad * 0.1f)));
        if (bonusPercentage < 0) bonusPercentage = 0;
        return bonusPercentage/100f;
    }

    public static float ExpLevelDifferencesBonus(int playerLvl, int encounterLvl)
    {
        float differenceModifiers = 0.1f;
        int minLvlDifferences = 5;
        int lvlDifferences = encounterLvl - playerLvl;
        if (Mathf.Abs(lvlDifferences) > minLvlDifferences)
            return ((differenceModifiers * (Mathf.Abs(lvlDifferences) - 5)) * Mathf.Clamp(lvlDifferences, -1f, 1f));
        else return 0f;
    }
    #endregion

    #region Skill
    public static float OffenseSkillDamage(OffensiveBattleSkill skill, Battler attacker, Battler defender)
    {
        float AttackerPower = 0f;
        float DefenderPower = 0f;
        float skillPower = skill.DamagePercentage;
        
        // Check skill is physical or magic
        if (skill.DamageType == TypeDamage.Physical)
        {
            AttackerPower = CalculateTotalDamage(attacker.BattlerAttribute[Legrand.core.Attribute.PhysicalAttack], attacker.SecondaryBuff);
            DefenderPower = CalculatePhysicalDefense(defender.BattlerAttribute[Legrand.core.Attribute.PhysicalDefense], defender.SecondaryBuff);
        }
        else if (skill.DamageType == TypeDamage.Magic)
        {
            AttackerPower = GetMagAttack(attacker.BattlerAttribute[Legrand.core.Attribute.MagicAttack], attacker.SecondaryBuff);
            DefenderPower = BuffMagicDefense(defender.BattlerAttribute[Legrand.core.Attribute.MagicDefense], attacker.SecondaryBuff);
        }

        float r1 = skillPower * AttackerPower;
        float rand = LegrandUtility.Random(140f, 150f);
        float r2 = rand * DefenderPower;
        float r3 = (r1 / r2) * 100f;
        float bonus = r3 + rand;
        return skillPower + bonus;
    }
    #endregion

    #region Leveling

    public static int EXPforLevelUp(int level, int maximumLevel)
    {
        if (level >= 1 && level <= 10)
        {
            return level * 10;
        }
        else if (level == maximumLevel)
        {
            return -1;
        }

        float expPoint = Mathf.RoundToInt(level * 206.25f - 1962.5f);
        return Mathf.RoundToInt(expPoint);
    }

    public static int EXPforLevelUp(int level, int maximumLevel, float growth)
    {
        if (level == 1)
        {
            return 0;
        }
        else if (level == maximumLevel)
        {
            return -1;
        }

        float expPoint = (level / 2) * ((level - 1)) * (1 + ((level - 1) * 0.02f));
        return Mathf.RoundToInt(expPoint / growth);
    }

    public static int ExpCalculation(int monsterLvl)
    {
        if (monsterLvl == 1) return 1;

        return Mathf.CeilToInt(EXPforLevelUp(monsterLvl, int.MaxValue, 1f) * 0.48f / (1+(monsterLvl * 0.02f)));
    }

    public static int ATPPoint(int Level)
    {
        if (Level == 1) return 0;

        return 1;
    }
    #endregion

    public static float GetAttLevel(Character character)
    {
        float average = 0f;

        if (character._Type == (int)Character.CharacterType.Main)
            average = (character as MainCharacter).AverageAttribute;
        else if (character._Type == (int)Character.CharacterType.Encounter)
            average = (character as Encounter).AverageAttribute;
        return average;
    }

    public static int GetAveragePartyLevel(List<Battler> battlers)
    {
        int total = 0;
        int count = 0;

        foreach (Battler mainBattler in battlers)
        {
            if (mainBattler.Character._Type == (int)Character.CharacterType.Main)
            {
                MainCharacter main = mainBattler.Character as MainCharacter;
                total = total + main.Level;
            }
            else if (mainBattler.Character._Type == (int)Character.CharacterType.Encounter)
            {
                Encounter enc = mainBattler.Character as Encounter;
                total = total + enc.Level;
            }
            count++;
        }
        return Mathf.RoundToInt(total / count);
    }

    public static float GetPartyLootLuck(Battler[] battlers)
    {
        float total = 0;

        for(int i=0;i<battlers.Length;i++)
        {
            Battler battler = battlers[i];
            total = total + GetCharacterLootLuck(battler.Character);
        }
        return total;
    }

    public static float GetCharacterLootLuck(Character character)
    {
        return 0.25f + (character.GetAttribute(Legrand.core.Attribute.LUCK) * 0.25f);
    }

    public static void CalculateGauge(Battler battler, GaugeType type)
    {
        MainCharacter mc = battler.Character as MainCharacter;
        battler.ChangeGauge(mc.GetGaugeSpeed(type));
    }

    public static float CalculateInterrupt(Battler Attacker, Battler Defender)
    {
        float ChanceInterrupt = 0f;
        float timedHitBonus = LegrandBackend.Instance.GetConstantValueFormula("TimedHitGoodInterrupt");

            float r1 = (Attacker.GetAttribute(Legrand.core.Attribute.LUCK) - Defender.GetAttribute(Legrand.core.Attribute.LUCK)) * 0.8f;
            float r2 = (GetAccRate(Attacker) / (GetEvaRate(Defender)*10f));
            float rnd = LegrandUtility.Random(-10f,10f);
            if (Attacker.TimedHit.AttackHitState == TimedHitState.Perfect)
                timedHitBonus = LegrandBackend.Instance.GetConstantValueFormula("TimedHitPerfectInterrupt");
            else if (Attacker.TimedHit.AttackHitState == TimedHitState.Miss)
                timedHitBonus = LegrandBackend.Instance.GetConstantValueFormula("TimedHitPoorInterrupt");

            ChanceInterrupt = (r1 * r2) + timedHitBonus + rnd;
        return ChanceInterrupt;
    }
}