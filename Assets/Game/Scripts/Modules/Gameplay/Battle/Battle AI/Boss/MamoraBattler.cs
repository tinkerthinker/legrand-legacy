﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;

public class MamoraBattler : Battler {

    private BattleAICommand _BattleAICommand;
    private Encounter _Encounter;
    private int Act = 0;

    private Transform DeadCamera;
    public Animator DeadCameraAnimator;
    [Header("MAMORA DOURDI")]
    //var Arkwora
    public List<RuntimeAnimatorController> ListAnimator;
    private AnimatorChanger _AnimChange;

    //check boss on X turn
    private Transform CameraPreview;
    public Animator CameraAnimator;

    private float CangkangHealth;
    private int _BattleTurn;

    private int _Tentacles;

    private bool firstClose = false, secondClose = false, thirdClose = false;
    public override void BossOnInit (){
        Battle.Instance.Controller.waitForCinematic = true;
        _BattleAICommand = Battle.Instance.battleAICommand;
        _Encounter = (character as Encounter);
        CangkangHealth = _Encounter.Health.MaxValue / 4;
        Act = 1;

        _AnimChange = gameObject.GetComponent<AnimatorChanger> ();
        ListAnimator = _AnimChange.ListAnimator;

        CameraPreview = ((BattleHelperBoss)_BattleHelper).PreviewAnimation;
        CameraAnimator = CameraPreview.GetComponent<Animator>();

        DeadCamera = ((BattleHelperBoss)_BattleHelper).DeadCinematic;
        DeadCameraAnimator = DeadCamera.GetComponent<Animator>();
    }

    public override Transform GetPreviewHelper(){
        return CameraPreview;
    }

    #region Unique
    public override void CheckBossOnEachTurn(){
       
    }

    public override void ReceiveDamage(Damage damage, Battler attacker, bool isSkill)
    {
        if (attacker.AttackType == AttackType.Melee && _Tentacles > 0)
        {
            
        }
            //damaged tentacle
        else {
            
        }
            //damaged main body
    }

    public override bool CheckUniqueBehaviour (){

        return false;
    }
    #endregion

    #region AICommand
    public override void BossBehaviour (){
        _BattleAICommand.SelectedTargets = new List<Battler> ();

        if (_BattleTurn >= 4 && Act <=1 && health.Value > health.MaxValue*0.5f)
        {
            Act++;
            BossAiDecision(1);
        }
        else if (_BattleTurn >= 4 && Act <=2 && health.Value > health.MaxValue*0.5f)
        {
            Act++;
            BossAiDecision(2);
        }
        else if (RollChance() <= 25 && Act <=3 && health.Value <= health.MaxValue*0.5f)
        {
            Act++;
            BossAiDecision(3);
        } 
        else if (RollChance() < 40 && Act <= 4) {
            Act++;
            BossAiDecision (4);
        } 
        else if (RollChance() < 50 && Act <= 5 && health.Value > health.MaxValue*0.5f) {
            Act++;
            BossAiDecision (5);
        } 
        else if (RollChance() < 40 && Act <= 6 && health.Value < health.MaxValue*0.5f) {
            Act++;
            BossAiDecision (6);
        } 
        else if (RollChance() < 80 && Act <= 7 ) {
            Act++;
            BossAiDecision (7);
        }
        else {
            LastAct (_Encounter);
        }
    }
    public void BossAiDecision(int id){
        //int i = 0;
        switch (id) {
            case 1:
                _Encounter.ComTypes = CommandTypes.Charging;
                //_Encounter.attackTarget = AttackTarget.RANDOM_TARGET;
                SendCommandTarget(1);
                break;
            case 2:
                _Encounter.ComTypes = CommandTypes.SkillUlti;
                _Encounter.attackTarget = AttackTarget.RANDOM_TARGET;
                _BattleAICommand.AiBehaviour.FilteringTarget(this, Battle.Instance.charList);
                if (_BattleAICommand.SelectedTargets.Count > 0){
                    SendCommandTarget (62546301);
                    Act = 1;
                    _BattleTurn = 0;
                }else{
                    BossBehaviour();
                }
                break;
            case 3:
                _Encounter.ComTypes = CommandTypes.SkillUlti;
                _Encounter.attackTarget = AttackTarget.RANDOM_TARGET;
                _BattleAICommand.AiBehaviour.FilteringTarget(this, Battle.Instance.charList);
                if (_BattleAICommand.SelectedTargets.Count > 0){
                    SendCommandTarget (62546300);
                    Act = 1;
                    _BattleTurn = 0;
                }else{
                    BossBehaviour();
                }
                break;
            case 4:
                _Encounter.ComTypes = CommandTypes.Magic;
                _Encounter.attackTarget = AttackTarget.ROW_TARGET;

                _BattleAICommand.AiBehaviour.FilteringTarget(this, Battle.Instance.charList);
                if (_BattleAICommand.SelectedTargets.Count > 0){
                    SendCommandTarget (1);
                    Act = 1;
                }else{
                    BossBehaviour();
                }
                break;
            case 5:
                _Encounter.ComTypes = CommandTypes.Magic;
                _Encounter.attackTarget = AttackTarget.ROW_TARGET;

                _BattleAICommand.AiBehaviour.FilteringTarget(this, Battle.Instance.charList);
                if (_BattleAICommand.SelectedTargets.Count > 0){
                    SendCommandTarget (2);
                    Act = 1;
                }else{
                    BossBehaviour();
                }
                break;
            case 6:
                _Encounter.ComTypes = CommandTypes.Magic;
                _Encounter.attackTarget = AttackTarget.ROW_TARGET;

                _BattleAICommand.AiBehaviour.FilteringTarget(this, Battle.Instance.charList);
                if (_BattleAICommand.SelectedTargets.Count > 0){
                    SendCommandTarget (3);
                    Act = 1;
                }else{
                    BossBehaviour();
                }
                break;
            case 7:
                _Encounter.ComTypes = CommandTypes.Attack;
                _Encounter.attackTarget = AttackTarget.RANDOM_TARGET;

                _BattleAICommand.AiBehaviour.FilteringTarget(this, Battle.Instance.charList);
                if (_BattleAICommand.SelectedTargets.Count > 0){
                    SendCommandTarget (1);
                    Act = 1;
                }else{
                    BossBehaviour();
                }
                break;
        }
        _BattleTurn++;
    }
    void SendCommandTarget (int comValue){
        _BattleAICommand.SendCommandToTarget (comValue, this);
    }
    public void LastAct (Encounter encounter){
        Act = 1;
        _Encounter.ComTypes = CommandTypes.Attack;
        _Encounter.attackTarget = AttackTarget.RANDOM_TARGET;

        _BattleAICommand.AiBehaviour.FilteringTarget(this, Battle.Instance.charList);
        if (_BattleAICommand.SelectedTargets.Count > 0){
            SendCommandTarget (1);
            Act = 1;
        }else{
            _BattleAICommand.EnemyGuard(this);
        }
    }

    private int RollChance (){
        int i = LegrandUtility.Random(1, 100);
        return i;
    }
    #endregion


    #region Camera Dead Cinematic
    public override void CheckBossOnDeadCinematic (){
        EventManager.Instance.TriggerEvent(new CameraDeadCinematic(this, DeadCamera));
        DeadCameraAnimator.CrossFade("DEAD_CINEMATIC", 0.1f);
    }
    #endregion
}
