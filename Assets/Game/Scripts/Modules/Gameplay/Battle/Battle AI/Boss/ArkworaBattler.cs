﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;

public class ArkworaBattler : Battler {

	private BattleAICommand _BattleAICommand;
	private Encounter _Encounter;
	private int Act = 0;

    private Transform DeadCamera;
    public Animator DeadCameraAnimator;
	[Header("ARKWORA")]
	//var Arkwora
	public List<RuntimeAnimatorController> ListAnimator;
	public int Hit = 0;
	private int _MaxGuard = 0;
	private bool canAttack = false;
    private AnimatorChanger _AnimChange;
    private int _BattleTurn;
    private bool _CalledOnce;
    //check boss on X turn
    private Transform CameraPreview;
    public Animator CameraAnimator;
    private bool isChanelling = false;
    private float CangkangHealth;

    private bool firstClose = false, secondClose = false, thirdClose = false;
	public override void BossOnInit (){
        Battle.Instance.Controller.waitForCinematic = true;
		_BattleAICommand = Battle.Instance.battleAICommand;
		_Encounter = (character as Encounter);
        CangkangHealth = _Encounter.Health.MaxValue / 4;
		Act = 1;

		canAttack = false;
		_AnimChange = gameObject.GetComponent<AnimatorChanger> ();
		ListAnimator = _AnimChange.ListAnimator;

		_MaxGuard = 3;

        CameraPreview = ((BattleHelperBoss)_BattleHelper).PreviewAnimation;
        CameraAnimator = CameraPreview.GetComponent<Animator>();

        DeadCamera = ((BattleHelperBoss)_BattleHelper).DeadCinematic;
        DeadCameraAnimator = DeadCamera.GetComponent<Animator>();
	}

    public override Transform GetPreviewHelper(){
        return CameraPreview;
    }

	#region Unique
	public override void CheckBossOnEachTurn(){
//		if (health.Value > health.MaxValue / 2) {
//			_MaxGuard = 3;
//		} else if (health.Value > health.MaxValue / 0.25f) {
//			_MaxGuard = 4;
//		} else {
//			_MaxGuard = 6;
//		}
        Debug.Log (CangkangHealth);
        if (CangkangHealth <= 0 && !canAttack) {
			canAttack = true;
			GetComponent<Animator> ().runtimeAnimatorController = ListAnimator[1];
			StateController.Anim = GetComponent<Animator>();
            battlerState = BattlerState.Preview;
            //StateController.Anim.CrossFade ("PREVIEW", 0f);
            //Hit = 0;
            return;
        } 
        else if (health.Value < health.MaxValue * 0.25f && canAttack && !firstClose) {//(Hit > _MaxHit && canAttack) {
            canAttack = false;
            firstClose = true;
            GetComponent<Animator> ().runtimeAnimatorController = ListAnimator[0];
            StateController.Anim = GetComponent<Animator>();
            battlerState = BattlerState.Preview;
            //StateController.Anim.CrossFade ("PREVIEW", 0f);
            //Hit = 0;
            CangkangHealth = _Encounter.Health.MaxValue/4;
            return;
        }
        else if (health.Value < health.MaxValue * 0.5f && canAttack && !secondClose) {//(Hit > _MaxHit && canAttack) {
            canAttack = false;
            secondClose = true;
            GetComponent<Animator> ().runtimeAnimatorController = ListAnimator[0];
            StateController.Anim = GetComponent<Animator>();
            battlerState = BattlerState.Preview;
            //StateController.Anim.CrossFade ("PREVIEW", 0f);
            //Hit = 0;
            CangkangHealth = _Encounter.Health.MaxValue/4;
            return;
        }
        else if (health.Value < health.MaxValue * 0.75f && canAttack && !thirdClose) {//(Hit > _MaxHit && canAttack) {
			canAttack = false;
            thirdClose = true;
			GetComponent<Animator> ().runtimeAnimatorController = ListAnimator[0];
            StateController.Anim = GetComponent<Animator>();
            battlerState = BattlerState.Preview;
            //StateController.Anim.CrossFade ("PREVIEW", 0f);
            //Hit = 0;
            CangkangHealth = _Encounter.Health.MaxValue/3.5f;
            return;
        }
    }
        
    public override void ReceiveDamage(Damage damage, Battler attacker, bool isSkill)
	{
		
		//collect information damage

		if (battlerState == BattlerState.Guard)
			damage.BaseDamage = damage.BaseDamage * 0.5f;



		if (!canAttack) {
            float damages = damage.BaseDamage/2;
            CangkangHealth -= damages;
            PopUp(Mathf.RoundToInt(damages).ToString(), (int)attacker.BattlerType, damage.IsCritical);
//			this.PopUp ("Guard !!", (int)BattleTextPopUp.BattleTextType.Enemy);
		} else {
			character.ReceiveDamage (damage.BaseDamage);
            PopUp(Mathf.RoundToInt(damage.BaseDamage).ToString(), (int)attacker.BattlerType, damage.IsCritical);
//            if (!isSkill)
//                ItemController._Instance.ExecBuffOnHittedAndAttack(this, attacker, Mathf.RoundToInt(damage.BaseDamage));
        }
        Hit++;
	}

    public override bool CheckUniqueBehaviour (){
        if (canAttack)
            return false;
        else
            return true;
    }
	#endregion

    #region AICommand
    public override void BossBehaviour (){
        _BattleAICommand.SelectedTargets = new List<Battler> ();

        if (_BattleTurn >= 4 && !isChanelling && Act <=1 && health.Value > health.MaxValue*0.5f)
        {
            Act++;
            isChanelling = true;
            BossAiDecision(1);
        }
        else if (_BattleTurn >= 4 && isChanelling && Act <=2 && health.Value > health.MaxValue*0.5f)
        {
            Act++;
            isChanelling = false;
            BossAiDecision(2);
            _BattleTurn = 0;
        }
        else if (RollChance() <= 25 && Act <=3 && health.Value <= health.MaxValue*0.5f)
        {
            Act++;
            isChanelling = false;
            BossAiDecision(3);
        } 
        else if (RollChance() < 40 && Act <= 4) {
            Act++;
            isChanelling = false;
            BossAiDecision (4);
        } 
        else if (RollChance() < 50 && Act <= 5 && health.Value > health.MaxValue*0.5f) {
            Act++;
            isChanelling = false;
            BossAiDecision (5);
        } 
        else if (RollChance() < 40 && Act <= 6 && health.Value < health.MaxValue*0.5f) {
            Act++;
            isChanelling = false;
            BossAiDecision (6);
        } 
        else if (RollChance() < 80 && Act <= 7 ) {
            Act++;
            isChanelling = false;
            BossAiDecision (7);
        }
        else {
            isChanelling = false;
            LastAct (_Encounter);
        }
    }
    public void BossAiDecision(int id){
        //int i = 0;
        switch (id) {
            case 1:
                _Encounter.ComTypes = CommandTypes.Charging;
                //_Encounter.attackTarget = AttackTarget.RANDOM_TARGET;
                SendCommandTarget(1);
                break;
            case 2:
                _Encounter.ComTypes = CommandTypes.SkillUlti;
                _Encounter.attackTarget = AttackTarget.RANDOM_TARGET;
                SetEncounterCountTarget(62546301);
                _BattleAICommand.AiBehaviour.FilteringTarget(this, Battle.Instance.charList);
                if (_BattleAICommand.SelectedTargets.Count > 0){
                    SendCommandTarget (62546301);
                    Act = 1;
                    _BattleTurn = 0;
                }else{
                    BossBehaviour();
                }
                break;
            case 3:
                _Encounter.ComTypes = CommandTypes.SkillUlti;
                _Encounter.attackTarget = AttackTarget.RANDOM_TARGET;
                SetEncounterCountTarget(62546300);
                _BattleAICommand.AiBehaviour.FilteringTarget(this, Battle.Instance.charList);
                if (_BattleAICommand.SelectedTargets.Count > 0){
                    SendCommandTarget (62546300);
                    Act = 1;
                }else{
                    BossBehaviour();
                }
                break;
            case 4:
                _Encounter.ComTypes = CommandTypes.Magic;
                _Encounter.attackTarget = AttackTarget.ROW_TARGET;

                _BattleAICommand.AiBehaviour.FilteringTarget(this, Battle.Instance.charList);
                if (_BattleAICommand.SelectedTargets.Count > 0){
                    SendCommandTarget (1);
                    Act = 1;
                }else{
                    BossBehaviour();
                }
                break;
            case 5:
                _Encounter.ComTypes = CommandTypes.Magic;
                _Encounter.attackTarget = AttackTarget.ROW_TARGET;

                _BattleAICommand.AiBehaviour.FilteringTarget(this, Battle.Instance.charList);
                if (_BattleAICommand.SelectedTargets.Count > 0){
                    SendCommandTarget (2);
                    Act = 1;
                }else{
                    BossBehaviour();
                }
                break;
            case 6:
                _Encounter.ComTypes = CommandTypes.Magic;
                _Encounter.attackTarget = AttackTarget.ROW_TARGET;

                _BattleAICommand.AiBehaviour.FilteringTarget(this, Battle.Instance.charList);
                if (_BattleAICommand.SelectedTargets.Count > 0){
                    SendCommandTarget (3);
                    Act = 1;
                }else{
                    BossBehaviour();
                }
                break;
            case 7:
                _Encounter.ComTypes = CommandTypes.Attack;
                _Encounter.attackTarget = AttackTarget.RANDOM_TARGET;

                _BattleAICommand.AiBehaviour.FilteringTarget(this, Battle.Instance.charList);
                if (_BattleAICommand.SelectedTargets.Count > 0){
                    SendCommandTarget (1);
                    Act = 1;
                }else{
                    BossBehaviour();
                }
                break;
        }
        _BattleTurn++;
    }
    void SetEncounterCountTarget(int SkillId){
        OffensiveBattleSkill offensiveSkill = _Encounter.GetOffensiveSkill(SkillId);
        if (offensiveSkill != null){
            switch (offensiveSkill.Target)
            {
                case TargetType.Single:
                    _Encounter.attackTarget = AttackTarget.RANDOM_TARGET;
                    break;
                case TargetType.Row:
                    _Encounter.attackTarget = AttackTarget.ROW_TARGET;
                    break;
                case TargetType.Column:
                    _Encounter.attackTarget = AttackTarget.COLOUMN_TARGET;
                    break;
                case TargetType.All:
                    _Encounter.attackTarget = AttackTarget.ALL_TARGET;
                    break;
            }
        }
    }
    void SendCommandTarget (int comValue){
        _BattleAICommand.SendCommandToTarget (comValue, this);
    }
    public void LastAct (Encounter encounter){
        Act = 1;
        _Encounter.ComTypes = CommandTypes.Attack;
        _Encounter.attackTarget = AttackTarget.RANDOM_TARGET;

        _BattleAICommand.AiBehaviour.FilteringTarget(this, Battle.Instance.charList);
        if (_BattleAICommand.SelectedTargets.Count > 0){
            SendCommandTarget (1);
            Act = 1;
        }else{
            _BattleAICommand.EnemyGuard(this);
        }
    }

    private int RollChance (){
        int i = LegrandUtility.Random(1, 100);
        return i;
    }
    #endregion


    #region Camera Dead Cinematic
    public override void CheckBossOnDeadCinematic (){
        EventManager.Instance.TriggerEvent(new CameraDeadCinematic(this, DeadCamera));
        DeadCameraAnimator.CrossFade("DEAD_CINEMATIC", 0.1f);
    }
    #endregion
}
