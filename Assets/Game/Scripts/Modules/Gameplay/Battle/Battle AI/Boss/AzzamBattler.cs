﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;

public class AzzamBattler : Battler {

    [Header("AZZAM")]
    private BattleAICommand _BattleAICommand;
    private Encounter _Encounter;
    private int Act = 0;

    private int _BattleTurn;    

    public override void BossOnInit (){
        Battle.Instance.Controller.waitForCinematic = true;
        _BattleAICommand = Battle.Instance.battleAICommand;
        _Encounter = (character as Encounter);

        Act = 1;
    }

    #region Unique
    public override void CheckBossOnEachTurn(){


    }
    #endregion

    #region AICommand
    public override void BossBehaviour (){
        _BattleAICommand.SelectedTargets = new List<Battler> ();

        if (Act >= 4) {
            Act++;
            BossAiDecision (1);
        } else {
            LastAct (_Encounter);
        }
    }
    public void BossAiDecision(int id){
        //int i = 0;
        switch (id) {
            case 1:
                _Encounter.ComTypes = CommandTypes.SkillUlti;
                _Encounter.attackTarget = AttackTarget.RANDOM_TARGET;
                _BattleAICommand.AiBehaviour.FilteringTarget(this, Battle.Instance.charList);
                if (_BattleAICommand.SelectedTargets.Count > 0){
                    SendCommandTarget (1946112066);
                    Act = 1;
                    _BattleTurn = 0;
                } else{
                    LastAct(_Encounter);
                }
                break;
        }
    }
    void SendCommandTarget (int comValue){
        _BattleTurn++;
        _BattleAICommand.SendCommandToTarget (comValue, this);
    }
    public void LastAct (Encounter encounter){
        _Encounter.ComTypes = CommandTypes.Attack;
        _Encounter.attackTarget = AttackTarget.RANDOM_TARGET;

        _BattleAICommand.AiBehaviour.FilteringTarget(this, Battle.Instance.charList);
        if (_BattleAICommand.SelectedTargets.Count > 0){
            SendCommandTarget (1);
            Act = 1;
        }else{
            _BattleAICommand.EnemyGuard(this);
        }
    }

    private int RollChance (){
        int i = LegrandUtility.Random(1, 100);
        return i;
    }
    #endregion

    #region Camera Dead Cinematic
    public override void CheckBossOnDeadCinematic (){
        //        EventManager.Instance.TriggerEvent(new CameraDeadCinematic(this, DeadCamera));
        //        DeadCameraAnimator.CrossFade("DEAD_CINEMATIC", 0.1f);
    }
    #endregion
}
