﻿using UnityEngine;
using System.Collections;

public class DeadCinematic : MonoBehaviour {

    public delegate void CinematicEvent (DeadCinematic c, Battler bat);
    public event CinematicEvent DoneCinematic;
    public Battler battler;

    void Start (){
        battler = GetComponentInParent<Battler>();
    }
    void DoneCinematicEvent (){
        if (DoneCinematic != null)
        {
            DoneCinematic(this, battler);
        }
    }

    public void ClearPreviewEvent (){
        DoneCinematic = null;
    }
}
