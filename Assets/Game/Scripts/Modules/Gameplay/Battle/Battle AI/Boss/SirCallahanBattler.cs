﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;

public class SirCallahanBattler : Battler {

    [Header("SIR CALLAHAN")]
    private BattleAICommand _BattleAICommand;
    private Encounter _Encounter;
    private int Act = 0;

    private Transform CameraPreview;
    public Animator PreviewCameraAnimator;
    private int _BattleTurn;

    private Transform DeadCamera;
    public Animator DeadCameraAnimator;

    public override void BossOnInit (){
        Battle.Instance.Controller.waitForCinematic = true;
        _BattleAICommand = Battle.Instance.battleAICommand;
        _Encounter = (character as Encounter);
        CameraPreview = ((BattleHelperBoss)_BattleHelper).PreviewAnimation;
        PreviewCameraAnimator = CameraPreview.GetComponent<Animator>();

        DeadCamera = ((BattleHelperBoss)_BattleHelper).DeadCinematic;
        DeadCameraAnimator = DeadCamera.GetComponent<Animator>();

        Act = 1;
    }

    public override Transform GetPreviewHelper(){
        return CameraPreview;
    }

    #region Unique
    public override void CheckBossOnEachTurn(){


    }
    #endregion

    #region AICommand
    public override void BossBehaviour (){
        _BattleAICommand.SelectedTargets = new List<Battler> ();

        if (_BattleTurn >= 5 && health.Value > health.MaxValue * 0.5f && Act <= 1) {
            Act++;
            BossAiDecision (1);
        } else if (_BattleTurn >= 5 && health.Value <= health.MaxValue * 0.5f && Act <= 2) {
            Act++;
            BossAiDecision (2);
        }
        else if (RollChance() <= 50 && Act <= 4) {
            Act++;
            BossAiDecision (3);
        } else if (RollChance() < 30 && Act <= 5) {
            Act++;
            BossAiDecision (4);
        } else if (RollChance() < 40 && Act <= 6 ) {
            Act++;
            BossAiDecision (5);
        }  else {
            LastAct (_Encounter);
        }
    }
    public void BossAiDecision(int id){
        //int i = 0;
        switch (id) {
            case 1:
                _Encounter.ComTypes = CommandTypes.SkillUlti;
                _Encounter.attackTarget = AttackTarget.RANDOM_TARGET;
                _BattleAICommand.AiBehaviour.FilteringTarget(this, Battle.Instance.charList);
                if (_BattleAICommand.SelectedTargets.Count > 0){
                    SendCommandTarget (2077204);
                    Act = 1;
                    _BattleTurn = 0;
                }else{
                    BossBehaviour();
                }
                break;
            case 2:
                _Encounter.ComTypes = CommandTypes.SkillUlti;
                _Encounter.attackTarget = AttackTarget.RANDOM_TARGET;
                _BattleAICommand.AiBehaviour.FilteringTarget(this, Battle.Instance.charList);
                if (_BattleAICommand.SelectedTargets.Count > 0){
                    SendCommandTarget (2077205);
                    Act = 1;
                    _BattleTurn = 0;
                }else{
                    BossBehaviour();
                }
                break;
            case 3:
                _Encounter.ComTypes = CommandTypes.Attack;
                _Encounter.attackTarget = AttackTarget.RANDOM_TARGET;

                _BattleAICommand.AiBehaviour.FilteringTarget(this, Battle.Instance.charList);
                if (_BattleAICommand.SelectedTargets.Count > 0){
                    SendCommandTarget (1);
                    Act = 1;
                }else{
                    BossBehaviour();
                }

                break;
            case 4:
                _Encounter.ComTypes = CommandTypes.Item;
                _Encounter.attackTarget = AttackTarget.RANDOM_TARGET;
                _BattleAICommand.AiBehaviour.FilteringTarget(this, Battle.Instance.charList);

                if (_BattleAICommand.SelectedTargets.Count > 0){
                    SendCommandTarget (2);
                    Act = 1;
                }else{
                    BossBehaviour();
                }
                break;
            case 5:
                _Encounter.ComTypes = CommandTypes.Magic;
                _Encounter.attackTarget = AttackTarget.RANDOM_TARGET;
                _BattleAICommand.AiBehaviour.FilteringTarget(this, Battle.Instance.charList);

                if (_BattleAICommand.SelectedTargets.Count > 0){
                    SendCommandTarget(1);
                    Act = 1;
                }else{
                    LastAct(_Encounter);
                }
                break;
        }
    }
    void SendCommandTarget (int comValue){
        _BattleTurn++;
        _BattleAICommand.SendCommandToTarget (comValue, this);
    }
    public void LastAct (Encounter encounter){
        Act = 1;
        _Encounter.ComTypes = CommandTypes.Attack;
        _Encounter.attackTarget = AttackTarget.RANDOM_TARGET;

        _BattleAICommand.AiBehaviour.FilteringTarget(this, Battle.Instance.charList);
        if (_BattleAICommand.SelectedTargets.Count > 0){
            SendCommandTarget (1);
            Act = 1;
        }else{
            _BattleAICommand.EnemyGuard(this);
        }
    }

    private int RollChance (){
        int i = LegrandUtility.Random(1, 100);
        return i;
    }
    #endregion

    #region Camera Dead Cinematic
    public override void CheckBossOnDeadCinematic (){
        EventManager.Instance.TriggerEvent(new CameraDeadCinematic(this, DeadCamera));
        DeadCameraAnimator.CrossFade("DEAD_CINEMATIC", 0.1f);
    }
    #endregion
}
