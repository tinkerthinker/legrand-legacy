﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;

public class RampokBattler : Battler {

    [Header("RAMPOK BOLONG")]
    private int _BattleTurn;

    public override void BossOnInit (){
        _BattleTurn = 0;
    }

    public override void CheckBossOnEachTurn()
    {
        _BattleTurn++;
    }
}
