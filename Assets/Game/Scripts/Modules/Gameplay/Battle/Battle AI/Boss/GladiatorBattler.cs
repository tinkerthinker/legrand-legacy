﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;


public class GladiatorBattler : Battler {

    [Header("GLADIATOR")]
    private BattleAICommand _BattleAICommand;
    private Encounter _Encounter;
    private int Act = 0;

    private Transform CameraPreview;
    public Animator PreviewCameraAnimator;
    private int _BattleTurn;
    private bool _CalledOnce;

    private Transform DeadCamera;
    public Animator DeadCameraAnimator;
	// Use this for initialization
    public override void BossOnInit (){
        Battle.Instance.Controller.waitForCinematic = true;
        _BattleAICommand = Battle.Instance.battleAICommand;
        _Encounter = (character as Encounter);
        CameraPreview = ((BattleHelperBoss)_BattleHelper).PreviewAnimation;
        PreviewCameraAnimator = CameraPreview.GetComponent<Animator>();

        DeadCamera = ((BattleHelperBoss)_BattleHelper).DeadCinematic;
        DeadCameraAnimator = DeadCamera.GetComponent<Animator>();

        Act = 1;
        _CalledOnce = false;
    }
    #region Unique
    public override void CheckBossOnEachTurn(){
        Act++;
    }
    #endregion
    public override Transform GetPreviewHelper(){
        return CameraPreview;
    }
    #region AICommand
    public override void BossBehaviour (){
        _BattleAICommand.SelectedTargets = new List<Battler> ();

        if (Act > 4)
        {
            Act++;
            BossAiDecision(1);
        } else {
            LastAct (_Encounter);
        }
    }
    public void BossAiDecision(int id){
        //int i = 0;
        switch (id) {
            case 1:
                _Encounter.ComTypes = CommandTypes.SkillUlti;
                _Encounter.attackTarget = AttackTarget.ALL_TARGET;
                _BattleAICommand.AiBehaviour.FilteringTarget(this, Battle.Instance.charList);
                if (_BattleAICommand.SelectedTargets.Count > 0){
                    SendCommandTarget (826166822);
                    Act = 1;
                    _BattleTurn = 0;
                }else{
                    BossBehaviour();
                }
                break;
            case 2:
                _Encounter.ComTypes = CommandTypes.Attack;
                _Encounter.attackTarget = AttackTarget.RANDOM_TARGET;
                _BattleAICommand.AiBehaviour.FilteringTarget(this, Battle.Instance.charList);

                if (_BattleAICommand.SelectedTargets.Count > 0){
                    SendCommandTarget(1);
                    Act = 1;
                }else{
                    LastAct(_Encounter);
                }
                break;
        }
    }
    void SendCommandTarget (int comValue){
        _BattleTurn++;
        _BattleAICommand.SendCommandToTarget (comValue, this);
    }
    public void LastAct (Encounter encounter){
        _Encounter.ComTypes = CommandTypes.Attack;
        _Encounter.attackTarget = AttackTarget.ALL_TARGET;

        _BattleAICommand.AiBehaviour.FilteringTarget(this, Battle.Instance.charList);
        if (_BattleAICommand.SelectedTargets.Count > 0){
            SendCommandTarget (1);
        }else{
            _BattleAICommand.EnemyGuard(this);
        }
    }

    private int RollChance (){
        int i = LegrandUtility.Random(1, 100);
        return i;
    }
    #endregion

    #region Camera Dead Cinematic
    public override void CheckBossOnDeadCinematic (){
        EventManager.Instance.TriggerEvent(new CameraDeadCinematic(this, DeadCamera));
        DeadCameraAnimator.CrossFade("DEAD_CINEMATIC", 0.1f);
    }
    #endregion
}
