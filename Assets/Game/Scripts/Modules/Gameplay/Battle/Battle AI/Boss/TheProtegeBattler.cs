﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;


public class TheProtegeBattler : Battler {

    [Header("THE PROTEGE")]
    private BattleAICommand _BattleAICommand;
    private Encounter _Encounter;
    private int Act = 0;

    private Transform CameraPreview;
    public Animator PreviewCameraAnimator;
    private int _BattleTurn;
    private bool _CalledOnce;

    private Transform DeadCamera;
    public Animator DeadCameraAnimator;

    public override void BossOnInit (){
        Battle.Instance.Controller.waitForCinematic = false;
        _BattleAICommand = Battle.Instance.battleAICommand;
        _Encounter = (character as Encounter);
        CameraPreview = ((BattleHelperBoss)_BattleHelper).PreviewAnimation;
        PreviewCameraAnimator = CameraPreview.GetComponent<Animator>();

        DeadCamera = ((BattleHelperBoss)_BattleHelper).DeadCinematic;
        DeadCameraAnimator = DeadCamera.GetComponent<Animator>();

        Act = 1;
        _CalledOnce = false;
    }

    public override Transform GetPreviewHelper(){
        return CameraPreview;
    }

    #region Unique
    public override void CheckBossOnEachTurn(){

    }
    #endregion

    #region AICommand
    public override void BossBehaviour (){
        _BattleAICommand.SelectedTargets = new List<Battler> ();

        if (Act >= 4) {
            Act++;
            BossAiDecision (1);
        } else {
            LastAct (_Encounter);
        }
    }
    public void BossAiDecision(int id){
        //int i = 0;
        switch (id) {
            case 1: // ulti 1
                _Encounter.ComTypes = CommandTypes.SkillUlti;
                _Encounter.attackTarget = FindSkilltarget(2580768); //id belum
                _BattleAICommand.AiBehaviour.FilteringTarget(this, Battle.Instance.charList);
                if (_BattleAICommand.SelectedTargets.Count > 0){
                    SendCommandTarget (2580768);
                    Act = 1;
                    _BattleTurn = 0;
                    _CalledOnce = true;
                }else{
                    LastAct(_Encounter);
                }
                break;
        }
    }
    void SendCommandTarget (int comValue){
        _BattleTurn++;
        _BattleAICommand.SendCommandToTarget (comValue, this);
    }
    public void LastAct (Encounter encounter){
        _Encounter.ComTypes = CommandTypes.Attack;
        _Encounter.attackTarget = AttackTarget.RANDOM_TARGET;

        _BattleAICommand.AiBehaviour.FilteringTarget(this, Battle.Instance.charList);
        if (_BattleAICommand.SelectedTargets.Count > 0){
            SendCommandTarget (1);
            Act = 1;
        }else{
            _BattleAICommand.EnemyGuard(this);
        }
    }

    private int RollChance (){
        int i = LegrandUtility.Random(1, 100);
        return i;
    }
    #endregion

    private AttackTarget FindSkilltarget (int comValue){
        TargetType targetType = _Encounter.Skills.Find(x => x.CommandValue == comValue).Target;
        AttackTarget attackTarget = AttackTarget.RANDOM_TARGET;
        switch (targetType){
            case TargetType.Single:
                attackTarget = AttackTarget.RANDOM_TARGET;
                break;
            case TargetType.Column:
                attackTarget = AttackTarget.COLOUMN_TARGET;
                break;
            case TargetType.Row:
                attackTarget = AttackTarget.ROW_TARGET;
                break;
            case TargetType.All:
                attackTarget = AttackTarget.ALL_TARGET;
                break;
        }

        return attackTarget;
    }
    #region Camera Dead Cinematic
    public override void CheckBossOnDeadCinematic (){
        //        EventManager.Instance.TriggerEvent(new CameraDeadCinematic(this, DeadCamera));
        //        DeadCameraAnimator.CrossFade("DEAD_CINEMATIC", 0.1f);
    }
    #endregion
}