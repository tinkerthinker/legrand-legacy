using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using Legrand.core;

public class BattleAICommand : MonoBehaviour {
	[SerializeField]
	private List<CommandData> _AICommandList = new List<CommandData>();
	public List<CommandData> AICommandList {
		get {return _AICommandList;}
	}
	private List<Battler> charList = new List<Battler> ();
	public List<Battler> CharList{
		get {return charList;}
	}

	private List<Battler> selected;

	private Battle battle;
	private BattleController battleController;
	private CommandData commandData;

	private BattleInput _BInput;
	public BattleInput BInput {
		get {return _BInput;}
	}

	private TargetController targetController;
	public TargetController TController {
		get {return targetController;}
	}

	private AIBehaviour _AiBehaviour;
	public AIBehaviour AiBehaviour
	{
		get{return _AiBehaviour;}
	}

	private Encounter encounter;
	public Encounter Encounter {
		get {return Encounter;}
	}

	private List<EncounterData> _CounterData;

	private List <AttackDataTemp> _AttackData;
	private int Act;
	[HideInInspector]
	public List<Battler> SelectedTargets; //target yang dipilih enc

    public List <Battler> targets; //target terakhir yang di confirmasi melihat kondisi dari target
	// Use this for initialization
	void Awake (){
		_BInput 			= GetComponent<BattleInput> ();
		battleController	= GetComponent<BattleController>();
	}
	void Start () {
		battle 				= Battle.Instance;
		this.charList 		= battle.charList;
		AddCommandList();
		Act = 1;
	}
	public void initAICommand()
	{
		targetController 	= new TargetController (_BInput);
	}
	void AddCommandList(){
		CommandData comAttack = new CommandData (0, "", CommandPos.Right, CommandPos.Right, CommandTypes.Attack, 1);
		_AICommandList.Add (comAttack);
		CommandData comGuard = new CommandData (0, "", CommandPos.Left, CommandPos.Left, CommandTypes.Guard, 1);
		_AICommandList.Add (comGuard);
		CommandData comMagic = new CommandData (0, "", CommandPos.Up, CommandPos.Up,CommandTypes.Magic, 1);
		_AICommandList.Add (comMagic);
		CommandData comItem1 = new CommandData (0, "", CommandPos.Down, CommandPos.Up,CommandTypes.Item, 0);
		_AICommandList.Add (comItem1);
		CommandData comItem2 = new CommandData (0, "", CommandPos.Down, CommandPos.Right,CommandTypes.Item, 1);
		_AICommandList.Add (comItem2);
		CommandData comItem3 = new CommandData (0, "", CommandPos.Down, CommandPos.Down,CommandTypes.Item, 2);
		_AICommandList.Add (comItem3);
        CommandData comSkill1 = new CommandData (0, "", CommandPos.Right, CommandPos.Up,CommandTypes.SkillUlti, 0);
        _AICommandList.Add (comSkill1);
        CommandData charging = new CommandData (0, "", CommandPos.Right, CommandPos.Right,CommandTypes.Charging, 0);
        _AICommandList.Add (charging);
	}

	public void Decision (Battler enc){
		initAICommand();
		_AiBehaviour			= new AIBehaviour();
        encounter               = (enc.Character as Encounter);
        targets                 = new List<Battler>();
		float EnemyCurrentHealth = encounter.Health.Value;

		switch(encounter.enemyType){
    		case EnemyType.Attacker:
                if (RollChance () <= 50 && Act <= 1 && !enc.IsActionNegated(CommandTypes.Attack)){
    				Act++;
    				AttackerAct(enc, encounter, null, 1);
    			} 
                else if (enc.Health.Value <= enc.Health.MaxValue/2 && RollChance () <= 60 && Act <= 2 && !enc.IsActionNegated(CommandTypes.Magic)) {
    				Act++;
    				Magic magic = encounter.GetMagic(1);
    				AttackerAct(enc, encounter, magic, 2);
    			}
    			else {
    				LastAct(enc, encounter);
    			}
    			break;
    		case EnemyType.Magic:
    			Magic magics = encounter.GetMagic(1);

                if (enc.Health.Value >= enc.Health.MaxValue/2 && RollChance () <= 70 && Act <= 1 && !enc.IsActionNegated(CommandTypes.Magic)){
    				//Hp over 50 m
    				Act++;
    				MagicAct(enc, encounter, magics, 1);
    			} 
    			else {
    				LastAct(enc, encounter);
    			}
    			break;
    		case EnemyType.Buffer:
    			if (RollChance () <= 80 && Act <= 1){
    				//Ally non buffed
    				Act++;
    				BuffAct(enc, encounter, null, 1);
    			} 
                else if (RollChance () <= 40 && Act <= 2 && enc.Health.Value > enc.Health.MaxValue/2) {
    				//Attack enemy non buffed
    				Act++;
    				BuffAct(enc, encounter, null, 2);
    			}
                else if (RollChance () <= 40 && Act <= 3  && enc.Health.Value <= enc.Health.MaxValue/2 && !enc.IsActionNegated(CommandTypes.Magic)) {
    				//random target magic attack
    				Act++;
    				Magic magic = encounter.GetMagic(1);
    				encounter.ComTypes = CommandTypes.Magic;
    				BuffAct(enc, encounter, magic, 3);
    			}
    			else {
    				encounter.ComTypes = CommandTypes.Attack;
    				LastAct(enc, encounter);
    			}
    			break;
    		case EnemyType.Debuffer:
                if (RollChance () <= 100  && Act <= 1 && !enc.IsActionNegated(CommandTypes.Magic)){
    				//Highest HP d
                    Act++;
                    DebuffAct(enc, encounter, null, 1);
    			} 
                else if (RollChance () <= 60  && Act <= 2 && !enc.IsActionNegated(CommandTypes.Magic)) {
                    //Debuffed target magic attack
                    Act++;
                    Magic magic = encounter.GetMagic(1);
                    encounter.ComTypes = CommandTypes.Magic;
                    DebuffAct(enc, encounter, magic, 2);
    			}
    			else {
    				encounter.ComTypes = CommandTypes.Attack;
    				LastAct(enc, encounter);
    			}
    			break;
    		case EnemyType.Healer:
                Magic encMagic = encounter.GetMagic(1);

                if (RollChance () <= 65 && Act <= 1 && !enc.IsActionNegated(CommandTypes.Magic)){
    				//Heal ally hp under 50
    				Act++;
    				encounter.ComTypes = CommandTypes.Item;
                    HealerAct(enc, encounter,encMagic, 1);
    			} 
                else if (RollChance () <= 30 && Act <= 2 && !enc.IsActionNegated(CommandTypes.Magic)){
    				//magic enemy Lowest HP
    				Act++;
    				encounter.ComTypes = CommandTypes.Magic;
                    HealerAct(enc, encounter,encMagic, 2);
    			}
    			else {
    				encounter.ComTypes = CommandTypes.Attack;
    				LastAct(enc, encounter);
    			}
    			break;
    		case EnemyType.BossUnique:
                SelectedTargets = new List<Battler> ();
    			enc.BossBehaviour();
    			break;
            case EnemyType.NormalUnique:
                SelectedTargets = new List<Battler> ();
                enc.BossBehaviour();
                break;
		}
	}

	private int RollChance (){
		int i = LegrandUtility.Random(0, 100);
		return i;
	}
	#region AI Attacker
	public void AttackerAct (Battler enc, Encounter encounter, Magic magic, int act){
		int i = 0;
		SelectedTargets = new List<Battler> ();
		switch (act) {
		    case 1:
    			encounter.ComTypes = CommandTypes.Attack;
                encounter.attackTarget = AttackTarget.RANDOM_TARGET;
                _AiBehaviour.MyDamage(enc, false);
			    if (SelectedTargets.Count > 0){
                    i = LegrandUtility.Random(0, SelectedTargets.Count);
                    targets.Add(SelectedTargets[i]);
                    SendTarget (enc, 1);

    			}else{
    				Decision (enc);
    			}
			break;
		    case 2:
    			encounter.ComTypes = CommandTypes.Magic;
    			encounter.attackTarget = AttackTarget.RANDOM_TARGET;
                _AiBehaviour.MyDamage(enc, false);
    			if (SelectedTargets.Count > 0){
    				i = LegrandUtility.Random(0, SelectedTargets.Count);
                    MagicTarget(enc, encounter, SelectedTargets[i], magic, 1);
    			}else{
    				Decision (enc);
    			}
			break;
		}
	}

	#endregion

	#region AI Magic
	public void MagicAct(Battler enc, Encounter encounter, Magic magic, int act){
		SelectedTargets = new List<Battler> ();
		int i = 0;
		switch (act) {
    		case 1:
    			encounter.ComTypes = CommandTypes.Magic;
                encounter.attackTarget = AttackTarget.RANDOM_TARGET;
                _AiBehaviour.MyDamage(enc, false);
    			if (SelectedTargets.Count > 0){
                    i = LegrandUtility.Random(0, SelectedTargets.Count);
                    MagicTarget(enc, encounter, SelectedTargets[i], magic, 1);
    			}else{
                    LastAct(enc, encounter);
    			}
			break;
		}
	}
	#endregion

	#region AI Buffer
	public void BuffAct (Battler enc, Encounter encounter, Magic magic, int act){
		SelectedTargets = new List<Battler> ();
		int i = 0;
		switch (act) {
    		case 1:
    			encounter.ComTypes = CommandTypes.Item;
    			encounter.attackTarget = AttackTarget.NON_BUFFED_ALLY;
                _AiBehaviour.MyDamage(enc, true);
    			if (SelectedTargets.Count > 0){
    				i = LegrandUtility.Random(0, SelectedTargets.Count);
                    ItemTarget(enc, encounter, SelectedTargets[i], 1, battle.encounterList);
    			}else{
    				Decision (enc);
    			}
    			break;
    		case 2:
    			encounter.ComTypes = CommandTypes.Attack;
    			encounter.attackTarget = AttackTarget.NON_BUFFED_TARGET;
                _AiBehaviour.MyDamage(enc, false);
    			Debug.Log ("2");
    			if (SelectedTargets.Count > 0){
                    i = LegrandUtility.Random(0, SelectedTargets.Count);
                    targets.Add(SelectedTargets[i]);
                    SendTarget (enc, 1);
    			}else{
    				Decision (enc);
    			}
    			break;
    		case 3:
    			encounter.ComTypes = CommandTypes.Magic;
    			encounter.attackTarget = AttackTarget.RANDOM_TARGET;
                _AiBehaviour.MyDamage(enc, false);
                if (_AiBehaviour.TargetList.Count > 0){
                    i = LegrandUtility.Random(0, SelectedTargets.Count);
                    MagicTarget(enc, encounter, SelectedTargets[i], magic, 1);
    			}
    			else{
    				LastAct(enc, encounter);
    			}
    			break;
		}
	}
    #endregion

    #region Debuff AI
    public void DebuffAct (Battler enc, Encounter encounter, Magic magic, int act)
    {
        SelectedTargets = new List<Battler> ();
        int i = 0;
        switch (act) {
            case 1:
                encounter.ComTypes = CommandTypes.Item;
                encounter.attackTarget = AttackTarget.NON_DEBUFFED_TARGET;
                _AiBehaviour.MyDamage(enc, false);
                if (SelectedTargets.Count > 0){
                    i = LegrandUtility.Random(0, SelectedTargets.Count);
                    ItemTarget(enc, encounter, SelectedTargets[i], 1, battle.charList);
                }else{
                    Decision (enc);
                }
            break;
            case 2:
                encounter.ComTypes = CommandTypes.Magic;
                encounter.attackTarget = AttackTarget.RANDOM_TARGET;
                _AiBehaviour.MyDamage(enc, false);
                if (SelectedTargets.Count > 0){
                    i = LegrandUtility.Random(0, SelectedTargets.Count);
                    MagicTarget(enc, encounter, SelectedTargets[i], magic, 1);
                }else{
                    LastAct (enc, encounter);
                }
            break;
        }
    }
	#endregion

	#region AI Healer
    public void HealerAct(Battler enc, Encounter encounter, Magic magic, int act){
		SelectedTargets = new List<Battler> ();
		int i = 0;
		switch (act) {
    		case 1:
    			encounter.attackTarget = AttackTarget.HEALER;
    			encounter.ComTypes = CommandTypes.Item;

                _AiBehaviour.MyDamage(enc, true);

    			if (SelectedTargets.Count > 0){
                    i = LegrandUtility.Random(0, SelectedTargets.Count);
                    ItemTarget(enc, encounter, SelectedTargets[i], 1, battle.encounterList);
    			}else{
                    HealerAct (enc, encounter, magic, 2);
    			}
			break;
    		case 2:
                encounter.attackTarget = AttackTarget.RANDOM_TARGET;
    			encounter.ComTypes = CommandTypes.Magic;

    			SelectedTargets = new List<Battler>();
                _AiBehaviour.MyDamage(enc, false);
                if (SelectedTargets.Count > 0 && magic != null){
//    				HighestLowest(enc, encounter, null, 2);
                    MagicTarget(enc, encounter, SelectedTargets[i], magic, 1);
    			}
    			else{
    				encounter.ComTypes = CommandTypes.Attack;
    				LastAct(enc, encounter);
    			}
			break;
		}
	}
	#endregion

	#region Boss
	public void SendCommandToTarget (int comValue, Battler enc){
		Encounter encounter = (enc.Character as Encounter);
		if (SelectedTargets.Count  > 0){
			switch (encounter.ComTypes){
    			case CommandTypes.Attack:
                    targets = SelectedTargets;
                    SendTarget (enc, comValue);
    				encounter.ComTypes = CommandTypes.Attack;
    				break;
    			case CommandTypes.Item:
                    ItemTarget(enc,  encounter,SelectedTargets[0], comValue, battle.charList);
    			    encounter.ComTypes = CommandTypes.Item;
    				break;
                case CommandTypes.Magic:
                    Magic magic = encounter.GetMagic(comValue);
//                    SendTarget(enc);
                    MagicTarget(enc, encounter, SelectedTargets[0], magic, comValue);
    				encounter.ComTypes = CommandTypes.Magic;

    				break;
    			case CommandTypes.SkillUlti:
    				SendSkill (enc, SelectedTargets, comValue);
    				encounter.ComTypes = CommandTypes.SkillUlti;
    				break;
			}
		}
		else{
            if (encounter.ComTypes == CommandTypes.Charging)
                EnemyCharge(enc);
            else
			    LastAct(enc, encounter);
		}
	}
	#endregion

	#region Last Act
	public void LastAct (Battler enc, Encounter encounter){
		encounter.ComTypes = CommandTypes.Attack;
		SelectedTargets = new List<Battler> ();
		int i = 0;
		encounter.attackTarget = AttackTarget.RANDOM_TARGET;

        _AiBehaviour.MyDamage(enc, false);

		if (SelectedTargets.Count > 0){
			i = LegrandUtility.Random(0, SelectedTargets.Count);
            targets.Add(SelectedTargets[0]);
            SendTarget (enc, 1);
		}else{
			EnemyGuard(enc);
		}
	}
	#endregion

	#region HighestAndLowestTargetVar
	void HighestLowest (Battler enc, Encounter encounter, Magic magic, int act){
		bool canAttack = false;
		int i = 0;
		while (!canAttack){
			if (TController.canAttack (enc, _AiBehaviour.TargetList[i])) {
				canAttack = true;
                SelectedTargets.Add(_AiBehaviour.TargetList[i]);
                targets.Add(SelectedTargets[0]);
                SendTarget (enc, 1);
			}
			else{
				i++;
				if (i >= _AiBehaviour.TargetList.Count-1){
                    canAttack = true;
                    EnemyGuard(enc);
				}
			}
		}
	}
	#endregion

    #region magicMoreThanOne
    void MagicTarget (Battler encBat, Encounter encounter, Battler target, Magic magic, int comValue)
    {
        switch (magic.MagicTargetType)
        {
            case TargetType.Single:
                targets.Add(target);
                break;
            case TargetType.Column:
                List <Battler> Coloumn = _AiBehaviour.GetColoumnTargets(target, SelectedTargets);

                for (int i = 0; i < Coloumn.Count; i++)
                {
                    if (Coloumn[i].BattlerState != BattlerState.Dead)
                        targets.Add(Coloumn[i]);
                }

                break;
            case TargetType.Row:
                List <Battler> Row = _AiBehaviour.GetRowTargets(target, SelectedTargets);
                for (int i = 0; i < Row.Count; i++)
                {
                    if (Row[i].BattlerState != BattlerState.Dead)
                        targets.Add(Row[i]);
                }
                break;
            case TargetType.All:
                for (int i = 0; i < Battle.Instance.charList.Count; i++)
                {
                    if (Battle.Instance.charList[i].BattlerState != BattlerState.Dead)
                        targets.Add(Battle.Instance.charList[i]);
                }
                break;
        }

        SendTarget(encBat, comValue);
    }
    #endregion

    #region ItemMoreThanOne
    void ItemTarget (Battler encBat, Encounter encounter, Battler target, int IdItem, List <Battler> targetBattler)
    {
        Consumable item = encBat.GetItem(IdItem) as Consumable;

        if (item != null)
        {
            switch (item.Target)
            {
                case TargetType.Single:
                    targets.Add(target);
                    break;
                case TargetType.Column:
                    List <Battler> Coloumn = _AiBehaviour.GetColoumnTargets(target, targetBattler);

                    for (int i = 0; i < Coloumn.Count; i++)
                    {
                        if (Coloumn[i].BattlerState != BattlerState.Dead)
                            targets.Add(Coloumn[i]);
                    }
                    break;
                case TargetType.Row:
                    List <Battler> Row = _AiBehaviour.GetRowTargets(target, targetBattler);
                    for (int i = 0; i < Row.Count; i++)
                    {
                        if (Row[i].BattlerState != BattlerState.Dead)
                            targets.Add(Row[i]);
                    }
                    break;
                case TargetType.All:
                    for (int i = 0; i < targetBattler.Count; i++)
                    {
                        if (targetBattler[i].BattlerState != BattlerState.Dead)
                            targets.Add(targetBattler[i]);
                    }
                    break;
            }
        }

        SendItem(encBat, IdItem);
    }
    #endregion

	#region Item
	public void SendItem (Battler enc, int IdItem){
		Act = 1;
		CommandData newCommand = new CommandData ();
//		List <Battler> targetList = new List<Battler>();
//		targetList.Add (target);
        if (targets.Count > 0){
			enc.BattleTurn = 1;
			switch (IdItem){
			case 1: //heal
				commandData = _AICommandList.Find (x => x.CommandType == (int)CommandTypes.Item && x.CommandValue == 1);
				newCommand = CommandData.Copy (commandData);
				enc.BattleStateCondition = BattleStateCondition.ItemBuff;
				break;
			case 2: //buff
				commandData = _AICommandList.Find (x => x.CommandType == (int)CommandTypes.Item && x.CommandValue == 2);
				newCommand = CommandData.Copy (commandData);
				enc.BattleStateCondition = BattleStateCondition.ItemBuff;
				break;
			case 3: //debuff
				commandData = _AICommandList.Find (x => x.CommandType == (int)CommandTypes.Item && x.CommandValue == 3);
				newCommand = CommandData.Copy (commandData);
				enc.BattleStateCondition = BattleStateCondition.ItemDebuff;
				break;
			case 4:
				commandData = _AICommandList.Find (x => x.CommandType == (int)CommandTypes.Item && x.CommandValue == 4);
				newCommand = CommandData.Copy (commandData);
				break;
			}
			battleController.SetAICommand(enc, newCommand);
            battleController.AddSelectedTarget(enc, targets);
		}
		else {
			EnemyGuard(enc);
		}
		
		battleController.collectCommand(enc);
	}
    #endregion

    #region AI Action Attack
    public void SendTarget (Battler enc, int comValue){
        Act = 1;
        CommandData newCommand = new CommandData ();
        Encounter encounter = (enc.Character as Encounter);
        //      if (!notSingle) {
        //          targets.Add (target);
        //      } else {
        //            for (int x = 0;x < SelectedTargets.Count; x++) {//each (Battler b in SelectedTargets) {
        //                targets.Add (SelectedTargets[x]);
        //          }
        //      }
        if (targets.Count > 0) {

            switch (encounter.ComTypes){
                case CommandTypes.Attack:
                    if(!enc.IsActionNegated((CommandTypes)encounter.ComTypes)){
                        enc.BattleTurn = 3;
                        commandData = _AICommandList.Find (x => x.CommandType == (int)CommandTypes.Attack); 
                        newCommand = CommandData.Copy (commandData);
                    }else{
                        EnemyGuard(enc);
                    }
                    break;
                case CommandTypes.Magic:
                    if(!enc.IsActionNegated((CommandTypes)encounter.ComTypes)){
                        commandData = _AICommandList.Find (x => x.CommandType == (int)CommandTypes.Magic);
                        newCommand = CommandData.Copy (commandData);
                        newCommand.CommandValue = comValue;
                        //search magic by value
                        CharacterMagicPreset MagicPreset = encounter.Magics.MagicPresets.Find (x=>x._ID == comValue);
                        if (MagicPreset != null){
                            newCommand.CommandName = MagicPreset.MagicPreset.Name;
                            enc.BattleTurn = 2;
                            enc.isCasting = false;
                        }
                        else{
                            enc.BattleTurn = 3;
                            commandData = _AICommandList.Find (x => x.CommandType == (int)CommandTypes.Attack); 
                            newCommand = CommandData.Copy (commandData);
                        }
                    }
                    else{
                        EnemyGuard(enc);
                    }

                    break;
                case CommandTypes.Guard:
                    EnemyGuard(enc);
                    break;
            }


            battleController.SetAICommand(enc, newCommand);
            //Random Player
            battleController.AddSelectedTarget(enc, targets);
        } else {
            EnemyGuard(enc);
        }

        battleController.collectCommand(enc);
        //      Debug.Log (enc + " " + commandData.CommandName);

    }
    #endregion

	#region Skill 
	public void SendSkill (Battler enc, List <Battler> targets, int ValueSkill){
		Act = 1;
		CommandData newCommand = new CommandData ();
		List <Battler> targetList = targets;
		if (targets != null){
			enc.BattleTurn = 1;
			commandData = _AICommandList.Find (x => x.CommandType == (int)CommandTypes.SkillUlti);
			newCommand = CommandData.Copy (commandData);
            newCommand.CommandValue = ValueSkill;
            OffensiveBattleSkill encSkill = encounter.Skills.Find (x=> x.CommandValue == newCommand.CommandValue);

			if (encSkill != null){
				if (encSkill.SkillType == SkillTypes.NormalOffense || encSkill.SkillType == SkillTypes.UltimateOffense){
                    if (encSkill.Description == "1")
						enc.BattleStateCondition = BattleStateCondition.EnemySkillFirst;
                    else if (encSkill.Description == "2")
						enc.BattleStateCondition = BattleStateCondition.EnemySkillSecond;
				}
				else {

				}
			}
			else{
				Debug.Log ("NULL SKILL");
			}
			battleController.SetAICommand(enc, newCommand);
			battleController.AddSelectedTarget(enc, targetList);
		}
		else {
			EnemyGuard(enc);
		}
		
		battleController.collectCommand(enc);
	}
	#endregion

	#region guard no action
	public void EnemyGuard(Battler enc){
		Act = 1;
		enc.BattleTurn = 0;
		commandData = _AICommandList.Find (x => x.CommandType == (int)CommandTypes.Guard);
		battleController.SetAICommand(enc, commandData);
	}
    #endregion

    #region charging "something" no action
    public void EnemyCharge(Battler enc){
        CommandData newCommand = new CommandData ();
        Act = 1;
        enc.BattleTurn = 3;
        commandData = _AICommandList.Find (x => x.CommandType == (int)CommandTypes.Charging);
        newCommand = CommandData.Copy (commandData);

        battleController.SetAICommand(enc, newCommand);
        battleController.collectCommand(enc);
    }
    #endregion
}
