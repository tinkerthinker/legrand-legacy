﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using Legrand.core;


[System.Serializable]
public class CollectDamage {
	public int IDAttacker;
	public Battler Attacker;
	public int IDTarget;
	public Battler Target;
	public float ReceiveDamage;
}

