﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using Legrand.core;

[System.Serializable]
public class EncounterData {
	public Battler Attacker;
	public float ReceiveDamage;
}
[System.Serializable]
public class EncounterDataAlly {
	public Battler Attacker;
	public float ReceiveDamage;
}
[System.Serializable]
public class AttackDataTemp {
	public Battler Target;
	public float AttackDamage;
}



public class AIBehaviour {

	private List<EncounterData> CounterData = new List<EncounterData>();
	private List<EncounterDataAlly> _CounterDataAlly = new List<EncounterDataAlly>();

	private List<AttackDataTemp> _attackData = new List<AttackDataTemp>();

	private EncounterData _CounterData;
	private EncounterDataAlly _counterDataAlly;
	private AttackDataTemp _AttackData;
	private Battle _Battle;

	private BattleController _BattleController;

	private Encounter encounter;
	private BattleAICommand _BattleAICommand;

//	private List<Battler> CharList = new List<Battler>();
	public List<Battler> TargetList = new List<Battler>();

	private List<Battler> _EncounterList = new List<Battler>();

	private void InitBehaviour (){
		_Battle 			= Battle.Instance;
		_BattleController	= _Battle.Controller;
		_BattleAICommand	= _Battle.battleAICommand;
		//		CharList			= _Battle.charList;
		_EncounterList		= _Battle.encounterList;
		
		TargetList 			= new List<Battler>();

	}

	public void FilteringTarget (Battler battler, List<Battler> targets){
		InitBehaviour();
		encounter = (battler.Character as Encounter);
		int rand = 0;

        for (int i = 0; i < targets.Count; i++){//each (Battler bt in targets){
            if (targets[i].BattlerState != BattlerState.Dead){
                TargetList.Add (targets[i]);
			}
		}

		switch (encounter.attackTarget){
        	case AttackTarget.RANDOM_TARGET:
        		List <Battler> selected = new List<Battler>();
                    for (int i =0;i<TargetList.Count; i++){ //each (Battler target in TargetList) {
                        if (_BattleAICommand.TController.canAttack (battler, TargetList[i])) {
                            selected.Add(TargetList[i]);
        			}
        		}
        		rand = LegrandUtility.Random(0, selected.Count);
        		_BattleAICommand.SelectedTargets.Add(selected[rand]);

    		break;
        	case AttackTarget.SELF_TARGET:
        		_BattleAICommand.SelectedTargets.Add (battler);
    		break;
        	case AttackTarget.LOWEST_CURRENT_HEALTH:
        		TargetList.Sort(AscendingCurrentHealth);
        		HighestLowest(battler);
    		break;
            case AttackTarget.COLOUMN_TARGET:
                rand = LegrandUtility.Random(0, TargetList.Count);
                List <Battler> Coloumn = GetColoumnTargets(TargetList[rand], TargetList);
                if (Coloumn.Count > 0)
                {
                    for (int i = 0; i < Coloumn.Count; i++) //each (Battler b in Coloumn)
                    {
                        if (Coloumn[i].BattlerState != BattlerState.Dead)
                            _BattleAICommand.SelectedTargets.Add(Coloumn[i]);
                    }
                }
    		break;
            case AttackTarget.ROW_TARGET:
                rand = LegrandUtility.Random(0, TargetList.Count);
                List <Battler> Row = GetRowTargets(TargetList[rand], TargetList);
                if (Row.Count > 0)
                {
                    for (int i = 0; i < Row.Count; i++)//each (Battler b in Row)
                    {
                        if (Row[i].BattlerState != BattlerState.Dead)
                            _BattleAICommand.SelectedTargets.Add(Row[i]);
                    }
                }
    		break;
            case AttackTarget.ALL_TARGET:
                for (int i =0;i<TargetList.Count;i++)//each (Battler b in TargetList)
                {
                    if (TargetList[i].BattlerState != BattlerState.Dead)
                        _BattleAICommand.SelectedTargets.Add(TargetList[i]);
                }
            break;
            case AttackTarget.NON_DEBUFFED_TARGET:
                for (int q =0; q < _BattleAICommand.CharList.Count; q++){ //each (Battler enemy in _BattleAICommand.CharList) {
                    Battler enemy = _BattleAICommand.CharList[q];

                    if (enemy.Targeted.Any(x=>x.Character._ID == battler.Character._ID)){//Contains(enc)){
                        Debug.Log("Yuhuu");
                    }
                    else{
                        Debug.Log("Awawa");
                    }
                    if (enemy.BuffDebuffStat.Any (x=>x.StatusType != StatusType.Debuff) && enemy.BattlerState != BattlerState.Dead && enemy.Targeted.Count == 0){
                        _BattleAICommand.SelectedTargets.Add(enemy);
                        enemy.Targeted.Add(battler);
                        Debug.Log (enemy);
                    }
                    else{
                        if (enemy.BuffDebuffStat.Any (x=>x.StatusType != StatusType.Debuff) && enemy.Targeted.Any(x=>x.Character._ID != battler.Character._ID)){
                            _BattleAICommand.SelectedTargets.Add(enemy);
                            enemy.Targeted.Add(battler);
                        }
                    }
                }
                break;
		}
	}

    public List <Battler>  GetRowTargets (Battler target, List<Battler> targets){
        return targets.FindAll(x => x.BattlePosition.y.Equals(target.BattlePosition.y) && x.BattlerState != BattlerState.Dead);
	}
    public List <Battler>  GetColoumnTargets (Battler target, List<Battler> targets){
        return targets.FindAll(x => x.BattlePosition.x.Equals(target.BattlePosition.x) && x.BattlerState != BattlerState.Dead);
	}
	public void MyDamage (Battler enc, bool isAlly){
		InitBehaviour();

		encounter 				= (enc.Character as Encounter);

        for (int i=0;i<_BattleAICommand.CharList.Count;i++)//each (Battler bt in _BattleAICommand.CharList)
        {
            TargetList.Add(_BattleAICommand.CharList[i]);
        }


		switch (encounter.attackTarget){
		    case AttackTarget.RANDOM_TARGET:
                for (int i=0; i<_BattleAICommand.CharList.Count;i++){ //each (Battler charTarget in _BattleAICommand.CharList) {
                    if (_BattleAICommand.TController.canAttack (enc, _BattleAICommand.CharList[i])) {
                        _BattleAICommand.SelectedTargets.Add(_BattleAICommand.CharList[i]);
    				}
			    }
			break;
		    case AttackTarget.UNDER_50:
                for (int i=0; i<_BattleAICommand.CharList.Count;i++) { //each (Battler charTarget in _BattleAICommand.CharList) {
                    Battler charTarget = _BattleAICommand.CharList[i];
				    if (_BattleAICommand.TController.canAttack (enc, charTarget) && charTarget.Health.Value <= charTarget.Health.MaxValue/2) {
					    _BattleAICommand.SelectedTargets.Add(charTarget);
				    }
			    }
			break;
		case AttackTarget.OVER_50:
                for (int i=0; i<_BattleAICommand.CharList.Count;i++){//each (Battler charTarget in _BattleAICommand.CharList) {
                    Battler charTarget = _BattleAICommand.CharList[i];
				    if (_BattleAICommand.TController.canAttack (enc, charTarget) && charTarget.Health.Value >= charTarget.Health.MaxValue/2) {
					    _BattleAICommand.SelectedTargets.Add(charTarget);
				    }
			    }
			break;
		case AttackTarget.HEALER:
                for (int i = 0; i < _EncounterList.Count; i++){ //each (Battler allyTarget in _EncounterList){
                    Battler allyTarget = _EncounterList[i];
				    if (allyTarget.Health.Value <= allyTarget.Health.MaxValue/2 && allyTarget.BattlerState != BattlerState.Dead){
					    _BattleAICommand.SelectedTargets.Add(allyTarget);
                        TargetList.Add(allyTarget);
				    }
			    }
			break;
		case AttackTarget.LOWEST_CURRENT_HEALTH:
			
			TargetList.Sort(AscendingCurrentHealth);
			
			break;
		case AttackTarget.HIGHEST_CURRENT_HEALTH:
			
			TargetList.Sort(DescendingCurrentHealth);
			
			break;
		case AttackTarget.DEBUFFED_TARGET:
			foreach (Battler charTarget in _BattleAICommand.CharList) {
				foreach (Buff statusBuff in charTarget.BuffDebuffStat){
					if (statusBuff.StatusType == StatusType.Debuff){
						_BattleAICommand.SelectedTargets.Add(charTarget);
					}
				}
			}
			break;
		case AttackTarget.NON_BUFFED_ALLY:
			foreach (Battler allyTarget in _EncounterList) {
				if (allyTarget.BuffDebuffStat.Count == 0){
					_BattleAICommand.SelectedTargets.Add(allyTarget);
					Debug.Log (allyTarget);
				}
				else{
					int i = 0;
					foreach (Buff statusBuff in allyTarget.BuffDebuffStat){
						if (statusBuff.StatusType == StatusType.Buff ){
							i++;
						}
					}

					if (i == 0) _BattleAICommand.SelectedTargets.Add(allyTarget);
				}
			}
			break;
		case AttackTarget.NON_BUFFED_TARGET:
                for (int x=0;x< _BattleAICommand.CharList.Count;x++)
                {// (Battler enemy in _BattleAICommand.CharList) {
                    Battler enemy = _BattleAICommand.CharList[x];
    				if (enemy.BuffDebuffStat.Count == 0){
    					_BattleAICommand.SelectedTargets.Add(enemy);
    					Debug.Log (enemy);
				    } else {
    					int i = 0;
    					foreach (Buff statusBuff in enemy.BuffDebuffStat){
                            if (statusBuff.StatusType == StatusType.Debuff ){
    						i++;
    					}
				    }
				    if (i == 0) _BattleAICommand.SelectedTargets.Add(enemy);
				    }
			    }
            break;
        case AttackTarget.NON_DEBUFFED_TARGET:
                for (int q =0; q < _BattleAICommand.CharList.Count; q++){ //each (Battler enemy in _BattleAICommand.CharList) {
                    Battler enemy = _BattleAICommand.CharList[q];

                    if (enemy.Targeted.Any(x=>x.Character._ID == enc.Character._ID)){//Contains(enc)){
//                        Debug.Log("Yuhuu");
                    }
                    else{
//                        Debug.Log("Awawa");
                    }
                    if (enemy.BuffDebuffStat.Any (x=>x.StatusType != StatusType.Debuff) && enemy.BattlerState != BattlerState.Dead && enemy.Targeted.Count == 0){
                        _BattleAICommand.SelectedTargets.Add(enemy);
                        enemy.Targeted.Add(enc);
                        Debug.Log (enemy);
                    }
                    else{
                        if (enemy.BuffDebuffStat.Any (x=>x.StatusType != StatusType.Debuff) && enemy.Targeted.Any(x=>x.Character._ID != enc.Character._ID)){
                            _BattleAICommand.SelectedTargets.Add(enemy);
                            enemy.Targeted.Add(enc);
                        }
                            
//                        for (int y=0;y<enemy.BuffDebuffStat.Count;y++){ //each (Buff statusBuff in enemy.BuffDebuffStat){
//                            Buff statusBuff = enemy.BuffDebuffStat[y];
//                            if (statusBuff.StatusType != StatusType.Debuff && enemy.BattlerState != BattlerState.Dead){
//                                _BattleAICommand.SelectedTargets.Add(enemy);
//                            }
//                        }

                    //if (i == 0) _BattleAICommand.SelectedTargets.Add(enemy);
                }
            }
            break;
		}
	}
	#region Set AI Data List
//	void ByDamage (Battler enc){
//		foreach (CollectDamage cd in _collectDamage){
//			if (enc == cd.Target){
//				_CounterData = new EncounterData();
//				_CounterData.Attacker = (cd.Attacker);
//				_CounterData.ReceiveDamage = (cd.ReceiveDamage);
//				CounterData.Add (_CounterData);
//				encounter.counterData = CounterData;
//			}
//		}
//	}
//
//	public void ByAttack (Battler battler){
//		_collectDamage			= Battle.Instance.Controller.collectDamage;
//		foreach (CollectDamage cd in _collectDamage){
//			if (battler.BattleID == cd.IDAttacker){
//				_AttackData = new AttackDataTemp();
//				_AttackData.Target = (cd.Target);
//				_AttackData.AttackDamage = (cd.ReceiveDamage);
//				_attackData.Add (_AttackData);
////				battler.AttackData = new List<AttackDataTemp>();
////				battler.AttackData = _attackData;
////				encounter.attackData = _attackData;
//			}
//		}
//	}
//	public void ByDamageAlly(Battler enc){
//		foreach (Battler battler in _EncounterList){
//			foreach (CollectDamage cd in _collectDamage){
//				if (battler == cd.Target){
//					_counterDataAlly = new EncounterDataAlly();
//					_counterDataAlly.Attacker = (cd.Attacker);
//					_counterDataAlly.ReceiveDamage = (cd.ReceiveDamage);
//					_CounterDataAlly.Add (_counterDataAlly);
//					encounter.counterDataAlly = _CounterDataAlly;
//				}
//			}
//		}
//	}
	#endregion

	#region Highest and Lowest Target Var
	void HighestLowest (Battler enc){
		bool canAttack = false;
		int i = 0;
		while (!canAttack){
			if (_BattleAICommand.TController.canAttack (enc, _BattleAICommand.AiBehaviour.TargetList[i])) {
				_BattleAICommand.SelectedTargets.Add(_BattleAICommand.AiBehaviour.TargetList[i]);
				canAttack = true;

//				_BattleAICommand.SendTarget(enc, _BattleAICommand.TargetDecision.SelectedTargets[0]);
			}
			else{
				i++;
				if (i >= _BattleAICommand.AiBehaviour.TargetList.Count-1){
					canAttack = true;
					encounter.attackTarget = AttackTarget.RANDOM_TARGET;
					FilteringTarget(enc, TargetList);
				}
			}
		}
	}
	#endregion

	#region Sorting List
	static int DescendingHealth (Battler l1, Battler l2){
		return (int)l2.Health.MaxValue - (int)l1.Health.MaxValue;
	}
	static int AscendingHealth (Battler l1, Battler l2){
		return (int)l1.Health.MaxValue - (int)l2.Health.MaxValue;
	}
	static int DescendingCurrentHealth (Battler l1, Battler l2){
		return (int)l2.Health.Value - (int)l1.Health.Value;
	}
	static int AscendingCurrentHealth (Battler l1, Battler l2){
		return (int)l1.Health.Value - (int)l2.Health.Value;
	}
	#endregion
}
