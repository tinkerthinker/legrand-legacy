﻿using UnityEngine;
using System.Collections;

public enum EnemyType{
	Attacker,
	Magic,
	Debuffer,
	Buffer,
	Healer,
	MiniBoss,
	BossUnique,
    NormalUnique
}
[System.Serializable]
public enum EncounterSize {
	Small,
	Medium,
	Large,
	Massive
}


//on progress
public enum AttackTarget {
	RANDOM_TARGET,
	UNDER_50,
	OVER_50,
	LOWEST_CURRENT_HEALTH,
	HIGHEST_CURRENT_HEALTH,
	DEBUFFED_TARGET,
	NON_BUFFED_ALLY,
	NON_BUFFED_TARGET,
	HEALER,
	SELF_TARGET,
	SPECIFIC,
	ROW_TARGET,
	COLOUMN_TARGET,
	ALL_TARGET,
    NON_DEBUFFED_TARGET
}

//coming soon
public enum HealerTarget {
	BY_MIN_DAMAGE,
	BY_MAX_DAMAGE,
	BY_MAX_HEALTH,
	BY_MIN_HEALTH,
	BY_MAX_CURRENT_HEALTH,
	BY_MIN_CURRENT_HEALTH,
	BY_MAX_ATTACK,
	BY_MIN_ATTACK,
	BY_MAX_DEFENSE,
	BY_MIN_DEFENSE
}

public enum BossBattler {
    none,
    GladiatorBattler,
    KlahmaranBattler,
    ArkworaBattler,
    RogarBattler,
    LyonBattler,
    SirCallahanBattler
}