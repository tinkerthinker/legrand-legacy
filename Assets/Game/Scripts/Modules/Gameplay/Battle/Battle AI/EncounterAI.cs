﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;

[System.Serializable]
public class EncounterAI{
	public int Chance;
	public CommandTypes Action;
	public int CommandValues = 1;
	public AttackTarget TargetCondition;
	public TargetFormation TargetType;
	public TargetType FormationType;
}
[System.Serializable]
public class ConditionEncounter {
//	public Attribute AttributeCondition;
	[Header("Health Value, + => over value, - => below value")]
	public float value;
}

