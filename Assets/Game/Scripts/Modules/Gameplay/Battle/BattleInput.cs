﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TeamUtility.IO;
using Legrand.GameSettings;

public enum BattleInputKey{
	UP 		= 0,
	RIGHT 	= 1,
	DOWN	= 2,
	LEFT 	= 3,
	CONFIRM = 4,
	CANCEL 	= 5,
	PAUSE 	= 6
}

public class BattleInput : MonoBehaviour
{
    public bool isTimeHit = false;

	private bool onListen = false, onAxis = false;
	public bool OnListen 
	{
		get{return onListen;}
	}
	private BattleInputKey inputKey;

	public delegate void OnRequestInput(int key);
	public event OnRequestInput onReqInput;

	public void startListen(){
		onListen = true;
	}
	public void stopListen(){
		onListen = false;
	}

	void Awake()
	{
		if(EventManager.Instance)
			EventManager.Instance.AddListener<ChangeGameStateEvent>(ChangeGameStateHandler);
	}

	void OnDestroy()
	{
		if(EventManager.Instance)
			EventManager.Instance.RemoveListener<ChangeGameStateEvent>(ChangeGameStateHandler);
	}

	void ChangeGameStateHandler(ChangeGameStateEvent p)
	{
        if (GlobalGameStatus.Instance.IsStateNormal())
            startListen ();
        else
            stopListen ();
	}

    private void getInput()
    {
        if (isTimeHit)
        {
            if (InputManager.GetButtonDown("Pause"))
            {
                sendKey((int)BattleInputKey.PAUSE);
            } 
            if (InputManager.GetButtonDown("Confirm"))
            {
                sendKey((int)BattleInputKey.CONFIRM);
            }
            if (InputManager.GetButtonDown("Cancel"))
            {
                sendKey((int)BattleInputKey.CANCEL);
            }
            if (!onAxis && GameplayInputManager.Instance.GetButton(ControlType.Forward, ControllerFor.Battle))
            {
                onAxis = true;
                sendKey((int)BattleInputKey.UP);
            }
            if (!onAxis && GameplayInputManager.Instance.GetButton(ControlType.Backward, ControllerFor.Battle))
            {
                onAxis = true;
                sendKey((int)BattleInputKey.DOWN);
            }
            if (!onAxis && GameplayInputManager.Instance.GetButton(ControlType.Left, ControllerFor.Battle))
            {
                onAxis = true;
                sendKey((int)BattleInputKey.LEFT);
            }
            if (!onAxis && GameplayInputManager.Instance.GetButton(ControlType.Right, ControllerFor.Battle))
            {
                onAxis = true;
                sendKey((int)BattleInputKey.RIGHT);
            }

        }
        else
        {
            float hm = InputManager.GetAxis ("Horizontal");
            float vm = InputManager.GetAxis ("Vertical");

            float ha = InputManager.GetAxis ("AltHorizontal");
            float va = InputManager.GetAxis ("AltVertical");

            float axHor = (Mathf.Abs(hm) > Mathf.Abs(ha)) ? hm : ha;
            float axVer = (Mathf.Abs(vm) > Mathf.Abs(va)) ? vm : va;

            if (axVer < 0.25f && axVer > -0.25f)
                axVer = 0f;
            if (axHor < 0.25f && axHor > -0.25f)
                axHor = 0f;

            if (InputManager.GetButtonDown("Pause"))
            {
                sendKey((int)BattleInputKey.PAUSE);
            } 
            if (InputManager.GetButtonDown("Confirm"))
            {
                sendKey((int)BattleInputKey.CONFIRM);
            }
            if (InputManager.GetButtonDown("Cancel"))
            {
                sendKey((int)BattleInputKey.CANCEL);
            }
            if (axVer != 0f && !onAxis)
            {
                onAxis = true;

                if (axVer > 0.25f)
                    sendKey((int)BattleInputKey.UP);
                else if (axVer < -0.25f)
                    sendKey((int)BattleInputKey.DOWN);
            }
            if (axHor != 0f && !onAxis)
            {
                onAxis = true;

                if (axHor > 0.25f)
                    sendKey((int)BattleInputKey.RIGHT);
                else if (axHor < -0.25f)
                    sendKey((int)BattleInputKey.LEFT);
            }
            if (axHor == 0 && axVer == 0)
            {
                onAxis = false;
            }
        }
	}

	void sendKey(int key){
		if (onReqInput != null) {
			onReqInput(key);
		}
	}

    void Update () {
        if (onListen && GlobalGameStatus.Instance.StateStatus[(int)GlobalGameStatus.GameState.Pause] == false) {
            getInput();
        }
    }
}
