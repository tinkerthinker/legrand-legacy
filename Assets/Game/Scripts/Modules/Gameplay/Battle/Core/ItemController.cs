using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;

public class ItemController : MonoBehaviour
{
    public static ItemController _Instance;

    void Awake()
    {
        _Instance = this;
    }
    //	private BattleFormula BFormula = new BattleFormula();
    private AIBehaviour _AIBehaviour = new AIBehaviour();

    public void InitItem(ConsumableMagic magicItem, Battler target, Battler source)
    {
        // false if can't stack
        switch (magicItem.TargetFormation)
        {
            //debuff
            case TargetFormation.Ally:
                SendAllyBuff(target, magicItem);
                break;
            case TargetFormation.Enemy:
                SendTargetDebuff(target, magicItem);
                break;
        }
    }
    public void CheckBuffCondition(Battler battler)
    {
        List<Buff> buffTemp = new List<Buff>();
        battler.DisableBuffBlink();
        bool statChanged = false;
        for (int i = 0; i < battler.BuffDebuffStat.Count; i++)
        { // each (Buff buff in battler.BuffDebuffStat){
            Buff buff = battler.BuffDebuffStat[i];
            buff.Turns -= 1;

            if (buff.Turns <= 0)
            {
                buffTemp.Add(buff);
                bool tempStatChanged;
                RemoveBuffAction(battler, buff, out tempStatChanged);
                if (tempStatChanged) statChanged = true;


            }
        }

        for (int i = 0; i < buffTemp.Count; i++)
        {//each (Buff buff in buffTemp){
            battler.BuffDebuffStat.Remove(buffTemp[i]);
        }

        if (statChanged) battler.RecalculateAttribute();

        if (battler.BuffDebuffStat.Count == 0)
        {
            battler.DisableBuffBlink();
        }
        else
        {
            battler.CheckBuffBlink();
        }
    }

    //Send buff status buff to target
    private void SendAllyBuff(Battler target, ConsumableMagic magicItem)
    {
        Buff buff = Buff.Copy(magicItem.Buff);
        SendTargetBuffDebuff(target, buff);
    }

    private void SendTargetDebuff(Battler target, ConsumableMagic magicItem)
    {
        Buff buff = Buff.Copy(magicItem.Buff);
        SendTargetDebuff(target, buff, Mathf.Clamp(100f - Formula.GetStatusResistance(target.Character, target.PrimaryBuff, target.SecondaryBuff), 0f, 100f));
    }

    public void SetMagicSkillDebuff(Battler target, Buff buff, float chance)
    {
        Buff debuff = Buff.Copy(buff);
        SendTargetDebuff(target, debuff, chance, false);
    }

    public void SetGuardBuff(Battler target, Buff buff)
    {
        Buff buffGuard = Buff.Copy(buff);
        SendTargetBuffDebuff(target, buff);
    }

    #region CommandBuff
    // only for magic attack & skill has a buff : karena dipakai berulang2 n g perlu nampilin resist pop up kecuali kalau kena
    public void SendTargetDebuff(Battler target, Buff buff, float chance, bool isPopUp = true)
    {
        if (target.BattlerState != BattlerState.Dead)
        {
            if (target.BattlerType == BattlerType.Enemy)
            {
                Encounter encounter = target.Character as Encounter;
                if (buff.StatusType == StatusType.Debuff && encounter.ResistDebuff && !encounter.ResistException.Exists(x => x == buff._MetaId))
                {
                    if (isPopUp)
                        target.PopUp("RESIST", (int)target.BattlerType, false);
                    return;
                }
            }

            if (LegrandUtility.Random(1, 100f) > (int)chance)
            {
                if (isPopUp)
                    target.PopUp("RESIST", (int)target.BattlerType, false);
                return;
            }

            SendTargetBuffDebuff(target, buff);
        }
    }
    //normal skill send to enemy / guard effect to enemy : perlu ditampilin resist pop up karena tujuanya cuman debuff n bukan damage
    public void SendTargetBuffDebuff(Battler target, Buff buff)
    {
        bool statChanged = false;
        bool removeBuffIsStatChanged = false;

        if (buff.StatusType == StatusType.Debuff && target.BattlerType == BattlerType.Enemy)
        {
            Encounter encounter = target.Character as Encounter;
            if (encounter.ResistDebuff && !encounter.ResistException.Exists(x => x == buff._MetaId))
            {
                target.PopUp("RESIST", (int)target.BattlerType, false);
                return;
            }
        }

        Buff existedBuff = target.BuffDebuffStat.Find(x => x._ID == buff._ID);
        if (existedBuff != null)
        {
            RemoveBuffAction(target, buff, out removeBuffIsStatChanged);
            target.BuffDebuffStat.Remove(existedBuff);
        }

        if (buff.Turns > 0)
        {
            target.BuffDebuffStat.Add(buff);
            SearchBuffAction(target, buff, out statChanged);
        }

        if (statChanged || removeBuffIsStatChanged) target.RecalculateAttribute();
    }

    //for defensive skill (target only self (character)
    public void SendTargetBuffDebuff(Battler target, List<Buff> listBuff)
    {
        List<Buff> buffs = new List<Buff>();
        for (int i = 0; i < listBuff.Count; i++)
        {
            Buff buff = Buff.Copy(listBuff[i]);
            buffs.Add(buff);
        }

        bool statChanged = false;
        bool removeBuffIsStatChanged = false;
        for (int i = 0; i < buffs.Count; i++)
        {
            Buff buff = buffs[i];
            Buff existedBuff = target.BuffDebuffStat.Find(x => x._ID == buff._ID);
            if (existedBuff != null)
            {
                RemoveBuffAction(target, buff, out removeBuffIsStatChanged);
                target.BuffDebuffStat.Remove(existedBuff);
            }

            if (buff.Turns < 1)
                buff.Turns = 2;

            target.BuffDebuffStat.Add(buff);
            SearchBuffAction(target, buff, out statChanged);
        }

        if (statChanged || removeBuffIsStatChanged) target.RecalculateAttribute();
    }
    #endregion

    public void SearchBuffAction(Battler target, Buff buff, out bool statChange)
    {
        statChange = false;
        target.DisableBuffBlink();
        switch (buff.Type)
        {
            case (BuffType.ACTION):
                for (int i = 0; i < buff.ActionsBuffEnum.Count; i++)
                {//each(CommandTypes command in buff.ActionsBuffEnum) 
                    CommandTypes command = buff.ActionsBuffEnum[i];
                    ExecItemOnTurnStart(target, command);
                }
                break;
            case (BuffType.OVER_TIME): // REGEN & POISON
                if (buff.StatusBuff == Status.Regen)
                {
                    target.PopUp(buff.Name, (int)BattleTextPopUp.BattleTextType.Buff, false);
                    target.AddBuffSprite(buff.StatusBuff.ToString(), "");
                }
                else
                {
                    target.PopUp(buff.Name, (int)BattleTextPopUp.BattleTextType.Debuff, true);
                    target.AddBuffSprite(buff.StatusBuff.ToString(), "");
                }
                break;
            case (BuffType.ACTION_SKIP): //paralyze
                ItemActionSkip(target, buff.value);
                //                target.PopUp("Debuff", (int)BattleTextPopUp.BattleTextType.Debuff, false);
                target.AddBuffSprite(buff.Type.ToString(), "");

                break;
            case (BuffType.REFLECT):
                target.ReflectPercentage = buff.value;
                target.PopUp("Reflect", (int)BattleTextPopUp.BattleTextType.Buff, false);
                target.AddBuffSprite("REFLECT", ""); break;
            case (BuffType.VAMPIRE): //drain
                target.DrainPercentage = buff.value;
                target.PopUp(buff.Name, (int)BattleTextPopUp.BattleTextType.Buff, false);
                target.AddBuffSprite(buff.Type.ToString(), "");
                break;
            case (BuffType.INVINCIBLE): 
                target.NoMagicCost = true; 
                target.AddBuffSprite(buff.Type.ToString(), ""); break;
            case (BuffType.RANDOM_TARGET):
                target.ConfuseChance = buff.value;
                target.PopUp(buff.Name, (int)BattleTextPopUp.BattleTextType.Debuff, false);
                target.AddBuffSprite(buff.Type.ToString(), "");
                break;
            case (BuffType.BASIC_PRIMARY):
                for (int i = 0; i < buff.BuffPrimary.Count; i++)//each(AttributeValue attributeBuff in buff.BuffPrimary)
                {
                    AttributeValue attributeBuff = buff.BuffPrimary[i];
                    string buffText = "";
                    BattleTextPopUp.BattleTextType buffTextType = BattleTextPopUp.BattleTextType.Buff;
                    if (attributeBuff.value > 0)
                    {
                        statChange = true;
                        buffText = " UP";
                        buffTextType = BattleTextPopUp.BattleTextType.Buff;
                    }
                    else if (attributeBuff.value < 0)
                    {
                        statChange = true;
                        buffText = " DOWN";
                        buffTextType = BattleTextPopUp.BattleTextType.Debuff;
                    }
                    target.PopUp(attributeBuff.AttributeType + buffText, (int)buffTextType, false);

                    if (!target.PrimaryBuff.ContainsKey(attributeBuff.AttributeType))
                        target.PrimaryBuff.Add(attributeBuff.AttributeType, attributeBuff.value);
                    else
                        target.PrimaryBuff[attributeBuff.AttributeType] = ((float)target.PrimaryBuff[attributeBuff.AttributeType]) + attributeBuff.value;

                    if (target.PrimaryBuff[attributeBuff.AttributeType] > 0f) target.AddBuffSprite(attributeBuff.AttributeType.ToString() + "+", "UP");
                    else target.AddBuffSprite(attributeBuff.AttributeType.ToString() + "-", "DOWN");
                }


                break;
            case (BuffType.BASIC_SECONDARY):
                for (int i = 0; i < buff.BuffSecondary.Count; i++)// each (AttributeValue attributeBuff in buff.BuffSecondary)
                {
                    AttributeValue attributeBuff = buff.BuffSecondary[i];
                    string buffText = "";
                    BattleTextPopUp.BattleTextType buffTextType = BattleTextPopUp.BattleTextType.Buff;
                    if (attributeBuff.value > 0)
                    {
                        buffText = " UP";
                        buffTextType = BattleTextPopUp.BattleTextType.Buff;
                    }
                    else if (attributeBuff.value < 0)
                    {
                        buffText = " DOWN";
                        buffTextType = BattleTextPopUp.BattleTextType.Debuff;
                    }

                    if (attributeBuff.AttributeType != Attribute.StatusResistance)
                    {
                        target.PopUp(attributeBuff.AttributeType + buffText, (int)buffTextType, false);

                        if (!target.SecondaryBuff.ContainsKey(attributeBuff.AttributeType))
                            target.SecondaryBuff.Add(attributeBuff.AttributeType, attributeBuff.value);
                        else
                            target.SecondaryBuff[attributeBuff.AttributeType] = ((float)target.SecondaryBuff[attributeBuff.AttributeType]) + attributeBuff.value;

                        if (target.SecondaryBuff[attributeBuff.AttributeType] > 0f)
                            target.AddBuffSprite(attributeBuff.AttributeType.ToString() + "+", "UP");
                        else
                            target.AddBuffSprite(attributeBuff.AttributeType.ToString() + "-", "DOWN");
                    }
                    else
                    {
                        target.PopUp("Miss Cleanse", (int)buffTextType, false);
                    }
                }
                break;
            case (BuffType.HEAL):
                if (target.BattlerState == BattlerState.Guard)
                { 
                    if (target.BattlerType == BattlerType.Player)
                    {
                        GuardBuff _GuardBuff = (target.Character as MainCharacter).GuardSkill;
                        if (LegrandUtility.Random(0, 100) <_GuardBuff.BuffPercentage)
                        {
                            switch (_GuardBuff.Target)
                            {
                                case TargetType.Single:
                                    target.PopUp(target.Health.IncreaseByPercentage(buff.value).ToString(), (int)BattleTextPopUp.BattleTextType.Heal);
                                    break;
                                case TargetType.All:
                                    for (int i = 0; i < Battle.Instance.charList.Count; i++)
                                    {
                                        Battler bat = Battle.Instance.charList[i];
                                        if (bat.BattlerState != BattlerState.Dead)
                                            bat.PopUp(bat.Health.IncreaseByPercentage(buff.value).ToString(), (int)BattleTextPopUp.BattleTextType.Heal);
                                    }
                                    break;
                            }
                        }
                    }
                }
                else
                    target.PopUp(target.Health.IncreaseByPercentage(buff.value).ToString(), (int)BattleTextPopUp.BattleTextType.Heal);
                break;
            case (BuffType.DEATH):
                Health battlerHealth = target.Character.GetHealth();
                battlerHealth.Decrease(battlerHealth.MaxValue);
                target.PopUp("DEATH", (int)target.BattlerType);
                break;
            case (BuffType.ACTION_POINT):
                target.PopUp(buff.Name, (int)BattleTextPopUp.BattleTextType.Buff, false);
                target.APDrain = buff.value;
                break;
            case (BuffType.REFLECT_HEAL):
                target.DrainPercentage = buff.value;
                target.PopUp(buff.Name, (int)BattleTextPopUp.BattleTextType.Buff, false);
                break;
            case (BuffType.PROVOKE):
                
                break;
        }

        if (target.BattlerType == BattlerType.Player)
        {
            target.CheckBuffBlink();
        }
    }

    private void RemoveBuffAction(Battler target, Buff buff, out bool statChanged)
    {
        statChanged = false;
        target.DisableBuffBlink();
        switch (buff.Type)
        {
            case (BuffType.ACTION):
                for (int i = 0; i < buff.ActionsBuffEnum.Count; i++)//each (CommandTypes command in buff.ActionsBuffEnum)
                    RemoveItemOnTurnStart(target, buff.ActionsBuffEnum[i]);
                break;
            case (BuffType.OVER_TIME): // REGEN & POISON
                    target.RemoveBuffSprite(buff.StatusBuff.ToString()); break;
            case (BuffType.ACTION_SKIP): ItemActionSkip(target, 0f); target.RemoveBuffSprite(buff.Type.ToString()); break;
            case (BuffType.REFLECT): target.ReflectPercentage = 0f; target.RemoveBuffSprite("REFLECT"); break;
            case (BuffType.VAMPIRE): target.DrainPercentage = 0f; target.RemoveBuffSprite(buff.Type.ToString()); break;
            case (BuffType.INVINCIBLE): target.NoMagicCost = false; target.RemoveBuffSprite(buff.Type.ToString()); break;
            case (BuffType.RANDOM_TARGET):
                target.ConfuseChance = 0f;
                target.RemoveBuffSprite(buff.Type.ToString());
                target.isConfuse = false; break;
            case (BuffType.BASIC_PRIMARY):
                for (int i = 0; i < buff.BuffPrimary.Count; i++) //each(AttributeValue attributeBuff in buff.BuffPrimary)
                {
                    AttributeValue attributeBuff = buff.BuffPrimary[i];
                    if (attributeBuff.value != 0)
                        statChanged = true;

                    if (!target.PrimaryBuff.ContainsKey(attributeBuff.AttributeType))
                        target.PrimaryBuff.Add(attributeBuff.AttributeType, attributeBuff.value);

                    target.PrimaryBuff[attributeBuff.AttributeType] = ((float)target.PrimaryBuff[attributeBuff.AttributeType]) - attributeBuff.value;
                    if (target.PrimaryBuff[attributeBuff.AttributeType] == 0f)
                    {
                        target.PrimaryBuff.Remove(attributeBuff.AttributeType);
                        target.RemoveBuffSprite(attributeBuff.AttributeType.ToString());
                    }
                    else if (target.PrimaryBuff[attributeBuff.AttributeType] > 0f)
                        target.AddBuffSprite(attributeBuff.AttributeType.ToString() + "+", "UP");
                    else
                        target.AddBuffSprite(attributeBuff.AttributeType.ToString() + "-", "DOWN");
                }
                break;
            case (BuffType.BASIC_SECONDARY):
                for (int i = 0; i < buff.BuffSecondary.Count; i++)//each(AttributeValue attributeBuff in buff.BuffSecondary)
                {
                    AttributeValue attributeBuff = buff.BuffSecondary[i];
                    target.SecondaryBuff[attributeBuff.AttributeType] = ((float)target.SecondaryBuff[attributeBuff.AttributeType]) - attributeBuff.value;

                    if (target.SecondaryBuff[attributeBuff.AttributeType] == 0f)
                    {
                        target.SecondaryBuff.Remove(attributeBuff.AttributeType);
                        target.RemoveBuffSprite(attributeBuff.AttributeType.ToString());
                    }
                    else if (target.SecondaryBuff[attributeBuff.AttributeType] > 0f)
                        target.AddBuffSprite(attributeBuff.AttributeType.ToString() + "+", "UP");
                    else
                        target.AddBuffSprite(attributeBuff.AttributeType.ToString() + "-", "DOWN");
                }
                break;
            case (BuffType.ACTION_POINT):
                target.APDrain = 0;
                break;
            case (BuffType.REFLECT_HEAL):
                target.DrainPercentage = 0f;
                break;
            case (BuffType.PROVOKE):
                break;
        }

        if (target.BattlerType == BattlerType.Player)
            target.CheckBuffBlink();
    }

    //called on init battle controller (before command)
    public void ExecBuff(Battler battler, BuffEvent buffEvent)
    {
        for (int i = 0; i < battler.BuffDebuffStat.Count; i++)//each (Buff buff in battler.BuffDebuffStat)
        {
            Buff buff = battler.BuffDebuffStat[i];
            if (buffEvent == buff.EventType && battler.BattlerState != BattlerState.Dead)
            {
                switch (buff.Type)
                {
                    case BuffType.OVER_TIME:
                        float overTime = OverTimeCalc(battler, battler.Health.MaxValue, buff);
                        if (overTime > 0)
                        {
                            //regen
                            battler.Health.Increase(overTime);
                            battler.PopUp(Mathf.FloorToInt(overTime).ToString(), (int)BattleTextPopUp.BattleTextType.Heal);
                        }
                        else
                        {
                            //poison
                            overTime = Mathf.Abs(overTime);
                            battler.Health.Decrease(overTime);
                            battler.PopUp(Mathf.FloorToInt(overTime).ToString(), (int)BattleTextPopUp.BattleTextType.Poison);
                        }
                        break;
                }
            }
        }
    }
    public void SetTargets(Battler battler, TargetType targetType, TargetFormation targetForm){
    }
    //item reflect and drain
    public void ExecBuffOnHittedAndAttack(Battler battlerHitted, Battler battlerAttacker, float damage)
    {
        if (battlerHitted.ReflectPercentage > 0f)
        {
            float reflectDmg = damage * battlerHitted.ReflectPercentage / 100f;
            if (battlerHitted.BattlerType == BattlerType.Player && battlerHitted.BattlerState == BattlerState.Guard)
            {
                battlerAttacker.Health.Decrease(Mathf.Abs(reflectDmg));
                battlerAttacker.PopUp(Mathf.FloorToInt(reflectDmg).ToString(), (int)battlerHitted.BattlerType);
                battlerAttacker.OnHitShader();
            }
            else if (battlerHitted.BattlerState != BattlerState.Guard)
            {
                battlerAttacker.Health.Decrease(Mathf.Abs(reflectDmg));
                battlerAttacker.PopUp(Mathf.FloorToInt(reflectDmg).ToString(), (int)battlerHitted.BattlerType);
                battlerAttacker.OnHitShader();
            }
        }

        if (battlerAttacker.DrainPercentage > 0f && battlerAttacker.BattlerState != BattlerState.Guard)
        {
            float drainHp = damage * battlerAttacker.DrainPercentage / 100f;
            battlerAttacker.Health.Increase(drainHp);
            battlerAttacker.PopUp(Mathf.FloorToInt(drainHp).ToString(), (int)BattleTextPopUp.BattleTextType.Heal);
        }

        if (battlerHitted.DrainPercentage > 0  && battlerHitted.BattlerState == BattlerState.Guard)
        {
            float drainHp = 0;
            if (battlerHitted.BattlerType == BattlerType.Player)
            {
                GuardBuff _GuardBuff = (battlerHitted.Character as MainCharacter).GuardSkill;
                if (LegrandUtility.Random(0, 100) < _GuardBuff.BuffPercentage)
                {
                    switch (_GuardBuff.Target)
                    {
                        case TargetType.Single:
                            drainHp = damage * battlerHitted.DrainPercentage / 100f;
                            battlerHitted.Health.Increase(drainHp);
                            battlerHitted.PopUp(Mathf.FloorToInt(drainHp).ToString(), (int)BattleTextPopUp.BattleTextType.Heal);
                            break;
                        case TargetType.All:
                            for (int i = 0; i < Battle.Instance.charList.Count; i++)
                            {
                                Battler bat = Battle.Instance.charList[i];
                                if (bat.BattlerState != BattlerState.Dead)
                                {
                                    drainHp = damage * battlerHitted.DrainPercentage / 100f;
                                    bat.Health.Increase(drainHp);
                                    bat.PopUp(Mathf.FloorToInt(drainHp).ToString(), (int)BattleTextPopUp.BattleTextType.Heal);
                                }
                            }
                            break;
                    }
                }
            }
        }
        if (battlerHitted.APDrain > 0)
        {
            if (battlerHitted.BattlerType == BattlerType.Player && battlerHitted.BattlerState == BattlerState.Guard)
            {
                GuardBuff _GuardBuff = (battlerHitted.Character as MainCharacter).GuardSkill;
                if (_GuardBuff.BuffPercentage < LegrandUtility.Random(0, 100))
                {
                    switch (_GuardBuff.Target)
                    {
                        case TargetType.Single:
                            battlerHitted.GaugeCharge = (int)battlerHitted.APDrain;
                            battlerHitted.UpdateGauge((int)battlerHitted.APDrain);
                            break;
                        case TargetType.All:
                            for (int i = 0; i < Battle.Instance.charList.Count; i++)
                            {
                                Battler bat = Battle.Instance.charList[i];
                                if (bat.BattlerState != BattlerState.Dead)
                                    bat.GaugeCharge = bat.GaugeCharge + (int)battlerHitted.APDrain;
                            }
                            break;
                    }
                }
            }
        }

    }

    //check condition item paralyze
    public void ItemActionSkip(Battler battler, float chance)
    {
        battler.SkipChance = chance;

        if (battler.SkipChance > 0f)
        {
            if (LegrandUtility.Random(0f, 100f) < battler.SkipChance)
            {
                battler.IsSkip = true;
                if (battler.BattlerState != BattlerState.Dead)
                    battler.PopUp("Paralyzed", (int)BattleTextPopUp.BattleTextType.Debuff, false);
            }
            else
            {
                battler.IsSkip = false;
                //                battler.PopUp("Debuff", (int)BattleTextPopUp.BattleTextType.Player, false);
            }
        }
    }
    //item silence and fear
    public void ExecItemOnTurnStart(Battler battler, CommandTypes comTypes)
    {
        if (!battler.NegatedAction.Exists(x => x == comTypes))
        {
            battler.NegatedAction.Add(comTypes);
            battler.AddBuffSprite("NEGATED" + comTypes.ToString(), "");
        }
    }
    //item silence and fear
    public void RemoveItemOnTurnStart(Battler battler, CommandTypes comTypes)
    {
        battler.NegatedAction.Remove(comTypes);
        battler.RemoveBuffSprite("NEGATED" + comTypes.ToString());
    }
    //item cleanse
    public void ItemCleanseEffect(Battler target)
    {
        List<Buff> buffTemp = new List<Buff>();
        List <BuffImage> buffImages = new List<BuffImage>();
        //Prevent all bad status 
        for (int i = 0; i < target.BuffDebuffStat.Count; i++)
        { //each (Buff buff in target.BuffDebuffStat){
            Buff buff = target.BuffDebuffStat[i];
            if (buff.StatusType == StatusType.Debuff)
            {
                buffTemp.Add(buff);
                for (int j = 0 ; j < target.BuffSprite.Count; j++)
                {
                    BuffImage imageBuff = target.BuffSprite[i];
                    if (buff.Name == imageBuff.SourceKey)
                    {
                        buffImages.Add(imageBuff);
                    }
                }
            }
        }

        for (int i = 0; i < buffTemp.Count; i++)
            target.BuffDebuffStat.Remove(buffTemp[i]);
        for (int i = 0; i < buffImages.Count; i++)
        {
            target.BuffSprite.Remove(buffImages[i]);
            target._BuffBar.BuffImageList.Remove(buffImages[i]);
        }

        target.CheckBuffBlink();
    }

    #region calculation
    public float OverTimeCalc(Battler battler, float maxHealth, Buff buff)
    {
        float ovrTimeDamage = maxHealth * buff.value / 100f;
        return ovrTimeDamage;
    }
    #endregion

    #region Confuse
    public void CheckConfuseEffect(Battler battler)
    {
        int ranChance = LegrandUtility.Random(1, 100);

        if (battler.ConfuseChance > 0f && ranChance >= battler.ConfuseChance)
            battler.isConfuse = true;
        else
            battler.isConfuse = false;
    }

    private List<Battler> PossibleTarget(Battler battler)
    {
        List<Battler> battlerList = new List<Battler>();
        List<Battler> newTarget = new List<Battler>();
        for (int x = 0; x < Battle.Instance.battlerList.Count; x++)
        {//each (Battler bat in Battle.Instance.battlerList){
            Battler bat = Battle.Instance.battlerList[x];
            if (bat != battler && bat.BattlerState != BattlerState.Dead)
            {
                battlerList.Add(bat);
            }
        }
        int i = LegrandUtility.Random(0, battlerList.Count);
        newTarget.Add(battlerList[i]);
        return newTarget;
    }
    public BattleCommand ConfuseCommand(Battler bat)
    {
        BattleCommand batCommand = new BattleCommand();
        batCommand.attacker = bat;
        batCommand.commandTurn = 3;
        batCommand.target = new List<Battler>(PossibleTarget(bat));
        batCommand.targetPos = null;
        batCommand.isCasting = false;
        batCommand.batPartner = null;
        batCommand.commandData = newCommandData(bat);

        for (int i = 0; i < batCommand.target.Count; i++)
            batCommand.GridTarget.Add(batCommand.target[i].BattlePosition);

        return batCommand;
    }
    private CommandData newCommandData(Battler bat)
    {
        CommandData comData = new CommandData();
        comData.CharacterID = bat.Character._ID;
        comData.CommandName = "Attack Confuse";
        comData.PanelPos = 1;
        comData.SlotPos = 1;
        comData.CommandType = (int)CommandTypes.Attack;
        comData.CommandValue = 1;
        return comData;
    }
    #endregion

    #region exist buff
    public bool CheckExistedBuff (Battler target, Buff buff)
    {
        
        Buff isExist = target.BuffDebuffStat.Find(x => x._ID == buff._ID);
        if (isExist != null)
            return true;
        else
            return false;
    }
    #endregion
}
