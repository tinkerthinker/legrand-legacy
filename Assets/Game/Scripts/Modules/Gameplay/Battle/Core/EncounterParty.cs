﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class EncounterParty{
	public enum PartyTypes
	{
		Normal,
		MiniBoss,
		Boss,
		MassiveNormal,
		MassiveMiniBoss,
		MassiveBoss
	}

    public string _ID;
    public string Name;
    public PartyTypes partyType;

    [HideInInspector]
    private List<EncounterFormation> _Formations;
    public List<EncounterFormation> Formations
    {
        get { return _Formations; }
    }
    public static readonly Vector2 MaxVector = new Vector2(4,2);
    public static readonly int MaxFormation = 8;
    
    public EncounterParty()
    {
        _Formations  = new List<EncounterFormation>();
    }
    
    public int AddFormation(EncounterFormation encounterFormation)
    {
        if (encounterFormation.Position.x > MaxVector.x || encounterFormation.Position.y > MaxVector.y)
            return 2;
        //0 succeed; 1 exceed maximum;
        if (_Formations.Count == MaxFormation)
            return 1;
        else
        {
            _Formations.Add(encounterFormation);
            return 0;
        }
    }
	
}
