﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;

[System.Serializable]
public class BattleFormation
{
    public Character Character;
    public Vector2 Position;

    public BattleFormation(Character character, Vector2 position)
    {
        this.Character = character;
        this.Position = position;
    }
}

[System.Serializable]
public class BattleFormations
{
    [SerializeField]
    private List<BattleFormation> _Formations;
    public List<BattleFormation> Formations
    {
        get { return _Formations; }
    }
    public static readonly Vector2 MaxVector = new Vector2(4,2);
    public int MaxFormation = 3;

    public BattleFormations()
    {
        _Formations  = new List<BattleFormation>();
    }

    public int AddFormation(string mainCharacterID, Vector2 position)
    {
        //0 succeed; 1 exceed maximum; 2 exceed vector position; 3 not found
        for (int i = 0;i < PartyManager.Instance.CharacterParty.Count;i++)
        {
            if (PartyManager.Instance.CharacterParty[i]._ID == mainCharacterID)
            {
                return AddFormation(PartyManager.Instance.CharacterParty[i], position);
            }
        }
        return 3;
    }

    public int AddFormation(Character characterToAdd, Vector2 position)
    {
        //0 succeed; 1 exceed maximum; 2 exceed vector position
        if (position.x > MaxVector.x || position.y > MaxVector.y)
            return 2;
        if (_Formations.Count == MaxFormation)
            return 1;
        else
        {
            _Formations.Add(new BattleFormation(characterToAdd, position));
            return 0;
        }
    }

    public int RemoveFromFormationById(string characterID)
    {
        //Only use this when there are no same id between character in formation.
        //0 succedd; 1 not found
        BattleFormation formation = _Formations.Find(x => x.Character._ID == characterID);
        if(formation != null)
        {
            _Formations.Remove(formation);
            return 0;
        }
        return 1;
    }

    public int RemoveFromFormation(Character characterToRemove)
    {
        //0 succeed; 1 not found
        foreach (BattleFormation formation in _Formations)
        {
            if (characterToRemove == formation.Character)
            {
                _Formations.Remove(formation);
                return 0;   
            }
        }
        return 1;
    }

	public int SwapFormation(Character toChar, Character fromChar)
	{
		foreach (BattleFormation formation in _Formations) 
		{
			//find old char in formation
			if(formation.Character._ID == toChar._ID)
			{
				formation.Character = fromChar;
				return 0;
			}
		}
		return 1;
	}

	public int SwapFormationByMainCharacterId(string toCharId, string fromCharId)
	{
		//0 = succed, 1 = fromChar not found, 2 = toChar not found
		BattleFormation fromChar, toChar;
		Vector2 tempChar;

		foreach (BattleFormation formation in _Formations) 
		{
			if(formation.Character._ID == toCharId)
			{
				toChar = formation;
				foreach (BattleFormation fromFormation in _Formations) 
				{
					if(fromFormation.Character._ID == fromCharId)
					{
						fromChar = fromFormation;

						tempChar = new Vector2(toChar.Position.x, toChar.Position.y);
						toChar.Position = new Vector2(fromChar.Position.x, fromChar.Position.y);
						fromChar.Position = new Vector2(tempChar.x, tempChar.y);
						return 0;
					}
				}
				return 1;
			}
		}
		return 2;
	}

	public void Clear()
	{
		_Formations.Clear ();
	}
}
