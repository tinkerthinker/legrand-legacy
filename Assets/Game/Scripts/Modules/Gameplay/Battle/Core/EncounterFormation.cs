﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Legrand.core;
using Legrand.database;

[System.Serializable]
public class EncounterFormation {
    public int _ID;
    public string _PartyId;
    public string MonsterId;
	public int MonsterLvl;
	public float HealthMonster;
    public Vector2 Position;

//	public static EncounterFormation setForm (string id, int lvl, float health){
//		EncounterFormation encForm = new EncounterFormation ();
//		encForm.MonsterId = id;
//		encForm.MonsterLvl = lvl;
//		encForm.HealthMonster = health;
//
//		return encForm;
//	}
}
