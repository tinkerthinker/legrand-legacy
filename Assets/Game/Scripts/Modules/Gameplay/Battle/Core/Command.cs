﻿using UnityEngine;
using System.Collections;

public enum CommandTypes
{
    Attack,	//0
    SkillUlti,	//1
    Magic,	//2
    Item,	//3
    Guard,	//4
    Flee,	//5
    Change,	//6
    Move,	//7
    None,	//8
	Skill,
    Charging
}

public enum CommandPos
{
    Up,
    Right,
    Down,
    Left
}

[System.Serializable]
public class Command {
	[HideInInspector]
	public int Index;
	public string Description;
}

[System.Serializable]
public class CommandData : Command{
//    public int Index;
    public string CharacterID;
	public string CommandName;
    public int PanelPos;
    public int SlotPos;
    public int CommandType;
    public int CommandValue;
    public bool Active;
    public bool Important;
//	public string Description;
    
    public CommandData()
    {
		Index = 0;
		CharacterID = "";
		CommandName = "";
		PanelPos = 0;
		SlotPos = 0;
		CommandType = 0;
		CommandValue = 0;
		Description = "";
        Active = true;
    }

    public CommandData(int index, string charId, CommandPos panelPos, CommandPos slotPos)
    {
        Index = index;
        CharacterID = charId;
        PanelPos = (int)panelPos;
		SlotPos = (int)slotPos;
        Active = true;
        if (panelPos == CommandPos.Right && slotPos == CommandPos.Right)
        {
			CommandType = (int)CommandTypes.Attack;
            CommandValue = 1;
			CommandName = "Attack";
        } else if (panelPos == CommandPos.Left && slotPos == CommandPos.Up)
        {
			CommandType = (int)CommandTypes.Change;
			CommandValue = 1;
			CommandName = "Change";
		} else if (panelPos == CommandPos.Left && slotPos == CommandPos.Right)
		{
			CommandType = (int)CommandTypes.Move;
			CommandValue = 1;
			CommandName = "Move";
        } else if (panelPos == CommandPos.Left && slotPos == CommandPos.Left)
        {
			CommandType = (int)CommandTypes.Guard;
			CommandValue = 1;
			CommandName = "Guard";
        } else if (panelPos == CommandPos.Left && slotPos == CommandPos.Down)
        {
			CommandType = (int)CommandTypes.Flee;
            CommandValue = 1;
        } else if (panelPos == CommandPos.Down && slotPos == CommandPos.Up)
        {
			CommandType = (int)CommandTypes.Item;
			CommandValue = 0;
			CommandName = "Item 1";
        } else if (panelPos == CommandPos.Down && slotPos == CommandPos.Right)
        {
			CommandType = (int)CommandTypes.Item;
			CommandValue = 1;
			CommandName = "Item 2";
        } else if (panelPos == CommandPos.Down && slotPos == CommandPos.Down)
        {
			CommandType = (int)CommandTypes.Item;
			CommandValue = 2;
			CommandName = "Item 3";
		} else if (panelPos == CommandPos.Down && slotPos == CommandPos.Left)
		{
			CommandType = (int)CommandTypes.Item;
			CommandValue = 3;
			CommandName = "Item 4";
		}
		else if (panelPos == CommandPos.Up && slotPos == CommandPos.Up)
		{
			CommandType = (int)CommandTypes.Magic;
			CommandValue = 1;
			CommandName = "Magic 1";
		}
		else {
            CommandType = (int)CommandTypes.None;
			CommandValue = -1;
        }
    }

    public CommandData(int index, string charId, CommandPos panelPos, CommandPos slotPos, CommandTypes commandType, int commandValue, bool active = true)
    {
        Index = index;
        CharacterID = charId;
        PanelPos = (int)panelPos;
		SlotPos = (int)slotPos;
		CommandType = (int)commandType;
        CommandValue = commandValue;
        Active = active;
    }
    public void SetCommandActive(bool active)
    {
        Active = active;
    }
    public void SetCommandImportant(bool important)
    {
        Important = important;
    }
    public static CommandData Copy (CommandData oldCommand)
	{
		CommandData newCommand = new CommandData();
		newCommand.Index = oldCommand.Index;
		newCommand.CharacterID = oldCommand.CharacterID;
		newCommand.CommandName = oldCommand.CommandName;
		newCommand.PanelPos = oldCommand.PanelPos;
		newCommand.SlotPos = oldCommand.SlotPos;
		newCommand.CommandType = oldCommand.CommandType;
		newCommand.CommandValue = oldCommand.CommandValue;
        newCommand.Active = oldCommand.Active;
		return newCommand;
	}
		
}
