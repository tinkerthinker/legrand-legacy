﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class BattleCommand {
	public int commandTurn;
	//Ref
	public Battler attacker;
	public List<Battler> target = new List<Battler>();
	public BattlePlane targetPos;
	public bool isCasting;
	public bool isExecuted;
	public bool isDone;
	public bool onTimedHit;

	public GameObject magicCasted;
	public CommandData commandData;
	public Battler batPartner;
    public List<Vector2> GridTarget = new List<Vector2>();
}
