using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using Legrand.core;

public class BattleEventListener {
//	private enum BattleGauge{
//		Attack,
//		Hit,
//		CritEvade,
//		CastHitMagic,
//		DealDamage,
//	}
//	private BattleController controller;
//	private PoolingSystem poolingSystem;
//
//	public BattleEventListener(){
//		poolingSystem = PoolingSystem.Instance;
//	}
//
//	public void initListener(BattleController controller){
//		this.controller = controller; 
//		EventManager.Instance.AddListener		<StartCastEvent>	(startCastListener);
//		EventManager.Instance.AddListener		<CastEvent>			(castListener);
//
//		EventManager.Instance.AddListener		<StartAttackEvent>	(startAttackListener);
//		EventManager.Instance.AddListener		<AttackEvent> 		(attackListener);
//		EventManager.Instance.AddListener		<SkillEvent> 		(skillListener);
//		EventManager.Instance.AddListener		<DoneAttackEvent> 	(attackDoneListener);
//		EventManager.Instance.AddListener		<DoneActionEvent> 	(actionDoneListener);
//
//		EventManager.Instance.AddListener		<ItemEvent> 		(itemListener);
//
//	}
//	public void removeListener(){
//		EventManager.Instance.RemoveListener	<StartCastEvent>	(startCastListener);
//		EventManager.Instance.RemoveListener	<CastEvent>			(castListener);
//
//		EventManager.Instance.RemoveListener	<StartAttackEvent>	(startAttackListener);
//		EventManager.Instance.RemoveListener	<AttackEvent> 		(attackListener);
//		EventManager.Instance.RemoveListener	<SkillEvent> 		(skillListener);
//		EventManager.Instance.RemoveListener	<DoneAttackEvent> 	(attackDoneListener);
//		EventManager.Instance.RemoveListener	<DoneActionEvent> 	(actionDoneListener);
//
//		EventManager.Instance.RemoveListener	<ItemEvent> 		(itemListener);
//	}
//
//	/* gauge charge
//        Fighter         Warrior         Warlord                 Any ally (including self) hit by an enemy
//        Archer          Ranger          Sharpshooter    		Hit an enemy
//        Apprentice      Mage            Sorceress               Casting magic / Receive magic damage
//        Thief           Rogue           Assassin                Critical Hit / Evade enemy attack
//        Brawler         Barbarian       Berserker               Deal Damage to enemy (calculated based on damage amount)
//        Dragoon         Dragoon         Dragoon                 Receive damage from enemy
// 
//        Full Gauge = 100 points
//        Finn = 1 points
//        Aria = 3 points
//        Eris = 5 points
//        Kael = 10 points / hit or eva
//        Azzam =
//        Scatia =
// 
//        */
//	
//	private void gaugeCharging(BattleGauge batGauge, Battler bat){
//		BattleClass battleClass = (bat.Character as MainCharacter)._BattleClass;
//		
//		switch (battleClass) {
//		case BattleClass.Fighter:
//		case BattleClass.Warrior:
//		case BattleClass.Warlord:
//			//
//			if(batGauge == BattleGauge.Hit)
//				bat.incereaseGauge(1);
//			break;
//		case BattleClass.Archer:
//		case BattleClass.Ranger:
//		case BattleClass.Sharpshooter:
//			//
//			if(batGauge == BattleGauge.Attack)
//				bat.incereaseGauge(3);
//			break;
//		case BattleClass.Apprentice:
//		case BattleClass.Mage:
//		case BattleClass.Sorceress:
//			//
//			if(batGauge == BattleGauge.CastHitMagic)
//				bat.incereaseGauge(5);
//			break;
//		case BattleClass.Thief:
//		case BattleClass.Rogue:
//		case BattleClass.Assassin:
//			//
//			if(batGauge == BattleGauge.CritEvade)
//				bat.incereaseGauge(10);
//			break;
//		case BattleClass.Brawler:
//		case BattleClass.Barbarian:
//		case BattleClass.Berserker:
//			//
//			if(batGauge == BattleGauge.Attack)
//				bat.incereaseGauge(1);
//			break;
//		case BattleClass.Dragoon:
//			if(batGauge == BattleGauge.Hit)
//				bat.incereaseGauge(1);
//			//
//			break;
//		}
//		
//	}
//	private void skillListener(SkillEvent evt){
//		foreach (Battler target in evt.command.target) {	
//			Damage dmg = target.receivePhysicalDamage (evt.command.attacker);
//			damagePopUp(target.transform, dmg.BaseDamage.ToString());
//		}
//	}
//	private void startAttackListener(StartAttackEvent evt){
//		foreach (Battler target in evt.command.target) {
//			if(target.BattlerType == BattlerType.Player){
//				EventManager.Instance.TriggerEvent (new TimedDefEvent(target, true));
//			}
//			target.Targeted.Add(evt.command.attacker);
//		}
//	}
//	private void attackListener(AttackEvent evt){
//		//evade hit attack
//		foreach (Battler target in evt.command.target) {				
//			if(evt.isFinalHit){
//				damageEvent(target, evt.command.attacker);
//				target.Targeted.Remove(evt.command.attacker);
//			}else{
//				target.hitState();
//			}
//		}
//		evt.command.attacker.OnTimedHit = false;
//	}
//
//	private void damageEvent(Battler target, Battler attacker){
//		var evasion = target.RollEvadeFrom(attacker);
//		//target evade
//		if(evasion){
//			if(attacker.Character._Type == 0){
//				gaugeCharging(BattleGauge.CritEvade, attacker);
//			}
//			
//			damagePopUp(target.transform, "Miss");
//		} else {
//			//attacker attack
//			Damage dmg = target.receivePhysicalDamage(attacker);
//			attacker.Attack(target, dmg);
//			
//			cameraShake(dmg.IsCritical);
//			
//			if(attacker.Character._Type == 0){
//				BattleClass[] classes = new BattleClass[]{
//					BattleClass.Fighter,
//					BattleClass.Warrior,
//					BattleClass.Warlord
//				};
//				
//				Battler AnyFighter = Battle.Instance.charList.Find(x => classes.Contains((x.Character as MainCharacter)._BattleClass));
//				if(AnyFighter != null && AnyFighter.BattlerState != BattlerState.Dead) gaugeCharging(BattleGauge.Hit, AnyFighter);
//				
//				if(dmg.IsCritical){
//					gaugeCharging(BattleGauge.CritEvade, attacker);
//				}else{
//					gaugeCharging(BattleGauge.Hit, attacker);
//				}
//			}else{
//				gaugeCharging(BattleGauge.Hit, target);
//			}
//			interruptTarget(target);
//			damagePopUp(target.transform, Mathf.FloorToInt(dmg.BaseDamage).ToString());
//		}
//	}
//	private void interruptTarget(Battler target){
//		BattleCommand attackedCommand = controller.commands.Find(x => x.attacker.Equals(target));
//		
//		if(attackedCommand != null){
//			if(attackedCommand.isCasting){
//				if (attackedCommand.magicCasted != null) {
//					PKFxFX fx = attackedCommand.magicCasted.GetComponentInChildren<PKFxFX> ();
//					if (fx != null) {
//						fx.StopEffect ();
//					}
//					attackedCommand.magicCasted.DestroyAPS ();
//				}
//				controller.removeCommand(attackedCommand);
//				
//			}else if(target.BattlerState == BattlerState.Dead){
//				controller.removeCommand(attackedCommand);
//				
//				foreach(Battler targetedAttacker in controller.commands.ConvertAll(x =>x.attacker)){
//					if(targetedAttacker.Targeted.Contains(target)) targetedAttacker.Targeted.Remove(target);
//				}
//			}
//		}
//	}
//	private void cameraShake(bool crit){
//		CameraShakeInstance shakeInstance;
//		if(crit){
//			shakeInstance = CameraShaker.Instance.ShakeOnce(4f, 4f, 0.2f, 0.8f);
//			controller.BattleCam.blurCrit();
//		}else{
//			shakeInstance = CameraShaker.Instance.ShakeOnce(2f, 2f, 0.2f, 0.4f);
//		}
//	}
//	private void attackDoneListener(DoneAttackEvent evt){
//		foreach (Battler target in evt.command.target) {
//			if(target.Targeted.Contains(evt.command.attacker)){
//				target.Targeted.Remove (evt.command.attacker);
//			}
//		}
//	}
//	private void actionDoneListener(DoneActionEvent evt){
//		evt.command.attacker.BattlerState = BattlerState.Idle;
//		controller.removeCommand (evt.command);
//	}
//	private void castListener(CastEvent evt){
//		controller.shiftCasterCommand (evt.command);
//	}
//	private void startCastListener(StartCastEvent evt){
//		evt.command.attacker.castingMagic (evt.command.target);
//		int count = 0;
//		if (evt.command.magicCasted != null) {
//			PKFxFX fx = evt.command.magicCasted.GetComponentInChildren<PKFxFX> ();
//			if (fx != null) {
//				fx.StopEffect ();
//			}
//			evt.command.magicCasted.DestroyAPS ();
//		}
//		if (evt.command.attacker.Character._Type == 0) {
//			gaugeCharging (BattleGauge.CastHitMagic, evt.command.attacker);
//		}
//
//		foreach (Battler magicTarget in evt.command.target) {
//			if(magicTarget.BattlerState != BattlerState.Dead){
//				Damage damage = magicTarget.receiveMagicalDamage(evt.command.commandData.CommandValue, evt.command.attacker);
//				damagePopUp(magicTarget.transform, Mathf.FloorToInt(damage.BaseDamage).ToString());
//
//				interruptTarget(magicTarget);
//
//				BattleClass[] classes = new BattleClass[]{BattleClass.Apprentice,BattleClass.Mage,BattleClass.Sorceress};
//				if(magicTarget.Character._Type == 0){
//					if(classes.Contains((magicTarget.Character as MainCharacter)._BattleClass))
//						gaugeCharging (BattleGauge.CastHitMagic, magicTarget);
//				}
//				count++;
//			}
//		}
//		
//		Damage cost = evt.command.attacker.receiveCostDamage (evt.command.commandData.CommandValue, count);
//		damagePopUp (evt.command.attacker.transform, Mathf.FloorToInt (cost.BaseDamage).ToString ());
//
//		foreach (Battler target in evt.command.target) {
//			if(target.Targeted.Contains(evt.command.attacker)){
//				target.Targeted.Remove (evt.command.attacker);
//			}
//		}
//		controller.removeCommand (evt.command);
//	}
//	private void itemListener(ItemEvent evt){
//		evt.command.attacker.useItem (evt.command.commandData.CommandValue, evt.command.target);
//		controller.removeCommand (evt.command);
//	}
//	private void damagePopUp(Transform trans, string text){
//		GameObject popUp = poolingSystem.InstantiateAPS("DamagePopUp", trans.position, trans.rotation);
//		popUp.GetComponent<Text> ().text = text;
//	}
}
