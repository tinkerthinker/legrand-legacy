using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using Legrand.core;

public class TurnController {
	private List<Battler> battlerList = new List<Battler>();

	private float averageAGI;
	public float AverageAGI{
		get {return averageAGI;}
	}
	private float maxSpeed;
	public float MaxSpeed{
		get {return maxSpeed;}
	}

	private bool hitMax = false;
	private bool inited = false;

	public delegate void OnDone(List<Battler> list);
	public delegate void OnInited();

	public event OnDone onDone;
	public event OnInited onInited;

	public void initSpeed(List<Battler> bat){
		battlerList = bat;
		initValue ();
	}
	public void SetBattlerList (List<Battler> bat){
		battlerList = bat;
	}
    public void regenerateSpeed(List<Battler> ba){
		foreach(Battler b in ba.Where(x => (int) x.ReactSpeed >= maxSpeed).ToList())
		{
			if(b.BattlerState != BattlerState.Dead){
				b.ReactSpeed 		= b.ReactSpeed - (int) maxSpeed;
			}
		}
		
		hitMax = false;
		initValue ();
	}
	private void initValue (){
		float tempAGI = 0;

		foreach (Battler b in battlerList) {
			if(b.BattlerState != BattlerState.Dead){
				tempAGI = tempAGI + b.Character.GetAttribute(Attribute.AGI);
			}
		}

		averageAGI 	= tempAGI / battlerList.Count;
		maxSpeed 	= averageAGI * battlerList.Count;
		
		inited 		= true;

		if (onInited != null) {
			onInited();
		}
	}

	private void calculateSpeed(){
		if (!hitMax) {
			for(int i = 0; i < battlerList.Count; i++) {
				int agi 					= battlerList[i].Character.GetAttribute(Attribute.AGI);
				battlerList[i].ReactSpeed 	= battlerList[i].ReactSpeed + (int) agi;
				
				if (battlerList[i].ReactSpeed >= maxSpeed) {
					hitMax = true;
				}
			}
		} else {
			//groupBySpeed(battlerList);
			battlerList = battlerList.OrderByDescending (x => x.ReactSpeed).ToList ();
			sendEvent();

			inited = false;
		}
	}
	private void groupBySpeed(List<Battler> list){
		IOrderedEnumerable<IGrouping<int, Battler>> charOrder = list.GroupBy(x => (int) x.ReactSpeed).OrderByDescending(z => z.Key); 
		
		int turn = 0;
		foreach (IGrouping<int,Battler> group in charOrder) 
		{ 
			foreach (Battler battler in group) 
			{ 
				battler.BattleTurn = turn;
			}
			turn++;
		}
	}
	private void sendEvent(){
		if (onDone != null) {
			onDone(battlerList);
		}
	}
	public IEnumerator updateSpeed(){
		while (inited) {
			calculateSpeed();
			yield return null;
		}
	}
}
