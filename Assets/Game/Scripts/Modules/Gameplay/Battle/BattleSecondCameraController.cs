﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class BattleSecondCameraController : MonoBehaviour {
    public bool FocusTarget = false;
    public bool IsMassive = false;
    public Transform NormalView;
    public Camera UICamera;

    private Tween _SelectTweenMove;
    private Tween _SelectTweenRot;
	// Use this for initialization
	void Start () {
	
	}
	
    public void StartTweenView (){
        FocusTarget = true;
        UICamera.gameObject.SetActive(true);
        _SelectTweenMove = transform.DOMove (new Vector3(transform.position.x, transform.position.y - 0.4f, transform.position.z-0.1f), 9f).SetLoops (-1, LoopType.Yoyo).SetEase(Ease.InOutQuad);
    }

    public void KillTween(){
        UICamera.gameObject.SetActive(false);
        FocusTarget = false;
        if (_SelectTweenMove != null)
            _SelectTweenMove.Kill(false);
    }
    public void FadeSecondCamera (){
        FocusTarget = false;
    }
	// Update is called once per frame
	void Update () {
        if (FocusTarget){
            transform.LookAt(NormalView);
        }
        
	}
}
