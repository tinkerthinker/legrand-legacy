﻿using UnityEngine;
using System.Collections;

public class DropItemTrigger : WorldTrigger {

    public GameObject myItem;
    public int id;
    public string itemID;

    public delegate void DropItemAction(int id);
    public static event DropItemAction OnDropItem;

    private GameObject helper;

    private Item _Temp;

    void Start()
    {
        _Temp = LegrandBackend.Instance.ItemData[itemID];
    }

    void OnTriggerEnter(Collider coll)
    {
        if(coll.tag == "Interaction")
        {
            if (_Temp != null && NeedActivate && isActiveAndEnabled && myItem.GetComponent<SpriteRenderer>().sprite == null /*&& PartyManager.Instance.Inventory.GetItem(_Temp._ID,_Temp.ItemType) != null*/)
            {
                helper = coll.GetComponent<DialogueHelperReference>().helper;
                PopUpUI.CallDialogueMarkPopUp(helper, true);
                EventManager.Instance.AddListener<EnterTrigger>(DropItem);
            }
        }
    }

    void OnTriggerExit(Collider coll)
    {
        if(coll.tag == "Interaction")
        {
            helper = null;
            PopUpUI.CallDialogueMarkPopUp(null, false);
            EventManager.Instance.RemoveListener<EnterTrigger>(DropItem);
        }
    }

    void OnDisable()
    {
        PopUpUI.CallDialogueMarkPopUp(null, false);
        helper = null;
        if (EventManager.Instance != null)
            EventManager.Instance.RemoveListener<EnterTrigger>(DropItem);
        else
            Debug.LogWarning("Event Manager tidak ada.");
    }

    void OnDestroy()
    {
        PopUpUI.CallDialogueMarkPopUp(null, false);
        helper = null;
        if (EventManager.Instance != null)
            EventManager.Instance.RemoveListener<EnterTrigger>(DropItem);
        else
            Debug.LogWarning("Event Manager tidak ada.");
    }

    void DropItem(EnterTrigger e)
    {
        if (OnDropItem != null)
            OnDropItem(id);

        StartCoroutine(FadingScreen());
    }

    IEnumerator FadingScreen()
    {
        EventManager.Instance.TriggerEvent(new SequenceEvent(false));
        WorldSceneManager._Instance.ScreenFader.EndScene();
        yield return new WaitForSeconds(1f);
        myItem.GetComponent<SpriteRenderer>().sprite = LegrandBackend.Instance.ItemData[itemID].Icon;
        PopUpUI.CallDialogueMarkPopUp(null, false);
        EventManager.Instance.RemoveListener<EnterTrigger>(DropItem);
        helper = null;
        WorldSceneManager._Instance.ScreenFader.StartScene();
        PopUpUI.OnEndPopUp += ResetPlayerState;
        PopUpUI.CallNotifPopUp(LegrandBackend.Instance.ItemData[itemID].Name + " has been placed.");

    }

    void ResetPlayerState(bool value = true)
    {
        PopUpUI.OnEndPopUp -= ResetPlayerState;
        EventManager.Instance.TriggerEvent(new SequenceEvent(true));
    }

    public void SetActive(bool active)
    {
        if (active)
        {
            this.gameObject.GetComponent<Collider>().enabled = true;
            this.enabled = true;
        }
        else
        {
            this.gameObject.GetComponent<Collider>().enabled = false;
            this.enabled = false;
        }
    }
}
