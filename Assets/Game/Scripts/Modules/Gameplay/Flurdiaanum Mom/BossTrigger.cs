﻿using UnityEngine;
using System.Collections;

public class BossTrigger : WorldTrigger {
    public string bossID = "";
    public bool active = false;

    public TransitionData Data;

    void OnTriggerEnter(Collider coll)
    {
        if(coll.tag == "PartyLeader")
        {
            if (active)
            {
                string encounterArea = AreaController.Instance.CurrAreaData.areaName;
                WorldSceneManager.TransWorldToBattle(null, null, this.gameObject, "Battle/Arena " + encounterArea, bossID, Data, MonsterEncounterEvent.AmbushStat.Neutral);
            }
        }
    }
}
