﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FleeTrigger : MonoBehaviour {

    public List<Transform> paths = new List<Transform>();

    public delegate void FleeAction(int pos);
    public event FleeAction OnFlee;

    public delegate void MovePosition();
    public event MovePosition OnMovePosition;

    private int indexPath = 0;

    private GameObject player;

    void OnTriggerEnter(Collider coll)
    {
        if (coll.tag == "PartyLeader" && isActiveAndEnabled)
        {
            player = coll.gameObject;
            StartCoroutine(Flee());
        }
    }

    void OnDestroy()
    {
        OnFlee = null;
        OnMovePosition = null;
    }

    IEnumerator Flee()
    {
        if (++indexPath >= paths.Count)
            indexPath = 0;
        //indexPath = Random.Range(0, paths.Count);
        if (OnFlee != null)
            OnFlee(indexPath);
        player.GetComponent<PlayerController>().ControllerState = PlayerController.PlayerControllerState.Sequence;
        WorldSceneManager._Instance.ScreenFader.EndScene();
        yield return new WaitForSeconds(1f);
        WwiseManager.Instance.PlaySFX(WwiseManager.WorldFX.Treasure);
        transform.position = paths[indexPath].position;
        transform.rotation = paths[indexPath].rotation;
        if (OnMovePosition != null)
            OnMovePosition();
        WorldSceneManager._Instance.ScreenFader.StartScene();
        PopUpUI.OnEndPopUp += ResetPlayerState;
        PopUpUI.CallNotifPopUp("Flurdiaanum has escaped");
    }

    void ChangeIndex(int pos)
    {
        indexPath = pos;
    }

    void ResetPlayerState(bool value = true)
    {
        PopUpUI.OnEndPopUp -= ResetPlayerState;
        player.GetComponent<PlayerController>().ControllerState = PlayerController.PlayerControllerState.Normal;
        player = null;
    }
}
