﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RAIN.Core;

public class FlurdiaanumMomController : MonoBehaviour {

    public List<SpriteRenderer> listItem = new List<SpriteRenderer>();

    private FleeTrigger fleeTrigger;
    private BossTrigger bossTrigger;

    private AIRig ai;

    private int currPos = -1;
    private int dropItemId = -1;

    private bool timerEat = false;

    private enum FlurdiaanumState
    {
        Normal,
        Eating
    }

    private FlurdiaanumState myState;

    void Awake()
    {
        fleeTrigger = GetComponent<FleeTrigger>();
        bossTrigger = GetComponent<BossTrigger>();
        ai = GetComponentInChildren<AIRig>();
    }

    void OnEnable()
    {
        myState = FlurdiaanumState.Normal;
        currPos = 0;
        fleeTrigger.OnFlee += Move;
        fleeTrigger.OnMovePosition += CheckMoveRotation;
        DropItemTrigger.OnDropItem += DropItem;
        timerEat = false;
    }

    void OnDisable()
    {
        fleeTrigger.OnFlee -= Move;
        fleeTrigger.OnMovePosition -= CheckMoveRotation;
        DropItemTrigger.OnDropItem -= DropItem;
        ClearDropTrigger();
    }

    void OnDestroy()
    {
        fleeTrigger.OnFlee -= Move;
        fleeTrigger.OnMovePosition -= CheckMoveRotation;
        DropItemTrigger.OnDropItem -= DropItem;
        ClearDropTrigger();
    }

    void Start()
    {
        fleeTrigger.enabled = true;
    }

    void Update()
    {
        switch (myState)
        {
            case FlurdiaanumState.Normal:
                fleeTrigger.enabled = true;
                bossTrigger.active = false;
                ai.AI.WorkingMemory.SetItem("state", 0);
                EnableDropTrigger();
                break;
            case FlurdiaanumState.Eating:
                fleeTrigger.enabled = false;
                bossTrigger.active = true;
                ai.AI.WorkingMemory.SetItem("state", 1);
                DisableDropTirgger();
                if (!timerEat)
                {
                    timerEat = true;
                    StartCoroutine(EatingTime());   
                }
                break;
        }
    }

    IEnumerator EatingTime()
    {
        yield return new WaitForSeconds(15f);
        StartCoroutine(ResetAll());
    }

    IEnumerator ResetAll()
    {
        WorldSceneManager._Instance.ScreenFader.EndScene();
        yield return new WaitForSeconds(1f);
        myState = FlurdiaanumState.Normal;
        ClearDropTrigger();
        WorldSceneManager._Instance.ScreenFader.StartScene();
        timerEat = false;
    }

    void Move(int pos)
    {
        currPos = pos;
        if (currPos == dropItemId)
            myState = FlurdiaanumState.Eating;
    }

    void CheckMoveRotation()
    {
        if (myState == FlurdiaanumState.Eating)
            transform.eulerAngles = transform.eulerAngles - new Vector3(0f, 180f, 0f);
    }

    void DropItem(int id)
    {
        ClearDropTrigger();
        dropItemId = id;
    }

    void ClearDropTrigger()
    {
        currPos = -1;
        dropItemId = -1;
        for (int i = 0; i < listItem.Count; i++)
            listItem[i].sprite = null;
    }

    void EnableDropTrigger()
    {
        for (int i = 0; i < listItem.Count; i++)
            listItem[i].transform.parent.GetComponent<DropItemTrigger>().SetActive(true);
    }

    void DisableDropTirgger()
    {
        for (int i = 0; i < listItem.Count; i++)
            listItem[i].transform.parent.GetComponent<DropItemTrigger>().SetActive(false);
    }
}
