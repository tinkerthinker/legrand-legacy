﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class GlobalGameStatus : MonoBehaviour {
    public enum GameState
    {
        WorldDialog,
        Sequence,
        Pause,
        Battle,
        OpenMenu,
        PopUp,
        Tutorial
    }

    public bool[] StateStatus;

    private static GlobalGameStatus s_Instance;
    public static GlobalGameStatus Instance
    {
        get
        {
            if (s_Instance == null)
            {
                s_Instance = GameObject.FindObjectOfType(typeof(GlobalGameStatus)) as GlobalGameStatus;
            }
            return s_Instance;
        }
    }

    public Dictionary<string, GameObject> RegisteredObject = new Dictionary<string, GameObject>();
    public static GameObject GetRegisteredObject(string id)
    {
        if (Instance.RegisteredObject.ContainsKey(id)) return Instance.RegisteredObject[id];
        return null;
    }

    private Camera _MainCamera;
	public Camera MainCamera
	{
		get 
		{
			if (_MainCamera == null) {
				_MainCamera = Camera.main;
			}

			return _MainCamera;
		}

		set 
		{
			_MainCamera = value;
		}
	}

    private bool _IsCurrRoomSafe = true;

	void Awake () {
        StateStatus = new bool[Enum.GetNames(typeof(GameState)).Length];
        EventManager.Instance.AddListener<AssignObjectsDialogueEvent>(AssignTalkingObjects);
        EventManager.Instance.AddListener<RemoveObjectsDialogueEvent>(RemoveTalkingObjects);
    }
    
    void OnDestroy()
    {
        if(EventManager.Instance)
            EventManager.Instance.AddListener<AssignObjectsDialogueEvent>(AssignTalkingObjects);
    }

    public void AssignTalkingObjects(AssignObjectsDialogueEvent e)
    {
        //		TalkingObjects.Clear ();
        foreach (TalkingObject to in e.TalkingObjects)
        {
            if (RegisteredObject.ContainsKey(to.ID))
                RegisteredObject[to.ID] = to.GObject;
            else
                RegisteredObject.Add(to.ID, to.GObject);
        }
    }

    public void RemoveTalkingObjects(RemoveObjectsDialogueEvent e)
    {
        //		TalkingObjects.Clear ();
        foreach (TalkingObject to in e.TalkingObjects)
        {
            if (RegisteredObject.ContainsKey(to.ID))
                RegisteredObject.Remove(to.ID);
        }
    }

    public bool IsStateNormal()
    {
        for (int i = 0; i < StateStatus.Length; i++)
        {
            if (StateStatus[i]) return false;
        }
        return true;
    }

    public void SetCurrRoomSafety(bool isSafe)
    {
        _IsCurrRoomSafe = isSafe;
    }

    public bool GetCurrRoomSafety()
    {
        return _IsCurrRoomSafe;
    }
}
