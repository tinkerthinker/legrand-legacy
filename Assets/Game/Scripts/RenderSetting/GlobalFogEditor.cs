﻿using UnityEngine;
using System.Collections;

public class GlobalFogEditor : MonoBehaviour {
	[SerializeField]
	public Color FogColor;
	public FogMode FogMode;
	public float StartDistance;
	public float EndDistance;

	void OnEnable()
	{
		RenderSettings.fog = true;
		RenderSettings.fogColor = FogColor;
		RenderSettings.fogMode = FogMode;
		RenderSettings.fogStartDistance = StartDistance;
		RenderSettings.fogEndDistance = EndDistance;
	}

	void OnDisable()
	{
		RenderSettings.fog = false;
	}
}
