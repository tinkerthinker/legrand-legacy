﻿using UnityEngine;
using System.Collections;

public class GILightSetter : MonoBehaviour {
	public Color AmbientColor;
	public float AmbientIntensity;

	void OnEnable()
	{
		RenderSettings.ambientMode = UnityEngine.Rendering.AmbientMode.Flat;
		RenderSettings.ambientLight = AmbientColor;
		RenderSettings.ambientIntensity = AmbientIntensity;
	}
}
