﻿using UnityEngine;
using System.Collections;

public class AmbientLightSetter : MonoBehaviour {
	public Color AmbientColor;
	public float Intensity;
	// Use this for initialization
	void OnEnable () {
		RenderSettings.ambientMode = UnityEngine.Rendering.AmbientMode.Flat;
		RenderSettings.ambientLight = AmbientColor;
		RenderSettings.ambientIntensity = Intensity;
	}
	
	void OnDisable()
	{
		RenderSettings.ambientMode = UnityEngine.Rendering.AmbientMode.Skybox;
	}
}
