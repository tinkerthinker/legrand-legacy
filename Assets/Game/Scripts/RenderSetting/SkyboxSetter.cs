﻿using UnityEngine;
using System.Collections;

public class SkyboxSetter : MonoBehaviour {
    public Material SkyboxMaterial;
	
    void OnEnable()
    {
        RenderSettings.skybox = SkyboxMaterial;
    }

    void OnDisable()
    {
        RenderSettings.skybox = null;
    }
}
