﻿using UnityEngine;
using System.Collections;

public class Screenshot : MonoBehaviour {
	public Texture2D snapshot;

	public event ScreenShotDone OnScreenShotDone;
	public delegate void ScreenShotDone();

	public void TakeSnapShot()
	{
		StartCoroutine (ScreenShot());
	}

	IEnumerator ScreenShot()
	{
		yield return new WaitForEndOfFrame();
		Texture2D texture = new Texture2D (Screen.width, Screen.height, TextureFormat.ARGB32, false);
		texture.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
		texture.Apply ();
		snapshot = texture;
		OnScreenShotDone();
	}
}
