﻿using UnityEngine;
using System.Collections;

public class SceneManager : MonoBehaviour 
{
    private static SceneManager _Instance;
    
    public string FirstScene = "";
    public ScreenFader ScreenFader;
    
    private string currentSceneName;
    private string nextSceneName;
    private AsyncOperation resourceUnloadTask;
    private AsyncOperation sceneLoadTask;
    private enum SceneState { Reset, Preload, Load, Unload, Postload, Ready, Run, Count };
    private SceneState sceneState;
    private delegate void UpdateDelegate();
    private UpdateDelegate[] updateDelegates;
    
	private bool _IsForced;

    public static void SwitchScene(string nextSceneName)
    {
        if(_Instance != null)
        {
            if (_Instance.currentSceneName != nextSceneName)
            {
                _Instance.nextSceneName = nextSceneName;
				_Instance.sceneState = SceneState.Run;
				_Instance._IsForced = false;

                if (nextSceneName == "TitleScene")
                {
                    EventManager.Instance.RemoveAll();
                }
            }
        }
    }

	public static void ForceSwitchScene(string nextSceneName)
	{
		if(_Instance != null)
		{
			_Instance.nextSceneName = nextSceneName;
			_Instance.sceneState = SceneState.Run;
			_Instance._IsForced = true;
		}
	}
    
    protected void Awake()
    {
        //Let's keep this alive between scene changes
        Object.DontDestroyOnLoad(gameObject);
        
        //Setup the singleton instance
        _Instance = this;
        Cursor.visible = false;
        //Setup the array of updateDelegates
        updateDelegates = new UpdateDelegate[(int)SceneState.Count];
        
        //Set each updateDelegate
        updateDelegates[(int)SceneState.Reset] = UpdateSceneReset;
        updateDelegates[(int)SceneState.Preload] = UpdateScenePreload;
        updateDelegates[(int)SceneState.Load] = UpdateSceneLoad;
        updateDelegates[(int)SceneState.Unload] = UpdateSceneUnload;
        updateDelegates[(int)SceneState.Postload] = UpdateScenePostload;
        updateDelegates[(int)SceneState.Ready] = UpdateSceneReady;
        updateDelegates[(int)SceneState.Run] = UpdateSceneRun;
        
        //First scene
        nextSceneName = FirstScene;
        sceneState = SceneState.Reset;
        GetComponent<Camera>().orthographicSize = Screen.height/2;
    }
    
    protected void OnDestroy()
    {
        //Clean up all the updateDelegates
        if(updateDelegates != null)
        {
            for(int i = 0; i < (int)SceneState.Count; i++)
            {
                updateDelegates[i] = null;
            }
            updateDelegates = null;
        }
        
        //Clean up the singleton instance
        if(_Instance != null)
        {
            _Instance = null;
        }
    }
    
    protected void Update()
    {
		if(updateDelegates[(int)sceneState] != null)
        {
            updateDelegates[(int)sceneState]();
        }
    }
    
    //Attach the new scene controller to start cascade of loading
    private void UpdateSceneReset()
    {
        // run a gc pass
        System.GC.Collect();
        sceneState = SceneState.Preload;
    }
    
    //Handle anything that needs to happen before loading
    private void UpdateScenePreload()
    {
        if (ScreenFader.IsInTransition) return;
        
        sceneLoadTask = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(nextSceneName);
		sceneLoadTask.allowSceneActivation = false;
        sceneState = SceneState.Load;
    }

    //Show the loading screen until it's loaded
    private void UpdateSceneLoad()
    {
        //Done loading?
		if (Mathf.Approximately (sceneLoadTask.progress, 0.9f)) 
		{
			sceneLoadTask.allowSceneActivation = true;
		}

        if(sceneLoadTask.isDone)
        {
            LoadingManager.StopLoading();
            ScreenFader.StartScene();
            sceneState = SceneState.Unload;
        }
    }
    
    //Clean up unused resources by unloading them
    private void UpdateSceneUnload()
    {
        //Cleaning up resources yet?
        if(resourceUnloadTask == null)
        {
            resourceUnloadTask = Resources.UnloadUnusedAssets();
        }
        else
        {
            //Done cleaning up?
            if(resourceUnloadTask.isDone == true && !ScreenFader.IsInTransition)
            {
                resourceUnloadTask = null;
                sceneState = SceneState.Postload;
            }
        }
    }
    
    //Handle anything that needs to happen immediately after loading
    private void UpdateScenePostload()
    {
        
		if (ScreenFader.IsInTransition) {
		} else {
			currentSceneName = nextSceneName;
			sceneState = SceneState.Ready;
			EventManager.Instance.TriggerEvent(new StartScreenEvent());
		}

    }
    
    //Handle anything that needs to happen immediately before running
    private void UpdateSceneReady()
    {
        //Run a gc pass
        //System.GC.Collect();
        
        sceneState = SceneState.Run;
    }
    
    //Wait for scene change
    private void UpdateSceneRun()
    {
        if(currentSceneName != nextSceneName || _IsForced)
        {
            ScreenFader.EndScene();
            sceneState = SceneState.Reset;
        }
		_IsForced = false;
    }
}
