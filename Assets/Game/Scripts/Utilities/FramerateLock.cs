﻿using UnityEngine;
using System.Collections;

public class FramerateLock : MonoBehaviour {
	public int frameRate = 60;
	
	private float oldTime  = 0.0f;
	private float deltaTime = 0.0f;
	private float curTime = 0.0f;
		
	void Start () {
		deltaTime = 1.0f / frameRate;
		oldTime = Time.realtimeSinceStartup;
	}
	
	void Update() {
		curTime = Time.realtimeSinceStartup;  
		
		while((curTime - oldTime) < deltaTime){
			curTime = Time.realtimeSinceStartup; // busy waiting        
		}
		
		oldTime = Time.realtimeSinceStartup;
	}
}
