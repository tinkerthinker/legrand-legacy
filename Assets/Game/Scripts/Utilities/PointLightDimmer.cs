﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class PointLightDimmer : MonoBehaviour
{
    private Light pLight;
    private bool isDimming = false;

    [Range(0f, 8f)]
    public float minIntensity;
    [Range(0f, 8f)]
    public float maxIntensity;
    public float duration;

    private float startValue;
    private float endValue;

    // Use this for initialization
    void Awake()
    {
        pLight = GetComponent<Light>();
    }

    void OnEnable()
    {
        if (pLight != null)
        {
            pLight.intensity = minIntensity;
            DOTween.To(() => pLight.intensity, x => pLight.intensity = x, maxIntensity, duration).SetLoops(-1, LoopType.Yoyo);
        }
    }

    IEnumerator DimLight()
    {
        float t = 0f;

        while (true)
        {
            t += Time.deltaTime / duration;
            pLight.intensity = Mathf.Lerp(startValue, endValue, t);

            if (pLight.intensity == endValue)
            {
                t = 0f;
                isDimming = !isDimming;
                if (isDimming)
                {
                    startValue = maxIntensity;
                    endValue = minIntensity;
                }
                else
                {
                    startValue = minIntensity;
                    endValue = maxIntensity;
                }
            }

            yield return null;
        }
    }
}
