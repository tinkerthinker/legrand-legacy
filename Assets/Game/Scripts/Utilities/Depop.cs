﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Depop : MonoBehaviour
{
    public Texture[] FireTexture;
	GameObject[] DiactivatedGameObjectAfterSC;
	public bool processing = false;
	public GameObject Target;
	public GameObject BurnFaderCamera;
	Material mat;
	public Screenshot SC;

	public float TimeTransition = 1f;

	float _SliceValue;

	void Start ()
	{
		mat = Target.GetComponent<Renderer>().material;
		SC.OnScreenShotDone += FinishSnapShot;
	}

	void OnDestroy()
	{
		SC.OnScreenShotDone -= FinishSnapShot;
	}

	void FinishSnapShot()
	{
		mat.mainTexture = SC.snapshot;
		BurnFaderCamera.gameObject.SetActive (true);
		foreach (GameObject gameObject in DiactivatedGameObjectAfterSC)
			gameObject.SetActive (false);
		processing = false;
//		StartCoroutine (DelayProcessing());
	}

	IEnumerator DelayProcessing()
	{
		yield return new WaitForSeconds (1f);
		
		mat.SetFloat("_SliceAmount", 0);
//		mat.SetFloat ("_BurnSize", 0);
		DOTween.To (x => _SliceValue = x, 0, 1, TimeTransition).OnUpdate(UpdateMaterial).OnComplete(StopTransition);

	}

	public void StartDepop (int EncounterType = 0)
	{
        mat.SetTexture("_BurnRamp", FireTexture[EncounterType]);
//		StartCoroutine(ProcessDepop());
		_SliceValue = 0;
//		int textureIndex = Random.Range (0,3);
//		switch(textureIndex)
//		{
//		case 0:mat.SetTexture ("_DissolveTex", mat.GetTexture("_DissolveTexOne")); break;
//		case 1:mat.SetTexture ("_DissolveTex", mat.GetTexture("_DissolveTexTwo")); break;
//		case 2:mat.SetTexture ("_DissolveTex", mat.GetTexture("_DissolveTexThree")); break;
//		}

		StartCoroutine (DelayProcessing ());
	}

	public void UpdateMaterial()
	{
		mat.SetFloat("_SliceAmount", _SliceValue);
//		mat.SetFloat ("_BurnSize", _SliceValue);
	}

	public void StopTransition()
	{
		mat.mainTexture = null;
		BurnFaderCamera.gameObject.SetActive (false);
		mat.SetFloat("_SliceAmount", 0);
		EventManager.Instance.TriggerEvent (new BattleTransitionDone());
	}

	public void StartTransition(GameObject[] diactivatedObjects)
	{
		DiactivatedGameObjectAfterSC = diactivatedObjects;
		processing = true;
		SC.TakeSnapShot();
	}

	public void StartTransition(GameObject diactivatedObject)
	{
		DiactivatedGameObjectAfterSC = new GameObject[1];
		DiactivatedGameObjectAfterSC [0] = diactivatedObject;
		processing = true;
		SC.TakeSnapShot();
	}
}