using UnityEngine;
using System.Collections;

public class ObscurableQueue : MonoBehaviour
{
	// Use this for initialization
	void Start ()
    { 
		RestartRender ();
    }

    /// <summary>
    /// Postpone render order after "DepthMask" shader in order to be obscured
    /// </summary>
	public void RestartRender()
	{
		Renderer[] renders = GetComponentsInChildren<Renderer>();
		
		foreach (Renderer rendr in renders)
		{            
			for(int j = 0;j<rendr.materials.Length;j++)
            {
                if(rendr.GetComponent<ParticleSystem>() == null) 
				    rendr.materials[j].renderQueue = 2452;
            }
        }
	}
}