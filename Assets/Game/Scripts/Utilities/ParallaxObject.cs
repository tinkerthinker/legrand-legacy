﻿using UnityEngine;
using System.Collections;

public class ParallaxObject : MonoBehaviour
{
	
	float parallaxScale;
	
	public float smoothing;
	
	public Transform cam;
	public Vector3 previousCamPos;
	
	private bool isParallaxing;
	
	private bool _Started = true;
	
	public bool MoveX = true;
	public bool MoveY = true;
	
	void Awake()
	{
		
	}
	
	public void Start()
	{
		cam = Camera.main.transform;
		
		parallaxScale = transform.localPosition.z * -1;
	}
	
	
	public void SetParallaxing(bool parallaxing)
	{
		previousCamPos = new Vector3(cam.position.x, cam.position.y, cam.position.z);
		isParallaxing = parallaxing;
		
	}
	
	
	void Update()
	{
		
		if (!_Started)
		{
			if (isParallaxing)
			{
				float backgroundPositionX = transform.localPosition.x;
				float backgroundPositionY = transform.localPosition.y;
				
				if (MoveX)
				{
					float parallaxX = (previousCamPos.x - cam.position.x) * parallaxScale;
					backgroundPositionX += parallaxX;
				}
				
				if (MoveY)
				{
					float parallaxY = (previousCamPos.y - cam.position.y) * parallaxScale;
					backgroundPositionY += parallaxY;
				}
				
				//				Vector3 backgroundTargetPos = new Vector3(backgroundPositionX,
				//				                                          transform.localPosition.y,
				//				                                          transform.localPosition.z);
				Vector3 backgroundTargetPos = new Vector3(backgroundPositionX,
				                                          backgroundPositionY,
				                                          transform.localPosition.z);
				
				transform.localPosition = Vector3.Lerp(transform.localPosition,
				                                       backgroundTargetPos,
				                                       0.1f);
				
				previousCamPos = new Vector3(cam.position.x, cam.position.y, cam.position.z);
			}
		}
		else
		{
			_Started = false;
		}
		
		
		
	}
}