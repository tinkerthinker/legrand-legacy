﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class ObjectPath : MonoBehaviour {
    public GameObject TargetObject;
    public string Path;
    public bool GetPath = false;

    void Update()
    {
        if(GetPath)
        {
            GetPath = !GetPath;
            if (TargetObject != null)
                Path = LegrandUtility.GetGameObjectPath(TargetObject);
        }
    }
}
