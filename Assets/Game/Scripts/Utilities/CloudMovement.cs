﻿using UnityEngine;
using System.Collections;

public class CloudMovement : MonoBehaviour {
	public BoxCollider cloudArea;
    private SpriteRenderer cloudSprite;
	public Vector2 MinMaxSpeed;
	public float direction = -1;
	private float speed;
    public bool diagonalMove = false;
	
    void Awake()
    {
        cloudSprite = GetComponent<SpriteRenderer>();
    }

    void Start()
    {
        speed = LegrandUtility.Random(MinMaxSpeed.x, MinMaxSpeed.y);
        if (direction != 1 && direction != -1) direction = 1;
    }

	// Update is called once per frame
	void Update ()
    {
        if(diagonalMove)
            transform.Translate(direction * speed * Time.deltaTime, direction * -speed * Time.deltaTime, 0f);
        else
            transform.Translate (direction * speed * Time.deltaTime, 0f, 0f);

        if(direction < 0)
        { 
            if(diagonalMove)
            {
                if(cloudSprite.bounds.min.y > cloudArea.bounds.max.y)
                {
                    transform.localPosition = new Vector3(cloudArea.bounds.max.x, cloudArea.bounds.min.y - cloudSprite.bounds.extents.y, transform.localPosition.z);
                    speed = LegrandUtility.Random(MinMaxSpeed.x, MinMaxSpeed.y);
                }
            }
            else
            {
                if (cloudSprite.bounds.max.x < cloudArea.bounds.min.x)
                {
                    transform.localPosition = new Vector3(cloudArea.bounds.max.x + cloudSprite.bounds.extents.x, transform.localPosition.y, transform.localPosition.z);
                    speed = LegrandUtility.Random(MinMaxSpeed.x, MinMaxSpeed.y);
                }
            }
        }
        else
        {
            if (diagonalMove)
            {
                if (cloudSprite.bounds.max.y < cloudArea.bounds.min.y)
                {
                    transform.localPosition = new Vector3(cloudArea.bounds.min.x, cloudArea.bounds.max.y + cloudSprite.bounds.extents.y, transform.localPosition.z);
                    speed = LegrandUtility.Random(MinMaxSpeed.x, MinMaxSpeed.y);
                }
            }
            else
            {
                if (cloudSprite.bounds.min.x > cloudArea.bounds.max.x)
                {
                    transform.localPosition = new Vector3(cloudArea.bounds.min.x - cloudSprite.bounds.extents.x, transform.localPosition.y, transform.localPosition.z);
                    speed = LegrandUtility.Random(MinMaxSpeed.x, MinMaxSpeed.y);
                }
            }
        }
    }
}
