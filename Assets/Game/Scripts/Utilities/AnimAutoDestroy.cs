﻿using UnityEngine;
using System.Collections;

public class AnimAutoDestroy : MonoBehaviour {
	public void DestroyAPSThis(){
		this.gameObject.DestroyBattleAPS ();
	}
	public void DestroyThis(){
		Destroy(this.gameObject);
	}
	public void DestroyThisTime(float t){
		Destroy (this.gameObject, t);
	}
   
}
