﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class SpriteAlphaDimmer : MonoBehaviour
{
    private SpriteRenderer spriteRenderer;
    private Color32 spriteColor;
    private bool isDimming = false;

    [Range(0f, 255f)] public float minAlpha;
    [Range(0f, 255f)] public float maxAlpha;
    public float duration;

    private float startValue;
    private float endValue;


    // Use this for initialization
    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    void OnEnable()
    {
        if(spriteRenderer != null)
        {
            spriteColor = spriteRenderer.color;
            spriteColor.a = (byte)minAlpha;
            spriteRenderer.color = spriteColor;

            Color32 endColor = spriteRenderer.color;
            endColor.a = (byte)maxAlpha;
            spriteRenderer.DOColor(endColor, duration).SetLoops(-1, LoopType.Yoyo);
        }
    }
}
