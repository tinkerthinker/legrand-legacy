﻿using UnityEngine;
using System.Collections;

public class GarbageCollector : MonoBehaviour {
    public float CleanUpFrequency = 5f;
	// Use this for initialization
	void Start () {
        StartCoroutine(Collect());
	}

    IEnumerator Collect()
    {
        while (true)
        {
            yield return new WaitForSeconds(CleanUpFrequency);
            System.GC.Collect();
        }
    }
}
