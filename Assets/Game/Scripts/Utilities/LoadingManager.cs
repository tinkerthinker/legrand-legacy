﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LoadingManager : MonoBehaviour {
	public static LoadingManager _Instance;
	public ScreenFader ScreenFader;

	[SerializeField]
	private Image _LoadingImage;

	public delegate void LoadingDelegate();
	public LoadingDelegate OnStartLoading;

	public void Start()
	{
		_Instance.ScreenFader.OnFadeIn += StopLoading;
	}

	public static void StartLoading()
	{
		if (_Instance != null) 
		{
			_Instance.ScreenFader.EndScene();
			_Instance.ScreenFader.OnFadeOut += _Instance.StartLoadingAnimation;
		}
	}

	public static void StopLoading()
	{
        _Instance._LoadingImage.gameObject.SetActive (false);
	}

	private void StartLoadingAnimation()
	{
		ScreenFader.OnFadeOut -= StartLoading;

		_LoadingImage.gameObject.SetActive (true);

		if (OnStartLoading != null)
			OnStartLoading ();
	}

	protected void Awake()
	{
		_Instance = this;
	}
}
