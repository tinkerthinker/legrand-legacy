﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Vector2String{
	public string x;
	public string y;

	public Vector2String()
	{
		this.x = string.Empty;
		this.y = string.Empty;
	}

	public Vector2String(string x, string y)
	{
		this.x = x;
		this.y = y;
	}

	public Vector2String(Vector2String NewVector)
	{
		this.x = NewVector.x;
		this.y = NewVector.y;
	}
}
