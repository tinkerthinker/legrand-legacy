﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class VectorStringDouble{
	public string x;
	public double y;
	
	public VectorStringDouble()
	{
		this.x = "";
		this.y = 0;
	}
	
	public VectorStringDouble(string x, double y)
	{
		this.x = x;
		this.y = y;
	}
	
	public VectorStringDouble(VectorStringDouble NewVector)
	{
		this.x = NewVector.x;
		this.y = NewVector.y;
	}
}
