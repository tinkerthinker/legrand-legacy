﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class VectorStringInteger{
	public string x;
	public int y;
	
	public VectorStringInteger()
	{
		this.x = "";
		this.y = 0;
	}
	
	public VectorStringInteger(string x, int y)
	{
		this.x = x;
		this.y = y;
	}
	
	public VectorStringInteger(VectorStringInteger NewVector)
	{
		this.x = NewVector.x;
		this.y = NewVector.y;
	}
}
