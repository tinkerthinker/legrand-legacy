﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Vector2KeycodeString
{
    public KeyCode x;
    public string y;

    public Vector2KeycodeString(KeyCode x, string y)
    {
        this.x = x;
        this.y = y;
    }

    public Vector2KeycodeString(Vector2KeycodeString NewVector)
    {
        this.x = NewVector.x;
        this.y = NewVector.y;
    }
}
