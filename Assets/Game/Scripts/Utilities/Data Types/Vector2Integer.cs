﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Vector2Integer{
	public int x;
	public int y;

	public Vector2Integer()
	{
		this.x = 0;
		this.y = 0;
	}

	public Vector2Integer(int x, int y)
	{
		this.x = x;
		this.y = y;
	}

	public Vector2Integer(Vector2Integer NewVector)
	{
		this.x = NewVector.x;
		this.y = NewVector.y;
	}

	public Vector2Integer(Vector2 FloatVector)
	{
		this.x = (int)FloatVector.x;
		this.y = (int)FloatVector.y;
	}
}
