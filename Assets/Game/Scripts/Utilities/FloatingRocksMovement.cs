﻿using UnityEngine;
using System.Collections;
public class FloatingRocksMovement : MonoBehaviour
{
    public Vector3 movementOffset;
    public float speed;

    private Vector3 _startPosition;
    private Vector3 _endPosition;

    private enum Direction
    {
        Up,
        Down
    }
    private Direction direction;

    void OnEnable()
    {
        _startPosition = transform.position;

        int randStartDir = LegrandUtility.Random((int)Direction.Up, (int)Direction.Down + 1);

        if (randStartDir == (int)Direction.Down)
            _endPosition = _startPosition - movementOffset;
        else
            _endPosition = _startPosition + movementOffset;

        direction = (Direction)randStartDir;
        StartCoroutine(MoveObject());
    }

    IEnumerator MoveObject()
    {
        while(true)
        {
            transform.position = Vector3.MoveTowards(transform.position, _endPosition, speed * Time.deltaTime);
            
            if (transform.position == _endPosition)
            {
                if (direction == Direction.Down)
                { 
                    _endPosition = _startPosition + movementOffset;
                    direction = Direction.Up;
                }
                else
                { 
                    _endPosition = _startPosition - movementOffset;
                    direction = Direction.Down;
                }
            }
                
            yield return null;
        }
    }

    void OnDisable()
    {
        transform.position = _startPosition;
    }
}
