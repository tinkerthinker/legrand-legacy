﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;
public class ImageColorTweener : MonoBehaviour {
    private Image _Image;

    public Color From;
    public Color To;
    public float Time;
    public LoopType loopType;
    public int Loop = -1;
    private Tweener tweener;
    void Awake()
    {
        _Image = LegrandUtility.GetComponentInChildren<Image>(this.gameObject);
    }

    void OnEnable()
    {
        _Image.color = From;
        tweener = _Image.DOColor(To, Time).SetLoops(Loop, loopType);
    }

    void OnDisable()
    {
        if (tweener != null) tweener.Kill(false);
    }
}
