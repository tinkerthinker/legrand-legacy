﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StepListener : MonoBehaviour {
	private TerrainType _CurrentTerrain;

    private GameObject _TerrainPos = null;

	private GameObject 	_LeftFoot;
	private GameObject	_RightFoot;

    // Particles on left foot and right foot
    private ParticleSystem pLeftFoot;
    private ParticleSystem pRightFoot;

	private WwiseManager.WorldFX _CurrentStepSound = WwiseManager.WorldFX.Footstep_Dessert;
	void Start()
	{
		SearchFoot();
	}

	public void ChangeTerrain(TerrainType newTerrainType)
	{
		if (newTerrainType == TerrainType.Rock) {
			_CurrentStepSound = WwiseManager.WorldFX.Footstep_Rock;
		}
		else if (newTerrainType == TerrainType.Sand) {
			_CurrentStepSound = WwiseManager.WorldFX.Footstep_Dessert;
		}
        else if (newTerrainType == TerrainType.Dringrs_Keep)
        {
            _CurrentStepSound = WwiseManager.WorldFX.Footstep_Dringrs_Keep;
        }
    }

    public void ChangeTerrainPos(GameObject terrainPos)
    {
        if(terrainPos != null)
            _TerrainPos = terrainPos;
    }

    public void SetupParticle(ParticleSystem[] Particle)
    {
        if(Particle.Length > 0)
        {
            pLeftFoot = Particle[0];
            pRightFoot = Particle[1];
        }
    }

	public void SearchFoot()
	{
		Transform temp = transform.Search("L_FEET_HLP");
		if(temp != null) _LeftFoot = temp.gameObject;

		temp = transform.Search("R_FEET_HLP");
		if(temp != null) _RightFoot = temp.gameObject;
	}

	public void FootStep(string type)
	{
//		if (this.isActiveAndEnabled) {
//			if (string.Compare (type, "Run", true) == 0)
//				EventManager.Instance.TriggerEvent (new FootStepEvent (true, _LeftFoot));
//			else
//				EventManager.Instance.TriggerEvent (new FootStepEvent (false, _RightFoot));
//		}
	}
	
    /// <summary>
    /// Called on character's animation
    /// </summary>
    /// <param name="side">Side of feet that triggers the sound/particle</param>
	public void FootPrint(string side)
	{
		if (this.isActiveAndEnabled) {
            Vector3 tempPos;

            if (LegrandUtility.CompareString(side, "Left") && _LeftFoot != null)
            {
                if (_TerrainPos != null)
                    tempPos = new Vector3(_LeftFoot.transform.position.x, _TerrainPos.transform.position.y, _LeftFoot.transform.position.z);
                else
                    tempPos = _LeftFoot.transform.position;

                if(pLeftFoot != null)
                {
                    pLeftFoot.transform.position = tempPos;
                    pLeftFoot.Play();
                }
            }
            else if (LegrandUtility.CompareString(side, "Right") && _RightFoot != null)
            {
                if (_TerrainPos != null)
                    tempPos = new Vector3(_RightFoot.transform.position.x, _TerrainPos.transform.position.y, _RightFoot.transform.position.z);
                else
                    tempPos = _RightFoot.transform.position;
                if(pRightFoot != null)
                {
                    pRightFoot.transform.position = tempPos;
                    pRightFoot.Play();
                }
            }

            if (LegrandUtility.CompareString(side, "Left") && _LeftFoot != null)
            {
                WwiseManager.Instance.PlaySFX(_CurrentStepSound, _LeftFoot.gameObject);
                //Debug.Log(_CurrentStepSound.ToString() + " " + _LeftFoot.gameObject.name);
            }
            else if (LegrandUtility.CompareString(side, "Right") && _RightFoot != null)
            {
                WwiseManager.Instance.PlaySFX(_CurrentStepSound, _RightFoot.gameObject);
                //Debug.Log(_CurrentStepSound.ToString() + " " + _RightFoot.gameObject.name);
            }
		}	
	}

	public void Swing()
	{
		WwiseManager.Instance.PlaySFX (WwiseManager.BattleFX.Aria_Attack);
//		SoundManager.PlaySoundEffect ("CharacterSound/Attack_2");
	}
}
