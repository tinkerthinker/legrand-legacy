﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class InputImageManager : MonoBehaviour {
	public List<InputImage> ListInputImage;
}

[System.Serializable]
public class InputImage
{
	public string InputName;
	public Sprite Image;
}