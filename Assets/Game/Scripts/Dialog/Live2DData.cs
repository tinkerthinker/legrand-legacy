﻿using UnityEngine;
using System.Collections;

public class Live2DData{
    int _Side;
    public int Side
    {
        get { return _Side; }
    }

    string _Name;
    public string Name
    {
        get { return _Name; }
    }

    string _Expression;
    public string Expression
    {
        get { return _Expression; }
    }

    string _SyncType;
    public string SyncType
    {
        get { return _SyncType; }
    }

    public Live2DData(string side, string name, string expression, string syncType)
    {
        int.TryParse(side, out _Side);
        _Side--;
        //_Side = int.Parse(side) - 1;
        _Name = name;
        _Expression = expression;
        _SyncType = syncType;
    }
}
