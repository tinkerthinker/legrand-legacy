﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BalloonPopUpRandom : MonoBehaviour {
    public string[] ListDialogID;
    public float PopUpTime;
    public Vector2 Min;
    public Vector2 Max;
    private RandomBalloon[] _Balloons;
    private List<int> _UnactivatedDialog;

    void OnEnable()
    {
        _Balloons = LegrandUtility.GetComponentsInChildren<RandomBalloon>(this.gameObject);
        _UnactivatedDialog = new List<int>();
        for (int i = 0; i < ListDialogID.Length; i++)
            _UnactivatedDialog.Add(i);
    }

    void StartPop()
    {
        if (ListDialogID.Length <= _Balloons.Length)
            StartCoroutine(PopUpEnumerate());
        else
            Debug.Log("Jumlah balon lebih sedikit daripada dialog yang ada");
    }

    IEnumerator PopUpEnumerate()
    {
        int i = 0;
        while(_UnactivatedDialog.Count > 0)
        {
            int nextDialogIndex = _UnactivatedDialog[LegrandUtility.Random(0, _UnactivatedDialog.Count)];
            _Balloons[i].Activate(ListDialogID[nextDialogIndex], Min, Max);
            _UnactivatedDialog.Remove(nextDialogIndex);
            i++;
            yield return new WaitForSeconds(PopUpTime);
        }
    }
}
