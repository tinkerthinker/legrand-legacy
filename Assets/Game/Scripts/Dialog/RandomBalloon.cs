﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class RandomBalloon : MonoBehaviour {
    public Text Text;
    
    public void Activate(string dialogID, Vector2 min, Vector2 max)
    {
        string s = LegrandBackend.Instance.GetLanguageData(dialogID);
        Text.text = s;
        (transform as RectTransform).anchoredPosition = new Vector2( LegrandUtility.Random(min.x, max.x), LegrandUtility.Random(min.y, max.y));
        gameObject.SetActive(true);
    }
}
