﻿using UnityEngine;
using System;
using System.Collections;
using DG.Tweening;

public class Live2DHandler{
    public enum LipSyncType
    {
        repeat,
        openhold,
        close
    }

    LAppModelProxy Live2DAppModel;
    LipSyncType CurrentLipSyncType;
    Tweener Tweener;

    float CurrentLipHeight;
    float TargetLipHeight = 1f;
    float LipOpenTime = 0.2f;

    int FrontLayer;
    int BackLayer;

    public Live2DHandler(LAppModelProxy model)
    {
        Live2DAppModel = model;
        CurrentLipHeight = 0f;
        FrontLayer = 2;
        BackLayer = 0;
    }

    public Live2DHandler(LAppModelProxy model, int frontLayer, int backLayer)
    {
        Live2DAppModel = model;
        CurrentLipHeight = 0f;
        FrontLayer = frontLayer;
        BackLayer = backLayer;
    }

    public void Talking(string JsonPath, string expression, string syncType)
    {
        if (JsonPath != null && JsonPath != "" && Live2DAppModel.path != JsonPath)
        {
            Live2DAppModel.path = JsonPath;
            Live2DAppModel.Init();
        }

        SetEnable(true);
        SetActive(true);
        if(expression != "")
            Live2DAppModel.GetModel().StartMotion(expression, 0, 3);
        else
            Live2DAppModel.GetModel().SetExpression("");

        if (syncType == "") syncType = "repeat";

        StartTalking((LipSyncType)Enum.Parse(typeof(LipSyncType), syncType));
    }

    public void SetEnable(bool active)
    {
        if (!active)
        {
            Live2DAppModel.SetVisible(false);
            Live2DAppModel.GetModel().setLipSyncValue(0f);
        }
        else
        {
            Live2DAppModel.GetModel().setLipSyncValue(0f);
            Live2DAppModel.SetVisible(true);
            Live2DAppModel.GetModel().setLipSync(true);
        }
    }

    public void SetActive(bool active)
    {
        if(active)
            Live2DAppModel.GetModel().getLive2DModel().getDrawParam().setBaseColor(1f, 1f, 1f, 1f);
        else
        {
            Live2DAppModel.GetModel().getLive2DModel().getDrawParam().setBaseColor(1f, 0.5f, 0.5f, 0.5f);
            StartTalking(LipSyncType.close);
        }
    }

    public void StartTalking(LipSyncType syncType)
    {
        if (Tweener != null) Tweener.Kill();

        switch (syncType)
        {
            case LipSyncType.repeat: CurrentLipHeight = 0f; Tweener = DOTween.To(() => CurrentLipHeight, x => CurrentLipHeight = x, TargetLipHeight, LipOpenTime).SetLoops(-1, LoopType.Yoyo).OnUpdate(() => Live2DAppModel.GetModel().setLipSyncValue(CurrentLipHeight)) ; break;
            case LipSyncType.openhold: CurrentLipHeight = 0f; Tweener = DOTween.To(() => CurrentLipHeight, x => CurrentLipHeight = x, TargetLipHeight, LipOpenTime).OnUpdate(() => Live2DAppModel.GetModel().setLipSyncValue(CurrentLipHeight)) ; break;
            case LipSyncType.close: Tweener = DOTween.To(() => CurrentLipHeight, x => CurrentLipHeight = x, 0f, LipOpenTime).OnUpdate(() => Live2DAppModel.GetModel().setLipSyncValue(CurrentLipHeight)); break;
        }

        CurrentLipSyncType = syncType;
    }

    public void StopTalking()
    {
        if (Tweener != null) Tweener.Kill();

        if(CurrentLipSyncType == LipSyncType.repeat)
        {
            StartTalking(LipSyncType.close);
        }
    }

    public void Reset()
    {
        SetEnable(false);
        Live2DAppModel.path = "";
    }
}
