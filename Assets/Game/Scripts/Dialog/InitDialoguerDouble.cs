﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using DG.Tweening;
using Legrand.core;

public class InitDialoguerDouble : MonoBehaviour
{
    public LAppModelProxy live2DAppModel1;
    public LAppModelProxy live2DAppModel2;
    Live2DHandler[] Live2DHandlers;
    public GameObject[] Live2DHolder;

    public int FrontLive2DIndex;
    public int BackLive2DIndex;

    public Image Live2DScreenFader;
    public GameObject LeftDialogCanvas;
    public GameObject MiddleDialogCanvas;
    public GameObject RightDialogCanvas;
    public GameObject NarrationCanvas;

    public Dictionary<string, GameObject> TalkingObjects = new Dictionary<string, GameObject>();

    public Dictionary<string, string> Live2DJson;

    private delegate void DialoguerEventDelegate(string metadata);

    private Dictionary<string, DialoguerEventDelegate> _DialoguerEventDelegates;

    [SerializeField]
    private bool IsTesting;
    // Use this for initialization
    void Awake()
    {
        Dialoguer.Initialize();

        Dialoguer.events.onStarted += HandleonStarted;
        Dialoguer.events.onMessageEvent += MessageEvent;
        Dialoguer.events.onTextPhase += ChangePicture;

        Dialoguer.events.onTextPhase += PlaySoundEffect;

        EventManager.Instance.AddListener<DialogWindowClosed>(Reset);
        EventManager.Instance.AddListener<DialogTextComplete>(UpdateLipSync);
        EventManager.Instance.AddListener<AssignObjectsDialogueEvent>(AssignTalkingObjects);

        Live2DJson = new Dictionary<string, string>();
        //Playable Character
        Live2DJson.Add("finn0", "live2d/finn/Finn-Slave.1024.model.json");
        Live2DJson.Add("finn1", "live2d/finn/finn1.1024.model.json");
        Live2DJson.Add("finn2", "live2d/finn/Finn2.1024.model.json");
        Live2DJson.Add("finn3", "live2d/finn/Finn3.1024.model.json");
        Live2DJson.Add("geddo", "live2d/Geddo/Geddo.1024.model.json");
        Live2DJson.Add("eris", "live2d/Eris/Eris.1024.model.json");
        Live2DJson.Add("aria0", "live2d/Aria/Aria1.1024.model.json");
        Live2DJson.Add("aria1", "live2d/Aria/Aria2.1024.model.json");
        Live2DJson.Add("kael", "live2d/Kael/Kael_1.1024.model.json");
        Live2DJson.Add("azzam0", "live2d/Azzam/Azzam1.1024.model.json");
        Live2DJson.Add("azzam1", "live2d/Azzam/Azzam2.1024.model.json");
        Live2DJson.Add("scatia", "live2d/Scatia/Scatia.1024.model.json");
        //Story NPC
        Live2DJson.Add("gunther", "live2d/Gunther/Gunther.1024.model.json");
        Live2DJson.Add("roland", "live2d/Roland/Roland.1024.model.json");
        Live2DJson.Add("lazarus", "live2d/Lazarus/Lazarus.1024.model.json");
        Live2DJson.Add("callahan", "live2d/NPC/Callahan.1024.model.json");
        //NPC
        Live2DJson.Add("uwil", "live2d/NPC/Hidden Boss - Uwill/HiddenBoss_Uwill.1024.model.json");
        Live2DJson.Add("morieno", "live2d/NPC/Alchemist Morieno.1024.model.json");
        Live2DJson.Add("andoran", "live2d/NPC/Andoran.1024.model.json");
        Live2DJson.Add("bolda", "live2d/NPC/Bolda.1024.model.json");
        Live2DJson.Add("egil", "live2d/NPC/Egil.1024.model.json");
        Live2DJson.Add("jalmanda", "live2d/NPC/Jalmanda.1024.model.json");
        Live2DJson.Add("lantoine", "live2d/NPC/Lantoine.1024.model.json");
        Live2DJson.Add("lucia", "live2d/NPC/Lucia.1024.model.json");
        Live2DJson.Add("rogar", "live2d/NPC/Rogar.1024.model.json");
        Live2DJson.Add("rungdahl", "live2d/NPC/Rungdal.1024.model.json");
        Live2DJson.Add("wayland", "live2d/NPC/Wayland.1024.model.json");

        _DialoguerEventDelegates = new Dictionary<string, DialoguerEventDelegate>();
        _DialoguerEventDelegates.Add("model1", Model1);
        _DialoguerEventDelegates.Add("model2", Model2);
        _DialoguerEventDelegates.Add("sequence", Sequence);
        _DialoguerEventDelegates.Add("itemadd", ItemAdd);
        _DialoguerEventDelegates.Add("itemremove", ItemRemove);
        _DialoguerEventDelegates.Add("live2d", Live2D);
        _DialoguerEventDelegates.Add("quest", Quest);
        _DialoguerEventDelegates.Add("updatequestdescription", LegrandSharedEvents.UpdateQuestDescription);
        _DialoguerEventDelegates.Add("unlockmap", LegrandSharedEvents.UnlockMap);
        _DialoguerEventDelegates.Add("unlockworldmap", LegrandSharedEvents.UnlockWorldMap);
        _DialoguerEventDelegates.Add("popup", LegrandSharedEvents.PopUp);
        _DialoguerEventDelegates.Add("hidecanvas", HideCanvas);
        _DialoguerEventDelegates.Add("hideall", HideAll);
        _DialoguerEventDelegates.Add("gold", Gold);
        _DialoguerEventDelegates.Add("addparty", AddParty);
        _DialoguerEventDelegates.Add("uifx", UIFX);
        _DialoguerEventDelegates.Add("worldfx", WorldFX);
        _DialoguerEventDelegates.Add("cutscenefx", CutsceneFX);
        _DialoguerEventDelegates.Add("stopcutscenefx", StopCutsceneFX);
        _DialoguerEventDelegates.Add("screenfade", ScreenFade);
        _DialoguerEventDelegates.Add("changemodel", ChangeModel);
        _DialoguerEventDelegates.Add("changemapversion", ChangeMapVersion);
        _DialoguerEventDelegates.Add("zoom", Zoom);
        _DialoguerEventDelegates.Add("animlive2d", AnimLive2D);
        _DialoguerEventDelegates.Add("addworldevent", AddWorldEvent);
        _DialoguerEventDelegates.Add("playnpcanimation", PlayNPCAnimation);
        _DialoguerEventDelegates.Add("playanimation", PlayAnimation);
        _DialoguerEventDelegates.Add("playbgm", PlayBGM);
        _DialoguerEventDelegates.Add("stopbgm", StopBGM);
        _DialoguerEventDelegates.Add("stopambience", StopAmbience);
        _DialoguerEventDelegates.Add("instantiate", Instantiate);
        _DialoguerEventDelegates.Add("setactive", SetActive);
        _DialoguerEventDelegates.Add("setplayerstate", this.SetPlayerState);
        _DialoguerEventDelegates.Add("updateglobalmoney", UpdateGlobalMoney);
        _DialoguerEventDelegates.Add("lookat", LookAt);
        _DialoguerEventDelegates.Add("stoplookat", StopLookAt);
        _DialoguerEventDelegates.Add("setcharacteritem", SetCharacterItem);
        _DialoguerEventDelegates.Add("setcharacteractive", SetCharacterActive);
        _DialoguerEventDelegates.Add("rotateto", RotateTo);
        _DialoguerEventDelegates.Add("follow", Follow);
        _DialoguerEventDelegates.Add("stopfollow", StopFollow);
        _DialoguerEventDelegates.Add("initbattletraining", InitBattleTraining);
        _DialoguerEventDelegates.Add("setbgm", SetBGMVolume);
        _DialoguerEventDelegates.Add("setsfx", SetSFXVolume);
        _DialoguerEventDelegates.Add("setambience", SetAmbienceVolume);
        _DialoguerEventDelegates.Add("tutorial", LegrandSharedEvents.TutorialPopUp);
        _DialoguerEventDelegates.Add("setcommand", SetCharacterCommand);
        _DialoguerEventDelegates.Add("setcommandactive", SetCharacterCommandActive);
        _DialoguerEventDelegates.Add("setallcommandactive", SetCharacterAllCommand);
        _DialoguerEventDelegates.Add("setAP", LegrandSharedEvents.SetAP);
        _DialoguerEventDelegates.Add("openshop", LegrandSharedEvents.OpenShop);
        _DialoguerEventDelegates.Add("openblacksmith", LegrandSharedEvents.OpenBlacksmith);
        _DialoguerEventDelegates.Add("openalchemy", LegrandSharedEvents.OpenAlchemy);
        _DialoguerEventDelegates.Add("openfasttravel", LegrandSharedEvents.OpenFastTravel);
        _DialoguerEventDelegates.Add("openstorage", LegrandSharedEvents.OpenStorage);
        if (!IsTesting)
        {
            _DialoguerEventDelegates.Add("addfact", LegrandSharedEvents.AddFact);
            _DialoguerEventDelegates.Add("removefact", LegrandSharedEvents.RemoveFact);
            _DialoguerEventDelegates.Add("query", LegrandSharedEvents.Query);
            _DialoguerEventDelegates.Add("refreshroom", LegrandSharedEvents.RefreshCurrentRoom);
            _DialoguerEventDelegates.Add("getfact", GetFact);
            _DialoguerEventDelegates.Add("playcutscene", LegrandSharedEvents.PlayCutscene);
            _DialoguerEventDelegates.Add("teleport", LegrandSharedEvents.Teleport);
            _DialoguerEventDelegates.Add("changeequipment", ChangeEquipment);
            _DialoguerEventDelegates.Add("startquest", QuestManager.Instance.StartQuest);
            _DialoguerEventDelegates.Add("finishquest", QuestManager.Instance.FinishQuest);
            _DialoguerEventDelegates.Add("finishtask", QuestManager.Instance.FinishTask);
            _DialoguerEventDelegates.Add("setformation", LegrandSharedEvents.SetFormation);
            _DialoguerEventDelegates.Add("storeformation", StoreFormation);
            _DialoguerEventDelegates.Add("loadformation", LoadFormation);
            _DialoguerEventDelegates.Add("addallexp", LegrandSharedEvents.AddAllExp);
            _DialoguerEventDelegates.Add("addallstats", LegrandSharedEvents.AddAllStats);
            _DialoguerEventDelegates.Add("addexp", LegrandSharedEvents.AddExp);
            _DialoguerEventDelegates.Add("setchapter", LegrandSharedEvents.UpdateChapter);
        }


        Live2DHandlers = new Live2DHandler[2];
        Live2DHandlers[0] = new Live2DHandler(live2DAppModel1);
        Live2DHandlers[1] = new Live2DHandler(live2DAppModel2);
    }

    void Start()
    {
        foreach (Live2DHandler l in Live2DHandlers)
            l.SetEnable(false);
    }

    public void AssignTalkingObjects(AssignObjectsDialogueEvent e)
    {
        TalkingObjects.Clear();
        foreach (TalkingObject to in e.TalkingObjects)
        {
            TalkingObjects.Add(to.ID, to.GObject);
        }
    }

    void OnDestroy()
    {
        Dialoguer.events.onStarted -= HandleonStarted;
        Dialoguer.events.onMessageEvent -= MessageEvent;
        Dialoguer.events.onTextPhase -= ChangePicture;
        Dialoguer.events.onTextPhase -= PlaySoundEffect;

        if (EventManager.Instance)
        {
            EventManager.Instance.RemoveListener<AssignObjectsDialogueEvent>(AssignTalkingObjects);
            EventManager.Instance.RemoveListener<DialogWindowClosed>(Reset);
            EventManager.Instance.RemoveListener<DialogTextComplete>(UpdateLipSync);
        }
    }

    void HandleonStarted()
    {
        Dialoguer.SetGlobalBoolean(2, false);
        live2DAppModel1.SetVisible(false);
        live2DAppModel2.SetVisible(false);
        Dialoguer.SetGlobalBoolean(3, false);
        Dialoguer.SetGlobalBoolean(4, false);
    }

    public void MessageEvent(string message, string metadata)
    {
        if (_DialoguerEventDelegates.ContainsKey(message))
            _DialoguerEventDelegates[message](metadata);
    }

    public void ChangePicture(DialoguerTextData data)
    {
        if (Dialoguer.GetGlobalBoolean(2) == false)
        {
            foreach (Live2DHandler l in Live2DHandlers)
                l.SetEnable(false);

            return;
        }

        // Disable semua live2d kalau metadata kosong
        if (data.metadata == "-")
        {
            HideAll(string.Empty);
            return;
        }
        if (string.IsNullOrEmpty(data.metadata))
        {
            for (int i = 0; i < Live2DHandlers.Length; i++)
                Live2DHandlers[i].Reset();
            if (Live2DHolder.Length > 2)
                Live2DHolder[2].SetActive(true); //Index Middle Live2D Holder -> 2
            return;
        }
        else
        {
            if (Live2DHolder.Length > 2)
                Live2DHolder[2].SetActive(false);
        }
        List<int> ActiveLive2d = new List<int>();
        Live2DData[] live2dDatas = Live2DMetaDataParse(data.metadata);
        foreach (Live2DData l in live2dDatas)
        {
            string jsonPath = "";
            Live2DJson.TryGetValue(GetVerifiedNameCode( l.Name.ToLower()), out jsonPath);
            if (jsonPath != null && jsonPath != "" && jsonPath != string.Empty)
            {
                Live2DHandlers[l.Side].Talking(jsonPath, l.Expression.ToLower(), l.SyncType.ToLower());
                Live2DHolder[l.Side].transform.SetSiblingIndex(FrontLive2DIndex);
                ActiveLive2d.Add(l.Side);
            }
        }

        // Live2d yang tidak bicara digelapin
        for (int i = 0; i < Live2DHandlers.Length; i++)
        {
            if (!ActiveLive2d.Contains(i))
            {
                Live2DHandlers[i].SetActive(false);
                Live2DHolder[i].transform.SetSiblingIndex(BackLive2DIndex);
            }
        }

        /*
        int side = int.Parse(data.metadata) - 1;
        Live2DHandlers[side].Talking(Live2DJson[data.name.ToLower()], data.portrait.ToLower(), data.theme.ToLower());
        Live2DHolder[side].transform.SetSiblingIndex(FrontLive2DIndex);

        if (side == 0) side += 1;
        else side -= 1;
        Live2DHandlers[side].SetActive(false);
        Live2DHolder[side].transform.SetSiblingIndex(BackLive2DIndex);
        */

        /*
		switch (data.metadata) {
		case "1":
			
			if (!Live2DJson.ContainsKey (data.name.ToLower ())) {
				live2DAppModel1.SetVisible (false);
			} else {
				if (string.Compare (Live2DJson [data.name.ToLower ()], live2DAppModel1.path, true) != 0) {
					live2DAppModel1.path = Live2DJson [data.name.ToLower ()];
					live2DAppModel1.Init ();
					Dialoguer.SetGlobalBoolean (3, true);
					
				}
			}
			Dialoguer.SetGlobalBoolean (3, true);
			//live2DAppModel1.SetVisible (true);
			live2DAppModel1.GetModel ().getLive2DModel ().getDrawParam ().setBaseColor (1f, 1f, 1f, 1f);
			live2DAppModel2.GetModel ().getLive2DModel ().getDrawParam ().setBaseColor (1f, 0.5f, 0.5f, 0.5f);

			LeftLive2DHolder.transform.SetSiblingIndex(FrontLive2DIndex);
			RightLive2DHolder.transform.SetSiblingIndex(BackLive2DIndex);
			break;
		case "2":
			
			if (!Live2DJson.ContainsKey (data.name.ToLower ())) {
				live2DAppModel2.SetVisible (false);
			} else {
				if (string.Compare (Live2DJson [data.name.ToLower ()], live2DAppModel2.path, true) != 0) {
					live2DAppModel2.path = Live2DJson [data.name.ToLower ()];
					live2DAppModel2.Init ();
					Dialoguer.SetGlobalBoolean (4, true);
					
				}
			}
			Dialoguer.SetGlobalBoolean (4, true);
			//live2DAppModel2.SetVisible (true);
			live2DAppModel2.GetModel ().getLive2DModel ().getDrawParam ().setBaseColor (1f, 1f, 1f, 1f);
			live2DAppModel1.GetModel ().getLive2DModel ().getDrawParam ().setBaseColor (1f, 0.5f, 0.5f, 0.5f);

			LeftLive2DHolder.transform.SetSiblingIndex(BackLive2DIndex);
			RightLive2DHolder.transform.SetSiblingIndex(FrontLive2DIndex);
			break;
		default:
			live2DAppModel1.GetModel ().getLive2DModel ().getDrawParam ().setBaseColor (1f, 0.5f, 0.5f, 0.5f);
			live2DAppModel2.GetModel ().getLive2DModel ().getDrawParam ().setBaseColor (1f, 0.5f, 0.5f, 0.5f);
			break;
		}
		
		if (Dialoguer.GetGlobalBoolean (3))
			live2DAppModel1.SetVisible (true);
		else
			live2DAppModel1.SetVisible (false);
		if (Dialoguer.GetGlobalBoolean (4))
			live2DAppModel2.SetVisible (true);
		else
			live2DAppModel2.SetVisible (false);
		*/
    }

    public void PlaySoundEffect(DialoguerTextData data)
    {
        if (data.audio != "")
        {
            //			SoundManager.PlaySoundEffect(data.audio);
            WwiseManager.WorldFX parsed_enum = (WwiseManager.WorldFX)System.Enum.Parse(typeof(WwiseManager.WorldFX), data.audio);
            WwiseManager.Instance.PlaySFX(parsed_enum);
        }
    }

    public void Reset(DialogWindowClosed d)
    {
        foreach (Live2DHandler l in Live2DHandlers)
            l.Reset();

        Dialoguer.SetGlobalBoolean(2, false);
        if (Live2DScreenFader != null)
        {
            Color tempColor = Live2DScreenFader.color;
            Live2DScreenFader.DOColor(new Color(tempColor.r, tempColor.g, tempColor.b, 0f), 1f);
        }
    }

    public void UpdateLipSync(DialogTextComplete d)
    {
        foreach (Live2DHandler l in Live2DHandlers)
            l.StopTalking();
        /*
        if(Live2DModel1LipSyncType == "Repeat")
        {
            Live2DModel1Tweener.Kill(false);
            live2DAppModel1.GetModel().setLipSync(false);
        }

        if (Live2DModel2LipSyncType == "Repeat")
        {
            Live2DModel2Tweener.Kill(false);
            live2DAppModel2.GetModel().setLipSync(false);
        }
        */
    }

    private Live2DData[] Live2DMetaDataParse(string metadata)
    {
        // metadata : live2dside,name,expression,lipsync|secondlive2dside,secondname,secondexpression,secondlipsync
        string[] live2dDataStrings = metadata.Split('|');
        Live2DData[] result = new Live2DData[live2dDataStrings.Length];
        for (int i = 0; i < live2dDataStrings.Length; i++)
        {
            string[] dataStrings = new string[4] { "", "", "", "" };

            int index = 0;
            foreach (string live2dDataString in live2dDataStrings[i].Split(','))
            {
                dataStrings[index] = live2dDataString;
                index += 1;
            }

            result[i] = new Live2DData(dataStrings[0], dataStrings[1], dataStrings[2], dataStrings[3]);
        }

        return result;
    }

    string GetVerifiedNameCode(string nameCode)
    {
        string verifiedNameCode = nameCode;

        if (PartyManager.Instance)
        {
            switch (nameCode.ToLower())
            {
                case "finn":
                    verifiedNameCode += PartyManager.Instance.StoryProgression.PartyModels[0];
                    if (!Live2DJson.ContainsKey(verifiedNameCode))
                    {
                        verifiedNameCode = nameCode;
                    }
                    break;
                case "aria":
                    verifiedNameCode += PartyManager.Instance.StoryProgression.PartyModels[3];
                    if (!Live2DJson.ContainsKey(verifiedNameCode))
                    {
                        verifiedNameCode = nameCode;
                    }
                    break;
                case "eris":
                    verifiedNameCode += PartyManager.Instance.StoryProgression.PartyModels[2];
                    if (!Live2DJson.ContainsKey(verifiedNameCode))
                    {
                        verifiedNameCode = nameCode;
                    }
                    break;
                case "kael":
                    verifiedNameCode += PartyManager.Instance.StoryProgression.PartyModels[4];
                    if (!Live2DJson.ContainsKey(verifiedNameCode))
                    {
                        verifiedNameCode = nameCode;
                    }
                    break;
                case "azzam":
                    verifiedNameCode += PartyManager.Instance.StoryProgression.PartyModels[5];
                    if (!Live2DJson.ContainsKey(verifiedNameCode))
                    {
                        verifiedNameCode = nameCode;
                    }
                    break;
            }
        }

        return verifiedNameCode;
    }

    public string GetPath(string name)
    {
        if (Live2DJson.ContainsKey(name))
            return Live2DJson[name];
        return "";
    }


    #region event function

    public void Model1(string metadata)
    {
        if (!Live2DJson.ContainsKey(metadata.ToLower()))
        {
            live2DAppModel1.SetVisible(false);
        }
        else
        {
            live2DAppModel1.path = Live2DJson[GetVerifiedNameCode( metadata.ToLower())];
            live2DAppModel1.Init();
            Dialoguer.SetGlobalBoolean(3, true);
        }
    }

    public void Model2(string metadata)
    {
        if (!Live2DJson.ContainsKey(metadata.ToLower()))
        {
            live2DAppModel2.SetVisible(false);
        }
        else
        {
            live2DAppModel2.path = Live2DJson[GetVerifiedNameCode( metadata.ToLower())];
            live2DAppModel2.Init();
            Dialoguer.SetGlobalBoolean(4, true);
        }
    }

    public void Sequence(string metadata)
    {
        Dialoguer.SetGlobalBoolean(5, false);
        if (metadata != "")
            EventManager.Instance.TriggerEvent(new ResumeSequenceEvent(float.Parse(metadata)));
        else
            EventManager.Instance.TriggerEvent(new ResumeSequenceEvent());
    }

    public void ItemAdd(string metadata)
    {
        if (IsTesting)
            return;
        //itemid_amount|itemid_amount
        string[] itemsData = metadata.Split('|');
        for (int i = 0; i < itemsData.Length; i++)
        {
            string[] itemData = itemsData[i].Split('_');
            string itemID = "";
            int amount = 0;

            if (itemData.Length == 1)
            {
                itemID = itemData[0];
                amount = 1;
            }
            else if (itemData.Length == 2)
            {
                itemID = itemData[0];
                amount = int.Parse(itemData[1]);
            }
            PartyManager.Instance.Inventory.AddItems(LegrandBackend.Instance.ItemData[itemID], amount);
        }
    }

    public void ItemRemove(string metadata)
    {
        string[] itemData = metadata.Split('_');
        string itemID = "";
        int amount = 0;

        if (itemData.Length == 1)
        {
            itemID = itemData[0];
            amount = 1;
        }
        else if (itemData.Length == 2)
        {
            itemID = itemData[0];
            amount = int.Parse(itemData[1]);
        }
        PartyManager.Instance.Inventory.DeleteItemsByItemID(itemID, amount);
    }

    public void Live2D(string metadata)
    {
        if (metadata == "true")
        {
            Dialoguer.SetGlobalBoolean(2, true);
            //				Color tempColor = Live2DScreenFader.color;
            //				Live2DScreenFader.DOColor (new Color (tempColor.r, tempColor.g, tempColor.b, 0.5f), 1f).SetUpdate (true);
            //Time.timeScale = 0f;
        }
        else
        {
            Dialoguer.SetGlobalBoolean(2, false);
            foreach (Live2DHandler l in Live2DHandlers)
                l.SetEnable(false);
            //				Color tempColor = Live2DScreenFader.color;
            //				Live2DScreenFader.DOColor (new Color (tempColor.r, tempColor.g, tempColor.b, 0f), 1f).SetUpdate (true);
            //				Time.timeScale = 1f;
        }
    }

    public void Quest(string metadata)
    {
        if (IsTesting)
            return;
    }

    public void HideCanvas(string metadata)
    {
        if (LegrandUtility.CompareString(metadata, "true"))
        {
            LeftDialogCanvas.SetActive(false);
            RightDialogCanvas.SetActive(false);
            MiddleDialogCanvas.SetActive(false);
            NarrationCanvas.SetActive(false);
            PopUpUI.CloseAllPopUp();
        }
        else if (LegrandUtility.CompareString(metadata, "false"))
        {
            LeftDialogCanvas.SetActive(true);
            RightDialogCanvas.SetActive(true);
        }
    }

    public void HideAll(string metadata)
    {
        HideCanvas("true");
        live2DAppModel1.SetVisible(false);
        live2DAppModel2.SetVisible(false);
    }

    public void Gold(string metadata)
    {
        PartyManager.Instance.Inventory.Gold += int.Parse(metadata);
    }

    public void AddParty(string metadata)
    {
        PartyManager.Instance.AddNewCharacter(metadata);
    }
    
    public void StoreFormation(string metadata)
    {
        PartyManager.Instance.StoreFormation();
    }

    public void LoadFormation(string metadata)
    {
        PartyManager.Instance.ApplyStoredFormation();
    }

    public void UIFX(string metadata)
    {
        WwiseManager.UIFX parsed_enum = (WwiseManager.UIFX)System.Enum.Parse(typeof(WwiseManager.UIFX), metadata);
        WwiseManager.Instance.PlaySFX(parsed_enum);
    }

    public void WorldFX(string metadata)
    {
        WwiseManager.WorldFX parsed_enum = (WwiseManager.WorldFX)System.Enum.Parse(typeof(WwiseManager.WorldFX), metadata);
        WwiseManager.Instance.PlaySFX(parsed_enum);
    }

    public void CutsceneFX(string metadata)
    {
        object parsedData = System.Enum.Parse(typeof(WwiseManager.CutsceneFX), metadata);
        WwiseManager.CutsceneFX parsed_enum = (WwiseManager.CutsceneFX)parsedData;
        WwiseManager.Instance.PlaySFX(parsed_enum);
    }

    public void StopCutsceneFX(string metadata)
    {
        object parsedData = System.Enum.Parse(typeof(WwiseManager.CutsceneFX), metadata);
        WwiseManager.CutsceneFX parsed_enum = (WwiseManager.CutsceneFX)parsedData;
        WwiseManager.Instance.StopSFX(parsed_enum);
    }

    public void ScreenFade(string metadata)
    {
        if (LegrandUtility.CompareString(metadata, "out"))
            WorldSceneManager._Instance.ScreenFader.EndScene();
        else if (LegrandUtility.CompareString(metadata, "in"))
            WorldSceneManager._Instance.ScreenFader.StartScene();
    }

    public void ChangeModel(string metadata)
    {
        // Format : [ID_NewModelIndex]
        string[] newModelData = metadata.Split('_');
        EventManager.Instance.TriggerEvent(new ChangeModelEvent(newModelData[0], int.Parse(newModelData[1])));
    }

    public void ChangeMapVersion(string metadata)
    {
        // Format : [ID_NewModelIndex_iscurrentroom]
        string[] newModelData = metadata.Split(',');

        EventManager.Instance.TriggerEvent(new ModRoomVersionEvent(new SubAreaInfo(newModelData[0], newModelData[1]), bool.Parse(newModelData[2])));
    }

    public void Zoom(string metadata)
    {
        EventManager.Instance.TriggerEvent(new CutsceneZoomEvent());
    }

    public void AnimLive2D(string metadata)
    {
        Live2DData[] live2DDatas = Live2DMetaDataParse(metadata);
        string jsonPath = "";
        foreach (Live2DData l in live2DDatas)
        {
            Live2DJson.TryGetValue(GetVerifiedNameCode(l.Name.ToLower()), out jsonPath);
            Live2DHandlers[l.Side].Talking(jsonPath, l.Expression.ToLower(), l.SyncType.ToLower());
        }
    }

    public void AddWorldEvent(string metadata)
    {
        if (PartyManager.Instance && PartyManager.Instance.StoryProgression.ActiveEventID != null)
            PartyManager.Instance.StoryProgression.ActiveEventID.Add(metadata);
    }

    public void PlayAnimation(string metadata)
    {
        // Character name, animation, layer, is crossfade
        string[] datas = metadata.Split(',');
        int layer = -1;
        bool crossfade = false;
        if (datas.Length > 3)
            crossfade = bool.Parse(datas[3]);
        else if (datas.Length > 2)
            layer = int.Parse(datas[2]);

        EventManager.Instance.TriggerEvent(new PlayAnimationEvent(datas[0], datas[1], crossfade, layer));
    }

    public void PlayNPCAnimation(string metadata)
    {
        // Character name, animation, is crossfade, crossfadetime
        string[] datas = metadata.Split(',');
        bool crossfade = false;
        float crossfadeTime = 0f;
        if (datas.Length > 2)
        {
            crossfade = bool.Parse(datas[2]);
            crossfadeTime = float.Parse(datas[3]);
        }

        GameObject target = GlobalGameStatus.GetRegisteredObject(datas[0]);
        if(target != null)
        {
            Animator anim = target.GetComponent<Animator>();
            int hashID = Animator.StringToHash(datas[1]);
            if (!crossfade)
                anim.Play(hashID);
            else
                anim.CrossFade(datas[1], crossfadeTime);
        }
    }

    public void PlayBGM(string metadata)
    {
        WwiseManager.BGM parsed_enum = (WwiseManager.BGM)System.Enum.Parse(typeof(WwiseManager.BGM), metadata);
        WwiseManager.Instance.PlayBG(parsed_enum);
    }

    public void StopBGM(string metadata)
    {
        WwiseManager.Instance.StopBG();
    }

    public void StopAmbience(string metadata)
    {
        WwiseManager.AmbienceFX parsed_enum = (WwiseManager.AmbienceFX)System.Enum.Parse(typeof(WwiseManager.AmbienceFX), metadata);
        WwiseManager.Instance.StopSFX(parsed_enum);
    }

    public void Instantiate(string metadata)
    {
        Instantiate(Resources.Load(metadata));
    }

    public void SetActive(string metadata)
    {
        string[] datas = metadata.Split('|');
        GameObject setObject = GlobalGameStatus.GetRegisteredObject(datas[0]);
        if (setObject != null)
        {
            setObject.SetActive(bool.Parse(datas[1]));
        }
    }

    public void SetPlayerState(string metadata)
    {
        PlayerController.PlayerControllerState newPlayerState = (PlayerController.PlayerControllerState)Enum.Parse(typeof(PlayerController.PlayerControllerState), metadata);
        AreaController.Instance.CurrPlayer.GetComponent<PlayerController>().ControllerState = newPlayerState;
    }

    public void UpdateGlobalMoney(string metadata)
    {
        Dialoguer.SetGlobalFloat(0, PartyManager.Instance.Inventory.Gold);
    }

    public void LookAt(string metadata)
    {
        string[] lookingDatas = metadata.Split(',');
        for (int i = 0; i < lookingDatas.Length; i++)
        {
            string[] datas = lookingDatas[i].Split('|');
            string lookedCharacter = datas[1];
            string[] lookingCharacters = datas[0].Split('_');
            GameObject TargetObject = GlobalGameStatus.Instance.RegisteredObject[lookedCharacter];

            for (int j = 0; j < lookingCharacters.Length; j++)
            {
                GameObject LookingCharacter = GlobalGameStatus.Instance.RegisteredObject[lookingCharacters[j]];
                HeadLookController headController = LookingCharacter.GetComponent<HeadLookController>();
                if (headController == null)
                    return;
                headController.TargetTransform = TargetObject.transform;
                DOTween.To(() => headController.effect, x => headController.effect = x, 1, 1);
            }
        }
    }

    public void StopLookAt(string metadata)
    {
        string[] lookingDatas = metadata.Split(',');
        for (int i = 0; i < lookingDatas.Length; i++)
        {
            GameObject LookingCharacter = GlobalGameStatus.Instance.RegisteredObject[lookingDatas[i]];
            HeadLookController headController = LookingCharacter.GetComponent<HeadLookController>();
            headController.TargetTransform = null;
            DOTween.To(() => headController.effect, x => headController.effect = x, 0, 1);
        }
    }

    public void SetCharacterItem(string metadata)
    {
        if (IsTesting)
            return;
        string[] characterData = metadata.Split(' ');
        MainCharacter targetMC = null;
        for (int i = 0; i < PartyManager.Instance.CharacterParty.Count; i++)
            if (characterData[0] == PartyManager.Instance.CharacterParty[i]._ID)
                targetMC = PartyManager.Instance.CharacterParty[i];

        if (!targetMC)
            return;

        string[] itemsData = characterData[1].Split('|');
        for (int i = 0; i < itemsData.Length; i++)
        {
            string[] itemData = itemsData[i].Split('_');
            targetMC.AddItems(itemData[0], int.Parse(itemData[1]));

        }
    }

    public void SetCharacterActive(string metadata)
    {
        // ID,IsActive|ID 2,IsActive 2
        if (IsTesting)
            return;
        string[] datas = metadata.Split('|');
        if (datas.Length > 0)
        {
            for (int i = 0; i < datas.Length; i++)
            {
                string[] data = datas[i].Split(',');
                if (data.Length == 2)
                {
                    bool isActive = bool.Parse(data[1]);
                    if (isActive)
                        PartyManager.Instance.SetCharacterActive(data[0]);
                    else
                        PartyManager.Instance.SetCharacterNonActive(data[0]);
                }
            }
        }
    }

    public void RotateTo(string metadata)
    {
        //rotatedcharacter,targetcharacter,time
        string[] datas = metadata.Split(',');
        string[] tRotatedCharacters = datas[0].Split('_');
        Transform tTargetCharacter = GlobalGameStatus.Instance.RegisteredObject[datas[1]].transform;

        for (int i = 0; i < tRotatedCharacters.Length; i++)
        {
            Transform tRotatedCharacter = GlobalGameStatus.Instance.RegisteredObject[tRotatedCharacters[i]].transform;
            NPCMovementController npcMovement = tRotatedCharacter.GetComponent<NPCMovementController>();
            if (npcMovement)
                npcMovement.SetLookAt(tTargetCharacter.position, float.Parse(datas[2]));
            else
                Debug.LogError("Dialoguer Rotate To NPCMovementController Does Not Exist in" + tRotatedCharacter.name);
        }
    }

    public void GetFact(string metadata)
    {
        string[] datas = metadata.Split(',');
        Fact fact = PartyManager.Instance.WorldInfo.GetFact(datas[1]);
        if (fact != null)
            Dialoguer.SetGlobalFloat(int.Parse(datas[0]), (float)fact.Value);
    }

    public void Follow(string metadata)
    {
        // Folower,Target
        string[] datas = metadata.Split(',');
        GameObject follower = GlobalGameStatus.GetRegisteredObject(datas[0]);
        GameObject target = GlobalGameStatus.GetRegisteredObject(datas[1]);
        if (follower && target)
        {
            AIPath aiPath = follower.GetComponent<AIPath>();
            aiPath.canMove = true;
            aiPath.canSearch = true;
            aiPath.target = target.transform;
        }
    }

    public void StopFollow(string metadata)
    {
        GameObject follower = GlobalGameStatus.GetRegisteredObject(metadata);
        if (follower)
        {
            AIPath aiPath = follower.GetComponent<AIPath>();
            aiPath.canMove = false;
            aiPath.canSearch = false;
            aiPath.target = null;
        }
    }

    public void ChangeEquipment(string metadata)
    {
        string[] datas = metadata.Split(',');
        MainCharacter mc = PartyManager.Instance.Find(datas[0]);
        if (mc == null) return;
        mc.Equipment().ChangeEquipment(datas[1]);
    }

    public void InitBattleTraining(string metadata)
    {
        string[] datas = metadata.Split('|');
        string battleArea = datas[0];
        string partyId = datas[1];
        string worldDest = datas[2];
        TransitionData tranData = new TransitionData();
        tranData.AfterFinishedGoTo = GameplayType.World;
        tranData.WorldDestination = worldDest;
        BattleAreaData battleAreaData = new BattleAreaData();
        WwiseManager.Instance.StopAll();
        WwiseManager.Instance.PlaySFX(WwiseManager.BattleFX.Battle_transition_wind);
        WorldSceneManager.TransNonWorldToBattle(battleArea, partyId, tranData);
    }

    public void SetBGMVolume(string metadata)
    {
        float time = 0f;
        string[] datas = metadata.Split(',');
        float soundVolume = float.Parse(datas[0]);
        if (datas.Length > 1) time = float.Parse(datas[1]);
        WwiseManager.Instance.SetSoundVolume(WwiseManager.SoundType.BGM, soundVolume, time);
    }

    public void SetSFXVolume(string metadata)
    {
        float time = 0f;
        string[] datas = metadata.Split(',');
        float soundVolume = float.Parse(datas[0]);
        if (datas.Length > 1) time = float.Parse(datas[1]);
        WwiseManager.Instance.SetSoundVolume(WwiseManager.SoundType.SFX, soundVolume, time);
    }

    public void SetAmbienceVolume(string metadata)
    {
        float time = 0f;
        string[] datas = metadata.Split(',');
        float soundVolume = float.Parse(datas[0]);
        if (datas.Length > 1) time = float.Parse(datas[1]);
        WwiseManager.Instance.SetSoundVolume(WwiseManager.SoundType.Ambience, soundVolume, time);
    }

    public void SetCharacterCommand(string metadata)
    {
        //0,Up_Down_CommandType_CommandValue|Up_Down_CommandType_CommandValue
        string[] datas = metadata.Split(',');
        MainCharacter c = PartyManager.Instance.GetCharacter(datas[0]);
        string[] commandDatas = datas[1].Split('|');
        foreach (string commandData in commandDatas)
        {
            string[] commandDataActive = commandData.Split('_');
            CommandPos commandPos1 = (CommandPos)Enum.Parse(typeof(CommandPos), commandDataActive[0]);
            CommandPos commandPos2 = (CommandPos)Enum.Parse(typeof(CommandPos), commandDataActive[1]);
            CommandTypes commandType = (CommandTypes)Enum.Parse(typeof(CommandTypes), commandDataActive[2]);
            int commandValue = int.Parse(commandDataActive[3]);
            c.SetCommand(commandPos1, commandPos2, commandType, commandValue);
        }
    }

    public void SetCharacterCommandActive(string metadata)
    {
        //0,Up_Down_true|Up_Right_true
        string[] datas = metadata.Split(',');
        MainCharacter c = PartyManager.Instance.GetCharacter(datas[0]);
        string[] commandDatas = datas[1].Split('|');
        foreach (string commandData in commandDatas)
        {
            string[] commandDataActive = commandData.Split('_');
            CommandPos commandPos1 = (CommandPos)Enum.Parse(typeof(CommandPos), commandDataActive[0]);
            CommandPos commandPos2 = (CommandPos)Enum.Parse(typeof(CommandPos), commandDataActive[1]);
            if(commandDataActive.Length == 3)
                c.SetCommandStatus(commandPos1, commandPos2, bool.Parse(commandDataActive[2]));
            if (commandDataActive.Length == 4)
                c.SetCommandStatus(commandPos1, commandPos2, bool.Parse(commandDataActive[2]), bool.Parse(commandDataActive[3]));
        }
    }

    public void SetCharacterAllCommand(string metadata)
    {
        //0,false,false
        string[] datas = metadata.Split(',');
        MainCharacter c = PartyManager.Instance.GetCharacter(datas[0]);
        bool active = bool.Parse(datas[1]);
        for (int i = 0; i < c.CommandPresets.Count; i++)
        {
            c.CommandPresets[i].SetCommandActive(active);
            if (datas.Length == 3) c.CommandPresets[i].SetCommandImportant(bool.Parse(datas[2]));
        }
    }
    #endregion
}