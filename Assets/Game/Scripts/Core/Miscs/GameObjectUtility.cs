﻿using UnityEngine;
using System.Collections;

public class GameObjectUtility : MonoBehaviour {
    public GameObject TargetGO;

    public void SetActive()
    {
        TargetGO.SetActive(true);
    }

    public void SetNonActive()
    {
        TargetGO.SetActive(false);
    }
}
