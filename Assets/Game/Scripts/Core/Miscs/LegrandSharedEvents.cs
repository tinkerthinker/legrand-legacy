﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Contain Shared Events Between Dialoguer and other legrand systems.
/// </summary>
public static class LegrandSharedEvents {

	public static void AddFact (string metadata)
	{
		string[] vectorDatas = metadata.Split ('|'); // Name|Value|Owner|ActiveGameObject|InactiveGameObject
		string owner = string.Empty;
		List<string> activeObjectData = new List<string>();
		List<string> inactiveObjectData = new List<string>();
		if (vectorDatas.Length >= 3)
			owner = vectorDatas[2];
		if (vectorDatas.Length >= 4 && !string.IsNullOrEmpty(vectorDatas[3]))
		{
			activeObjectData = new List<string>(vectorDatas[3].Split(','));
		}
		if (vectorDatas.Length >= 5 && !string.IsNullOrEmpty(vectorDatas[4]))
			inactiveObjectData = new List<string> (vectorDatas[4].Split(','));
		PartyManager.Instance.WorldInfo.NewFact(vectorDatas[0], double.Parse(vectorDatas[1]), owner, activeObjectData, inactiveObjectData);
	}

    public static void RemoveFact(string metadata)
    {
        PartyManager.Instance.WorldInfo.RemoveFact(metadata);
    }

    public static void UnlockMap(string metadata)
	{
		string[] unlockedMap = metadata.Split (',');
		EventManager.Instance.TriggerEvent (new UnlockCityPointsEvent (unlockedMap));
	}

    public static void UnlockWorldMap(string metadata)
    {
        string[] unlockedMap = metadata.Split(',');
        EventManager.Instance.TriggerEvent(new UnlockWorldMapPointEvent(unlockedMap));
    }

    public static void PopUp(string metadata)
	{
		Dialoguer.SetGlobalBoolean (5, false);
        string[] metaSplit = metadata.Split('|');
        metadata = metaSplit[0];
        string text = LegrandBackend.Instance.GetLanguageData(metadata);
        if (string.IsNullOrEmpty(text))
            text = metadata;

        text = ParseTextData(text);
        PopUpUI.CallNotifPopUp(text, () => Dialoguer.SetGlobalBoolean(5, true), metaSplit.Length > 1 ? int.Parse(metaSplit[1]) : 0);
	}

    public static void SetFormation(string metadata)
    {
        // [Id character]_[x]_[y],[Id character]_[x]_[y]
        PartyManager.Instance.BattleFormations.Clear();
        string[] vectorDatas = metadata.Split(',');
        foreach (string vectorData in vectorDatas)
        {
            string[] formationData = vectorData.Split('_');
            PartyManager.Instance.BattleFormations.AddFormation(formationData[0], new Vector2(float.Parse(formationData[1]), float.Parse(formationData[2])));
        }
    }

    public static void TutorialPopUp(string metadata)
    {
        Dialoguer.SetGlobalBoolean(5, false);
        PopUpUI.OnEndPopUp += (bool c) => Dialoguer.SetGlobalBoolean(5, true);
        PopUpUI.CallTutorialPopUp(metadata);        
    }

	public static void Query(string metadata)
	{		
		string[] conceptValuePairs = metadata.Split('|'); // concept;value|concept;value|....
		ResponsiveQuery query = new ResponsiveQuery();
		foreach(string conceptValuePair in conceptValuePairs)
		{
			string[] conceptValue = conceptValuePair.Split(';');
			query.Add(conceptValue[0], double.Parse(conceptValue[1]));
		}

		foreach (Fact f in PartyManager.Instance.WorldInfo.LatestFacts) {
			query.Add (f.Name, f.Value);
		}


        if (query.Match())
        {
            DynamicSystemController._Instance.RuleDB.Storing();
            DynamicSystemController._Instance.ResponseDB.ExecuteResponse(DynamicSystemController._Instance.RuleDB.GetLatestResult().ResponseName);
        }
	}

	public static void RefreshCurrentRoom(string metadata)
	{
		AreaController.Instance.CurrRoom.CheckRoomChanges();
	}

	public static void PlayCutscene(string metadata)
	{
		WorldSceneManager.TransWorldToSequence(AreaController.Instance.CurrPrefab, AreaController.Instance.CurrPlayer, metadata);
	}

	public static void Teleport(string metadata)
	{
		string[] datas = metadata.Split(',');
		EventManager.Instance.TriggerEvent(new TeleportPlayerEvent(datas[0], datas[1]));
	}

	public static void UpdateQuestDescription(string metadata)
	{
		// Format [id_quest];[new_description]
		string[] newDescription = metadata.Split (';');
        Quest q = QuestManager.Instance.GetQuestByKey(newDescription[0]);
        if (q != null) q.Description = newDescription[1];

    }

	public static void ResetTask(string metadata){
	}

    public static void SetAP(string metadata)
    {
        //id_AP,id2_AP2
        string[] datas = metadata.Split(',');
        for(int i=0;i<datas.Length;i++)
        {
            string[] APData = datas[i].Split('_');
            Legrand.core.MainCharacter mc = PartyManager.Instance.GetCharacter(APData[0]);
            mc.Gauge = int.Parse(APData[1]);
        }
    }

    #region Shop NPC
    public static void OpenShop(string metadata)
    {
        EventManager.Instance.TriggerEvent(new ShopEvent(true, metadata));
    }

    public static void OpenBlacksmith(string metadata)
    {
        EventManager.Instance.TriggerEvent(new CraftingShopEvent(true, LegrandBackend.Instance.GetMerchantLevel(metadata)));
    }

    public static void OpenAlchemy(string metadata)
    {
        EventManager.Instance.TriggerEvent(new AlchemistShopEvent(true, LegrandBackend.Instance.GetMerchantLevel(metadata)));
    }

    public static void OpenFastTravel(string metadata)
    {
        EventManager.Instance.TriggerEvent(new FastTravelUIEvent(true));
    }

    public static void OpenStorage(string metadata)
    {
        EventManager.Instance.TriggerEvent(new StorageUIEvent(true));
    }
    #endregion

    #region EXP
    public static void AddAllExp(string metadata)
    {
        int exp = int.Parse(metadata);
        foreach (Legrand.core.MainCharacter mc in PartyManager.Instance.CharacterParty)
            mc.IncreaseEXP(exp);
        foreach (Legrand.core.MainCharacter mc in PartyManager.Instance.NonActiveParty)
            mc.IncreaseEXP(exp);
    }

    //  Example
    // ATTRIBUTETYPE_value,ATTRIBUTETYPE_value
    public static void AddAllStats(string metadata)
    {
        string[] numquery = metadata.Split(',');
        foreach(string query in numquery)
        {
            string[] datas = query.Split('_');
            Legrand.core.Attribute attype = (Legrand.core.Attribute)System.Enum.Parse(typeof(Legrand.core.Attribute), datas[0]);
            int value = int.Parse(datas[1]);
            PartyManager.Instance.AddStatstoCharacters(attype, value);
        }
    }

    public static void AddExp(string metadata)
    {
        // id,exp|id,exp
        string[] dataExpPerPlayer = metadata.Split('|');
        for(int i=0;i<dataExpPerPlayer.Length;i++)
        {
            string[] dataExp = dataExpPerPlayer[i].Split(',');
            PartyManager.Instance.GetCharacter(dataExp[0]).IncreaseEXP(int.Parse(dataExp[1]));
        }
    }
    #endregion

    #region text dialog parser
    public static string ParseTextData(string sourceString)
    {
        string finalString = sourceString;
        bool repeatCheck = true;
        while (repeatCheck)
        {
            bool found = false;
            if (finalString.Contains("[item]") && finalString.Contains("[/item]"))
            {
                int indexStart = finalString.IndexOf("[item]");
                string itemID = finalString.Substring(indexStart + 6, finalString.IndexOf("[/item]") - (indexStart + 6));
                finalString = finalString.Remove(indexStart, (finalString.IndexOf("[/item]") + 7) - indexStart);
                finalString = finalString.Insert(indexStart, LegrandBackend.Instance.ItemData[itemID].Name);
                found = true;
            }
            else if (finalString.Contains("[equipment]") && finalString.Contains("[/equipment]"))
            {
                int indexStart = finalString.IndexOf("[equipment]");
                string equipmentID = finalString.Substring(indexStart + 11, finalString.IndexOf("[/equipment]") - (indexStart + 11));
                finalString = finalString.Remove(indexStart, (finalString.IndexOf("[/equipment]") + 12) - indexStart);
                finalString = finalString.Insert(indexStart, LegrandBackend.Instance.GetEquipment(equipmentID).Name);
                found = true;
            }
            if (!found) repeatCheck = false;
        }
        return finalString;
    }
    #endregion

    #region Party Manager
    public static void UpdateChapter(string newChapter)
    {
        PartyManager.Instance.State.currChapter = int.Parse(newChapter);
    }
    #endregion
}