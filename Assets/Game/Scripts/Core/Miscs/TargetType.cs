﻿using UnityEngine;
using System.Collections;

public enum TargetType
{
    Single,
    Row,
    Column,
    Square,
    All
}

public enum TargetFormation
{
	Ally,
	Enemy,
	All,
	Self
}