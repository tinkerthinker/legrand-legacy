﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class ModelChanger : MonoBehaviour
{
    [ContextMenuItem("Change model", "Change")]
    public int changeModelIndexTest;
    public bool test = false;
    void Change()
    {
        ChangeModel(changeModelIndexTest);
    }
    
	public string ModelID;
	[SerializeField]
	protected int _CurrentModelSet;
	public int CurrentModelSet
	{
		get { return _CurrentModelSet;}
	}

	public List<RuntimeAnimatorController> AnimatorControllers;
	public List<GameObject> ModelSets;
	public List<GameObject> AvoidToDestroy;

    void Awake()
    {
        UpdateToNewest(true);
    }

	void Start()
	{
		if(EventManager.Instance != null)
			EventManager.Instance.AddListener<ChangeModelEvent>(ChangeModel);
	}

    void OnEnable()
    {
        UpdateToNewest();
    }

    void UpdateToNewest(bool forced = false)
    {
        int newestCharacterSet = PartyManager.Instance.StoryProgression.GetLatestModelSet(ModelID);
        if (test)
            newestCharacterSet = changeModelIndexTest;
        if (forced || newestCharacterSet != _CurrentModelSet)
            ChangeModel(newestCharacterSet);
    }

	void OnDestroy()
	{
		if(EventManager.Instance != null)
			EventManager.Instance.RemoveListener<ChangeModelEvent>(ChangeModel);
	}

	public virtual void ChangeModel(int SetIndex)
	{
		foreach(GameObject needToDestoy in FilterDestroyGameobject())
		{
			DestroyImmediate(needToDestoy);
		}

		GameObject newModel = (GameObject)Instantiate(ModelSets[SetIndex]);
		int children = newModel.transform.childCount;
		while(newModel.transform.childCount > 0)
		{
			Transform child = newModel.transform.GetChild(0);
			Vector3 tempPosition = child.localPosition;
			Vector3 tempRotation = child.localEulerAngles;
			Vector3 tempScale = child.localScale;
			child.SetParent(this.transform, true);
			child.localPosition = tempPosition;
			child.localEulerAngles = tempRotation;
			child.localScale = tempScale;
		}
		Animator anim = LegrandUtility.GetComponentInChildren<Animator>(this.gameObject);
		AnimatorOverrideController overrideController = new AnimatorOverrideController();
		overrideController.runtimeAnimatorController = AnimatorControllers[SetIndex];
		anim.runtimeAnimatorController = null;			// Unity 5.2-5.3 override s	animator bug Bug Solution
		anim.runtimeAnimatorController = overrideController;
		Destroy(newModel);
		_CurrentModelSet = SetIndex;
		PartyManager.Instance.RebindCharacterAnimator(anim);

		WeaponSlots weapon = LegrandUtility.GetComponentInChildren<WeaponSlots>(this.gameObject);
		if(weapon != null) 
		{
			weapon.SearchWeaponSlot();
			weapon.PutWeaponDefault();
		}

		ObscurableQueue renderHelper = LegrandUtility.GetComponentInChildren<ObscurableQueue>(this.gameObject);
		if (renderHelper != null)
			renderHelper.RestartRender ();

		StepListener stepListener = LegrandUtility.GetComponentInChildren<StepListener>(this.gameObject);
		if (stepListener != null)
			stepListener.SearchFoot ();

		PlayerController playercontroller = LegrandUtility.GetComponentInChildren<PlayerController>(this.gameObject);
		if (playercontroller != null)
			playercontroller.GetBIP ();
	}

	private void ChangeModel(ChangeModelEvent ev)
	{
		if(LegrandUtility.CompareString(ModelID, ev.ModelId))
		{
			ChangeModel(ev.SetIndex);
		}
	}

	protected List<GameObject> FilterDestroyGameobject()
	{
		List<GameObject> candidate = new List<GameObject>();
		int children = transform.childCount;
		for (int i = 0; i < children; ++i)
			candidate.Add(transform.GetChild(i).gameObject);

		foreach(GameObject avoidedGameobject in AvoidToDestroy)
		{
			candidate.Remove(avoidedGameobject);
		}

		return candidate;
	}
}