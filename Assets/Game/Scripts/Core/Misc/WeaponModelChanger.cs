using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class WeaponModelChanger : ModelChanger
{

	public override void ChangeModel(int SetIndex)
	{
		foreach(GameObject needToDestoy in FilterDestroyGameobject())
		{
			DestroyImmediate(needToDestoy);
		}

		GameObject newModel = (GameObject)Instantiate(ModelSets[SetIndex]);
        newModel.transform.SetParent(this.transform, true);
		_CurrentModelSet = SetIndex;
	}
}