﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Legrand.core
{
    public enum Attribute
    {
		STR, VIT, INT, LUCK, AGI,  
        BaseAttack,
        BaseDefense,
        CriticalDamage,
        DamageMultiplier,
        DamageRadius,
    	TravelDistance,
    	SkillCooldown,
        EvasionRate,
        Health,
		StatusResistance,
        AttackSpeed,
        CriticalRate,
        MoveSpeed,
        AllowAttack,
        AllowCast,
        Visible,
        TileMove,
        MaxTroops,
        ReflectAttack,
        DrainHealth,
        AllowMagic,
        HitRate,
        Eva,
        AttackAlly,
        AllowMove,
        Scan,
    	TilePosition,
    	TileAttack,
    	PlainMove,
    	ForestMove,
    	DesertMove,
    	MountainMove,
    	FortMove,
		PhysicalAttack,
		PhysicalDefense,
		MagicAttack,
		MagicDefense,
		Accuracy,
		ReviveHealth,
		BaseDamage,
		CriticalResist,
        DEF
    };

    public enum WarAttribute
    {
        STR,
        DEF,
        AGI,
        Troop,
        RangeAttack,
        CanMoveHidden,
        CanAttackHidden,
        ACRegen
    };

    public enum MiscAttribute
    { 
        EncounterRate,
		InventoryWeight,
        Blueprint
    };

    [System.Serializable]
    public class BattleAttribute 
    {
        public int _ID;
        public string _CharacterID;

        public int _Attribute; //Attribute
        public int _Value;
        public int _Experiences;
        public string _Description;
        
        public BattleAttribute()
        {
        	_ID = 0;
        	_Attribute = 0;
        	_Value = 0;
        	_Experiences = 0;
        	_Description = "";
        }
        

		public BattleAttribute(string characterID, int attribute, int value, int experiences, string description)
		{
			_ID = -1;
			_CharacterID = characterID;
			_Attribute = attribute;
			_Value = value;
			_Experiences = experiences;
			_Description = description;
		}
    }

	[System.Serializable]
	public class WorldAttribute
	{
		public int _ID;
		public int _MiscAttribute;
		public int _Value;
    }

    [System.Serializable]
    public class AttributeValue
    {
        public Attribute AttributeType;
        public float value;

        public AttributeValue()
        {
            AttributeType = Attribute.STR;
            value = 0;
        }

        public AttributeValue(Attribute newAtt, float newValue)
        {
            AttributeType = newAtt;
            value = newValue;
        }
    }
}
