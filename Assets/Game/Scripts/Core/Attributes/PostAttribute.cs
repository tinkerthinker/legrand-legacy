﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using Legrand.core;
////using UnityEngine;
//
//// Handle serialization and calculation dynamic attribute modifier
//// Actual coroutine behavior is on Character class
//// Which is: Buff, Debuff, Status
//public class PostAttributes 
//{
//    // Buffs and debuffs
//    
//    private List<Buff> _Buffs;
//    private Dictionary<int, int> _BuffTurns;
//    
//    private Dictionary<Attribute, object> _BuffModifiers;
//    public Dictionary<Attribute, object> BuffModifiers
//    {
//        get { return _BuffModifiers; }
//    }
//    
//    // Status
//    
//    // Default is initializing arrays
//    public PostAttributes()
//    {
//        _Buffs = new List<Buff>();
//        _BuffTurns = new Dictionary<int, int>();
//        _BuffModifiers = new Dictionary<Attribute, object>();
//    }
//    
//    #region Buffs - Debuffs
//    
//    public bool IsBuffActive(int id)
//    {
//        foreach (Buff b in _Buffs)
//            if (b._ID == id) return true;
//        
//        return false;
//    }
//    
//    public int GetTurn(int id)
//    {
//        int turns;
//        bool exists = _BuffTurns.TryGetValue(id,out turns);
//        
//        return (exists) ? turns : -1;
//    }
//    
//    public void UpdateTurn(int id, int turns)
//    {
//        _BuffTurns[id] = turns;
//    }
//    
//    public void ApplyBuff(Buff buff)
//    {
//        ApplyBuffAttributes(buff);
//        
//        // Start buff
//        _Buffs.Add(buff);
//        _BuffTurns.Add(buff._ID, buff._Turns);
//    }
//    
//    void ApplyBuffAttributes(Buff buff)
//    {
//        foreach (Buff.AttributeModifier modifier in buff.AttributeModifiers)
//        {
//            Attribute attr = (Attribute)modifier._Attribute;
//            if (_BuffModifiers.ContainsKey(attr))
//                _BuffModifiers[attr] = (float)_BuffModifiers[attr] + modifier._Value;
//            else
//                _BuffModifiers.Add(attr, modifier._Value);
//        }
//    }
//    
//    public void RemoveBuff(Buff buff)
//    {
//        RemoveBuffAttributes(buff);
//        
//        // Done
//        _BuffTurns.Remove(buff._ID);
//        _Buffs.Remove(buff);
//    }
//    
//    void RemoveBuffAttributes(Buff buff)
//    {
//        foreach (Buff.AttributeModifier modifier in buff.AttributeModifiers)
//        {
//            Attribute attr = (Attribute)modifier._Attribute;
//            _BuffModifiers[attr] = (float)_BuffModifiers[attr] - modifier._Value;
//            if ((float)_BuffModifiers[attr] == 0f)
//                _BuffModifiers.Remove(attr);
//        }
//    }
//    
//    #endregion
//}
