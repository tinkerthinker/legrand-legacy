﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Legrand.core;

public enum BuffType
{
	BASIC_PRIMARY,
	BASIC_SECONDARY,
	OVER_TIME,
	REFLECT,
	VAMPIRE,
	ACTION,
	ACTION_SKIP,
	INVINCIBLE,
	CHANCE_DEAL,
	RANDOM_TARGET,
    HEAL,
    DEATH,
    PROVOKE,
    ACTION_POINT,
    REFLECT_HEAL
}

public enum BuffEvent
{
	ON_ATTACK,
	ON_HITTED,
	ON_TURN_START,
	ON_BATTLE_START,
	EVERYTIME,
	ON_CAST,
	ON_INIT
}

[System.Serializable]
public class Buff
{
    public int _ID;
    public int _MetaId;
    public string Name;
	public BuffType Type;
	public BuffEvent EventType;
    public string Description;
    public int Turns;

	public float value;
	public List<AttributeValue> BuffPrimary;
	public List<AttributeValue> BuffSecondary;
	public List<CommandTypes> ActionsBuffEnum;
	public Status StatusBuff;
	public StatusType StatusType;
	public bool Stackable;
	public int BuffInflictID;

	public Buff()
	{
		_ID = 0;
        _MetaId = 0;
		Name = "";
		Type = BuffType.BASIC_PRIMARY;
		EventType = BuffEvent.ON_ATTACK;
		Description = "";
		Turns = 0;
		
		value = 0;
		StatusBuff = Status.None;
		StatusType = StatusType.Buff;
		Stackable = false;
		BuffInflictID = 0;
		BuffPrimary = new List<AttributeValue>();
		BuffSecondary = new List<AttributeValue>();
		ActionsBuffEnum = new List<CommandTypes>();
	}

	public static Buff Copy (Buff oldBuff)
	{
		Buff newBuff = new Buff();
		newBuff._ID 			= oldBuff._ID;
        newBuff._MetaId         = oldBuff._MetaId;
        newBuff.Name 			= oldBuff.Name;
		newBuff.Type			= oldBuff.Type;
		newBuff.EventType 		= oldBuff.EventType;
		newBuff.Description 	= oldBuff.Description;
		newBuff.Turns 			= oldBuff.Turns;
		newBuff.value 			= oldBuff.value;
		newBuff.BuffPrimary		= oldBuff.BuffPrimary;
		newBuff.BuffSecondary 	= oldBuff.BuffSecondary;
		newBuff.ActionsBuffEnum	= oldBuff.ActionsBuffEnum;
		newBuff.StatusBuff		= oldBuff.StatusBuff;
		newBuff.StatusType		= oldBuff.StatusType;
		newBuff.Stackable		= oldBuff.Stackable;
		newBuff.BuffInflictID 	= oldBuff.BuffInflictID;


		return newBuff;
	}
}
