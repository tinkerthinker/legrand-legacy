﻿using System.Collections.Generic;
using UnityEngine;
using Legrand.core;

[System.Serializable]
public class LockedMagic  
{
    public int PreviousMagicId;
    public Magic Magic;
    public int CharacterClassLevel;
    public List<AttributeValue> AttributeRequirements;

    public LockedMagic()
    {
        PreviousMagicId = -1;
        CharacterClassLevel = 1;
        Magic = new Magic();
        AttributeRequirements = new List<AttributeValue>();
    }

    public LockedMagic(int upgradeMagicId, int requiredClassLvl, Magic willBeLearnedMagic, List<AttributeValue> attributeRequirements)
	{
        PreviousMagicId = upgradeMagicId;
        CharacterClassLevel = requiredClassLvl;
        Magic = willBeLearnedMagic;
        AttributeRequirements = new List<AttributeValue>(attributeRequirements);
    }

    public bool IsRequirementFulfilled(int currentClassLevel, Dictionary<Attribute, object> currentAttribute)
    {
        if (CharacterClassLevel > currentClassLevel) return false;

        if (AttributeRequirements.Count == 0) return true;
        for(int i=0;i<AttributeRequirements.Count;i++)
        {
            if ((int)currentAttribute[AttributeRequirements[i].AttributeType] < AttributeRequirements[i].value) return false;
        }
        return true;
    }
}
