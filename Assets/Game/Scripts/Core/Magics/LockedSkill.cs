﻿using System.Collections.Generic;
using UnityEngine;
using Legrand.core;

[System.Serializable]
public class LockedSkill
{
    public string PreviousSkillId;
    public NormalSkill Skill;
    public int CharacterClassLevel;
    public List<AttributeValue> AttributeRequirements;

    public LockedSkill()
    {
        PreviousSkillId = string.Empty;
        CharacterClassLevel = 1;
        Skill = new NormalSkill();
        AttributeRequirements = new List<AttributeValue>();
    }

    public LockedSkill(string skillId, int requiredClassLvl, NormalSkill willBeLearnedSkill, List<AttributeValue> attributeRequirements)
	{
        PreviousSkillId = skillId;
        CharacterClassLevel = requiredClassLvl;
        Skill = willBeLearnedSkill;
        AttributeRequirements = new List<AttributeValue>(attributeRequirements);
    }

    public bool IsRequirementFulfilled(int currentClassLevel, Dictionary<Attribute, object> currentAttribute)
    {
        if (CharacterClassLevel > currentClassLevel) return false;

        if (AttributeRequirements.Count == 0) return true;
        for(int i=0;i<AttributeRequirements.Count;i++)
        {
            if ((int)currentAttribute[AttributeRequirements[i].AttributeType] < AttributeRequirements[i].value) return false;
        }
        return true;
    }
}
