﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Legrand.core;

[System.Serializable]
public class CharacterMagicPreset
{
    public Magic MagicPreset;
    public bool IsNameDefault;
    public int _ID;
	public string CharacterId;

    public CharacterMagicPreset(int id, Magic magic, bool isNameDefault)
    {
        _ID = id;
        MagicPreset = magic;
        IsNameDefault = isNameDefault;
    }   

	public CharacterMagicPreset(int id, string CharacterId ,Magic magic, bool isNameDefault)
	{
		_ID = id;
		this.CharacterId = CharacterId;
		MagicPreset = magic;
		IsNameDefault = isNameDefault;
	}

	public void SetParticleMagic(CharacterAttackType attackType)
	{
		if (attackType == CharacterAttackType.Melee) {
			MagicPreset.AttackMagicEffect = BattleParticleEffect.GenerateMeleeMagicCharacterEffect (MagicPreset.Element, MagicPreset.MagicTargetType);
		} else {
			MagicPreset.AttackMagicEffect = BattleParticleEffect.GenerateRangeMagicEffect (MagicPreset.Element, MagicPreset.MagicTargetType);
		}

		MagicPreset.CastingMagicEffect = BattleParticleEffect.GenerateMeleeCastingMagicCharacterEffect (MagicPreset.Element, MagicPreset.MagicTargetType);
	}

//	IEnumerator SetParticle()
//	{
//		while (!PartyManager.Instance._ReadyToPlay)
//			yield return null;
//		MainCharacter selectedChar = PartyManager.Instance.GetCharacter (CharacterId);
//		MagicPreset.CastingMagicEffect = "";
//		if (selectedChar != null) {
//			if (selectedChar.CharacterAttackType == CharacterAttackType.Melee) {
//				//		if (MagicPreset.AttackMagicEffect == "")
//				MagicPreset.AttackMagicEffect = BattleParticleEffect.GenerateMeleeMagicCharacterEffect (magic.Element, magic.MagicTargetType);
//			} else {
//				MagicPreset.AttackMagicEffect = BattleParticleEffect.GenerateRangeMagicEffect (magic.Element, magic.MagicTargetType);
//			}
//		}
//	}

	public CharacterMagicPreset(int id, string magicName, string magicDescription, Element magicElement ,TargetType magicTargetType, float magicDamageModifier, float interuptChance, string castingEffect, string attackEffect, string hitEffect, bool isNameDefault)
    {
        _ID = id;
        MagicPreset = new Magic();
        MagicPreset.Name = magicName;
        MagicPreset.Description = magicDescription;
        MagicPreset.Element = magicElement;
        MagicPreset.MagicDamageModifier = magicDamageModifier;
        MagicPreset.InteruptChance = interuptChance;
        MagicPreset.MagicTargetType = magicTargetType;
		MagicPreset.CastingMagicEffect = castingEffect;
		MagicPreset.AttackMagicEffect = attackEffect;
		MagicPreset.HitEffect = hitEffect;
        IsNameDefault = isNameDefault;
    }   
}

[System.Serializable]
public class CharacterMagicPresets
{
	private Dictionary<int, bool> _IDMagicPresetDictionary;

    [SerializeField]
    private List<CharacterMagicPreset> _MagicPresets;
    public int MaxMagicPreset = 16;
    public List<CharacterMagicPreset> MagicPresets
    {
        get { return _MagicPresets; }
    }
    
    public CharacterMagicPresets()
    {
		_IDMagicPresetDictionary = new Dictionary<int, bool> ();
        _MagicPresets  = new List<CharacterMagicPreset>();
    }

    public CharacterMagicPreset GetMagicPreset(int magicPresetID)
    {
        for (int i = 0; i < MagicPresets.Count; i++)
        {
            if (MagicPresets[i]._ID == magicPresetID)
            {
                return MagicPresets[i];
            }
        }
        return null;
    }

    public bool ChangeMagicTargetType(int magicID, TargetType newTargetType)
    {
        for (int i = 0; i < MagicPresets.Count; i++)
        {
            if (MagicPresets[i]._ID == magicID)
            {
                MagicPresets[i].MagicPreset.MagicTargetType = newTargetType;
                
                return true;
            }
        }
        
        return false;
    }

    public bool ChangeMagicDamageModifier(int magicID, int newDamageModifier)
    {
        for (int i = 0; i < MagicPresets.Count; i++)
        {
            if (MagicPresets[i]._ID == magicID)
            {
                MagicPresets[i].MagicPreset.MagicDamageModifier = newDamageModifier;

                return true;
            }
        }
        
        return false;
    }

	public bool ChangeMagicPreset(int magicID, string name, string description, TargetType targetType, float damageModifier, float interruptModifier, bool isNameDefault)
	{
        for(int i=0;i< MagicPresets.Count;i++)
		{
			if (MagicPresets[i]._ID == magicID)
			{
                CharacterMagicPreset magic = MagicPresets[i];
                magic.MagicPreset.Name = name;
				magic.MagicPreset.Description = description;
				magic.MagicPreset.MagicTargetType = targetType;
				magic.MagicPreset.MagicDamageModifier = damageModifier;
                magic.MagicPreset.InteruptChance = interruptModifier;
                magic.IsNameDefault = isNameDefault;
				return true;
			}
		}
		
		return false;
	}

	public bool ChangeMagicPresetElement(int magicID, Element element)
	{
		foreach (CharacterMagicPreset magic in MagicPresets)
		{
			if (magic._ID == magicID)
			{
				magic.MagicPreset.Element = element;
				return true;
			}
		}

		return false;
	}

    #region LearnMagicWithGeneratedID
    public int GenerateID()
    {
        if(MagicPresets.Count >= 0)
        {
			for(int i = 1; i<MaxMagicPreset; i++)
			{
				if(!_IDMagicPresetDictionary.ContainsKey(i) || (_IDMagicPresetDictionary.ContainsKey(i) && _IDMagicPresetDictionary[i] == false))
				{
					return i;
				}
			}
            	
        }
        return -1;
    }

	public void SetID(int newId)
	{
		if(!_IDMagicPresetDictionary.ContainsKey(newId))
		{
			_IDMagicPresetDictionary.Add(newId, true);
		}
		else if(_IDMagicPresetDictionary.ContainsKey(newId) && _IDMagicPresetDictionary[newId] == false)
		{
			_IDMagicPresetDictionary[newId] = true;
		}
	}

    //learn magic in game from save data
	public bool LearnMagicPreset(Magic magic, string CharacterId, bool isNameDefault)
	{
		if (MagicPresets.Count < MaxMagicPreset)
		{
			int newId = GenerateID();
			SetID(newId);
			MagicPresets.Add(new CharacterMagicPreset(newId, CharacterId, magic, isNameDefault));

			MainCharacter selectedChar = PartyManager.Instance.GetCharacter (CharacterId);
			if (selectedChar != null) {
				MagicPresets [MagicPresets.Count - 1].SetParticleMagic (selectedChar.CharacterAttackType);
			}
			return true;
		}
		return false;
	}
    #endregion

    #region LearnMagicWithID
    //deserialize from database and save
    public bool LearnMagicPreset(int id, string CharacterId,Magic magic, bool isNameDefault)
	{
		if (MagicPresets.Count < MaxMagicPreset)
		{
			SetID(id);
			MagicPresets.Add(new CharacterMagicPreset(id, CharacterId,magic, isNameDefault));
			return true;
		}
		return false;
	}
	//Learn Magic encounter
	public bool LearnMagicPreset(int id, string name, string description, Element element, TargetType targetType, float damageModifier, int interuptChance, string castingEffect, string attackEffect, string hitEffect, bool isNameDefault)
    {
        if(MagicPresets.Count < MaxMagicPreset)
        {
			SetID(id);
			MagicPresets.Add(new CharacterMagicPreset(id, name, description, element, targetType,damageModifier, interuptChance, castingEffect, attackEffect, hitEffect, isNameDefault));
            return true;
        }
       return false;
    }
    #endregion

	public bool UnlearnMagicPreset(int id)
	{
		for (int i=0; i< MagicPresets.Count;i++ )
        {
            if (MagicPresets[i]._ID == id)
			{
				MagicPresets.Remove(MagicPresets[i]);
				_IDMagicPresetDictionary[id] = false;
				return true;
			}
		}
		return false;
	}
}