﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Magic  
{
    public static readonly int MaxAttackModifier = 4;
    public static readonly int MaxDelayModifier = 8;
    public static readonly int MaxCostModifier = 8;

    // General description
    public int ID;
    public string Name = "Default";
    public string Description;
    public float MagicDamageModifier;
    public float InteruptChance;
    public float HitPercentage;
    public float CritPercentage;

    public Element Element = Element.None;

    public TargetType MagicTargetType;
   
	public string CastingMagicEffect;
	public string AttackMagicEffect;
	public string HitEffect;
    public List<BuffChance> BuffEffect;

	public Magic()
	{
        ID = 0;
        Name = "Default";
        Description = "";
        MagicDamageModifier = 100f;
        InteruptChance = 65;
        HitPercentage = 90f;
        CritPercentage = 5f;

        Element = Element.None;

        MagicTargetType = TargetType.Single;

        CastingMagicEffect = "";
        AttackMagicEffect = "";
        HitEffect = "";
        BuffEffect = new List<BuffChance>();
	}

	public Magic(string magicName, string magicDescription, Element magicElement ,TargetType magicTargetType, float magicDamageModifier, float interuptChance,string castingEffect, string attackEffect, string hitEffect, List<BuffChance> buffEffect)
	{
		Name = magicName;
		Description = magicDescription;
		Element = magicElement;
		MagicDamageModifier = magicDamageModifier;
		InteruptChance = interuptChance;
		CastingMagicEffect = castingEffect;
		AttackMagicEffect = attackEffect;
		MagicTargetType = magicTargetType;
		HitEffect = hitEffect;
        BuffEffect = buffEffect != null ? new List<BuffChance>(buffEffect) : null;
	}
}

