﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class BuffChance
{
    public float Chance;
    public Buff BuffEffect;

    public BuffChance()
    {
        Chance = 0;
        BuffEffect = new Buff();
    }

}
