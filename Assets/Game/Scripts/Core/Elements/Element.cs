﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterElement
{
	private List<Element> _Elements;
	public List<Element> Elements
	{
		get{return _Elements;}
	}

	private Element _Weakness;
	public Element Weakness
	{
		get{return _Weakness;}
	}

	private Element _Stronger;
	public Element Stronger
	{
		get{return _Stronger;}
	}

	public CharacterElement()
	{
		_Elements = new List<Element> ();
	}

	public void AddElement(Element newElement)
	{
		bool basicElement = false;
		if (_Elements.Count == 0)
			basicElement = true;

		_Elements.Add (newElement);

		// Search weakness and stronger
        for(int f = 0;f < LegrandBackend.Instance.LegrandDatabase.ElementCircleData.Count;f++)
        {
            if (LegrandBackend.Instance.LegrandDatabase.ElementCircleData.Get(f).CurrentElement.Equals(newElement))
            {
                _Stronger = LegrandBackend.Instance.LegrandDatabase.ElementCircleData.Get(f).StrongerTo;
                _Weakness = LegrandBackend.Instance.LegrandDatabase.ElementCircleData.Get(f).WeakerTo;
            }
        }
		/*_Stronger = Element.None;
		_Weakness = Element.None;
		if (basicElement && newElement != Element.Air) 
		{
			switch(newElement)
			{
				case Element.Fire : _Stronger = Element.Earth; _Weakness = Element.Water;break;
				case Element.Earth : _Stronger = Element.Lighting; _Weakness = Element.Fire;break;
				case Element.Lighting : _Stronger = Element.Water; _Weakness = Element.Earth;break;
				case Element.Water : _Stronger = Element.Fire; _Weakness = Element.Light;break;
				case Element.Light : _Stronger = Element.Dark; _Weakness = Element.Dark;break;
				case Element.Dark : _Stronger = Element.Light; _Weakness = Element.Light;break;
			}
		}*/
	}

}

public enum Element
{
    Fire, //0
    Water, //1
    Air, //2
    Earth, //3
    Lighting, //4
    Light, //5
    Dark, //6
    None //7
}
