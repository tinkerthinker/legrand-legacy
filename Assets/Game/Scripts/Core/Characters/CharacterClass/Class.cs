using UnityEngine;
using System.Collections;

namespace Legrand.core
{
	public class CharacterClass
	{
		public BattleClass ClassType;

		public CharacterClass()
		{
		}

		public CharacterClass(BattleClass type)
		{
			ClassType = type;
		}
	}

        public enum BattleClass
        {
            Fighter, //0
            Warrior, //1
            Warlord, //2
            Archer, //3
            Ranger, //4
            Sharpshooter, //5
            Apprentice, //6
            Mage, //7
            Sorceress, //8
            Thief,
            Rogue,
            Assassin,
            Brawler,
            Barbarian,
            Berserker,
            Dragoon
        };

        public enum CharacterAttackType
        {
            Melee,
            Range,
            Caster
        };
}