﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class InputDataCharacter  {
	public string NameCombo		= "";
	
	[System.Serializable]
	public enum ButtonType {
		BUTTON_CONFIRM,
		BUTTON_MENU,
		BUTTON_CANCEL,
		BUTTON_ARROWLEFT,
		BUTTON_ARROWRIGHT,
		BUTTON_ARROWUP,
		BUTTON_ARROWDOWN,
		BUTTON_PAUSE,
		BUTTON_SCROLL_LEFT,
		BUTTON_SCROLL_RIGHT
	}
	
	[System.Serializable]
	public enum ButtonAct{
		ON_UP,
		ON_DRAG,
		ON_DOWN
	}
	
	public ButtonType[] ButtonList;
}