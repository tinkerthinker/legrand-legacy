using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Legrand.database;

namespace Legrand.core
{
	public class MainCharacter : Character 
    {
        private Health _Health;
        public Health Health
        { 
            get { return _Health; }
        }
		
		public CharacterAttackType CharacterAttackType;
        public CharacterClass _BattleClass;

		[SerializeField]
		private OffensiveBattleSkill _BattleOffensiveSkill;
		public OffensiveBattleSkill BattleOffensiveSkill
		{
			get { return _BattleOffensiveSkill; }
		}
		
//		[SerializeField]
//		private DefensiveBattleSkill _BattleDefensiveSkill;
//        public DefensiveBattleSkill BattleDefensiveSkill
//        {
//            get{ return _BattleDefensiveSkill;}
//		}

        [SerializeField]
        private GuardBuff _GuardSkill;
        public GuardBuff GuardSkill
        {
            get { return _GuardSkill; }
        }

        [SerializeField]
		private List<CombinationSkill> _BattleComboSkill;
		public List<CombinationSkill> BattleComboSkill
		{
			get { return _BattleComboSkill; }
		}

        public List<LockedSkill> _LockedNormalSkill;
        [SerializeField]
		private List<NormalSkill> _NormalSkills;
		public List<NormalSkill> NormalSkills{
			get { return _NormalSkills; }
		}

        // base value of all attribute
        private Dictionary<Attribute, object> _BaseBattleAttributes;
        public Dictionary<Attribute, object> BaseBattleAttributes
        {
            get { return _BaseBattleAttributes; }
        }

        // Final value of all attribute
        private Dictionary<Attribute, object> _BattleAttributes;
        public Dictionary<Attribute, object> BattleAttributes
        {
            get { return _BattleAttributes; }
        }

        [SerializeField]
        private int _AttributePoint;
        public int AttributePoint
        {
            get { return _AttributePoint; }
            set { _AttributePoint = value; }
        }
        // Experience Gained
        [SerializeField]
		private int _StoredExperience;
		public int StoredExperience
		{
			get { return _StoredExperience;}
			set 
			{
				if(value > 999999) _StoredExperience = 999999;
				else if(value < 0) _StoredExperience = 0;
				else _StoredExperience = value;
			}
		}

        [SerializeField]
        private int _Level;
        public int Level
        {
            get { return _Level; }
            set { _Level = value; }
        }

        [SerializeField]
        private int _CurrentEXP;
        public int CurrentEXP
        {
            get { return _CurrentEXP; }
            set { _CurrentEXP = value; }
        }

        [SerializeField]
        private int _NextLevelEXP;
        public int NextLevelEXP
        {
            get { return _NextLevelEXP; }
        }

        // Current exp for all attribute
        private Dictionary<Attribute, object> _BattleExperiences;
        public Dictionary<Attribute, object> BattleExperiences
        {
            get { return _BattleExperiences; }
        }

        // Experiences Needed of all attribute
        private Dictionary<Attribute, object> _BattleExperiencesNeeded;
        public Dictionary<Attribute, object> BattleExperiencesNeeded
        {
            get { return _BattleExperiencesNeeded; }
        }

        // Growth of all attribute
        private Dictionary<Attribute, object> _BattleAttributeGrowths;
        public Dictionary<Attribute, object> BattleAttributeGrowths
        {
            get { return _BattleAttributeGrowths; }
        }
        public override float GetGrowth(Attribute attributeType)
        {
            if (!_BattleAttributeGrowths.ContainsKey(attributeType)) return 1f;
            return (float)_BattleAttributeGrowths[attributeType];
        }

        public CharacterElement CharacterElements;

		[SerializeField]
		private CharacterMagicPresets _Magics;
        public CharacterMagicPresets Magics
        {
            get { return _Magics; }
        }

        public List<LockedMagic> _LockedMagics; 
        [SerializeField]
        private List<CommandData> _CommandPresets;
        public List<CommandData> CommandPresets
        {
            get { return _CommandPresets;}
        }

		private Weapon _BaseWeapon;
		public Weapon BaseWeapon
		{
			get { return _BaseWeapon;}
			set { _BaseWeapon = value;}
		}
		override public Weapon Weapon()
		{
			return _BaseWeapon;
		}

		private Armor _BaseArmor;
		public Armor BaseArmor
		{
			get { return _BaseArmor;}
			set { _BaseArmor = value;}
		}
		override public Armor Armor()
		{
			return _BaseArmor;
		}

		[SerializeField]
        private Equipments _Equipments;
        
        override public Equipments Equipment()
        {
            return _Equipments;
        }

        [SerializeField]
        private List<ItemSlot> _Items;
        public List<ItemSlot> Items
        {
            get {return _Items;}
        }

		[SerializeField]
		private int _Gauge;
		public int Gauge
		{
			get {return _Gauge;}
			set 
			{
				if(value > 100) _Gauge = 100;
				else if(value < 0) _Gauge = 0;
				else _Gauge = value;
			}
		}

        [SerializeField]
        private int[] GaugeSpeed;
        public int GetGaugeSpeed(GaugeType type)
        {
            if ((int)type < GaugeSpeed.Length) return GaugeSpeed[(int)type];
            return 0;
        }

        public List <Sprite> Avatar;
        
		public int MaxItem = 4;
		public int AverageAttribute = 0;

        private int _ClassLevel = 1;
        public int ClassLevel
        {
            get { return _ClassLevel;}
            set { _ClassLevel = value; }
        }

        public void LoadCharacter(bool fromSave)
        {
            // Should actually load it from DB
            _BattleClass = new CharacterClass();
            
			_BattleOffensiveSkill = new OffensiveBattleSkill();
//            _BattleDefensiveSkill = new DefensiveBattleSkill();
			_BattleComboSkill = new List <CombinationSkill> ();
			_NormalSkills = new List<NormalSkill> ();
            //_Equipments = new Equipments();
            
            // This is dynamic
//            _PostAttributes = new PostAttributes();
            
            // Calculation cache
            _BaseBattleAttributes = new Dictionary<Attribute, object>();
            _BattleAttributes = new Dictionary<Attribute, object>();
            _BattleExperiences = new Dictionary<Attribute, object>();
            _BattleExperiencesNeeded = new Dictionary<Attribute, object>();
            _BattleAttributeGrowths = new Dictionary<Attribute, object>();
            
            _Magics = new CharacterMagicPresets();
            CharacterElements = new CharacterElement();
            if(_Equipments == null)
                _Equipments = new Equipments();
            _CommandPresets = new List<CommandData>();
            GaugeSpeed = new int[Enum.GetNames(typeof(GaugeType)).Length];
//			_DefensiveSkillBuff = new List<Buff>();
            for (int i=0; i<4; i++)
            {
                for (int j=0; j<4; j++)
                {
                    _CommandPresets.Add(new CommandData((i*4)+j, _ID, (CommandPos)i, (CommandPos)j));
                }
            }
            
            _Items = new List<ItemSlot> ();
            for (int i=0;i<MaxItem; i++) 
            {
                ItemSlot tempSlot = new ItemSlot();
                Items.Add(tempSlot);
            }

            LoadBaseBattleAttributes();
            
            //Create New Character
            if (fromSave)
                DeserializeFromSave();
            else
                DeserializeFromDatabase();

            CalculateAttributes(false);
            
            LearnMagic(false);
            LearnNormalSkill(false);
            GetLevelData();

            InitializeCommand(fromSave);
        }

        void DeserializeFromDatabase()
        {
		if (LegrandBackend.Instance != null)
	    {
			bool found = false;
			for(int i=0;i<LegrandBackend.Instance.LegrandDatabase.PartyData.Count && !found;i++)
			{
					PartyDataModelTest model = LegrandBackend.Instance.LegrandDatabase.PartyData.Get(i);
					if(model.ID == _ID) 
				    {
					    found = true;

                        _Level = model.Level;
                        _ClassLevel = model.ClassLevel;
					    //Load Battle Attribute dan Experiences nya
                        for (int j = 0; j < model.NewGameBattleAttributes.Count; j++) // each(var partyBattleAttribute in model.NewGameBattleAttributes)
					    {
                            var partyBattleAttribute = model.NewGameBattleAttributes[j];
						    if(_BaseBattleAttributes.ContainsKey((Attribute)partyBattleAttribute._Attribute))
						    {
							    _BaseBattleAttributes[(Attribute)partyBattleAttribute._Attribute] = partyBattleAttribute._Value;
							    _BattleExperiences[(Attribute)partyBattleAttribute._Attribute] = partyBattleAttribute._Experiences;
						    }
						    else
						    {
							    _BaseBattleAttributes.Add((Attribute)partyBattleAttribute._Attribute,partyBattleAttribute._Value);
							    _BattleExperiences.Add   ((Attribute)partyBattleAttribute._Attribute,partyBattleAttribute._Experiences);
						    }
					    }
                        
					    //Load Element
                        for (int j = 0; j < model.NewGameCharacterElements.Count; j++) // each(var Elements in model.NewGameCharacterElements)
					    {
                            var Elements = model.NewGameCharacterElements[j];
						    CharacterElements.AddElement(Elements.Element);
					    }
                        //Load Character Item
                        for (int j = 0; j < model.NewGameCharacterItems.Count; j++) // each (var item in model.NewGameCharacterItems)
                        {
                            var item = model.NewGameCharacterItems[j];
                            PartyManager.Instance.Inventory.AddItems(LegrandBackend.Instance.ItemData[item.HealingAndMagicItemID], item.Stack);
                            Items[item.Index] = PartyManager.Instance.Inventory.GetItem(item.HealingAndMagicItemID);
                        }

                        _Equipments.ChangeEquipment(model.Equipment);
                    }
			    }
            }
        }

        void DeserializeFromSave()
        {
            if (LegrandBackend.Instance != null)
            {
                //Load Battle Attribute dan Experiences nya
                for(int i = 0; i< LegrandBackend.Instance.MainCharacterBattleAttributes.Count;i++)
                {
                    BattleAttribute battleAttribute = LegrandBackend.Instance.MainCharacterBattleAttributes[i];
                    if (battleAttribute._CharacterID == _ID)
                    {
                        if (_BaseBattleAttributes.ContainsKey((Attribute)battleAttribute._Attribute))
                        {
                            _BaseBattleAttributes[(Attribute)battleAttribute._Attribute] = battleAttribute._Value;
                            _BattleExperiences[(Attribute)battleAttribute._Attribute] = battleAttribute._Experiences;
                        }
                        else
                        {
                            _BaseBattleAttributes.Add((Attribute)battleAttribute._Attribute, battleAttribute._Value);
                            _BattleExperiences.Add((Attribute)battleAttribute._Attribute, battleAttribute._Experiences);
                        }
                    }
                }

                //Load Element
                for (int i = 0; i < LegrandBackend.Instance.MainCharacterElements.Count; i++)
                {
                    ElementModel elementModel = LegrandBackend.Instance.MainCharacterElements[i];
                    if (elementModel.CharacterId == _ID)
                    {
                        CharacterElements.AddElement((Element)elementModel.Element);
                    }
                }

                for (int i = 0; i < LegrandBackend.Instance.MainCharacterItems.Count; i++)
                {
                    CharacterItemModel item = LegrandBackend.Instance.MainCharacterItems[i];
                    if (item.ItemId != null && item.CharacterId == _ID)
                    {
                        Items[item.Index] = PartyManager.Instance.Inventory.GetItem(item.ItemId);
                    }
                }
            }
        }

        void LoadBaseBattleAttributes()
        {
            // Change this one when level up
            _BaseBattleAttributes.Add(Attribute.STR, 1);
            _BaseBattleAttributes.Add(Attribute.LUCK, 1);
            _BaseBattleAttributes.Add(Attribute.INT, 1);
            _BaseBattleAttributes.Add(Attribute.AGI, 1);
            _BaseBattleAttributes.Add(Attribute.VIT, 1);

            _BattleExperiences.Add(Attribute.STR, 0);
            _BattleExperiences.Add(Attribute.AGI, 0);
            _BattleExperiences.Add(Attribute.VIT, 0);
            _BattleExperiences.Add(Attribute.LUCK, 0);
            _BattleExperiences.Add(Attribute.INT, 0);

            /*foreach (BattleGrowthData growth in LegrandBackend.Instance.iBox.BattleAttributeGrowth) {
				if(growth.chraracterId == _ID)
				{
					_BattleAttributeGrowths.Add((Attribute)growth.attributeType, growth.value);
				}
			}*/


            bool found = false;
            
			for(int index=0;index<LegrandBackend.Instance.LegrandDatabase.PartyData.Count && !found;index++)
			{
                PartyDataModelTest dataModelCharacter = LegrandBackend.Instance.LegrandDatabase.PartyData.Get(index);
                if (dataModelCharacter.ID.Equals(_ID))
                {
                    foreach (var bag in dataModelCharacter.BattleGrowth)
					{
						_BattleAttributeGrowths.Add(bag.attributeType, bag.value);
					}
                    


                    _LockedMagics = new List<LockedMagic>(dataModelCharacter.LockedMagic);
                    _LockedNormalSkill = new List<LockedSkill>(dataModelCharacter.LockedSkill);
                    found = true;
                }
			}
        }

        void GetLevelData()
        {
            bool found = false;
            for (int index = 0; index < LegrandBackend.Instance.LegrandDatabase.CharacterLevelData.Count && !found; index++)
            {
                CharacterLevel charLevel = LegrandBackend.Instance.LegrandDatabase.CharacterLevelData.Get(index);
                if(charLevel.ID_Character == this._ID)
                {
                    for(int i = 0; i<charLevel.CharacterLevelDatas.Count && !found; i++)
                    {
                        CharacterLevelData charLevelData = charLevel.CharacterLevelDatas[i];
                        if (charLevelData.Level == _ClassLevel)
                        {
                            _BattleClass = new CharacterClass(charLevelData.Class);
                            LearnOffensiveSkill(charLevelData.SkillUltimateOffensive);
//                            LearnDefensiveSkill(charLevelData.SkillUltimateDefensive);
                            LearnGuardSkill(charLevelData.PassiveGuardSkill);
                            for(int gaugeIndex = 0; gaugeIndex < charLevelData.GaugeDatas.Count; gaugeIndex++)
                            {
                                GaugeSpeed[(int)charLevelData.GaugeDatas[gaugeIndex].Type] = charLevelData.GaugeDatas[gaugeIndex].Value;
                            }
                            found = true;
                        }
                    }
                }
            }
        }

        void CalculateExperiences()
        {
            _NextLevelEXP = Formula.EXPforLevelUp(Level, MaxLevel);
        }
        
        public int IncreaseBattleAttributeEXP(Attribute type, int exp)
        {
			// return how many levels up
			int levelUp = 0;
            if ((int)_BattleAttributes[type] == MaxLevel) return levelUp;
            _BattleExperiences[type] = (int)_BattleExperiences[type] + exp;
            while ((int)_BattleExperiences[type]  >= (int)_BattleExperiencesNeeded[type])
            {
                LevelUp(type, false);
				levelUp += 1;
            }
			return levelUp;
        }

        public int IncreaseEXP(int exp)
        {
            if (Level == MaxLevel) return 0;

            return SetExp(exp + _CurrentEXP);
        }

        public int SetExp(int newExp)
        {
            if (Level == MaxLevel) return 0;
            int atpGained = 0;
            while (newExp - _NextLevelEXP >= 0)
            {
                newExp = newExp - _NextLevelEXP;
                atpGained += LevelUp();
            }

            if(newExp > 0) _CurrentEXP = newExp;
            return atpGained;
        }

        public int ExpDifferences()
        {
            return _NextLevelEXP - CurrentEXP;
        }

        private int LevelUp()
        {
            _Level += 1;
            _CurrentEXP = 0;
            int GainedATPPoint = LegrandBackend.Instance.GetLevelATP(_Level);
            _AttributePoint += GainedATPPoint;
            _NextLevelEXP = Formula.EXPforLevelUp(_Level, MaxLevel);
            return GainedATPPoint;
        }

        public void LevelUp(Attribute type, bool isRestartExperience)
        {
			_BaseBattleAttributes [type] = (int)_BaseBattleAttributes [type] + 1;
			if (isRestartExperience)
				_BattleExperiences [type] = 0;
			else
				_BattleExperiences [type] = (int)_BattleExperiences [type] - (int)_BattleExperiencesNeeded [type];
            
			CalculateAttributes(false);
        }

		public void LevelUp(Attribute type, int value)
		{
			// Level Up Multiple Times Always Restart Experience
            float percHealth = Health.Value / Health.MaxValue; //save current health percentage
            _BaseBattleAttributes [type] = (int)_BaseBattleAttributes [type] + value;
			_BattleExperiences[type] = 0;
			CalculateAttributes(false);
            Health.ChangeCurrentValue(percHealth * Health.MaxValue);
		}

		public bool ChangeBaseAttribute(Attribute type, int value)
		{
			if (_BaseBattleAttributes.ContainsKey (type)) {
				_BaseBattleAttributes [type] = value;
				_BattleExperiences [type] = 0;
				CalculateAttributes (false);
				return true;
			}
			return false;
		}

        #region Attributes

        public void CalculateAttributes(bool isTemporary)
        { 
            // Make sure we start from nothing
            if(_BattleAttributes != null)
                _BattleAttributes.Clear();
            
            CalculatePrimaryAttributes();
            CalculateSecondaryAttributes();
			CalculateExperiences();
			if(!isTemporary) AssignHealths();
        }

        void CalculatePrimaryAttributes()
        {
            _BattleAttributes.Add(Attribute.STR, (int)_BaseBattleAttributes[Attribute.STR]);
            _BattleAttributes.Add(Attribute.INT, (int)_BaseBattleAttributes[Attribute.INT]);
            _BattleAttributes.Add(Attribute.AGI, (int)_BaseBattleAttributes[Attribute.AGI]);
            _BattleAttributes.Add(Attribute.VIT, (int)_BaseBattleAttributes[Attribute.VIT]);
            _BattleAttributes.Add(Attribute.LUCK, (int)_BaseBattleAttributes[Attribute.LUCK]);

			int total = 0;
			int countAttribute = 0;
			foreach (KeyValuePair<Attribute,object> attribute in _BattleAttributes) 
			{
				total = total + (int)attribute.Value;
				countAttribute++;
			}
			
			AverageAttribute = total / countAttribute;
        }
        
        void CalculateSecondaryAttributes()
        {
           int health = Formula.CalculateHealth (this);
            _BattleAttributes.Add(Attribute.Health, health);
        }
		
		public override int GetAttribute (Attribute attribute)
		{
			return (int)_BattleAttributes [attribute];
		}
		public override float GetSecondaryAtt (Attribute attribute){
			float value = 1f;
			if(_BattleAttributes.ContainsKey(attribute)) value = (float) _BattleAttributes [attribute];
			return value;
		}
//        public void AddBuff(Buff buff)
//        {
//            int buffId = buff._ID;
//            int turn;
//            
//            bool exist = PostAttributes.IsBuffActive(buffId);
//            
//            if (!exist)
//            {
//                // Apply attributes
//                _PostAttributes.ApplyBuff(buff);
//                CalculateAttributes(true);
//            }
//        }

        #endregion

        #region Healths
        
        void AssignHealths()
        {
            if (_Health == null)
            {
                _Health = gameObject.AddComponent<Health>();
                _Health.Name = "HP";
                _Health.ChangeMaximumValue((int)_BattleAttributes[Attribute.Health]);
				_Health.ChangeCurrentValue(_Health.MaxValue);
            }
			else _Health.ChangeMaximumValue((int)_BattleAttributes[Attribute.Health]);
        }
        public override void ReceiveDamage(float damage)
        {
            // Also should calculate this one
            _Health.Decrease(damage);
        }
		public override Health GetHealth ()
		{
			return Health;
		}
        #endregion


        #region Item
        public void AddItem(ItemSlot itemToAdd, int index)
        {
            Items [index] = itemToAdd;
        }

        public void AddItems(Item itemToAdd, int index)
        {
            AddItem(PartyManager.Instance.Inventory.GetItem(itemToAdd._ID), index);
		}

        public void AddItems(string itemID, int index)
        {
            AddItems(LegrandBackend.Instance.ItemData[itemID], index);
        }

        public void SwapItem(int indexFrom, int indexTo)
		{
			ItemSlot temp = Items [indexFrom];
			Items [indexFrom] = Items [indexTo];
			Items [indexTo] = temp;
		}

        public int ReturnItem(int index)
		{
		    Items [index] = null;
			return 0;
		}

		public bool UseItem(int index, List<Battler> targets, Battler source)
        {
            // return true kalau terpakai 
            if (index < Items.Count && Items [index] != null && Items [index].ItemMaster != null && PartyManager.Instance.Inventory.GetStackItem(Items[index].ItemMaster) > 0)// && Items [index].Stack > 0)
            {
                bool used = false;
                for (int i = 0; i < targets.Count; i++)
                {
                    if (UseItem(Items[index].ItemMaster, targets[i], source))
                        used = true;
                }

                if (used)
                {
                    PartyManager.Instance.Inventory.DeleteItemsByItemID(Items[index].ItemMaster._ID,1);
                    return true;
                }
            }
               
            return false;
        }
        
		private bool UseItem(Item itemToUse, Battler target, Battler source)
        {
			MainCharacter mc = (target.Character as MainCharacter);
			bool used = false;	
			if(itemToUse.ItemType == Item.Type.Consumable)
			{
				Consumable consumedItem = (Consumable) itemToUse;
				#region itemhealing
				if(consumedItem.ConsumableType == Consumable.ConsumeType.Healing)
				{
					ConsumableHealing healItem = (ConsumableHealing) consumedItem;
					foreach(Status curedStatus in healItem.CuredStatuses)
					{
						if(curedStatus == Status.Dead)
						{
							foreach(ConsumableHealing.RestoredAttribute restoredAttribute in healItem.RestoredAttributes)
							{
                                if(restoredAttribute._Attribute == Attribute.ReviveHealth && target.BattlerState == BattlerState.Dead) 
                                {
                                    target.SetLockedState(false);
                                    target.BattlerState = BattlerState.Revived;
									float restoredHeal = target.Health.MaxValue*((float)restoredAttribute.Value/100f);
									target.Health.Revive(target.Health.MaxValue*((float)restoredAttribute.Value/100f));	
									target.PopUp(Mathf.RoundToInt (restoredHeal).ToString(), (int)BattleTextPopUp.BattleTextType.Heal);
                                }
                                else
                                {
                                    target.PopUp("Miss", 0, false);
                                }
							}
						}
						//Jenis cure status lain
					}

					foreach(ConsumableHealing.RestoredAttribute restoredAttribute in healItem.RestoredAttributes)
					{
						if(restoredAttribute._Attribute == Attribute.Health)
						{
							float restoredHeal = target.Health.MaxValue*((float)restoredAttribute.Value/100f);
							target.Health.Increase(target.Health.MaxValue*((float)restoredAttribute.Value/100f));
							target.PopUp(Mathf.RoundToInt (restoredHeal).ToString(), (int)BattleTextPopUp.BattleTextType.Heal);
						}
						//Jenis Heal Lainnya
					}
									
					used = true;
				}
				#endregion
				#region item magic
				else if (consumedItem.ConsumableType == Consumable.ConsumeType.Magic)
				{
					ConsumableMagic magicItem = (ConsumableMagic) consumedItem;
					ItemController._Instance.InitItem (magicItem, target, source);

					used = true;
					//Jenis Buff Lainnya
				}
				#endregion
			}
            return used;
        }

		public bool UseItem(int index, List<string> targetIDs)
		{
			// return true kalau terpakai 
			if (index < Items.Count && Items [index] != null && Items [index].ItemMaster != null && Items [index].Stack > 0)
			{
				if (UseItem(Items [index].ItemMaster, targetIDs))
				{
                    PartyManager.Instance.Inventory.DeleteItemsByItemID(Items[index].ItemMaster._ID, 1);
                    return true;
				}
			}
			
			return false;
		}

		private bool UseItem(Item itemToUse, List<string> targetIDs)
		{
			bool used = false;	
			if(itemToUse.ItemType == Item.Type.Consumable)
			{
				Consumable consumedItem = (Consumable) itemToUse;
				#region itemhealing
				if(consumedItem.ConsumableType == Consumable.ConsumeType.Healing)
				{
					ConsumableHealing healItem = (ConsumableHealing) consumedItem;
					foreach(Status curedStatus in healItem.CuredStatuses)
					{
						if(curedStatus == Status.Dead)
						{
							foreach(ConsumableHealing.RestoredAttribute restoredAttribute in healItem.RestoredAttributes)
							{
								if(restoredAttribute._Attribute == Attribute.ReviveHealth) 
								{
									foreach(MainCharacter partyMember in PartyManager.Instance.CharacterParty)
									{
										if(targetIDs.Contains(partyMember._ID))
										{
											partyMember.Health.Revive(partyMember.Health.MaxValue*((float)restoredAttribute.Value/100f));
										}
									}
								}
							}
						}
						//Jenis cure status lain
					}
					
					foreach(ConsumableHealing.RestoredAttribute restoredAttribute in healItem.RestoredAttributes)
					{
						if(restoredAttribute._Attribute == Attribute.Health)
						{
							foreach(MainCharacter partyMember in PartyManager.Instance.CharacterParty)
							{
								if(targetIDs.Contains(partyMember._ID))
									partyMember.Health.Increase(partyMember.Health.MaxValue*((float)restoredAttribute.Value/100f));
							}
						}
						//Jenis Heal Lainnya
					}
					
					used = true;
				}
				#endregion
	
				#region itemattribute
				if(consumedItem.ConsumableType == Consumable.ConsumeType.Attributes)
				{
					ConsumableAttribute attributeItem = (ConsumableAttribute) consumedItem;
					foreach(ConsumableAttribute.RaisedAttribute raisedAttribute in attributeItem.RaisedAttributes)
					{
						foreach(MainCharacter partyMember in PartyManager.Instance.CharacterParty)
						{
							if(targetIDs.Contains(partyMember._ID))
							{
								partyMember.LevelUp(raisedAttribute._Attribute, raisedAttribute._Value);
							}
						}
					}
					used = true;
				}
				#endregion
			
			}
			
			foreach(MainCharacter partyMember in PartyManager.Instance.CharacterParty)
			{
				if(targetIDs.Contains(partyMember._ID))
				{
					partyMember.CalculateAttributes(false);
				}
			}
			return used;
		}

        public void DeleteAll()
        {
            Items.Clear(); 
        }
		public override Item GetItem (int id)
		{
			return Items[id].ItemMaster;
		}
        #endregion

		#region Magic
		public override Magic GetMagic (int id)
		{
			return _Magics.MagicPresets.Find (x => x._ID == id).MagicPreset;
		}

        public List<LockedMagic> LearnMagic(bool autoAssignCommand = true)
        {
            List<LockedMagic> LearnedLockedMagic = new List<LockedMagic>();
            for(int i=0;i<_LockedMagics.Count; i++)
            {
                // Check is requirement fulfilled
                if (_LockedMagics[i].IsRequirementFulfilled(_ClassLevel, _BattleAttributes))
                {
                    _Magics.LearnMagicPreset(_LockedMagics[i].Magic.ID, this._ID, _LockedMagics[i].Magic, false);
                    bool foundPreviousMagic = false;
                    if (_LockedMagics[i].PreviousMagicId >= 0)
                    {
                        // Unlearn previous magic
                        _Magics.UnlearnMagicPreset(_LockedMagics[i].PreviousMagicId);
                        // Replace previous magic command with new command id
                        for (int j = 0; j < _CommandPresets.Count && !foundPreviousMagic; j++)
                        {
                            if (_CommandPresets[j].CommandType == (int)CommandTypes.Magic && _CommandPresets[j].CommandValue == _LockedMagics[i].PreviousMagicId)
                            {
                                foundPreviousMagic = true;
                                _CommandPresets[j].CommandValue = _LockedMagics[i].Magic.ID;
                            }
                        }

                        
                    }

                    // Auto  assign magic to empty command
                    if (autoAssignCommand && !foundPreviousMagic)
                    {
                        bool foundEmptySlot = false;
                        for (int j = 0; j < 4 && !foundEmptySlot; j++)
                        {
                            CommandData magicCommand = GetCommand(CommandPos.Up, (CommandPos)j);
                            if (magicCommand.CommandType == (int)CommandTypes.None)
                            {
                                foundEmptySlot = true;
                                magicCommand.CommandType = (int)CommandTypes.Magic;
                                magicCommand.CommandValue = _LockedMagics[i].Magic.ID;
                            }
                        }
                    }

                    LearnedLockedMagic.Add(_LockedMagics[i]);
                }
            }

            for(int i=0; i<LearnedLockedMagic.Count;i++)
                _LockedMagics.Remove(LearnedLockedMagic[i]);

            return LearnedLockedMagic;
        }
        #endregion

        #region skill

        private void LearnOffensiveSkill(OffensiveBattleSkill newOffensiveSkill)
        {
            _BattleOffensiveSkill = newOffensiveSkill;
            SetCommand(CommandPos.Right, CommandPos.Up, CommandTypes.SkillUlti, _BattleOffensiveSkill.CommandValue);
        }

//        private void LearnDefensiveSkill(DefensiveBattleSkill newDefensiveSkill)
//        {
//            _BattleDefensiveSkill = newDefensiveSkill;
//            SetCommand(CommandPos.Right, CommandPos.Down, CommandTypes.SkillUlti, _BattleDefensiveSkill.CommandValue);
//        }
        public List<LockedSkill> LearnNormalSkill (bool autoAssignSkill = true)
        {
            List<LockedSkill> LearnedNormalSkill = new List<LockedSkill>();
            for (int i = 0; i < _LockedNormalSkill.Count; i++)
            {
                if (_LockedNormalSkill[i].IsRequirementFulfilled(_ClassLevel, _BattleAttributes))
                {
                    _NormalSkills.Add(_LockedNormalSkill[i].Skill);
                    bool foundPreviousSkill = false;
                    if (_LockedNormalSkill[i].PreviousSkillId != string.Empty && _LockedNormalSkill[i].PreviousSkillId != "")
                    {
                        // Remove previous skill
                        NormalSkill previousSkill = _NormalSkills.Find(skill => skill.IdSkill == _LockedNormalSkill[i].PreviousSkillId);
                        if (previousSkill != null)
                        {
                            _NormalSkills.Remove(previousSkill);

                            // Change if command skill contain previous skill id with new skill id
                            for (int j = 0; j < _CommandPresets.Count && !foundPreviousSkill; j++)
                            {
                                if (_CommandPresets[j].CommandType == (int)CommandTypes.Skill && _CommandPresets[j].CommandValue == previousSkill.CommandValue)
                                {
                                    foundPreviousSkill = true;
                                    _CommandPresets[j].CommandValue = _LockedNormalSkill[i].Skill.CommandValue;
                                }
                            }
                        }
                    }

                    // Auto  assign skill to empty command
                    if (autoAssignSkill && !foundPreviousSkill)
                    {
                        bool foundEmptySlot = false;
                        for (int j = 0; j < 4 && !foundEmptySlot; j++)
                        {
                            CommandData magicCommand = GetCommand(CommandPos.Up, (CommandPos)j);
                            if (magicCommand.CommandType == (int)CommandTypes.None)
                            {
                                foundEmptySlot = true;
                                magicCommand.CommandType = (int)CommandTypes.Skill;
                                magicCommand.CommandValue = _LockedNormalSkill[i].Skill.CommandValue;
                            }
                        }
                    }

                    LearnedNormalSkill.Add(_LockedNormalSkill[i]);
                }
            }

            for (int i = 0; i < LearnedNormalSkill.Count; i++)
                _LockedNormalSkill.Remove(LearnedNormalSkill[i]);

            return LearnedNormalSkill;
        }

        private void LearnGuardSkill(GuardBuff newGuardSkill)
        {
            _GuardSkill = newGuardSkill;
        }

//        public override DefensiveBattleSkill GetDefensiveBuff (int id)
//        {
//            if (_BattleDefensiveSkill.CommandValue == id)
//                return _BattleDefensiveSkill;
//            else
//                return null;
//		}

		public override NormalSkill GetNormalSkill (int id)
		{
			return _NormalSkills.Find (x => x.CommandValue == id);
		}

        public override OffensiveBattleSkill GetOffensiveSkill (int id)
        {
            if (_BattleOffensiveSkill.CommandValue == id)
                return _BattleOffensiveSkill;
            else
                return null;
		}
		#endregion

		#region Command
		public CommandData GetCommand(CommandPos panelPos, CommandPos slotPos)
		{
			foreach (CommandData command in CommandPresets) {
				if((CommandPos)command.PanelPos == panelPos && (CommandPos)command.SlotPos == slotPos)
					return command;
			}
			//return new CommandData(_ID, panelPos, slotPos, CommandType.None, 0);
			return null;
		}


        public void SetCommand(CommandPos panelPos, CommandPos slotPos, CommandTypes type, int value)
		{
			foreach (CommandData command in CommandPresets) {
				if((CommandPos)command.PanelPos == panelPos && (CommandPos)command.SlotPos == slotPos)
				{
					_CommandPresets[command.Index].CommandType = (int)type;
					_CommandPresets[command.Index].CommandValue = value;
					return;
				}
			}
		}

        public void SetCommandStatus(CommandPos panelPos, CommandPos slotPos, bool active)
        {
            CommandData command = GetCommand(panelPos, slotPos);
            command.SetCommandActive(active);
        }

        public void SetCommandStatus(CommandPos panelPos, CommandPos slotPos, bool active, bool important)
        {
            CommandData command = GetCommand(panelPos, slotPos);
            command.SetCommandActive(active);
            command.SetCommandImportant(important);
        }

        public void InitializeCommand(bool fromSave)
        {
                if (LegrandBackend.Instance != null)
                {

                    if (fromSave)
                    {
                        for (int i = 0; i < LegrandBackend.Instance.MainCharacterCommands.Count; i++)
                        {
                            CommandData commandData = LegrandBackend.Instance.MainCharacterCommands[i];
                            if (commandData.CharacterID == _ID)
                            {
                                SetCommand((CommandPos)commandData.PanelPos, (CommandPos)commandData.SlotPos, (CommandTypes)commandData.CommandType, commandData.CommandValue);
                            }
                        }
                    }
                    else
                    {
                        bool found = false;
                        for (int i = 0; i < LegrandBackend.Instance.LegrandDatabase.PartyData.Count && !found; i++)
                        {
                            PartyDataModelTest model = LegrandBackend.Instance.LegrandDatabase.PartyData.Get(i);
                            if (model.ID == _ID)
                            {
                                found = true;
                                //Load Command Preset
                                for (int j = 0; j < model.NewGameCommands.Count; j++) // each(var commandData in model.NewGameCommands)
                                {
                                    SetCommand(model.NewGameCommands[j].PanelPos, model.NewGameCommands[j].SlotPos, model.NewGameCommands[j].CommandType, model.NewGameCommands[j].CommandValue);
                                }
                            }
                        }
                    }
                }
        }
        #endregion
    }
}