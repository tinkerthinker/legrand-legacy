﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Damage
{
	public bool IsCritical;
	public float BaseDamage;
	
	public GameObject Owner;
	
	public Damage(bool critical, float damage)
	{
		IsCritical = critical;
		BaseDamage = damage;
	}
}