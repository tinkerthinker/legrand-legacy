﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour
{
    public delegate void _OnReduced(float percent);
    public event _OnReduced OnReduced;
 
	public delegate void _OnIncreased(float percent);
	public event _OnReduced OnIncreased;

    // Delegate when the health reach zero
    public delegate void _OnZero();
    public event _OnZero OnZero;
    
    // Delegate when the health reach zero
    public delegate void _OnDead(GameObject g);
    public event _OnDead OnDead;
    
	// Delegate when the health reach zero
	public delegate void _OnRevive(GameObject g);
	public event _OnRevive OnRevive;

    // Name of the 'health'
    public string Name;
    
    // Initial and Maximum Health
    [SerializeField]
    private float _MaximumValue;
    
    // Current health
    [SerializeField]
    private float _CurrentValue;
    
	// IsDead
	[SerializeField]
	private bool _IsDead;

    // Does this regenerate or not, how much per tick
    public float RegenerationRate = 0f;
    public float TickTime = 1f;
    
    // Getter for current value
    public float Value
    {
        get { return _CurrentValue; }
    }
    
    // Getter for max value
    public float MaxValue
    {
        get { return _MaximumValue; }
    }
    
	// Getter for max value
	public bool IsDead
	{
		get { return _IsDead; }
		set { _IsDead = value;}
	}

    private IEnumerator RegenerationEnumerator;
    
	public bool Invincible = false;

    // Use this for initialization
    void Start()
    {
		_IsDead = false;
        ChangeMaximumValue(_MaximumValue);
    }
    
    public void ChangeMaximumValue(float max)
    {
        _MaximumValue = max;
    }

	public void ChangeCurrentValue(float value)
	{
		_CurrentValue = value;
	}

    public int IncreaseByPercentage(float percentage)
    {
        int healthIncreased = Mathf.RoundToInt(percentage / 100f * _MaximumValue);
        Increase(healthIncreased);
        return healthIncreased;
    }

    IEnumerator Regenerate()
    {
        while (true)
        {
            // Increase per tick time
            if (_CurrentValue < _MaximumValue)
                Increase(RegenerationRate);
            
            yield return new WaitForSeconds(TickTime);
        }
    }
    
    // Decrease the health until 0
    public void Decrease(float dec)
    {
		if (Invincible)
			return;
        // Subtract
        _CurrentValue -= dec;
        
        // Don't drop below zero
        if ((int)_CurrentValue <= 0)
        {
            _CurrentValue = 0;

            // Call things when zero
            if (OnZero != null){
                OnZero();
				_IsDead = true;
                //if(OnDead != null)
                    OnDead(this.gameObject);
            }
        }
        else
        {
            if (OnReduced != null) OnReduced(dec/_MaximumValue);
            
            // Start regenerating if needed and haven't
            if (RegenerationRate > 0f)
            {
                if (RegenerationEnumerator == null)
                {
                    RegenerationEnumerator = Regenerate();
                    StartCoroutine(RegenerationEnumerator);
                }
            }
        }
    }
    
    public void Increase(float inc)
    {
        // Add
		if (!_IsDead) {
			_CurrentValue += inc;
        
			// Stop at maximum
			if (_CurrentValue >= _MaximumValue) {
				_CurrentValue = _MaximumValue;
            
				// Stop regenerating
				if (RegenerationEnumerator != null) {
					StopCoroutine (RegenerationEnumerator);
					RegenerationEnumerator = null;
				}
			}

			if (OnIncreased != null) OnIncreased(inc/_MaximumValue);
		}
    }

	public bool Revive(float inc)
	{
		if(_IsDead || Value == 0)
		{
		_IsDead = false;
			Increase(inc);
			if (OnRevive != null){
				OnRevive(this.gameObject);
			}
			return true;
		}
		return false;
	}

    public bool IsOnReduced (){
        if (OnReduced != null)
            return true;
        return false;
    }

    public bool IsOnIncreased(){
        if (OnIncreased != null)
            return true;
        return false;
    }

    public bool IsOnZero(){
        if (OnZero != null)
            return true;
        return false;
    }

    public bool IsOnDead(){
        if (OnDead != null)
            return true;
        return false;
    }
    public bool IsOnRevive (){
        if (OnRevive != null)
            return true;
        return false;
    }
}

