using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Extensions;
using Legrand.core;
//using UnityEngine;

[System.Serializable]
public class Equipments 
{
    public string ID;
    public string Name;
    public string Description;
    public Sprite Picture;
    public Dictionary<Attribute, float> Attributes;

	public Equipments()
    {
        Name = "";
        Description = "";
        Picture = null;
        Attributes = new Dictionary<Attribute, float>();
    }

    public void ChangeEquipment(string ID)
    {
        for(int i = 0; i < LegrandBackend.Instance.LegrandDatabase.EquipmentDatabase.Count; i++)
        {
            EquipmentDataModel equipment = LegrandBackend.Instance.LegrandDatabase.EquipmentDatabase.Get(i);
            if(LegrandUtility.CompareString(equipment.ID,ID))
            {
                ChangeEquipment(equipment);
                return;
            }
        }
    }

    public void ChangeEquipment(EquipmentDataModel newEquipment)
    {
        ID = newEquipment.ID;
        Name = newEquipment.Name;
        Description = newEquipment.Description;
        Picture = newEquipment.Picture;
        Attributes.Clear();
        for (int i = 0; i < newEquipment.Attributes.Count; i++)
            Attributes.Add(newEquipment.Attributes[i].AttributeType, newEquipment.Attributes[i].value);
    }

    public float GetEquipmentAttribute(Attribute attribute)
    {
        float value = 0f;
        Attributes.TryGetValue(attribute, out value);
        return value;
    }
}