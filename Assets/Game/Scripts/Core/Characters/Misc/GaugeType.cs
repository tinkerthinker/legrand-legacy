﻿public enum GaugeType
{
    BaseAttack,
    GoodAttack,
    PerfectAttack,
    BaseMagic,
    GoodMagic,
    PerfectMagic,
    Guard
}
