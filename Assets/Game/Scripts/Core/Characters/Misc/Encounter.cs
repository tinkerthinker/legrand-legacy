using UnityEngine;
using System.Collections.Generic;
using Legrand.database;
using System.Linq;

namespace Legrand.core
{
	public class Encounter : Character 
	{
        private string _PartyID;
        public string PartyID
        {
            get { return _PartyID; }
        }
        private Health _Health;
        public Health Health
        { 
            get { return _Health; }
        }
		private float _HealthModifier;

		public CharacterAttackType _EncounterClass;
		public EncounterSize MonsterSize;
        
        // base value of all attribute
        private Dictionary<Attribute, object> _BaseBattleAttributes;
        public Dictionary<Attribute, object> BaseBattleAttributes
        {
            get { return _BaseBattleAttributes; }
        }
        
        // Final value of all attribute
        private Dictionary<Attribute, object> _BattleAttributes;
        public Dictionary<Attribute, object> BattleAttributes
        {
            get { return _BattleAttributes; }
        }

        // base value of all attribute
        private Dictionary<Attribute, object> _BattleAttributeGrowths;
        public Dictionary<Attribute, object> BattleAttributeGrowths
        {
            get { return _BattleAttributeGrowths; }
        }
        public override float GetGrowth(Attribute attributeType)
        {
            if (!_BattleAttributeGrowths.ContainsKey(attributeType)) return 1f;
            return (float)_BattleAttributeGrowths[attributeType];
        }

        private Weapon _BaseWeapon;
		public Weapon BaseWeapon
		{
			get { return _BaseWeapon;}
			set { _BaseWeapon = value;}
		}
		override public Weapon Weapon()
		{
			return BaseWeapon;
		}
		
		private Armor _BaseArmor;
		public Armor BaseArmor
		{
			get { return _BaseArmor;}
			set { _BaseArmor = value;}
		}
		override public Armor Armor()
		{
			return BaseArmor;
		}

		[SerializeField]
		private List<ItemSlot> _Items;
		public List<ItemSlot> Items
		{
			get {return _Items;}
		}
		[SerializeField]
		public CharacterMagicPresets _Magics;
		public CharacterMagicPresets Magics
		{
			get { return _Magics; }
		}

        [SerializeField]
        private List <OffensiveBattleSkill> _Skills;
        public List <OffensiveBattleSkill> Skills{
			get { return _Skills; }
		}
		//Element type of monster from element class
        public CharacterElement EncounterElements;

		//enemy using an item (chance)
		private List<Loot> _Loots;
		public List<Loot> Loots 
		{
				get{ return _Loots ;}
		}

		public int Exp = 0;
		public int Level = 0;
		public int AverageAttribute = 1;

		private CommandTypes _ComTypes;
		public CommandTypes ComTypes{
			get{return _ComTypes;}
			set{_ComTypes = value;}
		}

		public EnemyType enemyType;

		//AI set target condition
		private AttackTarget _AttackTarget;
		public AttackTarget attackTarget{
			get {return _AttackTarget;}
			set {_AttackTarget = value;}
		}

		//public EnemyAction enemyAtion;
		[HideInInspector]
		public List <EncounterData> counterData = new List<EncounterData>();
		[HideInInspector]
		public List <EncounterDataAlly> counterDataAlly = new List<EncounterDataAlly>();
		
		private ProjectileType _Projectile;
		public ProjectileType Projectile
		{
			get { return _Projectile;}
		}

		private ItemController _ItemController = new ItemController();


		public List <EncounterAI> _AIEncounter = new List<EncounterAI>();
		public List <EncounterAI> AIEncounter{
			get {return _AIEncounter;}
		}


		public EncounterFormationTest EncForm;

        public bool ResistDebuff;
        public List<int> ResistException;
        public string BattlerScript;


        public void init(int lvl, EncounterFormationTest eForm, string id, string partyID){
			// Should actually load it from DB
			_EncounterClass = new CharacterAttackType();
            _Skills = new List<OffensiveBattleSkill>();
			_ID = id;
            _PartyID = partyID;
			EncForm = eForm;
			// Calculation cache
			_BaseBattleAttributes = new Dictionary<Attribute, object>();
			_BattleAttributes = new Dictionary<Attribute, object>();
            _BattleAttributeGrowths = new Dictionary<Attribute, object>();
            _Items = new List<ItemSlot> ();
            Level = 0;
            Exp = 0;

            for (int i=0;i<4; i++) 
			{
				ItemSlot tempSlot = new ItemSlot();
				Items.Add(tempSlot);
			}
			_Loots = new List<Loot> ();

			EncounterElements = new CharacterElement ();


            if (lvl > 0)
                Level = lvl;

            LoadBaseBattleAttributes();
			
			//Load Dari Save Game
			DeserializeFromDatabase();
            

            //Load Dari Save Game
            //            if (!Backend.OfflineMode && LoadFromServer)
            //            {
            //                DeserializeFromServer();
            //            }

            CalculateAttributes();


			//_Health.OnDead += PlayerDeadEvent;
		}

        void DeserializeFromDatabase()
        {
            if (LegrandBackend.Instance != null)
            {
                bool found = false;
		
				for(int i=0;i<LegrandBackend.Instance.LegrandDatabase.EncounterData.Count && !found;i++)
				{
                    //Load Data Monster
                    EncounterDataModel encounterBaseData = LegrandBackend.Instance.LegrandDatabase.EncounterData.Get(i);
                    if (encounterBaseData.ID.Equals(_ID))
                    {
                        this._EncounterClass = encounterBaseData.Class;
                        this.gameObject.name = encounterBaseData.Name;
                        this._Name = encounterBaseData.Name;
                        if(Level == 0)
                            this.Level = encounterBaseData.baseLevel;
                        this.EncounterElements.AddElement(encounterBaseData.Element);
                        this._Type = (int)CharacterType.Encounter;
                        if(encounterBaseData.HealthModifier > 0f)
                            this._HealthModifier = encounterBaseData.HealthModifier;

                        if (encounterBaseData.baseExp > 0f)
                            this.Exp = encounterBaseData.baseExp;
                        else if(encounterBaseData.baseExp == 0)
                            this.Exp = Formula.ExpCalculation(this.Level);

                        IsFlying = encounterBaseData.IsFlying;
                        IsAntiMagic = encounterBaseData.IsAntiMagic;
                        IsAntiPhysical = encounterBaseData.IsAntiPhysical;

                        this.enemyType = encounterBaseData.EnemyType;
                        this._BaseWeapon = new Weapon((Weapon.DamageType)encounterBaseData.Weapon);
                        this._BaseArmor = new Armor((Armor.ArmorType)encounterBaseData.Armor);
                        this._Projectile = encounterBaseData.Projectile;
                        
                        for (int x = 0; x < encounterBaseData.EncounterBaseAttributes.Count; x++)//each (BattleAttributeTest dbBaseAttribute in encounterBaseData.EncounterBaseAttributes)
                        {
                            BattleAttributeTest dbBaseAttribute = encounterBaseData.EncounterBaseAttributes[x];
                            if (_BaseBattleAttributes.ContainsKey(dbBaseAttribute._Attribute))
                                _BaseBattleAttributes[dbBaseAttribute._Attribute] = dbBaseAttribute._Value;
                            else
                            {
                                _BaseBattleAttributes.Add(dbBaseAttribute._Attribute, dbBaseAttribute._Value);//It means secondary attribute
                            }
                        }

                        if (EncForm.ReplaceAttributes)
                            ReplacePrimaryAttributes();

                        foreach (var magicPreset in encounterBaseData.EncounterMagics)
                        {
                            //Magics.LearnMagicPreset(magicPreset, this._ID,false);
                            Magics.LearnMagicPreset(LegrandBackend.Instance.GetMagic(magicPreset.ID), this._ID, false);
                        }
                        foreach (var skillPresent in encounterBaseData.OffensiveBattleSkills) 
						{
                            //var skillPresent = encounterBaseData.EncounterSkills[x];
							_Skills.Add (skillPresent);
						}
						//Load Item (30 sept blm ada)
                        for (int y = 0; encounterBaseData.EncounterItems.Count > 0 && y < encounterBaseData.EncounterItems.Count; y++)//each(var item in encounterBaseData.EncounterItems)
						{
                            var item = encounterBaseData.EncounterItems[y];
							/*for(int it=0;it<LegrandBackend.Instance.LegrandDatabase.ConsumableHealingItemData.Count;it++)
						{
							if(LegrandBackend.Instance.LegrandDatabase.ConsumableHealingItemData.Get(it)._ID.Equals(item.HealingAndMagic))
							{
								
								Items[item.Index].ItemMaster = LegrandBackend.Instance.LegrandDatabase.ConsumableHealingItemData.Get(it);
								break;
							}
						}
						for(int it=0;it<LegrandBackend.Instance.LegrandDatabase.MagicItemData.Count;it++)
						{
							if(LegrandBackend.Instance.LegrandDatabase.MagicItemData.Get(it)._ID.Equals(item.HealingAndMagic))
							{
								Items[item.Index].ItemMaster = LegrandBackend.Instance.LegrandDatabase.MagicItemData.Get(it);
								break;
							}
						}*/
							Items[item.Index].ItemMaster = LegrandBackend.Instance.ItemData[item.HealingAndMagicItemID];
							Items[item.Index].Stack = item.Stack;
						}
						//Loots
                        for (int x = 0; x < encounterBaseData.EncounterLoots.Count; x++)//each(var loot in encounterBaseData.EncounterLoots)
						{
                            var loot = encounterBaseData.EncounterLoots[x];
                            Loot monsterLoot = null;
                            if (loot.Chance > 0)
                                monsterLoot = new Loot(LegrandBackend.Instance.ItemData[loot.AllItemID], loot.Amount, loot.Chance);
                            else
							    monsterLoot = new Loot(LegrandBackend.Instance.ItemData[loot.AllItemID], loot.Amount);
							_Loots.Add(monsterLoot);
						}
                        _Loots = _Loots.OrderBy(x => x.Chance).ToList();

                        foreach (BattleGrowthDataTest growthData in encounterBaseData.BattleGrowth)
                            _BattleAttributeGrowths[growthData.attributeType] = growthData.value;

                        ResistDebuff = encounterBaseData.IsResistDebuff;
                        ResistException = new List<int>(encounterBaseData.DebuffException);
                        found = true;
					}
				}
            }
        }



        #region Attributes
        void LoadBaseBattleAttributes()
        {
            // Change this one when level up
            _BaseBattleAttributes.Add(Attribute.STR, 1);
            _BaseBattleAttributes.Add(Attribute.LUCK, 1);
            _BaseBattleAttributes.Add(Attribute.INT, 1);
            _BaseBattleAttributes.Add(Attribute.AGI, 1);
            _BaseBattleAttributes.Add(Attribute.VIT, 1);

            // Load Attribute Asli
        }

        public void CalculateAttributes()
        { 
            // Make sure we start from nothing
            if(_BattleAttributes != null)
                _BattleAttributes.Clear();
            
            CalculatePrimaryAttributes(); 
            CalculateSecondaryAttributes(); 

            AssignHealths();
        }
        
        void CalculatePrimaryAttributes()
        {
            _BattleAttributes.Add(Attribute.STR, (int)_BaseBattleAttributes[Attribute.STR]);
            _BattleAttributes.Add(Attribute.INT, (int)_BaseBattleAttributes[Attribute.INT]);
            _BattleAttributes.Add(Attribute.AGI, (int)_BaseBattleAttributes[Attribute.AGI]);
            _BattleAttributes.Add(Attribute.VIT, (int)_BaseBattleAttributes[Attribute.VIT]);
            _BattleAttributes.Add(Attribute.LUCK, (int)_BaseBattleAttributes[Attribute.LUCK]);

			int total = 0;
			int countAttribute = 0;
			foreach (KeyValuePair<Attribute,object> attribute in _BattleAttributes) 
			{
				total =  total + (int) attribute.Value;
				countAttribute++;
			}

			AverageAttribute = total / countAttribute;
        }

		void ReplacePrimaryAttributes ()
		{
            for (int i = 0; i < EncForm.EncounterBaseAttributes.Count; i++)//each (BattleAttributeTest bt in EncForm.EncounterBaseAttributes) 
			{
                BattleAttributeTest bt = EncForm.EncounterBaseAttributes[i];
				if (!_BaseBattleAttributes.ContainsKey(bt._Attribute))
                    _BaseBattleAttributes.Add(bt._Attribute, bt._Value);
                else
                    _BaseBattleAttributes[bt._Attribute] = bt._Value;
            }
		}

		void ReplaceHealthValue (){
            if(!_BattleAttributes.ContainsKey(Attribute.Health))
			    _BattleAttributes.Add(Attribute.Health, EncForm.HealthMonster);
            else _BattleAttributes[Attribute.Health] = EncForm.HealthMonster;
        }
        
        void CalculateSecondaryAttributes()
        {
            if (!EncForm.ReplaceHealth)
            {
                int health = 0;
                if (_HealthModifier > 0f)
                    health = (int)_HealthModifier;
                else
                    health = Formula.CalculateHealth(this);
                _BattleAttributes.Add(Attribute.Health, health);
            }
            else
				ReplaceHealthValue ();
        }


		public int RoundBaseAttributes()
		{
			return (int)(((int)_BaseBattleAttributes [Attribute.STR] + (int)_BaseBattleAttributes [Attribute.AGI] + (int)_BaseBattleAttributes [Attribute.INT] + (int)_BaseBattleAttributes [Attribute.LUCK] + (int)_BaseBattleAttributes [Attribute.VIT]) / 5f);
		}
		public override int GetAttribute (Attribute attribute)
		{
			return (int)_BattleAttributes [attribute];
		}
		
		public override float GetSecondaryAtt (Attribute attribute){
			float value = 1f;
			if(_BattleAttributes.ContainsKey(attribute)) value = (float) _BattleAttributes [attribute];
			return value;
		}
//        public void AddBuff(Buff buff)
//        {
//            int buffId = buff._ID;
//            int turn;
//            
//            bool exist = PostBattleAttributes.IsBuffActive(buffId);
//            
//            if (!exist)
//            {
//                // Apply attributes
//                _PostBattleAttributes.ApplyBuff(buff);
//                CalculateAttributes();
//            }
//
//        }

        #endregion
        #region Healths
        
        void AssignHealths()
        {
            if (_Health == null)
            {
                _Health = gameObject.AddComponent<Health>();
                _Health.Name = "HP";
                _Health.ChangeMaximumValue((int)_BattleAttributes[Attribute.Health]);
				_Health.ChangeCurrentValue(_Health.MaxValue);
            }
            else _Health.ChangeMaximumValue((int)_Health.Value);
        }
        
        public override void ReceiveDamage(float damage)
        {
            // Also should calculate this one
            _Health.Decrease(damage);
        }
		public override Health GetHealth ()
		{
			return Health;
		}
        #endregion

		#region Item
		public override Item GetItem (int id)
		{
			return Items[id].ItemMaster;
		}

        public bool UseItem(int index, List<Battler> targets, Battler source)
        {
            // return true kalau terpakai 
            bool used = false;
            for (int i = 0; i < targets.Count; i++) //each (Battler target in targets)
            {
                if(UseItem(_Items[index].ItemMaster, targets[i], source))
                    used = true;
            }
            return used;
        }

        public override bool UseItem (int index, Battler target, Battler source)
		{
			// return true kalau terpakai 
			return UseItem(_Items[index].ItemMaster, target, source);
		}

		private bool UseItem(Item itemToUse, Battler target, Battler source){
			
			bool used = false;	
			if (itemToUse.ItemType == Item.Type.Consumable){
				Consumable consumedItem = (Consumable) itemToUse;

				#region itemhealing
				if(consumedItem.ConsumableType == Consumable.ConsumeType.Healing)
				{
					ConsumableHealing healItem = (ConsumableHealing) consumedItem;
                    for (int i = 0; i < healItem.CuredStatuses.Count; i++)//each(Status curedStatus in healItem.CuredStatuses)
					{
                        Status curedStatus = healItem.CuredStatuses[i];
						if(curedStatus == Status.Dead)
						{
                            for (int x = 0; x < healItem.RestoredAttributes.Count; x++)//each(ConsumableHealing.RestoredAttribute restoredAttribute in healItem.RestoredAttributes)
                            {
                                ConsumableHealing.RestoredAttribute restoredAttribute = healItem.RestoredAttributes[x];
								if(restoredAttribute._Attribute == Attribute.ReviveHealth) 
								{
									float healing = target.Health.MaxValue*((float)restoredAttribute.Value/100f);
									target.Health.Revive(target.Health.MaxValue*((float)restoredAttribute.Value/100f));	
									target.PopUp(Mathf.RoundToInt(healing).ToString(), (int)BattleTextPopUp.BattleTextType.Heal);
								}
							}
						}
						//Jenis cure status lain
					}
					
                    for (int i = 0; i < healItem.RestoredAttributes.Count; i++)//each(ConsumableHealing.RestoredAttribute restoredAttribute in healItem.RestoredAttributes)
					{
                        ConsumableHealing.RestoredAttribute restoredAttribute = healItem.RestoredAttributes[i];
						if(restoredAttribute._Attribute == Attribute.Health)
						{
							float healing = target.Health.MaxValue*((float)restoredAttribute.Value/100f);
							target.Health.Increase(target.Health.MaxValue*((float)restoredAttribute.Value/100f));
							target.PopUp(Mathf.RoundToInt(healing).ToString(), (int)BattleTextPopUp.BattleTextType.Heal);
						}
						//Jenis Heal Lainnya
					}
					
					used = true;
				}
				#endregion

				#region item magic
				else if (consumedItem.ConsumableType == Consumable.ConsumeType.Magic)
				{
					ConsumableMagic magicItem = (ConsumableMagic) consumedItem;
					_ItemController.InitItem(magicItem, target, source);
				}
				#endregion
			}
			return used;
		}


		#endregion

		#region Magic
		public override Magic GetMagic (int id)
		{
            if (_Magics != null)
                return _Magics.MagicPresets.Find(x => x._ID == id).MagicPreset;
            else
                return null;
		}
		#endregion

		#region Skill
        public override OffensiveBattleSkill GetOffensiveSkill (int id)
        {
            if (_Skills.Count > 0)
            {
                return _Skills.Find(x => x.CommandValue == id);
            }
            else
                return null;
        }

        #endregion

        #region Loot
        public void RetrieveLoots(float lootBonus, ref List<VectorStringInteger> retrievedlLoots)
        {
            List<Loot> lootCandidates = new List<Loot>(Loots);
            List<Loot> bossDrop = lootCandidates.FindAll(x => x.ItemLoot.RareType == Item.Rarity.BossOnly);
            if(bossDrop != null)
            {
                for (int i = 0; i < bossDrop.Count; i++)
                {
                    PartyManager.Instance.Inventory.AddItems(bossDrop[i].ItemLoot, bossDrop[i].Amount);
                    VectorStringInteger existLoot = retrievedlLoots.Find(existLoots => existLoots.x == bossDrop[i].ItemLoot.Name);
                    int amount = bossDrop[i].Amount;

                    if (existLoot != null) existLoot.y += amount;
                    else retrievedlLoots.Add(new VectorStringInteger(bossDrop[i].ItemLoot.Name, amount));
                    lootCandidates.Remove(bossDrop[i]);
                }
            }

            float rng = LegrandUtility.Random(0f,100f);
            lootCandidates.RemoveAll(x => rng - lootBonus > x.Chance);
            if(lootCandidates.Count>0)
            {
                lootCandidates.RemoveAll(x => x.Chance > lootCandidates[0].Chance);
                Loot receivedLoot = lootCandidates[LegrandUtility.Random(0,lootCandidates.Count)];

                VectorStringInteger existLoot = retrievedlLoots.Find(existLoots => existLoots.x == receivedLoot.ItemLoot.Name);
                int amount = receivedLoot.Amount;
                PartyManager.Instance.Inventory.AddItems(receivedLoot.ItemLoot, amount);
                if (receivedLoot.Amount > 1) amount = LegrandUtility.Random(1, receivedLoot.Amount + 1);

                if (existLoot != null) existLoot.y += amount;
                else retrievedlLoots.Add(new VectorStringInteger(receivedLoot.ItemLoot.Name, amount));
            }
            
        }
        #endregion
    }
}
