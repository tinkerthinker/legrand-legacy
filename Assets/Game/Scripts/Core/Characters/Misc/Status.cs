﻿using System.Collections;

namespace Legrand.core
{
    public enum Status
    {
		None,
        Focus,
        Regen,
        Cleanse,
        Reflect,
        Dead,
        Drain,
        Cure,
        Burn,
        Silence,
        Paralyze,
        Poison,
        Blind,
        Fear,
        Confuse
    }

	public enum StatusType
	{
		Buff,
		Debuff
	}

	public class WorldStatus
	{
		//Attibute
		public MiscAttribute Attribute;
		public int Value;
		
		// Jumlah Langkah
		public int Duration;
	}
}
//    public int StatusType;
    
//    public enum Content
//    { 
//        OverTime,
//        Instant,
//    }
//    public int StatusContent;
//    
//    public override string ToString()
//    {
//        return "\nID : "+_ID+"\nStatusType : "+(Status.Type)StatusType+"\nStatus Content : "+(Status.Content)StatusContent;
//    }
//}
//
//[System.Serializable]
//public class StatusOverTime : Status
//{
//    [System.Serializable]
//    public class OverTimeModifier
//    {
//        public int _ID;
//        public int StatusID;
//        public int _Attribute; //Attribute
//        public float Value;
//        public bool Percentage; // Are we using percentage or direct value
//        public float Duration;
//        public float TickTime; //Tick..
//    }
//    public OverTimeModifier[] OverTimeModifiers;
//    
//    public override string ToString()
//    {
//        string overTimeMod = "";
//        foreach (var otMod in OverTimeModifiers)
//        {
//            overTimeMod += "\nID : " + otMod._ID + "\nStatusID : " + otMod.StatusID + "\nAttribute : " + (Attribute)otMod._Attribute + "\nValue : " + otMod.Value + "\nPercentage : " + otMod.Percentage + "\nDuration : " + otMod.Duration + "\nTickTime : " + otMod.TickTime;
//        }
//        return base.ToString() + overTimeMod;
//    }
//}
//
//[System.Serializable]
//public class StatusInstant : Status
//{
//    [System.Serializable]
//    public class InstantModifier
//    {
//        public int _ID;
//        public int StatusID;
//        public int _Attribute; //Attribute
//        public float Value;
//        public bool Percentage;
//        public float Duration;
//    }
//    public InstantModifier[] InstantModifiers;
//    
//    public override string ToString()
//    {
//        string intantMod = "";
//        foreach (var inMod in InstantModifiers)
//        {
//            intantMod += "\nID : " + inMod._ID + "\nStatusID : " + inMod.StatusID + "\nAttribute : " + (Attribute)inMod._Attribute + "\nValue : " + inMod.Value + "\nPercentage : " + inMod.Percentage + "\nDuration : " + inMod.Duration;
//        }
//        return base.ToString() + intantMod;
//    }
//}