﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;
//namespace Legrand.core
//{
	[System.Serializable]
	public class EncounterGrowth
	{
		public string Name;
		public List<AttributeValue> AttributeGrowth;

		public EncounterGrowth()
		{
			Name = "";
			AttributeGrowth = new List<AttributeValue> ();
		}
	}
//}