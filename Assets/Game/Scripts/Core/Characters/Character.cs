﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Legrand.core
{
    public class Character : MonoBehaviour
	{
        public enum CharacterType
        {
            Main,
            Encounter
        }

        public const int MaxLevel = 99;
        
        public string _ID;
        
        public string _Name;

        public int _Type;

        virtual public Weapon Weapon()
        {
            return null;
        }

		virtual public Armor Armor()
		{
			return null;
		}

        virtual public Equipments Equipment()
        {
            return null;
        }

        virtual public void ReceiveDamage(float damage){}
		virtual public int GetAttribute(Attribute attribute){
			return 0;
		}
		virtual public float GetSecondaryAtt(Attribute attribute){
			return 0f;
		}
		virtual public bool UseItem(int index, Battler target, Battler source){
			return false;
		}
		virtual public bool AddDeBuff(){
			return false;
		}
		virtual public bool AddBuff(){
			return false;
		}
		virtual public Health GetHealth(){
			return new Health ();
		}
		virtual public Magic GetMagic(int id){
			return new Magic ();
		}
		virtual public Item GetItem(int id){
			return new Item ();
		}
        virtual public DefensiveBattleSkill GetDefensiveBuff(int id)
		{
            return new DefensiveBattleSkill ();
		}
		virtual public OffensiveBattleSkill GetOffensiveSkill(int id)
		{
			return new OffensiveBattleSkill ();
		}

		virtual public NormalSkill GetNormalSkill(int id)
		{
			return new NormalSkill ();
		}

        virtual public float GetGrowth(Attribute attributeType)
        {
            return 1f;
        }

        private bool _IsFlying = false;
        public bool IsFlying
        {
            get { return _IsFlying; }
            set { _IsFlying = value; } 
        }
        private bool _IsAntiMagic = false;
        public bool IsAntiMagic
        {
            get { return _IsAntiMagic; }
            set { _IsAntiMagic = value; }
        }
        private bool _IsAntiPhysical = false;
        public bool IsAntiPhysical
        {
            get { return _IsAntiPhysical; }
            set { _IsAntiPhysical = value; }
        }
    }
}
