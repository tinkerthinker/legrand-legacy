﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Legrand.core
{
	[System.Serializable]
	public class StoryProgression 
	{
		public List<int> PartyModels;
		public List<string> ActiveEventID;                                  //For Story Progression
        public List<string> FinishedEventID;

        public StoryProgression()
        {
            PartyModels = new List<int>();
            for (int i = 0; i < 15; i++)
                PartyModels.Add(0);
            this.ActiveEventID = new List<string>();
            this.FinishedEventID = new List<string>();
        }

        public void Copy(StoryProgression anotherClass)
		{
            PartyModels = new List<int>();
            for(int i=0;i<anotherClass.PartyModels.Count;i++)
                this.PartyModels.Add(anotherClass.PartyModels[i]);

            PartyModels = anotherClass.PartyModels;
            if (anotherClass.ActiveEventID == null)
            {
                this.ActiveEventID = new List<string>();
                this.FinishedEventID = new List<string>();
            }
            else
            {
                this.ActiveEventID = new List<string>(anotherClass.ActiveEventID);
                this.FinishedEventID = new List<string>(anotherClass.FinishedEventID);
            }
		}

        public void ChangeModelData(string ID, int set)
        {
            PartyModels[int.Parse(ID)] = set;
        }

        public int GetLatestModelSet(string ID)
        {
            return PartyModels[int.Parse(ID)];
        }
    }
}