﻿using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class SubArea
{
    public enum Types
    {
        Room,
        CityMap
    }

	public SubArea (string prefabName, string subAreaName, Types subAreaType, Connection[] connections, WwiseManager.BGM BGM, bool hasVersions, RoomVersion[] roomVersion, float mainHorizontalFOV, float secondaryHorizontalFOV, string[] dynamicObjectsName, DungeonMonster[] dungeonMonsters)
	{
		this.prefabName = prefabName;
        this.subAreaName = subAreaName;
		this.subAreaType = subAreaType;
		this.connections = connections;
		this.BGM = BGM;
		this.hasVersions = hasVersions;
		this.roomVersion = roomVersion;
		this.mainHorizontalFOV = mainHorizontalFOV;
        this.secondaryHorizontalFOV = secondaryHorizontalFOV;
        this.dynamicObjectsName = dynamicObjectsName;
        this.dungeonMonsters = dungeonMonsters;
    }
    [System.Serializable]
    public class Connection
    {
        public string portalFrom;
        public string destination;
        public string portalTo;
        public string[] unlockPoint;
        public string[] unlockWorldMapPoint;
        public WwiseManager.WorldFX SFX;
    }

    public string           prefabName;
    public string           subAreaName;
    public Types            subAreaType;
    public Connection[]     connections;
    public WwiseManager.BGM BGM;
    public bool             hasVersions;
    public RoomVersion[]    roomVersion;
    public float            mainHorizontalFOV;
    public float            secondaryHorizontalFOV;
    public string[]         dynamicObjectsName;
    public DungeonMonster[] dungeonMonsters;

    public Connection GetConnectionFrom(string pPortalFrom)
    {
        foreach (Connection conn in connections)
            if(LegrandUtility.CompareString(conn.portalFrom, pPortalFrom)) return conn;

        return null;
    }
}

[System.Serializable]
public class DungeonMonster
{
    public string monsterID;
    public int probabilityMultiplier;

    public DungeonMonster()
    {
        monsterID = "";
        probabilityMultiplier = 1;
    }
}

[System.Serializable]
public class SubAreaInfo
{
    public string   name;
    public string   version;

    public SubAreaInfo()
    {
        name = "";
        version = "";
    }

    public SubAreaInfo(string name, string version)
    {
        this.name = name;
        this.version = version;
    }
}

[System.Serializable]
public class CityPointInfo
{
    public string   name;
    public bool     isVisited;

    public CityPointInfo()
    {
        name = "";
        isVisited = false;
    }

    public CityPointInfo(string name)
    {
        this.name = name;
        this.isVisited = false;
    }
}

[System.Serializable]
public class WorldMapPointInfo
{
    public string name;
    public bool isVisited;

    public WorldMapPointInfo()
    {
        name = "";
        isVisited = false;
    }

    public WorldMapPointInfo(string name)
    {
        this.name = name;
        this.isVisited = false;
    }
}

[System.Serializable]
public class SubAreaFact
{
    public string subAreaName;
    public List<Fact> fact;
    public SubAreaFact()
    {
        subAreaName = "";
        fact = new List<Fact>();
    }
}