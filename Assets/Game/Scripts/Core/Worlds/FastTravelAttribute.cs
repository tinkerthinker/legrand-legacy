﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class FastTravelAttribute {
    public string Name;
    public Area.AreaTypes AreaType;

    public FastTravelAttribute()
    {
        Name = "";
        AreaType = Area.AreaTypes.City;
    }

    public FastTravelAttribute(string name, Area.AreaTypes areaType)
    {
        this.Name = name;
        this.AreaType = areaType;
    }
}
