﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Area 
{
    public enum AreaTypes
    {
        City,
        Dungeon,
        WorldMap
    };
    
    //public int          areaID;
    public string       areaName;
    public string       areaDescription;
    public AreaTypes    areaType;
    public SubArea[]    subArea;

	public Area (string areaName, string areaDescription, AreaTypes areaType, SubArea[] subArea)
	{
		this.areaName = areaName;
		this.areaDescription = areaDescription;
		this.areaType = areaType;
		this.subArea = subArea;
    }

    public SubArea GetSubAreasByName(string pSubAreaName)
    {
        foreach (SubArea sa in subArea)
            if(LegrandUtility.CompareString(sa.prefabName, pSubAreaName)) return sa;

        return null;
    }
}
