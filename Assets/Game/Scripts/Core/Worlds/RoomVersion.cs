﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class RoomVersion
{
    [System.Serializable]
    public class Content
    {
        public Material material;
        public GameObject[] gameObjects;
    }

    public string version;
    public Content modification;

	public RoomVersion()
	{
		version = "";
		modification = new Content();
	}
}
