﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Legrand.core;

[System.Serializable]
public enum TypeDamage{
	Physical,
	Magic
}

[System.Serializable]
public enum SkillTypes {
	NormalOffense,
	NormalDefense,
	UltimateOffense,
	UltimateDefense,
	Guard,
	Combination
}
[System.Serializable]
public class Skill  
{
	public string IdSkill;
    // General description
    public string Name;
    public string Description;

	public Skill()
	{
		IdSkill = "";
		// General description
		Name = "";
		Description = "";
	}
}

[System.Serializable]
public class WarSkill : Skill
{ 
	public enum Type
	{
		Instant,
		Target,
		Point,
		CrowdControl,
		DamageOverTime,
		InstantDamage,
	};
	public int AttributeID;

	[System.Serializable]
	public class WarSkillAttribute
	{
		public int _ID;

		//Attibute
		public Attribute _Attribute;
		public float Value;
		public WarSkillAttribute()
		{
			_ID = 0;
			_Attribute = Attribute.STR;
			Value = 0;
		}
	}

	[HideInInspector]
	public List<WarSkillAttribute> WarSkillAttributes;
	public int SkillType;

	public WarSkill()
	{
		WarSkillAttributes = new List<WarSkillAttribute> ();
		SkillType = 0;
	}
}
[System.Serializable]
public class SkillBuffs : Skill
{
	public int CommandValue;
	public SkillTypes SkillType;
	public float BuffPercentage;
	public TargetType Target;

	public SkillBuffs()
	{
		CommandValue = LegrandUtility.GetUniqueSymbol(IdSkill);
		BuffPercentage = 0;
		Target = TargetType.Single;
	}
}

[System.Serializable]
public class DefensiveBattleSkill : SkillBuffs
{
    public int SkillBaseLevel;
    public int Cost;
    public List<Buff> Buffs;
    public TargetFormation TargetFormationType;

	public DefensiveBattleSkill()
	{
        SkillBaseLevel = 1;
		SkillType = SkillTypes.UltimateDefense;
		Buffs = new List<Buff> ();
		BuffPercentage = 0;
        Cost = 100;
		Target = TargetType.Single;
		TargetFormationType = TargetFormation.Self;
	}
}

[System.Serializable]
public class OffensiveBattleSkill : SkillBuffs
{
    public int SkillBaseLevel;
	public float DamagePercentage;
	public Element Element;
	public TypeDamage DamageType;
    public int Cost;
    public List<BuffChance> BuffsChance;

    public OffensiveBattleSkill()
	{
        SkillBaseLevel = 1;
		SkillType = SkillTypes.UltimateOffense;
		BuffPercentage = 0;
        BuffsChance = new List<BuffChance>();
		Target = TargetType.Single;
		DamagePercentage = 0;
		Element = Element.Air;
		DamageType = TypeDamage.Physical;
        Cost = 100;
    }
}

[System.Serializable]
public class CombinationSkill : OffensiveBattleSkill
{
	public string PartnerID;

	public CombinationSkill ()
	{
		SkillType = SkillTypes.Combination;
		BuffPercentage = 0;
		Target = TargetType.Single;
		DamagePercentage = 0;
		Element = Element.Air;
		DamageType = TypeDamage.Physical;
		PartnerID = "";
	}
}

[System.Serializable]
public class NormalSkill : SkillBuffs
{
    public float DamagePercentage;
    public Element Element;
    public TypeDamage DamageType;
	public TargetFormation TargetFormationType;
	public string NormalSkillParticle;
    public List<BuffChance> BuffsChance;

    public NormalSkill()
	{
        NormalSkillParticle = "";
		SkillType = SkillTypes.NormalOffense;
		BuffsChance = new List<BuffChance> ();
		BuffPercentage = 0;
		Target = TargetType.Single;
		DamagePercentage = 0;
		Element = Element.Air;
		DamageType = TypeDamage.Physical;
		TargetFormationType = TargetFormation.Enemy;
	}
}

[System.Serializable]
public class GuardBuff : SkillBuffs{
    public int SkillBaseLevel;
    public List<Buff> Buffs;
    public TargetFormation TargetFormations;

    public GuardBuff()
	{
        SkillBaseLevel = 1;
		SkillType = SkillTypes.Guard;
		Buffs = new List<Buff> ();
		BuffPercentage = 0;
		Target = TargetType.Single;
        TargetFormations = TargetFormation.Self;
	}
}

//[System.Serializable]
//public class EncounterSkill : Skill
//{
//	public SkillTypes SkillType;
//	public float DamagePrecentage;
//	public TypeDamage DamageType;
//	public List<Buff> StatusEffect;
//	public float BuffPercentage;
//	public TargetType Target;
//	public TargetFormation TargetForm;
//	public Element Element;
//	public string Particle;
//
//	public EncounterSkill()
//	{
//		StatusEffect = new List<Buff>();
//		SkillType = SkillTypes.NormalDefense;
//		DamagePrecentage = 0;
//		DamageType = TypeDamage.Magic;
//		Target = TargetType.Single;
//		TargetForm = TargetFormation.Self;
//		Element = Element.Fire;
//		Particle = "";
//	}
//}
