﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Legrand.core;

[System.Serializable]
public class OffensiveSkillPreset
{
    public OffensiveBattleSkill OffenseSkillPreset;
    public int _ID;
    public string CharacterId;

    public OffensiveSkillPreset(int id, OffensiveBattleSkill offensiveSkill, bool isNameDefault)
    {
        _ID = id;
        OffenseSkillPreset = offensiveSkill;
    }   

    public OffensiveSkillPreset(int id, string CharacterId ,OffensiveBattleSkill offensiveSkill, bool isNameDefault)
    {
        _ID = id;
        this.CharacterId = CharacterId;
        OffenseSkillPreset = offensiveSkill;
    }

     
}

[System.Serializable]
public class OffensiveSkillPresets
{
    private Dictionary<int, bool> _IDOffenseSkillPresetDictionary;

    [SerializeField]
    private List<OffensiveSkillPreset> _SkillPresets;
    public List<OffensiveSkillPreset> SkillPresets
    {
        get { return _SkillPresets; }
    }

    public OffensiveSkillPresets()
    {
        _IDOffenseSkillPresetDictionary = new Dictionary<int, bool> ();
        _SkillPresets  = new List<OffensiveSkillPreset>();
    }

    public OffensiveSkillPreset GetSkillPreset(int skillPresetID)
    {
        foreach (OffensiveSkillPreset skillPreset in SkillPresets)
        {
            if (skillPreset._ID == skillPresetID)
            {
                return skillPreset;
            }
        }
        return null;
    }




    public bool UnlearnSkillPreset(int id)
    {
        foreach (OffensiveSkillPreset magic in SkillPresets) {
            if (magic._ID == id)
            {
                SkillPresets.Remove(magic);
                _IDOffenseSkillPresetDictionary[id] = false;
                return true;
            }
        }

        return false;
    }
}