﻿using UnityEngine;
using System.Collections;

public class ResponseDatabase : MonoBehaviour {
    public void ExecuteResponse(string responseName)
    {
        if (responseName == null || responseName == "")
            return;

        Invoke(responseName, 0f);
    }

    void OpenDringrsKeepEntranceDoor()
    {
        Instantiate(Resources.Load<GameObject>("Cinematics/World Sequences/Dringrs Keep Entrance Door Open"));
    }

    void OpenDringrsKeepInsideCDoor()
    {
        Instantiate(Resources.Load<GameObject>("Cinematics/World Sequences/Dringrs Keep Inside C Door Open"));
    }

    void SummonDringrsKeepBauBau()
    {
        Instantiate(Resources.Load<GameObject>("Cinematics/World Sequences/Dringrs Keep Bau Bau Spawner Sequence"));
    }

    void InsertBlueStone()
    {
        Instantiate(Resources.Load<GameObject>("Cinematics/World Sequences/Dunabad Cave Inside E Stone Inserted"));
        Memory m1 = PartyManager.Instance.WorldInfo.GetFact("DunabadCaveInsideEStoneInserted");
        Memory m2 = PartyManager.Instance.WorldInfo.GetFact("DunabadCaveInsideFStoneInserted");
        if ((m1 != null && m1.Value == 1) && (m2 != null && m2.Value == 1))
        {
            WwiseManager.Instance.PlaySFX(WwiseManager.CutsceneFX.Dringrs_Keep_Ground);
        }
    }

    void InsertPurpleStone()
    {
        Instantiate(Resources.Load<GameObject>("Cinematics/World Sequences/Dunabad Cave Inside F Stone Inserted"));
        Memory m1 = PartyManager.Instance.WorldInfo.GetFact("DunabadCaveInsideEStoneInserted");
        Memory m2 = PartyManager.Instance.WorldInfo.GetFact("DunabadCaveInsideFStoneInserted");
        if ((m1 != null && m1.Value == 1) && (m2 != null && m2.Value == 1))
        {
            WwiseManager.Instance.PlaySFX(WwiseManager.CutsceneFX.Dringrs_Keep_Ground);
        }
    }

    void HealSphereDialog()
    {
        EventManager.Instance.TriggerEvent(new TalkToPlayerEvent(false));
        LegrandUtility.StartDialogue(LegrandUtility.GetDialogueID("CH1_Tutorial_Healing_Point"));
    }

    void SearchLazarusNotif()
    {
        EventManager.Instance.TriggerEvent(new TalkToPlayerEvent(false));
        LegrandUtility.StartDialogue(LegrandUtility.GetDialogueID("CH1_Find_Lazarus_Notif"));
    }

    void ErisLittleTripProgressing()
    {
        Fact latestFact = PartyManager.Instance.WorldInfo.GetFact("ErisLittleTripProgress");
        if (latestFact == null) PartyManager.Instance.WorldInfo.NewFact("ErisLittleTripProgress", 1);
        else
        {
            latestFact.Value += 1;
        }
    }
}
