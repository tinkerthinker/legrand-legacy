﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class CriterionStatic{
	[Header("Criterion adalah key")]
	public string Criterion;
	[Header("Fa dan Fb adalah value, gunakan rule IEEE754; Valve said string value is for noob")]
	public double Fa;
	public double Fb;
    public CriterionStatic()
    {
        Criterion = "";
        Fa = 0;
        Fb = 0;
    }

	public bool Compare(double x)
	{
		return Fa <= x && x <= Fb; 
	}

	public bool IsZero()
	{
		if (Fa == 0 && Fb == 0)
			return true;
		else
			return false;
	}
}
