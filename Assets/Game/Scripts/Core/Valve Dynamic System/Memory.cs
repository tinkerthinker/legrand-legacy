﻿using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class Memory{
	public string Name;
	public double Value;

    public Memory()
    {
        Name = "";
        Value = 0;
    }

    public Memory(string name, double value)
	{
		Name = name;
		Value = value;
	}
}

[System.Serializable]
public class Fact : Memory
{
    public string Owner;
    public List<string> ActiveGameObjects;
    public List<string> InactiveGameObjects;

    public Fact()
    {
        Owner = "";
        ActiveGameObjects = new List<string>();
        InactiveGameObjects = new List<string>();
    }

    public Fact(string name, double value, string factOwner, List<string> activeGameObjects, List<string> inactiveGameObjects) : base(name, value)
    {
        this.Owner = factOwner;
        this.ActiveGameObjects = activeGameObjects;
        this.InactiveGameObjects = inactiveGameObjects;
    }
}
