﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class StringConverter : MonoBehaviour {
	public string KeyString;
	public int SymbolValue;

    // Update is called once per frame
    void Update () {
		if (KeyString != "") 
		{
			SymbolValue = LegrandUtility.GetUniqueSymbol(KeyString);
		}
	}
}
