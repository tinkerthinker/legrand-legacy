﻿using UnityEngine;
using System.Collections;

public class DynamicSystemController : MonoBehaviour {
	public static DynamicSystemController _Instance;

	[HideInInspector]
	public RuleDatabase RuleDB;
	[HideInInspector]
	public ResponseDatabase ResponseDB;
	[HideInInspector]
	public StoredContent Memories;

	void Awake()
	{
		_Instance = this;
		RuleDB = GetComponent <RuleDatabase> ();
		ResponseDB = GetComponent <ResponseDatabase> ();
		Memories = GetComponent <StoredContent> ();
	}
}
