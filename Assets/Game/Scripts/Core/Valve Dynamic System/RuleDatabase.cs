﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class RuleDatabase : MonoBehaviour
{
    public RulesDatabase rulesData;
	public List<Rule> Rules;
    private RuleResult BiggestMatchResult;

    public void AddChangeAreaListener(Area currentArea = null)
    {
        if (EventManager.Instance != null && !EventManager.Instance.HasListener<LoadAreaEvent>(LoadRules))
        {
            EventManager.Instance.AddListener<LoadAreaEvent>(LoadRules);
            if(currentArea != null)
                LoadRules(new LoadAreaEvent(currentArea));
        }
    }
    void LoadRules(LoadAreaEvent e)
    {
        Rules.Clear();
        for (int i = 0; i < rulesData.Count; i++)
        {
            if (rulesData.Get(i).AreaName.Equals(e.AreaData.areaName))
            {
                Rules.Add(DeepCopy<Rule>(rulesData.Get(i)));
            }
        }
    }
    T DeepCopy<T>(T obj)
    {
        using (MemoryStream stream = new MemoryStream())
        {
            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Serialize(stream, obj);
            stream.Position = 0;
            return (T)formatter.Deserialize(stream);
        }
    }

	public bool Matching(Dictionary<string, double> query)
	{
		BiggestMatchResult = null;
		foreach (Rule rule in Rules) 
		{
			RuleResult tempResult = rule.Matching (query);
			if(tempResult.IsMatched && (BiggestMatchResult == null || BiggestMatchResult.NumOfMatchedCriterion <= tempResult.NumOfMatchedCriterion))
            {
                BiggestMatchResult = tempResult;
            }				
		}

        if (BiggestMatchResult != null)
        {
            Debug.Log("Rule Matching Result : " + BiggestMatchResult.ResponseName);
            return true;
        }
		return false;
	}

    public RuleResult GetLatestResult(){
        return BiggestMatchResult;
    }

    public string Storing()
    {
        if (BiggestMatchResult != null)
        {
            foreach (Fact fact in BiggestMatchResult.Facts)
                PartyManager.Instance.WorldInfo.NewFact(fact.Name, fact.Value, fact.Owner, fact.ActiveGameObjects, fact.InactiveGameObjects);
            return BiggestMatchResult.ResponseName;
        }

        return null;
    }
}

[System.Serializable]
public class Rule{
	public string Name;
    public string ResponseName;
    public string AreaName;
	public List<CriterionStatic> Criterions;
	public List<Fact> ApplyFacts;

    public Rule()
    {
        Name = "";
        ResponseName = "";
        Criterions = new List<CriterionStatic>();
        ApplyFacts = new List<Fact>();
    }

	public RuleResult Matching(Dictionary<string, double> KeyPair)
	{
		RuleResult result = new RuleResult();
		result.ResponseName = ResponseName;
		result.Facts = ApplyFacts;
        
        foreach (CriterionStatic criterion in Criterions) 
		{
            string log = string.Empty;
            double keyValue = 0;
			if (KeyPair.ContainsKey (criterion.Criterion))
				keyValue = KeyPair [criterion.Criterion];
            
			if (criterion.Compare(keyValue)) {
                result.NumOfMatchedCriterion += 1;	
			}
            else
            {
                result.IsMatched = false;
                return result;
            }
        }

        if (result.NumOfMatchedCriterion == Criterions.Count)
			result.IsMatched = true;

        return result;
	}
}

[System.Serializable]
public class RuleResult
{
	public string ResponseName;
	public bool IsMatched;
	public int NumOfMatchedCriterion;
	public List<Fact> Facts;

	public RuleResult()
	{
		IsMatched = false;
		NumOfMatchedCriterion = 0;
	}
}