﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ResponsiveQuery{
	private Dictionary<string, double> _Query;

	public ResponsiveQuery()
	{
		_Query = new Dictionary<string, double> ();
	}

	public void Add(string key, double x)
	{
		_Query.Add (key, x);
	}

    public void Add(Dictionary<string, double> datas)
    {
        _Query = new Dictionary<string, double>(datas);
    }

    public void Add(List<Fact> facts)
    {
        foreach (Fact fact in facts)
            _Query.Add(fact.Name, fact.Value);
    }

    public void Add(List<VectorStringDouble> datas)
    {
        foreach (VectorStringDouble v in datas)
            _Query[v.x] = v.y;
    }

    public bool Match()
	{
		return DynamicSystemController._Instance.RuleDB.Matching (_Query);
	}
}
