﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StoredContent : MonoBehaviour {
	public Dictionary<string, float> Datas;

	void Start()
	{
		Datas = new Dictionary<string, float> ();
	}
    
	public void NewMemory(string key, float value)
	{
		if (Datas.ContainsKey (key))
			Datas [key] = value;
		else
			Datas.Add (key, value);
	}

	public void RemoveMemory(string key, float value)
	{
		if (Datas.ContainsKey (key) && Datas[key] == value)
			Datas.Remove (key);
	}
}
