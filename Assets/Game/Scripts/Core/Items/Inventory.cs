﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;

[System.Serializable]
public class Inventory
{
    public List<ItemSlot>[] Items;
    public List<Fact>[] InventoryFacs;

    public int Gold;
    private int _CurrentWeight;
    public int CurrentWeight
    {
        get { return _CurrentWeight; }
        set
        {
            _CurrentWeight = value;
            if(!IsOverWeight && _CurrentWeight > MaxWeight)
            {
                IsOverWeight = true;
                EventManager.Instance.TriggerEvent(new SetEncumberedEvent(IsOverWeight));
            }
            else if(IsOverWeight && _CurrentWeight <= MaxWeight)
            {
                IsOverWeight = false;
                EventManager.Instance.TriggerEvent(new SetEncumberedEvent(IsOverWeight));
            }
        }
    }
    public int MaxWeight;
    public bool IsOverWeight;

    public Inventory()
    {
        Gold = 0;
        MaxWeight = 200;
        Items = new List<ItemSlot>[5];
        for (int i = 0; i < Items.Length; i++)
        {
            Items[i] = new List<ItemSlot>();
        }
        InventoryFacs = new List<Fact>[5];
        for (int i = 0; i < InventoryFacs.Length; i++)
        {
            InventoryFacs[i] = new List<Fact>();
        }
        IsOverWeight = false;
    }

	public bool Contains(Item item)
	{
		foreach (ItemSlot invenSlot in Items[(int)item.ItemType])
		{
			if (invenSlot.ItemMaster._ID == item._ID)
			{
				return true;
			}
		}
		return false;
	}

	public int GetStackItem(Item item)
	{
		foreach (ItemSlot invenSlot in Items[(int)item.ItemType])
		{
			if (invenSlot.ItemMaster._ID == item._ID)
			{
				return invenSlot.Stack;
			}
		}
		return 0;
	}

    public bool AddItem(Item itemToAdd)
    {
        return AddItems(itemToAdd, 1);
    }

    public bool AddItems(Item itemToAdd, int amount)
    {
        // true = berhasil // false = gagal
            //bool sameItem = false;
        ItemSlot inventoryItem = Items[(int)itemToAdd.ItemType].Find(x => x.ItemMaster._ID == itemToAdd._ID);
        if(inventoryItem != null)
            inventoryItem.Stack += amount;
        else
        {
            inventoryItem = new ItemSlot(itemToAdd);
            inventoryItem.Stack = amount;
            Items[(int)itemToAdd.ItemType].Add(inventoryItem);
            SortItem((int)itemToAdd.ItemType);
        }
            //foreach (ItemSlot charItem in )
            //{
            //    if (charItem.ItemMaster._ID == itemToAdd._ID)
            //    {
            //        charItem.Stack += amount;
            //        sameItem = true;
            //        //Debug.Log("StackItemAdded " + amount);

            //    }
            //}

    //        if (sameItem == false)
    //        {
    //            ItemSlot tempItem = new ItemSlot(itemToAdd);
    //            tempItem.Stack = amount;
    //            //saveDatabase n dapet ID
    //            Items[(int)itemToAdd.ItemType].Add(tempItem);
    //            //Debug.Log("Create New Stack Item " + amount);
				//SortItem((int)itemToAdd.ItemType);
    //        }

        Fact fact = InventoryFacs[(int)itemToAdd.ItemType].Find(x => x.Name == "Has Item " + itemToAdd._ID);
        if (fact == null) InventoryFacs[(int)itemToAdd.ItemType].Add(new Fact("Has Item " + inventoryItem.ItemMaster._ID, inventoryItem.Stack, string.Empty, null, null));
        else fact.Value = inventoryItem.Stack;

            CurrentWeight += itemToAdd.Weight * amount; //Add Size

            // Check key effect
            if (itemToAdd.ItemType == Item.Type.Key)
            {
                Key addedKeyItem = (Key)itemToAdd;
                for (int i = 0; addedKeyItem.ItemEffect != null && i < addedKeyItem.ItemEffect.Count; i++)
                {
                    //if it's blueprint
                    if (addedKeyItem.ItemEffect[i]._Attribute == MiscAttribute.Blueprint)
                    {
                        PartyManager.Instance.UnlockedBlueprints.Add(LegrandBackend.Instance.GetBlueprint(addedKeyItem.ItemEffect[i]._Value));
                    }
                }
            }
            return true;
    }

    public int DeleteItemsByItemID(string ItemID, int number)
    {
        // 0 = succeed, 1 = item not found, 2 = unrealistic amount, 3 = other
        int i = 0;
        while (i < Items.Length)
        {
            foreach (ItemSlot invenSlot in Items[i])
            {
                if (invenSlot.ItemMaster._ID == ItemID)
                {
                    if (invenSlot.Stack >= number)
                    {
                        //Debug.Log("ReduceStack " + invenSlot.Stack + " by " + number);
                        invenSlot.Stack -= number;
                        Fact fact = InventoryFacs[(int)invenSlot.ItemMaster.ItemType].Find(x => x.Name == "Has Item " + invenSlot.ItemMaster._ID);
                        if (fact == null) InventoryFacs[(int)invenSlot.ItemMaster.ItemType].Add(new Fact("Has Item " + invenSlot.ItemMaster._ID, invenSlot.Stack, string.Empty, null, null));
                        else fact.Value = invenSlot.Stack;

                        if (invenSlot.Stack == 0)
                        {
                            Items[i].Remove(invenSlot);
                            //Debug.Log("Delete Item");
                        }
                        CurrentWeight -= invenSlot.ItemMaster.Weight * number;
                        return 0;
                    }
                    else
                    {
                        //Debug.Log("Unrealistic amount");
                        return 2;
                    }


                }

            }
            i++;
        }
        //Debug.Log("NotFound");
        return 1;
    }

    public ItemSlot GetItem(string itemID)
    {
        // return true kalau terpakai 
        foreach (ItemSlot itemSlot in Items[(int)Item.Type.Consumable])
        {
            if (itemSlot.ItemMaster != null && itemSlot.ItemMaster._ID == itemID)
            {
                return itemSlot;
            }
        }
        return null;
    }

	public ItemSlot GetItem(string itemID, Item.Type typeItem)
	{
		// return true kalau terpakai 
		foreach (ItemSlot itemSlot in Items[(int)typeItem])
		{
			if (itemSlot.ItemMaster != null && itemSlot.ItemMaster._ID == itemID)
			{
				return itemSlot;
			}
		}
		return null;
	}

    public bool UseItem(string itemID, List<string> targetIDs)
    {
        // return true kalau terpakai 
        foreach (ItemSlot itemSlot in Items[(int)Item.Type.Consumable])
        {
            if (itemSlot.ItemMaster != null && itemSlot.Stack > 0)
            {
                if (itemSlot.ItemMaster._ID == itemID)
                {
                    if (UseItem(itemSlot.ItemMaster, targetIDs))
                    {
                        DeleteItemsByItemID(itemID, 1);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public bool UseItem(Item itemToUse, List<string> targetIDs)
    {
        bool used = false;
        if (itemToUse.ItemType == Item.Type.Consumable)
        {
            Consumable consumedItem = (Consumable)itemToUse;
            #region itemhealing
            if (consumedItem.ConsumableType == Consumable.ConsumeType.Healing)
            {
                ConsumableHealing healItem = (ConsumableHealing)consumedItem;
                foreach (Status curedStatus in healItem.CuredStatuses)
                {
                    if (curedStatus == Status.Dead)
                    {
                        foreach (ConsumableHealing.RestoredAttribute restoredAttribute in healItem.RestoredAttributes)
                        {
                            if (restoredAttribute._Attribute == Attribute.ReviveHealth)
                            {
                                foreach (MainCharacter partyMember in PartyManager.Instance.CharacterParty)
                                {
                                    if (targetIDs.Contains(partyMember._ID))
                                        partyMember.Health.Revive(partyMember.Health.MaxValue * ((float)restoredAttribute.Value / 100f));
                                }
                            }
                        }
                    }
                    //Jenis cure status lain
                }

                foreach (ConsumableHealing.RestoredAttribute restoredAttribute in healItem.RestoredAttributes)
                {
                    if (restoredAttribute._Attribute == Attribute.Health)
                    {
                        foreach (MainCharacter partyMember in PartyManager.Instance.CharacterParty)
                        {
                            if (targetIDs.Contains(partyMember._ID))
                                partyMember.Health.Increase(partyMember.Health.MaxValue * ((float)restoredAttribute.Value / 100f));
                        }
                    }
                    //Jenis Heal Lainnya
                }

                used = true;
            }
            #endregion

            #region itemattribute
            if (consumedItem.ConsumableType == Consumable.ConsumeType.Attributes)
            {
                ConsumableAttribute attributeItem = (ConsumableAttribute)consumedItem;
                foreach (ConsumableAttribute.RaisedAttribute raisedAttribute in attributeItem.RaisedAttributes)
                {
                    foreach (MainCharacter partyMember in PartyManager.Instance.CharacterParty)
                    {
                        if (targetIDs.Contains(partyMember._ID))
                        {
                            partyMember.LevelUp(raisedAttribute._Attribute, raisedAttribute._Value);
                        }
                    }
                }
                used = true;
            }
            #endregion
            
            foreach (MainCharacter partyMember in PartyManager.Instance.CharacterParty)
            {
                if (targetIDs.Contains(partyMember._ID))
                {
                    partyMember.CalculateAttributes(false);
                }
            }
        }


        return used;
    }

    public void DeleteAll()
    {
        CurrentWeight = 0;
        for (int i = 0; i < 7; i++)
        {
            Items[i].Clear();
        }
    }

    public int BuyItemByItemID(string itemID)
    {
        //0 = success, 1 = not enough gold, 2 = not enough weight
        bool res;
        Item item = LegrandBackend.Instance.ItemData[itemID];
        if (Gold - item.BuyPrice > 0)
        {
            res = AddItem(item);
            if (res)
            {
                Gold -= item.BuyPrice;
                return 0;
            }
            else
                return 2;
        }
        else
            return 1;
    }

    public int BuyItemsByItemID(string itemID, int amount)
    {
        //0 = success, 1 = not enough gold or no item buyed, 2 = not enough weight
        bool res;
        Item item = LegrandBackend.Instance.ItemData[itemID];
        if (Gold - (item.BuyPrice * amount) >= 0 && amount > 0)
        {
            res = AddItems(item, amount);
            if (res)
            {
                Gold -= item.BuyPrice * amount;
                return 0;
            }
            else
                return 2;
        }
        else
            return 1;
    }

    public void SellItemByItemID(string itemID)
    {
        int res;
        Item item = LegrandBackend.Instance.ItemData[itemID];
        res = DeleteItemsByItemID(itemID, 1);
        if (res == 0)
            Gold += item.SellPrice;
    }

    public void SellItemsByItemID(string itemID, int amount)
    {
        int res;
        Item item = LegrandBackend.Instance.ItemData[itemID];
        res = DeleteItemsByItemID(itemID, amount);
        if (res == 0)
            Gold += item.SellPrice * amount;
    }

    public int CompareItem(ItemSlot a, ItemSlot b)
    {
        int value = 0;
        // consumable, material, blueprint sorted by name
        //value = (a.ItemMaster.Name.CompareTo(b.ItemMaster.Name));

        if (a.ItemMaster.ItemType == Item.Type.Consumable)
        {
            value = (int)((Consumable)a.ItemMaster).ConsumableType - (int)((Consumable)b.ItemMaster).ConsumableType;
            if (value == 0)
                value = (a.ItemMaster._ID.CompareTo(b.ItemMaster._ID));
        }
        else if (a.ItemMaster.ItemType == Item.Type.CraftingMaterial)
        {
            value = (a.ItemMaster.Name.CompareTo(b.ItemMaster.Name));
        }
        else if (a.ItemMaster.ItemType == Item.Type.Key)
        {
            value = value = (a.ItemMaster._ID.Substring(0,2).CompareTo(b.ItemMaster._ID.Substring(0,2)));
            if(value == 0)
                value = (a.ItemMaster.Name.CompareTo(b.ItemMaster.Name));
        }
        else
            value = (a.ItemMaster._ID.CompareTo(b.ItemMaster._ID));

        return value;

    }

    public void RecalculateCurrentSize()
    {
        CurrentWeight = 0;
        for (int i = 0; i < Items.Length; i++)
        {
            foreach (var it in Items[i])
            {
                CurrentWeight += (it.Stack * it.ItemMaster.Weight);
            }
        }
    }

    public void SortItem(int type)
    {
        Items[type].Sort(CompareItem);
    }

	void SortAll()
	{
		for (int i = 0; i < Items.Length; i++) {
			SortItem (i);
		}
	}

    public List<Fact> GetInventoryFacts(Item.Type type)
    {
        return InventoryFacs[(int)type];
    }
}
