﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.Collections;
using System.Collections.Generic;
using Legrand.core;

[System.Serializable]
public class Consumable : Item 
{
	public enum ConsumeType
	{
		Healing,	// darah/HP
		//Cure,		// Menghilangkan segala jenis status
		//Effect,	// Memberikan status (poison, regen, silence and friends) -- bisa temporary, bisa instant
		Magic,		// memberikan / menghilangkan status selama battle
		Attributes,	// increase attribute permanent
		Misc
	};
	
	public ConsumeType ConsumableType;
	public TargetType Target;
	
    public string ItemParticle;
	
	public Consumable()
	{
		ConsumableType = ConsumeType.Healing;
		Target = TargetType.Single;
        ItemParticle = "";
	}
}

[System.Serializable]
public class ConsumableHealing : Consumable
{
	[System.Serializable]
	public class RestoredAttribute
	{
		public int _ID;
		public string ItemID;
		
		//Attibute
		public Attribute _Attribute; // Contrary to the field, we only restore Health and Energy
		public int Value;
		
		public RestoredAttribute()
		{
			_ID = 0;
			ItemID = "";
			
			//Attibute
			_Attribute = Attribute.STR; // Contrary to the field, we only restore Health and Energy
			Value = 0;
		}
	}
	
	[HideInInspector]
	public List<RestoredAttribute> RestoredAttributes;
	public List<Status> CuredStatuses;
	
	public ConsumableHealing()
	{
		RestoredAttributes = new List<RestoredAttribute>();
		CuredStatuses = new List<Status>();
	}
	
	public override string ToString()
	{
		string restoredAttributes = "\nRestoredAttributes : ";
		foreach (var resAtt in RestoredAttributes)
		{
			restoredAttributes += "\nID : " + resAtt._ID + "\n- Attribute : " + (Attribute)resAtt._Attribute + "\n- Value : " + resAtt.Value;
		}
		return base.ToString() + restoredAttributes;
	}
}

[System.Serializable]
public class ConsumableMagic : Consumable
{
	public TargetFormation TargetFormation;
	
	public Buff Buff;
	
	public ConsumableMagic()
	{
		TargetFormation = TargetFormation.Ally;
		
		Buff = new Buff ();
	}
}

[System.Serializable]
public class ConsumableAttribute : Consumable
{
	[System.Serializable]
	public class RaisedAttribute
	{
		public int _ID;
		public string ItemID;
		
		//Attibute
		public Attribute _Attribute;
		public int _Value;
		
		public RaisedAttribute()
		{
			_ID = 0;
			ItemID = "";
			_Attribute = Attribute.STR;
			_Value = 0;
		}
	}
	
	public List<RaisedAttribute> RaisedAttributes;
	
	public ConsumableAttribute()
	{
		RaisedAttributes = new List<RaisedAttribute>();
		ItemType = Item.Type.Consumable;
		ConsumableType = Consumable.ConsumeType.Attributes;
	}
	
	public ConsumableAttribute(ConsumableAttribute item)
	{
		Clone (item);
	}
	
	public void Clone(ConsumableAttribute item)
	{
		base.Clone (item);
		ConsumableType = item.ConsumableType;
		Target = item.Target;
		
	}
	
	#if UNITY_EDITOR
	public override void OnGUI()
	{
		base.OnGUI ();
		Target = (TargetType)EditorGUILayout.EnumPopup ("Target: ", Target);
	}
	#endif
	
}

[System.Serializable]
public class ConsumableMisc : Consumable
{
	[System.Serializable]
	public class MiscEffect
	{
		public int _ID;
		public string ItemID;
		
		//Attibute
		public MiscAttribute Attribute;
		public int Value;

        public MiscEffect()
        {
            _ID = 0;
            ItemID = "";
            Attribute = MiscAttribute.InventoryWeight;
            Value = 0;
        }
    }
	
	[HideInInspector]
	public List<MiscEffect> MiscEffects;

    public ConsumableMisc()
    {
        MiscEffects = new List<MiscEffect>();
    }
}