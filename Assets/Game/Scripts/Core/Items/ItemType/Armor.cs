﻿using System.Collections;
using System.Collections.Generic;
using Legrand.core;
//namespace Legrand.core
//{
	public class Armor 
	{
		private Dictionary<Attribute , float> _ArmorBuff;
		public Dictionary<Attribute , float> ArmorBuff
		{
			get { return _ArmorBuff;}
		}
		private ArmorType _Type;
		public ArmorType Type
		{
			get { return _Type;}
		}

		public Armor(ArmorType armorType)
		{
			_Type = armorType;
			_ArmorBuff = new Dictionary<Attribute, float> ();
			bool found = false;
			for (int i = 0; i < LegrandBackend.Instance.LegrandDatabase.ArmorCalculationData.Count && !found; i++)
			{
				ArmorCalculationModel dbArmor = LegrandBackend.Instance.LegrandDatabase.ArmorCalculationData.Get(i);
				if(dbArmor.ArmorType == armorType)
				{
					foreach(Buff buff in dbArmor.ArmorBuff)
					{
						if(buff.Type == BuffType.BASIC_PRIMARY)
						{
							foreach(AttributeValue attribute in buff.BuffPrimary)
							{
								if(!_ArmorBuff.ContainsKey(attribute.AttributeType)) _ArmorBuff.Add(attribute.AttributeType, (int)attribute.value);
								else _ArmorBuff[attribute.AttributeType] = (int)_ArmorBuff[attribute.AttributeType] + (int)attribute.value;
							}
						}
						else if(buff.Type == BuffType.BASIC_SECONDARY)
						{
							foreach(AttributeValue attribute in buff.BuffSecondary)
							{
								if(!_ArmorBuff.ContainsKey(attribute.AttributeType)) _ArmorBuff.Add(attribute.AttributeType, (float)attribute.value);
								else _ArmorBuff[attribute.AttributeType] = (int)_ArmorBuff[attribute.AttributeType] + (float)attribute.value;
							}
						}
					}
					found = true;
				}
			}
			//_Type = armorType;
			//_ArmorBuff = new Dictionary<Attribute, float> ();
			//bool found = false;
			/*for (int i = 0; i < LegrandBackend.Instance.iBox.BattlerArmorCalculations.Count && !found; i++)
			{
				ArmorCalculationModel dbArmor = LegrandBackend.Instance.iBox.BattlerArmorCalculations[i];
				if(dbArmor.ArmorType == armorType)
				{
					foreach(Buff buff in dbArmor.ArmorBuff)
					{
						if(buff.Type == BuffType.BASIC_PRIMARY)
						{
							foreach(AttributeBuff attribute in buff.BuffPrimary)
							{
								if(!_ArmorBuff.ContainsKey(attribute.AttributeType)) _ArmorBuff.Add(attribute.AttributeType, (int)attribute.value);
								else _ArmorBuff[attribute.AttributeType] = (int)_ArmorBuff[attribute.AttributeType] + (int)attribute.value;
							}
						}
						else if(buff.Type == BuffType.BASIC_SECONDARY)
						{
							foreach(AttributeBuff attribute in buff.BuffSecondary)
							{
								if(!_ArmorBuff.ContainsKey(attribute.AttributeType)) _ArmorBuff.Add(attribute.AttributeType, (float)attribute.value);
								else _ArmorBuff[attribute.AttributeType] = (int)_ArmorBuff[attribute.AttributeType] + (float)attribute.value;
							}
						}
					}
					found = true;
				}
			}*/
		}

		public enum ArmorType
		{
			Light, Medium , Heavy
		};

	}
//}