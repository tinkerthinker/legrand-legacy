﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;

[System.Serializable]
public class Key : Item 
{
	public List<KeyEffect> ItemEffect;
	public Key()
	{
        ItemType = Type.Key;
		ItemEffect = new List<KeyEffect> ();
	}

	[System.Serializable]
	public class KeyEffect
	{
		//Attibute
		public MiscAttribute _Attribute;
		public string _Value;

		public KeyEffect()
		{
			_Attribute = MiscAttribute.Blueprint;
			_Value = string.Empty;
		}
	}
}