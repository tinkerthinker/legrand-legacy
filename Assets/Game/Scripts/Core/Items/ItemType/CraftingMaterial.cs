﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;

[System.Serializable]
public class CraftingMaterial : Item {
	//Dummy crafting material for data effect list on that material when used for crafting, please fix it if anything wrong.
	[System.Serializable]
	public class EffectMaterial
	{
		//Attibute
		public Attribute _Attribute;
		public int Value;

		public EffectMaterial()
		{
			_Attribute = Attribute.STR;
			Value = 0;
		}
	}

	public List<EffectMaterial> EffectsCrafting;

	public CraftingMaterial()
	{
		EffectsCrafting = new List<EffectMaterial>();
		_ID = "LO-";
		ItemType = Item.Type.CraftingMaterial;
		Usability = UsabilityType.Combinable;
	}
}
