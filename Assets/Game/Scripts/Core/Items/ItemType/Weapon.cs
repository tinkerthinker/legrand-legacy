﻿using System.Collections;
using System.Collections.Generic;
using Legrand.core;

//namespace Legrand.core
//{
	public class Weapon
	{
		private Dictionary<Attribute , float> _WeaponBuff;
		public Dictionary<Attribute , float> WeaponBuff
		{
			get{ return _WeaponBuff;}
		}

		private DamageType _Type;
		public DamageType Type
		{
			get { return _Type;}
		}

		private DamageType _StrongerTo;
		public DamageType StrongerTo
		{
			get { return _StrongerTo;}
		}

		private DamageType _WeakerTo;
		public DamageType WeakerTo
		{
			get { return _WeakerTo;}
		}

		public Weapon(DamageType weaponType)
		{
			_Type = weaponType;
			int weaponTypeValue = (int)weaponType;
            
            for(int w = 0;w < LegrandBackend.Instance.LegrandDatabase.WeaponCircleData.Count;w++)
            {
                if (LegrandBackend.Instance.LegrandDatabase.WeaponCircleData.Get(w).CurrentWeapon.Equals(weaponType))
                {
                    _StrongerTo = LegrandBackend.Instance.LegrandDatabase.WeaponCircleData.Get(w).StrongerTo;
                    _WeakerTo = LegrandBackend.Instance.LegrandDatabase.WeaponCircleData.Get(w).WeakerTo;
                }
            }
            
			/*if ((int)weaponType + 1 == System.Enum.GetValues (typeof(DamageType)).Length) _StrongerTo = (DamageType) 0;
			else _StrongerTo = (DamageType) ((int)weaponType + 1);

			if ((int)weaponType - 1 < 0) _WeakerTo = (DamageType) (System.Enum.GetValues (typeof(DamageType)).Length-1);
			else _WeakerTo = (DamageType) ((int)weaponType - 1);*/

			_WeaponBuff = new Dictionary<Attribute, float> ();
			
		bool found = false;
		for (int i = 0; i < LegrandBackend.Instance.LegrandDatabase.WeaponCalculationData.Count && !found; i++)
		{
			WeaponCalculationModel dbWeapon = LegrandBackend.Instance.LegrandDatabase.WeaponCalculationData.Get(i);
			if(dbWeapon.DamageType == weaponType)
			{
				foreach(Buff buff in dbWeapon.WeaponBuff)
				{
					if(buff.Type == BuffType.BASIC_PRIMARY)
					{
						foreach(AttributeValue attribute in buff.BuffPrimary)
						{
							if(!_WeaponBuff.ContainsKey(attribute.AttributeType)) _WeaponBuff.Add(attribute.AttributeType, (int)attribute.value);
							else _WeaponBuff[attribute.AttributeType] = (int)_WeaponBuff[attribute.AttributeType] + (int)attribute.value;
						}
					}
					else if(buff.Type == BuffType.BASIC_SECONDARY)
					{
						foreach(AttributeValue attribute in buff.BuffSecondary)
						{
							if(!_WeaponBuff.ContainsKey(attribute.AttributeType)) _WeaponBuff.Add(attribute.AttributeType, (float)attribute.value);
							else _WeaponBuff[attribute.AttributeType] = (int)_WeaponBuff[attribute.AttributeType] + (float)attribute.value;
						}
					}
				}
				found = true;
			}
		}
			//bool found = false;
			/*for (int i = 0; i < LegrandBackend.Instance.iBox.BattlerWeaponCalculations.Count && !found; i++) {
				WeaponCalculationModel dbWeapon = LegrandBackend.Instance.iBox.BattlerWeaponCalculations[i];
				if(dbWeapon.DamageType == weaponType)
				{
					foreach(Buff buff in dbWeapon.WeaponBuff)
					{
						if(buff.Type == BuffType.BASIC_PRIMARY)
						{
							foreach(AttributeBuff attribute in buff.BuffPrimary)
							{
								if(!_WeaponBuff.ContainsKey(attribute.AttributeType)) _WeaponBuff.Add(attribute.AttributeType, (int)attribute.value);
								else _WeaponBuff[attribute.AttributeType] = (int)_WeaponBuff[attribute.AttributeType] + (int)attribute.value;
							}
						}
						else if(buff.Type == BuffType.BASIC_SECONDARY)
						{
							foreach(AttributeBuff attribute in buff.BuffSecondary)
							{
								if(!_WeaponBuff.ContainsKey(attribute.AttributeType)) _WeaponBuff.Add(attribute.AttributeType, (float)attribute.value);
								else _WeaponBuff[attribute.AttributeType] = (int)_WeaponBuff[attribute.AttributeType] + (float)attribute.value;
							}
						}
					}
					found = true;
				}
			}*/
		}

	    public enum DamageType
	    {
	        Impact, Pierce, Slash
	    };
		public enum AttackType
		{
			Melee, Range
		};
	}
//}
