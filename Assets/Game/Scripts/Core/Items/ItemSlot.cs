﻿using System.Collections;
using System.Collections.Generic;
using Legrand.core;
using UnityEngine;

// Item that is binded to a character
[System.Serializable]
public class ItemSlot 
{
    public int ItemSlotID;
    
    public Item ItemMaster;
    public int Stack; // How many in a stack, if it's stackable
    
    public ItemSlot(Item item)
    {
        ItemMaster = item;
        Stack = 1;
    }

	public ItemSlot()
	{
		ItemMaster = null;
		Stack = 0;
	}
}
