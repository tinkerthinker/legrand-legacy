﻿using UnityEngine;
using System.Collections;
using Legrand.core;

public class Loot {

    public Item ItemLoot;
    public int Amount;
    private float _Chance;
    public float Chance
    {
        get { return _Chance; }
    }

    public Loot(Item item, int amount)
    {
        float chance = 0f;
        switch (item.RareType)
        {
            case Item.Rarity.Common: chance = LegrandBackend.Instance.LegrandDatabase.lootChanceData.Get(0)._CommonChance; break;
            case Item.Rarity.Uncommon: chance = LegrandBackend.Instance.LegrandDatabase.lootChanceData.Get(0)._UnCommonChance; break;
            case Item.Rarity.Rare: chance = LegrandBackend.Instance.LegrandDatabase.lootChanceData.Get(0)._RareChance; break;
            case Item.Rarity.VeryRare: chance = LegrandBackend.Instance.LegrandDatabase.lootChanceData.Get(0)._VeryRareChance; break;
            case Item.Rarity.SuperRare: chance = LegrandBackend.Instance.LegrandDatabase.lootChanceData.Get(0)._SuperRareChance; break;
            case Item.Rarity.BossOnly: chance = LegrandBackend.Instance.LegrandDatabase.lootChanceData.Get(0)._BossOnlyChance; break;
        }
        SetLoot(item, amount, chance);
    }

    public Loot(Item item, int amount, float chance)
    {
        SetLoot(item,amount,chance);
    }

    private void SetLoot(Item item, int amount, float chance)
    {
        ItemLoot = item;
        Amount = amount;
        _Chance = chance;
    }


    public bool TryToGet()
	{
		return LegrandUtility.Random(0, 100) < _Chance;
	}

    public bool TryToGet(float bonus)
    {
        return LegrandUtility.Random(0, 100) - bonus < _Chance;
    }
}
