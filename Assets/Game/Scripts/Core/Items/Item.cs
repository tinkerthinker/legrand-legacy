﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
[System.Serializable]
public class Item 
{
    public enum Rarity
    {
        Common,
        Uncommon,
        Rare,
        VeryRare,
        SuperRare,
		BossOnly,
    };
    
    public enum Type
    {
        Weapon,
        Armor,
        Consumable,
        CraftingMaterial,
        Key
    };

	public enum UsabilityType
	{
		Normal,
		WeaponCraftable,
		ArmorCraftable,
		Combinable,
		AllCraftable
	}
    
    	public string _ID;
    
    	public Type ItemType;
	public UsabilityType Usability;
   	 public string Name = "";
	public Sprite Icon;
    	public string Description = "";
    	public int BuyPrice = 0;
    	public int SellPrice = 0;
    	public int Weight = 0;
	public Rarity RareType;

	public Item()
	{
		_ID = "";
		
		ItemType = Type.Consumable;
		Usability = UsabilityType.Normal;
		Name = "";
		Icon = new Sprite();
		Description = "";
		BuyPrice = 0;
		SellPrice = 0;
		Weight = 0;
		RareType = Rarity.Common;
	}

	public Item(Item item)
	{
		Clone (item);
	}
	
	public void Clone(Item item)
	{
		_ID = item._ID;
		ItemType = item.ItemType;
		Name = item.Name;
		Icon = item.Icon;
		Description = item.Description;
		BuyPrice = item.BuyPrice;
		SellPrice = item.SellPrice;
		Weight = item.Weight;
		Description = item.Description;
	}

	#if UNITY_EDITOR
	public virtual void OnGUI()
	{
		_ID = EditorGUILayout.TextField ("ID: ", _ID);
		Name = EditorGUILayout.TextField ("Name: ", Name);
		Icon = EditorGUILayout.ObjectField ("Icon", Icon, typeof(Sprite), false) as Sprite;
		Description = EditorGUILayout.TextField("Description: ", Description);
		BuyPrice = EditorGUILayout.IntField ("Buy Price: ", BuyPrice);
		SellPrice = EditorGUILayout.IntField ("Sell Price: ", SellPrice);
		Weight = EditorGUILayout.IntField ("Weight: ", Weight);
	}
	#endif
}
