﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Legrand.core
{
    [System.Serializable]
    public class BlueprintRequirement
    {
        public string AllItemID;
        public int Amount;

        public BlueprintRequirement()
        {
            AllItemID = "";
            Amount = 0;
        }
    }
	[System.Serializable]
	public class BluePrint
	{
		public enum Type
		{
			Crafting,
			Alchemy
		};

		public string ID;
        public string CharacterID;
		public string Description;
		public int Level = 1;
		public int Price;

		public string ResultItemID;
		public Type BluePrintType;
        public List<BlueprintRequirement> RequiredItems;
		//[Header("X For ID, Y For Amount")]
		//public List<VectorStringInteger> RequiredItems;

		public bool IsLocked = true;

		public BluePrint()
		{
			ID = "";
            CharacterID = "";
            Description = "";
			Level = 1;
			Price = 0;
			
			ResultItemID = "";
			BluePrintType = Type.Crafting;

            RequiredItems = new List<BlueprintRequirement>();
			//RequiredItems = new List<VectorStringInteger>();
			
			IsLocked = true;
		}

		public bool TryCombine()
		// Try and error if combination could be success or not
		{
			bool failed = false;
			// Iterate every requirement
			for (int index = 0; index<RequiredItems.Count && !failed; index++) 
			{
                BlueprintRequirement RequiredItem = RequiredItems[index];
				bool found = false;
				// See Material Inventory First
				int typeInteger = (int)Item.Type.CraftingMaterial;
				for(int j = 0; j< PartyManager.Instance.Inventory.Items[typeInteger].Count && !found; j++)
				{
					ItemSlot itemSlot = PartyManager.Instance.Inventory.Items[typeInteger][j];
					// Found it, continue to next requirement
					if(RequiredItem.AllItemID == itemSlot.ItemMaster._ID + "" 
					   && 
					   RequiredItem.Amount <= itemSlot.Stack
					   ) found = true;
				}

				if(found) continue;

				// If not found in material then maybe it's in consumeable
				typeInteger = (int)Item.Type.Consumable;
				for(int j = 0; j< PartyManager.Instance.Inventory.Items[typeInteger].Count && !found; j++)
				{
					ItemSlot itemSlot = PartyManager.Instance.Inventory.Items[typeInteger][j];
					// Found it, continue to next requirement
					if(RequiredItem.AllItemID == itemSlot.ItemMaster._ID + "" 
					   && 
					   RequiredItem.Amount <= itemSlot.Stack
					   ) found = true;
				}

				if(found) continue;
				// Can't find it, it means failed to combine. Quit Iteration and return false
				failed = true;
			}

			return !failed;
		}
	}
}