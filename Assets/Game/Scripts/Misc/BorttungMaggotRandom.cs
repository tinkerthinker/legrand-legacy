﻿using UnityEngine;
using System.Collections;

public class BorttungMaggotRandom : MonoBehaviour
{

	void Start ()
	{
		Animator objAnim;
		Vector3 objScale;

		objAnim = GetComponent<Animator> ();
		objAnim.Play ("Cre_Borttung_Maggot_Bat_Stance", -1, LegrandUtility.Random(0.0f, 1.0f));
		objAnim.speed = LegrandUtility.Random(0.3f, 0.8f);
		objScale = transform.localScale;
		transform.localScale = new Vector3 (objScale.x, LegrandUtility.Random(0.3f, 0.7f), objScale.z);
		transform.Rotate (0, LegrandUtility.Random(0, 180), 0);
	}

	void Update ()
	{
	
	}
}
