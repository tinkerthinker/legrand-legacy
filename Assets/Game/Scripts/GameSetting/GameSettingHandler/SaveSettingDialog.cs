﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Legrand.GameSettings
{
    public class SaveSettingDialog : MonoBehaviour
    {
        public GameObject Panel;
        public Button NoButton;
        public Button ApplySettingButton;
        public int CountDownStart;
        int CountDownTime;
        public Text TimeText;
        public GameSettingHandler SettingHandler;
        
        IEnumerator SelectButton(Button btn)
        {
            yield return null;
            btn.Select();
        }

        public void StartDialog()
        {
            CountDownTime = CountDownStart;
            Panel.SetActive(true);
            StartCoroutine(SelectButton(NoButton));
            InvokeRepeating("SetText", 0f, 1f);
        }

        void StopCountDown()
        {
            CancelInvoke("SetText");
        }

        void CloseDialog()
        {
            Panel.SetActive(false);
            ApplySettingButton.Select();
        }

        public void YesButtonAction()
        {
            StopCountDown();
            SettingHandler.SaveAll();
            CloseDialog();
        }

        public void NoButtonAction()
        {
            StopCountDown();
            SettingHandler.LoadSetting();
            SettingHandler.ApplySetting();
            SettingHandler.componentHandler.SetNotMakeChange();
            CloseDialog();
        }

        void SetText()
        {
            TimeText.text = CountDownTime.ToString();
            CountDownTime--;
            if (CountDownTime <= -1)
            {
                StopCountDown();
                NoButtonAction();
            }
        }
    }
}