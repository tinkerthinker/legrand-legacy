﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Legrand.GameSettings
{
    public class GameSettingHandler : MonoBehaviour
    {
        public static string[] ControllerKey = new string[]
            {
                "LegrandGameplayControllerKey",
                "LegrandBattleControllerKey",
                "LegrandWarControllerKey"
            };
        public static GameSettingHandler Instance;
        public OptionComponentHandler componentHandler;
        public bool SaveDirectionToInputManager;
        public SettingControllerHandler[] settingControllerHandler;
        public GameObject GameSettingObj;
        public VideoSettingHandler videoHandler;
        public SaveSettingDialog SaveSettingDialog;
        public GameObject SaveChangesSuccessfull;
        public Button SaveChangesSuccessfullButton;
        public bool SettingActive = false;
        bool AnyBindActive = false;
        Action OnExit;
        public int[] LoadControllerIndex;

        void Awake()
        {
            Instance = this;
            Application.targetFrameRate = 60;
            videoHandler.SetQualityOption();
            videoHandler.SetResolutionOption();
            componentHandler.LoadData();
            LoadSetting();
            videoHandler.ApplySetting();
        }
        void Start()
        {
            GameSettingObj.SetActive(false);
        }
        public void Activate(Action OnExit)
        {
            this.OnExit = OnExit;
            SettingActive = true;
            GameplayInputManager.Instance.AddChangeControllerListener();
            SetOptionInput.Instance.AddChangeControllerListener();
            GameSettingObj.SetActive(true);
            LoadSetting();
        }
        public bool CheckAnyBind()
        {
            foreach (SettingControllerHandler handler in settingControllerHandler)
            {
                if (handler.UseThis)
                {
                    foreach (ButtonBindInput bind in handler.GamepadButtonBind)
                    {
                        if (bind.setButton)
                            return true;
                    }
                    foreach (ButtonBindInput bind in handler.keyboardButtonBind)
                    {
                        if (bind.setButton)
                            return true;
                    }
                }
            }
            return false;
        }
        public void Exit()
        {
            AnyBindActive = CheckAnyBind();
            if (SettingActive)
            {
                if (!AnyBindActive)
                {
                    LoadGameplay();
                    LoadSound();
                    LoadControl();
                    Close();
                }
            }
        }
        #region main button
        public void Close()
        {
            SettingActive = false;
            GameSettingObj.SetActive(false);
            WwiseManager.Instance.PlaySFX(WwiseManager.UIFX.Cancel);
            OnExit();
        }
        public void ApplyButton()
        {
            if (componentHandler.IsVideoMakeChange())
                ApplyForTemp();
            else
                ApplyAll();
        }
        void ApplyForTemp()
        {
            LoadDataFromUI();
            ApplySetting();
            SaveControl(false);
            SaveSettingDialog.StartDialog();
            WwiseManager.Instance.PlaySFX(WwiseManager.UIFX.Confirm);
        }
        public void SaveAll()
        {
            SaveGameplay();
            SaveControl(true);
            componentHandler.SetNotMakeChange();
        }
        void ApplyAll()
        {
            LoadDataFromUI();
            SaveGameplay();
            SaveControl(true);
            ApplySetting();
            componentHandler.SetNotMakeChange();
            WwiseManager.Instance.PlaySFX(WwiseManager.UIFX.Confirm);

            SaveChangesSuccessfull.SetActive(true);
            StartCoroutine(SelectButton(SaveChangesSuccessfullButton));
        }
        IEnumerator SelectButton(Button btn)
        {
            yield return null;
            btn.Select();
        }
        public void ResetAll()
        {
            Resetter();
            componentHandler.SetNotMakeChange();
            WwiseManager.Instance.PlaySFX(WwiseManager.UIFX.Cancel);
        }
        #endregion
        #region save
        bool SaveGameplay()
        {
            if(LegrandBackend.Instance.SaveSetting("Setting", "GameSetting.gd",GameSetting.GetStringData()))
            {
                return true;
            }
            return false;
        }
        bool SaveControl(bool saveToFile)
        {
            foreach (SettingControllerHandler sch in settingControllerHandler)
            {
                if (sch.UseThis)
                {
                    sch.SaveControllerToClass();
                }
            }
            bool result = false;
            ControllerSaveClass ControllerData = new ControllerSaveClass();
            ControllerData.Controller = GameSetting.Controller;
            if(LegrandBackend.Instance.SaveController("Setting", "ControllerSetting.gd", ControllerData))
            {
                result = true;
            }
            GameplayInputManager.Instance.SetControllerData();
            return result;
        }
        #endregion
        #region reset
        void Resetter()
        {
            GameSetting.DefaultSetting();
            SetUIFromData();

            GameSetting.DefaultController();
            resetControllerView();
        }
        #endregion
        #region load
        void LoadDataFromUI()
        {
            GameSetting.RandomActPosition = componentHandler.getValueBool(componentHandler.Keys[0]);
            GameSetting.OneButtonAct = componentHandler.getValueBool(componentHandler.Keys[1]);
            GameSetting.FullScreen = componentHandler.getValueBool(componentHandler.Keys[2]);
            GameSetting.VideoResolution = componentHandler.getValueInt(componentHandler.Keys[3]);
            GameSetting.VideoQuality = componentHandler.getValueInt(componentHandler.Keys[4]);
            GameSetting.SetVolume(0, componentHandler.getValueFloat(componentHandler.Keys[5]));
            GameSetting.SetVolume(1, componentHandler.getValueFloat(componentHandler.Keys[6]));
            GameSetting.SetVolume(2, componentHandler.getValueFloat(componentHandler.Keys[7]));
        }
        public void LoadSetting()
        {
            LoadGameplay();
            SetUIFromData();
            LoadControl();
            Invoke("LoadSound", 1f);
        }

        void LoadSound()
        {
            GameSetting.SetVolume(0, GameSetting.Volume[0]);
            GameSetting.SetVolume(1, GameSetting.Volume[1]);
            GameSetting.SetVolume(2, GameSetting.Volume[2]);
        }

        void LoadGameplay()
        {
            string SettingData = LegrandBackend.Instance.LoadSetting("Setting", "GameSetting.gd");
            if (SettingData.Equals(""))
                GameSetting.DefaultSetting();
            else
            {
                if(!GameSetting.SetData(SettingData))
                {
                    Debug.Log("default setting save gameplay");
                    SaveGameplay();
                }
            }
        }
        void LoadControl()
        {
            GameSetting.DefaultController();
            ControllerSaveClass ControllerData = LegrandBackend.Instance.LoadController("Setting", "ControllerSetting.gd");
            if (ControllerData == null)
                GameSetting.DefaultController();
            else
            {
                int bound1 = GameSetting.Controller.GetUpperBound(1);

                foreach (int firstIndex in LoadControllerIndex)
                {
                    for (int secondIndex = 0; secondIndex <= bound1; secondIndex++)
                    {
                        GameSetting.Controller[firstIndex, secondIndex] = ControllerData.Controller[firstIndex, secondIndex];
                    }
                }
            }
            resetControllerView();
        }
        void resetControllerView()
        {
            foreach (SettingControllerHandler sch in settingControllerHandler)
            {
                if (sch.UseThis)
                {
                    sch.SetDataAndKey();
                    sch.RefreshControllerView();
                    if (sch.SaveToInputManager)
                        sch.SetInputManager();
                }
            }
        }
        #endregion
        #region setOptionComponentView
        void SetUIFromData()
        {
            componentHandler.GetIComponent(componentHandler.Keys[0]).SetValue(GameSetting.RandomActPosition);
            componentHandler.GetIComponent(componentHandler.Keys[1]).SetValue(GameSetting.OneButtonAct);
            componentHandler.GetIComponent(componentHandler.Keys[2]).SetValue(GameSetting.FullScreen);
            componentHandler.GetIComponent(componentHandler.Keys[3]).SetValue(GameSetting.VideoResolution);
            componentHandler.GetIComponent(componentHandler.Keys[4]).SetValue(GameSetting.VideoQuality);
            componentHandler.GetIComponent(componentHandler.Keys[5]).SetValue(GameSetting.Volume[0]);
            componentHandler.GetIComponent(componentHandler.Keys[6]).SetValue(GameSetting.Volume[1]);
            componentHandler.GetIComponent(componentHandler.Keys[7]).SetValue(GameSetting.Volume[2]);
        }
        #endregion
        #region applySetting
        //yg di apply setting selain controller baru setting untuk video
        public void ApplySetting()
        {
            videoHandler.ApplySetting();
        }
        #endregion
    }
}