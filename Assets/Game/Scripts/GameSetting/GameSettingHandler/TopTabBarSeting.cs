﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TeamUtility.IO;
using UnityEngine.EventSystems;

namespace Legrand.GameSettings
{
    public class TopTabBarSeting : MonoBehaviour
    {
        int CurrentTab;
        [System.Serializable]
        public class Select
        {
            public Selectable[] things;
            public GameObject[] PanelParent;
            public GameObject[] BottomObject;
        }
        public Select[] SelectedThing;
        public GameObject[] TabObject;
        public Dropdown[] AllDropdown;
        public Button[] BottomButton;
        
        void OnEnable()
        {
            if (EventManager.Instance)
                EventManager.Instance.AddListener<ChangeController>(SetMode);
            SetActiveTab();
        }
        void OnDisable()
        {
            if (EventManager.Instance)
                EventManager.Instance.RemoveListener<ChangeController>(SetMode);
        }
        void SetMode(ChangeController e)
        {
            if (gameObject.activeInHierarchy)
                SetActiveTab();
        }
        IEnumerator WaitForSelect(Selectable thing)
        {
            if (EventSystem.current != null)
            {
                GameObject onSelect = EventSystem.current.currentSelectedGameObject;
                yield return null;
                if ((thing != null && thing.IsActive()))
                {
                    if (!isBottomButtonSelected(onSelect))
                        thing.Select();
                    StartCoroutine(SetBottomButtonNavigation());
                }
            }
            yield return null;
        }
        IEnumerator SetBottomButtonNavigation()
        {
            yield return null;
            for (int ii = 0; ii < BottomButton.Length; ii++)
            {
                Navigation navi = BottomButton[ii].navigation;
                for (int i = 0; i < SelectedThing[CurrentTab].BottomObject.Length; i++)
                {
                    if (SelectedThing[CurrentTab].BottomObject[i].activeInHierarchy)
                    {
                        navi.selectOnUp = SelectedThing[CurrentTab].BottomObject[i].GetComponent<Selectable>();
                    }
                }
                BottomButton[ii].navigation = navi;
            }
        }
        void Update()
        {
            if (InputManager.anyKeyDown)
            {
                if (LegrandUtility.GetChangeTab() == 1 && CurrentTab < TabObject.Length - 1)
                {
                    if (!GameSettingHandler.Instance.CheckAnyBind())
                    {
                        SetSelectable();
                        CurrentTab++;
                        SetActiveTab();
                    }
                }
                if (LegrandUtility.GetChangeTab() == 0 && CurrentTab > 0)
                {
                    if (!GameSettingHandler.Instance.CheckAnyBind())
                    {
                        SetSelectable();
                        CurrentTab--;
                        SetActiveTab();
                    }
                }
            }
        }
        bool isBottomButtonSelected(GameObject obj)
        {
            for (int ii = 0; ii < BottomButton.Length; ii++)
            {
                if (obj == BottomButton[ii].gameObject)
                    return true;
            }
            return false;
        }
        bool isLegal(GameObject obj)
        {
            for(int i=0;i<SelectedThing[CurrentTab].PanelParent.Length;i++)
            {
                if(SelectedThing[CurrentTab].PanelParent[i].activeSelf && obj == SelectedThing[CurrentTab].PanelParent[i])
                {
                    return true;
                }
            }
            return false;
        }
        void SetSelectable()
        {
            GameObject onSelect = EventSystem.current.currentSelectedGameObject;
            if (onSelect && (onSelect.transform.parent && (isLegal(onSelect.transform.parent.gameObject))))
            {
                for (int i = 0; i < SelectedThing[CurrentTab].things.Length; i++)
                {
                    if (SelectedThing[CurrentTab].things[i].IsActive())
                    {
                        SelectedThing[CurrentTab].things[i] = onSelect.GetComponent<Selectable>();
                    }
                }
            }
        }
        void SetActiveTab()
        {
            foreach (Dropdown drop in AllDropdown)
            {
                for (int i = 0; i < drop.gameObject.transform.childCount; i++)
                {
                    if (drop.gameObject.transform.GetChild(i).name.Equals("Dropdown List"))
                    {
                        GameObject.Destroy(drop.gameObject.transform.GetChild(i).gameObject);
                    }
                }
            }
            for (int i = 0; i < TabObject.Length; i++)
            {
                if (CurrentTab != i)
                {
                    TabObject[i].SetActive(false);
                }
                else
                {
                    TabObject[i].SetActive(true);
                    foreach (Selectable thing in SelectedThing[i].things)
                    {
                        if(thing.gameObject.activeInHierarchy)
                            StartCoroutine(WaitForSelect(thing));
                    }
                }
            }
        }
    }
}