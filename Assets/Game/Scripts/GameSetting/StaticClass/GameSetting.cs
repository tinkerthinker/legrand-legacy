﻿using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class ControllerSaveClass
{
    public string[,] Controller = new string[6, 14];

    public ControllerSaveClass()
    {
        Controller = new string[6, 14];
    }
}

public static class GameSetting
{
    #region Video
    public static bool FullScreen;
    public static int VideoQuality;
    public static int VideoResolution;
    #endregion
    #region Gameplay
    public static bool RandomActPosition;
    public static bool OneButtonAct;
    #endregion
    #region Controller
    public static string[,] Controller = new string[6,14];//0 gameplay keyboard 1 gameplay gamepad 2 battle keyboard 3 battle gamepad 4 war keyboard 5 war joystick
    #endregion
    #region Volume
    public static float[] Volume = new float[3];//0 = BGM, 1 = SFX, 2 = Ambience
    #endregion
    #region language
    public static Language Language;
    public static Dictionary<string,string> GetLanguage()
    {
        Dictionary<string,string> language = new Dictionary<string, string>();
        for(int i = 0; i < LegrandBackend.Instance.LegrandDatabase.LanguageDatabase.Count;i++)
        {
            foreach(Languages lang in LegrandBackend.Instance.LegrandDatabase.LanguageDatabase.Get(i).languages)
            {
                if(lang.Language == GameSetting.Language)
                {
                    if (language.ContainsKey(LegrandBackend.Instance.LegrandDatabase.LanguageDatabase.Get(i).ID)) Debug.LogError("Sudah ada id dialog "+ LegrandBackend.Instance.LegrandDatabase.LanguageDatabase.Get(i).ID);
                    language[LegrandBackend.Instance.LegrandDatabase.LanguageDatabase.Get(i).ID] = lang.Data;
                    break;
                }
            }
        }
        return language;
    }
    #endregion

    #region save load
    public static string GetStringData()
    {
        return RandomActPosition.ToString() + "_" + OneButtonAct.ToString() + "_" + FullScreen.ToString() + "_" + VideoQuality.ToString() + "_" + VideoResolution.ToString() + "_" + Volume[0].ToString() + "_" + Volume[1].ToString() + "_" + Volume[2].ToString();
    }

    public static bool SetData(string Data)
    {
        string[] data = Data.Split('_');

        if (data.Length == 8)
        {
            if (data[0].Equals("True"))
                RandomActPosition = true;
            else
                RandomActPosition = false;

            if (data[1].Equals("True"))
                OneButtonAct = true;
            else
                OneButtonAct = false;

            if (data[2].Equals("True"))
                FullScreen = true;
            else
                FullScreen = false;
            
            VideoQuality = int.Parse(data[3]);
            VideoResolution = int.Parse(data[4]);
            Volume[0] = float.Parse(data[5]);
            Volume[1] = float.Parse(data[6]);
            Volume[2] = float.Parse(data[7]);

            return true;
        }

        DefaultSetting();

        return false;
    }
    #endregion

    public static void DefaultSetting()
    {
        FullScreen = true;
        VideoQuality = 1;
        VideoResolution = 0;

        RandomActPosition = true;

        OneButtonAct = false;
        
        SetVolume(0, 100f);
        SetVolume(1, 100f);
        SetVolume(2, 100f);
        
        Language = Language.ENGLISH;
    }

    public static void SetVolume(int type, float value)
    {
        Volume[type] = value;
        WwiseManager.Instance.SetSoundVolume((WwiseManager.SoundType)type, 100f);
    }

    public static void DefaultController()
    {
        #region world keyboard
		Controller[0, 0] = KeyCode.Return.ToString();
        Controller[0, 1] = KeyCode.Backspace.ToString();
        Controller[0, 2] = KeyCode.W.ToString() + "?1?0";
        Controller[0, 3] = KeyCode.S.ToString() + "?0?0";
        Controller[0, 4] = KeyCode.A.ToString() + "?0?0";
        Controller[0, 5] = KeyCode.D.ToString() + "?1?0";
        Controller[0, 6] = KeyCode.Space.ToString();

		Controller[0, 7] = KeyCode.E.ToString();
        Controller[0, 8] = KeyCode.Tab.ToString();
        Controller[0, 9] = KeyCode.UpArrow.ToString() + "?1?0";
        Controller[0, 10] = KeyCode.DownArrow.ToString() + "?0?0";
        Controller[0, 11] = KeyCode.LeftArrow.ToString() + "?0?0";
        Controller[0, 12] = KeyCode.RightArrow.ToString() + "?1?0";
        Controller[0, 13] = KeyCode.Z.ToString();
        #endregion
        #region world gamepad
        Controller[1, 0] = KeyCode.JoystickButton0.ToString();
        Controller[1, 1] = KeyCode.JoystickButton1.ToString();
        Controller[1, 2] = "joy_0_axis_1?0?0";
        Controller[1, 3] = "joy_0_axis_1?1?0";
        Controller[1, 4] = "joy_0_axis_0?0?0";
        Controller[1, 5] = "joy_0_axis_0?1?0";
        Controller[1, 6] = KeyCode.JoystickButton3.ToString();
        #endregion
        #region battle keyboard
        Controller[2, 0] = KeyCode.W.ToString() + "?1?0";
        Controller[2, 1] = KeyCode.S.ToString() + "?0?0";
        Controller[2, 2] = KeyCode.A.ToString() + "?0?0";
        Controller[2, 3] = KeyCode.D.ToString() + "?1?0";

        Controller[2, 4] = KeyCode.UpArrow.ToString() + "?1?0";
        Controller[2, 5] = KeyCode.DownArrow.ToString() + "?0?0";
        Controller[2, 6] = KeyCode.LeftArrow.ToString() + "?0?0";
        Controller[2, 7] = KeyCode.RightArrow.ToString() + "?1?0";
        #endregion
        #region battle gamepad
        Controller[3, 0] = KeyCode.Joystick1Button3.ToString();
        Controller[3, 1] = KeyCode.Joystick1Button0.ToString();
        Controller[3, 2] = KeyCode.Joystick1Button2.ToString();
        Controller[3, 3] = KeyCode.Joystick1Button1.ToString();

        Controller[3, 4] = "joy_0_axis_1?0?0";
        Controller[3, 5] = "joy_0_axis_1?1?0";
        Controller[3, 6] = "joy_0_axis_0?0?0";
        Controller[3, 7] = "joy_0_axis_0?1?0";
        #endregion
    }
    
}
