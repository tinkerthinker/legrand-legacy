﻿using UnityEngine;
using UnityEngine.UI;
using TeamUtility.IO;
using UnityEngine.EventSystems;
using System;

namespace Legrand.GameSettings
{
    public class SliderOptionComponent : MonoBehaviour, IUpdateSelectedHandler, IOptionComponent, ISelectHandler
    {
        public string SliderID;
        public float Speed;
        public Slider slider;
        public string Info;
        public Text InfoText;
        public bool VideoSetting;
        bool MadeChange;
        public bool isVolumeSetting;
        public WwiseManager.SoundType SoundType;
        public WwiseManager.UIFX SoundCursorMoving = WwiseManager.UIFX.Cursor_Moving;
        public WwiseManager.UIFX SoundFail = WwiseManager.UIFX.Cursor_Moving;

        #region IOptionComponent implementation
        public string ID
        {
            get
            {
                return SliderID;
            }
        }

        public object Value
        {
            get
            {
                return slider.value;
            }
        }

        public bool IsVideoSetting
        {
            get
            {
                return VideoSetting;
            }
        }

        public bool IsMadeChange
        {
            set
            {
                MadeChange = value;
            }
            get
            {
                return MadeChange;
            }
        }

        public void SetValue(object value)
        {
            slider.value = (float)value;
        }
        #endregion

        void SetVolume()
        {
            GameSetting.SetVolume((int)SoundType, slider.value);
        }

        #region IUpdateSelectedHandler implementation
        void IUpdateSelectedHandler.OnUpdateSelected(BaseEventData eventData)
        {
            TeamUtility.IO.StandaloneInputModule module = EventSystem.current.GetComponent<TeamUtility.IO.StandaloneInputModule>();
            bool allow = module.allowMoveEvent;

            if (LegrandUtility.GetAxisRawHorizontal() > 0.1f && allow)
            {
                if(slider.value < slider.maxValue)
                    WwiseManager.Instance.PlaySFX(SoundCursorMoving);
                //else
                //    WwiseManager.Instance.PlaySFX(SoundFail);
                slider.value += Speed * Time.deltaTime;
                MadeChange = true;
                if (isVolumeSetting)
                    SetVolume();
            }
            else if (LegrandUtility.GetAxisRawHorizontal() < -0.1f && allow)
            {
                if (slider.value > slider.minValue)
                    WwiseManager.Instance.PlaySFX(SoundCursorMoving);
                //else
                //    WwiseManager.Instance.PlaySFX(SoundFail);
                slider.value -= Speed * Time.deltaTime;
                MadeChange = true;
                if (isVolumeSetting)
                    SetVolume();
            }
        }

        public void SetDetailInfo()
        {
            InfoText.text = Info;
        }

        public void OnSelect(BaseEventData eventData)
        {
            SetDetailInfo();
        }
        #endregion
    }
}