﻿

namespace Legrand.GameSettings
{
    public interface IOptionComponent
    {
        string ID{ get; }
        bool IsVideoSetting{ get; }
        bool IsMadeChange { get; set; }
        void SetValue(object value);
        object Value{ get; }
        void SetDetailInfo();
    }
}