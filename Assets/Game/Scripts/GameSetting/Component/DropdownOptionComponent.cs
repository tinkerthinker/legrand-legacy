﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System;

namespace Legrand.GameSettings
{
    public class DropdownOptionComponent : MonoBehaviour,IOptionComponent, ISelectHandler
    {
        public string DropdownID;
        public Dropdown dropdown;
        public string Info;
        public Text InfoText;
        public bool VideoSetting;
        bool MadeChange;
        public int lastValue = 0;
        public Button ThisButton;
        Color NormalColor;
        
        public void OnClick()
        {
            lastValue = dropdown.value;
            dropdown.Show();
            SetOption();
            SetSelectedColor();
        }

        void SetOption()
        {
            if(dropdown.transform.childCount > 2)
            {
                Transform child = dropdown.transform.GetChild(3);
                if(child.childCount > 0)
                {
                    Toggle[] options = child.GetChild(0).gameObject.GetComponentsInChildren<Toggle>();
                    for(int i=0;i<options.Length;i++)
                    {
                        options[i].gameObject.AddComponent<DropdownOptionSound>();
                    }
                }
            }
        }

        void SetSelectedColor()
        {
            ColorBlock colorBlock = ThisButton.colors;
            NormalColor = colorBlock.normalColor;
            colorBlock.normalColor = colorBlock.highlightedColor;
            ThisButton.colors = colorBlock;
        }

        public void ResetColor()
        {
            ColorBlock colorBlock = ThisButton.colors;
            colorBlock.normalColor = NormalColor;
            ThisButton.colors = colorBlock;
        }

        #region IOptionComponent implementation
        public string ID
        {
            get
            {
                return DropdownID;
            }
        }

        public object Value
        {
            get
            {
                return dropdown.value;
            }
        }

        public bool IsVideoSetting
        {
            get
            {
                return VideoSetting;
            }
        }

        public bool IsMadeChange
        {
            set
            {
                MadeChange = value;
            }
            get
            {
                return MadeChange;
            }
        }

        public void SetValue(object value)
        {
            dropdown.value = (int)value;
        }

        public void SetDetailInfo()
        {
            InfoText.text = Info;
        }

        public void OnSelect(BaseEventData eventData)
        {
            SetDetailInfo();
        }
        #endregion
    }
}