﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

namespace Legrand.GameSettings
{
    public class ToggleOptionComponent : MonoBehaviour, IOptionComponent, ISelectHandler
    {
        public string ToggleID;
        public Toggle toggle;
        public string Info;
        public Text InfoText;
        public bool VideoSetting;
        bool MadeChange;
        
        public void OnClick()
        {
            toggle.isOn = !toggle.isOn;
            MadeChange = true;
        }
        #region IOptionComponent implementation
        public void SetValue(object value)
        {
            toggle.isOn = (bool)value;
        }

        public void SetDetailInfo()
        {
            InfoText.text = Info;
        }

        public void OnSelect(BaseEventData eventData)
        {
            SetDetailInfo();
        }

        public string ID
        {
            get
            {
                return ToggleID;
            }
        }
        public object Value
        {
            get
            {
                return toggle.isOn;
            }
        }

        public bool IsVideoSetting
        {
            get
            {
                return VideoSetting;
            }
        }

        public bool IsMadeChange
        {
            set
            {
                MadeChange = value;
            }
            get
            {
                return MadeChange;
            }
        }
        #endregion

    }
}