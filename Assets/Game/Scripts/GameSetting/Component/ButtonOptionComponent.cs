﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TeamUtility.IO;
using System;

namespace Legrand.GameSettings
{
    public class ButtonOptionComponent : MonoBehaviour, IUpdateSelectedHandler, IOptionComponent, ISelectHandler
    {
        public string ButtonOptionID;
        int Selected;
        public GameObject[] option;
        public bool invert;
        public bool SpriteSwap;
        public Color NormalColor;
        public Color SelectedColor;
        public Sprite NormalSprite;
        public Sprite SelectedSprite;
        public string Info;
        public Text InfoText;
        public bool VideoSetting;
        bool MadeChange;
        public WwiseManager.UIFX SoundCursorMoving = WwiseManager.UIFX.Cursor_Moving;
        public WwiseManager.UIFX SoundFail = WwiseManager.UIFX.Cursor_Moving;

        void OnEnable()
        {
            SetValue(Selected);
        }
        void SetValue(int selected)
        {
            for (int i = 0; i < option.Length; i++)
            {
                if (i != selected)
                {
                    if (SpriteSwap)
                        option[i].GetComponent<Image>().sprite = NormalSprite;
                    else
                        option[i].GetComponent<Image>().color = NormalColor;
                }
                else
                {
                    if (SpriteSwap)
                        option[i].GetComponent<Image>().sprite = SelectedSprite;
                    else
                        option[i].GetComponent<Image>().color = SelectedColor;
                }
            }
        }
        #region IOptionComponent implementation
        public string ID
        {
            get
            {
                return ButtonOptionID;
            }
        }
        public object Value
        {
            get
            {
                if (invert)
                    return option.Length - Selected - 1;
                return Selected;
            }
        }

        public bool IsVideoSetting
        {
            get
            {
                return VideoSetting;
            }
        }

        public bool IsMadeChange
        {
            set
            {
                MadeChange = value;
            }
            get
            {
                return MadeChange;
            }
        }

        public void SetValue(object value)
        {
            Selected = (int)value;
            if (invert)
                Selected = option.Length - Selected - 1;
            SetValue(Selected);
        }
        #endregion

        #region IUpdateSelectedHandler implementation
        void IUpdateSelectedHandler.OnUpdateSelected(BaseEventData eventData)
        {
            TeamUtility.IO.StandaloneInputModule module = EventSystem.current.GetComponent<TeamUtility.IO.StandaloneInputModule>();
            bool allow = module.allowMoveEvent;

            if (LegrandUtility.GetAxisRawHorizontal() > 0.1f && allow)
            {
                if (Selected < option.Length - 1)
                {
                    Selected++;
                    WwiseManager.Instance.PlaySFX(SoundCursorMoving);
                    SetValue(Selected);
                    MadeChange = true;
                }
                //else
                //    WwiseManager.Instance.PlaySFX(SoundFail);
            }
            else if (LegrandUtility.GetAxisRawHorizontal() < -0.1f && allow)
            {
                if (Selected > 0)
                {
                    Selected--;
                    WwiseManager.Instance.PlaySFX(SoundCursorMoving);
                    SetValue(Selected);
                    MadeChange = true;
                }
                //else
                //    WwiseManager.Instance.PlaySFX(SoundFail);
            }
        }

        public void SetDetailInfo()
        {
            InfoText.text = Info;
        }

        public void OnSelect(BaseEventData eventData)
        {
            SetDetailInfo();
        }
        #endregion
    }
}