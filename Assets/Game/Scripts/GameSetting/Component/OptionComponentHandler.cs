﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Legrand.GameSettings
{
    public class OptionComponentHandler : MonoBehaviour
    {
        public GameObject[] OptionComponent;
        Dictionary<string,IOptionComponent> _OptionComponentDictionary = new Dictionary<string, IOptionComponent>();
        public List<string> Keys = new List<string>();

        public bool IsVideoMakeChange()
        {
            foreach (KeyValuePair<string,IOptionComponent> opt in _OptionComponentDictionary)
            {
                if (opt.Value.IsMadeChange && opt.Value.IsVideoSetting)
                    return true;
            }
            return false;
        }
        public void SetNotMakeChange()
        {
            foreach(string key in Keys)
            {
                _OptionComponentDictionary[key].IsMadeChange = false;
            }
        }
        public void LoadData()
        {
            _OptionComponentDictionary.Clear();
            Keys.Clear();
            foreach (GameObject go in OptionComponent)
            {
                IOptionComponent temp = go.GetComponent<IOptionComponent>();
                _OptionComponentDictionary.Add(temp.ID, temp);
                Keys.Add(temp.ID);
            }
            SetNotMakeChange();
        }
        public IOptionComponent GetIComponent(string key)
        {
            return _OptionComponentDictionary[key];
        }
        public bool getValueBool(string key)
        {
            return (bool)_OptionComponentDictionary[key].Value;
        }
        public int getValueInt(string key)
        {
            return (int)_OptionComponentDictionary[key].Value;
        }
        public float getValueFloat(string key)
        {
            return (float)_OptionComponentDictionary[key].Value;
        }
    }
}