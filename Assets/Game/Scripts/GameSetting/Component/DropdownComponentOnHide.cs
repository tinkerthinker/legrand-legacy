﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Legrand.GameSettings
{
    public class DropdownComponentOnHide : MonoBehaviour, IUpdateSelectedHandler
    {
        public Button buttonNavigator;
        public DropdownOptionComponent dropdownOptionComponent;
        public Dropdown dropdown;

        #region IUpdateSelectedHandler implementation
        public void OnUpdateSelected(BaseEventData eventData)
        {
            GameObject blocker = GameObject.Find("Blocker");
            if (!blocker)
            {
                buttonNavigator.Select();
                dropdownOptionComponent.ResetColor();
                if (dropdownOptionComponent.lastValue != dropdown.value)
                    dropdownOptionComponent.IsMadeChange = true;
            }
        }

        #endregion
        
    }
}