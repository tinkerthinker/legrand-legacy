﻿using UnityEngine;
using UnityEngine.EventSystems;

public class DropdownOptionSound : MonoBehaviour, ISelectHandler
{
    public WwiseManager.UIFX SoundCursorMoving = WwiseManager.UIFX.Cursor_Moving;
    public void OnSelect(BaseEventData eventData)
    {
        WwiseManager.Instance.PlaySFX(SoundCursorMoving);
    }
}
