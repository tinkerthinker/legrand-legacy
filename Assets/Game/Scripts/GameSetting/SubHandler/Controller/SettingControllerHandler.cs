﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TeamUtility.IO;

namespace Legrand.GameSettings
{
    public enum InputMode{KeyboardAndMouse,Gamepad};
    public class SettingControllerHandler : MonoBehaviour
    {
        public bool UseThis;
        public bool SaveToInputManager;
        int firstIndex = 0;
        public ControllerFor ActiveController;
        public InputMode inputMode;
        public GameObject GamepadConfig;
        public GameObject KeyboardConfig;

        public ButtonBindInput[] keyboardButtonBind;
        public ButtonBindInput[] GamepadButtonBind;

        string keyboardKey = "";
        string gamepadKey = "";

        public void SetDataAndKey()
        {
            firstIndex = (int)ActiveController * 2;
            keyboardKey = GameSettingHandler.ControllerKey[(int)ActiveController] + InputMode.KeyboardAndMouse.ToString();
            gamepadKey = GameSettingHandler.ControllerKey[(int)ActiveController] + InputMode.Gamepad.ToString();
        }

        public void RemoveSameControlKeyboard(BindInfo bindInfo)
        {
            foreach (ButtonBindInput buttonbind in keyboardButtonBind)
            {
                if (!buttonbind.bindInfo.Equals(bindInfo) && buttonbind.bindInfo.keyCode == bindInfo.keyCode)
                {
                    buttonbind.bindInfo.keyCode = KeyCode.None;
                    buttonbind.RefreshView();
                }
            }
        }
        public void RemoveSameControlGamepad(BindInfo bindInfo)
        {
            foreach (ButtonBindInput buttonbind in GamepadButtonBind)
            {
                if (!buttonbind.bindInfo.Equals(bindInfo))
                {
                    if (buttonbind.bindInfo.keyCode == bindInfo.keyCode && bindInfo.keyCode != KeyCode.None)
                    {
                        buttonbind.bindInfo.keyCode = KeyCode.None;
                        buttonbind.RefreshView();
                    }
                    if (buttonbind.bindInfo.isAxis &&(buttonbind.bindInfo.AxisName.Equals(bindInfo.AxisName) && buttonbind.bindInfo.isPositive.Equals(bindInfo.isPositive)))
                    {
                        buttonbind.bindInfo.AxisName = "";
                        buttonbind.RefreshView();
                        SetOppositeAxis soa = buttonbind.gameObject.GetComponent<SetOppositeAxis>();
                        if (soa != null && soa.Other.bindInfo != bindInfo)
                        {
                            soa.RemoveOther();
                            soa.Other.RefreshView();
                        }
                    }
                }
            }
        }
        public void ChangeInputDevice()
        {
            if (inputMode == InputMode.Gamepad)
            {
                GamepadConfig.SetActive(true);
                KeyboardConfig.SetActive(false);
            }
            else
            {
                GamepadConfig.SetActive(false);
                KeyboardConfig.SetActive(true);
            }
        }
        public void SaveControllerToClass()
        {
            for (int i = 0; i < keyboardButtonBind.Length; i++)
            {
                if (keyboardButtonBind[i].bindInfo.isAxis)
                {
                    string value1 = "";
                    if (keyboardButtonBind[i].bindInfo.isPositive)
                        value1 = "1";
                    else
                        value1 = "0";
                    string value2 = "";
                    if (keyboardButtonBind[i].bindInfo.invertAxis)
                        value2 = "1";
                    else
                        value2 = "0";
                    SaveToClass(firstIndex,i,keyboardButtonBind[i].bindInfo.keyCode.ToString() + "?" + value1 + "?" + value2);
                }
                else
                    SaveToClass(firstIndex,i,keyboardButtonBind[i].bindInfo.keyCode.ToString());
            }
            for (int i = 0; i < GamepadButtonBind.Length; i++)
            {
                if (GamepadButtonBind[i].bindInfo.isAxis)
                {
                    string value1 = "";
                    if (GamepadButtonBind[i].bindInfo.isPositive)
                        value1 = "1";
                    else
                        value1 = "0";
                    string value2 = "";
                    if (GamepadButtonBind[i].bindInfo.invertAxis)
                        value2 = "1";
                    else
                        value2 = "0";
                    SaveToClass(firstIndex + 1,i,GamepadButtonBind[i].bindInfo.AxisName.ToString() + "?" + value1 + "?" + value2);
                }
                else
                    SaveToClass(firstIndex + 1,i,GamepadButtonBind[i].bindInfo.keyCode.ToString());
            }
            if (SaveToInputManager)
            {
                SetInputManager();
            }
        }
        public void SetInputManager()
        {
            InputConfiguration GMPinput = InputManager.GetInputConfiguration("Gamepad");
            foreach (var inp in GamepadButtonBind)
            {
                SetInputManager(inp.bindInfo,ref GMPinput);
            }
            InputConfiguration KYBinput = InputManager.GetInputConfiguration("KeyboardAndMouse");
            foreach (var inp in keyboardButtonBind)
            {
                SetInputManager(inp.bindInfo,ref KYBinput);
            }
        }
        void SetInputManager(string axesname,bool positive,BindInfo info,ref InputConfiguration input)
        {
            for (int i = 0;i < input.axes.Count; i++)
            {
                if (input.axes[i].name.Equals(axesname))
                {
                    if (!info.isAxis || info.inputMode == InputMode.KeyboardAndMouse)
                    {
                        if (info.isAxis && info.inputMode == InputMode.KeyboardAndMouse)
                            input.axes[i].type = InputType.DigitalAxis;
                        else if (info.inputMode == InputMode.Gamepad && info.isAxis)
                            input.axes[i].type = InputType.AnalogAxis;
                        else
                            input.axes[i].type = InputType.Button;
                        
                        if (info.MainButton)
                        {
                            if (positive)
                            {
                                input.axes[i].positive = info.keyCode;
                            }
                            else
                            {
                                input.axes[i].negative = info.keyCode;
                            }
                        }
                        else
                        {
                            if (positive)
                            {
                                input.axes[i].altPositive = info.keyCode;
                            }
                            else
                            {
                                input.axes[i].altNegative = info.keyCode;
                            }
                        }
                    }
                    else
                    {
                        input.axes[i].type = InputType.AnalogAxis;

                        if (info.AxisName.Equals("joy_0_axis_0"))
                            input.axes[i].axis = 0;
                        else if (info.AxisName.Equals("joy_0_axis_1"))
                            input.axes[i].axis = 1;
                        else if (info.AxisName.Equals("joy_0_axis_2"))
                            input.axes[i].axis = 2;
                        else if (info.AxisName.Equals("joy_0_axis_3"))
                            input.axes[i].axis = 3;
                        else if (info.AxisName.Equals("joy_0_axis_4"))
                            input.axes[i].axis = 4;
                        else if (info.AxisName.Equals("joy_0_axis_5"))
                            input.axes[i].axis = 5;
                        else if (info.AxisName.Equals("joy_0_axis_6"))
                            input.axes[i].axis = 6;
                        else if (info.AxisName.Equals("joy_0_axis_7"))
                            input.axes[i].axis = 7;
                        else if (info.AxisName.Equals("joy_0_axis_8"))
                            input.axes[i].axis = 8;
                        else if (info.AxisName.Equals("joy_0_axis_9"))
                            input.axes[i].axis = 9;

                        input.axes[i].invert = false;

                        if (info.AxisName.Equals(GameplayInputManager.invertThisAxis) && (info.controlType == ControlType.Forward || info.controlType == ControlType.Backward))
                            input.axes[i].invert = false;
                        if((info.controlType == ControlType.Forward || info.controlType == ControlType.Backward) && input.axes[i].name.Equals("Vertical"))
                            input.axes[i].invert = true;
                        if((info.controlType == ControlType.Forward && info.isPositive) && input.axes[i].name.Equals("Vertical"))
                            input.axes[i].invert = false;
                        if((info.controlType == ControlType.Backward && !info.isPositive) && input.axes[i].name.Equals("Vertical"))
                            input.axes[i].invert = false;
                        if((info.controlType == ControlType.Left && info.isPositive) && input.axes[i].name.Equals("Horizontal"))
                            input.axes[i].invert = true;
                        if((info.controlType == ControlType.Right && !info.isPositive) && input.axes[i].name.Equals("Horizontal"))
                            input.axes[i].invert = true;

                        if ((!axesname.Equals("Menu") && axesname.Contains("Menu")) && (!info.AxisName.Equals(GameplayInputManager.invertThisAxis) && input.axes[i].name.Contains("Vertical")))
                        {
                            if (!info.isPositive && info.controlType == ControlType.Forward)
                                input.axes[i].invert = true;
                            if (info.isPositive && info.controlType == ControlType.Backward)
                                input.axes[i].invert = true;
                        }
                        else if ((!axesname.Equals("Menu") && axesname.Contains("Menu")) && input.axes[i].name.Contains("Horizontal"))
                        {
                            if (input.axes[i].axis == 6)
                            {
                                if(info.controlType == ControlType.Left && info.isPositive)
                                    input.axes[i].invert = true;
                                if(info.controlType == ControlType.Right && !info.isPositive)
                                    input.axes[i].invert = true;
                            }
                        }
                    }
                }
            }
        }
        void SetInputManager(BindInfo info,ref InputConfiguration input)
        {
            if (info.controlType == ControlType.Confirm)
            {
                SetInputManager("Confirm",true, info,ref input);
            }
            else if (info.controlType == ControlType.Cancel)
            {
                SetInputManager("Cancel",true, info,ref input);
            }
            else if (info.controlType == ControlType.Menu)
            {
                SetInputManager("Menu",true, info,ref input);
            }
            if (GameSettingHandler.Instance.SaveDirectionToInputManager)
            {
                if (info.controlType == ControlType.Forward)
                {
                    SetInputManager("Vertical", true, info, ref input);
                }
                else if (info.controlType == ControlType.Backward)
                {
                    SetInputManager("Vertical", false, info, ref input);
                }
                else if (info.controlType == ControlType.Left)
                {
                    SetInputManager("Horizontal", false, info, ref input);
                }
                else if (info.controlType == ControlType.Right)
                {
                    SetInputManager("Horizontal", true, info, ref input);
                }
            }
        }
        public void RefreshControllerView()
        {
            for (int i = 0; i < keyboardButtonBind.Length; i++)
            {
                if (!string.IsNullOrEmpty(GameSetting.Controller[firstIndex, i]))
                {
                    if (keyboardButtonBind[i].bindInfo.isAxis)
                    {
                        if (GameSetting.Controller[firstIndex, i].Contains("?") && !GameSetting.Controller[firstIndex, i].Equals(""))
                            keyboardButtonBind[i].bindInfo.keyCode = (KeyCode)System.Enum.Parse(typeof(KeyCode), GameSetting.Controller[firstIndex, i].Split('?')[0]);
                        else
                            keyboardButtonBind[i].bindInfo.keyCode = KeyCode.None;
                        if (GameSetting.Controller[firstIndex, i].Contains("?"))
                        {
                            if (GameSetting.Controller[firstIndex, i].Split('?')[1].Equals("1"))
                                keyboardButtonBind[i].bindInfo.isPositive = true;
                            else
                                keyboardButtonBind[i].bindInfo.isPositive = false;
                            if (GameSetting.Controller[firstIndex, i].Split('?')[1].Equals("2"))
                                keyboardButtonBind[i].bindInfo.invertAxis = true;
                            else
                                keyboardButtonBind[i].bindInfo.invertAxis = false;
                        }
                    }
                    else
                    {
                        keyboardButtonBind[i].bindInfo.keyCode = (KeyCode)System.Enum.Parse(typeof(KeyCode), GameSetting.Controller[firstIndex, i]);
                    }
                    keyboardButtonBind[i].RefreshView();
                }
            }
            for (int i = 0; i < GamepadButtonBind.Length; i++)
            {
                if (!string.IsNullOrEmpty(GameSetting.Controller[firstIndex, i]))
                {
                    if (GameSetting.Controller[firstIndex + 1, i].Split('?')[0].Contains("joy_0_axis"))
                    {
                        GamepadButtonBind[i].bindInfo.AxisName = GameSetting.Controller[firstIndex + 1, i].Split('?')[0];
                        if (GameSetting.Controller[firstIndex + 1, i].Split('?')[1].Equals("1"))
                            GamepadButtonBind[i].bindInfo.isPositive = true;
                        else
                            GamepadButtonBind[i].bindInfo.isPositive = false;
                        if (GameSetting.Controller[firstIndex + 1, i].Split('?')[2].Equals("1"))
                            GamepadButtonBind[i].bindInfo.invertAxis = true;
                        else
                            GamepadButtonBind[i].bindInfo.invertAxis = false;
                        GamepadButtonBind[i].bindInfo.isAxis = true;
                    }
                    else
                    {
                        GamepadButtonBind[i].bindInfo.keyCode = (KeyCode)System.Enum.Parse(typeof(KeyCode), GameSetting.Controller[firstIndex + 1, i]);
                        GamepadButtonBind[i].bindInfo.isAxis = false;
                    }
                    GamepadButtonBind[i].RefreshView();
                }
            }
        }
        void SaveToClass(int index1,int index2,string value)
        {
            GameSetting.Controller[index1, index2] = value;
        }
    }
}
