﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;
using TeamUtility.IO;

namespace Legrand.GameSettings
{
    public class SetOptionInput : MonoBehaviour
    {
        public SettingControllerHandler[] settingController;
        public Button ForSelect;
        public static SetOptionInput Instance;
        void Awake()
        {
            Instance = this;
        }
        public void AddChangeControllerListener()
        {
            if (EventManager.Instance && !EventManager.Instance.HasListener< ChangeController>(SetMode))
                EventManager.Instance.AddListener<ChangeController>(SetMode);
            FirstSet();
        }
        void FirstSet()
        {
            if (InputManager.PlayerOneConfiguration.name == "Gamepad")
            {
                EventManager.Instance.TriggerEvent (new ChangeController(ControllerType.XboxController));
            }
            else if (InputManager.PlayerOneConfiguration.name == "KeyboardAndMouse")
            {
                EventManager.Instance.TriggerEvent (new ChangeController(ControllerType.Keyboard));
            }
        }
        void SetMode(ChangeController e)
        {
            InputMode im;
            if (e.CtrlType == ControllerType.XboxController)
            {
                im = InputMode.Gamepad;
            }
            else
            {
                im = InputMode.KeyboardAndMouse;
            }
            foreach (SettingControllerHandler sch in settingController)
            {
                sch.inputMode = im;
                sch.ChangeInputDevice();
            }
            if (GameSettingHandler.Instance != null && (GameSettingHandler.Instance.SettingActive && ForSelect != null))
            {
                StartCoroutine(LegrandUtility.WaitToSelectObject(ForSelect.gameObject, 1));
            }
        }
    }
}
