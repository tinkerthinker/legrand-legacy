﻿using UnityEngine;
using System.Collections.Generic;
using TeamUtility.IO;

public class GameSettingIconButton : MonoBehaviour
{
    public List<KeySpriteHandler> KeySpriteHandlers;

    void OnEnable()
    {
        if(EventManager.Instance)
            EventManager.Instance.AddListener<ChangeController>(ChangeInputType);

        if (InputManager.PlayerOneConfiguration.name == "KeyboardAndMouse")
            ChangeToKeyboard();
        else if (InputManager.PlayerOneConfiguration.name == "Gamepad")
            ChangeToController();
    }

    void OnDisable()
    {
        if (EventManager.Instance != null)
            EventManager.Instance.RemoveListener<ChangeController>(ChangeInputType);
    }

    void ChangeInputType(ChangeController e)
    {
        if (e.CtrlType == ControllerType.Keyboard)
            ChangeToKeyboard();
        else if (e.CtrlType == ControllerType.XboxController)
            ChangeToController();
    }

    void ChangeToKeyboard()
    {
        for (int i = 0; i < KeySpriteHandlers.Count; i++)
            KeySpriteHandlers[i].ChangeToKeyBoard();
    }

    void ChangeToController()
    {
        for (int i = 0; i < KeySpriteHandlers.Count; i++)
            KeySpriteHandlers[i].ChangeToController();
    }
}
