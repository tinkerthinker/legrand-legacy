﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

namespace Legrand.GameSettings
{
    public enum ControllerFor
    {
        Gameplay,
        Battle,
        War
    };
    public class ButtonBindInput : MonoBehaviour, ISelectHandler
    {
        [System.Serializable]
        public class AxisData
        {
            public string AxisName;
            public float AxisValue;
        }
        public string Info;
        public Text InfoText;
        public SettingControllerHandler controllerHandler;
        public InputMode inputType;
        public BindInfo bindInfo;
        public AxisData[] AllAxisData;
        public bool setButton = false;
        public bool FixedOption = false;

        string KeyCodeTranslator(KeyCode code)
        {
            return LegrandBackend.Instance.GetKeyName(code.ToString());
        }
        string AxisTranslator(string axisName, bool isPositive, bool invert)
        {
            if (!axisName.Equals(""))
            {
                bool result = isPositive;
                result = invert ? !result : result;
                result = GameplayInputManager.invertThisAxis.Equals(axisName) ? !result : result;
                string name = axisName.Split('_')[3];
                if (name == "1" || name == "4" || name == "6")
                    result = !result;
                string axisCode = "Axis " + name + " " + ((result) ? "+" : "-");
                return LegrandBackend.Instance.GetKeyName(axisCode);
            }
            return "None";
        }
        public void RefreshView()
        {
            if (bindInfo.AxisName.Equals("") && inputType == InputMode.Gamepad)
            {
                bindInfo.isAxis = false;
            }
            if (bindInfo.AxisName.Equals(""))
                gameObject.GetComponentInChildren<Text>().text = KeyCodeTranslator(bindInfo.keyCode);
            else
                gameObject.GetComponentInChildren<Text>().text = AxisTranslator(bindInfo.AxisName,bindInfo.isPositive,bindInfo.invertAxis);
        }
        KeyCode GetKeyCode()
        {
            foreach(KeyCode kcode in Enum.GetValues(typeof(KeyCode)))
            {
                if (Input.GetKeyDown(kcode))
                {
                    if(bindInfo.inputMode == InputMode.Gamepad && kcode.ToString().Contains("Joystick"))
                        return kcode;
                    else if (bindInfo.inputMode == InputMode.KeyboardAndMouse)
                        return kcode;
                }
            }
            return KeyCode.None;
        }
        void DisableButton()
        {
            Button butt = GetComponent<Button>();
            Navigation temp = butt.navigation;
            temp.mode = Navigation.Mode.None;
            butt.navigation = temp;
        }
        void ActivateButton()
        {
            Button butt = GetComponent<Button>();
            Navigation temps = butt.navigation;
            temps.mode = Navigation.Mode.Automatic;
            butt.navigation = temps;
        }
        public void OnPress()
        {
            if (!setButton)
            {
                setButton = true;
                DisableButton();
                StartCoroutine(StartInputScanDelayed());
            }
        }
        private IEnumerator StartInputScanDelayed()
        {
            gameObject.GetComponent<Button>().image.color = Color.gray;
            yield return null;
            while (setButton)
            {
                if (AllAxisData.Length > 0)
                {
                    int index = -1;
                    float val = float.NegativeInfinity;
                    for (int i = 0; i < AllAxisData.Length; i++)
                    {
                        float value = Input.GetAxis(AllAxisData[i].AxisName);
                        if (Math.Abs(value) > GameplayInputManager.Instance.AnalogDeadZone && Math.Abs(value) > val)
                        {
                            AllAxisData[i].AxisValue = value;
                            val = value;
                            index = i;
                        }
                    }
                    if (index != -1)
                    {
                        bindInfo.AxisValue = AllAxisData[index].AxisValue;
                        bindInfo.AxisName = AllAxisData[index].AxisName;
                        if (!bindInfo.invertAxis)
                        {
                            if (bindInfo.AxisValue >= 0)
                                bindInfo.isPositive = true;
                            else
                                bindInfo.isPositive = false;
                        }
                        else
                        {
                            if (bindInfo.AxisValue >= 0)
                                bindInfo.isPositive = false;
                            else
                                bindInfo.isPositive = true;
                        }
                        gameObject.GetComponentInChildren<Text>().text = AxisTranslator(bindInfo.AxisName,bindInfo.isPositive,bindInfo.invertAxis);
                        bindInfo.isAxis = true;
                        setButton = false;
                    }
                    if (index == -1 && !FixedOption)
                    {
                        KeyCode temp = GetKeyCode();
                        if (temp != KeyCode.None)
                        {
                            bindInfo.keyCode = temp;
                            gameObject.GetComponentInChildren<Text>().text = KeyCodeTranslator(bindInfo.keyCode);
                            bindInfo.AxisName = "";
                            bindInfo.isAxis = false;
                            setButton = false;
                        }
                    }
                    foreach (AxisData axis in AllAxisData)
                    {
                        axis.AxisValue = 0f;
                    }
                }
                else
                {
                    KeyCode temp = GetKeyCode();
                    if (temp != KeyCode.None)
                    {
                        bindInfo.keyCode = temp;
                        gameObject.GetComponentInChildren<Text>().text = KeyCodeTranslator(bindInfo.keyCode);
                        bindInfo.AxisName = "";
                        setButton = false;
                    }
                }
                yield return null;
            }
            gameObject.GetComponent<Button>().image.color = Color.white;
            if (inputType == InputMode.Gamepad)
            {
                controllerHandler.RemoveSameControlGamepad(bindInfo);
                SetOppositeAxis setOther = gameObject.GetComponent<SetOppositeAxis>();
                if (setOther)
                    setOther.SetOther();
            }
            else
                controllerHandler.RemoveSameControlKeyboard(bindInfo);
            ActivateButton();
        }

        public void SetDetailInfo()
        {
            InfoText.text = Info;
        }

        public void OnSelect(BaseEventData eventData)
        {
            SetDetailInfo();
        }
    }
}