﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Legrand.GameSettings
{
    public class LoadController : MonoBehaviour
    {
        int firstIndex = 0;

        ControllerFor controllerFor;

        string keyboardKey = "";
        string gamepadKey = "";

        [System.Serializable]
        public class PresetConfigurationButton
        {
            public BindInfo[] keyboard;
            public BindInfo[] gamepad;
        }

        public PresetConfigurationButton[] PresetData = new PresetConfigurationButton[3];

        public void init(ControllerFor controller)
        {
            controllerFor = controller;
            firstIndex = (int)controllerFor * 2;
            keyboardKey = GameSettingHandler.ControllerKey[(int)controllerFor] + InputMode.KeyboardAndMouse.ToString();
            gamepadKey = GameSettingHandler.ControllerKey[(int)controllerFor] + InputMode.Gamepad.ToString();
            GameSetting.DefaultController();
        }
        public BindInfo[] GetKeyboardAndMouse()
        {
            ProcessDataKeyboard();
            return PresetData[(int)controllerFor].keyboard;
        }
        public BindInfo[] GetGamepad()
        {
            ProcessDataGamepad();
            return PresetData[(int)controllerFor].gamepad;
        }
        void ProcessDataKeyboard()
        {
            for (int i = 0; i < PresetData[(int)controllerFor].keyboard.Length; i++)
            {
                if (PresetData[(int)controllerFor].keyboard[i].isAxis)
                {
                    if (GameSetting.Controller[firstIndex, i].Split('?')[1].Equals("1"))
                        PresetData[(int)controllerFor].keyboard[i].isPositive = true;
                    else
                        PresetData[(int)controllerFor].keyboard[i].isPositive = false;
                    if (GameSetting.Controller[firstIndex, i].Split('?')[2].Equals("1"))
                        PresetData[(int)controllerFor].keyboard[i].invertAxis = true;
                    else
                        PresetData[(int)controllerFor].keyboard[i].invertAxis = false;
                    PresetData[(int)controllerFor].keyboard[i].keyCode = (KeyCode)System.Enum.Parse (typeof(KeyCode), GameSetting.Controller[firstIndex, i].Split('?')[0]);
                }
                else
                    PresetData[(int)controllerFor].keyboard[i].keyCode = (KeyCode)System.Enum.Parse (typeof(KeyCode), GameSetting.Controller[firstIndex, i]);
            }
        }
        void ProcessDataGamepad()
        {
            for (int i = 0; i < PresetData[(int)controllerFor].gamepad.Length; i++)
            {
                if (GameSetting.Controller[firstIndex + 1, i].Contains("joy_0_axis"))
                {
                    PresetData[(int)controllerFor].gamepad[i].AxisName = GameSetting.Controller[firstIndex + 1, i].Split('?')[0];
                    if (GameSetting.Controller[firstIndex + 1, i].Split('?')[1].Equals("1"))
                        PresetData[(int)controllerFor].gamepad[i].isPositive = true;
                    else
                        PresetData[(int)controllerFor].gamepad[i].isPositive = false;
                    if (GameSetting.Controller[firstIndex + 1, i].Split('?')[2].Equals("1"))
                        PresetData[(int)controllerFor].gamepad[i].invertAxis = true;
                    else
                        PresetData[(int)controllerFor].gamepad[i].invertAxis = false;
                    PresetData[(int)controllerFor].gamepad[i].isAxis = true;
                    PresetData[(int)controllerFor].gamepad[i].keyCode = KeyCode.None;
                }
                else
                {
                    PresetData[(int)controllerFor].gamepad[i].keyCode = (KeyCode)System.Enum.Parse(typeof(KeyCode), GameSetting.Controller[firstIndex + 1, i]);
                    PresetData[(int)controllerFor].gamepad[i].isAxis = false;
                    PresetData[(int)controllerFor].gamepad[i].AxisName = "";
                }
            }
        }
    }
}
