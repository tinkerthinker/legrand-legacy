﻿using UnityEngine;
using System.Collections;

namespace Legrand.GameSettings
{
    public class SetOppositeAxis : MonoBehaviour
    {
        public ButtonBindInput Other;
        ButtonBindInput thisBtn;
        void Awake()
        {
            thisBtn = GetComponent<ButtonBindInput>();
        }
        public void SetOther()
        {
            Other.bindInfo.isAxis = true;
            Other.bindInfo.AxisName = thisBtn.bindInfo.AxisName;
            Other.bindInfo.isPositive = !thisBtn.bindInfo.isPositive;
            Other.RefreshView();
        }

        public void RemoveOther()
        {
            Other.bindInfo.AxisName = "";
        }
    }
}