﻿using UnityEngine;
using System.Collections;

namespace Legrand.GameSettings
{
    public class ControllerTabHandler : MonoBehaviour
    {
        public ControllerFor Active;
        public GameObject GameplayObj;
        public GameObject BattleObj;

        void Awake()
        {
            Active = ControllerFor.Gameplay;
            SetActive();
        }
        public void ActivateGameplay()
        {
            Active = ControllerFor.Gameplay;
            SetActive();
        }
        public void ActivateBattle()
        {
            Active = ControllerFor.Battle;
            SetActive();
        }
        void SetActive()
        {
            if (Active == ControllerFor.Gameplay)
            {
                GameplayObj.SetActive(true);
                BattleObj.SetActive(false);
            }
            else if (Active == ControllerFor.Battle)
            {
                GameplayObj.SetActive(false);
                BattleObj.SetActive(true);
            }
        }
    }
}
