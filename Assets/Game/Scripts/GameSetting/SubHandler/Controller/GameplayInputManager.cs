﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TeamUtility.IO;

namespace Legrand.GameSettings
{
    public enum ControlType
    {
        Confirm,
        Cancel,
        Forward,
        Backward,
        Left,
        Right,
        Menu
    }
    public enum AxisType
    {
        Vertical,
        Horizontal
    }
    [System.Serializable]
    public class BindInfo
    {
        public bool MainButton;
        public KeyCode keyCode;
        public ControlType controlType;
        public bool isAxis;
        public bool isPositive;
        public string AxisName;
        public float AxisValue;
        public bool invertAxis;
        public InputMode inputMode;
    }
    public class GameplayInputManager : MonoBehaviour
    {
        public float AnalogDeadZone;

        public static GameplayInputManager Instance;
        public static string invertThisAxis = "joy_0_axis_6";

        private DataController[] ControllerDataBind = new DataController[3];
        private Dictionary<ControlType,BindInfo>[] _activeBinding = new Dictionary<ControlType, BindInfo>[3];

        public InputMode activeController;
        public LoadController loadController;


        void initActiveBinding()
        {
            for (int i = 0; i < _activeBinding.Length; i++)
            {
                _activeBinding[i] = new Dictionary<ControlType, BindInfo>();
            }
        }
        void Awake()
        {
            Instance = this;
            initActiveBinding();
            SetControllerData();
        }
        public void AddChangeControllerListener()
        {
            if (EventManager.Instance && !EventManager.Instance.HasListener<ChangeController>(SetMode))
            {
                EventManager.Instance.AddListener<ChangeController>(SetMode);
            }
        }
        void SetMode(ChangeController e)
        {
            if (e.CtrlType == ControllerType.XboxController)
                activeController = InputMode.Gamepad;
            else
                activeController = InputMode.KeyboardAndMouse;
            initActiveBinding();
        }

        struct DataController
        {
            public BindInfo[] KeyboardAndMouse;
            public BindInfo[] Gamepad;
        };

        public float GetAxisRaw(AxisType axis,ControllerFor controller)
        {
            if (axis == AxisType.Vertical)
            {
                if (_activeBinding[(int)controller].ContainsKey(ControlType.Forward))
                {
                    if (_activeBinding[(int)controller][ControlType.Forward].AxisName.Equals(invertThisAxis))
                        return -1f;
                    return 1f;
                }
                if (_activeBinding[(int)controller].ContainsKey(ControlType.Backward))
                {
                    if (_activeBinding[(int)controller][ControlType.Backward].AxisName.Equals(invertThisAxis))
                        return 1f;
                    return -1f;
                }
            }
            else
            {
                if (_activeBinding[(int)controller].ContainsKey(ControlType.Left))
                {
                    return -1f;
                }
                else
                {
                    return 1f;
                }
            }
            return 0f;
        }

        public float GetAxis(AxisType axis,ControllerFor controller, bool raw = false)
        {
            if (activeController == InputMode.Gamepad && !raw)
            {
                if (axis == AxisType.Vertical)
                {
                    float plus = 0f;
                    float minus = 0f;
                    if (_activeBinding[(int)controller].ContainsKey(ControlType.Forward))
                    {
                        if (_activeBinding[(int)controller][ControlType.Forward].AxisName.Equals(invertThisAxis))
                        {
                            _activeBinding[(int)controller][ControlType.Forward].AxisValue = _activeBinding[(int)controller][ControlType.Forward].AxisValue * -1;
                        }
                        plus = _activeBinding[(int)controller][ControlType.Forward].AxisValue;
                        if (_activeBinding[(int)controller][ControlType.Forward].invertAxis)
                            plus = plus * -1;
                    }
                    if (_activeBinding[(int)controller].ContainsKey(ControlType.Backward))
                    {
                        if (_activeBinding[(int)controller][ControlType.Backward].AxisName.Equals(invertThisAxis))
                        {
                            _activeBinding[(int)controller][ControlType.Backward].AxisValue = _activeBinding[(int)controller][ControlType.Backward].AxisValue * -1;
                        }
                        minus = _activeBinding[(int)controller][ControlType.Backward].AxisValue;
                        if (_activeBinding[(int)controller][ControlType.Backward].invertAxis)
                            minus = minus * -1;
                    }
                    if (Math.Abs(plus) > Math.Abs(minus))
                        return plus;
                    else
                        return minus;
                }
                else
                {
                    float plus = 0f;
                    float minus = 0f;
                    if (_activeBinding[(int)controller].ContainsKey(ControlType.Right))
                    {
                        plus = _activeBinding[(int)controller][ControlType.Right].AxisValue;
                        if (_activeBinding[(int)controller][ControlType.Right].invertAxis)
                            plus = plus * -1;
                    }
                    if (_activeBinding[(int)controller].ContainsKey(ControlType.Left))
                    {
                        minus = _activeBinding[(int)controller][ControlType.Left].AxisValue;
                        if (_activeBinding[(int)controller][ControlType.Left].invertAxis)
                            minus = minus * -1;
                    }
                    if (Math.Abs(plus) > Math.Abs(minus))
                        return plus;
                    else
                        return minus;
                }
            }
            else
            {
                if (axis == AxisType.Vertical)
                {
                    if (_activeBinding[(int)controller].ContainsKey(ControlType.Forward))
                    {
                        if (_activeBinding[(int)controller][ControlType.Forward].isPositive)
                        {
                            if (_activeBinding[(int)controller][ControlType.Forward].invertAxis)
                                return -1f;
                            return 1f;
                        }
                        else
                        {
                            if (_activeBinding[(int)controller][ControlType.Forward].invertAxis)
                                return 1f;
                            return -1f;
                        }
                    }
                    if (_activeBinding[(int)controller].ContainsKey(ControlType.Backward))
                    {
                        if (_activeBinding[(int)controller][ControlType.Backward].isPositive)
                        {
                            if (_activeBinding[(int)controller][ControlType.Backward].invertAxis)
                                return -1f;
                            return 1f;
                        }
                        else
                        {
                            if (_activeBinding[(int)controller][ControlType.Backward].invertAxis)
                                return 1f;
                            return -1f;
                        }
                    }
                }
                else
                {
                    if (_activeBinding[(int)controller].ContainsKey(ControlType.Right))
                    {
                        if (_activeBinding[(int)controller][ControlType.Right].isPositive)
                        {
                            if (_activeBinding[(int)controller][ControlType.Right].invertAxis)
                                return -1f;
                            return 1f;
                        }
                        else
                        {
                            if (_activeBinding[(int)controller][ControlType.Right].invertAxis)
                                return 1f;
                            return -1f;
                        }
                    }
                    if (_activeBinding[(int)controller].ContainsKey(ControlType.Left))
                    {
                        if (_activeBinding[(int)controller][ControlType.Left].isPositive)
                        {
                            if (_activeBinding[(int)controller][ControlType.Left].invertAxis)
                                return -1f;
                            return 1f;
                        }
                        else
                        {
                            if (_activeBinding[(int)controller][ControlType.Left].invertAxis)
                                return 1f;
                            return -1f;
                        }
                    }
                }
            }
            return 0f;
        }

        public bool GetButton(ControlType button,ControllerFor controller)
        {
            if (activeController == InputMode.KeyboardAndMouse && (Input.anyKeyDown && _activeBinding[(int)controller].ContainsKey(button)))
                return true;
            else if (activeController == InputMode.Gamepad && _activeBinding[(int)controller].ContainsKey(button))
                return true;
            return false;
        }

        void KeyboardAndMouseProcess()
        {
            foreach (ControllerFor Control in Enum.GetValues(typeof(ControllerFor)))
            {
                foreach (BindInfo bi in ControllerDataBind[(int)Control].KeyboardAndMouse)
                {
                    if (Input.GetKeyDown(bi.keyCode))
                    {
                        if (_activeBinding[(int)Control].ContainsKey(bi.controlType))
                            _activeBinding[(int)Control][bi.controlType] = bi;
                        else
                            _activeBinding[(int)Control].Add(bi.controlType, bi);
                    }
                    if (Input.GetKeyUp(bi.keyCode))
                    {
                        if (_activeBinding[(int)Control].ContainsKey(bi.controlType))
                            _activeBinding[(int)Control].Remove(bi.controlType);
                    }
                }
            }
        }
        bool ValidAxis(float value)
        {
            if (Math.Abs(value) > AnalogDeadZone)
                return true;
            return false;
        }
        bool ValidDirection(bool isPositive,float value)
        {
            if (isPositive && value >= 0)
                return true;
            else if (!isPositive && value < 0)
                return true;
            return false;
        }
        void GamepadProcess()
        {
            foreach (ControllerFor Control in Enum.GetValues(typeof(ControllerFor)))
            {
                foreach (BindInfo bi in ControllerDataBind[(int)Control].Gamepad)
                {
                    if (bi.isAxis)
                    {
                        float axisValue = 0f;
                        if (!bi.AxisName.Equals(""))
                            axisValue = Input.GetAxis(bi.AxisName);
                        bool mainButton = bi.MainButton;
                        if (ValidAxis(axisValue) && ValidDirection(bi.isPositive, axisValue))
                        {
                            bi.AxisValue = axisValue;
                            if (_activeBinding[(int)Control].ContainsKey(bi.controlType))
                                _activeBinding[(int)Control][bi.controlType] = bi;
                            else
                                _activeBinding[(int)Control].Add(bi.controlType, bi);
                        }
                        else
                        {
                            if (_activeBinding[(int)Control].ContainsKey(bi.controlType))
                            {
                                if (_activeBinding[(int)Control][bi.controlType].MainButton == mainButton)
                                    _activeBinding[(int)Control].Remove(bi.controlType);
                            }
                        }
                    }
                    else
                    {
                        if (Input.GetKeyDown(bi.keyCode))
                        {
                            if (_activeBinding[(int)Control].ContainsKey(bi.controlType))
                                _activeBinding[(int)Control][bi.controlType] = bi;
                            else
                                _activeBinding[(int)Control].Add(bi.controlType, bi);
                        }
                        if (Input.GetKeyUp(bi.keyCode))
                        {
                            if (_activeBinding[(int)Control].ContainsKey(bi.controlType))
                                _activeBinding[(int)Control].Remove(bi.controlType);
                        }
                    }
                }
            }
        }
        void ProcessInput()
        {
            if (activeController == InputMode.KeyboardAndMouse)
                KeyboardAndMouseProcess();
            else
                GamepadProcess();
        }
        void Update()
        {
            ProcessInput();
        }
        public void SetControllerData()
        {
            foreach (ControllerFor cf in Enum.GetValues(typeof(ControllerFor)))
            {
                loadController.init(cf);
                ControllerDataBind[(int)cf].KeyboardAndMouse = loadController.GetKeyboardAndMouse();
                ControllerDataBind[(int)cf].Gamepad = loadController.GetGamepad();
            }
        }
    }
}
