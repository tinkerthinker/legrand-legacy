﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

namespace Legrand.GameSettings
{
    public class VideoSettingHandler : MonoBehaviour
    {
        public Dropdown ResolutionDropdown;
        public List<Text> QualityText;

        void SetQuality()
        {
            QualitySettings.SetQualityLevel(GameSetting.VideoQuality, true);
        }
        public void SetQualityOption()
        {
            for (int i = 0; i < QualitySettings.names.Length; i++)
            {
                if(QualityText.Count > i)
                    QualityText[i].text = QualitySettings.names[i];
            }
        }
        public void SetResolutionOption()
        {
            ResolutionDropdown.ClearOptions();
            List<string> resOpt = new List<string>();
            for (int i = Screen.resolutions.Length - 1; i >= 0; i--)
            {
                resOpt.Add(Screen.resolutions[i].width + " x " + Screen.resolutions[i].height);
            }
            ResolutionDropdown.AddOptions(resOpt);
        }
        void SetResolutionSetting()
        {
            Screen.SetResolution(Screen.resolutions[(ResolutionDropdown.options.Count - 1) - ResolutionDropdown.value].width, Screen.resolutions[(ResolutionDropdown.options.Count - 1) - ResolutionDropdown.value].height, GameSetting.FullScreen);
        }
        public void ApplySetting()
        {
            SetQuality();
            SetResolutionSetting();
        }
    }
}
