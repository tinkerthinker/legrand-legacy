﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterSound : MonoBehaviour {

    public void PlayBattleSFX(string sfxString)
    {
        // Format : [name_of_enum],[sound_chance_to_be_played]
        string[] sfxDatas = sfxString.Split(',');
        try
        {
            WwiseManager.BattleFX parsed_enum = (WwiseManager.BattleFX)System.Enum.Parse(typeof(WwiseManager.BattleFX), sfxDatas[0]);
            float chance = 100f;
            if (sfxDatas.Length == 2)
                chance = float.Parse(sfxDatas[1]);
            PlayBattleSFX(parsed_enum, chance);
        }
        catch (System.ArgumentException)
        {
            Debug.LogError(sfxDatas[0] + " not found");
        }
    }

    public void PlayCutsceneSFX(string sfxString)
    {
        // Format : [name_of_enum],[sound_chance_to_be_played]
        string[] sfxDatas = sfxString.Split(',');
        WwiseManager.CutsceneFX parsed_enum = (WwiseManager.CutsceneFX)System.Enum.Parse(typeof(WwiseManager.CutsceneFX), sfxDatas[0]);
        PlayCutsceneSFX(parsed_enum);
    }

    public void PlayBattleSFX(WwiseManager.BattleFX battleFX, float soundChance = 100f)
    {
        if (soundChance < 100f && LegrandUtility.Random(0, 100f) > soundChance)
            return;

        WwiseManager.Instance.PlaySFX(battleFX);
    }

    public void PlayWorldSFX(WwiseManager.WorldFX worldFX, float soundChance = 100f)
    {
        if (soundChance < 100f && LegrandUtility.Random(0, 100f) > soundChance)
            return;

        WwiseManager.Instance.PlaySFX(worldFX);
    }

    public void PlayCutsceneSFX(WwiseManager.CutsceneFX cutsceneSFX)
    {
        WwiseManager.Instance.PlaySFX(cutsceneSFX);
    }
}
