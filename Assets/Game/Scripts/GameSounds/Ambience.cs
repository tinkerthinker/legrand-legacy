﻿using UnityEngine;
using System.Collections;

public class Ambience : MonoBehaviour {
	public WwiseManager.AmbienceFX[] AmbiencesPlayOnEnable;
    public WwiseManager.AmbienceFX[] AmbiencesStopOnEnable;
    public WwiseManager.AmbienceFX[] AmbiencesPlayOnDisable;
    public WwiseManager.AmbienceFX[] AmbiencesStopOnDisable;

	void OnEnable()
    {
        foreach (WwiseManager.AmbienceFX ambience in AmbiencesStopOnEnable)
        {
            if (WwiseManager.Instance != null) WwiseManager.Instance.AmbienceRemove(ambience);
        }
        foreach (WwiseManager.AmbienceFX ambience in AmbiencesPlayOnEnable) {
				if(WwiseManager.Instance != null)  WwiseManager.Instance.AmbienceAdd(ambience);
	    }
    }

	void OnDisable()
    {
        foreach (WwiseManager.AmbienceFX ambience in AmbiencesStopOnDisable)
        {
            if (WwiseManager.Instance != null) WwiseManager.Instance.AmbienceRemove(ambience);
        }
        foreach (WwiseManager.AmbienceFX ambience in AmbiencesPlayOnDisable)
        {
            if (WwiseManager.Instance != null) WwiseManager.Instance.AmbienceAdd(ambience);
        }
	}
}
