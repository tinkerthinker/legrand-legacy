﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;
using Legrand.database;
using System;

public class DatabaseBuffManager : DatabaseTools
{
	private BuffDatabase buffDatabase = ScriptableObject.CreateInstance<BuffDatabase>();

	public override void LoadDatabase ()
	{
		buffDatabase = BuffDatabase.GetDatabase <BuffDatabase>(DATABASE_PATH,FileName.Get(0));
        ThisDatabase.Add(buffDatabase);
    }
	[MenuItem("Legrand Legacy/Database/Buff")]
	private static void Init ()
	{
		DatabaseBuffManager window = EditorWindow.GetWindow<DatabaseBuffManager> ();
		window.minSize = new Vector2 (WINDOW_WIDTH,WINDOW_HEIGHT);
		window.titleContent.text = "Buff Database";
		window.Show ();
	}
	private DatabaseBuffManager()
	{
		ManyTab = 1;
		//tab name and path for prefab
		//database name for scriptable object, none for prefab
		TabName.Set("Buff",0);
		FileName.Set (@"BuffDatabase.asset",0);
	}
	private void OnEnable ()
	{
		LoadDatabase();
		CopyToTemp();
	}
	private void OnDisable ()
	{
		OnExit();
	}
    public override void RefreshThisReference()
    {
        RefreshReference<Buff>("_ID", buffDatabase);
    }
    private void OnGUI ()
	{
		TopTabBar();
		switch(currentTab)
		{
		case 0:
            ScriptableObjectView<BuffDatabase,Buff>(buffDatabase,"_ID","Name");
			break;
		}
	}
}
