﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;
using Legrand.database;
using System;

public class DatabaseSkillManager : DatabaseTools
{
	private OffenseSkillDatabase offensiveSkill = ScriptableObject.CreateInstance<OffenseSkillDatabase>();
//	private DefensiveSkillDatabase defensiveSkill = ScriptableObject.CreateInstance<DefensiveSkillDatabase>();
//    private CombinationSkillDatabase combinationSkill = ScriptableObject.CreateInstance<CombinationSkillDatabase>();
	private NormalSkillDatabase normalSkill = ScriptableObject.CreateInstance<NormalSkillDatabase>();
	private GuardBuffDatabase guardBuff = ScriptableObject.CreateInstance<GuardBuffDatabase>();


	public override void LoadDatabase ()
	{
		offensiveSkill = OffenseSkillDatabase.GetDatabase <OffenseSkillDatabase>(DATABASE_PATH,FileName.Get(0));
//		defensiveSkill = DefensiveSkillDatabase.GetDatabase <DefensiveSkillDatabase>(DATABASE_PATH,FileName.Get(1));
//        combinationSkill = CombinationSkillDatabase.GetDatabase<CombinationSkillDatabase> (DATABASE_PATH, FileName.Get (2));
		normalSkill = NormalSkillDatabase.GetDatabase <NormalSkillDatabase>(DATABASE_PATH,FileName.Get(1));
		guardBuff = GuardBuffDatabase.GetDatabase<GuardBuffDatabase> (DATABASE_PATH, FileName.Get (2));
        ThisDatabase.Add(offensiveSkill);
//        ThisDatabase.Add(defensiveSkill);
        ThisDatabase.Add(normalSkill);
//        ThisDatabase.Add(combinationSkill);
        ThisDatabase.Add(guardBuff);
    }
	[MenuItem("Legrand Legacy/Database/Skill")]
	private static void Init ()
	{
		DatabaseSkillManager window = EditorWindow.GetWindow<DatabaseSkillManager> ();
		window.minSize = new Vector2 (WINDOW_WIDTH,WINDOW_HEIGHT);
		window.titleContent.text = "Skill";
		window.Show ();
	}
	private DatabaseSkillManager()
	{
		ManyTab = 3;
		//tab name and path for prefab
		//database name for scriptable object, none for prefab
		TabName.Set("Skill Ultimate",0);
		FileName.Set (@"OffenseSkillDatabase.asset",0);

//		TabName.Set("Defensive Skill",1);
//		FileName.Set (@"DefenseSkillDatabase.asset",1);
//
//        TabName.Set("Combination Skill",2);
//        FileName.Set (@"CombinationSkillDatabase.asset",2);

		TabName.Set("Normal Skill",1);
		FileName.Set (@"NormalSkillDatabase.asset",1);

		TabName.Set("Guard Buff",2);
		FileName.Set (@"GuardBuffDatabase.asset",2);
	}
	private void OnEnable ()
	{
		LoadDatabase();
		CopyToTemp();
	}
	private void OnDisable ()
	{
		OnExit();
	}
    public override void RefreshThisReference()
    {
//        RefreshReference<CombinationSkill>("IdSkill", combinationSkill);
        RefreshReference<OffensiveBattleSkill>("IdSkill", offensiveSkill);
//        RefreshReference<DefensiveBattleSkill>("IdSkill", defensiveSkill);
        RefreshReference<NormalSkill>("IdSkill", normalSkill);
        RefreshReference<GuardBuff>("IdSkill", guardBuff);
    }
    private void OnGUI ()
	{
		TopTabBar();
		switch(currentTab)
		{
		case 0:
			ScriptableObjectView<OffenseSkillDatabase,OffensiveBattleSkill>(offensiveSkill, "IdSkill", "Name");
			if(CurrentIndex.Get(currentTab) >= 0 && CurrentIndex.Get(currentTab) < offensiveSkill.Count) 
				offensiveSkill.Get(CurrentIndex.Get(currentTab)).CommandValue = LegrandUtility.GetUniqueSymbol(offensiveSkill.Get(CurrentIndex.Get(currentTab)).IdSkill);
			break;
//		case 1:
//			ScriptableObjectView<DefensiveSkillDatabase,DefensiveBattleSkill>(defensiveSkill, "IdSkill", "Name");
//			if(CurrentIndex.Get(currentTab) >= 0 && CurrentIndex.Get(currentTab) < defensiveSkill.Count) 
//				defensiveSkill.Get(CurrentIndex.Get(currentTab)).CommandValue = LegrandUtility.GetUniqueSymbol(defensiveSkill.Get(CurrentIndex.Get(currentTab)).IdSkill);
//			break;
//		case 2:
//			ScriptableObjectView<CombinationSkillDatabase,CombinationSkill>(combinationSkill, "IdSkill", "Name");
//			if(CurrentIndex.Get(currentTab) >= 0 && CurrentIndex.Get(currentTab) < combinationSkill.Count) 
//				combinationSkill.Get(CurrentIndex.Get(currentTab)).CommandValue = LegrandUtility.GetUniqueSymbol(combinationSkill.Get(CurrentIndex.Get(currentTab)).IdSkill);
//                break;
        case 1:
            ScriptableObjectView<NormalSkillDatabase,NormalSkill>(normalSkill, "IdSkill", "Name");
            if(CurrentIndex.Get(currentTab) >= 0 && CurrentIndex.Get(currentTab) < normalSkill.Count) 
                normalSkill.Get(CurrentIndex.Get(currentTab)).CommandValue = LegrandUtility.GetUniqueSymbol(normalSkill.Get(CurrentIndex.Get(currentTab)).IdSkill);
            break;
		case 2:
			ScriptableObjectView<GuardBuffDatabase,GuardBuff>(guardBuff, "IdSkill", "Name");
			if(CurrentIndex.Get(currentTab) >= 0 && CurrentIndex.Get(currentTab) < guardBuff.Count) 
				guardBuff.Get(CurrentIndex.Get(currentTab)).CommandValue = LegrandUtility.GetUniqueSymbol(guardBuff.Get(CurrentIndex.Get(currentTab)).IdSkill);
			break;
		}
	}
}
