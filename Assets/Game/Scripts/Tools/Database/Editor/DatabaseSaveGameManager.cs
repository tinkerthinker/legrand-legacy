﻿using UnityEditor;
using UnityEngine;
using Legrand.database;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Security.Cryptography;
using System.Text;

public class DatabaseSaveGameManager : DatabaseTools
{
    private SaveGameDatabase saveData = ScriptableObject.CreateInstance<SaveGameDatabase>();
    string SaveFileName = "Save 01";

    public override void LoadDatabase()
    {
        saveData = SaveGameDatabase.GetDatabase<SaveGameDatabase>(DATABASE_PATH, FileName.Get(0));
        ThisDatabase.Add(saveData);
    }
    [MenuItem("Legrand Legacy/Database/Save Game")]
    private static void Init()
    {
        DatabaseSaveGameManager window = EditorWindow.GetWindow<DatabaseSaveGameManager>();
        window.minSize = new Vector2(WINDOW_WIDTH, WINDOW_HEIGHT);
        window.titleContent.text = "Save Game Database";
        window.Show();
    }
    private DatabaseSaveGameManager()
    {
        ManyTab = 1;
        //tab name and path for prefab
        //database name for scriptable object, none for prefab
        TabName.Set("Save Game", 0);
        FileName.Set(@"SaveGameDatabase.asset", 0);
    }
    private void OnEnable()
    {
        LoadDatabase();
        CopyToTemp();
    }
    private void OnDisable()
    {
        OnExit();
    }
    void SaveLoad()
    {
        GUILayout.BeginVertical();
        SaveFileName = DetailElement("File Name", SaveFileName);
        GUILayout.BeginHorizontal();
        if(GUILayout.Button("Load"))
        {
            Load();
            saveData.Get(CurrentIndex.Get(currentTab)).SaveData.Clear();
            saveData.Get(CurrentIndex.Get(currentTab)).SaveData = SaveModel;
            saveData.refresh();
        }
        if (GUILayout.Button("Create File"))
        {
            SaveModel = saveData.Get(CurrentIndex.Get(currentTab)).SaveData;
            Create();
        }
        GUILayout.EndHorizontal();
        GUILayout.EndVertical();
    }
    private void OnGUI()
    {
        TopTabBar();
        if(saveData.Count > 0)
            SaveLoad();
        switch (currentTab)
        {
            case 0:
            ScriptableObjectView<SaveGameDatabase, SaveGameDataModel>(saveData, "Name");
            break;
        }
    }
    public SaveData SaveModel;
    
    void Load()
    {
        if (UpdateDataFromFile(SaveFileName))
        {
            Debug.Log("file loaded");
        }
        else
        {
            Debug.Log("load fail");
        }
    }
    void Create()
    {
        if (SaveGame(SaveFileName))
        {
            Debug.Log("file created");
        }
        else
        {
            Debug.Log("create fail");
        }
    }
    public bool SaveGame(string SaveName)
    {
        BinaryFormatter bf = new BinaryFormatter();
        Directory.CreateDirectory(LegrandBackend.SavePath + "/" + SaveName);

        using (FileStream stream = new FileStream(LegrandBackend.SavePath + "/" + SaveName + "/Save.gd", FileMode.Create))
        {
            try
            {
                bf.Serialize(stream, SaveModel);
            }
            catch (System.Exception)
            {
                return false;
            }
        }

        return true;
    }
    public bool UpdateDataFromFile(string FileLocation)
    {
        BinaryFormatter bf = new BinaryFormatter();

        if (File.Exists(LegrandBackend.SavePath + "/" + FileLocation + "/Save.gd"))
        {
            using (FileStream stream = new FileStream(LegrandBackend.SavePath + "/" + FileLocation + "/Save.gd", FileMode.Open))
            {
                try
                {
                    SaveModel = bf.Deserialize(stream) as SaveData;
                }
                catch (System.Exception e)
                {
                    Debug.LogError(e + "Exception caught in encoding");
                    return false;
                }
            }
        }
        else
            return false;

        return true;
    }
}
