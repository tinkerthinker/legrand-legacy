﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;
using Legrand.database;

public class DatabaseLanguageManager : DatabaseTools
{
    private LanguageDatabase language = ScriptableObject.CreateInstance<LanguageDatabase>();

    public override void LoadDatabase ()
    {
        language = LanguageDatabase.GetDatabase <LanguageDatabase>(DATABASE_PATH,FileName.Get(0));
        ThisDatabase.Add(language);
    }
    [MenuItem("Legrand Legacy/Database/Language")]
    private static void Init ()
    {
        DatabaseLanguageManager window = EditorWindow.GetWindow<DatabaseLanguageManager> ();
        window.minSize = new Vector2 (WINDOW_WIDTH,WINDOW_HEIGHT);
        window.titleContent.text = "Language Database";
        window.Show ();
    }
    private DatabaseLanguageManager()
    {
        ManyTab = 1;
        //tab name and path for prefab
        //database name for scriptable object, none for prefab
        TabName.Set("Language",0);
        FileName.Set (@"LanguageDatabase.asset",0);
    }
    private void OnEnable ()
    {
        LoadDatabase();
        CopyToTemp();
    }
    private void OnDisable ()
    {
        OnExit();
    }
    private void OnGUI ()
    {
        TopTabBar();
        switch(currentTab)
        {
            case 0:
                ScriptableObjectView<LanguageDatabase,LanguageData>(language,"ID");
                break;
        }
    }
}
