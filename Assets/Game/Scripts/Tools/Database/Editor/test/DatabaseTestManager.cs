﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;
using Legrand.database;

public class DatabaseTestManager : DatabaseTools
{
    private TestingDatabase test = ScriptableObject.CreateInstance<TestingDatabase>();

    public override void LoadDatabase ()
    {
        test = TestingDatabase.GetDatabase <TestingDatabase>(DATABASE_PATH,FileName.Get(0));
        ThisDatabase.Add(test);
    }
    //[MenuItem("Legrand Legacy/Database/Test")]
    private static void Init ()
    {
        DatabaseTestManager window = EditorWindow.GetWindow<DatabaseTestManager> ();
        window.minSize = new Vector2 (WINDOW_WIDTH,WINDOW_HEIGHT);
        window.titleContent.text = "Test Database";
        window.Show ();
    }
    private DatabaseTestManager()
    {
        ManyTab = 1;
        //tab name and path for prefab
        //database name for scriptable object, none for prefab
        TabName.Set ("Test", 0);
        FileName.Set (@"TestDatabase.asset",0);
    }
    private void OnEnable ()
    {
        LoadDatabase();
        CopyToTemp();
    }
    private void OnDisable ()
    {
        OnExit();
    }
    private void OnGUI ()
    {
        TopTabBar();
        switch(currentTab)
        {
            case 0:
                ScriptableObjectView<TestingDatabase,DataModelTest>(test, "ID");
                break;
        }
    }
}
