﻿using UnityEditor;
using UnityEngine;
using Legrand.database;

public class DatabaseKeyCodeManager : DatabaseTools
{
    private KeyCodeDatabase keycode = ScriptableObject.CreateInstance<KeyCodeDatabase>();

    public override void LoadDatabase ()
    {
        keycode = KeyCodeDatabase.GetDatabase <KeyCodeDatabase>(DATABASE_PATH,FileName.Get(0));
        ThisDatabase.Add(keycode);
    }
    [UnityEditor.MenuItem("Legrand Legacy/Database/KeyCode Translate")]
    private static void Init ()
    {
        DatabaseKeyCodeManager window = EditorWindow.GetWindow<DatabaseKeyCodeManager> ();
        window.minSize = new Vector2 (WINDOW_WIDTH,WINDOW_HEIGHT);
        window.titleContent.text = "KeyCode Database";
        window.Show ();
    }
    private DatabaseKeyCodeManager()
    {
        ManyTab = 1;
        //tab name and path for prefab
        //database name for scriptable object, none for prefab
        TabName.Set("KeyCodeTranslate",0);
        FileName.Set (@"KeyCodeDatabase.asset",0);
    }
    private void OnEnable ()
    {
        LoadDatabase();
        CopyToTemp();
    }
    private void OnDisable ()
    {
        OnExit();
    }
    private void OnGUI ()
    {
        TopTabBar();
        switch (currentTab)
        {
            case 0:
            ScriptableObjectView<KeyCodeDatabase,KeyCodeTranslate>(keycode,"KeyCode", "AxisCode", "KeyName");
            break;
        }
    }
}
