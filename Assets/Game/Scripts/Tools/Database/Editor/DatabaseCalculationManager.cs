﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;
using Legrand.database;

public class DatabaseCalculationManager : DatabaseTools
{
	private BattlerWeaponCalculationDatabase battlerWeaponDatabase = ScriptableObject.CreateInstance<BattlerWeaponCalculationDatabase>();
	private BattlerArmorCalculationDatabase battlerArmorDatabase = ScriptableObject.CreateInstance<BattlerArmorCalculationDatabase>();
	
	public override void LoadDatabase ()
	{
		battlerWeaponDatabase = BattlerWeaponCalculationDatabase.GetDatabase <BattlerWeaponCalculationDatabase>(DATABASE_PATH,FileName.Get(0));
		battlerArmorDatabase = BattlerArmorCalculationDatabase.GetDatabase <BattlerArmorCalculationDatabase>(DATABASE_PATH,FileName.Get(1));
        ThisDatabase.Add(battlerWeaponDatabase);
        ThisDatabase.Add(battlerArmorDatabase);
	}
	[MenuItem("Legrand Legacy/Database/Battler")]
	private static void Init ()
	{
		DatabaseCalculationManager window = EditorWindow.GetWindow<DatabaseCalculationManager> ();
		window.minSize = new Vector2 (WINDOW_WIDTH,WINDOW_HEIGHT);
		window.titleContent.text = "Battler Database";
		window.Show ();
	}
	private DatabaseCalculationManager()
	{
		ManyTab = 2;
		//tab name and path for prefab
		//database name for scriptable object, none for prefab
		TabName.Set("Weapon",0);
		FileName.Set (@"BattlerWeaponDatabase.asset",0);

		TabName.Set("Armor",1);
		FileName.Set (@"BattlerArmorDatabase.asset",1);
	}
	private void OnEnable ()
	{
		LoadDatabase();
		CopyToTemp();
	}
	private void OnDisable ()
	{
		OnExit();
	}
	private void OnGUI ()
	{
		TopTabBar();
		switch(currentTab)
		{
		case 0:
			ScriptableObjectView<BattlerWeaponCalculationDatabase,WeaponCalculationModel>(battlerWeaponDatabase,"None");
			break;
		case 1:
			ScriptableObjectView<BattlerArmorCalculationDatabase,ArmorCalculationModel>(battlerArmorDatabase,"None");
			break;
		}
	}
}
