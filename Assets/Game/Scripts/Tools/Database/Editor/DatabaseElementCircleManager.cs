﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;
using Legrand.database;

public class DatabaseElementCircleManager : DatabaseTools
{
    private ElementCircleDatabase elementDatabase = ScriptableObject.CreateInstance<ElementCircleDatabase>();

    public override void LoadDatabase ()
    {
        elementDatabase = ElementCircleDatabase.GetDatabase<ElementCircleDatabase>(DATABASE_PATH,FileName.Get(0));
        ThisDatabase.Add(elementDatabase);
    }
    [MenuItem("Legrand Legacy/Database/Element Circle")]
    private static void Init ()
    {
        DatabaseElementCircleManager window = EditorWindow.GetWindow<DatabaseElementCircleManager> ();
        window.minSize = new Vector2 (WINDOW_WIDTH,WINDOW_HEIGHT);
        window.titleContent.text = "Element Circle Database";
        window.Show ();
    }
    private DatabaseElementCircleManager()
    {
        ManyTab = 1;
        //tab name and path for prefab
        //database name for scriptable object, none for prefab
        TabName.Set ("ElementCircle", 0);
        FileName.Set (@"ElementCircleDatabase.asset",0);
    }
    private void OnEnable ()
    {
        LoadDatabase();
        CopyToTemp();
    }
    private void OnDisable ()
    {
        OnExit();
    }
    private void OnGUI ()
    {
        TopTabBar();
        switch(currentTab)
        {
            case 0:
                ScriptableObjectView<ElementCircleDatabase,ElementCircle>(elementDatabase,"CurrentElement");
                break;
        }
    }
}
