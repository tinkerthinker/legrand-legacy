﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;
using Legrand.database;

public class DatabaseBattleEffectManager : DatabaseTools
{
    private BattleEffectDatabase battleEffectDatabase = ScriptableObject.CreateInstance<BattleEffectDatabase>();

    public override void LoadDatabase ()
    {
        battleEffectDatabase = BattleEffectDatabase.GetDatabase <BattleEffectDatabase>(DATABASE_PATH,FileName.Get(0));
        ThisDatabase.Add(battleEffectDatabase);
    }
    [MenuItem("Legrand Legacy/Database/Battle Effect Option")]
    private static void Init ()
    {
        DatabaseBattleEffectManager window = EditorWindow.GetWindow<DatabaseBattleEffectManager> ();
        window.minSize = new Vector2 (WINDOW_WIDTH,WINDOW_HEIGHT);
        window.titleContent.text = "Battle Effect Database";
        window.Show ();
    }
    private DatabaseBattleEffectManager()
    {
        ManyTab = 1;
        //tab name and path for prefab
        //database name for scriptable object, none for prefab
        TabName.Set("Effect Option",0);
        FileName.Set (@"BattleEffectDatabase.asset",0);
    }
    private void OnEnable ()
    {
        LoadDatabase();
        CopyToTemp();
    }
    private void OnDisable ()
    {
        OnExit();
    }
    private void OnGUI ()
    {
        TopTabBar();
        switch(currentTab)
        {
            case 0:
                ScriptableObjectView<BattleEffectDatabase,BattleEffect>(battleEffectDatabase,"Name");
                break;
        }
    }
}
