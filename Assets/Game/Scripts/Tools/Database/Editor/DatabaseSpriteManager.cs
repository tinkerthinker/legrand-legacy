﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;
using Legrand.database;

public class DatabaseSpriteManager : DatabaseTools
{
    private SpriteDatabase mapSprite = ScriptableObject.CreateInstance<SpriteDatabase>();
    private SpriteDatabase uiSprite = ScriptableObject.CreateInstance<SpriteDatabase>();

    private MapAreaDatabase mapAreaDatabase = ScriptableObject.CreateInstance<MapAreaDatabase>();

    public override void LoadDatabase ()
    {
        mapSprite =  SpriteDatabase.GetDatabase <SpriteDatabase>(DATABASE_PATH,FileName.Get(1));
        uiSprite =  SpriteDatabase.GetDatabase <SpriteDatabase>(DATABASE_PATH,FileName.Get(0));
        ThisDatabase.Add(mapSprite);
        ThisDatabase.Add(uiSprite);
    }
    [MenuItem("Legrand Legacy/Database/Sprite")]
    private static void Init ()
    {
        DatabaseSpriteManager window = EditorWindow.GetWindow<DatabaseSpriteManager> ();
        window.minSize = new Vector2 (WINDOW_WIDTH,WINDOW_HEIGHT);
        window.titleContent.text = "Sprite Database";
        window.Show ();
    }
    private DatabaseSpriteManager()
    {
        ManyTab = 2;
        //tab name and path for prefab
        //database name for scriptable object, none for prefab
        TabName.Set ("Area Sprite", 1);
        FileName.Set (@"MapSpriteDatabase.asset",1);

        TabName.Set ("Sprite", 0);
        FileName.Set (@"UISpriteDatabase.asset",0);
    }
    private void OnEnable ()
    {
        LoadDatabase();
        CopyToTemp();
        SetAreaSprite();
    }
    void SetAreaSprite()
    {
        mapAreaDatabase = MapAreaDatabase.GetDatabase<MapAreaDatabase>(DATABASE_PATH, "MapAreaData.asset");
        for(int i=0;i<mapAreaDatabase.Count;i++)
        {
            if (i < mapSprite.Count)
            {
                mapSprite.Get(i).ID = mapAreaDatabase.Get(i).areaName;
            }
            else
            {
                SpriteDataModel temp = new SpriteDataModel();
                temp.ID = mapAreaDatabase.Get(i).areaName;
                temp.sprite = new Sprite();
                mapSprite.AddValue(temp);
            }
        }
    }
    private void OnDisable ()
    {
        OnExit();
    }
    private void OnGUI ()
    {
        TopTabBar();
        switch(currentTab)
        {
            case 1:
                ScriptableObjectView<SpriteDatabase,SpriteDataModel>(mapSprite,"ID");
                break;
            case 0:
                ScriptableObjectView<SpriteDatabase,SpriteDataModel>(uiSprite,"ID");
                break;
        }
    }
}
