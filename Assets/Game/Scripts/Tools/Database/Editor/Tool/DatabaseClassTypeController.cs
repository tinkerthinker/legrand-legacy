#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;
using System.Reflection;
using System;
using System.IO;
using UnityEngine.UI;
using Legrand.War;

namespace Legrand.database
{
    public enum GetStatus {WORLDMAPPOINT,CITYPOINT,PORTAL,AREADIR,SUBAREA,ChildOf};
    public class GetGameObject
    {
        static GameObject GetGameObjectFromFullPath(string Path)
        {
            Path = Path.Replace("Resources","?").Split('?')[1].Substring(1).Replace(@"\",@"/").Split('.')[0];

            GameObject Go = (GameObject)Resources.Load(Path) as GameObject;
            return Go;
        }

        public static void GetGameObjectInSubDir(string objectName,string path,ref UnityEngine.Object Go)
        {
            bool found = false;
            string [] subdirectoryEntries = Directory.GetDirectories(path);
            foreach(var sub in subdirectoryEntries)
            {
                DirectoryInfo dir = new DirectoryInfo(sub);
                FileInfo[] info = dir.GetFiles("*.prefab");
                foreach (FileInfo f in info)
                {
                    string nama = f.Name.Split('.')[0];
                    if(nama.Equals(objectName))
                    {
                        Go = GetGameObjectFromFullPath(f.FullName);
                        found = true;
                        break;
                    }
                }
                if(!found)
                    GetGameObjectInSubDir(objectName,sub,ref Go);
            }
        }
        private static string[] PrefabName(string dirName)
        {
            List<string> name = new List<string>();
            DirectoryInfo dir = new DirectoryInfo(dirName);
            FileInfo[] info = dir.GetFiles("*.prefab");
            foreach (FileInfo f in info) 
            {
                name.Add(f.Name.Split('.')[0]);
            }
            return name.ToArray();
        }
        public static void GetChildOfGameObject(string dirName,string name,List<string> option)
        {
            GetChildName(dirName,name,option);
        }
        public static void GetChildsName(GameObject Go,List<string> childs)
        {
            for(int a=0;a<Go.transform.childCount;a++)
            {
                childs.Add(Go.transform.GetChild(a).name);
                GetChildsName(Go.transform.GetChild(a).gameObject,childs);
            }
        }
        private static void GetChildName(string dirName,string Name,List<string> option)
        {
            string[] prefabs = PrefabName(dirName);
            foreach(var name in prefabs)
            {
                if(name.Equals(Name))
                {
                    #if UNITY_EDITOR
                    GameObject Go = AssetDatabase.LoadAssetAtPath(dirName + "/" + name + ".prefab", typeof(GameObject)) as GameObject;
                    GetChilds(Go,option,dirName,GetStatus.ChildOf);
                    #endif
                }
            }
            string [] subdirectoryEntries = Directory.GetDirectories(dirName);
            if(subdirectoryEntries.Length > 0)
            {
                foreach(var s in subdirectoryEntries)
                {
                    GetChildOfGameObject(s,Name,option);
                }
            }
        }

        public static void Get(string dirName,List<string> GameObjectName,bool GetChild,GetStatus status)
        {
            if(status != GetStatus.AREADIR)
                GetPrefabName(dirName,GameObjectName,GetChild,status);
            else
                GetFolderName(dirName,GameObjectName);
        }
        private static bool isHasSubFolder(string path)
        {
            string [] subdirectoryEntries = Directory.GetDirectories(path);
            if(subdirectoryEntries.Length > 0)
                return true;
            return false;
        }
        private static void GetFolderName(string dirName,List<string> GameObjectName)
        {
            string [] subdirectoryEntries = Directory.GetDirectories(dirName);
            if(!isHasSubFolder(dirName))
                GameObjectName.Add(dirName);
            foreach(var s in subdirectoryEntries)
            {
                GetFolderName(s,GameObjectName);
            }
        }
        private static string GetRootName(Transform obj)
        {
            while (obj.parent != null)
            {
                obj = obj.parent;
            }
            return obj.name;
        }
        private static void GetChilds(GameObject Go,List<string> GameObjectList,string dirName,GetStatus status)
        {
            if (status == GetStatus.WORLDMAPPOINT)
            {
                WorldMapPortalTrigger wmpt = Go.GetComponent<WorldMapPortalTrigger>();
                if (wmpt != null)
                {
                    GameObjectList.Add(Go.name);
                }
            }
            else if (status == GetStatus.CITYPOINT)
            {               
                Button bu = Go.GetComponent<Button>();
                if (bu != null)
                {
                    GameObjectList.Add(Go.name);
                }
            }
            else if(status == GetStatus.PORTAL)
            {
                MapPointSelection bu = Go.GetComponent<MapPointSelection>();
                PortalTrigger pt = Go.GetComponent<PortalTrigger>();
                WorldMapPortalTrigger wmpt = Go.GetComponent<WorldMapPortalTrigger>();
                
                if(bu != null || pt != null || wmpt != null)
                {
                    string subAreaName = GetRootName(Go.transform);
                    GameObjectList.Add(subAreaName + "+" + Go.name);
                }
            }
            else if(status == GetStatus.ChildOf)
            {
                GameObjectList.Add(Go.name);
            }
            for(int c=0;c<Go.transform.childCount;c++)
            {
                GetChilds(Go.transform.GetChild(c).gameObject,GameObjectList,dirName,status);
            }
        }
        private static void GetPrefabName(string dirName,List<string> GameObjectList,bool GetChild,GetStatus status)
        {
            string[] prefabs = PrefabName(dirName);
            foreach(var name in prefabs)
            {
                GameObject Go = AssetDatabase.LoadAssetAtPath(dirName + "/" + name + ".prefab", typeof(GameObject)) as GameObject;
                if (Go != null)
                {
                    if (GetChild)
                    {
                        GetChilds(Go, GameObjectList, dirName, status);
                    }
                    else
                    {
                        GameObjectList.Add(Go.name);
                    }
                }
            }
            string [] subdirectoryEntries = Directory.GetDirectories(dirName);
            if(subdirectoryEntries.Length > 0)
            {
                foreach(var s in subdirectoryEntries)
                {
                    Get(s,GameObjectList,GetChild,status);
                }
            }
        }
    }

    public class DatabaseVector2Controller
    {
        public static object DropDownControl(object input,string name)
        {
            string[] option = new string[7];
            option [0] = "front left";
            option [1] = "front center";
            option [2] = "front right";
            option [3] = "back left";
            option [4] = "back center";
            option [5] = "back right";
            option[6] = "none";

            int selected = 0;
            Vector2 value = (Vector2)input;
            if (value.x == 0 && value.y == 0)
                selected = 3;
            else if (value.x == 1 && value.y == 0)
                selected = 4;
            else if (value.x == 2 && value.y == 0)
                selected = 5;
            else if (value.x == 0 && value.y == 1)
                selected = 0;
            else if (value.x == 1 && value.y == 1)
                selected = 1;
            else if (value.x == 2 && value.y == 1)
                selected = 2;
            else if (value.x == -1 && value.y == -1)
                selected = 6;

            GUILayout.Label(name,GUILayout.Width(170));
            selected = EditorGUILayout.Popup(selected,option ,GUILayout.Width(150));

            if (selected == 0)
                return new Vector2 (0, 1);
            else if (selected == 1)
                return new Vector2 (1, 1);
            else if (selected == 2)
                return new Vector2 (2, 1);
            else if (selected == 3)
                return new Vector2 (0, 0);
            else if (selected == 4)
                return new Vector2 (1, 0);
            else if (selected == 5)
                return new Vector2(2, 0);

            return new Vector2 (-1, -1);
        }
    }

    public class DatabaseClassTypeController : ScriptableObject
    {
        public BuffDatabase buffDatabase;//ini
        public EncounterGrowthDatabase encounterGrowthDatabase;//ini
        //item
        public ConsumableHealingsDatabase consumableHealingDatabase;
        public ConsumableItemAttributeDatabase consumableAttributeDatabase;
        public MaterialItemDatabase materialItemDatabase;
        public MagicItemDatabase magicItemDatabase;
        public ConsumableMiscDatabase miscItemDatabase;
        public KeyDatabase keyDatabase;
        //Encounter
        public EncounterDatabase encounterDatabase;//ini
        public EncounterPartyDatabase encounterPartyDatabase;
        //story
        public StoryDatabase storyData;
        //battle effect
        public BattleEffectDatabase battleEffectOption;
		//skill
		public NormalSkillDatabase normalSkillDatabase;//ini
		public DefensiveSkillDatabase defensiveSkillDatabase;//ini
        public OffenseSkillDatabase offensiveSkillDatabase;//ini
        public CombinationSkillDatabase combinationSkillDatabase;//ini
        public GuardBuffDatabase guardBuffDatabase;//ini
        //magic
        public MagicDatabase MagicData;//ini
        //equipments
        public EquipmentsDatabase EquipmentsData;//ini
        //war
        public WarArcanaDataModelDatabase warArcanaDataModelDatabase;
        public WarArcanaEffectDatabase warArcanaEffectDatabase;
        public WarBuffDatabase warBuffDatabase;
        //area data
        public MapAreaDatabase MapAreaDatabase;

        private DatabaseGameObjectList listAndIndexController = new DatabaseGameObjectList();//dictionary untuk menampung list child dalam object

        protected Dictionary<string, string> SearchPopupKey = new Dictionary<string, string>();
        int DropDownPopup(string name, int index, string[] data, int width = DatabaseState.DROP_DOWN_ENUM_WIDTH)
        {
            GUILayout.BeginHorizontal();
            int result = index;
            List<int> indexData = new List<int>();
            if (DatabaseState.SearchActive)
            {
                string key = "";
                if (SearchPopupKey.ContainsKey(name))
                    key = SearchPopupKey[name];
                else
                    SearchPopupKey.Add(name, "");

                GUILayout.Label("Search");
                key = EditorGUILayout.TextField(key);

                List<string> newData = new List<string>();
                for(int i = 0;i<data.Length;i++)
                {
                    if (data[i].ToLower().Replace(" ", "").Contains(key.ToLower().Replace(" ", "")))
                    {
                        newData.Add(data[i]);
                        indexData.Add(i);
                        if(data[i].Equals(data[index]))
                            index = newData.Count - 1;
                    }
                }
                result = EditorGUILayout.Popup(index, newData.ToArray());

                SearchPopupKey[name] = key;

                if(result < indexData.Count)
                    result = indexData[result];
            }
            else
                result = EditorGUILayout.Popup(index, data);
            GUILayout.EndHorizontal();

            return result;
        }

        private void AddList<T>(IList listElement,T data,int index)where T :IComponentDatabase
        {
            GUILayout.BeginHorizontal();
            if(GUILayout.Button ("Add", GUILayout.Width(DatabaseState.BOTTOM_BUTTON_WIDTH)))
            {
                listElement.Add(default(T));
                data.refresh();
            }

            /*if(index >= 0 && GUILayout.Button ("Delete", GUILayout.Width(DatabaseState.BOTTOM_BUTTON_WIDTH)))
            {
                if(EditorUtility.DisplayDialog("Delete","Do you want to delete index " + index.ToString() + " ?","Yes","No"))
                {
                    listElement.RemoveAt(index);
                    data.refresh();
                }
            }*/
            GUILayout.EndHorizontal();
        }

        public bool isKnownClass(FieldInfo field)//for all
        {
            if(field.FieldType == typeof(EncounterGrowth))
                return true;
            if(field.FieldType == typeof(List<EncounterGrowth>))
                return true;
            if(field.FieldType == typeof(List<Buff>))
                return true;
            if(field.FieldType == typeof(Buff))
                return true;
            if(field.FieldType == typeof(EncounterDataModel))
                return true;
            if(field.FieldType == typeof(List<EncounterDataModel>))
                return true;
            
            if (field.Name.Equals("AreaName"))//rules area name
                return true;
            if (field.Name.Equals("Owner"))//rules
                return true;
            if(field.Name.Equals("ActiveGameObjects"))//rules
                return true;
            if(field.Name.Equals("InactiveGameObjects"))//rules
                return true;
            if(field.Name.Equals("HealingAndMagicItemID"))
                return true;
            if(field.Name.Equals("AllItemID"))
                return true;
            if (field.Name.Equals("dungeonMonsters"))//map tool
                return true;
            if (field.Name.Equals("ListWorldEventID"))//quest
                return true;
            if(field.Name.Equals("CastingMagicEffect"))//char magic
                return true;
            if(field.Name.Equals("AttackMagicEffect"))//char magic
				return true;
			if(field.Name.Equals("HitEffect"))//char skill
				return true;
			if(field.Name.Equals("OffensiveSkillData"))
				return true;
			if(field.Name.Equals("DefensiveSkillData"))
				return true;
			if(field.Name.Equals("CombinationSkillData"))
                return true;
            if(field.Name.Equals("NormalSkillData"))
                return true;
			if(field.Name.Equals("GuardBuffData"))
				return true;
            if(field.Name.Equals("ItemParticle"))
                return true;
            if (field.Name.Equals("NormalSkillParticle"))
                return true;
            if (field.Name.Equals("BattlerScript"))
                return true;
            if (field.Name.Equals("SkillUltimateOffensive")) 
                return true;
            if (field.Name.Equals("OffensiveBattleSkills"))
                return true;
            if (field.Name.Equals("SkillUltimateDefensive"))
                return true;
            if(field.Name.Equals("PassiveGuardSkill"))
                return true;
            if(field.Name.Equals("Magic"))
                return true;
            if(field.Name.Equals("Skill"))
                return true;
            if (field.Name.Equals("EncounterMagics"))
                return true;
            if(field.Name.Equals("Equipment"))
                return true;
            if (field.Name.Equals("Arcana"))
                return true;
            if (field.Name.Equals("AppliedBuff"))
                return true;
            if (field.Name.Equals("ArcanaEffects"))
                return true;

            return false;
        }

        public object DropDownControl(FieldInfo field,object obj,float windowWidth,string nodeID, bool isDungeon)//untuk map tool
        {
            if (isDungeon && (field.FieldType == typeof(List<DungeonMonster>) && field.Name.Equals("dungeonMonsters")))
            {
                if (!obj.Equals(null))
                {
                    List<int> deletedIndex = new List<int>();
                    int index = -1;
                    IList listElement = (IList)obj;
                    GUILayout.BeginVertical("Box");
                    GUILayout.Label("List : Monsters", EditorStyles.boldLabel);
                    foreach (var encounter in listElement)
                    {
                        index++;
                        GUILayout.BeginHorizontal();
                        int selected = 0;
                        DungeonMonster encounterDungeon = (DungeonMonster)encounter;
                        for (int n = 0; n < encounterPartyDatabase.Count; n++)
                        {
                            if (encounterPartyDatabase.Get(n)._ID.Equals(encounterDungeon.monsterID))
                            {
                                selected = n;
                                break;
                            }
                        }
                        List<string> list = new List<string>();
                        for (int a = 0; a < encounterPartyDatabase.Count; a++)
                        {
                            list.Add(encounterPartyDatabase.Get(a)._ID.ToString());
                        }

                        GUILayout.BeginVertical();

                        GUILayout.Label("index " + index.ToString());
                        GUILayout.BeginHorizontal();
                        GUILayout.Label("Monster Party ID ");
                        selected = DropDownPopup("Monsters" + index.ToString() + nodeID, selected, list.ToArray(), (int)(windowWidth / 3f));
                        GUILayout.EndHorizontal();
                        GUILayout.BeginHorizontal();
                        GUILayout.Label("Probability multiplier ");
                        encounterDungeon.probabilityMultiplier = EditorGUILayout.IntField(encounterDungeon.probabilityMultiplier);
                        GUILayout.EndHorizontal();

                        GUILayout.EndVertical();

                        encounterDungeon.monsterID = encounterPartyDatabase.Get(selected)._ID;
                        listElement[index] = encounterDungeon;
                        int idx = DatabaseTools.DeleteThisIndex(listElement, index);
                        if (idx != -1)
                            deletedIndex.Add(idx);
                        GUILayout.EndHorizontal();
                    }
                    foreach (var ind in deletedIndex)
                    {
                        listElement.RemoveAt(ind);
                    }
                    if (GUILayout.Button("Add", GUILayout.Width(DatabaseState.BOTTOM_BUTTON_WIDTH)))
                    {
                        listElement.Add(new DungeonMonster());
                    }
                    GUILayout.EndVertical();
                }
            }
            if (field.FieldType == typeof(List<string>) && field.Name.Equals("ListWorldEventID"))
            {
                if(!obj.Equals(null))
                {
                    List<int> deletedIndex = new List<int>();
                    int index = -1;
                    IList listElement = (IList)obj;
                    GUILayout.BeginVertical("Box");
                    GUILayout.Label("List : Story",EditorStyles.boldLabel);
                    foreach(var story in (IList)obj)
                    {
                        index ++;
                        GUILayout.BeginHorizontal();
                        int selected = 0;
                        string storyItem = (string)story;
                        for (int n=0; n<storyData.Count; n++)
                        {
                            if(storyData.Get(n).ID.Equals(storyItem))
                            {
                                selected = n;
                                break;
                            }
                        }
                        List<string> list = new List<string>();
                        for(int a=0;a<storyData.Count;a++)
                        {
                            list.Add(storyData.Get(a).ID.ToString());
                        }
                        GUILayout.BeginVertical("Box");
                        selected = DropDownPopup("Story", selected, list.ToArray());
                        //selected = EditorGUILayout.Popup(selected,list.ToArray() ,GUILayout.Width(DatabaseState.DROP_DOWN_ENUM_WIDTH));
                        GUILayout.Label("World Trigger ID : " + storyData.Get(selected).Scene.WorldTriggerID);
                        GUILayout.Label("World Event : " + storyData.Get(selected).Scene.WorldEvent);
                        listElement[index] = storyData.Get(selected).ID;
                        int idx = DatabaseTools.DeleteThisIndex(listElement,index);
                        if(idx != -1)
                            deletedIndex.Add(idx);
                        GUILayout.EndVertical();
                        GUILayout.EndHorizontal();
                    }
                    foreach(var ind in deletedIndex)
                    {
                        listElement.RemoveAt(ind);
                    }
                    if(GUILayout.Button ("Add", GUILayout.Width(DatabaseState.BOTTOM_BUTTON_WIDTH)))
                    {
                        listElement.Add(storyData.Get(0).ID);
                    }
                    GUILayout.EndVertical();
                }
            }
            return obj;
        }
        public enum ParameterFormula {PARAMETER1,PARAMETER2,PARAMETER3};
        public object DropDownControl<T>(T data,FieldInfo field,object obj,string name,int currentIndex,int currentIndexList,int idIndexSearch)where T :IComponentDatabase//untuk scriptable database
        {
            if (obj == null)
                return obj;
            if (field.FieldType == typeof(WarArcanaDataModel) && field.Name.Equals("Arcana"))
            {
                if (!obj.Equals(null))
                {
                    WarArcanaDataModel arcana = (WarArcanaDataModel)obj;
                    GUILayout.BeginHorizontal();
                    GUILayout.Label("Arcana ", GUILayout.Width(DatabaseState.NAME_DETAIL_WIDTH));
                    int selected = 0;
                    for (int n = 0; n < warArcanaDataModelDatabase.Count; n++)
                    {
                        if (warArcanaDataModelDatabase.Get(n).ID.Equals(arcana.ID))
                        {
                            selected = n;
                            break;
                        }
                    }
                    List<string> list = new List<string>();
                    for (int a = 0; a < warArcanaDataModelDatabase.Count; a++)
                    {
                        list.Add(warArcanaDataModelDatabase.Get(a).ID.ToString() + ", " + warArcanaDataModelDatabase.Get(a).Name.ToString());
                    }
                    int tempIndex = DropDownPopup("Arcana" + idIndexSearch.ToString() + currentIndex.ToString() + currentIndexList.ToString(), selected, list.ToArray());
                    if (tempIndex != selected)
                        data.refreshElement();
                    selected = tempIndex;
                    //selected = EditorGUILayout.Popup(selected, list.ToArray(), GUILayout.Width(DatabaseState.DROP_DOWN_ENUM_WIDTH));
                    arcana = warArcanaDataModelDatabase.Get(selected);
                    GUILayout.EndHorizontal();
                    return arcana;
                }
            }
            else if (field.FieldType == typeof(WarBuff) && field.Name.Equals("AppliedBuff"))
            {
                if (!obj.Equals(null))
                {
                    WarBuff warBuff = (WarBuff)obj;
                    GUILayout.BeginHorizontal();
                    GUILayout.Label("AppliedBuff", GUILayout.Width(DatabaseState.NAME_DETAIL_WIDTH));
                    int selected = 0;
                    for (int n = 0; n < warBuffDatabase.Count; n++)
                    {
                        if (warBuffDatabase.Get(n).ID.Equals(warBuff.ID))
                        {
                            selected = n;
                            break;
                        }
                    }
                    List<string> list = new List<string>();
                    for (int a = 0; a < warBuffDatabase.Count; a++)
                    {
                        list.Add(warBuffDatabase.Get(a).ID.ToString() + ", " + warBuffDatabase.Get(a).Name.ToString());
                    }
                    int tempIndex = DropDownPopup("WarBuff" + idIndexSearch.ToString() + currentIndex.ToString() + currentIndexList.ToString(), selected, list.ToArray());
                    if (tempIndex != selected)
                        data.refreshElement();
                    selected = tempIndex;
                    //selected = EditorGUILayout.Popup(selected, list.ToArray(), GUILayout.Width(DatabaseState.DROP_DOWN_ENUM_WIDTH));
                    warBuff = warBuffDatabase.Get(selected);
                    GUILayout.EndHorizontal();
                    return warBuff;
                }
            }
            else if (field.FieldType == typeof(List<WarArcanaEffect>) && field.Name.Equals("ArcanaEffects"))
            {
                if (!obj.Equals(null))
                {
                    List<int> deletedIndex = new List<int>();
                    int index = -1;
                    IList listElement = (IList)obj;
                    GUILayout.BeginVertical("Box");
                    GUILayout.Label("List : ArcanaEffects");
                    foreach (var arcanaEffect in (IList)obj)
                    {
                        index++;
                        GUILayout.BeginHorizontal();
                        int selected = 0;
                        WarArcanaEffect arcanaE = (WarArcanaEffect)arcanaEffect;
                        for (int n = 0; n < warArcanaEffectDatabase.Count; n++)
                        {
                            if (warArcanaEffectDatabase.Get(n).type.Equals(arcanaE.type))
                            {
                                selected = n;
                                break;
                            }
                        }
                        List<string> list = new List<string>();
                        for (int a = 0; a < warArcanaEffectDatabase.Count; a++)
                        {
                            list.Add(warArcanaEffectDatabase.Get(a).type.ToString());
                        }
                        GUILayout.Label(index.ToString(), GUILayout.Width(DatabaseState.NAME_DETAIL_WIDTH));
                        int tempIndex = DropDownPopup("Arcana Effects" + index.ToString() + idIndexSearch.ToString() + currentIndex.ToString() + currentIndexList.ToString(), selected, list.ToArray());
                        if (tempIndex != selected)
                            data.refreshElement();
                        selected = tempIndex;
                        //selected = EditorGUILayout.Popup(selected,list.ToArray() ,GUILayout.Width(DatabaseState.DROP_DOWN_ENUM_WIDTH));
                        listElement[index] = warArcanaEffectDatabase.Get(selected);
                        int idx = DatabaseTools.DeleteThisIndex(listElement, index);
                        if (idx != -1)
                            deletedIndex.Add(idx);
                        data.refreshElement(false);
                        GUILayout.EndHorizontal();
                    }
                    foreach (var ind in deletedIndex)
                    {
                        listElement.RemoveAt(ind);
                        data.refreshElement();
                    }
                    AddList<T>(listElement, data, index);
                    GUILayout.EndVertical();
                }
            }
            else if (field.FieldType == typeof(EquipmentDataModel) && field.Name.Equals("Equipment"))
            {
                if (!obj.Equals(null))
                {
                    EquipmentDataModel equipment = (EquipmentDataModel)obj;
                    GUILayout.BeginHorizontal();
                    GUILayout.Label("Equipment ", GUILayout.Width(DatabaseState.NAME_DETAIL_WIDTH));
                    int selected = 0;
                    for (int n = 0; n < EquipmentsData.Count; n++)
                    {
                        if (EquipmentsData.Get(n).ID.Equals(equipment.ID))
                        {
                            selected = n;
                            break;
                        }
                    }
                    List<string> list = new List<string>();
                    for (int a = 0; a < EquipmentsData.Count; a++)
                    {
                        list.Add(EquipmentsData.Get(a).ID.ToString() + ", " + EquipmentsData.Get(a).Name.ToString());
                    }
                    int tempIndex = DropDownPopup("Equipment" + idIndexSearch.ToString() + currentIndex.ToString() + currentIndexList.ToString(), selected, list.ToArray());
                    if (tempIndex != selected)
                        data.refreshElement();
                    selected = tempIndex;
                    //selected = EditorGUILayout.Popup(selected, list.ToArray(), GUILayout.Width(DatabaseState.DROP_DOWN_ENUM_WIDTH));
                    equipment = EquipmentsData.Get(selected);
                    GUILayout.EndHorizontal();
                    return equipment;
                }
            }
            else if (field.FieldType == typeof(List<Magic>) && field.Name.Equals("EncounterMagics"))
            {
                if (!obj.Equals(null))
                {
                    List<int> deletedIndex = new List<int>();
                    int index = -1;
                    IList listElement = (IList)obj;
                    GUILayout.BeginVertical("Box");
                    GUILayout.Label("List : Magic", EditorStyles.boldLabel);
                    foreach (var magicc in (IList)obj)
                    {
                        index++;
                        GUILayout.BeginHorizontal();
                        int selected = 0;
                        Magic magicItem = (Magic)magicc;
                        for (int n = 0; n < MagicData.Count; n++)
                        {
                            if (MagicData.Get(n).ID.Equals(magicItem.ID))
                            {
                                selected = n;
                                break;
                            }
                        }
                        List<string> list = new List<string>();
                        for (int a = 0; a < MagicData.Count; a++)
                        {
                            list.Add(MagicData.Get(a).Name.ToString());
                        }
                        GUILayout.Label(index.ToString(), GUILayout.Width(DatabaseState.NAME_DETAIL_WIDTH));
                        int tempIndex = DropDownPopup("Magic" + index.ToString() + idIndexSearch.ToString() + currentIndex.ToString() + currentIndexList.ToString(), selected, list.ToArray());
                        if (tempIndex != selected)
                            data.refreshElement();
                        selected = tempIndex;
                        //selected = EditorGUILayout.Popup(selected, list.ToArray(), GUILayout.Width(DatabaseState.DROP_DOWN_ENUM_WIDTH));
                        listElement[index] = MagicData.Get(selected);
                        int idx = DatabaseTools.DeleteThisIndex(listElement, index);
                        if (idx != -1)
                            deletedIndex.Add(idx);
                        data.refreshElement(false);
                        GUILayout.EndHorizontal();
                    }
                    foreach (var ind in deletedIndex)
                    {
                        listElement.RemoveAt(ind);
                        data.refreshElement();
                    }
                    AddList<T>(listElement, data, index);
                    GUILayout.EndVertical();
                }
            }
            else if (field.FieldType == typeof(Magic) && field.Name.Equals("Magic"))
            {
                if (!obj.Equals(null))
                {
                    Magic magic = (Magic)obj;
                    GUILayout.BeginHorizontal();
                    GUILayout.Label("Magic ",GUILayout.Width(DatabaseState.NAME_DETAIL_WIDTH));
                    int selected = 0;
                    for (int n = 0; n < MagicData.Count; n++)
                    {
                        if (MagicData.Get(n).Name.Equals(magic.Name))
                        {
                            selected = n;
                            break;
                        }
                    }
                    List<string> list = new List<string>();
                    for (int a = 0; a < MagicData.Count; a++)
                    {
                        list.Add(MagicData.Get(a).ID.ToString() + ", " + MagicData.Get(a).Name.ToString());
                    }
                    int tempIndex = DropDownPopup("Magic" + idIndexSearch.ToString() + currentIndex.ToString() + currentIndexList.ToString(), selected, list.ToArray());
                    if (tempIndex != selected)
                        data.refreshElement();
                    selected = tempIndex;
                    //selected = EditorGUILayout.Popup(selected, list.ToArray(), GUILayout.Width(DatabaseState.DROP_DOWN_ENUM_WIDTH));
                    magic = MagicData.Get(selected);
                    GUILayout.EndHorizontal();
                    return magic;
                }
            }
            else if (field.FieldType == typeof(OffensiveBattleSkill) && field.Name.Equals("SkillUltimateOffensive"))
            {
                if (!obj.Equals(null))
                {
                    OffensiveBattleSkill skill = (OffensiveBattleSkill)obj;
                    GUILayout.BeginHorizontal();
                    GUILayout.Label("Offensive Skill ", GUILayout.Width(DatabaseState.NAME_DETAIL_WIDTH));
                    int selected = 0;
                    for (int n = 0; n < offensiveSkillDatabase.Count; n++)
                    {
                        if (offensiveSkillDatabase.Get(n).Name.Equals(skill.Name))
                        {
                            selected = n;
                            break;
                        }
                    }
                    List<string> list = new List<string>();
                    for (int a = 0; a < offensiveSkillDatabase.Count; a++)
                    {
                        list.Add(offensiveSkillDatabase.Get(a).Name.ToString());
                    }
                    int tempIndex = DropDownPopup("Offensive Skill" + idIndexSearch.ToString() + currentIndex.ToString() + currentIndexList.ToString(), selected, list.ToArray());
                    if (tempIndex != selected)
                        data.refreshElement();
                    selected = tempIndex;
                    //selected = EditorGUILayout.Popup(selected, list.ToArray(), GUILayout.Width(DatabaseState.DROP_DOWN_ENUM_WIDTH));
                    skill = offensiveSkillDatabase.Get(selected);
                    GUILayout.EndHorizontal();
                    return skill;
                }
            }
            else if (field.FieldType == typeof(List<OffensiveBattleSkill>) && field.Name.Equals("OffensiveBattleSkills"))
            {
                if (!obj.Equals(null))
                {
                    List<int> deletedIndex = new List<int>();
                    int index = -1;
                    IList listElement = (IList)obj;
                    GUILayout.BeginVertical("Box");
                    GUILayout.Label("List : Offensive Skill");
                    foreach (var skill in (IList)obj)
                    {
                        index++;
                        GUILayout.BeginHorizontal();
                        int selected = 0;
                        OffensiveBattleSkill offensiveSkill = (OffensiveBattleSkill)skill;
                        for (int n = 0; n < offensiveSkillDatabase.Count; n++)
                        {
                            if (offensiveSkillDatabase.Get(n).IdSkill.Equals(offensiveSkill.IdSkill))
                            {
                                selected = n;
                                break;
                            }
                        }
                        List<string> list = new List<string>();
                        for (int a = 0; a < offensiveSkillDatabase.Count; a++)
                        {
                            list.Add(offensiveSkillDatabase.Get(a).Name.ToString());
                        }
                        GUILayout.Label(index.ToString(), GUILayout.Width(DatabaseState.NAME_DETAIL_WIDTH));
                        int tempIndex = DropDownPopup("Offensive Skills" + index.ToString() + idIndexSearch.ToString() + currentIndex.ToString() + currentIndexList.ToString(), selected, list.ToArray());
                        if (tempIndex != selected)
                            data.refreshElement();
                        selected = tempIndex;
                        //selected = EditorGUILayout.Popup(selected,list.ToArray() ,GUILayout.Width(DatabaseState.DROP_DOWN_ENUM_WIDTH));
                        listElement[index] = offensiveSkillDatabase.Get(selected);
                        int idx = DatabaseTools.DeleteThisIndex(listElement, index);
                        if (idx != -1)
                            deletedIndex.Add(idx);
                        data.refreshElement(false);
                        GUILayout.EndHorizontal();
                    }
                    foreach (var ind in deletedIndex)
                    {
                        listElement.RemoveAt(ind);
                        data.refreshElement();
                    }
                    AddList<T>(listElement, data, index);
                    GUILayout.EndVertical();
                }
            }
            else if (field.FieldType == typeof(DefensiveBattleSkill) && field.Name.Equals("SkillUltimateDefensive"))
            {
                if (!obj.Equals(null))
                {
                    DefensiveBattleSkill skill = (DefensiveBattleSkill)obj;
                    GUILayout.BeginHorizontal();
                    GUILayout.Label("Defensive Skill ",GUILayout.Width(DatabaseState.NAME_DETAIL_WIDTH));
                    int selected = 0;
                    for (int n = 0; n < defensiveSkillDatabase.Count; n++)
                    {
                        if (defensiveSkillDatabase.Get(n).Name.Equals(skill.Name))
                        {
                            selected = n;
                            break;
                        }
                    }
                    List<string> list = new List<string>();
                    for (int a = 0; a < defensiveSkillDatabase.Count; a++)
                    {
                        list.Add(defensiveSkillDatabase.Get(a).Name.ToString());
                    }
                    int tempIndex = DropDownPopup("Defensive Skill" + idIndexSearch.ToString() + currentIndex.ToString() + currentIndexList.ToString(), selected, list.ToArray());
                    if (tempIndex != selected)
                        data.refreshElement();
                    selected = tempIndex;
                    //selected = EditorGUILayout.Popup(selected, list.ToArray(), GUILayout.Width(DatabaseState.DROP_DOWN_ENUM_WIDTH));
                    skill = defensiveSkillDatabase.Get(selected);
                    GUILayout.EndVertical();
                    return skill;
                }
            }
            else if (field.FieldType == typeof(NormalSkill) && field.Name.Equals("Skill"))
            {
                if (!obj.Equals(null))
                {
                    NormalSkill skill = (NormalSkill)obj;
                    GUILayout.BeginHorizontal();
                    GUILayout.Label("Normal Skill",GUILayout.Width(DatabaseState.NAME_DETAIL_WIDTH));
                    int selected = 0;
                    for (int n = 0; n < normalSkillDatabase.Count; n++)
                    {
                        if (normalSkillDatabase.Get(n).Name.Equals(skill.Name))
                        {
                            selected = n;
                            break;
                        }
                    }
                    List<string> list = new List<string>();
                    for (int a = 0; a < normalSkillDatabase.Count; a++)
                    {
                        list.Add(normalSkillDatabase.Get(a).Name.ToString());
                    }
                    int tempIndex = DropDownPopup("Normal Skill" + idIndexSearch.ToString() + currentIndex.ToString() + currentIndexList.ToString(), selected, list.ToArray());
                    if (tempIndex != selected)
                        data.refreshElement();
                    selected = tempIndex;
                    //selected = EditorGUILayout.Popup(selected, list.ToArray(), GUILayout.Width(DatabaseState.DROP_DOWN_ENUM_WIDTH));
                    skill = normalSkillDatabase.Get(selected);
                    GUILayout.EndVertical();
                    return skill;
                }
            }
            else if (field.FieldType == typeof(GuardBuff) && field.Name.Equals("PassiveGuardSkill"))
            {
                if (!obj.Equals(null))
                {
                    GuardBuff skill = (GuardBuff)obj;
                    GUILayout.BeginHorizontal();
                    GUILayout.Label("Passive Skill ",GUILayout.Width(DatabaseState.NAME_DETAIL_WIDTH));
                    int selected = 0;
                    for (int n = 0; n < guardBuffDatabase.Count; n++)
                    {
                        if (guardBuffDatabase.Get(n).Name.Equals(skill.Name))
                        {
                            selected = n;
                            break;
                        }
                    }
                    List<string> list = new List<string>();
                    for (int a = 0; a < guardBuffDatabase.Count; a++)
                    {
                        list.Add(guardBuffDatabase.Get(a).Name.ToString());
                    }
                    int tempIndex = DropDownPopup("Passive Skill" + idIndexSearch.ToString() + currentIndex.ToString() + currentIndexList.ToString(), selected, list.ToArray());
                    if (tempIndex != selected)
                        data.refreshElement();
                    selected = tempIndex;
                    //selected = EditorGUILayout.Popup(selected, list.ToArray(), GUILayout.Width(DatabaseState.DROP_DOWN_ENUM_WIDTH));
                    skill = guardBuffDatabase.Get(selected);
                    GUILayout.EndHorizontal();
                    return skill;
                }
            }
            else if(field.FieldType == typeof(List<Buff>))
            {
                if(!obj.Equals(null))
                {
                    List<int> deletedIndex = new List<int>();
                    int index = -1;
                    IList listElement = (IList)obj;
                    GUILayout.BeginVertical("Box");
                    GUILayout.Label("List : Buff");
                    foreach(var buff in (IList)obj)
                    {
                        index ++;
                        GUILayout.BeginHorizontal();
                        int selected = 0;
                        Buff buffItem = (Buff)buff;
                        for (int n=0; n<buffDatabase.Count; n++)
                        {
                            if(buffDatabase.Get(n)._ID.Equals(buffItem._ID))
                            {
                                selected = n;
                                break;
                            }
                        }
                        List<string> list = new List<string>();
                        for(int a=0;a<buffDatabase.Count;a++)
                        {
                            list.Add(buffDatabase.Get(a).Name.ToString());
                        }
                        GUILayout.Label(index.ToString(),GUILayout.Width(DatabaseState.NAME_DETAIL_WIDTH));
                        int tempIndex = DropDownPopup("Buff" + index.ToString() + idIndexSearch.ToString() + currentIndex.ToString() + currentIndexList.ToString(), selected, list.ToArray());
                        if (tempIndex != selected)
                            data.refreshElement();
                        selected = tempIndex;
                        //selected = EditorGUILayout.Popup(selected,list.ToArray() ,GUILayout.Width(DatabaseState.DROP_DOWN_ENUM_WIDTH));
                        listElement[index] = buffDatabase.Get(selected);
                        int idx = DatabaseTools.DeleteThisIndex(listElement,index);
                        if(idx != -1)
                            deletedIndex.Add(idx);
                        data.refreshElement(false);
                        GUILayout.EndHorizontal();
                    }
                    foreach(var ind in deletedIndex)
                    {
                        listElement.RemoveAt(ind);
                        data.refreshElement();
                    }
                    AddList<T>(listElement,data,index);
                    GUILayout.EndVertical();
                }
            }
            else if(field.FieldType == typeof(List<EncounterGrowth>))
            {
                if(!obj.Equals(null))
                {
                    List<int> deletedIndex = new List<int>();
                    int index = -1;
                    IList listElement = (IList)obj;
                    GUILayout.BeginVertical("Box");
                    GUILayout.Label("List : EncounterGrowth");
                    foreach(var encounterGrowth in (IList)obj)
                    {
                        index ++;
                        GUILayout.BeginHorizontal();
                        int selected = 0;
                        EncounterGrowth encounterOBJ = (EncounterGrowth)encounterGrowth;
                        for (int n=0; n<encounterGrowthDatabase.Count; n++)
                        {
                            if(encounterGrowthDatabase.Get(n).Name.Equals(encounterOBJ.Name))
                            {
                                selected = n;
                                break;
                            }
                        }
                        List<string> list = new List<string>();
                        for(int a=0;a<encounterGrowthDatabase.Count;a++)
                        {
                            list.Add(encounterGrowthDatabase.Get(a).Name.ToString());
                        }
                        GUILayout.Label(index.ToString(),GUILayout.Width(DatabaseState.NAME_DETAIL_WIDTH));
                        int tempIndex = DropDownPopup("EncounterGrowth" + index.ToString() + idIndexSearch.ToString() + currentIndex.ToString() + currentIndexList.ToString(), selected, list.ToArray());
                        if (tempIndex != selected)
                            data.refreshElement();
                        selected = tempIndex;
                        //selected = EditorGUILayout.Popup(selected,list.ToArray() ,GUILayout.Width(DatabaseState.DROP_DOWN_ENUM_WIDTH));
                        listElement[index] = encounterGrowthDatabase.Get(selected);
                        int idx = DatabaseTools.DeleteThisIndex(listElement,index);
                        if(idx != -1)
                            deletedIndex.Add(idx);
                        GUILayout.EndHorizontal();
                        data.refreshElement(false);
                    }
                    foreach(var ind in deletedIndex)
                    {
                        listElement.RemoveAt(ind);
                        data.refreshElement();
                    }
                    AddList<T>(listElement,data,index);
                    GUILayout.EndVertical();
                }
            }
            else if(obj.GetType() == typeof(List<string>))
            {
                if (name.Equals("OffensiveSkillData"))
                {
                    if (!obj.Equals(null))
                    {
                        List<int> deletedIndex = new List<int>();
                        int index = -1;
                        IList listElement = (IList)obj;
                        GUILayout.BeginVertical("Box");
                        GUILayout.Label("List : Offensive Skill",EditorStyles.boldLabel);
                        foreach(var dataa in (IList)obj)
                        {
                            index ++;
                            GUILayout.BeginHorizontal();
                            int selected = 0;
                            for (int n=0; n<offensiveSkillDatabase.Count; n++)
                            {
                                if(offensiveSkillDatabase.Get(n).Name.Equals((string)dataa))
                                {
                                    selected = n;
                                    break;
                                }
                            }
                            List<string> list = new List<string>();
                            for(int a=0;a<offensiveSkillDatabase.Count;a++)
                            {
                                list.Add(offensiveSkillDatabase.Get(a).Name.ToString());
                            }
                            GUILayout.Label(index.ToString(),GUILayout.Width(DatabaseState.NAME_DETAIL_WIDTH));
                            int tempIndex = DropDownPopup("Offensive Skill" + index.ToString() + idIndexSearch.ToString() + currentIndex.ToString() + currentIndexList.ToString(), selected, list.ToArray());
                            if (tempIndex != selected)
                                data.refreshElement();
                            selected = tempIndex;
                            //selected = EditorGUILayout.Popup(selected,list.ToArray() ,GUILayout.Width(DatabaseState.DROP_DOWN_ENUM_WIDTH));
                            listElement[index] = offensiveSkillDatabase.Get(selected).Name;
                            int idx = DatabaseTools.DeleteThisIndex(listElement,index);
                            if(idx != -1)
                                deletedIndex.Add(idx);
                            GUILayout.EndHorizontal();
                            data.refreshElement(false);
                        }
                        foreach(var ind in deletedIndex)
                        {
                            listElement.RemoveAt(ind);
                            data.refreshElement();
                        }
                        AddList<T>(listElement,data,index);
                        GUILayout.EndVertical();
                    }
                }
				else if (name.Equals ("DefensiveSkillData"))
				{
					if (!obj.Equals(null))
					{
                        List<int> deletedIndex = new List<int>();
                        int index = -1;
                        IList listElement = (IList)obj;
                        GUILayout.BeginVertical("Box");
                        GUILayout.Label("List : Defense Skill",EditorStyles.boldLabel);
                        foreach(var dataa in (IList)obj)
                        {
                            index ++;
                            GUILayout.BeginHorizontal();
                            int selected = 0;
                            for (int n=0; n<defensiveSkillDatabase.Count; n++)
                            {
                                if(defensiveSkillDatabase.Get(n).Name.Equals((string)dataa))
                                {
                                    selected = n;
                                    break;
                                }
                            }
                            List<string> list = new List<string>();
                            for(int a=0;a<defensiveSkillDatabase.Count;a++)
                            {
                                list.Add(defensiveSkillDatabase.Get(a).Name.ToString());
                            }
                            GUILayout.Label(index.ToString(),GUILayout.Width(DatabaseState.NAME_DETAIL_WIDTH));
                            int tempIndex = DropDownPopup("Defense Skill" + index.ToString() + idIndexSearch.ToString() + currentIndex.ToString() + currentIndexList.ToString(), selected, list.ToArray());
                            if (tempIndex != selected)
                                data.refreshElement();
                            selected = tempIndex;
                            //selected = EditorGUILayout.Popup(selected,list.ToArray() ,GUILayout.Width(DatabaseState.DROP_DOWN_ENUM_WIDTH));
                            listElement[index] = defensiveSkillDatabase.Get(selected).Name;
                            int idx = DatabaseTools.DeleteThisIndex(listElement,index);
                            if(idx != -1)
                                deletedIndex.Add(idx);
                            GUILayout.EndHorizontal();
                            data.refreshElement(false);
                        }
                        foreach(var ind in deletedIndex)
                        {
                            listElement.RemoveAt(ind);
                            data.refreshElement();
                        }
                        AddList<T>(listElement,data,index);
                        GUILayout.EndVertical();
					}
				}
				else if (name.Equals ("CombinationSkillData"))
				{
					if (!obj.Equals(null))
					{
                        List<int> deletedIndex = new List<int>();
                        int index = -1;
                        IList listElement = (IList)obj;
                        GUILayout.BeginVertical("Box");
                        GUILayout.Label("List : Combination Skill",EditorStyles.boldLabel);
                        foreach(var dataa in (IList)obj)
                        {
                            index ++;
                            GUILayout.BeginHorizontal();
                            int selected = 0;
                            for (int n=0; n<combinationSkillDatabase.Count; n++)
                            {
                                if(combinationSkillDatabase.Get(n).Name.Equals((string)dataa))
                                {
                                    selected = n;
                                    break;
                                }
                            }
                            List<string> list = new List<string>();
                            for(int a=0;a<combinationSkillDatabase.Count;a++)
                            {
                                list.Add(combinationSkillDatabase.Get(a).Name.ToString());
                            }
                            GUILayout.Label(index.ToString(),GUILayout.Width(DatabaseState.NAME_DETAIL_WIDTH));
                            int tempIndex = DropDownPopup("Combination Skill" + index.ToString() + idIndexSearch.ToString() + currentIndex.ToString() + currentIndexList.ToString(), selected, list.ToArray());
                            if (tempIndex != selected)
                                data.refreshElement();
                            selected = tempIndex;
                            //selected = EditorGUILayout.Popup(selected,list.ToArray() ,GUILayout.Width(DatabaseState.DROP_DOWN_ENUM_WIDTH));
                            listElement[index] = combinationSkillDatabase.Get(selected).Name;
                            int idx = DatabaseTools.DeleteThisIndex(listElement,index);
                            if(idx != -1)
                                deletedIndex.Add(idx);
                            GUILayout.EndHorizontal();
                            data.refreshElement(false);
                        }
                        foreach(var ind in deletedIndex)
                        {
                            listElement.RemoveAt(ind);
                            data.refreshElement();
                        }
                        AddList<T>(listElement,data,index);
                        GUILayout.EndVertical();
					}
				}
                else if (name.Equals("NormalSkillData"))
                {
                    if (!obj.Equals(null))
                    {
                        List<int> deletedIndex = new List<int>();
                        int index = -1;
                        IList listElement = (IList)obj;
                        GUILayout.BeginVertical("Box");
                        GUILayout.Label("List : Normal Skill",EditorStyles.boldLabel);
                        foreach(var dataa in (IList)obj)
                        {
                            index ++;
                            GUILayout.BeginHorizontal();
                            int selected = 0;
                            for (int n=0; n<normalSkillDatabase.Count; n++)
                            {
                                if(normalSkillDatabase.Get(n).Name.Equals((string)dataa))
                                {
                                    selected = n;
                                    break;
                                }
                            }
                            List<string> list = new List<string>();
                            for(int a=0;a<normalSkillDatabase.Count;a++)
                            {
                                list.Add(normalSkillDatabase.Get(a).Name.ToString());
                            }
                            GUILayout.Label(index.ToString(),GUILayout.Width(DatabaseState.NAME_DETAIL_WIDTH));
                            int tempIndex = DropDownPopup("Normal Skill" + index.ToString() + idIndexSearch.ToString() + currentIndex.ToString() + currentIndexList.ToString(), selected, list.ToArray());
                            if (tempIndex != selected)
                                data.refreshElement();
                            selected = tempIndex;
                            //selected = EditorGUILayout.Popup(selected,list.ToArray() ,GUILayout.Width(DatabaseState.DROP_DOWN_ENUM_WIDTH));
                            listElement[index] = normalSkillDatabase.Get(selected).Name;
                            int idx = DatabaseTools.DeleteThisIndex(listElement,index);
                            if(idx != -1)
                                deletedIndex.Add(idx);
                            GUILayout.EndHorizontal();
                            data.refreshElement(false);
                        }
                        foreach(var ind in deletedIndex)
                        {
                            listElement.RemoveAt(ind);
                            data.refreshElement();
                        }
                        AddList<T>(listElement,data,index);
                        GUILayout.EndVertical();
                    }
                }
				else if (name.Equals ("GuardBuffData"))
				{
					if (!obj.Equals(null))
					{
                        List<int> deletedIndex = new List<int>();
                        int index = -1;
                        IList listElement = (IList)obj;
                        GUILayout.BeginVertical("Box");
                        GUILayout.Label("List : Guard Buff",EditorStyles.boldLabel);
                        foreach(var dataa in (IList)obj)
                        {
                            index ++;
                            GUILayout.BeginHorizontal();
                            int selected = 0;
                            for (int n=0; n<guardBuffDatabase.Count; n++)
                            {
                                if(guardBuffDatabase.Get(n).Name.Equals((string)dataa))
                                {
                                    selected = n;
                                    break;
                                }
                            }
                            List<string> list = new List<string>();
                            for(int a=0;a<guardBuffDatabase.Count;a++)
                            {
                                list.Add(guardBuffDatabase.Get(a).Name.ToString());
                            }
                            GUILayout.Label(index.ToString(),GUILayout.Width(DatabaseState.NAME_DETAIL_WIDTH));
                            int tempIndex = DropDownPopup("Guard Buff" + index.ToString() + idIndexSearch.ToString() + currentIndex.ToString() + currentIndexList.ToString(), selected, list.ToArray());
                            if (tempIndex != selected)
                                data.refreshElement();
                            selected = tempIndex;
                            //selected = EditorGUILayout.Popup(selected,list.ToArray() ,GUILayout.Width(DatabaseState.DROP_DOWN_ENUM_WIDTH));
                            listElement[index] = guardBuffDatabase.Get(selected).Name;
                            int idx = DatabaseTools.DeleteThisIndex(listElement,index);
                            if(idx != -1)
                                deletedIndex.Add(idx);
                            GUILayout.EndHorizontal();
                            data.refreshElement(false);
                        }
                        foreach(var ind in deletedIndex)
                        {
                            listElement.RemoveAt(ind);
                            data.refreshElement();
                        }
                        AddList<T>(listElement,data,index);
                        GUILayout.EndVertical();
					}
				}
                else if (name.Equals("ActiveGameObjects") || name.Equals("InactiveGameObjects"))
                {
                    if (!obj.Equals(null))
                    {
                        List<int> deletedIndex = new List<int>();
                        GUILayout.BeginVertical("Box");
                        List<string> objList = (List<string>)obj;
                        GUILayout.Label("List : " + name, EditorStyles.boldLabel);
                        int index = 0;
                        foreach (string objName in objList)
                        {
                            GUILayout.BeginHorizontal();
                            string key = currentIndex.ToString() + currentIndexList.ToString();
                            List<string> option = listAndIndexController.Get(key);
                            string IDindex = currentIndex.ToString() + index.ToString() + currentIndexList.ToString();
                            if (name.Equals("ActiveGameObjects"))
                                IDindex = "a" + IDindex;
                            else
                                IDindex = "b" + IDindex;
                            if (option.Count > 0)
                            {
                                int selected = listAndIndexController.GetIndex(IDindex);
                                if (selected == 0)
                                {
                                    for (int i = 0; i < option.Count; i++)
                                    {
                                        if(option[i].Equals((objName)))
                                            selected = i;
                                    }
                                }
                                //listAndIndexController.SetIndex(IDindex, EditorGUILayout.Popup(selected, option.ToArray()));
                                int tempIndex = DropDownPopup(name + index.ToString() + idIndexSearch.ToString() + currentIndex.ToString() + currentIndexList.ToString(), selected, option.ToArray());
                                if (tempIndex != selected)
                                    data.refreshElement();
                                selected = tempIndex;
                                listAndIndexController.SetIndex(IDindex, selected);
                            }
                            else
                            {
                                GUILayout.BeginVertical("Box");
                                GUILayout.BeginHorizontal();
                                GUILayout.Label("index " + index.ToString(), GUILayout.Width(DatabaseState.NAME_DETAIL_WIDTH));
                                objList[index] = EditorGUILayout.TextField(objList[index]);
                                GUILayout.EndHorizontal();
                                GUILayout.EndVertical();
                            }
                            int idx = DatabaseTools.DeleteThisIndex(objList,index);
                            if(idx != -1)
                                deletedIndex.Add(idx);
                            GUILayout.EndHorizontal();
                            if(option.Count > listAndIndexController.GetIndex(IDindex))
                                objList[index] = option[listAndIndexController.GetIndex(IDindex)];
                            data.refreshElement(false);
                            index++;
                        }
                        foreach(var ind in deletedIndex)
                        {
                            for (int i = ind + 1; i < objList.Count; i++)
                            {
                                string IDindex = currentIndex.ToString() + i.ToString() + currentIndexList.ToString();
                                if (name.Equals("activeGameObjects"))
                                    IDindex = "a" + IDindex;
                                else
                                    IDindex = "b" + IDindex;
                                int state = listAndIndexController.GetIndex(IDindex);
                                IDindex = currentIndex.ToString() + (i-1).ToString() + currentIndexList.ToString();
                                if (name.Equals("activeGameObjects"))
                                    IDindex = "a" + IDindex;
                                else
                                    IDindex = "b" + IDindex;
                                listAndIndexController.SetIndex(IDindex, state);
                            }
                            objList.RemoveAt(ind);
                            data.refreshElement();
                        }
                        GUILayout.BeginHorizontal();
                        if(GUILayout.Button ("Add", GUILayout.Width(DatabaseState.BOTTOM_BUTTON_WIDTH)))
                        {
                            objList.Add("");
                            data.refresh();
                        }
                        GUILayout.EndHorizontal();
                        GUILayout.EndVertical();
                    }
                }
                else if(name.Equals("AllItemID"))
                {
                    if(!obj.Equals(null))
                    {
                        IList listElement = (IList)obj;
                        GUILayout.BeginVertical("Box");
                        GUILayout.Label("List : Item");

                        for(int c=0;c<listElement.Count;c++)
                        {
                            GUILayout.BeginHorizontal();
                            listElement[c] = AllItem<T>(data,listElement[c],c.ToString(),idIndexSearch.ToString() + c.ToString());
                            data.refreshElement(false);
                            if (GUILayout.Button ("Delete", GUILayout.Width(DatabaseState.BOTTOM_BUTTON_WIDTH),GUILayout.Height(DatabaseState.DELETE_LIST_BUTTON_HEIGHT)))
                            {
                                if(EditorUtility.DisplayDialog("Delete","Do you want to delete index " + c.ToString() + " ?","Yes","No"))
                                {
                                    listElement.RemoveAt(c);
                                }
                            }
                            GUILayout.EndHorizontal();
                        }
                        AddList<T>(listElement,data,listElement.Count - 1);
                        GUILayout.EndVertical();
                    }
                }
            }
            else if(obj.GetType() == typeof(string))
            {
                if (name.Equals("Owner"))
                {
                    string ObjectName = (string)obj;
                    string ButtonText = ObjectName;
                    if (ButtonText.Equals(""))
                        ButtonText = "Select Object";
                    if (GUILayout.Button(ButtonText))
                    {
                        EditorGUIUtility.ShowObjectPicker<GameObject>(null, true, null, currentIndex + 1000);
                    }
                    List<string> option = new List<string>();
                    string commandName = Event.current.commandName;
                    string key = currentIndex.ToString() + currentIndexList.ToString();
                    if (listAndIndexController.Get(key).Count == 0 && ObjectName != "")
                    {
                        option.Clear();
                        UnityEngine.Object parent = new UnityEngine.Object();
                        GetGameObject.GetGameObjectInSubDir(ObjectName, "Assets/Resources/Worlds/Area",ref parent);
                        if (parent != null)
                        {
                            GetGameObject.GetChildsName((GameObject)parent, option);
                            key = currentIndex.ToString() + currentIndexList.ToString();
                            listAndIndexController.Set(key, option);
                        }
                    }
                    if(commandName == "ObjectSelectorUpdated" && EditorGUIUtility.GetObjectPickerControlID() == currentIndex + 1000)
                    {
                        option.Clear();
                        GameObject parent = (GameObject)EditorGUIUtility.GetObjectPickerObject();
                        ObjectName = parent.name;
                        GetGameObject.GetChildsName(parent, option);
                        key = currentIndex.ToString() + currentIndexList.ToString();
                        listAndIndexController.Set(key, option);
                    }
                    return ObjectName;
                }
                else if(name.Equals("AreaName"))
                {
                    string AreaName = (string)obj;
                    GUILayout.BeginHorizontal();
                    GUILayout.Label("Area ", GUILayout.Width(DatabaseState.NAME_DETAIL_WIDTH));
                    int selected = 0;
                    for (int n = 0; n < MapAreaDatabase.Count; n++)
                    {
                        if (MapAreaDatabase.Get(n).areaName.Equals(AreaName))
                        {
                            selected = n;
                            break;
                        }
                    }
                    List<string> list = new List<string>();
                    for (int a = 0; a < MapAreaDatabase.Count; a++)
                    {
                        list.Add(MapAreaDatabase.Get(a).areaName.ToString());
                    }
                    int tempIndex = DropDownPopup("AreaName" + idIndexSearch.ToString() + currentIndex.ToString() + currentIndexList.ToString(), selected, list.ToArray());
                    if (tempIndex != selected)
                        data.refreshElement();
                    selected = tempIndex;
                    //selected = EditorGUILayout.Popup(selected, list.ToArray(), GUILayout.Width(DatabaseState.DROP_DOWN_ENUM_WIDTH));
                    AreaName = MapAreaDatabase.Get(selected).areaName;
                    GUILayout.EndHorizontal();
                    return AreaName;
                }
                else if (name.Equals("CastingMagicEffect") || name.Equals("AttackMagicEffect") || name.Equals("HitEffect") || name.Equals("ItemParticle") || name.Equals("NormalSkillParticle") || name.Equals("BattlerScript"))
                {
                    if(!obj.Equals(null))
                    {
                        int selected = 0;
                        int selectedOption = 0;
                        string selectedID = (string)obj;
                        List<string> option = new List<string>();
                        for (int n=0; n<battleEffectOption.Count; n++)
                        {
                            if (battleEffectOption.Get(n).Name.Equals(name))
                            {
                                selectedOption = n;
                                break;
                            }
                        }
                        for (int n=0; n<battleEffectOption.Get(selectedOption).Option.Count; n++)
                        {
                            string opt = battleEffectOption.Get(selectedOption).Option[n];
                            string[] res = opt.Split('/');
                            option.Add(res[res.Length - 1]);
                            if(battleEffectOption.Get(selectedOption).Option[n].Equals(selectedID))
                            {
                                selected = n;
                            }
                        }

                        GUILayout.Label(name,GUILayout.Width(DatabaseState.NAME_DETAIL_WIDTH));
                        int tempIndex = DropDownPopup("CastingMagicEffect" + idIndexSearch.ToString() + currentIndex.ToString() + currentIndexList.ToString(), selected, option.ToArray());
                        if (tempIndex != selected)
                            data.refreshElement();
                        selected = tempIndex;
                        //selected = EditorGUILayout.Popup(selected,option.ToArray() ,GUILayout.Width(DatabaseState.DROP_DOWN_ENUM_WIDTH));
                        return battleEffectOption.Get(selectedOption).Option[selected];
                    }
                }
                else if(name.Equals("AllItemID"))
                {
                    return AllItem<T>(data,obj,"Item",idIndexSearch.ToString());
                }
                else if(name.Equals("HealingAndMagicItemID"))
                {
                    int selected = 0;
                    int itemNumb = 0;
                    string selectedID = (string)obj;
                    for (int n=0; n<consumableHealingDatabase.Count; n++)
                    {
                        if(consumableHealingDatabase.Get(n)._ID.Equals(selectedID))
                        {
                            selected = n;
                            itemNumb = 1;
                            break;
                        }
                    }
                    for (int n=0; n<magicItemDatabase.Count; n++)
                    {
                        if(magicItemDatabase.Get(n)._ID.Equals(selectedID))
                        {
                            selected = n;
                            itemNumb = 2;
                            break;
                        }
                    }
                    List<string> list = new List<string>();
                    for(int a=0;a<consumableHealingDatabase.Count;a++)
                    {
                        list.Add(consumableHealingDatabase.Get(a)._ID.ToString() + " , " + consumableHealingDatabase.Get(a).Name.ToString());
                    }
                    for(int a=0;a<magicItemDatabase.Count;a++)
                    {
                        list.Add(magicItemDatabase.Get(a)._ID.ToString() + " , " + magicItemDatabase.Get(a).Name.ToString());
                    }
                    if(itemNumb == 1)
                    {}
                    else if(itemNumb == 2)
                    {
                        selected = selected + consumableHealingDatabase.Count;
                    }

                    GUILayout.Label("Item",GUILayout.Width(DatabaseState.NAME_DETAIL_WIDTH));
                    int tempIndex = DropDownPopup("Item" + idIndexSearch.ToString() + currentIndex.ToString() + currentIndexList.ToString(), selected, list.ToArray());
                    if (tempIndex != selected)
                        data.refreshElement();
                    selected = tempIndex;
                    //selected = EditorGUILayout.Popup(selected,list.ToArray() ,GUILayout.Width(DatabaseState.DROP_DOWN_ENUM_WIDTH));
                    if (selected < consumableHealingDatabase.Count)
                        return consumableHealingDatabase.Get(selected)._ID;
                    else
                        return magicItemDatabase.Get(selected - consumableHealingDatabase.Count)._ID;
                }
            }
            else if (obj.GetType () == typeof(Buff))
            {
                int selected = 0;
                Buff buff = (Buff)obj;
                for (int n=0; n<buffDatabase.Count; n++)
                {
                    if(buffDatabase.Get(n)._ID.Equals(buff._ID))
                    {
                        selected = n;
                        break;
                    }
                }
                List<string> list = new List<string>();
                for(int a=0;a<buffDatabase.Count;a++)
                {
                    list.Add(buffDatabase.Get(a).Name.ToString());
                }
                GUILayout.Label(name,GUILayout.Width(DatabaseState.NAME_DETAIL_WIDTH));
                int tempIndex = DropDownPopup("BuffItem" + idIndexSearch.ToString() + currentIndex.ToString() + currentIndexList.ToString(), selected, list.ToArray());
                if (tempIndex != selected)
                    data.refreshElement();
                selected = tempIndex;
                //selected = EditorGUILayout.Popup(selected,list.ToArray() ,GUILayout.Width(DatabaseState.DROP_DOWN_ENUM_WIDTH));
                return buffDatabase.Get(selected);
            }

            else if (obj.GetType () == typeof(EncounterGrowth))
            {
                int selected = 0;
                EncounterGrowth encounterGrowth = (EncounterGrowth)obj;
                for (int n=0; n<encounterGrowthDatabase.Count; n++)
                {
                    if(encounterGrowthDatabase.Get(n).Name.Equals(encounterGrowth.Name))
                    {
                        selected = n;
                        break;
                    }
                }
                List<string> list = new List<string>();
                for(int a=0;a<encounterGrowthDatabase.Count;a++)
                {
                    list.Add(encounterGrowthDatabase.Get(a).Name.ToString());
                }
                GUILayout.Label(name,GUILayout.Width(DatabaseState.NAME_DETAIL_WIDTH));
                int tempIndex = DropDownPopup("EncounterGrowth" + idIndexSearch.ToString() + currentIndex.ToString() + currentIndexList.ToString(), selected, list.ToArray());
                if (tempIndex != selected)
                    data.refreshElement();
                selected = tempIndex;
                //selected = EditorGUILayout.Popup(selected,list.ToArray() ,GUILayout.Width(DatabaseState.DROP_DOWN_ENUM_WIDTH));
                return encounterGrowthDatabase.Get(selected);
            }

            if(field.FieldType == typeof(EncounterDataModel))
            {
                int selected = 0;
                EncounterDataModel encounter = (EncounterDataModel)obj;
                for (int n=0; n<encounterDatabase.Count; n++)
                {
                    if(encounterDatabase.Get(n).ID.Equals(encounter.ID))
                    {
                        selected = n;
                        break;
                    }
                }
                List<string> list = new List<string>();
                for(int a=0;a<encounterDatabase.Count;a++)
                {
                    list.Add(encounterDatabase.Get(a).ID.ToString() + " " + encounterDatabase.Get(a).Name.ToString());
                }
                GUILayout.Label(name,GUILayout.Width(DatabaseState.NAME_DETAIL_WIDTH));
                int tempIndex = DropDownPopup("EncounterDataModel" + idIndexSearch.ToString() + currentIndex.ToString() + currentIndexList.ToString(), selected, list.ToArray());
                if (tempIndex != selected)
                    data.refreshElement();
                selected = tempIndex;
                //selected = EditorGUILayout.Popup(selected,list.ToArray() ,GUILayout.Width(DatabaseState.DROP_DOWN_ENUM_WIDTH));
                return encounterDatabase.Get(selected);
            }

            return obj;
        }
        private object AllItem<T>(T data, object obj,string label, string idIndex) where T : IComponentDatabase
        {
            int selected = 0;
            int itemNumb = 0;
            string selectedID = (string)obj;
            for (int n=0; n<consumableHealingDatabase.Count; n++)
            {
                if(consumableHealingDatabase.Get(n)._ID.Equals(selectedID))
                {
                    selected = n;
                    itemNumb = 1;
                    break;
                }
            }
            for (int n=0; n<consumableAttributeDatabase.Count; n++)
            {
                if(consumableAttributeDatabase.Get(n)._ID.Equals(selectedID))
                {
                    selected = n;
                    itemNumb = 2;
                    break;
                }
            }
            for (int n=0; n<materialItemDatabase.Count; n++)
            {
                if(materialItemDatabase.Get(n)._ID.Equals(selectedID))
                {
                    selected = n;
                    itemNumb = 3;
                    break;
                }
            }
            for (int n=0; n<magicItemDatabase.Count; n++)
            {
                if(magicItemDatabase.Get(n)._ID.Equals(selectedID))
                {
                    selected = n;
                    itemNumb = 4;
                    break;
                }
            }
            for (int n=0; n<miscItemDatabase.Count; n++)
            {
                if(miscItemDatabase.Get(n)._ID.Equals(selectedID))
                {
                    selected = n;
                    itemNumb = 5;
                    break;
                }
            }
            for (int n=0; n<keyDatabase.Count; n++)
            {
                if(keyDatabase.Get(n)._ID.Equals(selectedID))
                {
                    selected = n;
                    itemNumb = 6;
                    break;
                }
            }
            List<string> list = new List<string>();
            for(int a=0;a<consumableHealingDatabase.Count;a++)
            {
                list.Add(consumableHealingDatabase.Get(a)._ID.ToString() + " , " + consumableHealingDatabase.Get(a).Name.ToString());
            }
            for(int a=0;a<consumableAttributeDatabase.Count;a++)
            {
                list.Add(consumableAttributeDatabase.Get(a)._ID.ToString() + " , " + consumableAttributeDatabase.Get(a).Name.ToString());
            }
            for(int a=0;a<materialItemDatabase.Count;a++)
            {
                list.Add(materialItemDatabase.Get(a)._ID.ToString() + " , " + materialItemDatabase.Get(a).Name.ToString());
            }
            for(int a=0;a<magicItemDatabase.Count;a++)
            {
                list.Add(magicItemDatabase.Get(a)._ID.ToString() + " , " + magicItemDatabase.Get(a).Name.ToString());
            }
            for(int a=0;a<miscItemDatabase.Count;a++)
            {
                list.Add(miscItemDatabase.Get(a)._ID.ToString() + " , " + miscItemDatabase.Get(a).Name.ToString());
            }
            for(int a=0;a<keyDatabase.Count;a++)
            {
                list.Add(keyDatabase.Get(a)._ID.ToString() + " , " + keyDatabase.Get(a).Name.ToString());
            }
            int[] valueAdd = new int[8];
            valueAdd[1] = 0;
            valueAdd[2] = consumableHealingDatabase.Count;
            valueAdd[3] = consumableHealingDatabase.Count + consumableAttributeDatabase.Count;
            valueAdd[4] = consumableHealingDatabase.Count + consumableAttributeDatabase.Count + materialItemDatabase.Count;
            valueAdd[5] = consumableHealingDatabase.Count + consumableAttributeDatabase.Count + materialItemDatabase.Count + magicItemDatabase.Count;
            valueAdd[6] = consumableHealingDatabase.Count + consumableAttributeDatabase.Count + materialItemDatabase.Count + magicItemDatabase.Count + miscItemDatabase.Count;

            selected = selected + valueAdd[itemNumb];

            GUILayout.Label(label,GUILayout.Width(DatabaseState.NAME_DETAIL_WIDTH));

            int tempIndex = DropDownPopup("AllItem" + idIndex, selected, list.ToArray());
            if (tempIndex != selected)
                data.refreshElement();
            selected = tempIndex;
            //selected = EditorGUILayout.Popup(selected,list.ToArray() ,GUILayout.Width(DatabaseState.DROP_DOWN_ENUM_WIDTH));

            if (selected < valueAdd[2])
                return consumableHealingDatabase.Get(selected)._ID;
            else if(selected >= valueAdd[2] && selected < valueAdd[3])
                return consumableAttributeDatabase.Get(selected - valueAdd[2])._ID;
            else if(selected >= valueAdd[3] && selected < valueAdd[4])
                return materialItemDatabase.Get(selected - valueAdd[3])._ID;
            else if(selected >= valueAdd[4] && selected < valueAdd[5])
                return magicItemDatabase.Get(selected - valueAdd[4])._ID;
            else if(selected >= valueAdd[5] && selected < valueAdd[6])
                return miscItemDatabase.Get(selected - valueAdd[5])._ID;
            else if(selected >= valueAdd[6])
                return keyDatabase.Get(selected - valueAdd[6])._ID;
            else return "";
        }
    }
}
