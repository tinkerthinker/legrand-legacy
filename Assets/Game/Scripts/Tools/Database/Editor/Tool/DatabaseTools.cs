using UnityEditor;
using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using Legrand.core;
using System.Runtime.Serialization.Formatters.Binary;

namespace Legrand.database
{
	public class DatabaseTools : DatabaseGeneralTool
	{
		#region PreviewDataController
		protected void PrefabView()
		{
			GUILayout.BeginHorizontal ();
			PrefabList();
			GUILayout.BeginVertical();
			ViewScript(TabName.Get(currentTab));
			GUILayout.EndVertical();
			GUILayout.EndHorizontal();
		}
        //void DatabaseIDView<T>(T database) where T : IComponentDatabase
        //{
        //    GUILayout.BeginHorizontal("Box");
        //    if (database.DatabaseID == "")
        //        database.DatabaseID = TabName.Get(currentTab);
        //    database.DatabaseID = DetailElement("Database ID", database.DatabaseID);
        //    database.refreshElement();
        //    GUILayout.EndHorizontal();
        //}
        void SearchSwitch()
        {
            if(DatabaseState.SearchActive)
                GUI.color = SelectedColor;
            if (GUILayout.Button("DropDown Search " + (DatabaseState.SearchActive ? "On" : "Off"), GUILayout.Width(BOTTOM_BUTTON_WIDTH * 3)))
            {
                DatabaseState.SearchActive = !DatabaseState.SearchActive;
            }
            GUI.color = Color.white;
        }
        protected void ScriptableObjectView<T,U>(T database,string FieldName,string SecondName = "",string ThirdName = "") where T : IComponentDatabase,IGetAndAddDatabase<U>
		{
            if (! FieldName.Equals("NoList"))
            {
                GUILayout.BeginHorizontal();
                GUILayout.Label("Search , ");
                SearchKey[0] = DetailElement(FieldName + " : " , SearchKey[0],150);
                if(!SecondName.Equals(""))
                    SearchKey[1] = DetailElement(SecondName + " : ", SearchKey[1], 150);
                if (!ThirdName.Equals(""))
                    SearchKey[2] = DetailElement(ThirdName + " : ", SearchKey[2], 150);
                GUILayout.FlexibleSpace();
                GUILayout.EndHorizontal();
                GUILayout.BeginHorizontal();
                ListView(database, FieldName, SecondName, ThirdName);
                GUILayout.BeginVertical();
                //DatabaseIDView(database);
                Detail<T,U>(database);
                GUILayout.EndVertical();
                GUILayout.EndHorizontal();
                GUILayout.BeginVertical();
                AddAndDelete<T,U>(database, FieldName);
                SearchSwitch();
                SaveAndLoad();
                GUILayout.EndVertical();
            }
            else
            {
                GUILayout.BeginVertical();
                //DatabaseIDView(database);
                NoListDetail<T,U>(database);
                GUILayout.FlexibleSpace();
                GUILayout.BeginHorizontal();
                SearchSwitch();
                SaveAndLoad();
                GUILayout.EndVertical();
            }
		}
		#endregion
		#region SaveAndLoadScriptableObject
		public virtual void LoadDatabase()
		{

		}
		private string TempPath()
		{
			string dir = Directory.GetCurrentDirectory ();
			string tempPath = dir + "/Assets/Database/DatabaseTemp/";
			tempPath = tempPath.Replace (@"\", "/");
			return tempPath;
		}
		private string DatabasePath()
		{
			string dir = Directory.GetCurrentDirectory ();
			string path = dir + "/Assets/Database/";
			path = path.Replace (@"\", "/");
			return path;
		}
		protected void CopyToTemp()
		{
			List<string> Path = new List<string>();
			List<string> Name = new List<string>();
			for(int i=0;i<ManyTab;i++)
			{
				Name.Add(FileName.Get(i));
				Path.Add(DatabasePath() + FileName.Get(i));
			}
			DatabaseBackupAndRestore.Copy(TempPath(),Path,Name,true);
		}
		private void CopyFromTemp()
		{
			List<string> Path = new List<string>();
			List<string> Name = new List<string>();
			for(int i=0;i<ManyTab;i++)
			{
				Name.Add(FileName.Get(i));
				Path.Add(TempPath() + FileName.Get(i));
			}
			DatabaseBackupAndRestore.Copy(DatabasePath(),Path,Name,true);
		}
        private bool isBiteEqual(byte[] file1,byte[] file2)
        {
            if (file1.Length == file2.Length)
            {
                for (int i = 0; i < file1.Length; i++)
                {
                    if (file1[i] != file2[i])
                    {
                        return false;
                    }
                }
                return true;
            }
            return false;
        }
        private bool isEdited()
        {
            //EditorApplication.SaveAssets ();
            //for (int i = 0; i < ManyTab; i++)
            //{
            //    byte[] file1 = File.ReadAllBytes(DatabasePath() + FileName.Get(i));
            //    byte[] file2 = File.ReadAllBytes(TempPath() + FileName.Get(i));
            //    if(!isBiteEqual(file1,file2))
            //        return true;
            //}
            //return false;
            for(int i=0;i<ThisDatabase.Count;i++)
            {
                if (ThisDatabase[i].isEdited)
                    return true;
            }
            return false;
        }
        public virtual void RefreshThisReference()
        {

        }
		protected void OnExit()
		{
            if (!AutoSave)
            {
                if (isEdited())
                {
                    if (!EditorUtility.DisplayDialog("Exit", "Save Changes ?", "Yes", "No"))
                    {
                        CopyFromTemp();
                    }
                    else
                        RefreshThisReference();
                }
                RemoveTemp();
            }
            else
                RefreshThisReference();
            EditorApplication.SaveAssets ();
        }
		private void RemoveTemp()
		{
			List<string> Path = new List<string>();
			for(int i=0;i<ManyTab;i++)
			{
				Path.Add(TempPath() + FileName.Get(i));
			}
			DatabaseBackupAndRestore.Remove(Path);
		}
		private void SaveAndLoad()
		{
			GUILayout.FlexibleSpace();
			string text = "AutoSave Off";
			if(AutoSave)
			{
				text = "AutoSave On";
				GUI.color = SelectedColor;
			}
			if(GUILayout.Button (text, GUILayout.Width(BOTTOM_BUTTON_WIDTH * 2)))
			{
				if(!AutoSave)
				{
					if(EditorUtility.DisplayDialog("Confirm","Enable AutoSave?","Yes","No"))
					{
						AutoSave = true;
						RemoveTemp();
					}
				}
				else
				{
					if(EditorUtility.DisplayDialog("Confirm","Disable AutoSave?","Yes","No"))
					{
						AutoSave = false;
						CopyToTemp();
					}
				}
			}
			GUI.color = Color.white;
			GUILayout.EndHorizontal();
			if(!AutoSave)
			{
				GUILayout.BeginHorizontal();
				GUILayout.FlexibleSpace();
				if(GUILayout.Button ("Reset", GUILayout.Width(BOTTOM_BUTTON_WIDTH)))
				{
					if(EditorUtility.DisplayDialog("Reset","Reset all Changes from latest save ?","Yes","No"))
					{
                        CurrentIndex.Set (-1,currentTab);
						CopyFromTemp();
                        ThisDatabase.Clear();
						LoadDatabase();
                        UnfocusElement();
                        RefreshThisReference();
                        setIsEdited(false);
                        EditorApplication.SaveAssets();
                    }
				}
				if(GUILayout.Button ("Save", GUILayout.Width(BOTTOM_BUTTON_WIDTH)))
				{
					if(EditorUtility.DisplayDialog("Save","Save changes ?","Yes","No"))
					{
						CopyToTemp();
                        RefreshThisReference();
                        setIsEdited(false);
                        EditorApplication.SaveAssets();
                    }
				}
				GUILayout.EndHorizontal();
			}
		}
        void setIsEdited(bool edited)
        {
            for(int i = 0;i<ThisDatabase.Count;i++)
            {
                ThisDatabase[i].isEdited = edited;
            }
        }
		#endregion
		#region Prefab
		private void PrefabList()
		{
			string[] nama = PrefabName(TabName.Get(currentTab));
			MainScrollPosition (LIST_VIEW_WIDTH,currentTab);
			for (int cnt = 0; cnt < nama.Length; cnt++)
			{
				if(CurrentIndex.Get(currentTab) == cnt)
					GUI.color = Color.gray;
				if (ButtonList (nama [cnt]))
					CurrentIndex.Set (cnt, currentTab);
				GUI.color = Color.white;
			}
			GUILayout.EndScrollView ();
			ScrollPositionDetail();
		}
		private string[] PrefabName(string dirName)
		{
			List<string> name = new List<string>();
			DirectoryInfo dir = new DirectoryInfo(dirName);
			FileInfo[] info = dir.GetFiles("*.prefab");
			foreach (FileInfo f in info) 
			{
				name.Add(f.Name.Split('.')[0]);
			}
			return name.ToArray();
		}
		private void Refresh(UnityEngine.Object Go)
		{
			EditorUtility.SetDirty(Go);
			AssetDatabase.SaveAssets();
			AssetDatabase.Refresh();
		}
		private bool RegulerDataType(GameObject Go,object obj,string name,object temp,FieldInfo field, int currentIndex)
		{
			if(object.ReferenceEquals(null, temp))
			{
				return false;
			}
			
			Type typeTest = temp.GetType();
			if(temp is int)
			{
                int value = DetailElement(RemovePrivateMark(name),(int)temp);
				if(value != int.Parse(temp.ToString()))
				{
					EditorUtility.SetDirty(Go);
					field.SetValue(obj,value);
				}
				return true;
			}
			else if(temp is string)
			{
				string value = DetailElement(RemovePrivateMark(name),temp.ToString());
				if(value != temp.ToString())
				{
					EditorUtility.SetDirty(Go);
					field.SetValue(obj,value);
				}
				return true;
			}
			else if(temp is float)
			{
				float value = DetailElement(RemovePrivateMark(name),float.Parse(temp.ToString()));
				if(value != float.Parse(temp.ToString()))
				{
					EditorUtility.SetDirty(Go);
					field.SetValue(obj,value);
				}
				return true;
			}
			else if(temp is bool)
			{
				GUILayout.Label(RemovePrivateMark(name),GUILayout.Width(NAME_DETAIL_WIDTH));
				bool value = EditorGUILayout.Toggle(Convert.ToBoolean(temp));
				if(value != Convert.ToBoolean(temp))
				{
					EditorUtility.SetDirty(Go);
					field.SetValue(obj,value);
				}
				return true;
			}
			else if(temp is DateTime)
			{
				GUILayout.Label(RemovePrivateMark(name) + " : " + Convert.ToDateTime(temp.ToString()));
			}
			else if(typeTest == typeof(UnityEngine.GameObject))
			{
				GUILayout.Label(RemovePrivateMark(name) + " is Object");
			}
			else if(temp is Sprite)
			{
				Texture2D selectedTexture;
				GUILayout.BeginHorizontal();
				
				GUILayout.Label(RemovePrivateMark(name),GUILayout.Width(NAME_DETAIL_WIDTH));
				Sprite tex = (Sprite)temp;
				if(tex)
				{
					selectedTexture = tex.texture;
				}
				else
				{
					selectedTexture = null;
				}
				
				if (GUILayout.Button(selectedTexture, GUILayout.Width(SPRITE_BUTTON_DETAIL_VIEW_SIZE), GUILayout.Height(SPRITE_BUTTON_DETAIL_VIEW_SIZE)))
				{
					int controlerID = EditorGUIUtility.GetControlID(FocusType.Passive);
					EditorGUIUtility.ShowObjectPicker<Sprite>(null, true, null, controlerID);
				}
				
				string commandName = Event.current.commandName;
				if(commandName == "ObjectSelectorUpdated")
				{
					temp = (Sprite)EditorGUIUtility.GetObjectPickerObject();
					Repaint();
					field.SetValue(obj,temp);
				}
				
				GUILayout.EndHorizontal();
			}
			else if(typeTest.IsEnum)
			{
				GUILayout.BeginHorizontal();
				string[] EnumMember = Enum.GetNames(typeTest);
				GUILayout.Label(RemovePrivateMark(name),GUILayout.Width(NAME_DETAIL_WIDTH));
                int value = DropDownPopup(name + currentIndex.ToString() + CurrentIndex.Get(currentTab).ToString(), (int)temp, EnumMember);
                if (value != (int)temp)
				{
					EditorUtility.SetDirty(Go);
					temp = value;
					field.SetValue(obj,value);
				}
				GUILayout.EndHorizontal();
			}
			else if(temp is IList)
			{
				IList test = (IList)temp;
				Type type = test.GetType().GetGenericArguments()[0];
				
				if(type.IsClass && type.ToString() != "System.String")
				{
					return false;
				}
				
				GUILayout.BeginVertical("Box");
				GUILayout.Label("List : " + RemovePrivateMark(name),EditorStyles.boldLabel);
				GUILayout.Label("Total data : " + test.Count);
				
				if(type == typeof(bool))
				{
					for(int cnt=0;cnt<test.Count;cnt++)
					{
						GUILayout.BeginHorizontal();
						GUILayout.Label(cnt.ToString(),GUILayout.Width(NAME_DETAIL_WIDTH));
						bool value = EditorGUILayout.Toggle(Convert.ToBoolean(test[cnt]));
						if(value != Convert.ToBoolean(test[cnt]))
						{
							EditorUtility.SetDirty(Go);
							test[cnt] = value;
						}
						GUILayout.EndHorizontal();
					}
				}
				else if(type == typeof(int))
				{
					for(int cnt=0;cnt<test.Count;cnt++)
					{
						GUILayout.BeginHorizontal();
						int value = DetailElement(cnt.ToString(),int.Parse(test[cnt].ToString()));
						if(value != int.Parse(test[cnt].ToString()))
						{
							EditorUtility.SetDirty(Go);
							test[cnt] = value;
						}
						GUILayout.EndHorizontal();
					}
				}
				else if(type == typeof(float))
				{
					for(int cnt=0;cnt<test.Count;cnt++)
					{
						GUILayout.BeginHorizontal();
						float value = DetailElement(cnt.ToString(),float.Parse(test[cnt].ToString()));
						if(value != float.Parse(test[cnt].ToString()))
						{
							EditorUtility.SetDirty(Go);
							test[cnt] = value;
						}
						GUILayout.EndHorizontal();
					}
				}
				else if(type == typeof(string) && !type.IsEnum)
				{
					for(int cnt=0;cnt<test.Count;cnt++)
					{
						GUILayout.BeginHorizontal();
						string value = DetailElement(cnt.ToString(),test[cnt].ToString());
						if(value != test[cnt].ToString())
						{
							EditorUtility.SetDirty(Go);
							test[cnt] = value;
						}
						GUILayout.EndHorizontal();
					}
				}
				else if(type.IsEnum)
				{
					for(int cnt=0;cnt<test.Count;cnt++)
					{
						GUILayout.BeginHorizontal();
						string[] EnumMember = Enum.GetNames(type);
						GUILayout.Label(cnt.ToString(),GUILayout.Width(NAME_DETAIL_WIDTH));
						int value = DropDownPopup(name + cnt.ToString() + currentIndex.ToString() + CurrentIndex.Get(currentTab).ToString(), (int)temp, EnumMember);
                        if (value != (int)test[cnt])
						{
							EditorUtility.SetDirty(Go);
							test[cnt] = value;
						}
						GUILayout.EndHorizontal();
					}
				}
				GUILayout.BeginHorizontal();
				if(GUILayout.Button ("Add", GUILayout.Width(BOTTOM_BUTTON_WIDTH)))
				{
					if(type == typeof(int))
						test.Add(0);
					if(type == typeof(bool))
						test.Add(false);
					if(type == typeof(float))
						test.Add(0.0f);
					if(type == typeof(string) && !type.IsEnum)
						test.Add("New Value");
					if(type.IsEnum)
						test.Add(0);
					Refresh(Go);
				}
				/*int ind;
				ind = EditorGUILayout.IntField(ind,GUILayout.Width(30));
				if(test.Count > 0 && GUILayout.Button ("Insert", GUILayout.Width(BOTTOM_BUTTON_WIDTH)))
				{
					if(type.ToString() == "System.Int32")
						test.Insert(ind,0);
					if(type.ToString() == "System.Boolean")
						test.Insert(ind,false);
					if(type.ToString() == "System.Single")
						test.Insert(ind,0.0f);
					if(type.ToString() == "System.String" && !type.IsEnum)
						test.Insert(ind,"New Value");
					if(type.IsEnum)
						test.Insert(ind,0);
					data.refresh();
				}*/
				if(test.Count > 0 && GUILayout.Button ("Delete", GUILayout.Width(BOTTOM_BUTTON_WIDTH)))
				{
					if(EditorUtility.DisplayDialog("Delete","Do you want to delete " + test[test.Count - 1] + "?","Yes","No"))
					{
						test.RemoveAt(test.Count - 1);
						Refresh(Go);
					}
				}
				GUILayout.EndHorizontal();
				GUILayout.EndVertical();
			}
			return true;
		}
		private void GetAllScript(GameObject Go,List<MonoBehaviour> scriptList)
		{
			foreach(MonoBehaviour mono in Go.GetComponents<MonoBehaviour>())
			{
				scriptList.Add(mono);
			}
			for(int c=0;c<Go.transform.childCount;c++)
			{
				GetAllScript(Go.transform.GetChild(c).gameObject,scriptList);
			}
		}
		private void DetailViewList(GameObject Go,object element,int b)
		{
			IList listElement = (IList)element;
			int curre = 0;
			for(int fieldNum=0;fieldNum<listElement.Count;fieldNum++)
			{
				GUILayout.BeginHorizontal();
				if(!object.ReferenceEquals(null, listElement[fieldNum]) && (fieldNum == b))
				{
					FieldInfo[] fields = listElement[fieldNum].GetType().GetFields();
					GUILayout.BeginVertical();
					int currentListElement = 0;
					foreach (var field in fields)
					{
						GUILayout.BeginHorizontal();
						string name = field.Name;
						object temp = field.GetValue(listElement[fieldNum]);
						if(!RegulerDataType(Go,listElement[fieldNum],name,temp,field, currentListElement))
						{
							if(temp is IList)
							{
								GUILayout.BeginVertical("Box");
								GUILayout.Label("List : " + name,EditorStyles.boldLabel);
								string[] arrayDropDown = DetailElementDropDown(temp);
								IList test = (IList)temp;
								GUILayout.BeginHorizontal();
								curre = CurrentDropDownElementList.Get(currentTab,CurrentDropDownMainList.Get(currentTab,name).ToString() + CurrentIndex.Get(currentTab).ToString() + name);
								if(test.Count > 0)
								{
									string title = "ID";
									GUILayout.Label(title + " : ",GUILayout.Width(ID_LIST_WIDTH));
									curre = EditorGUILayout.Popup(curre,arrayDropDown ,GUILayout.Width(ID_LIST_WIDTH));
								}
								GUILayout.Label("Total data : " + test.Count);
								GUILayout.EndHorizontal();
								DetailViewList(Go,temp,curre);
								curre = AddAndDelete(temp,Go,curre);
								GUILayout.EndVertical();
								CurrentDropDownElementList.Set (currentTab, CurrentDropDownMainList.Get (currentTab, name).ToString () + CurrentIndex.Get (currentTab).ToString () + name, curre);
								currentListElement++;
							}
						}
						GUILayout.EndHorizontal();
					}
					GUILayout.EndVertical();
				}
				GUILayout.EndHorizontal();
			}
		}
		private void ViewScript(string dirName)
		{
			string[] prefabName = PrefabName(dirName);
			GameObject Go = AssetDatabase.LoadAssetAtPath(dirName + "/" + prefabName[CurrentIndex.Get(currentTab)] + ".prefab", typeof(GameObject)) as GameObject;
			List<MonoBehaviour> scriptList = new List<MonoBehaviour>();
			GetAllScript(Go,scriptList);
			MonoBehaviour[] script = scriptList.ToArray();
			List<string> scriptName = new List<string>();
			for(int k=0;k<script.Length;k++)
			{
				scriptName.Add(script[k].ToString().Split('(')[1].Split(')')[0]);
			}
			int currentScript = CurrentDropDownScript.Get (currentTab, CurrentIndex.Get (currentTab).ToString());
			GUILayout.BeginVertical("Box");
			GUILayout.Label("Script [" + currentScript + "] , Total Script : " + scriptName.Count);
			currentScript = EditorGUILayout.Popup(currentScript,scriptName.ToArray());
			CurrentDropDownScript.Set (currentTab, CurrentIndex.Get (currentTab).ToString(), currentScript);
			GUILayout.EndHorizontal();
			GUILayout.BeginVertical();
			FieldInfo[] fields = script[currentScript].GetType().GetFields();
			int currentList = 0;
			foreach (var field in fields)
			{
				GUILayout.BeginHorizontal();
				string name = field.Name;
				object temp = field.GetValue(script[currentScript]);
				if(!RegulerDataType(Go,script[currentScript],name,temp,field,currentList))
				{
					if(temp is IList)
					{
						GUILayout.BeginVertical("Box");
						GUILayout.Label("List : " + name,EditorStyles.boldLabel);
						string[] arrayDropDown = DetailElementDropDown(temp);
						IList test = (IList)temp;
						GUILayout.BeginHorizontal();
						if(test.Count > 0)
						{
							string title = "ID";
							GUILayout.Label(title + " : ",GUILayout.Width(ID_LIST_WIDTH));
							CurrentDropDownMainList.Set (currentTab, name, EditorGUILayout.Popup (CurrentDropDownMainList.Get(currentTab,name), arrayDropDown, GUILayout.Width (ID_LIST_WIDTH)));
						}
						GUILayout.Label("Total data : " + test.Count);
						GUILayout.EndHorizontal();
						DetailViewList (Go, temp, CurrentDropDownMainList.Get (currentTab, name));
						CurrentDropDownMainList.Set(currentTab,name,AddAndDelete(temp,Go,CurrentDropDownMainList.Get(currentTab,name)));
						GUILayout.EndVertical();
						currentList++;
					}
					else
					{
						GUILayout.Label(RemovePrivateMark(name) + " is Class");
					}
				}
				GUILayout.EndHorizontal();
			}
			GUILayout.EndVertical();
			GUILayout.EndScrollView ();
		}
		private int AddAndDelete(object element,GameObject Go,int index)
		{
			IList listElement = (IList)element;
			
			GUILayout.BeginHorizontal();
			if(GUILayout.Button ("Add", GUILayout.Width(BOTTOM_BUTTON_WIDTH)))
			{
				listElement.Add(null);
				Refresh(Go);
				index = listElement.Count -1;
			}
			if((listElement.Count > 0 && index > -1) && GUILayout.Button ("Insert", GUILayout.Width(BOTTOM_BUTTON_WIDTH)))
			{
				listElement.Insert(index - 1,null);
				Refresh(Go);
			}
			if((listElement.Count > 0 && index > -1) && GUILayout.Button ("Delete", GUILayout.Width(BOTTOM_BUTTON_WIDTH)))
			{
				if(EditorUtility.DisplayDialog("Delete","Do you want to delete " + index + "?","Yes","No"))
				{
					listElement.RemoveAt(index);
					Refresh(Go);
					index = index - 1;
				}
			}
			GUILayout.EndHorizontal();
			return index;
		}
		#endregion
		#region ScriptableObject
        private void ListView<T>(T data,string fieldName, string Name1, string Name2) where T :IComponentDatabase
		{
			MainScrollPosition (LIST_VIEW_WIDTH,currentTab);
            int ViewCount = 0;
			for (int cnt = 0; cnt < data.Count; cnt++)
			{
                if (ViewCount >= 1000)
                    break;
				if(CurrentIndex.Get(currentTab) == cnt)
					GUI.color = SelectedColor;
				if(!fieldName.Equals("None"))
				{
                    if(SearchResult(data.GetFieldValue(cnt, fieldName), 0) && SearchResult(data.GetFieldValue(cnt, Name1), 1) && SearchResult(data.GetFieldValue(cnt, Name2), 2))
                    {
                        ViewCount++;
                        if (ButtonList(data.GetFieldValue(cnt, fieldName) + (Name1.Equals("") ? "" : (" , " + data.GetFieldValue(cnt, Name1)) + (Name2.Equals("") ? "" : (" , " + data.GetFieldValue(cnt, Name2))))))
                        {
                            CurrentIndex.Set(cnt, currentTab);
                            UnfocusElement();
                            formulaResult = "";
                        }
                    }
                }
				else
				{
                    ViewCount++;
                    if (ButtonList(cnt.ToString()))
                    {
                        CurrentIndex.Set(cnt, currentTab);
                        UnfocusElement();
                    }
				}
				GUI.color = Color.white;
			}
			GUILayout.EndScrollView ();
            ScrollPositionDetail();
		}
		private bool RegulerDataType<T>(object obj,string name,object temp,T data,FieldInfo field, int currentIndex)where T :IComponentDatabase
		{
			if(object.ReferenceEquals(null, temp))
			{
				return false;
			}
            
            Type typeTest = temp.GetType();

			if (temp is Vector2)
			{
                object value = new object();
                if (obj.GetType() == typeof(Legrand.database.EncounterFormationTest) || name.Equals("Formation"))
                    value = DatabaseVector2Controller.DropDownControl(temp, name);
                else
                {
                    Vector2 vektor2 = (Vector2)temp;
                    GUILayout.BeginVertical();
                    GUILayout.BeginHorizontal();
                    GUILayout.Label(RemovePrivateMark(name),GUILayout.Width(NAME_DETAIL_WIDTH));
                    float x = DetailElement("X", vektor2.x,30);
                    float y = DetailElement("Y", vektor2.y,30);
                    GUILayout.EndHorizontal();
                    GUILayout.EndVertical();
                    value = new Vector2(x, y);
                }
				if((Vector2)value != (Vector2)temp)
				{
					data.refreshElement();
					field.SetValue(obj,value);
				}
			}
			else if(temp is int)
			{
				int value = DetailElement(RemovePrivateMark(name),int.Parse(temp.ToString()));
				if(value != int.Parse(temp.ToString()))
				{
					data.refreshElement();
					field.SetValue(obj,value);
				}
			}
			else if(temp is string)
			{
				string value = DetailElement(RemovePrivateMark(name),temp.ToString());
				if(value != temp.ToString())
				{
					data.refreshElement();
					field.SetValue(obj,value);
				}
			}
			else if(temp is float)
			{
				float value = DetailElement(RemovePrivateMark(name),float.Parse(temp.ToString()));
				if(value != float.Parse(temp.ToString()))
				{
					data.refreshElement();
					field.SetValue(obj,value);
				}
			}
            else if (temp is double)
            {
                double value = DetailElement(RemovePrivateMark(name), double.Parse(temp.ToString()));
                if (value != double.Parse(temp.ToString()))
                {
                    data.refreshElement();
                    field.SetValue(obj, value);
                }
            }
            else if(temp is bool)
			{
				GUILayout.Label(RemovePrivateMark(name),GUILayout.Width(NAME_DETAIL_WIDTH));
				bool value = EditorGUILayout.Toggle(Convert.ToBoolean(temp));
				if(value != Convert.ToBoolean(temp))
				{
					data.refreshElement();
					field.SetValue(obj,value);
				}
			}
			else if(temp is DateTime)
			{
				GUILayout.Label(RemovePrivateMark(name) + " : " + Convert.ToDateTime(temp.ToString()));
			}
			else if(typeTest == typeof(UnityEngine.GameObject))
			{
				GUILayout.Label(RemovePrivateMark(name) + " is Object");
			}
			else if(temp is Sprite)
			{
				Texture2D selectedTexture;
				GUILayout.BeginHorizontal();

				GUILayout.Label(RemovePrivateMark(name),GUILayout.Width(NAME_DETAIL_WIDTH));
				Sprite tex = (Sprite)temp;
				if(tex)
				{
                    selectedTexture = tex.texture;

                    try
                    {
                        Sprite sprite = tex;
                        Color[] pixels = selectedTexture.GetPixels(
                            (int)sprite.rect.x,
                            (int)sprite.rect.y,
                            (int)sprite.rect.width,
                            (int)sprite.rect.height);
                        Texture2D tcropped = new Texture2D(
                            (int)sprite.rect.width,
                            (int)sprite.rect.height);
                        tcropped.SetPixels(pixels);
                        tcropped.Apply();

                        selectedTexture = tcropped;
                    }
                    catch(UnityException e)
                    {

                    }

					selectedTexture.alphaIsTransparency = true;
				}
				else
				{
					selectedTexture = null;
				}

				string texName = "None";
				if(tex)
				{
					texName = tex.name;

					if (GUILayout.Button(selectedTexture, GUILayout.Width(SPRITE_BUTTON_DETAIL_VIEW_SIZE), GUILayout.Height(SPRITE_BUTTON_DETAIL_VIEW_SIZE)))
					{
						int controlerID = EditorGUIUtility.GetControlID(FocusType.Passive);
						EditorGUIUtility.ShowObjectPicker<Sprite>(null, true, null, controlerID);
					}
				}
				else
				{
					if (GUILayout.Button("None", GUILayout.Width(SPRITE_BUTTON_DETAIL_VIEW_SIZE), GUILayout.Height(SPRITE_BUTTON_DETAIL_VIEW_SIZE)))
					{
						int controlerID = EditorGUIUtility.GetControlID(FocusType.Passive);
						EditorGUIUtility.ShowObjectPicker<Sprite>(null, true, null, controlerID);
					}
				}
				GUILayout.Label("Sprite Name : " + texName);
				string commandName = Event.current.commandName;
				if(commandName == "ObjectSelectorUpdated")
				{
					object result = temp;
					result = EditorGUIUtility.GetObjectPickerObject();
					if(result != null)
					{
						temp = result;
						data.refreshElement();
						field.SetValue(obj,temp);
						Repaint();
					}
				}
				
				GUILayout.EndHorizontal();
			}
            else if (typeTest.IsEnum)
            {
                    GUILayout.BeginHorizontal();
                    string[] EnumMember = Enum.GetNames(typeTest);
                    GUILayout.Label(RemovePrivateMark(name), GUILayout.Width(NAME_DETAIL_WIDTH));
                    int value = DropDownPopup(name + currentIndex.ToString() + CurrentIndex.Get(currentTab).ToString(), (int)temp, EnumMember);
                    if (value != (int)temp)
                    {
                        data.refreshElement();
                        temp = value;
                        field.SetValue(obj, value);
                    }
                    GUILayout.EndHorizontal();
			}
			else if(temp is IList)
			{
				IList test = (IList)temp;
				Type type = test.GetType().GetGenericArguments()[0];

				if(type.IsClass && type.ToString() != "System.String")
				{
					return false;
				}
				
				GUILayout.BeginVertical("Box");
				GUILayout.Label("List : " + RemovePrivateMark(name),EditorStyles.boldLabel);
				GUILayout.Label("Total data : " + test.Count);

				List<int> deletedIndex = new List<int>();

				if(type == typeof(bool))
				{
					for(int cnt=0;cnt<test.Count;cnt++)
					{
						GUILayout.BeginHorizontal();
						GUILayout.Label(cnt.ToString(),GUILayout.Width(NAME_DETAIL_WIDTH));
						bool value = EditorGUILayout.Toggle(Convert.ToBoolean(test[cnt]));
						int idx = DeleteThisIndex(test,cnt);
						if(idx != -1)
							deletedIndex.Add(idx);
						if(value != Convert.ToBoolean(test[cnt]))
						{
							data.refreshElement();
							test[cnt] = value;
						}
						GUILayout.EndHorizontal();
					}
				}
				else if(type == typeof(int))
				{
					for(int cnt=0;cnt<test.Count;cnt++)
					{
						GUILayout.BeginHorizontal();
						int value = DetailElement(RemovePrivateMark(cnt.ToString()),int.Parse(test[cnt].ToString()));
						int idx = DeleteThisIndex(test,cnt);
						if(idx != -1)
							deletedIndex.Add(idx);
						if(value != int.Parse(test[cnt].ToString()))
						{
							data.refreshElement();
							test[cnt] = value;
						}
						GUILayout.EndHorizontal();
					}
				}
				else if(type == typeof(float))
				{
					for(int cnt=0;cnt<test.Count;cnt++)
					{
						GUILayout.BeginHorizontal();
						float value = DetailElement(RemovePrivateMark(cnt.ToString()),float.Parse(test[cnt].ToString()));
						int idx = DeleteThisIndex(test,cnt);
						if(idx != -1)
							deletedIndex.Add(idx);
						if(value != float.Parse(test[cnt].ToString()))
						{
							data.refreshElement();
							test[cnt] = value;
						}
						GUILayout.EndHorizontal();
					}
				}
				else if(type == typeof(string) && !type.IsEnum)
				{
					for(int cnt=0;cnt<test.Count;cnt++)
					{
						GUILayout.BeginHorizontal();
						string value = DetailElement(RemovePrivateMark(cnt.ToString()),test[cnt].ToString());
						int idx = DeleteThisIndex(test,cnt);
						if(idx != -1)
							deletedIndex.Add(idx);
						if(value != test[cnt].ToString())
						{
							data.refreshElement();
							test[cnt] = value;
						}
						GUILayout.EndHorizontal();
					}
				}
                else if (type.IsEnum)
                {
					for(int cnt=0;cnt<test.Count;cnt++)
					{
                            GUILayout.BeginHorizontal();
                            string[] EnumMember = Enum.GetNames(type);
                            GUILayout.Label(RemovePrivateMark(cnt.ToString()), GUILayout.Width(NAME_DETAIL_WIDTH));
                            int value = DropDownPopup(name + currentIndex.ToString() + CurrentIndex.Get(currentTab).ToString() + cnt.ToString(), (int)test[cnt], EnumMember);
                            int idx = DeleteThisIndex(test, cnt);
                            if (idx != -1)
                                deletedIndex.Add(idx);
                            if (value != (int)test[cnt])
                            {
                                data.refreshElement();
                                test[cnt] = value;
                            }
                            GUILayout.EndHorizontal();
					}
				}
				foreach(var ind in deletedIndex)
				{
					test.RemoveAt(ind);
					data.refreshElement();
				}
				GUILayout.BeginHorizontal();
				if(GUILayout.Button ("Add", GUILayout.Width(BOTTOM_BUTTON_WIDTH)))
				{
					if(type == typeof(int))
						test.Add(0);
					if(type == typeof(bool))
						test.Add(false);
					if(type == typeof(float))
						test.Add(0.0f);
					if(type == typeof(string) && !type.IsEnum)
						test.Add("new");
					if(type.IsEnum)
						test.Add(0);
					data.refresh();
                    UnfocusElement();
				}
				/*int ind;
				ind = EditorGUILayout.IntField(ind,GUILayout.Width(30));
				if(test.Count > 0 && GUILayout.Button ("Insert", GUILayout.Width(BOTTOM_BUTTON_WIDTH)))
				{
					if(type.ToString() == "System.Int32")
						test.Insert(ind,0);
					if(type.ToString() == "System.Boolean")
						test.Insert(ind,false);
					if(type.ToString() == "System.Single")
						test.Insert(ind,0.0f);
					if(type.ToString() == "System.String" && !type.IsEnum)
						test.Insert(ind,"New Value");
					if(type.IsEnum)
						test.Insert(ind,0);
					data.refresh();
				}*/
				/*if(test.Count > 0 && GUILayout.Button ("Delete", GUILayout.Width(BOTTOM_BUTTON_WIDTH)))
				{
					if(EditorUtility.DisplayDialog("Delete","Do you want to delete " + test[test.Count - 1] + "?","Yes","No"))
					{
						test.RemoveAt(test.Count - 1);
						data.refresh();
					}
				}*/
				GUILayout.EndHorizontal();
				GUILayout.EndVertical();
			}
			return true;
		}

		public static int DeleteThisIndex(IList List,int index)
		{
			if(GUILayout.Button ("Delete", GUILayout.Width(DatabaseState.BOTTOM_BUTTON_WIDTH),GUILayout.Height(DELETE_LIST_BUTTON_HEIGHT)))
			{
				if(EditorUtility.DisplayDialog("Delete","Do you want to delete index " + index.ToString() + " ?","Yes","No"))
				{
					return index;
				}
			}
			return -1;
		}

        private void DetailViewList<T,U>(object element,T data,int listNum)where T : IComponentDatabase,IGetAndAddDatabase<U>
		{
			IList listElement = (IList)element;
			for(int fieldNum=0;fieldNum<listElement.Count;fieldNum++)
			{
				GUILayout.BeginHorizontal();
				if(!object.ReferenceEquals(null, listElement[fieldNum]) && (fieldNum == CurrentDropDownState.Get(currentTab,listNum.ToString())))
				{
					FieldInfo[] fields = listElement[fieldNum].GetType().GetFields(bindFlags);

					GUILayout.BeginVertical();
					foreach (var field in fields)
					{
                        int currentList = 0;
						GUILayout.BeginHorizontal();
						string name = field.Name;
						object temp = field.GetValue(listElement[fieldNum]);
                        ClassTypeView<T, U>(field, temp, data);
                        if (ClassController.isKnownClass(field))
						{
                            object result = ClassController.DropDownControl<T>(data,field,temp,name,CurrentIndex.Get(currentTab),fieldNum, currentList);
							if(result != null && !result.Equals(temp))
							{
								data.refreshElement(false);
								field.SetValue(listElement[fieldNum],result);
							}
						}
						else
						{
                            if (!RegulerDataType<T> (listElement [fieldNum], name, temp, data, field, currentList))
                            {
                                if (temp is IList)
                                {
                                    GUILayout.BeginVertical("Box");
                                    GUILayout.Label("List : " + RemovePrivateMark(name), EditorStyles.boldLabel);
                                    string[] arrayDropDown = DetailElementDropDown(temp);
                                    IList test = (IList)temp;
                                    GUILayout.BeginHorizontal();
                                    int __index = CurrentDropDownState.Get(currentTab, name + CurrentDropDownState.Get(currentTab, listNum.ToString()) + currentList.ToString());
                                    if (test.Count > 0 && __index > -1)
                                    {
                                        GUILayout.Label("List index : ", GUILayout.Width(ID_LIST_WIDTH));
                                        CurrentDropDownState.Set(currentTab, name + CurrentDropDownState.Get(currentTab,listNum.ToString()) + currentList.ToString(), EditorGUILayout.Popup(__index, arrayDropDown, GUILayout.Width(DROP_DOWN_LIST_WIDTH)));
                                    }
                                    GUILayout.Label("Total data : " + test.Count);
                                    GUILayout.EndHorizontal();
                                    if (test.Count > 0)
                                    {
                                        if (__index >= test.Count)
                                            __index = 0;
                                        FieldInfo[] fieldz = test[__index].GetType().GetFields(bindFlags);

                                        foreach (var fiel in fieldz)
                                        {
                                            GUILayout.BeginHorizontal();
                                            object temps = fiel.GetValue(test[__index]);
                                            
                                            if (!RegulerDataType<T>(test[__index], fiel.Name, temps, data, fiel, __index))
                                            {
                                                GUILayout.BeginVertical();
                                                DetailViewList<T,U>(temp, data, currentList);
                                                GUILayout.EndVertical();
                                            }
                                            currentList++;
                                            ClassTypeView<T, U>(fiel, temps, data);
                                            GUILayout.EndHorizontal();
                                        }
                                    }
                                    
                                    GUILayout.BeginHorizontal();
                                    if (GUILayout.Button("Add", GUILayout.Width(BOTTOM_BUTTON_WIDTH)))
                                    {
                                        test.Add(null);
                                        data.refresh();
                                        __index = test.Count - 1;
                                        UnfocusElement();
                                    }

                                    if ((test.Count > 0 && __index > -1) && GUILayout.Button("Delete", GUILayout.Width(BOTTOM_BUTTON_WIDTH)))
                                    {
                                        if (EditorUtility.DisplayDialog("Delete", "Do you want to delete " + __index + "?", "Yes", "No"))
                                        {
                                            test.RemoveAt(__index);
                                            __index = 0;
                                            data.refresh();
                                            UnfocusElement();
                                        }
                                    }
                                    CurrentDropDownState.Set(currentTab, name + CurrentDropDownState.Get(currentTab, listNum.ToString()) + currentList.ToString(),__index);
                                    GUILayout.EndVertical();
                                    GUILayout.EndVertical();
                                }
                            }
						}
                        GUILayout.EndHorizontal();
					}
					GUILayout.EndVertical();
				}
				GUILayout.EndHorizontal();
			}
			GUILayout.BeginHorizontal();

                int _index = CurrentDropDownState.Get(currentTab, listNum.ToString());

                if (GUILayout.Button("Add", GUILayout.Width(BOTTOM_BUTTON_WIDTH)))
                {
                    listElement.Add(null);
                    data.refresh();
                    _index = listElement.Count - 1;
                    UnfocusElement();
                }
                /*if((listElement.Count > 0 && _index > -1) && GUILayout.Button ("Insert", GUILayout.Width(BOTTOM_BUTTON_WIDTH)))
			    {
				    listElement.Insert(_index - 1,null);
				    data.refresh();
			    }*/
                if ((listElement.Count > 0 && _index > -1) && GUILayout.Button("Delete", GUILayout.Width(BOTTOM_BUTTON_WIDTH)))
                {
                    if (EditorUtility.DisplayDialog("Delete", "Do you want to delete " + _index + "?", "Yes", "No"))
                    {
                        listElement.RemoveAt(_index);
                        data.refresh();
                        _index = _index - 1;
                    }
                }
                CurrentDropDownState.Set(currentTab, listNum.ToString(), _index);
            GUILayout.EndHorizontal();
		}
        protected void Detail<T,U>(T data) where T : IComponentDatabase,IGetAndAddDatabase<U>
		{
            int index = CurrentIndex.Get(currentTab);
            int currentList = 0;
            if (!object.ReferenceEquals(null, data) && (index != -1 && data.Count > 0))
            {
                GUILayout.BeginVertical();
                FieldInfo[] fields = data.Get(index).GetType().GetFields(bindFlags);
                int idIndex = 0;
                foreach (var field in fields)
                {
                    idIndex++;
                    GUILayout.BeginHorizontal();
                    string name = field.Name;
                    object temp = field.GetValue(data.Get(index));
                    if (ClassController.isKnownClass(field))
                    {
                        object result = ClassController.DropDownControl<T>(data, field, temp, name, CurrentIndex.Get(currentTab), index, idIndex);
                        if (result != null && !result.Equals(temp))
                        {
                            data.refreshElement(false);
                            field.SetValue(data.Get(index), result);
                        }
                    }
                    else if (!RegulerDataType<T>(data.Get(index), name, temp, data, field, idIndex))
                    {
                        if (temp is IList)
                        {
                            GUILayout.BeginVertical("Box");
                            GUILayout.Label("List : " + RemovePrivateMark(name), EditorStyles.boldLabel);
                            string[] arrayDropDown = DetailElementDropDown(temp);
                            IList test = (IList)temp;
                            GUILayout.BeginHorizontal();
                            if (test.Count > 0)
                            {
                                GUILayout.Label("List index : ", GUILayout.Width(ID_LIST_WIDTH));
                                CurrentDropDownState.Set(currentTab, currentList.ToString(), EditorGUILayout.Popup(CurrentDropDownState.Get(currentTab, currentList.ToString()), arrayDropDown, GUILayout.Width(DROP_DOWN_LIST_WIDTH)));
                            }
                            GUILayout.Label("Total data : " + test.Count);
                            GUILayout.EndHorizontal();
                            DetailViewList<T, U>(temp, data, currentList);
                            GUILayout.EndVertical();
                            currentList++;
                        }
                    }
                    ClassTypeView<T,U>(field, temp, data);
                    GUILayout.EndHorizontal();
                    if (name.Equals("Formula"))
                    {
                        if (GUILayout.Button("Test Formula"))
                        {
                            List<FormulaDataModel.Parameter> paraValue = new List<FormulaDataModel.Parameter>();
                            List<string> parameter = new List<string>();
                            List<string> paraName = new List<string>();
                            foreach (var f in fields)
                            {
                                if (f.Name.Equals("Formula"))
                                {
                                    formulaResult = f.GetValue(data.Get(index)).ToString();
                                }
                                else if (f.Name.Equals("ParameterVariable"))
                                {
                                    paraValue = (List<FormulaDataModel.Parameter>)f.GetValue(data.Get(index));
                                }
                            }
                            foreach (var para in paraValue)
                            {
                                paraName.Add(para.Name);
                                parameter.Add(para.valueTest);
                            }
                            //eksekusi formula, masukkan ke formula result
                            formulaResult = Formula.AddDeclaration(formulaResult, paraName, parameter);
                            formulaCalculationResult = "Result : \n" + FormulaEval.EvaluateFormula(formulaResult).ToString();
                        }
                        GUILayout.Label(formulaCalculationResult);//tampilkan hasil kalkulasi
                    }
                }
                GUILayout.EndVertical();
            }
			GUILayout.EndScrollView ();
		}
        void ViewDataFromObject<T,U>(object dataTemp,T data) where T : IComponentDatabase, IGetAndAddDatabase<U>
        {
            FieldInfo[] fieldss = dataTemp.GetType().GetFields(bindFlags);
            GUILayout.BeginVertical("Box");
            GUILayout.Label(dataTemp.GetType().ToString(), EditorStyles.boldLabel);
            int currentLists = 0;
            foreach (var fie in fieldss)
            {
                GUILayout.BeginHorizontal();
                string names = fie.Name;
                object temps = fie.GetValue(dataTemp);
                if (!RegulerDataType<T>(dataTemp, names, temps, data, fie, currentLists))
                {
                    if (temps is IList)
                    {
                        GUILayout.BeginVertical("Box");
                        GUILayout.Label("List : " + RemovePrivateMark(fie.Name), EditorStyles.boldLabel);
                        string[] arrayDropDown = DetailElementDropDown(temps);
                        IList test = (IList)temps;
                        GUILayout.BeginHorizontal();
                        if (test.Count > 0)
                        {
                            GUILayout.Label("List index : ", GUILayout.Width(ID_LIST_WIDTH));
                            CurrentDropDownState.Set(currentTab, currentLists.ToString(), EditorGUILayout.Popup(CurrentDropDownState.Get(currentTab, currentLists.ToString()), arrayDropDown, GUILayout.Width(DROP_DOWN_LIST_WIDTH)));
                        }
                        GUILayout.Label("Total data : " + test.Count);
                        GUILayout.EndHorizontal();
                        DetailViewList<T, U>(temps, data, currentLists);
                        GUILayout.EndVertical();
                        currentLists++;
                    }
                }
                ClassTypeView<T, U>(fie, temps, data);
                GUILayout.EndHorizontal();
            }
            GUILayout.EndVertical();
        }
        void ClassTypeView<T, U>(FieldInfo Field,object temp,T data) where T : IComponentDatabase, IGetAndAddDatabase<U>
        {
            bool found = false;
            if (Field.FieldType == typeof(QuestWorldEvents))
            {
                found = true;
            }
            else if (Field.FieldType == typeof(SaveData))
            {
                found = true;
            }
            else if (Field.FieldType == typeof(StoryProgression))
            {
                found = true;
            }
            else if (Field.FieldType == typeof(WorldState))
            {
                found = true;
            }
            else if (Field.FieldType == typeof(WorldInfoModel))
            {
                found = true;
            }
            if(found)
            {
                ViewDataFromObject<T, U>(temp, data);
            }
        }
        protected void NoListDetail<T,U>(T data) where T : IComponentDatabase,IGetAndAddDatabase<U>
        {
            int currentList = 0;
            if(data.Count == 0)
                data.AddValue(default(U));
            FieldInfo[] fields = data.Get(0).GetType().GetFields(bindFlags);
            foreach (FieldInfo field in fields)
            {
                GUILayout.BeginHorizontal();
                object temp = field.GetValue(data.Get(0));
                if (!RegulerDataType<T>(data.Get(0), field.Name, temp, data, field, currentList))
                {
                    if (temp is IList)
                    {
                        GUILayout.BeginVertical("Box");
                        GUILayout.Label("List : " + RemovePrivateMark(field.Name), EditorStyles.boldLabel);
                        string[] arrayDropDown = DetailElementDropDown(temp);
                        IList test = (IList)temp;
                        GUILayout.BeginHorizontal();
                        if (test.Count > 0)
                        {
                            GUILayout.Label("List index : ", GUILayout.Width(ID_LIST_WIDTH));
                            CurrentDropDownState.Set(currentTab, currentList.ToString(), EditorGUILayout.Popup(CurrentDropDownState.Get(currentTab, currentList.ToString()), arrayDropDown, GUILayout.Width(DROP_DOWN_LIST_WIDTH)));
                        }
                        GUILayout.Label("Total data : " + test.Count);
                        GUILayout.EndHorizontal();
                        DetailViewList<T,U>(temp, data, currentList);
                        GUILayout.EndVertical();
                        currentList++;
                    }
                }
                GUILayout.EndHorizontal();
            }
        }
        T DeepCopy<T>(T obj)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, obj);
                stream.Position = 0;
                return (T)formatter.Deserialize(stream);
            }
        }
        private void AddAndDelete<T,U> (T data,string name)where T:IComponentDatabase , IGetAndAddDatabase<U>
		{
			GUILayout.BeginHorizontal();
			if(GUILayout.Button ("Add", GUILayout.Width(BOTTOM_BUTTON_WIDTH)))
			{
				data.Modify(CurrentIndex.Get(currentTab),DataAction.ADD);
				CurrentIndex.Set (data.Count -1,currentTab);
			}
            if (GUILayout.Button("Duplicate", GUILayout.Width(BOTTOM_BUTTON_WIDTH * 2)))
            {
                try
                {
                    data.Modify(CurrentIndex.Get(currentTab) - 1, DataAction.INSERT);
                    data.SetItem(CurrentIndex.Get(currentTab), DeepCopy<U>(data.Get(CurrentIndex.Get(currentTab) + 1)));
                }
                catch(Exception e)
                {
                    data.Modify(CurrentIndex.Get(currentTab), DataAction.REMOVE);
                    Debug.LogError("Duplicate error type not supported");
                }
            }
            if ((data.Count > 0 && CurrentIndex.Get(currentTab) > -1))
            {
                if (GUILayout.Button("Insert", GUILayout.Width(BOTTOM_BUTTON_WIDTH)))
                {
                    data.Modify(CurrentIndex.Get(currentTab) - 1, DataAction.INSERT);
                }
                if (GUILayout.Button("Delete", GUILayout.Width(BOTTOM_BUTTON_WIDTH)))
                {
                    if (CurrentIndex.Get(currentTab) != -1)
                    {
                        if (!name.Equals("None"))
                        {
                            if (EditorUtility.DisplayDialog("Delete", "Do you want to delete " + data.GetFieldValue(CurrentIndex.Get(currentTab), name) + " ?", "Yes", "No"))
                            {
                                data.Modify(CurrentIndex.Get(currentTab), DataAction.REMOVE);
                                CurrentIndex.Set(CurrentIndex.Get(currentTab) - 1, currentTab);
                            }
                        }
                        else
                        {
                            if (EditorUtility.DisplayDialog("Delete", "Do you want to delete " + CurrentIndex.Get(currentTab).ToString() + " ?", "Yes", "No"))
                            {
                                data.Modify(CurrentIndex.Get(currentTab), DataAction.REMOVE);
                                CurrentIndex.Set(CurrentIndex.Get(currentTab) - 1, currentTab);
                            }
                        }
                    }
                }
                if (GUILayout.Button("Delete All", GUILayout.Width(BOTTOM_BUTTON_WIDTH * 1.5f)))
                {
                    if (EditorUtility.DisplayDialog("Delete All", "Delete All ?", "Yes", "No"))
                    {
                        data.Clear();
                    }
                }
                if (!name.Equals("None"))
                {
                    if ((data.Count > 0 && CurrentIndex.Get(currentTab) > -1) && GUILayout.Button("Sort Ascending", GUILayout.Width(BOTTOM_BUTTON_WIDTH * 2.5f)))
                    {
                        if (EditorUtility.DisplayDialog("Sort", "Sort Ascending ?", "Yes", "No"))
                        {
                            data.Sort(name, "Ascending");
                        }
                    }
                    if ((data.Count > 0 && CurrentIndex.Get(currentTab) > -1) && GUILayout.Button("Sort Descending", GUILayout.Width(BOTTOM_BUTTON_WIDTH * 2.5f)))
                    {
                        if (EditorUtility.DisplayDialog("Sort", "Sort Descending ?", "Yes", "No"))
                        {
                            data.Sort(name, "Descending");
                        }
                    }
                }
            }
		}
		#endregion
	}
}