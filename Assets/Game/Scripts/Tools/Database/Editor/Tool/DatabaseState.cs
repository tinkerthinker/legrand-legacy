using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.IO;

namespace Legrand.database
{
	public class DatabaseState : EditorWindow
	{
		protected bool AutoSave = false;
        public static bool SearchActive = false;
		public static Color SelectedColor = new Color(0.97f,0.67f,0.67f);

        public static BindingFlags bindFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;

		public const float FORMULA_HEIGHT = 400f;
		public const int LIST_VIEW_WIDTH = 280;
		public const int LIST_VIEW_BUTTON_WIDTH = 260;
		public const int LIST_VIEW_BUTTON_HEIGHT = 25;
		public const int NAME_DETAIL_WIDTH = 170;
		public const int SPRITE_BUTTON_DETAIL_VIEW_SIZE = 92;
		public const int BOTTOM_BUTTON_WIDTH = 50;
		public const string DATABASE_PATH = @"Database";
		public const int WINDOW_WIDTH = 800;
		public const int WINDOW_HEIGHT = 400;
		public const int DROP_DOWN_LIST_WIDTH = 250;
		public const int SIDE_SCROLL_VALUE = 10;
		public const int DROP_DOWN_ENUM_WIDTH = 210;
		public const int ID_LIST_WIDTH = 80;
		public const float DELETE_LIST_BUTTON_HEIGHT = 14f;

		protected int currentTab;
		protected int ManyTab;
		protected DatabaseScroolPosition MainScroolPosition  = new DatabaseScroolPosition();
		protected DatabaseScroolPosition DetailScroolPosition = new DatabaseScroolPosition();
		protected DatabaseName FileName = new DatabaseName();
		protected DatabaseName TabName = new DatabaseName();
		protected DatabaseIndex CurrentIndex = new DatabaseIndex();
		protected DatabaseStateKey CurrentDropDownMainList = new DatabaseStateKey();
		protected DatabaseStateKey CurrentDropDownScript = new DatabaseStateKey();
		protected DatabaseStateKey CurrentDropDownState = new DatabaseStateKey();
		protected DatabaseStateKey CurrentDropDownElementList = new DatabaseStateKey();

		protected DatabaseClassTypeController ClassController = new DatabaseClassTypeController();

        protected string formulaResult = "";
        protected string formulaCalculationResult = "";

        protected Dictionary<string, string> SearchPopupKey = new Dictionary<string, string>();

        protected string[] SearchKey = new string[4];

        protected List<IComponentDatabase> ThisDatabase;

        protected List<IComponentDatabase> DatabaseFile;

        IComponentDatabase LoadFile(string name)
        {
            IComponentDatabase d = (IComponentDatabase)AssetDatabase.LoadAssetAtPath("Assets/Database/" + name, typeof(IComponentDatabase));
            return d;
        }

        public DatabaseState()
        {
            ThisDatabase = new List<IComponentDatabase>();

            for (int i = 0; i < SearchKey.Length; i++)
            {
                SearchKey[i] = "";
            }

            DatabaseFile = new List<IComponentDatabase>();

            DirectoryInfo dir = new DirectoryInfo("Assets/Database");
            FileInfo[] info = dir.GetFiles("*.asset");
            foreach (FileInfo f in info)
            {
                DatabaseFile.Add(LoadFile(f.Name));
            }
        }
	}
}