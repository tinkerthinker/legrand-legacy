using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System;
using Legrand.core;

namespace Legrand.database
{
    public class DatabaseGeneralTool : DatabaseState
    {
        bool SetItem(FieldInfo otherData, string idFieldName, IComponentDatabase ThisData, object obj)
        {
            bool isSet = false;
            if (obj != null)
            {
                object otherItem = otherData.GetValue(obj);
                if (otherItem != null)
                {
                    for (int t = 0; t < ThisData.Count; t++)
                    {
                        object thisItem = ThisData.GetItem(t);
                        object thisVal = thisItem.GetType().GetField(idFieldName).GetValue(thisItem);
                        FieldInfo fieldinfo = otherItem.GetType().GetField(idFieldName);
                        if (fieldinfo != null)
                        {
                            object otherVal = fieldinfo.GetValue(otherItem);
                            if (thisVal.Equals(otherVal))
                            {
                                otherData.SetValue(obj, ThisData.GetItem(t));
                                //Debug.Log("set item : " +ThisData.GetType().ToString() + " , this item : " + thisItem.GetType().ToString() + " , other item : " + otherItem.GetType().ToString() + " , id value : " + otherVal.ToString());
                                isSet = true;
                                break;
                            }
                        }
                    }
                }
            }
            return isSet;
        }

        void SearchField(object temp, string[] fieldName, string idField, IComponentDatabase thisData, ref bool isset, ref int recursivecount, Type[] types = null)
        {
            IList ilist = (IList)temp;
            foreach (var item in ilist)
            {
                FieldInfo[] itemInfo = item.GetType().GetFields();
                foreach (FieldInfo fi in itemInfo)
                {
                    object obj = fi.GetValue(item);
                    if (obj != null && obj is IList)
                    {
                        if (CompareFieldNameOrType(fieldName, fi.Name, types, obj.GetType()))
                        {
                            isset = SearchList(ilist, idField, thisData, temp, isset);
                        }
                        else if (recursivecount < 5)
                        {
                            //Debug.Log("list recursive : " + temp.GetType().ToString() + ", recursive count : " + recursivecount.ToString());
                            recursivecount++;
                            SearchField(temp, fieldName, idField, thisData, ref isset, ref recursivecount, types);
                        }
                    }
                    else if (obj != null && CompareFieldNameOrType(fieldName, fi.Name, types, obj.GetType()))
                    {
                        isset = SetItem(fi, idField, thisData, item);
                    }
                }
            }
        }
        bool SearchList(IList list, string idField, IComponentDatabase ThisData, object reference, bool isSet)
        {
            for (int i = 0; i < list.Count; i++)
            {
                FieldInfo field = list[i].GetType().GetField(idField);
                if (field != null)
                {
                    object otherValue = field.GetValue(list[i]);
                    for (int t = 0; t < ThisData.Count; t++)
                    {
                        object thisItem = ThisData.GetItem(t);
                        object thisVal = thisItem.GetType().GetField(idField).GetValue(thisItem);
                        if (thisVal.Equals(otherValue))
                        {
                            list[i] = ThisData.GetItem(t);
                            //Debug.Log("set list item : " + list.GetType().ToString() + " id : " + thisItem.ToString());
                            isSet = true;
                        }
                    }
                }
                else
                {
                    Debug.Log("list null at : " + i.ToString() + ", type : " + list[i].GetType().ToString() + ", id : " + idField);
                }
            }
            return isSet;
        }
        bool CompareFieldNameOrType(string[] fieldName, string thisFieldName, Type[] types = null, Type thisType = null)
        {
            if (types != null && thisType != null)
            {
                foreach (Type type in types)
                {
                    if (type.Equals(thisType))
                        return true;
                }
            }
            else
            {
                foreach (string name in fieldName)
                {
                    if (name.Equals(thisFieldName))
                        return true;
                }
            }
            return false;
        }
        protected void RefreshReference<T>(string idField, IComponentDatabase ThisData, string fieldName = "")
        {
            List<Type> types = new List<Type>();
            types.Add(typeof(T));
            types.Add(typeof(List<T>));

            string[] fieldNames = fieldName.Split('+');

            RefreshReference(fieldNames, idField, ThisData, types.ToArray());
        }
        void RefreshReference(string[] fieldName, string idField, IComponentDatabase ThisData, Type[] types = null)
        {
            List<IComponentDatabase> otherDatabase = DatabaseFile;

            foreach (IComponentDatabase other in otherDatabase)
            {
                if (other != null && !ThisData.Equals(other))
                {
                    bool isSet = false;
                    for (int i = 0; i < other.Count; i++)
                    {
                        object reference = other.GetItem(i);
                        FieldInfo[] fields = other.GetItem(i).GetType().GetFields();
                        foreach (FieldInfo field in fields)
                        {
                            int recursiveCount = 0;

                            object temp = field.GetValue(other.GetItem(i));
                            if (temp != null)
                            {
                                if (temp is IList)
                                {
                                    if (CompareFieldNameOrType(fieldName, field.Name, types, temp.GetType()))
                                    {
                                        isSet = SearchList((IList)temp, idField, ThisData, temp, isSet);
                                        other.refreshElement(false);
                                    }
                                    else
                                        SearchField(temp, fieldName, idField, ThisData, ref isSet, ref recursiveCount, types);
                                }
                                else if (CompareFieldNameOrType(fieldName, field.Name, types, temp.GetType()))
                                {
                                    isSet = SetItem(field, idField, ThisData, reference);
                                }
                            }
                        }
                    }
                    if (isSet)
                    {
                        other.refresh();
                        Debug.Log(other.GetType().ToString() + " Updated");
                    }
                }
            }
            EditorApplication.SaveAssets();
        }

        public static void UnfocusElement()
        {
            string dummyControl = "hihihihihi";
            GUI.FocusControl(dummyControl);
            GUI.SetNextControlName(dummyControl);
            GUI.Button(new Rect(-10000, -10000, 0, 0), GUIContent.none);
        }
        protected int DropDownPopup(string name, int index, string[] data)
        {
            GUILayout.BeginHorizontal();

            int result = 0;
            List<int> indexData = new List<int>();
            if (DatabaseState.SearchActive)
            {

                string key = "";
                if (SearchPopupKey.ContainsKey(name))
                    key = SearchPopupKey[name];
                else
                    SearchPopupKey.Add(name, "");

                GUILayout.Label("Search");
                key = EditorGUILayout.TextField(key);

                List<string> newData = new List<string>();
                for (int i = 0; i < data.Length; i++)
                {
                    if (data[i].ToLower().Replace(" ", "").Contains(key.ToLower().Replace(" ", "")))
                    {
                        newData.Add(data[i]);
                        indexData.Add(i);
                        if (data[i].Equals(data[index]))
                            index = newData.Count - 1;
                    }
                }
                result = EditorGUILayout.Popup(index, newData.ToArray());

                SearchPopupKey[name] = key;

                if (result < indexData.Count)
                    result = indexData[result];
            }
            else
                result = EditorGUILayout.Popup(index, data);
            GUILayout.EndHorizontal();

            return result;
        }
        protected bool SearchResult(string value, int index)
        {
            if (SearchKey[index].Equals("") || value.Equals(""))
                return true;
            if (value.ToLower().Replace(" ", "").Contains(SearchKey[index].ToLower().Replace(" ", "")))
                return true;
            return false;
        }
        protected string RemovePrivateMark(string data)
        {
            if (data.Length > 0 && data[0] == '_')
                return data.Replace("_", "");
            return data;
        }
        protected string DetailElement(string title, string element)
        {
            if (title.Equals("Formula"))
            {
                string result = "";
                GUILayout.BeginVertical();
                GUILayout.Label("Formula");
                result = EditorGUILayout.TextArea(element, GUILayout.Height(FORMULA_HEIGHT));
                GUILayout.EndVertical();
                return result;
            }
            else
            {
                GUILayout.BeginHorizontal();
                GUILayout.Label(RemovePrivateMark(title), GUILayout.Width(NAME_DETAIL_WIDTH));
                string result = EditorGUILayout.TextField(element);
                GUILayout.EndHorizontal();
                return result;
            }
        }
        protected string DetailElement(string title, string element, int fieldWidth)
        {
            GUILayout.Label(RemovePrivateMark(title));
            return EditorGUILayout.TextField(element, GUILayout.Width(fieldWidth));
        }
        protected int DetailElement(string title, int element)
        {
            GUILayout.Label(RemovePrivateMark(title), GUILayout.Width(NAME_DETAIL_WIDTH));
            return EditorGUILayout.IntField(element);
        }
        protected double DetailElement(string title, double element)
        {
            GUILayout.Label(RemovePrivateMark(title), GUILayout.Width(NAME_DETAIL_WIDTH));
            return EditorGUILayout.DoubleField(element);
        }
        protected float DetailElement(string title, float element)
        {
            GUILayout.Label(RemovePrivateMark(title), GUILayout.Width(NAME_DETAIL_WIDTH));
            return EditorGUILayout.FloatField(element);
        }
        protected float DetailElement(string title, float element, int width)
        {
            GUILayout.Label(RemovePrivateMark(title), GUILayout.Width(width));
            return EditorGUILayout.FloatField(element, GUILayout.Width(width + 20));
        }
        protected void ScrollPositionDetail()
        {
            DetailScroolPosition.Set(GUILayout.BeginScrollView(DetailScroolPosition.Get(currentTab + CurrentIndex.Get(currentTab)), "Box", GUILayout.ExpandHeight(true), GUILayout.Width(Screen.width - LIST_VIEW_WIDTH - SIDE_SCROLL_VALUE)), currentTab + CurrentIndex.Get(currentTab));
        }
        public void TopTabBar()
        {
            TabBar(ManyTab);
        }
        private void TabBar(int maxtab)
        {
            GUILayout.BeginHorizontal("Box", GUILayout.ExpandWidth(true));
            for (int x = 0; x < maxtab; x++)
            {
                if (currentTab == x)
                    GUI.color = SelectedColor;
                if (GUILayout.Button(TabName.Get(x)))
                {
                    currentTab = x;
                    UnfocusElement();
                }
                GUI.color = Color.white;
            }
            GUILayout.EndHorizontal();
        }
        protected bool ButtonList(string name)
        {
            GUIStyle skin = new GUIStyle(GUI.skin.button);
            skin.alignment = TextAnchor.MiddleLeft;
            return GUILayout.Button(name, skin, GUILayout.Width(LIST_VIEW_BUTTON_WIDTH), GUILayout.Height(LIST_VIEW_BUTTON_HEIGHT));
        }
        protected void MainScrollPosition(int width, int tab)
        {
            MainScroolPosition.Set(GUILayout.BeginScrollView(MainScroolPosition.Get(tab), "Box", GUILayout.ExpandHeight(true), GUILayout.Width(width)), tab);
        }
        private int getNameIndex(FieldInfo[] fields)
        {
            int name = -1;
            int id = -1;
            for (int f = 0; f < fields.Length; f++)
            {
                if (name == -1 && RemovePrivateMark(fields[f].Name).ToLower().Equals("name"))
                    name = f;
                else if (name == -1 && RemovePrivateMark(fields[f].Name).ToLower().Contains("name"))
                    name = f;
                if (id == -1 && RemovePrivateMark(fields[f].Name).ToLower().Contains("id"))
                    id = f;
            }
            if (name != -1)
                return name;
            if (id != -1)
                return id;
            if (name == -1 && id == -1)
                return -1;
            return 0;
        }
        protected string[] DetailElementDropDown(object element)
        {
            List<string> dropDownList = new List<string>();
            IList listElement = (IList)element;
            int dropDownKe = 0;
            for (int fieldNum = 0; fieldNum < listElement.Count; fieldNum++)
            {
                if (!object.ReferenceEquals(null, listElement[fieldNum]))
                {
                    BindingFlags bindFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
                    FieldInfo[] fields = listElement[fieldNum].GetType().GetFields(bindFlags);
                    int inde = getNameIndex(fields);

                    if (inde > -1 && fields[inde].GetValue(listElement[fieldNum]) != null)
                        {
                            dropDownList.Add(dropDownKe.ToString() + "." + RemovePrivateMark(fields[inde].Name) + " = " + fields[inde].GetValue(listElement[fieldNum]).ToString());
                        }
                        else
                            dropDownList.Add(dropDownKe.ToString());
                        dropDownKe++;
                }
            }
            return dropDownList.ToArray();
        }
    }
}
