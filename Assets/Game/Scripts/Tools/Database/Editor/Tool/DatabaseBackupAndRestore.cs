﻿using UnityEditor;
using UnityEngine;
using System.IO;
using System.Collections;
using System.Collections.Generic;

namespace Legrand.database
{
	public class DatabaseBackupAndRestore : EditorWindow
	{
		[MenuItem("Legrand Legacy/Backup And Restore/Backup Database")]
		private static void Backup()
		{
			List<string> FilePath = new List<string>();
			List<string> FileName = new List<string>();

			string dir = Directory.GetCurrentDirectory ();
			string pathSource = dir + "/Assets/Database/";
			pathSource = pathSource.Replace (@"\", "/");
			string pathTarget = dir + "/Assets/Database/DatabaseBackup/";
			pathTarget = pathTarget.Replace (@"\", "/");

			if(EditorUtility.DisplayDialog("Backup","Backup All Database ?","Yes","No"))
			{
				GetAllFiles(pathSource,FilePath,FileName);
				Copy(pathTarget,FilePath,FileName,true);
			}
		}
		[MenuItem("Legrand Legacy/Backup And Restore/Restore Database")]
		private static void Restore()
		{
			List<string> FilePath = new List<string>();
			List<string> FileName = new List<string>();

			string dir = Directory.GetCurrentDirectory ();
			string pathSource = dir + "/Assets/Database/DatabaseBackup/";
			pathSource = pathSource.Replace (@"\", "/");
			string pathTarget = dir + "/Assets/Database/";
			pathTarget = pathTarget.Replace (@"\", "/");

			if(EditorUtility.DisplayDialog("Restore","Restore All Database ?","Yes","No"))
			{
				GetAllFiles(pathSource,FilePath,FileName);
				Copy(pathTarget,FilePath,FileName,true);
			}
		}
		[MenuItem("Legrand Legacy/Backup And Restore/Remove Backup")]
		private static void RemoveBackup()
		{
			List<string> FilePath = new List<string>();

			string dir = Directory.GetCurrentDirectory ();
			string pathBackup = dir + "/Assets/Database/DatabaseBackup/";
			pathBackup = pathBackup.Replace (@"\", "/");

			if(EditorUtility.DisplayDialog("Remove","Remove Backup Database ?","Yes","No"))
			{
				GetAllFiles(pathBackup,FilePath);
				Remove(FilePath);
			}
		}
		private static void GetAllFiles(string path,List<string> FilePath, List<string>FileName)
		{
			DirectoryInfo dir = new DirectoryInfo(path);
			FileInfo[] info = dir.GetFiles("*.asset");
			foreach (FileInfo f in info) 
			{
				FilePath.Add(f.FullName);
				FileName.Add(f.Name);
			}
		}
		private static void GetAllFiles(string path,List<string> FilePath)
		{
			DirectoryInfo dir = new DirectoryInfo(path);
			FileInfo[] info = dir.GetFiles("*.asset");
			foreach (FileInfo f in info) 
			{
				FilePath.Add(f.FullName);
			}
		}
		public static void Copy(string targetPath, List<string>FilePath , List<string>FileName,bool overwrite)
		{
            if(!Directory.Exists(targetPath))
            {
                Directory.CreateDirectory(targetPath);
            }
			if(overwrite)
			{
				for(int i=0;i<FilePath.Count;i++)
				{
					File.Delete(targetPath + FileName[i]);
				}
				AssetDatabase.Refresh();
			}
			for(int i=0;i<FilePath.Count;i++)
			{
				if(overwrite)
				{
					File.Copy (FilePath [i], targetPath + FileName [i], true);
				}
				else
				{
					if(!File.Exists(targetPath + FileName[i]))
					{
						File.Copy(FilePath[i],targetPath + FileName[i]);
					}
				}
			}
			AssetDatabase.Refresh();
		}
		public static void Remove(List<string>FilePath)
		{
			for(int i=0;i<FilePath.Count;i++)
			{
				File.Delete(FilePath[i]);
			}
			AssetDatabase.Refresh();
		}
	}
}