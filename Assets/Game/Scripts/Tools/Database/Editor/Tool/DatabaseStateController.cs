﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Legrand.database
{
	public class DatabaseScroolPosition
	{
		private List<Vector2> scroolPosition = new List<Vector2>();
		private void ScroolPositionController(int index)
		{
			while (index > scroolPosition.Count - 1)
			{
				scroolPosition.Add (new Vector2());
			}
		}
		public Vector2 Get(int index)
		{
			ScroolPositionController (index);
			if(index > -1)
				return scroolPosition [index];
			else
				return new Vector2();
		}
		public void Set(Vector2 value,int index)
		{
			ScroolPositionController (index);
			if(index > -1)
				scroolPosition [index] = value;
		}
	}
	public class DatabaseName
	{
		private List<string> databaseName = new List<string>();
		private void databaseNameController(int index)
		{
			while (index > databaseName.Count - 1)
			{
				databaseName.Add ("");
			}
		}
		public string Get(int index)
		{
			databaseNameController (index);
			if(index > -1)
				return databaseName[index];
			else
				return "";
		}
		public void Set(string value,int index)
		{
			databaseNameController (index);
			if(index > -1)
				databaseName [index] = value;
		}
	}
	public class DatabaseIndex
	{
		private List<int> databaseIndex = new List<int>();
		private void databaseIndexController(int index)
		{
			while (index > databaseIndex.Count - 1)
			{
				databaseIndex.Add (0);
			}
		}
		public int Get(int index)
		{
			databaseIndexController (index);
			if(index > -1)
				return databaseIndex[index];
			else
				return 0;
		}
		public void Set(int value,int index)
		{
			databaseIndexController (index);
			if(index > -1)
				databaseIndex [index] = value;
		}
	}
	public class DatabaseStateKey
	{
		private Dictionary<string,int> data = new Dictionary<string, int>();
		public void Set(int currentTab,string name,int value)
		{
			if (!data.ContainsKey (currentTab.ToString() + name))
			{
				data.Add (currentTab.ToString() + name, value);
			}
			else
			{
				data [currentTab.ToString() + name] = value;
			}
		}
		public int Get(int currentTab,string name)
		{
			if (!data.ContainsKey (currentTab.ToString() + name))
			{
				data.Add (currentTab.ToString() + name, 0);
			}
			return data [currentTab.ToString() + name];
		}
	}
    public class DatabaseGameObjectList
    {
        private Dictionary<string,List<string>> data = new Dictionary<string, List<string>>();
        public void Set(string name,List<string> value)
        {
            if (!data.ContainsKey (name))
            {
                data.Add (name,new List<string>());
            }
            else
            {
                data [name] = value;
            }
        }
        public List<string> Get(string name)
        {
            if (!data.ContainsKey (name))
            {
                data.Add(name, new List<string>());
            }
            return data [name];
        }
        public void RemoveOption(string name)
        {
            if (data.ContainsKey(name))
            {
                data.Remove(name);
            }
        }
        private Dictionary<string,int> dataIndex = new Dictionary<string, int>();
        public void SetIndex(string name,int value)
        {
            if (!dataIndex.ContainsKey (name))
            {
                dataIndex.Add (name, value);
            }
            else
            {
                dataIndex [name] = value;
            }
        }
        public int GetIndex(string name)
        {
            if (!dataIndex.ContainsKey (name))
            {
                dataIndex.Add (name, 0);
            }
            return dataIndex [name];
        }
        public void RemoveIndex(string name)
        {
            if (dataIndex.ContainsKey (name))
            {
                dataIndex.Remove(name);
            }
        }
    }
}