﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;
using Legrand.database;

public class DatabaseWeaponCircleManager : DatabaseTools
{
    private WeaponCircleDatabase elementDatabase = ScriptableObject.CreateInstance<WeaponCircleDatabase>();

    public override void LoadDatabase ()
    {
        elementDatabase = WeaponCircleDatabase.GetDatabase<WeaponCircleDatabase>(DATABASE_PATH,FileName.Get(0));
        ThisDatabase.Add(elementDatabase);
    }
    [MenuItem("Legrand Legacy/Database/Weapon Circle")]
    private static void Init ()
    {
        DatabaseWeaponCircleManager window = EditorWindow.GetWindow<DatabaseWeaponCircleManager> ();
        window.minSize = new Vector2 (WINDOW_WIDTH,WINDOW_HEIGHT);
        window.titleContent.text = "Weapon Circle Database";
        window.Show ();
    }
    private DatabaseWeaponCircleManager()
    {
        ManyTab = 1;
        //tab name and path for prefab
        //database name for scriptable object, none for prefab
        TabName.Set ("WeaponCircle", 0);
        FileName.Set (@"WeaponCircleDatabase.asset",0);
    }
    private void OnEnable ()
    {
        LoadDatabase();
        CopyToTemp();
    }
    private void OnDisable ()
    {
        OnExit();
    }
    private void OnGUI ()
    {
        TopTabBar();
        switch(currentTab)
        {
            case 0:
                ScriptableObjectView<WeaponCircleDatabase,WeaponCircle>(elementDatabase,"CurrentWeapon");
                break;
        }
    }
}
