﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;
using Legrand.database;

public class DatabaseStoryDataManager : DatabaseTools
{
	private StoryDatabase storyDatabase = ScriptableObject.CreateInstance<StoryDatabase>();
	
	public override void LoadDatabase ()
	{
		storyDatabase =	StoryDatabase.GetDatabase <StoryDatabase>(DATABASE_PATH,FileName.Get(0));
        ThisDatabase.Add(storyDatabase);
	}
	[MenuItem("Legrand Legacy/Database/Story")]
	private static void Init ()
	{
		DatabaseStoryDataManager window = EditorWindow.GetWindow<DatabaseStoryDataManager> ();
		window.minSize = new Vector2 (WINDOW_WIDTH,WINDOW_HEIGHT);
		window.titleContent.text = "Story Database";
		window.Show ();
	}
	private DatabaseStoryDataManager()
	{
		ManyTab = 1;
		//tab name and path for prefab
		//database name for scriptable object, none for prefab
		TabName.Set ("Story", 0);
		FileName.Set (@"StoryData.asset",0);
	}
	private void OnEnable ()
	{
		LoadDatabase();
		CopyToTemp();
	}
	private void OnDisable ()
	{
		OnExit();
	}
	private void OnGUI ()
	{
		TopTabBar();
		switch(currentTab)
		{
		case 0:
			ScriptableObjectView<StoryDatabase,StoryData>(storyDatabase, "ID");
			break;
		}
	}
}
