﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;
using Legrand.database;

public class DatabaseFormulaManager : DatabaseTools
{
	private FormulaDatabase formulaDatabase = ScriptableObject.CreateInstance<FormulaDatabase>();
    private ConstantPercentageValueDatabase ConstantValue = ScriptableObject.CreateInstance<ConstantPercentageValueDatabase>();

	public override void LoadDatabase ()
	{
		formulaDatabase = FormulaDatabase.GetDatabase <FormulaDatabase>(DATABASE_PATH,FileName.Get(0));
        ConstantValue = ConstantPercentageValueDatabase.GetDatabase <ConstantPercentageValueDatabase>(DATABASE_PATH,FileName.Get(1));
        ThisDatabase.Add(formulaDatabase);
        ThisDatabase.Add(ConstantValue);
    }
	[MenuItem("Legrand Legacy/Database/Formula")]
	private static void Init ()
	{
		DatabaseFormulaManager window = EditorWindow.GetWindow<DatabaseFormulaManager> ();
		window.minSize = new Vector2 (WINDOW_WIDTH,WINDOW_HEIGHT);
		window.titleContent.text = "Formula Database";
		window.Show ();
	}
	private DatabaseFormulaManager()
	{
		ManyTab = 2;
		//tab name and path for prefab
		//database name for scriptable object, none for prefab
		TabName.Set("Formula",0);
		FileName.Set (@"FormulaDatabase.asset",0);
        TabName.Set("Constant Value",1);
        FileName.Set (@"ConstantValueFormulaDatabase.asset",1);
	}
	private void OnEnable ()
	{
		LoadDatabase();
		CopyToTemp();
	}
	private void OnDisable ()
	{
		OnExit();
	}
	private void OnGUI ()
	{
		TopTabBar();
		switch(currentTab)
		{
		case 0:
			ScriptableObjectView<FormulaDatabase,FormulaDataModel>(formulaDatabase,"Name");
			break;
        case 1:
            ScriptableObjectView<ConstantPercentageValueDatabase,ConstantPercentageValueDataModel>(ConstantValue,"Name");
            break;
		}
	}
}
