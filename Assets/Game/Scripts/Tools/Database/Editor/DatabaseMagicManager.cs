﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;
using Legrand.database;
using System;

public class DatabaseMagicManager : DatabaseTools
{
    private MagicDatabase magic = ScriptableObject.CreateInstance<MagicDatabase>();

    public override void LoadDatabase ()
    {
        magic = MagicDatabase.GetDatabase <MagicDatabase>(DATABASE_PATH,FileName.Get(0));
        ThisDatabase.Add(magic);
    }
    [MenuItem("Legrand Legacy/Database/Magic")]
    private static void Init ()
    {
        DatabaseMagicManager window = EditorWindow.GetWindow<DatabaseMagicManager> ();
        window.minSize = new Vector2 (WINDOW_WIDTH,WINDOW_HEIGHT);
        window.titleContent.text = "Magic Database";
        window.Show ();
    }
    private DatabaseMagicManager()
    {
        ManyTab = 1;
        //tab name and path for prefab
        //database name for scriptable object, none for prefab
        TabName.Set("Magic",0);
        FileName.Set (@"AllMagicDatabase.asset",0);
    }
    private void OnEnable ()
    {
        LoadDatabase();
        CopyToTemp();
    }
    private void OnDisable ()
    {
        OnExit();
    }
    public override void RefreshThisReference()
    {
        RefreshReference<Magic>("ID", magic);
    }
    private void OnGUI ()
    {
        TopTabBar();
        switch(currentTab)
        {
            case 0:
                ScriptableObjectView<MagicDatabase,Magic>(magic, "ID", "Name");
                break;
        }
    }
}
