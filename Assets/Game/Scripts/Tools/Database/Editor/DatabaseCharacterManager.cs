using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.database;

public class DatabaseCharacterManager : DatabaseTools
{
	private PartyDatabase partyDatabase = ScriptableObject.CreateInstance<PartyDatabase>();
	private InventoryDatabase inventoryDatabase = ScriptableObject.CreateInstance<InventoryDatabase>();
    private CharacterLevelDatabase characterLevel = ScriptableObject.CreateInstance<CharacterLevelDatabase>();
    private AttributePointDatabase attribute = ScriptableObject.CreateInstance<AttributePointDatabase>();

    public override void LoadDatabase ()
	{
		partyDatabase =	PartyDatabase.GetDatabase<PartyDatabase>(DATABASE_PATH,FileName.Get(0));
		inventoryDatabase = InventoryDatabase.GetDatabase<InventoryDatabase>(DATABASE_PATH,FileName.Get(1));
        characterLevel = CharacterLevelDatabase.GetDatabase<CharacterLevelDatabase>(DATABASE_PATH,FileName.Get(2));
        attribute = AttributePointDatabase.GetDatabase<AttributePointDatabase>(DATABASE_PATH, FileName.Get(3));
        ThisDatabase.Add(partyDatabase);
        ThisDatabase.Add(inventoryDatabase);
        ThisDatabase.Add(characterLevel);
        ThisDatabase.Add(attribute);
    }
	[MenuItem("Legrand Legacy/Database/Character")]
	private static void Init ()
	{
		DatabaseCharacterManager window = EditorWindow.GetWindow<DatabaseCharacterManager> ();
		window.minSize = new Vector2 (WINDOW_WIDTH,WINDOW_HEIGHT);
		window.titleContent.text = "Character Database";
		window.Show ();
	}
	private DatabaseCharacterManager()
	{
		ManyTab = 4;
		//tab name and path for prefab
		//database name for scriptable object, none for prefab
		TabName.Set ("Party member", 0);
		FileName.Set (@"NewPartyMemberDatabase.asset", 0);

		TabName.Set ("Inventory", 1);
		FileName.Set (@"InventoryDatabase.asset", 1);

        TabName.Set ("CharacterLevel", 2);
        FileName.Set (@"CharacterLevelDatabase.asset", 2);

        TabName.Set("Attribute Point", 3);
        FileName.Set(@"AttributePointDatabase.asset", 3);

    }
	private void OnEnable ()
	{
		LoadDatabase();
		CopyToTemp();
	}
	private void OnDisable ()
	{
		OnExit();
	}
	private void OnGUI ()
	{
		TopTabBar();
		switch(currentTab)
		{
		case 0:
            ScriptableObjectView<PartyDatabase,PartyDataModelTest>(partyDatabase,"ID","Name");
			break;
		case 1:
			ScriptableObjectView<InventoryDatabase,InventoryModel>(inventoryDatabase,"NoList");
			break;
        case 2:
            ScriptableObjectView<CharacterLevelDatabase,CharacterLevel>(characterLevel,"ID_Character");
            break;
        case 3:
            ScriptableObjectView<AttributePointDatabase, AttributePoint>(attribute, "Level");
            break;
        }
	}
}
