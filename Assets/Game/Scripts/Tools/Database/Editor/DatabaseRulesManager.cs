﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;
using Legrand.database;

public class DatabaseRulesManager : DatabaseTools
{
    private RulesDatabase rulesDatabase = ScriptableObject.CreateInstance<RulesDatabase>();

    public override void LoadDatabase ()
    {
        rulesDatabase = RulesDatabase.GetDatabase<RulesDatabase>(DATABASE_PATH,FileName.Get(0));
        ThisDatabase.Add(rulesDatabase);
    }
    [MenuItem("Legrand Legacy/Database/Rules")]
    private static void Init ()
    {
        DatabaseRulesManager window = EditorWindow.GetWindow<DatabaseRulesManager> ();
        window.minSize = new Vector2 (WINDOW_WIDTH,WINDOW_HEIGHT);
        window.titleContent.text = "Rules Database";
        window.Show ();
    }
    private DatabaseRulesManager()
    {
        ManyTab = 1;
        //tab name and path for prefab
        //database name for scriptable object, none for prefab
        TabName.Set ("Rules", 0);
        FileName.Set (@"RulesDatabase.asset",0);
    }
    private void OnEnable ()
    {
        LoadDatabase();
        CopyToTemp();
    }
    private void OnDisable ()
    {
        OnExit();
    }
    private void OnGUI ()
    {
        TopTabBar();
        switch(currentTab)
        {
            case 0:
                ScriptableObjectView<RulesDatabase,Rule>(rulesDatabase,"Name", "AreaName");
                break;
        }
    }
}
