using UnityEditor;
using UnityEngine;
using Legrand.database;

public class DatabaseFencingManager : DatabaseTools
{
    private FencingParticipantStatusDatabase participant = ScriptableObject.CreateInstance<FencingParticipantStatusDatabase>();

    public override void LoadDatabase ()
    {
        participant = FencingParticipantStatusDatabase.GetDatabase <FencingParticipantStatusDatabase>(DATABASE_PATH,FileName.Get(0));
        ThisDatabase.Add(participant);
    }
    [MenuItem("Legrand Legacy/Database/Fencing")]
    private static void Init ()
    {
        DatabaseFencingManager window = EditorWindow.GetWindow<DatabaseFencingManager> ();
        window.minSize = new Vector2 (WINDOW_WIDTH,WINDOW_HEIGHT);
        window.titleContent.text = "MiniGame Database";
        window.Show ();
    }
    private DatabaseFencingManager()
    {
        ManyTab = 1;
        //tab name and path for prefab
        //database name for scriptable object, none for prefab
        TabName.Set("Participant",0);
        FileName.Set (@"MiniGameParticipantDatabase.asset",0);
    }
    private void OnEnable ()
    {
        LoadDatabase();
        CopyToTemp();
    }
    private void OnDisable ()
    {
        OnExit();
    }
    private void OnGUI ()
    {
        TopTabBar();
        switch(currentTab)
        {
            case 0:
                ScriptableObjectView<FencingParticipantStatusDatabase,FencingParticipantStatusData>(participant,"ID");
                break;
        }
    }
}
