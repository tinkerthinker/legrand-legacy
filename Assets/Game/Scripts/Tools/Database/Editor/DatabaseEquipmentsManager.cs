﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.database;
using System;

public class DatabaseEquipmentsManager : DatabaseTools
{
    private EquipmentsDatabase equipmentData = ScriptableObject.CreateInstance<EquipmentsDatabase>();

    public override void LoadDatabase()
    {
        equipmentData = EquipmentsDatabase.GetDatabase<EquipmentsDatabase>(DATABASE_PATH, FileName.Get(0));
        ThisDatabase.Add(equipmentData);
    }
    [MenuItem("Legrand Legacy/Database/Equipments")]
    private static void Init()
    {
        DatabaseEquipmentsManager window = EditorWindow.GetWindow<DatabaseEquipmentsManager>();
        window.minSize = new Vector2(WINDOW_WIDTH, WINDOW_HEIGHT);
        window.titleContent.text = "Equipments Database";
        window.Show();
    }
    private DatabaseEquipmentsManager()
    {
        ManyTab = 1;
        //tab name and path for prefab
        //database name for scriptable object, none for prefab
        TabName.Set("Equipments", 0);
        FileName.Set(@"EquipmentsDatabase.asset", 0);

    }
    private void OnEnable()
    {
        LoadDatabase();
        CopyToTemp();
    }
    private void OnDisable()
    {
        OnExit();
    }
    public override void RefreshThisReference()
    {
        RefreshReference<EquipmentDataModel>("ID", equipmentData);
    }
    private void OnGUI()
    {
        TopTabBar();
        switch (currentTab)
        {
            case 0:
            ScriptableObjectView<EquipmentsDatabase, EquipmentDataModel>(equipmentData, "ID", "Name");
            break;
        }
    }
}
