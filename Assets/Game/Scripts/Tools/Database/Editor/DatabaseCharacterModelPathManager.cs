﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;
using Legrand.database;

public class DatabaseCharacterModelPathManager : DatabaseTools
{
    private CharacterModelPathDatabase characterModel = ScriptableObject.CreateInstance<CharacterModelPathDatabase>();

    public override void LoadDatabase()
    {
        characterModel = CharacterModelPathDatabase.GetDatabase<CharacterModelPathDatabase>(DATABASE_PATH, FileName.Get(0));
        ThisDatabase.Add(characterModel);
    }
    [MenuItem("Legrand Legacy/Database/Character Model")]
    private static void Init()
    {
        DatabaseCharacterModelPathManager window = EditorWindow.GetWindow<DatabaseCharacterModelPathManager>();
        window.minSize = new Vector2(WINDOW_WIDTH, WINDOW_HEIGHT);
        window.titleContent.text = "Live2D Model";
        window.Show();
    }
    private DatabaseCharacterModelPathManager()
    {
        ManyTab = 1;
        //tab name and path for prefab
        //database name for scriptable object, none for prefab
        TabName.Set("Character Model", 0);
        FileName.Set(@"CharacterModelPathDatabase.asset", 0);
    }
    private void OnEnable()
    {
        LoadDatabase();
        CopyToTemp();
    }
    private void OnDisable()
    {
        OnExit();
    }
    private void OnGUI()
    {
        TopTabBar();
        switch (currentTab)
        {
            case 0:
            ScriptableObjectView<CharacterModelPathDatabase, CharacterModelPath>(characterModel, "Name");
            break;
        }
    }
}
