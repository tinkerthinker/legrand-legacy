﻿using UnityEditor;
using UnityEngine;
using Legrand.database;
using Legrand.War;

public class DatabaseWarCharacterDataModelManager : DatabaseTools
{
    private WarCharacterDataModelDatabase warCharacterDatabase = ScriptableObject.CreateInstance<WarCharacterDataModelDatabase>();
    private WarArcanaDataModelDatabase warArcanaDatabase = ScriptableObject.CreateInstance<WarArcanaDataModelDatabase>();
    private WarArcanaEffectDatabase warArcanaEffectDatabase = ScriptableObject.CreateInstance<WarArcanaEffectDatabase>();
    private WarBuffDatabase warBuffDatabase = ScriptableObject.CreateInstance<WarBuffDatabase>();

    public override void LoadDatabase ()
    {
        warCharacterDatabase = WarCharacterDataModelDatabase.GetDatabase <WarCharacterDataModelDatabase>(DATABASE_PATH,FileName.Get(0));
        warArcanaDatabase = WarArcanaDataModelDatabase.GetDatabase<WarArcanaDataModelDatabase>(DATABASE_PATH, FileName.Get(1));
        warArcanaEffectDatabase = WarArcanaEffectDatabase.GetDatabase<WarArcanaEffectDatabase>(DATABASE_PATH, FileName.Get(2));
        warBuffDatabase = WarBuffDatabase.GetDatabase<WarBuffDatabase>(DATABASE_PATH, FileName.Get(3));
        ThisDatabase.Add(warCharacterDatabase);
        ThisDatabase.Add(warArcanaDatabase);
        ThisDatabase.Add(warArcanaEffectDatabase);
        ThisDatabase.Add(warBuffDatabase);
    }
    [MenuItem("Legrand Legacy/Database/War Database")]
    private static void Init ()
    {
        DatabaseWarCharacterDataModelManager window = EditorWindow.GetWindow<DatabaseWarCharacterDataModelManager> ();
        window.minSize = new Vector2 (WINDOW_WIDTH,WINDOW_HEIGHT);
        window.titleContent.text = "War Database";
        window.Show ();
    }
    private DatabaseWarCharacterDataModelManager()
    {
        ManyTab = 4;
        //tab name and path for prefab
        //database name for scriptable object, none for prefab
        TabName.Set("War Character",0);
        FileName.Set (@"WarCharacterDatabase.asset",0);

        TabName.Set("War Arcana", 1);
        FileName.Set(@"WarArcanaDatabase.asset", 1);

        TabName.Set("War Arcana Effect", 2);
        FileName.Set(@"WarArcanaEffectDatabase.asset", 2);

        TabName.Set("War Buff", 3);
        FileName.Set(@"WarBuffDatabase.asset", 3);
    }
    private void OnEnable ()
    {
        LoadDatabase();
        CopyToTemp();
    }
    private void OnDisable ()
    {
        OnExit();
    }
    private void OnGUI ()
    {
        TopTabBar();
        switch(currentTab)
        {
            case 0:
                ScriptableObjectView<WarCharacterDataModelDatabase, WarCharacterDataModel>(warCharacterDatabase,"ID","Name");
                break;
            case 1:
            ScriptableObjectView<WarArcanaDataModelDatabase, WarArcanaDataModel>(warArcanaDatabase, "ID", "Name");
                break;
            case 2:
            ScriptableObjectView<WarArcanaEffectDatabase, WarArcanaEffect>(warArcanaEffectDatabase, "type");
                break;
            case 3:
            ScriptableObjectView<WarBuffDatabase, WarBuff>(warBuffDatabase, "ID", "Name");
            break;
        }
    }
}
