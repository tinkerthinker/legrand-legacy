﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;
using Legrand.database;

public class DatabaseShopManager : DatabaseTools
{
	private ShopModelDatabase shopDatabase = ScriptableObject.CreateInstance<ShopModelDatabase>();
    private MerchantLevelDatabase MerchantLevelDatabase = ScriptableObject.CreateInstance<MerchantLevelDatabase>();
    private BluePrintDatabase blueprintDatabase	= ScriptableObject.CreateInstance<BluePrintDatabase>();

	public override void LoadDatabase ()
	{
		shopDatabase =	ShopModelDatabase.GetDatabase <ShopModelDatabase>(DATABASE_PATH,FileName.Get(0));
        MerchantLevelDatabase = ShopModelDatabase.GetDatabase<MerchantLevelDatabase>(DATABASE_PATH, FileName.Get(1));
        blueprintDatabase = BluePrintDatabase.GetDatabase<BluePrintDatabase>(DATABASE_PATH,FileName.Get(2));
        ThisDatabase.Add(shopDatabase);
        ThisDatabase.Add(MerchantLevelDatabase);
        ThisDatabase.Add(blueprintDatabase);
	}
	[MenuItem("Legrand Legacy/Database/Shop")]
	private static void Init ()
	{
		DatabaseShopManager window = EditorWindow.GetWindow<DatabaseShopManager> ();
		window.minSize = new Vector2 (WINDOW_WIDTH,WINDOW_HEIGHT);
		window.titleContent.text = "Shop Database";
		window.Show ();
	}
	private DatabaseShopManager()
	{
		ManyTab = 3;
		//tab name and path for prefab
		//database name for scriptable object, none for prefab
		TabName.Set ("Shop", 0);
		FileName.Set (@"ShopModelDatabase.asset",0);

		TabName.Set ("Merchant Level", 1);
		FileName.Set (@"MerchantLevelDatabase.asset",1);

        TabName.Set("Blueprint", 2);
        FileName.Set(@"BlueprintDatabase.asset", 2);

        TabName.Set ("Alchemy", 3);

		TabName.Set ("Blacksmith", 4);


	}
	private void OnEnable ()
	{
		LoadDatabase();
		CopyToTemp();
	}
	private void OnDisable ()
	{
		OnExit();
	}
	private void OnGUI ()
	{
		TopTabBar();
		switch(currentTab)
		{
		case 0:
			ScriptableObjectView<ShopModelDatabase,ShopDataModelTest>(shopDatabase,"_ID");
			break;
		case 1:
			ScriptableObjectView<MerchantLevelDatabase,MerchantLevel>(MerchantLevelDatabase,"ID");
			break;
        case 2:
            ScriptableObjectView<BluePrintDatabase, BluePrint>(blueprintDatabase, "ID");
            break;
        }
	}
}
