﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;
using Legrand.database;

public class DatabaseTutorialManager : DatabaseTools
{
	private TutorialStepsDatabase tutorialDatabase = ScriptableObject.CreateInstance<TutorialStepsDatabase>();

	public override void LoadDatabase ()
	{
		tutorialDatabase = TutorialStepsDatabase.GetDatabase<TutorialStepsDatabase>(DATABASE_PATH,FileName.Get(0));
        ThisDatabase.Add(tutorialDatabase);
	}
	[MenuItem("Legrand Legacy/Database/Tutorial")]
	private static void Init ()
	{
		DatabaseTutorialManager window = EditorWindow.GetWindow<DatabaseTutorialManager> ();
		window.minSize = new Vector2 (WINDOW_WIDTH,WINDOW_HEIGHT);
		window.titleContent.text = "Tutorial Database";
		window.Show ();
	}
	private DatabaseTutorialManager()
	{
		ManyTab = 1;
		//tab name and path for prefab
		//database name for scriptable object, none for prefab
		TabName.Set ("Tutorial", 0);
		FileName.Set (@"TutorialData.asset",0);
	}
	private void OnEnable ()
	{
		LoadDatabase();
		CopyToTemp();
	}
	private void OnDisable ()
	{
		OnExit();
	}
	private void OnGUI ()
	{
		TopTabBar();
		switch(currentTab)
		{
		case 0:
			ScriptableObjectView<TutorialStepsDatabase,TutorialSteps>(tutorialDatabase,"Name");
			break;
		}
	}
}
