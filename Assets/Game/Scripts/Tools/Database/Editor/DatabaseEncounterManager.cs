using UnityEditor;
using UnityEngine;
using System.Collections.Generic;
using Legrand.database;
using System;

public class DatabaseEncounterManager : DatabaseTools
{
	private EncounterDatabase encounterDatabase	= ScriptableObject.CreateInstance<EncounterDatabase>();
	private EncounterPartyDatabase encounterPartyDatabase = ScriptableObject.CreateInstance<EncounterPartyDatabase>();
	private EncounterGrowthDatabase encounterGrowthDatabase = ScriptableObject.CreateInstance<EncounterGrowthDatabase>();
	
	public override void LoadDatabase ()
	{
		encounterDatabase = EncounterDatabase.GetDatabase<EncounterDatabase>(DATABASE_PATH,FileName.Get(0));
		encounterPartyDatabase = EncounterPartyDatabase.GetDatabase<EncounterPartyDatabase>(DATABASE_PATH,FileName.Get(1));
		encounterGrowthDatabase = EncounterGrowthDatabase.GetDatabase<EncounterGrowthDatabase>(DATABASE_PATH,FileName.Get(2));
        ThisDatabase.Add(encounterDatabase);
        ThisDatabase.Add(encounterPartyDatabase);
        ThisDatabase.Add(encounterGrowthDatabase);
    }
	[MenuItem("Legrand Legacy/Database/Encounter")]
	private static void Init ()
	{
		DatabaseEncounterManager window = EditorWindow.GetWindow<DatabaseEncounterManager> ();
		window.minSize = new Vector2 (WINDOW_WIDTH,WINDOW_HEIGHT);
		window.titleContent.text = "Encounter Database";
		window.Show ();
	}
	private DatabaseEncounterManager()
	{
		ManyTab = 3;
		//tab name and path for prefab
		//database name for scriptable object, none for prefab
		TabName.Set ("Encounter", 0);
		FileName.Set (@"EncounterDatabase.asset",0);
		
		TabName.Set ("Encounter Party", 1);
		FileName.Set (@"EncounterPartyDatabase.asset",1);

		TabName.Set ("Encounter Growth", 2);
		FileName.Set (@"EncounterGrowthDatabase.asset",2);
	}
	private void OnEnable ()
	{
		LoadDatabase();
		CopyToTemp();
	}
	private void OnDisable ()
	{
		OnExit();
	}
    public override void RefreshThisReference()
    {
        RefreshReference<EncounterDataModel>("ID", encounterDatabase);
        RefreshReference<EncounterGrowth>("Name", encounterGrowthDatabase);
    }
    private void OnGUI ()
	{
		TopTabBar();
		switch(currentTab)
		{
		case 0:
            ScriptableObjectView<EncounterDatabase,EncounterDataModel>(encounterDatabase,"ID","Name");
			break;
		case 1:
            ScriptableObjectView<EncounterPartyDatabase,EncounterPartyDataModelTest>(encounterPartyDatabase,"_ID","Name");
			break;
		case 2:
			ScriptableObjectView<EncounterGrowthDatabase,EncounterGrowth>(encounterGrowthDatabase,"Name");
			break;
		}
	}
}
