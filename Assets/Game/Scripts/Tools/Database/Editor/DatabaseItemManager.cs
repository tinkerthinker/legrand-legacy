using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;
using Legrand.database;

public class DatabaseItemManager : DatabaseTools
{
	private ConsumableHealingsDatabase consumableHealingDatabase = ScriptableObject.CreateInstance<ConsumableHealingsDatabase>();
	private ConsumableItemAttributeDatabase consumableAttribute = ScriptableObject.CreateInstance<ConsumableItemAttributeDatabase>();
	private MaterialItemDatabase materialItemDatabase = ScriptableObject.CreateInstance<MaterialItemDatabase>();
	private MagicItemDatabase magicDatabase = ScriptableObject.CreateInstance<MagicItemDatabase>();
	private ConsumableMiscDatabase miscDatabase = ScriptableObject.CreateInstance<ConsumableMiscDatabase>();
	private KeyDatabase keyDatabase	= ScriptableObject.CreateInstance<KeyDatabase>();
    private LootChanceDatabase lootChanceDatabase = ScriptableObject.CreateInstance<LootChanceDatabase>();

	public override void LoadDatabase ()
	{
		consumableHealingDatabase =	ConsumableHealingsDatabase.GetDatabase <ConsumableHealingsDatabase>(DATABASE_PATH,FileName.Get(0));
		consumableAttribute = ConsumableItemAttributeDatabase.GetDatabase <ConsumableItemAttributeDatabase>(DATABASE_PATH,FileName.Get(1));
		materialItemDatabase =	MaterialItemDatabase.GetDatabase <MaterialItemDatabase>(DATABASE_PATH,FileName.Get(2));
		magicDatabase = MagicItemDatabase.GetDatabase <MagicItemDatabase>(DATABASE_PATH,FileName.Get(3));
		miscDatabase = ConsumableMiscDatabase.GetDatabase<ConsumableMiscDatabase>(DATABASE_PATH,FileName.Get(4));
		keyDatabase = KeyDatabase.GetDatabase<KeyDatabase>(DATABASE_PATH,FileName.Get(5));
        lootChanceDatabase = LootChanceDatabase.GetDatabase<LootChanceDatabase>(DATABASE_PATH,FileName.Get(6));
        ThisDatabase.Add(consumableHealingDatabase);
        ThisDatabase.Add(consumableAttribute);
        ThisDatabase.Add(materialItemDatabase);
        ThisDatabase.Add(magicDatabase);
        ThisDatabase.Add(miscDatabase);
        ThisDatabase.Add(keyDatabase);
        ThisDatabase.Add(lootChanceDatabase);
    }
	[MenuItem("Legrand Legacy/Database/Item")]
	private static void Init ()
	{
		DatabaseItemManager window = EditorWindow.GetWindow<DatabaseItemManager> ();
		window.minSize = new Vector2 (WINDOW_WIDTH,WINDOW_HEIGHT);
		window.titleContent.text = "Item Database";
		window.Show ();
	}
	private DatabaseItemManager()
	{
		ManyTab = 7;
		//tab name and path for prefab
		//database name for scriptable object, none for prefab
		TabName.Set ("Healing", 0);
		FileName.Set (@"ConsumableHealingDatabase.asset", 0);

		TabName.Set ("ConsumableAttributes", 1);
		FileName.Set (@"ConsumableAttributesDatabase.asset", 1);

		TabName.Set ("Material", 2);
		FileName.Set (@"MaterialItemDatabase.asset", 2);

		TabName.Set ("Magic", 3);
		FileName.Set (@"MagicDatabase.asset", 3);

		TabName.Set ("Misc", 4);
		FileName.Set(@"MiscDatabase.asset",4);

		TabName.Set ("Key", 5);
		FileName.Set (@"KeyDatabase.asset",5);

        TabName.Set ("Encounter Loot Chance", 6);
        FileName.Set (@"LootChanceDatabase.asset",6);
	}
	private void OnEnable ()
	{
		LoadDatabase();
		CopyToTemp();
	}
	private void OnDisable ()
	{
		OnExit();
	}
	private void OnGUI ()
	{
		TopTabBar();
		switch(currentTab)
		{
		case 0:
            ScriptableObjectView<ConsumableHealingsDatabase,ConsumableHealing>(consumableHealingDatabase,"_ID","Name","Target");
			break;
		case 1:
                ScriptableObjectView<ConsumableItemAttributeDatabase,ConsumableAttribute>(consumableAttribute,"_ID","Name","Target");
			break;
		case 2:
                ScriptableObjectView<MaterialItemDatabase,CraftingMaterial>(materialItemDatabase,"_ID","Name");
			break;
		case 3:
                ScriptableObjectView<MagicItemDatabase,ConsumableMagic>(magicDatabase,"_ID","Name","Target");
			break;
		case 4:
                ScriptableObjectView<ConsumableMiscDatabase,ConsumableMisc>(miscDatabase,"_ID","Name","Target");
			break;
		case 5:
            ScriptableObjectView<KeyDatabase,Key>(keyDatabase,"_ID","Name");
			break;
        case 6:
                ScriptableObjectView<LootChanceDatabase,LootChanceDataModel>(lootChanceDatabase,"NoList","Name");
            break;
		}
	}
}
