using System;

namespace Legrand.database
{
	public interface IComponentDatabase
	{
        string DatabaseID{ get; set; }
		int Count{ get; }
        object GetObjectFieldValue(int index, string fieldName);
		string GetFieldValue(int index, string fieldName);
		int GetEnumFieldValue(int index, string fieldName);
        object GetItem(int index);
		void Modify(int index,DataAction act);
		void refresh(bool edited = true);
		void refreshElement(bool edited = true);
        void Clear();
		void Sort(string field,string mode);
        bool isEdited { get; set; }
    }
}