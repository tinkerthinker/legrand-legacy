﻿using UnityEngine;
using System.Collections;

namespace Legrand.database
{
	public interface IGetAndAddDatabase<T>
	{
		T Get(int index);
		void AddValue(T obj);
        void SetItem(int index,T data);
    }
}