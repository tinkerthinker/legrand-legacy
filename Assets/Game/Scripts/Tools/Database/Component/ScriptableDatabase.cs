#if UNITY_EDITOR
using UnityEditor;
#endif

using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;
using System;

namespace Legrand.database
{
	public enum DataAction {ADD,INSERT,REMOVE};
	[System.Serializable]
    public class ScriptableDatabase<T> : ScriptableObject, IComponentDatabase,IGetAndAddDatabase<T>
	{
        bool _IsEdited = false;

		[SerializeField]private List<T> database = new List<T>();
		
        [SerializeField]string _DatabaseID = "";

        public string DatabaseID
        {
            get
            {
                return _DatabaseID;
            }
            set
            {
                _DatabaseID = value;
            }
        }

		public string GetFieldValue(int index, string fieldName)
		{
            if (fieldName.Equals(""))
                return "";
			BindingFlags bindFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
			FieldInfo field = database[index].GetType().GetField(fieldName,bindFlags);
			return field.GetValue(database[index]).ToString();
		}

		public int GetEnumFieldValue(int index, string fieldName)
		{
			BindingFlags bindFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
			FieldInfo field = database[index].GetType().GetField(fieldName,bindFlags);
			return (int)field.GetValue(database[index]);
		}

        public object GetObjectFieldValue(int index, string fieldName)
        {
            BindingFlags bindFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
            FieldInfo field = database[index].GetType().GetField(fieldName,bindFlags);
            return field.GetValue(database[index]);
        }

        public object GetItem(int index)
        {
            return database[index];
        }

        public void SetItem(int index,T data)
        {
            database[index] = data;
        }

        public void Modify(int index,DataAction act)
		{
            switch(act)
			{
				case DataAction.ADD:
				{
                    database.Add(default(T));
					refresh();
				}
				break;
                case DataAction.INSERT:
				{
                    database.Insert(index + 1,default(T));
					refresh();
				}
				break;
				case DataAction.REMOVE:
				{
					database.RemoveAt(index);
					refresh();
				}
				break;
			}
		}
        public void RemoveAt(int index)
        {
            database.RemoveAt(index);
        }
		public void AddValue(T obj)
		{
			database.Add(obj);
		}
		public void refreshElement(bool edited = true)
		{
			#if UNITY_EDITOR
			EditorUtility.SetDirty(this);
            if (edited)
            {
                _IsEdited = edited;
                Debug.Log("edit element");
            }
            #endif
        }
		
		public void Sort (string field,string mode)
		{
			Sort_List(mode,field,database);
			refresh();
		}
		private object GetDynamicSortProperty(object item, string propName)
		{
			return item.GetType().GetField(propName).GetValue(item);
		}
		private void Sort_List(string sortDirection, string sortExpression, List <T> data)
		{
			List<T> data_sorted = new List<T>();
			if (sortDirection == "Ascending")
			{
				data_sorted = (from n in data
				               orderby GetDynamicSortProperty(n, sortExpression) ascending
				               select n).ToList();
			}
			else if (sortDirection == "Descending")
			{
				data_sorted = (from n in data
				               orderby GetDynamicSortProperty(n, sortExpression) descending
				               select n).ToList();
			}
			database = data_sorted;
		}
		public void refresh(bool edited = true)
		{
			#if UNITY_EDITOR
			EditorUtility.SetDirty(this);
			AssetDatabase.SaveAssets();
			AssetDatabase.Refresh();
            if (edited)
            {
                _IsEdited = edited;
                Debug.Log("edit database");
            }
#endif
        }
		public void Clear()
		{
			database.Clear();
		}
		public int Count
		{
			get {return database.Count;}
		}

        public bool isEdited
        {
            get
            {
                return _IsEdited;
            }

            set
            {
                _IsEdited = value;
            }
        }

        public T Get(int index)
		{
            if(database.Count > index)
			    return database[index];
            return default(T);
		}

		public static U GetDatabase<U>(string dbPath, string dbName) where U: ScriptableObject
		{
			#if UNITY_EDITOR
			string dbFullPath = @"Assets/" + dbPath + "/" + dbName;

			U db = AssetDatabase.LoadAssetAtPath(dbFullPath, typeof(U)) as U;

			if(db == null)
			{
				if(!AssetDatabase.IsValidFolder("Assets/" + dbPath))
				{
					AssetDatabase.CreateFolder("Assets", dbPath);
				}

				db = ScriptableObject.CreateInstance<U>() as U;
				AssetDatabase.CreateAsset(db, dbFullPath);
				AssetDatabase.SaveAssets();
				AssetDatabase.Refresh();
			}
			return db;
			#else
			return null;
			#endif
		}
        
    }
}