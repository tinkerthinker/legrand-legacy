﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;

namespace Legrand.database
{
	[System.Serializable]
	public class FormulaDataModel
	{
        [System.Serializable]
        public class Parameter
        {
            public string Name;
            public string valueTest;

            public Parameter()
            {
                Name = "";
                valueTest = "0";
            }
        }

		public string Name;
        public List<Parameter> ParameterVariable;
		public string Formula;

		public FormulaDataModel()
		{
			Name = "";
            ParameterVariable = new List<Parameter>();
			Formula = "";
		}
	}
}
