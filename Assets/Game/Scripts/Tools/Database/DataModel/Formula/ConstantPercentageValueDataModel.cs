﻿using UnityEngine;
using System.Collections.Generic;

namespace Legrand.database
{
    [System.Serializable]
    public class ConstantPercentageValueDataModel
    {
        public string Name;
        public float Value;
        
        public ConstantPercentageValueDataModel()
        {
            Name = "";
            Value = 0f;
        }
    }
}
