﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class ElementCircle
{
    public Element CurrentElement;
    public Element WeakerTo;
    public Element StrongerTo;

    public ElementCircle()
    {
        CurrentElement = Element.Fire;
        WeakerTo = Element.Fire;
        StrongerTo = Element.Fire;
    }
}
