﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Legrand.War
{
    [System.Serializable]
    public class WarArcana
    {
        public string Name;
        public string Description;
        public int TargetRange;
        public int AreaRange;
        public float DamageModifier;
        public bool StraightRangeOnly;
        public bool LineArea;
        public bool TargetFriendly;
        public List<WarArcanaEffect> Effects;

        public WarArcana()
        {
            Name = "Power Strike";
            Description = "Attack a single grid within range with high damage";
            TargetRange = 1;
            AreaRange = 0;
            DamageModifier = 2.0f;
            StraightRangeOnly = false;
            LineArea = false;
            TargetFriendly = false;
            Effects = new List<WarArcanaEffect>();
        }

        public WarArcana(WarArcanaDataModel model)
        {
            Name = model.Name;
            Description = model.Description;
            TargetRange = model.TargetRange;
            AreaRange = model.AreaRange;
            DamageModifier = model.DamageModifier;
            StraightRangeOnly = model.StraightRangeOnly;
            LineArea = false;
            TargetFriendly = model.TargetFriendly;
            Effects = model.ArcanaEffects;
        }
    }

    [System.Serializable]
    public class WarArcanaEffect
    {
        public WarArcanaEffectType type;
        public WarBuff AppliedBuff; //===>>>> ini yang jadi dropdown warbuff dari database
        public float chance;
        public float statModifierAmount; //morale boosted, ATK and DEF decrease amount in percent (0-100)
        public int power; //debuff duration, boost/cut value, teleport/knockback range

        public WarArcanaEffect()
        {
            type = WarArcanaEffectType.Burn;
            AppliedBuff = new WarBuff();
            chance = 0;
            statModifierAmount = 0;
            power = 0;
        }
    }


    public enum WarArcanaEffectType
    {
        Knockback,
        Teleported,
        Burn,
        ACBoost,
        ACCut,
        MoraleBoost,
        ATKDown,
        DEFDown,
        Blind,
        Multihit,
        Paralyze
    }
}
