﻿using UnityEngine;
using System.Collections;

namespace Legrand.War
{
    public enum PassiveSkillType
    {
        Counter,
        APCharge,
        APDrain,
        ACPlus,
        Taunt,
        Evasion,
        CounterHeal
    }

    [System.Serializable]
    public struct PassiveTypeAndChance
    {
        public PassiveSkillType Type;
        public int Chance;
    }
}
