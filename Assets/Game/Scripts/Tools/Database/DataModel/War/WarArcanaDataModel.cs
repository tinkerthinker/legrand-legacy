﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.War;

[System.Serializable]
public class WarArcanaDataModel
{
    public string ID;
    public string Name;
    public string Description;
    public int TargetRange;
    public int AreaRange;
    public float DamageModifier;
    public List<WarArcanaEffect> ArcanaEffects;
    public bool StraightRangeOnly;
    public bool TargetFriendly;

    public WarArcanaDataModel()
    {
        ID = "";
        Name = "";
        Description = "";
        TargetRange = 0;
        AreaRange = 0;
        DamageModifier = 0;
        StraightRangeOnly = false;
        TargetFriendly = false;
        ArcanaEffects = new List<WarArcanaEffect>();
    }
}
