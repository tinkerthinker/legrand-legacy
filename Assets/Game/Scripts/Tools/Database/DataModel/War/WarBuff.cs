﻿using UnityEngine;
using System.Collections;
using Legrand.core;
using UnityEngine.UI;

namespace Legrand.War
{
    [System.Serializable]
    public class WarBuff
    {
        public string ID;
        public string Name;
        public WarBuffType Type;
        public bool IsBuff; //true = buff, false = debuff
        public AttributeBuffed AttributeModified;
        public float modifier;
        public WarBuffTriggerType triggerType;
        public Sprite Icon;
        public int duration;
        public WarBuff()
        {
            ID = "";
            Name = "";
            Type = WarBuffType.ActionSkipper;
            IsBuff = false;
            AttributeModified = AttributeBuffed.AGI;
            modifier = 0f;
            triggerType = WarBuffTriggerType.FieldBound;
            Icon = new Sprite();
            duration = 0;
        }
    }

    public enum WarBuffType
    {
        HealthModifier,
        ActionSkipper,
        StatModifier
    }

    public enum WarBuffTriggerType
    {
        OnStartTurn,
        Ongoing,
        FieldBound,
        FieldBoundPeriodic
    }

    public enum AttributeBuffed
    {
        Health,
        STR,
        DEF,
        AGI,
        HIT,
        AllCoreStat,
        None
    }
}
