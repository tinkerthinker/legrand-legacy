﻿using UnityEngine;
using System.Collections;
using Legrand.core;
using Legrand.War;

[System.Serializable]
public class WarBuffDataModel
{
    public string ID;
    public string Name;
    public WarBuffType Type;
    public bool IsBuff; //true = buff, false = debuff
    public AttributeBuffed AttributeModified;
    public float modifier;
    public WarBuffTriggerType triggerType;
    public Sprite Icon;
    public int duration;

    public WarBuffDataModel()
    {
        ID = "";
        Type = WarBuffType.HealthModifier;
        IsBuff = false;
        AttributeModified = AttributeBuffed.None;
        modifier = 0;
        triggerType = WarBuffTriggerType.Ongoing;
        Icon = null;
        duration = 0;
    }
}