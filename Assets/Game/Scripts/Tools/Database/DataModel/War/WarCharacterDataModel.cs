﻿using UnityEngine;
using System.Collections;
using Legrand.War;

[System.Serializable]
public class WarCharacterDataModel{
    public string ID;
    public string Name;
    public WarClass Class;
    public string ArcanaID; 
    public WarArcanaDataModel Arcana; //===>>> ini yang jadi dropdown arcana dari database

    public int MaxAC;
    public int ACRegen;
    public int MaxAP;
    public PassiveTypeAndChance PassiveSkill;

    // Optional
    public int HP;
    public int AP;
    public int AC;
    public int STR;
    public int DEF;
    public int AGI;
    public int HIT;
    public int Troops;

    public WarCharacterDataModel()
    {
        ID = "";
        Name = "";
        Class = WarClass.Mercenary;
        ArcanaID = "";
        PassiveSkill.Type = PassiveSkillType.Counter;
        PassiveSkill.Chance = -1;
        MaxAC = 10;
        ACRegen = 3;
        MaxAP = 100;
        Arcana = new WarArcanaDataModel();
        HP = -1;
        AP = -1;
        AC = -1;
        STR = -1;
        DEF = -1;
        AGI = -1;
        HIT = -1;
        Troops = -1;
    }
}
