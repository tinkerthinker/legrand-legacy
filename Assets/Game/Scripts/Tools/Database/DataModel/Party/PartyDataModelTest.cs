﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;

namespace Legrand.database
{
	[System.Serializable]
	public class PartyDataModelTest
	{
		public string ID;
		public AttackType AttackType;
		public string Name;
        public int Level;
        public Weapon.DamageType WeaponType;
		public Armor.ArmorType ArmorType;
		public float Health;
		public int Gauge;
        public int ClassLevel;
        public Vector2 Formation;

		public List<BattleAttributeTest> NewGameBattleAttributes;
		public List<ElementModelTest> NewGameCharacterElements;
        public List<LockedMagic> LockedMagic;
        public List<LockedSkill> LockedSkill;
		public List<CommandDataTest> NewGameCommands;
		public List<CharacterItemModelTest> NewGameCharacterItems;
        public EquipmentDataModel Equipment;

        public List<BattleGrowthDataTest> BattleGrowth;
		
		public PartyDataModelTest()
		{
			ID = "";
            Equipment = new EquipmentDataModel();
            AttackType = AttackType.Melee;
			Name = "";
			WeaponType = Weapon.DamageType.Slash;
			ArmorType = Armor.ArmorType.Light;
			Health = 0;
			Gauge = 0;
            Level = 0;
            ClassLevel = 1;
			NewGameBattleAttributes = new List<BattleAttributeTest>();
			LockedSkill = new List<LockedSkill>();
            LockedMagic = new List<LockedMagic>();
			NewGameCharacterElements = new List<ElementModelTest>();
			NewGameCommands = new List<CommandDataTest>();
			NewGameCharacterItems = new List<CharacterItemModelTest>();
            Formation = new Vector2(-1f, -1f);
            BattleGrowth = new List<BattleGrowthDataTest>();
		}
	}
}
