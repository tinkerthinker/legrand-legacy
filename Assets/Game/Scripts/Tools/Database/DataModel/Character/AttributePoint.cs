﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class AttributePoint
{
    public int Level;
    public int Point;
}
