﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;

namespace Legrand.database
{
	[System.Serializable]
	public class InventoryModel
	{
		[System.Serializable]
		public class ItemID
		{
			public string AllItemID;
			public int Stack;
			
			public ItemID()
			{
				AllItemID = "";
				Stack = 0;
			}
		}
		
		public int NewGameCash;
		public List<ItemID> item = new List<ItemID>();
		
		public InventoryModel()
		{
			NewGameCash = 0;
		}
	}
}