﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;

namespace Legrand.database
{
	[System.Serializable]
	public class EncounterLootModelTest{
		public int ID;
		public string AllItemID;
		public int Amount;
		public int Chance;
		
		public EncounterLootModelTest()
		{
			ID = 0;
			AllItemID = "";
			Amount = 0;
			Chance = 0;
		}
	}

	[System.Serializable]
	public class CharacterItemModelEncounterTest
	{
		public int Index;
		public string HealingAndMagicItemID;
		public int Stack;
		
		public CharacterItemModelEncounterTest()
		{
			Index = 0;
			HealingAndMagicItemID = "";
			Stack = 0;
		}
	}

	[System.Serializable]
	public class MagicPresetModelEncounterTest{
		public int Id;
		public int MagicPresetID;
		public string Name;
		public string Description;
		public Element Element;
		public TargetType TargetType;
		public float MagicDamageModifier;
		public int MagicDelayModifier;
		public int MagicCostModifier;
		public string CastingMagicEffect;
		public string AttackMagicEffect;
		public string HitEffect;
		public bool IsNameDefault;
		
		public MagicPresetModelEncounterTest()
		{
			Id = 0;
			MagicPresetID = 0;
			Name = "";
			Description = "";
			Element = Element.Air;
			TargetType = TargetType.Single;
			MagicDamageModifier = 0;
			MagicDelayModifier = 0;
			MagicCostModifier = 0;
			CastingMagicEffect = "";
			AttackMagicEffect = "";
			HitEffect = "";
			IsNameDefault = false;
		}
	}

	[System.Serializable]
	public class BattleAttributeTest 
	{
		public Attribute _Attribute; //Attribute
		public int _Value;
		public int _Experiences;
		public string _Description;
		
		public BattleAttributeTest()
		{
			_Attribute = Attribute.STR;
			_Value = 0;
			_Experiences = 0;
			_Description = "";
		}

//		public static BattleAttributeTest copy (BattleAttributeTest btOld){
//			BattleAttribute bt = new BattleAttribute ();
//			bt._Attribute = btOld._Attribute;
//			bt._Value = btOld._Value;
//			bt._Experiences = btOld._Experiences;
//			bt._Description = btOld._Description;
//
//			return bt;
//		}
	}
//
//	public static BattleAttributeTest Copy (BattleAttributeTest oldCommand)
//	{
//		
//	}
}
