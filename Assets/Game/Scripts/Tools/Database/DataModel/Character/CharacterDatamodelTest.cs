using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;

namespace Legrand.database
{
	[System.Serializable]
	public class ElementModelTest{
		public int Id;
		public Element Element;
		
		public ElementModelTest()
		{
			Element = Element.Fire;
		}
	}

	[System.Serializable]
	public class MagicPresetModelTest{
		public int MagicPresetID;
		public string Name;
		public string Description;
		public Element Element;
		public TargetType TargetType;
		public float MagicDamageModifier;
		public int MagicDelayModifier;
		public int MagicCostModifier;
		public string CastingMagicEffect;
		public string AttackMagicEffect;
		public string HitEffect;
		public bool IsNameDefault;
		
		public MagicPresetModelTest()
		{
			MagicPresetID = 0;
			Name = "";
			Description = "";
			Element  = Element.Fire;
			TargetType = TargetType.Single;
			MagicDamageModifier = 0;
			MagicDelayModifier = 0;
			MagicCostModifier = 0;
			CastingMagicEffect = "";
			AttackMagicEffect = "";
			HitEffect = "";
			IsNameDefault = false;
		}
	}

	[System.Serializable]
	public class CommandDataTest
	{
		public int Index;
		public string CommandName;
		public CommandPos PanelPos;
		public CommandPos SlotPos;
		public CommandTypes CommandType;
		public int CommandValue;
		public CommandDataTest()
		{
			Index = 0;
			CommandName = "";
			PanelPos = CommandPos.Up;
			SlotPos = CommandPos.Up;
			CommandType  = CommandTypes.Attack;
			CommandValue = 0;
		}
	}

	[System.Serializable]
	public class CharacterItemModelTest
	{
		public int Index;
		public string HealingAndMagicItemID;
		public int Stack;
		
		public CharacterItemModelTest()
		{
			Index = 0;
			HealingAndMagicItemID = "";
			Stack = 0;
		}
	}

	[System.Serializable]
	public class EquipmentUpgradeModelTest{
		public float Value;
		public int Type;
		public int Index;
		public int UpgradeID;
		
		public EquipmentUpgradeModelTest()
		{
			Value = 0;
			Type = 0;
			Index = 0;
			UpgradeID = 0;
		}
	}

	[System.Serializable]
	public class BattleFormationModelTest
	{
		public int ID;
		public Vector2 Formation;
		
		public BattleFormationModelTest()
		{
			ID = 0;
			Formation = new Vector2(0f,1f);
		}
	}

	[System.Serializable]
	public class BattleGrowthDataTest{
		public Attribute attributeType;
		public float value;

		public BattleGrowthDataTest()
		{
			attributeType  = Attribute.STR;
			value = 0;
		}
	}

//	[System.Serializable]
//	public class SkillTest  
//	{
//		// General description
//		public int _ID;
//		public string Name;
//		public string Description;
//		
//		public SkillTest()
//		{
//			// General description
//			Name = "";
//			Description = "";
//		}
//		
//	}
//
//	[System.Serializable]
//	public class DefensiveBattleSkillTest : SkillTest
//	{
//		public List<Buff> Buffs;
//		public TargetFormation TargetFormationType;
//		public SkillTypes SkillType;
//		public TypeDamage DamageType;
//		public TargetType TargetTypes;
//		
//		public DefensiveBattleSkillTest()
//		{
//			TargetFormationType = TargetFormation.Self;
//			Buffs = new List<Buff> ();
//			TargetTypes = TargetType.Single;
//			SkillType = SkillTypes.UltimateDefense;
//			DamageType = TypeDamage.Magic;
//		}
//	}
//
//	[System.Serializable]
//	public class OffensiveBattleSkillTest : SkillTest
//	{
//		public List<Buff> Buffs;
//		public TargetType Target;
//		public float DamagePercentage;
//		public SkillTypes SkillType;
//		public TypeDamage DamageType;
//		public Element Element;
//		
//		public OffensiveBattleSkillTest()
//		{
//			Target = TargetType.Single;
//			DamagePercentage = 0;
//			Element = Element.Fire;
//			Buffs = new List<Buff>();
//			SkillType = SkillTypes.UltimateOffense;
//			DamageType = TypeDamage.Physical;
//		}
//	}

    [System.Serializable]
    public class BaseEquipmentStat
    {
        public Attribute AttributeType;
        public float Value;

        public BaseEquipmentStat()
        {
            AttributeType = Attribute.BaseDamage;
            Value = 0f;
        }
    }

    [System.Serializable]
	public class CharacterDatamodelTest
	{
		public string ID;
		public AttackType AttackType;
		public string Name;
		public Weapon.DamageType WeaponType;
		public Armor.ArmorType ArmorType;
		public float health;
		public int gauge;
        public int Level;
        
		public List<BattleAttributeTest> NewGameBattleAttributes;
		public List<ElementModelTest> NewGameCharacterElements;
		public List<LockedSkill> LockedSkill;
        public List<LockedMagic> LockedMagic;
		public List<CommandDataTest> NewGameCommands;
		public List<CharacterItemModelTest> NewGameCharacterItems;
        public List<BaseEquipmentStat> NewCharacterWeaponBaseStat;
        public List<BaseEquipmentStat> NewCharacterArmorBaseStat;
        public List<EquipmentUpgradeModelTest> NewGameWeaponUpgrade;
		public List<EquipmentUpgradeModelTest> NewGameArmorUpgrade;
		
		public List<BattleFormationModelTest> NewGameFormations;

		public List<BattleGrowthDataTest> BattleGrowth;

		public CharacterDatamodelTest()
        {
			ID = "";
            AttackType = AttackType.Melee;
			Name = "";
			WeaponType = Weapon.DamageType.Slash;
			ArmorType = Armor.ArmorType.Light;
			health = 0;
			gauge = 0;
            Level = 1;
			NewGameBattleAttributes = new List<BattleAttributeTest>();

			LockedSkill = new List<LockedSkill>();
            LockedMagic = new List<LockedMagic>();
			NewGameCharacterElements = new List<ElementModelTest>();
			NewGameCommands = new List<CommandDataTest>();
			NewGameCharacterItems = new List<CharacterItemModelTest>();
            NewCharacterWeaponBaseStat = new List<BaseEquipmentStat>();
            NewCharacterArmorBaseStat = new List<BaseEquipmentStat>();
            NewGameWeaponUpgrade = new List<EquipmentUpgradeModelTest>();
            NewGameArmorUpgrade = new List<EquipmentUpgradeModelTest>();
			
			NewGameFormations = new List<BattleFormationModelTest>();

			BattleGrowth = new List<BattleGrowthDataTest>();
		}
	}
}
