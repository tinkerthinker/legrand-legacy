﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Legrand.database
{

	[System.Serializable]
	public class EncounterFormationTest {
		public EncounterDataModel Monster;
		public int MonsterLvl;
		public bool ReplaceHealth;
		public int HealthMonster;
		public Vector2 Position;
		public bool ReplaceAttributes;
		public List<BattleAttributeTest> EncounterBaseAttributes;

		public EncounterFormationTest()
		{
			EncounterBaseAttributes = new List<BattleAttributeTest> ();
			Monster  = new EncounterDataModel();
			MonsterLvl = 0;
			HealthMonster = 0;
			Position = new Vector2(0f,1f);
			ReplaceAttributes = false;
		}
	}


	[System.Serializable]
	public class EncounterPartyDataModelTest
	{
		public enum PartyTypes
		{
			Normal,
			MiniBoss,
			Boss,
			MassiveNormal,
			MassiveMiniBoss,
			MassiveBoss
		}
		
		public string _ID;
		public string Name;
		public PartyTypes partyType;
        public bool CanFlee;
        public WwiseManager.BGM Music;
		public BattleCameraController.CameraView View;
		public int ExperiencePoint;

		public List<EncounterFormationTest> Formations;

		public static readonly Vector2 MaxVector = new Vector2(4,2);
		public static readonly int MaxFormation = 8;
		
		public EncounterPartyDataModelTest()
		{
			_ID = "";
			Name = "";
			partyType = PartyTypes.Normal;
            CanFlee = true;
            Music = WwiseManager.BGM.Battle_BGM;
            View = BattleCameraController.CameraView.NormalViews;
			ExperiencePoint = 0;
			Formations  = new List<EncounterFormationTest>();
		}
		
		public int AddFormation(EncounterFormationTest encounterFormation)
		{
			if (encounterFormation.Position.x > MaxVector.x || encounterFormation.Position.y > MaxVector.y)
				return 2;
			//0 succeed; 1 exceed maximum;
			if (Formations.Count == MaxFormation)
				return 1;
			else
			{
				Formations.Add(encounterFormation);
				return 0;
			}
		}
	}
}
