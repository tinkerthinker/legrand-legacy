﻿using UnityEngine;
using System.Collections;
using Legrand.core;

[System.Serializable]
public class WeaponCircle
{
    public Weapon.DamageType CurrentWeapon;
    public Weapon.DamageType WeakerTo;
    public Weapon.DamageType StrongerTo;
}
