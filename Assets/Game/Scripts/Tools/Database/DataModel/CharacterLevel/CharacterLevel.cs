﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;

[System.Serializable]
public class GaugeData
{
    public GaugeType Type;
    public int Value;

    public GaugeData()
    {
        Type = GaugeType.BaseAttack;
        Value = 0;
    }

    public GaugeData(GaugeType type, int value)
    {
        Type = type;
        Value = value;
    }

}

[System.Serializable]
public class CharacterLevelData
{
    public int Level;
    public BattleClass Class;
    public OffensiveBattleSkill SkillUltimateOffensive;
//    public DefensiveBattleSkill SkillUltimateDefensive;
    public GuardBuff PassiveGuardSkill;
    public List<GaugeData> GaugeDatas;

    public CharacterLevelData()
    {
        Level = 1;
        Class = BattleClass.Fighter;
        SkillUltimateOffensive = new OffensiveBattleSkill();
//        SkillUltimateDefensive = new DefensiveBattleSkill();
        PassiveGuardSkill = new GuardBuff();
        GaugeDatas = new List<GaugeData>();
        for(int i = 0; i<Enum.GetNames(typeof(GaugeType)).Length;i++)
            GaugeDatas.Add(new GaugeData((GaugeType)i, 0));
    }
}

[System.Serializable]
public class CharacterLevel
{
    public string ID_Character;
    public List<CharacterLevelData> CharacterLevelDatas;

    public CharacterLevel()
    {
        ID_Character = "";
        CharacterLevelDatas = new List<CharacterLevelData>();
    }
    
}
