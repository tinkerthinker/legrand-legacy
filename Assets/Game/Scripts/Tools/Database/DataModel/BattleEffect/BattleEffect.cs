﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class BattleEffect
{
    public string Name;
    public List<string> Option;

    public BattleEffect()
    {
        Name = "";
        Option = new List<string>();
    }
}
