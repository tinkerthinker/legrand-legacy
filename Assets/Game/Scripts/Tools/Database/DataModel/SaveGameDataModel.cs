﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class SaveGameDataModel
{
    public string Name;
    public SaveData SaveData;

    public SaveGameDataModel()
    {
        Name = "";
        SaveData = new SaveData();
    }
}
