﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class KeyCodeTranslate
{
    public KeyCode KeyCode;
    public string AxisCode;
    public string KeyName;

    public KeyCodeTranslate()
    {
        KeyCode = KeyCode.None;
        AxisCode = "";
        KeyName = "";
    }
}
