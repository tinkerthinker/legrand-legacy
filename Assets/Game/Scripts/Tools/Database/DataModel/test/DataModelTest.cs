﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;

public interface IStructDataModel<T>
{
    T NewObject();
}

[System.Serializable]
public struct testes : IStructDataModel<testes>
{
    public Legrand.core.Attribute att;
    public string data;

    #region IStructDataModel implementation
    public testes NewObject()
    {
        testes tes = new testes();
        tes.att = Legrand.core.Attribute.Accuracy;
        tes.data = "cfd";
        return tes;
    }
    #endregion
}
[System.Serializable]
public struct DataModelTest : IStructDataModel<DataModelTest>
{
    public string ID;
    public int intVal;
    public float floatTest;
    public Sprite sprite;
    public Attribute att;
    public List<int> listInt;
    public List<float> listFloat;
    public List<string> listString;

    #region IStructDataModel implementation
    public DataModelTest NewObject()
    {
        DataModelTest data = new DataModelTest();
        data.ID = "new";
        data.intVal = 0;
        data.floatTest = 0f;
        data.sprite = new Sprite();
        data.att = Attribute.Accuracy;
        data.listInt = new List<int>();
        data.listFloat = new List<float>();
        data.listString = new List<string>();
        return data;
    }
    #endregion
}
