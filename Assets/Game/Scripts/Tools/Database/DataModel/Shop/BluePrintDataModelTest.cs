﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Legrand.database
{
	[System.Serializable]
	public class BluePrintDataModelTest
	{
		public enum Type
		{
			Crafting,
			Alchemy
		};

		public string ID;
		public string Description;
		public int Level = 1;
		public int Price;

		public string ResultItemID;
		public Type BluePrintType;
		[Header("X For ID, Y For Amount")]
		public List<VectorStringInteger> RequiredItems;

		public BluePrintDataModelTest()
		{
			ID = "";
			Description = "";
			Level = 1;
			Price = 0;

			ResultItemID = "";
			BluePrintType = Type.Crafting;

			RequiredItems = new List<VectorStringInteger>();
		}

		public bool TryCombine()
		// Try and error if combination could be success or not
		{
			bool failed = false;
			// Iterate every requirement
			for (int index = 0; index<RequiredItems.Count && !failed; index++) 
			{
				VectorStringInteger RequiredItem = RequiredItems[index];
				bool found = false;
				// See Material Inventory First
				int typeInteger = (int)Item.Type.CraftingMaterial;
				for(int j = 0; j< PartyManager.Instance.Inventory.Items[typeInteger].Count && !found; j++)
				{
					ItemSlot itemSlot = PartyManager.Instance.Inventory.Items[typeInteger][j];
					// Found it, continue to next requirement
					if(RequiredItem.x == itemSlot.ItemMaster._ID + "" 
						&& 
						RequiredItem.y <= itemSlot.Stack
					) found = true;
				}

				if(found) continue;

				// If not found in material then maybe it's in consumeable
				typeInteger = (int)Item.Type.Consumable;
				for(int j = 0; j< PartyManager.Instance.Inventory.Items[typeInteger].Count && !found; j++)
				{
					ItemSlot itemSlot = PartyManager.Instance.Inventory.Items[typeInteger][j];
					// Found it, continue to next requirement
					if(RequiredItem.x == itemSlot.ItemMaster._ID + "" 
						&& 
						RequiredItem.y <= itemSlot.Stack
					) found = true;
				}

				if(found) continue;
				// Can't find it, it means failed to combine. Quit Iteration and return false
				failed = true;
			}

			return !failed;
		}
	}
}