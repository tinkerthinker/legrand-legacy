using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Legrand.database
{
	[System.Serializable]
	public class ShopDataModelTest
	{
		public string _ID;
		public List<string> AllItemID;
		public ShopDataModelTest()
		{
			 _ID = "";
			AllItemID = new List<string>();
		}

	}
}
