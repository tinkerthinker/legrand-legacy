using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class FencingParticipantStatusData
{
    public string ID;
    public int Hp;
    public int Ap;
    public int GoldDrop;

    public List<FencingAttribute.FencingSkillAttribute> SkillAttribute;

    public FencingParticipantStatusData()
    {
        ID = "";
        Hp = 0;
        Ap = 0;
        GoldDrop = 0;
        SkillAttribute = new List<FencingAttribute.FencingSkillAttribute>();
    }
}
