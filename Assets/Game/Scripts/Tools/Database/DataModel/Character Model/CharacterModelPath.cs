﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class CharacterModelPath
{
    public string Name;
    public List<string> JsonPaths;

    public CharacterModelPath()
    {
        Name = "";
        JsonPaths = new List<string>();
    }
}
