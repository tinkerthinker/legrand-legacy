﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum Language {ENGLISH,FRENCH,ITALIAN,GERMANY,SPANISH,JAPANESE};

[System.Serializable]
public class Languages
{
    public Language Language;
    public string Data;

    public Languages()
    {
        Language = Language.ENGLISH;
        Data = "";
    }
}

[System.Serializable]
public class LanguageData
{
    public string ID;
    public List<Languages> languages;

    public LanguageData()
    {
        ID = "";
        languages = new List<Languages>();
    }
}
