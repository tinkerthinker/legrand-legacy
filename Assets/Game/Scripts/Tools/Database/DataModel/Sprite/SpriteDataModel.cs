﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class SpriteDataModel
{
    public string ID;
    public Sprite sprite;

    public SpriteDataModel()
    {
        ID = "";
        sprite = new Sprite();
    }
}
