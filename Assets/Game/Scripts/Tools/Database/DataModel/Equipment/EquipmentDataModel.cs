﻿using UnityEngine;
using System.Collections.Generic;
using Legrand.core;

[System.Serializable]
public class EquipmentDataModel{
    public string ID;
    public string Name;
    public string Description;
    public Sprite Picture;
    public List<AttributeValue> Attributes;

    public EquipmentDataModel()
    {
        ID = "";
        Name = "";
        Description = "";
        Picture = new Sprite();
        Attributes = new List<AttributeValue>();
    }
}
