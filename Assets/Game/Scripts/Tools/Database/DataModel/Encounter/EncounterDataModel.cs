﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;
using Legrand.database;

namespace Legrand.database
{
	[System.Serializable]
	public class EncounterDataModel
	{
		public string ID;
		public string Name;
		public string Description;
		public int baseExp;
		public int baseLevel;
		public float HealthModifier;
		public bool IsFlying;
        public bool IsAntiPhysical;
        public bool IsAntiMagic;
		public CharacterAttackType Class;
		public Element Element;
		public ProjectileType Projectile;
		public EncounterSize MonsterSize;
		
		public Weapon.DamageType Weapon;
		public Armor.ArmorType Armor;
		public EnemyType EnemyType;
		public EncounterGrowth Attribute;
        public string PrefabName;
		
		public List<BattleAttributeTest> EncounterBaseAttributes;
        public List<BattleGrowthDataTest> BattleGrowth;
        public List<Magic> EncounterMagics;
        public List<OffensiveBattleSkill> OffensiveBattleSkills;
		public List<CharacterItemModelEncounterTest> EncounterItems;
		public List<EncounterLootModelTest> EncounterLoots;

        public bool IsResistDebuff;
        public List<int> DebuffException;
        public string BattlerScript;
		public EncounterDataModel()
		{
			ID = "";
			Name = "";
			Description = "";
			baseExp = 0;
			baseLevel = 0;
			HealthModifier = 0;
			IsFlying = false;
            IsAntiPhysical = false;
            IsAntiMagic = false;
            Class = CharacterAttackType.Melee;
			Element = Element.Fire;
			Projectile = ProjectileType.None;
			MonsterSize = EncounterSize.Small;
            PrefabName = "";

            Weapon = global::Weapon.DamageType.Slash;
			Armor = global::Armor.ArmorType.Light;
			EnemyType = EnemyType.Attacker;
			Attribute = new EncounterGrowth ();
			
			EncounterBaseAttributes = new List<BattleAttributeTest>();
			EncounterMagics = new List<Magic>();
			EncounterItems = new List<CharacterItemModelEncounterTest>();
			EncounterLoots = new List<EncounterLootModelTest>();
            OffensiveBattleSkills = new List<OffensiveBattleSkill> ();
            IsResistDebuff = false;
            DebuffException = new List<int>();
            BattlerScript = "";
		}
	}
}