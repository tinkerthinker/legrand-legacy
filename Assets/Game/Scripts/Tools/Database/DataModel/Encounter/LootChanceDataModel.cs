﻿using UnityEngine;
using System.Collections;
namespace Legrand.database
{
    [System.Serializable]
    public class LootChanceDataModel
    {
        public float _CommonChance;
        public float _UnCommonChance;
        public float _RareChance;
        public float _VeryRareChance;
        public float _SuperRareChance;
        public float _BossOnlyChance;

        public LootChanceDataModel()
        {
            _CommonChance = 0f;
            _UnCommonChance = 0f;
            _RareChance = 0f;
            _VeryRareChance = 0f;
            _SuperRareChance = 0f;
            _BossOnlyChance = 0f;
        }
    }
}