﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.IO;

namespace Legrand.database
{
	public class GetObject
	{
		public enum GetStatus {BUTTONONLY,PORTAL,AREADIR,SUBAREA,ChildOf};
		private static string[] PrefabName(string dirName)
		{
			List<string> name = new List<string>();
			DirectoryInfo dir = new DirectoryInfo(dirName);
			FileInfo[] info = dir.GetFiles("*.prefab");
			foreach (FileInfo f in info) 
			{
				name.Add(f.Name.Split('.')[0]);
			}
			return name.ToArray();
		}
		public static void GetChildOfGameObject(string dirName,string name,List<string> option)
		{
			GetChildName(dirName,name,option);
		}
		private static void GetChildName(string dirName,string Name,List<string> option)
		{
			string[] prefabs = PrefabName(dirName);
			foreach(var name in prefabs)
			{
				if(name.Equals(Name))
				{
					#if UNITY_EDITOR
					GameObject Go = AssetDatabase.LoadAssetAtPath(dirName + "/" + name + ".prefab", typeof(GameObject)) as GameObject;
					GetChilds(Go,option,dirName,GetStatus.ChildOf);
					#endif
				}
			}
			string [] subdirectoryEntries = Directory.GetDirectories(dirName);
			if(subdirectoryEntries.Length > 0)
			{
				foreach(var s in subdirectoryEntries)
				{
					GetChildOfGameObject(s,Name,option);
				}
			}
		}

		public static void Get(string dirName,List<string> GameObjectName,bool GetChild,GetStatus status)
		{
			if(status != GetStatus.AREADIR)
				GetPrefabName(dirName,GameObjectName,GetChild,status);
			else
				GetFolderName(dirName,GameObjectName);
		}
		private static bool isHasSubFolder(string path)
		{
			string [] subdirectoryEntries = Directory.GetDirectories(path);
			if(subdirectoryEntries.Length > 0)
				return true;
			return false;
		}
		private static void GetFolderName(string dirName,List<string> GameObjectName)
		{
			string [] subdirectoryEntries = Directory.GetDirectories(dirName);
			if(!isHasSubFolder(dirName))
				GameObjectName.Add(dirName);
			foreach(var s in subdirectoryEntries)
			{
				GetFolderName(s,GameObjectName);
			}
		}
		private static void GetChilds(GameObject Go,List<string> GameObjectList,string dirName,GetStatus status)
		{
			if(status == GetStatus.BUTTONONLY)
			{				
				Button bu = Go.GetComponent<Button>();
				if(bu != null)
				{
					GameObjectList.Add(Go.name);
				}
			}
			else if(status == GetStatus.PORTAL)
			{
				MapPointSelection bu = Go.GetComponent<MapPointSelection>();
				PortalTrigger pt = Go.GetComponent<PortalTrigger>();
				if(bu != null || pt != null)
				{
					GameObjectList.Add(Go.transform.parent.transform.parent.name + "+" + Go.name);
				}
			}
			else if(status == GetStatus.ChildOf)
			{
				GameObjectList.Add(Go.name);
			}
			for(int c=0;c<Go.transform.childCount;c++)
			{
				GetChilds(Go.transform.GetChild(c).gameObject,GameObjectList,dirName,status);
			}
		}
		private static void GetPrefabName(string dirName,List<string> GameObjectList,bool GetChild,GetStatus status)
		{
			string[] prefabs = PrefabName(dirName);
			foreach(var name in prefabs)
			{
                #if UNITY_EDITOR
				GameObject Go = AssetDatabase.LoadAssetAtPath(dirName + "/" + name + ".prefab", typeof(GameObject)) as GameObject;
				if(GetChild)
				{
					GetChilds(Go,GameObjectList,dirName,status);
				}
				else
				{
					GameObjectList.Add(new DirectoryInfo(@dirName).Name + "?" + Go.name);
				}
                #endif
			}
			string [] subdirectoryEntries = Directory.GetDirectories(dirName);
			if(subdirectoryEntries.Length > 0)
			{
				foreach(var s in subdirectoryEntries)
				{
					Get(s,GameObjectList,GetChild,status);
				}
			}
		}
	}
	public class InstantiateArea : MonoBehaviour
	{
		public Dropdown areaDropDown;
		public Dropdown subAreaDropDown;

		List<string> areaList = new List<string>();
		List<string> subList = new List<string>();
		List<string> subAreaList = new List<string>();

		Object subAreaPrefab;
		public GameObject areaHold;

		public GameObject CharModel;
		GameObject Character;

		public LayerMask mask;

		void Start()
		{
			GetObject.Get("Assets/Resources/Worlds/Area",areaList,false,GetObject.GetStatus.AREADIR);
			GetObject.Get("Assets/Resources/Worlds/Area",subList,false,GetObject.GetStatus.SUBAREA);
			GetFolderAreaName ();

			areaDropDown.AddOptions (areaList);

			OnAreaChange ();

			Character = new GameObject ();
			Character = GameObject.Instantiate (CharModel);
		}
		private void GetFolderAreaName()
		{
			for(int i=0;i<areaList.Count;i++)
			{
				areaList[i] = new DirectoryInfo(@areaList[i]).Name;
			}
		}

		public void OnAreaChange()
		{
			subAreaList.Clear ();
			subAreaDropDown.options.Clear ();
			string area = areaList [areaDropDown.value];
			foreach (var sub in subList)
			{
				if (sub.Split ('?') [0].Equals (area))
					subAreaList.Add (sub.Split ('?') [1]);
			}
			subAreaDropDown.AddOptions (subAreaList);
		}

		GameObject GetGameObjectFromFullPath(string Path)
		{
			Path = Path.Replace("Resources","?").Split('?')[1].Substring(1).Replace(@"\",@"/").Split('.')[0];

			GameObject Go = (GameObject)Resources.Load(Path) as GameObject;
			return Go;
		}

		void GetGameObjectInSubDir(string objectName,string path,ref Object Go)
		{
			bool found = false;
			string [] subdirectoryEntries = Directory.GetDirectories(path);
			foreach(var sub in subdirectoryEntries)
			{
				DirectoryInfo dir = new DirectoryInfo(sub);
				FileInfo[] info = dir.GetFiles("*.prefab");
				foreach (FileInfo f in info)
				{
					string nama = f.Name.Split('.')[0];
					if(nama.Equals(objectName))
					{
						Go = GetGameObjectFromFullPath(f.FullName);
						found = true;
						break;
					}
				}
				if(!found)
					GetGameObjectInSubDir(objectName,sub,ref Go);
			}
		}

		public void instantiate()
		{
			GameObject old = GameObject.Find ("hasilClone");
			GameObject.Destroy (old);
			string path = "Assets/Resources/Worlds/Area";
			GetGameObjectInSubDir (subAreaList [subAreaDropDown.value], path,ref subAreaPrefab);
			GameObject newSub = GameObject.Instantiate ((GameObject)subAreaPrefab as GameObject);
			newSub.name = "hasilClone";
			newSub.transform.SetParent (areaHold.transform);
		}
		void rotateCharacter()
		{
			if (Input.GetKey (KeyCode.A))
			{
				Character.transform.Rotate (0, -90 * Time.deltaTime, 0);
			}
			if (Input.GetKey (KeyCode.D))
			{
				Character.transform.Rotate (0, 90 * Time.deltaTime, 0);
			}
		}
		void Update()
		{
			if (Input.GetMouseButtonDown(0))
			{
				Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
				RaycastHit hit;

				if (Physics.Raycast(ray, out hit, 1000f, mask))
				{
					Character.transform.position = hit.point;
				}
			}
			rotateCharacter ();
		}
	}
}