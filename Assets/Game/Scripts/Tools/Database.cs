﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;

namespace Legrand.database
{
	public class Database : MonoBehaviour
	{
		public BluePrintDatabase BlueprintData;
		public BuffDatabase BuffData;
		public ConsumableItemAttributeDatabase ConsumableItemAttributeData;
		public ConsumableHealingsDatabase ConsumableHealingItemData;
		public EncounterDatabase EncounterData;
		public EncounterGrowthDatabase EncounterGrowthData;
		public EncounterPartyDatabase EncounterPartyData;
		public KeyDatabase KeyData;
		public MagicItemDatabase MagicItemData;
		public MaterialItemDatabase MaterialItemData;
		public ConsumableMiscDatabase ConsumableMiscData;
		public PartyDatabase PartyData;
		public QuestDatabase QuestData;
		public StoryDatabase storyData;
		public ShopModelDatabase ShopModelData;
		public TutorialStepsDatabase TutorialData;
		public BattlerWeaponCalculationDatabase WeaponCalculationData;
		public BattlerArmorCalculationDatabase ArmorCalculationData;
		public InventoryDatabase inventoryData;
        public FormulaDatabase formulaData;
        public ConstantPercentageValueDatabase constantValueFormula;
        public ElementCircleDatabase ElementCircleData;
        public WeaponCircleDatabase WeaponCircleData;
		public BattleEffectDatabase BattleEffectData;
        public SpriteDatabase SpriteData;
        public LootChanceDatabase lootChanceData;
        public OffenseSkillDatabase OffensiveSkillData;
        public DefensiveSkillDatabase DefensiveSkillData;
        public CombinationSkillDatabase CombinationSkillData;
        public NormalSkillDatabase NormalSkillData;
        public GuardBuffDatabase GuardBuffData;
        public CharacterLevelDatabase CharacterLevelData;
        public KeyCodeDatabase KeyCodeData;
        public MagicDatabase MagicData;
        public AttributePointDatabase AttributePointData;
        public EquipmentsDatabase EquipmentDatabase;
        public LanguageDatabase LanguageDatabase;
        public SpriteDatabase AreaSpriteDatabase;
        public MerchantLevelDatabase MerchantLevelDatabase;
    }
}