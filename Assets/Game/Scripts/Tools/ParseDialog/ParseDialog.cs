﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using System.IO;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

#if UNITY_EDITOR
namespace Legrand.database
{
    public class ParseDialog : EditorWindow
    {
        static LanguageDatabase getLanguageDatabase()
        {
            LanguageDatabase language = ScriptableObject.CreateInstance<LanguageDatabase>();
            language = LanguageDatabase.GetDatabase <LanguageDatabase>(@"Database",@"LanguageDatabase.asset");
            return language;
        }

        [MenuItem("Legrand Legacy/Dialog/Load Dialog")]
        static void Load( )
        {
            string path = EditorUtility.OpenFilePanel( "Load Dialog", "", "txt" );
            if( path.Length != 0 )
            {
                Process(true, path);
            }
        }
        [MenuItem("Legrand Legacy/Dialog/Add Dialog")]
        static void Add( )
        {
            string path = EditorUtility.OpenFilePanel( "Add Dialog", "", "txt" );
            if( path.Length != 0 )
            {
                Process(false, path);
            }
        }
        static Language getLanguage(string id)
        {
            if (id.Equals("en"))
                return Language.ENGLISH;
            else if (id.Equals("ja"))
                return Language.JAPANESE;
            else if (id.Equals("fr"))
                return Language.FRENCH;
            else if (id.Equals("gr"))
                return Language.GERMANY;
            else if (id.Equals("sp"))
                return Language.SPANISH;
            else if (id.Equals("it"))
                return Language.ITALIAN;
            return Language.ENGLISH;
        }
        static bool CheckAndReplace(LanguageDatabase database,string ID,LanguageData data)
        {
            for(int i=0;i<database.Count;i++)
            {
                if(LegrandUtility.CompareString(database.Get(i).ID,ID))
                {
                    database.SetItem(i, data);
                    return true;
                }
            }
            return false;
        }
        static void Process(bool load,string path)
        {
            List<LanguageData> language = new List<LanguageData>();
            string[] Data = File.ReadAllText(path).Split('⌡');
            for (int ii = 0; ii < Data.Length - 1; ii++)
            {
                if (Data[ii].Length > 1)
                {
                    LanguageData data = new LanguageData();
                    string[] splitValue = Data[ii].Split('|');

                    for (int i = 0; i < splitValue.Length; i++)
                    {
                        if (i == 0)
                            data.ID = splitValue[0].Replace("\r","").Replace("\n","");
                        else
                        {
                            Languages datas = new Languages();
                            datas.Data = splitValue[i].Remove(0, 3);

                            datas.Language = getLanguage(splitValue[i].Split('-')[0]);
                            data.languages.Add(datas);
                        }
                    }
                    language.Add(data);
                }
            }

            LanguageDatabase database = getLanguageDatabase();
            if (load)
                database.Clear();
            foreach (LanguageData data in language)
            {
                if(!CheckAndReplace(database,data.ID,data))
                    database.AddValue(data);
                database.refreshElement(false);
            }
            database.refresh();
            EditorApplication.SaveAssets ();
        }
    }
}
#endif