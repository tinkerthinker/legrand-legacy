﻿using UnityEngine;
using System.Collections;
using UnityEditor;

namespace Legrand.database
{
	public enum CurveStatus {MAKETRANSITION,QUEST,AREATOSUB,SUBTOCONNECTOR,AREATOAREA,CONNECTOR1WAY,CONNECTOR2WAY};
	public class DrawerClass
	{
		private enum Direction {Up,Down,Left,Right};
		public static void DrawNodeCurve(Rect start, Rect end,bool makeTransition,CurveStatus curveMode,bool arrow)
		{
			Color color = new Color();
			if(curveMode == CurveStatus.QUEST)
			{
				color = Color.green;
			}
			else if(curveMode == CurveStatus.AREATOSUB)
			{
				color = Color.magenta;
			}
			else if(curveMode == CurveStatus.CONNECTOR1WAY)
			{
				color = Color.yellow;
			}
			else if(curveMode == CurveStatus.CONNECTOR2WAY)
			{
				color = Color.green;
			}
			else if(curveMode == CurveStatus.MAKETRANSITION)
			{
				color = Color.magenta;
			}
			else if(curveMode == CurveStatus.SUBTOCONNECTOR)
			{
				color = Color.cyan;
			}

			Direction dir = GotoDirection(start,end);
			if(dir == Direction.Up)
			{
				if(makeTransition)
					DrawNodeCurve(start, end, new Vector2(0.5f, 0.5f), new Vector2(0f, 0f),color);
				else
					DrawNodeCurve(start, end, new Vector2(0.5f, 0.0f), new Vector2(0.5f, 1.0f),color);
				if(arrow)
					DrawArrowhead(start, end, new Vector2(0.5f, 0.0f), new Vector2(0.5f, 1.0f), 50f, 30f, 10f,color);
			}
			else if(dir == Direction.Down)
			{
				if(makeTransition)
					DrawNodeCurve(start, end, new Vector2(0.5f, 0.5f), new Vector2(0f, 0f),color);
				else
					DrawNodeCurve(start, end, new Vector2(0.5f, 1.0f), new Vector2(0.5f, 0.0f),color);
				if(arrow)
					DrawArrowhead(start, end, new Vector2(0.5f, 1.0f), new Vector2(0.5f, 0.0f), 50f, 30f, 10f,color);
			}
			else if(dir == Direction.Left)
			{
				if(makeTransition)
					DrawNodeCurve(start, end, new Vector2(0.5f, 0.5f), new Vector2(0f, 0f),color);
				else
					DrawNodeCurve(start, end, new Vector2(0.0f, 0.5f), new Vector2(1.0f, 0.5f),color);
				if(arrow)
					DrawArrowhead(start, end, new Vector2(0.0f, 0.5f), new Vector2(1.0f, 0.5f), 50f, 30f, 10f,color);
			}
			else if(dir == Direction.Right)
			{
				if(makeTransition)
					DrawNodeCurve(start, end, new Vector2(0.5f, 0.5f), new Vector2(0f, 0f),color);
				else
					DrawNodeCurve(start, end, new Vector2(1.0f, 0.5f), new Vector2(0.0f, 0.5f),color);
				if(arrow)	
					DrawArrowhead(start, end, new Vector2(1.0f, 0.5f), new Vector2(0.0f, 0.5f), 50f, 30f, 10f,color);
			}
		}
		static bool isBigger(float start,float end)
		{
			if(start > end)
				return true;
			return false;
		}
		static bool isHorizontal(Rect start,Rect end)
		{
			if(Mathf.Abs(end.position.x - start.position.x) > Mathf.Abs(end.position.y - start.position.y))
				return true;
			return false;
		}
		static Direction GotoDirection(Rect start, Rect end)
		{
			if(isBigger(end.position.y,start.position.y) && !isHorizontal(start,end))//down
			{
				return Direction.Down;
			}
			else if(isBigger(start.position.y,end.position.y) && !isHorizontal(start,end))//up
			{
				return Direction.Up;
			}
			else if(isBigger(end.position.x,start.position.x) && isHorizontal(start,end))//right
			{
				return Direction.Right;
			}
			else if(isBigger(start.position.x,end.position.x) && isHorizontal(start,end))//left
			{
				return Direction.Left;
			}
			return Direction.Left;
		}
		static void DrawArrowhead(Rect start, Rect end, Vector2 vStartPercentage, Vector2 vEndPercentage, float fHandleDistance, float fLength, float fWidth, Color color)
		{
			float fHandleDistanceDouble = fHandleDistance * 2;
			
			Vector3 startPos = new Vector3(start.x + start.width * vStartPercentage.x, start.y + start.height * vStartPercentage.y, 0);
			Vector3 startTan = startPos + Vector3.right * (-fHandleDistance + fHandleDistanceDouble * vStartPercentage.x) + Vector3.up * (-fHandleDistance + fHandleDistanceDouble * vStartPercentage.y);
			
			Vector3 endPos = new Vector3(end.x + end.width * vEndPercentage.x, end.y + end.height * vEndPercentage.y, 0);
			Vector3 endTan = endPos + Vector3.right * (-fHandleDistance + fHandleDistanceDouble * vEndPercentage.x) + Vector3.up * (-fHandleDistance + fHandleDistanceDouble * vEndPercentage.y);
			
			float dy = endTan.y - endPos.y;
			float dx = endTan.x - endPos.x;
			
			Vector3 vDelta = endTan - endPos;
			Vector3 vNormal = new Vector3 ( -dy, dx, 0f ).normalized;
			
			Vector3 vArrowHeadEnd1 = endPos + vDelta.normalized * fLength + vNormal.normalized * fWidth;
			Vector3 vArrowHeadEnd2 = endPos + vDelta.normalized * fLength + vNormal.normalized * -fWidth;
			
			Vector3 vHalfwayPoint = endPos  + vDelta.normalized * fLength * 0.5f;
			
			Color shadowCol = new Color(0, 0, 0, 0.06f);
			
			for (int i = 0; i < 3; i++) // Draw a shadow
				Handles.DrawBezier(endPos, vArrowHeadEnd1, endPos, vHalfwayPoint, shadowCol, null, (i + 1) * 5);
			Handles.DrawBezier(endPos, vArrowHeadEnd1, endPos, vHalfwayPoint, color, null, 2);
			
			for (int i = 0; i < 3; i++) // Draw a shadow
				Handles.DrawBezier(endPos, vArrowHeadEnd2, endPos, vHalfwayPoint, shadowCol, null, (i + 1) * 5);
			Handles.DrawBezier(endPos, vArrowHeadEnd2, endPos, vHalfwayPoint, color, null, 2);
		}
		static void DrawNodeCurve(Rect start, Rect end, Vector2 vStartPercentage, Vector2 vEndPercentage ,Color color)
		{
			Vector3 startPos = new Vector3(start.x + start.width * vStartPercentage.x, start.y + start.height * vStartPercentage.y, 0);
			Vector3 endPos = new Vector3(end.x + end.width * vEndPercentage.x, end.y + end.height * vEndPercentage.y, 0);
			Vector3 startTan = startPos + Vector3.right * (-50 + 100 * vStartPercentage.x) + Vector3.up * (-50 + 100 * vStartPercentage.y);
			Vector3 endTan = endPos + Vector3.right * (-50 + 100 * vEndPercentage.x) + Vector3.up * (-50 + 100 * vEndPercentage.y);
			Handles.DrawBezier(startPos, endPos, startTan, endTan, color, null, 3);
		}
		private static Texture2D _staticRectTexture;
		private static GUIStyle _staticRectStyle;
		
		public static void GUIDrawRect( Rect position, Color color )
		{
			if( _staticRectTexture == null )
			{
				_staticRectTexture = new Texture2D( 1, 1 );
			}
			
			if( _staticRectStyle == null )
			{
				_staticRectStyle = new GUIStyle();
			}
			
			_staticRectTexture.SetPixel( 0, 0, color );
			_staticRectTexture.Apply();
			
			_staticRectStyle.normal.background = _staticRectTexture;
			
			GUI.Box( position, GUIContent.none, _staticRectStyle );
		}
	}
}
