using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.database;
using System.IO;

namespace Legrand.database
{
	public static class CanvasZoomBounds
	{
		public static float zoomMin = 0.6f;
		public static float zoomMax = 2.0f;
	}
	public class CanvasData : EditorWindow
	{
		// Settings
		public enum EditorMode {Quest,Map};
		public EditorMode Mode;

		protected bool PanWindow = false;
		protected Vector2 PanOffset = new Vector2 ();
		protected static Vector2 ZoomPanAdjust = new Vector2();
		protected float Zoom = 1;
		protected Vector2 ZoomPos { get { return CanvasRect.size/2; } }
		
		public static int RightPanelWidth = 300;
		public static int propertiesPanelHeight = 0;
		public static int statePanelHeight = 0;
		public Rect CanvasRect
		{
			get { return new Rect (0, propertiesPanelHeight, position.width - RightPanelWidth, position.height - propertiesPanelHeight - statePanelHeight); }
		}
	}
	public class NodeEditorSystem : CanvasData
	{
		protected List<EditorNode> Node = new List<EditorNode>();

		protected static Vector2 mousePos;
		protected static Texture2D Background;

		struct MakeTransitionMode
		{
			public bool isMake;
			public enum TransMode {QUEST,ADDSUBTOAREA,ADDCONNECTION,SUBAREA1WAY,SUBAREA2WAY};
			public TransMode TransitionMode;
			public int From;
		};
		private MakeTransitionMode makeTransitionMode;
		Event e;
		int SelectedNode = -1;

		int SelectedNodePanelButton = -1;
		int SelectedSubArea = -1;
		Vector2 panelScroll = new Vector2();
        
        string SearchKey = "";

        bool ChapterFilter = false;
        int Chapter = 0;

		//bool isCreatePrefabMode = false;
		//struct PrefabData
		//{
		//	public string Name;
		//};
		//private PrefabData NewPrefabData;

		//database map
		private MapConnectionNodeDatabase databaseMap = ScriptableObject.CreateInstance<MapConnectionNodeDatabase>();
        private MapAreaDatabase RealAreaData = MapAreaDatabase.GetDatabase<MapAreaDatabase>(@"Database",@"MapAreaData.asset");
        private MapSubAreaData RealSubAreaData = MapSubAreaData.GetDatabase<MapSubAreaData>(@"Database",@"MapSubAreaData.asset");
        private MapConnectionData RealConnectionData = MapConnectionData.GetDatabase<MapConnectionData>(@"Database",@"MapConnectionSubData.asset");

        protected bool isEdited = false;
        protected void Exit()
        {
            PlayerPrefs.SetInt("MapChapterFilterOption", ChapterFilter ? 1 : 0);
            PlayerPrefs.SetInt("MapChapterFilter", Chapter);
        }
		protected void init()
		{
            if (Mode == EditorMode.Map)
            {
                ListDataMap.init();
                GetGameObject.Get("Assets/Resources/Worlds/Area",ListDataMap.AreaList,false,GetStatus.AREADIR);
                GetGameObject.Get("Assets/Resources/Worlds/Area",ListDataMap.PrefabSubAreaName,false,GetStatus.SUBAREA);
                GetGameObject.Get("Assets/Resources/Worlds/Area",ListDataMap.UnlockedPortalList,true,GetStatus.CITYPOINT);
                GetGameObject.Get("Assets/Resources/Worlds/Area",ListDataMap.PortalListOption,true,GetStatus.PORTAL);
                GetGameObject.Get("Assets/Resources/Worlds/Area",ListDataMap.WorldMapPointList, true, GetStatus.WORLDMAPPOINT);

                ChapterFilter = PlayerPrefs.GetInt("MapChapterFilterOption", 1) == 1 ? true : false;
                Chapter = PlayerPrefs.GetInt("MapChapterFilter", 0);
            }
			makeTransitionMode.isMake = false;
			Load();
			Background = UnityEditor.AssetDatabase.LoadAssetAtPath ("Assets/Game/Textures/Tools/background.png", typeof(Texture2D)) as Texture2D;
		}
		int searchIndex(EditorNode node)
		{
			for(int a=0;a<Node.Count;a++)
			{
				if(Node[a].Equals(node))
					return a;
			}
			return -1;
		}
		protected void Save()
		{
            isEdited = false;

			if(Mode == EditorMode.Map)
			{
				RealAreaData.Clear();
				RealSubAreaData.Clear();
				databaseMap.Clear();
				RealConnectionData.Clear();
				foreach(var node in Node)
				{
					if(node.nodeType == NodeType.AREA)
					{
						EditorNodeAreaMap nodeM = (EditorNodeAreaMap)node;
						EditorNodeData temp = new EditorNodeData();
						temp.NodeRect = nodeM.NodeRect;
						temp.NodeID = nodeM.NodeID;
						foreach(var c in nodeM.Child)
						{
							temp.Child.Add(searchIndex(c));
						}
						temp.NodeType = NodeType.AREA;
						databaseMap.AddValue(temp);
						AreaTest area = Copy.DeepCopy<AreaTest>(nodeM.AreaData);
						temp.NodeID = nodeM.NodeID;

						area.subArea.Clear();
						foreach(var c in node.Child)
						{
							SubAreaTest sub = new SubAreaTest();
							sub.prefabName = c.NodeID;//simpan ID
							area.subArea.Add(sub);
						}
						area.areaName = temp.NodeID;
						RealAreaData.AddValue(area);
					}
					else if(node.nodeType == NodeType.SUBAREA)
					{
						EditorNodeSubAreaMap nodeM = (EditorNodeSubAreaMap)node;
						EditorNodeData temp = new EditorNodeData();
						temp.NodeRect = nodeM.NodeRect;
						temp.NodeID = nodeM.NodeID;
						foreach(var p in nodeM.Parent)
						{
							temp.Parent.Add(searchIndex(p));
						}
						foreach(var c in nodeM.Child)
						{
							temp.Child.Add(searchIndex(c));
						}
						temp.NodeType = NodeType.SUBAREA;
						databaseMap.AddValue(temp);
						SubAreaTest subArea = Copy.DeepCopy<SubAreaTest>(nodeM.SubAreaData);
						temp.NodeID = nodeM.NodeID;
						subArea.connections.Clear();
						foreach(var c in nodeM.Child)
						{
							if(c.Child.Count > 0)
							{
								ConnectionTest ct = new ConnectionTest();
								int index = searchIndex(c);
								ct.ConnectionName = index.ToString();
								subArea.connections.Add(ct);
							}
						}
						subArea.prefabName = temp.NodeID;
						if (subArea.hasVersions)
						{
							for(int r=0;r<subArea.roomVersion.Count;r++)
							{
								subArea.roomVersion [r].modification.material = GetMaterialPath (subArea.roomVersion [r].modification.material);
								for (int g = 0; g < subArea.roomVersion [r].modification.gameObjects.Count; g++)
								{
									subArea.roomVersion [r].modification.gameObjects [g] = subArea.roomVersion [r].modification.gameObjects [g].Split ('?') [0];
									subArea.roomVersion [r].modification.gameObjects[g] = subArea.roomVersion [r].modification.gameObjects[g] + "?" + GetPrefabPath(subArea.prefabName);
								}
							}
						}
						RealSubAreaData.AddValue(subArea);
					}
					else if(node.nodeType == NodeType.CONNECTION)
					{
						EditorNodeConnectionMap nodeC = (EditorNodeConnectionMap)node;
						EditorNodeData temp = new EditorNodeData();
						temp.NodeRect = nodeC.NodeRect;
						temp.NodeID = nodeC.NodeID;
						foreach(var p in nodeC.Parent)
						{
							temp.Parent.Add(searchIndex(p));
						}
						foreach(var c in nodeC.Child)
						{
							temp.Child.Add(searchIndex(c));
						}
						temp.NodeType = NodeType.CONNECTION;
						databaseMap.AddValue(temp);
						ConnectionTest connection = Copy.DeepCopy<ConnectionTest>(nodeC.ConnectionData);
						temp.NodeID = nodeC.NodeID;

						connection.portalFrom = nodeC.NodeID;
						foreach(var p in nodeC.Parent)
						{
							if(p.nodeType == NodeType.CONNECTION)
							{

							}
						}
						foreach(var c in nodeC.Child)
						{
							if(c.nodeType == NodeType.CONNECTION)
							{
								connection.portalTo = c.NodeID;
								foreach(var sp in c.Parent)
								{
									if(sp.nodeType == NodeType.SUBAREA)
									{
										connection.destination = sp.NodeID;
									}
								}
							}
						}
						connection.ConnectionName = searchIndex(node).ToString();
						RealConnectionData.AddValue(connection);
					}
				}
				databaseMap.refresh();
				RealAreaData.refresh();
				RealSubAreaData.refresh();
				RealConnectionData.refresh();
			}
            EditorApplication.SaveAssets ();
		}
		string GetPrefabPath(string name)
		{
			string path = "Assets/Resources/Worlds/Area";
			Object Go = new Object();
			string pathResult = "";
			GetGameObjectInSubDir(name,path,ref Go,ref pathResult);
			if(!pathResult.Equals(""))
				pathResult = pathResult.Replace("Resources","?").Split('?')[1].Substring(1).Replace(@"\",@"/").Split('.')[0];
			return pathResult;
		}
		void GetGameObjectInSubDir(string objectName,string path,ref Object Go,ref string pathResult)
		{
			bool found = false;
			string [] subdirectoryEntries = Directory.GetDirectories(path);
			foreach(var sub in subdirectoryEntries)
			{
				DirectoryInfo dir = new DirectoryInfo(sub);
				FileInfo[] info = dir.GetFiles("*.prefab");
				foreach (FileInfo f in info)
				{
					string nama = f.Name.Split('.')[0];
					if(nama.Equals(objectName))
					{
						Go = GetGameObjectFromFullPath(f.FullName);
						pathResult = f.FullName;
						found = true;
						break;
					}
				}
				if (!found)
					GetGameObjectInSubDir (objectName, sub, ref Go,ref pathResult);
			}
		}
		GameObject GetGameObjectFromFullPath(string Path)
		{
			Path = Path.Replace("Resources","?").Split('?')[1].Substring(1).Replace(@"\",@"/").Split('.')[0];

			GameObject Go = (GameObject)Resources.Load(Path) as GameObject;
			return Go;
		}
		string GetMaterialPath(string Name)
		{
			string pathToFile = "";
			string path = "";
			path = "Assets/Resources/Worlds/AreaMaterials";

			GetSubPathMaterial(path,Name,ref pathToFile);
			if(!pathToFile.Equals(""))
				pathToFile = pathToFile.Replace("Resources","?").Split('?')[1].Substring(1).Replace(@"\",@"/").Split('.')[0];
			if (pathToFile.Equals (""))
				return Name;
			else
				return pathToFile;
		}
		void GetSubPathMaterial(string path,string Name,ref string pathToFile)
		{
			string [] subdirectoryEntries = Directory.GetDirectories(path);
			foreach(var sub in subdirectoryEntries)
			{
				DirectoryInfo dir = new DirectoryInfo(sub);
				FileInfo[] info = dir.GetFiles("*.mat");
				foreach (FileInfo f in info)
				{
					string nama = f.Name.Split('.')[0];
					if(nama.Equals(Name))
					{
						pathToFile = f.FullName;
						break;
					}
				}
				if(pathToFile.Equals(""))
				{
					GetSubPathMaterial(sub,Name,ref pathToFile);
				}
			}
		}
		private void Load()
		{
			Node.Clear();
			if(Mode == EditorMode.Map)
			{
				MapAreaDatabase RealAreaData = MapAreaDatabase.GetDatabase<MapAreaDatabase>(@"Database",@"MapAreaData.asset");
				MapSubAreaData RealSubAreaData = MapSubAreaData.GetDatabase<MapSubAreaData>(@"Database",@"MapSubAreaData.asset");
				MapConnectionData RealConnectionData = MapConnectionData.GetDatabase<MapConnectionData>(@"Database",@"MapConnectionSubData.asset");
				databaseMap = MapConnectionNodeDatabase.GetDatabase<MapConnectionNodeDatabase>(@"Database",@"MapConnectionData.asset");
				int AreaCount = 0;
				int SubAreaCount = 0;
				int ConnectionCount = 0;
				for(int a=0;a<databaseMap.Count;a++)
				{
					if(databaseMap.Get(a).NodeType == NodeType.AREA)
					{
						EditorNodeAreaMap temp = new EditorNodeAreaMap();
						temp.NodeID = databaseMap.Get(a).NodeID;
						temp.NodeRect = databaseMap.Get(a).NodeRect;
						temp.AreaData = Copy.DeepCopy<AreaTest>(RealAreaData.Get(AreaCount));
						temp.nodeType = NodeType.AREA;
						Node.Add(temp);
						AreaCount ++;
					}
					else if(databaseMap.Get(a).NodeType == NodeType.SUBAREA)
					{
						EditorNodeSubAreaMap temp = new EditorNodeSubAreaMap();
						temp.NodeID = databaseMap.Get(a).NodeID;
						temp.NodeRect = databaseMap.Get(a).NodeRect;
						temp.SubAreaData = Copy.DeepCopy<SubAreaTest>(RealSubAreaData.Get(SubAreaCount));
						temp.nodeType = NodeType.SUBAREA;
						Node.Add(temp);
						SubAreaCount ++;
					}
					else if(databaseMap.Get(a).NodeType == NodeType.CONNECTION)
					{
						EditorNodeConnectionMap temp = new EditorNodeConnectionMap();
						temp.NodeID = databaseMap.Get(a).NodeID;
						temp.NodeRect = databaseMap.Get(a).NodeRect;
						temp.ConnectionData = Copy.DeepCopy<ConnectionTest>(RealConnectionData.Get(ConnectionCount));
						temp.nodeType = NodeType.CONNECTION;
						Node.Add(temp);
						ConnectionCount ++;
					}
				}
				for(int m=0;m<databaseMap.Count;m++)
				{
					foreach(var p in databaseMap.Get(m).Parent)
					{
						Node[m].Parent.Add(Node[p]);
					}
					foreach(var c in databaseMap.Get(m).Child)
					{
						Node[m].Child.Add(Node[c]);
					}
				}
                foreach(EditorNode node in Node)
                {
                    if (node.Parent.Count == 0)
                    {
                        node.SetThisChapter();
                        node.SetChildChapter();
                    }
                }
			}
		}

        bool StringSearchCompare(string data,string key)
        {
            if (key == "")
                return true;
            if (data.ToLower().Replace(" ", "").Replace("_", "").Contains(key.ToLower().Replace(" ", "").Replace("_", "")))
                return true;
            return false;
        }

		protected void Draw(EditorWindow editorWindow)
		{
			GUILayout.BeginArea (leftPanelRect);
			DrawPanel();
			GUILayout.EndArea ();

			DrawCanvas(editorWindow);
		}
		void DrawPanel()
		{
			GUILayout.BeginVertical();
			if(GUILayout.Button ("Save"))
			{
                if (EditorUtility.DisplayDialog("Save", "Save Changes ?", "Yes", "No"))
                {
                    Save();
                }
			}
			if(GUILayout.Button ("Reset"))
			{
                if (EditorUtility.DisplayDialog("Reset", "Reset all Changes from latest save ?", "Yes", "No"))
                {
                    Load();
                }
			}
			if(Mode == EditorMode.Map)
			{
                GUILayout.Space(10f);
                if (DatabaseState.SearchActive)
                    GUI.color = DatabaseState.SelectedColor;
                if (GUILayout.Button("Dropdown Search " + (DatabaseState.SearchActive ? "On" : "Off"), GUILayout.Width(DatabaseState.BOTTOM_BUTTON_WIDTH * 3)))
                {
                    DatabaseState.SearchActive = !DatabaseState.SearchActive;
                }
                GUI.color = Color.white;
                
                GUILayout.Space(10f);

                GUILayout.BeginHorizontal();
                if (ChapterFilter)
                    GUI.color = DatabaseState.SelectedColor;
                if (GUILayout.Button("Chapter Filter " + (ChapterFilter ? "On" : "Off"), GUILayout.Width(DatabaseState.BOTTOM_BUTTON_WIDTH * 3)))
                {
                    ChapterFilter = !ChapterFilter;
                }
                GUI.color = Color.white;
                if (ChapterFilter)
                {
                    GUILayout.Label("Chapter ");
                    Chapter = EditorGUILayout.IntField(Chapter);
                }
                GUILayout.EndHorizontal();

                GUILayout.Space(10f);

                GUILayout.BeginHorizontal();
                GUILayout.Label("Area Search ");
                SearchKey = EditorGUILayout.TextField(SearchKey);
                GUILayout.EndHorizontal();
                //if(isCreatePrefabMode)
                //	GUI.color = DatabaseState.SelectedColor;
                //if(GUILayout.Button ("New Sub Area Panel"))
                //{
                //	isCreatePrefabMode = !isCreatePrefabMode;
                //}
                //GUI.color = Color.white;

                //if(isCreatePrefabMode)
                //{
                //	GUILayout.BeginHorizontal();
                //	GUILayout.Label("Name");
                //	NewPrefabData.Name = EditorGUILayout.TextField(NewPrefabData.Name);
                //	GUILayout.EndHorizontal();

                //	if(GUILayout.Button ("Create"))
                //	{
                //		string masterPath = "Assets/Resources/Worlds/MasterSubArea/Master.prefab";
                //		string newPath = "Assets/Resources/Worlds/Area/" + NewPrefabData.Name + ".prefab";
                //		AssetDatabase.CopyAsset(masterPath,newPath);
                //		AssetDatabase.Refresh();
                //		foreach(var n in Node)
                //		{
                //			if(n.nodeType == NodeType.SUBAREA)
                //			{
                //				EditorNodeSubAreaMap sub = (EditorNodeSubAreaMap)n;
                //				sub.RefreshSubOption();
                //			}
                //		}
                //		NewPrefabData.Name = "";
                //	}
                //	GUILayout.Space(10f);
                //}
            }
			panelScroll = GUILayout.BeginScrollView(panelScroll);
			if(Mode == EditorMode.Map)
			{
				GUILayout.Space(10f);
                for (int n = 0; n < Node.Count; n++)
                {
                    if (!ChapterFilter || ((ChapterFilter && (Node[n].Chapter == Chapter || Node[n].Chapter == 0)) && StringSearchCompare(Node[n].NodeID, SearchKey)))
                    {
                        if (Node[n].nodeType == NodeType.AREA)
                        {
                            GUILayout.Space(10f);
                            GUILayout.Label("Jump To Area");
                            if (SelectedNodePanelButton == n)
                                GUI.color = DatabaseState.SelectedColor;
                            if (GUILayout.Button(Node[n].NodeID))
                            {
                                SelectedNodePanelButton = n;
                                SelectedSubArea = -1;
                                Vector2 delta = (CanvasRect.center - Node[n].NodeRect.center);
                                PanOffset += delta * Zoom;
                                for (int nodeCnt = 0; nodeCnt < Node.Count; nodeCnt++)
                                {
                                    Rect rectEditorNoderect = Node[nodeCnt].NodeRect;
                                    rectEditorNoderect.position += delta * Zoom;
                                    Node[nodeCnt].NodeRect = rectEditorNoderect;
                                }
                            }
                            GUI.color = Color.white;
                            GUILayout.Label("Jump To Sub Area");
                            for (int sb = 0; sb < Node[n].Child.Count; sb++)
                            {
                                if (SelectedNodePanelButton == n && SelectedSubArea == sb)
                                    GUI.color = DatabaseState.SelectedColor;
                                if (GUILayout.Button(Node[n].Child[sb].NodeID))
                                {
                                    SelectedNodePanelButton = n;
                                    SelectedSubArea = sb;
                                    Vector2 delta = (CanvasRect.center - Node[n].Child[sb].NodeRect.center);
                                    PanOffset += delta * Zoom;
                                    for (int nodeCnt = 0; nodeCnt < Node.Count; nodeCnt++)
                                    {
                                        Rect rectEditorNoderect = Node[nodeCnt].NodeRect;
                                        rectEditorNoderect.position += delta * Zoom;
                                        Node[nodeCnt].NodeRect = rectEditorNoderect;
                                    }
                                }
                                GUI.color = Color.white;
                            }
                            GUILayout.Space(10f);
                        }
                    }
                }
			}
			GUILayout.FlexibleSpace();
			GUILayout.EndScrollView();
			GUILayout.EndVertical();
		}
		protected void DrawCanvas (EditorWindow editorWindow)
		{
			if (Event.current.type == EventType.Repaint) 
			{
				GUI.BeginClip (CanvasRect);
				
				float width = Background.width / Zoom;
				float height = Background.height / Zoom;
				Vector2 offset = new Vector2 ((PanOffset.x / Zoom)%width - width, 
				                              (PanOffset.y / Zoom)%height - height);
				int tileX = Mathf.CeilToInt ((CanvasRect.width + (width - offset.x)) / width);
				int tileY = Mathf.CeilToInt ((CanvasRect.height + (height - offset.y)) / height);
				
				for (int x = 0; x < tileX; x++) 
				{
					for (int y = 0; y < tileY; y++) 
					{
						Rect texRect = new Rect (offset.x + x*width, 
						                         offset.y + y*height, 
						                         width, height);
						
						GUI.DrawTexture (texRect, Background);
					}
				}
				GUI.EndClip ();
			}

			// Check the inputs
			InputEvents (editorWindow);
			
			// We want to scale our nodes, but as GUI.matrix also scales our widnow's clipping group, 
			// we have to scale it up first to receive a correct one as a result
			#region Scale Setup
			
			// End the default clipping group
			GUI.EndGroup ();
			
			// The Rect of the new clipping group to draw our nodes in
			Rect ScaledCanvasRect = ScaleRect (CanvasRect, ZoomPos + CanvasRect.position, new Vector2 (Zoom, Zoom));
			ScaledCanvasRect.y += 23; // Header tab height
			
			// Now continue drawing using the new clipping group
			GUI.BeginGroup (ScaledCanvasRect);
			//ScaledCanvasRect.position = CanvasRect.center; // Adjust because we entered the new group
			ScaledCanvasRect.position = Vector2.zero; // Adjust because we entered the new group
			
			// Because I currently found no way to actually scale to the center of the window rather than (0, 0),
			// I'm going to cheat and just pan it accordingly to let it appear as if it would scroll to the center
			// Note, due to that, other controls are still scaled to (0, 0)
			ZoomPanAdjust = ScaledCanvasRect.center - CanvasRect.size/2 + ZoomPos;
			
			// Take a matrix backup to restore back later on
			Matrix4x4 GUIMatrix = GUI.matrix;
			
			// Scale GUI.matrix. After that we have the correct clipping group again.
			GUIUtility.ScaleAroundPivot (new Vector2 (1/Zoom, 1/Zoom), ZoomPanAdjust);
			
			#endregion

			if(makeTransitionMode.isMake && SelectedNode != -1)//gambar curve untuk buat transisi
			{
				Rect mouseRect = new Rect(e.mousePosition.x, e.mousePosition.y, 1,1);
				DrawerClass.DrawNodeCurve(Node[SelectedNode].NodeRect,mouseRect,true,CurveStatus.MAKETRANSITION,false);
				Repaint();
			}
			
			foreach(var n in Node)//gambar masing2 curve
			{
                if(!ChapterFilter || (n.Chapter == Chapter || n.Chapter == 0))
                    n.DrawCurves();
			}

			BeginWindows();

			for(int i = 0; i< Node.Count; i++)//gambar node
			{
                if(!ChapterFilter || (Node[i].Chapter == Chapter || Node[i].Chapter == 0))
                    DrawNode(Node[i],i);
			}

			EndWindows();

			GUI.matrix = GUIMatrix;
			//GUILayout.BeginArea (statePanelRect);
			//DrawStatePanel ();
			//GUILayout.EndArea ();
		}
		public void DrawNode (EditorNode editorNode,int index)
		{
			Rect nodeRect = editorNode.NodeRect;
			nodeRect.position += ZoomPanAdjust;

			editorNode.NodeRect = GUI.Window(index, editorNode.NodeRect, DrawNodeWindow, editorNode.NodeID);
		}
		void DrawNodeWindow(int ind)
		{
			if(Mode == EditorMode.Map)
			{
				if(Node[ind].nodeType == NodeType.AREA)
				{
					EditorNodeAreaMap map = (EditorNodeAreaMap)Node[ind];
					map.DrawNode(ind);
				}
				else if(Node[ind].nodeType == NodeType.SUBAREA)
				{
					EditorNodeSubAreaMap map = (EditorNodeSubAreaMap)Node[ind];
					map.DrawNode(ind);
				}
				else if(Node[ind].nodeType == NodeType.CONNECTION)
				{
					EditorNodeConnectionMap map = (EditorNodeConnectionMap)Node[ind];
					map.DrawNode(ind);
				}
			}
			GUI.DragWindow();
		}
		public void DrawStatePanel ()
		{
			GUILayout.BeginVertical();
			GUILayout.FlexibleSpace();
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			EditorGUIUtility.labelWidth = GUI.skin.label.CalcSize(new GUIContent("Zoom_")).x;
			Zoom = EditorGUILayout.Slider (new GUIContent ("Zoom"), Zoom, CanvasZoomBounds.zoomMin, CanvasZoomBounds.zoomMax);
			GUILayout.EndHorizontal();
			GUILayout.FlexibleSpace();
			GUILayout.EndVertical();
		}
		private bool isClickOnNode()
		{
			for(int i=0; i< Node.Count; i++)
			{
				if((Node[i].Chapter == 0 || Node[i].Chapter == Chapter) && Node[i].NodeRect.Contains(mousePos))
				{
					return true;
				}
			}
			return false;
		}
		private int ClickNode()
		{
			for(int i=0; i< Node.Count; i++)
			{
				if(Node[i].NodeRect.Contains(mousePos))
				{
					return i;
				}
			}
			return -1;
		}
		private void SetChildrenRect(EditorNode node)
		{
			foreach(var c in node.Child)
			{
				//set child position relative to parent
				foreach(var ch in c.Child)
				{
					SetChildrenRect(ch);
				}
			}
		}
		private void InputEvents (EditorWindow editorWindow)
		{
			e = Event.current;
			mousePos = e.mousePosition;

			bool insideCanvas = CanvasRect.Contains(mousePos);
			bool ClickedOnNode = isClickOnNode();

			//0 klik kiri
			//1 klik kanan
			//2 tombol tengah

			switch (e.type)
			{
			case EventType.MouseDown:
				if(!ClickedOnNode && !makeTransitionMode.isMake)
				{
					SelectedNode = -1;//hapus selected node
					makeTransitionMode.isMake = false;//karena di klik di luar node
					if (e.button == 2 || e.button == 0)//geser canvas
					{
						PanWindow = true;
						e.delta = Vector2.zero;
					}
					else if(e.button == 1)//klik kanan pada canvas
					{
						GenericMenu menu = new GenericMenu();
						if(Mode == EditorMode.Map)
						{
							menu.AddItem(new GUIContent("Add Area" + Mode.ToString()),false, ContextCallback, "AddNode?Area");
							menu.AddItem(new GUIContent("Add SubArea" + Mode.ToString()),false, ContextCallback, "AddNode?SubArea");
						}
						menu.ShowAsContext();
						e.Use();
					}
				}
				else if(ClickedOnNode && !makeTransitionMode.isMake)
				{
					SelectedNode = ClickNode();//set selected node
					if (e.button == 0)// klik kiri
					{
                            isEdited = true;
						//set rect children agar mengikuti this
						//SetChildrenRect(Node[SelectedNode]);
					}
					else if(e.button == 1)//klik kanan pada node
					{
						GenericMenu menu = new GenericMenu();
						if(Mode == EditorMode.Map)
						{
							if(Node[SelectedNode].nodeType == NodeType.AREA)
							{
								if(Node[SelectedNode].Child.Count > 0)
								{
									for(int cc=0;cc<Node[SelectedNode].Child.Count;cc++)
									{
                                        if (Node[SelectedNode].Child[cc].nodeType == NodeType.SUBAREA)
                                        {
                                            menu.AddItem(new GUIContent("Jump To Sub : " + Node[SelectedNode].Child[cc].NodeID), false, ContextCallback, "Move?Child?" + SelectedNode.ToString() + "?" + Node[SelectedNode].Child[cc].NodeID);
                                        }
									}
                                    menu.AddSeparator("");
                                    for (int cc = 0; cc < Node[SelectedNode].Child.Count; cc++)
                                    {
                                        if (Node[SelectedNode].Child[cc].nodeType == NodeType.SUBAREA)
                                        {
                                            menu.AddItem(new GUIContent("Delete " + Node[SelectedNode].Child[cc].NodeID + " from " + Node[SelectedNode].NodeID), false, ContextCallback, "DeleteChild?" + SelectedNode + "?" + Node[SelectedNode].Child[cc].NodeID);
                                        }
                                    }
                                    menu.AddSeparator("");
								}
							}
							if(Node[SelectedNode].nodeType == NodeType.SUBAREA)
							{
								if(Node[SelectedNode].Parent.Count == 0)//sub area cuma memiliki satu area
								{
									menu.AddItem(new GUIContent("Add This To Area"),false, ContextCallback, "AddSubToArea");
								}
								if(Node[SelectedNode].Child.Count > 0)
								{
									for(int cc=0;cc<Node[SelectedNode].Child.Count;cc++)
									{
										if(Node[SelectedNode].Child[cc].nodeType == NodeType.CONNECTION)
											menu.AddItem(new GUIContent("Jump To Connection : " + Node[SelectedNode].Child[cc].NodeID),false, ContextCallback, "Move?Child?" + SelectedNode.ToString() + "?" + Node[SelectedNode].Child[cc].NodeID);
									}
									menu.AddSeparator("");
								}
								menu.AddItem(new GUIContent("Add Connection"),false, ContextCallback, "AddConnection");
							}
							else if(Node[SelectedNode].nodeType == NodeType.CONNECTION)
							{
								if(Node[SelectedNode].Child.Count == 0)//connector cuma memiliki satu koneksi keluar
								{
									menu.AddItem(new GUIContent("Create Connection 1 way"),false, ContextCallback, "CreateConnection1");
									menu.AddItem(new GUIContent("Create Connection 2 way"),false, ContextCallback, "CreateConnection2");
								}
								foreach(var c in Node[SelectedNode].Child)
								{
									menu.AddItem(new GUIContent("Delete Connection To " + c.NodeID),false, ContextCallback, "DeleteChild?" + SelectedNode + "?" + c.NodeID);
								}
								if(Node[SelectedNode].Child.Count > 0)
								{
									menu.AddSeparator("");
									for(int cc=0;cc<Node[SelectedNode].Child.Count;cc++)
									{
										menu.AddItem(new GUIContent("Jump To Destination : " + Node[SelectedNode].Child[cc].NodeID),false, ContextCallback, "Move?Child?" + SelectedNode.ToString() + "?" + Node[SelectedNode].Child[cc].NodeID);
									}
									menu.AddSeparator("");
								}
								if(Node[SelectedNode].Parent.Count > 1)
								{
									for(int pp=0;pp<Node[SelectedNode].Parent.Count;pp++)
									{
										if(Node[SelectedNode].Parent[pp].nodeType == NodeType.CONNECTION)
											menu.AddItem(new GUIContent("Back To : " + Node[SelectedNode].Parent[pp].NodeID),false, ContextCallback, "Move?Parent?" + SelectedNode.ToString() + "?" + Node[SelectedNode].Parent[pp].NodeID);
									}
									menu.AddSeparator("");
								}
							}
						}
						menu.AddItem(new GUIContent("Delete This"),false, ContextCallback, "DeleteNode");
						menu.ShowAsContext();
						e.Use();
					}
				}
				else if(ClickedOnNode && makeTransitionMode.isMake)
				{   
					if(e.button == 0)//menambah child
					{
                        isEdited = true;
						int selectIndex = -1;
						selectIndex = SearchSelected();
						if(makeTransitionMode.TransitionMode == MakeTransitionMode.TransMode.QUEST)
						{
							if(selectIndex != -1)
							{
								if(!Node[selectIndex].Equals(Node[SelectedNode]))
								{
									Node[selectIndex].SetInput((EditorNode) Node[SelectedNode], mousePos);
                                    makeTransitionMode.isMake = false;
									SelectedNode = -1;
								}
							}
						}
						else if(makeTransitionMode.TransitionMode == MakeTransitionMode.TransMode.ADDSUBTOAREA)
						{
							if(selectIndex != -1 && Node[selectIndex].nodeType == NodeType.AREA)
							{
								if(!Node[selectIndex].Equals(Node[SelectedNode]))
								{
									Node[SelectedNode].SetInput((EditorNode)Node[selectIndex] , mousePos);
									makeTransitionMode.isMake = false;
									SelectedNode = -1;
								}
							}
						}
						else if(makeTransitionMode.TransitionMode == MakeTransitionMode.TransMode.SUBAREA1WAY)
						{
							if(selectIndex != -1 && Node[selectIndex].nodeType == NodeType.SUBAREA)
							{
								Vector2 pos = Node[selectIndex].NodeRect.position;
								pos = new Vector2(pos.x + (Node[selectIndex].Child.Count * 50f),pos.y + 200f);
								EditorNodeConnectionMap node = new EditorNodeConnectionMap();
								node.NodeRect = new Rect(pos,new Vector2(250f,150f));
								node.NodeID = Node[selectIndex].NodeID + " Connection " + Node[selectIndex].Child.Count;
								node.nodeType = NodeType.CONNECTION;
								node.SetInput((EditorNode)Node[selectIndex] , pos);
								node.SetInput((EditorNode)Node[SelectedNode] , pos);
								Node.Add(node);
								makeTransitionMode.isMake = false;
							}
							else if(selectIndex != -1 && Node[selectIndex].nodeType == NodeType.CONNECTION)
							{
								if(!Node[selectIndex].Equals(Node[SelectedNode]))
								{
									Node[selectIndex].SetInput((EditorNode)Node[SelectedNode] , mousePos);
									makeTransitionMode.isMake = false;
									SelectedNode = -1;
								}
							}
						}
						else if(makeTransitionMode.TransitionMode == MakeTransitionMode.TransMode.SUBAREA2WAY)
						{
							if(selectIndex != -1 && Node[selectIndex].nodeType == NodeType.SUBAREA)
							{
								Vector2 pos = Node[selectIndex].NodeRect.position;
								pos = new Vector2(pos.x + (Node[selectIndex].Child.Count * 50f),pos.y + 200f);
								EditorNodeConnectionMap node = new EditorNodeConnectionMap();
								node.NodeRect = new Rect(pos,new Vector2(250f,150f));
								node.NodeID = Node[selectIndex].NodeID + " Connection " + Node[selectIndex].Child.Count;
								node.nodeType = NodeType.CONNECTION;
								node.SetInput((EditorNode)Node[selectIndex] , pos);
								node.SetInput((EditorNode)Node[SelectedNode] , pos);
								Node[SelectedNode].SetInput((EditorNode)node , pos);
								Node.Add(node);
								makeTransitionMode.isMake = false;
							}
							else if(selectIndex != -1 && Node[selectIndex].nodeType == NodeType.CONNECTION)
							{
								if(!Node[selectIndex].Equals(Node[SelectedNode]) && Node[selectIndex].Child.Count < 1)
								{
									Node[SelectedNode].SetInput((EditorNode)Node[selectIndex] , mousePos);
									Node[selectIndex].SetInput((EditorNode)Node[SelectedNode] , mousePos);
									makeTransitionMode.isMake = false;
									SelectedNode = -1;
								}
							}
						}
                        Node[selectIndex].SetChildChapter();
                    }
                }
				else if(!ClickedOnNode && makeTransitionMode.isMake)
				{
					if(makeTransitionMode.TransitionMode == MakeTransitionMode.TransMode.ADDCONNECTION)
					{
                        isEdited = true;
						EditorNodeConnectionMap node = new EditorNodeConnectionMap();
						node.NodeRect = new Rect(mousePos,new Vector2(250f,150f));
						node.NodeID = Node[SelectedNode].NodeID + " Connection " + Node[SelectedNode].Child.Count;
						node.nodeType = NodeType.CONNECTION;
						Node.Add(node);
						Node[Node.Count - 1].SetInput((EditorNode) Node[SelectedNode], mousePos);
                        Node[SelectedNode].SetChildChapter();
                    }
					makeTransitionMode.isMake = false;
					SelectedNode = -1;
				}
				break;
					
			case EventType.MouseUp:
				PanWindow = false;
				break;

			/*case EventType.ScrollWheel:
					
				if (insideCanvas)
					Zoom = Mathf.Min (CanvasZoomBounds.zoomMax, Mathf.Max (CanvasZoomBounds.zoomMin, Zoom + e.delta.y / 15));
				editorWindow.Repaint();
				break;*/
			}

			if (PanWindow)//geser kanvas
			{
				PanOffset += e.delta / 2 * Zoom;
				for (int nodeCnt = 0; nodeCnt < Node.Count; nodeCnt++)
				{
					Rect rectEditorNoderect = Node[nodeCnt].NodeRect;
					rectEditorNoderect.position += e.delta / 2 * Zoom;
					Node[nodeCnt].NodeRect = rectEditorNoderect;
				}
				editorWindow.Repaint();
			}
		}

		private static Rect ScaleRect (Rect rect, Vector2 pivot, Vector2 scale) 
		{
			rect.position = Vector2.Scale (rect.position - pivot, scale) + pivot;
			rect.size = Vector2.Scale (rect.size, scale);
			return rect;
		}
		public Rect statePanelRect 
		{
			get { return new Rect (RightPanelWidth, position.height - statePanelHeight, position.width - RightPanelWidth, statePanelHeight); }
		}
		public Rect leftPanelRect 
		{
			get { return new Rect (position.width - RightPanelWidth, 0, RightPanelWidth, position.height); }
		}
		private int SearchSelected()
		{
			for(int i=0;i<Node.Count;i++)
			{
				if(Node[i].NodeRect.Contains(mousePos))
					return i;
			}
			return -1;
		}
		private void ContextCallback(object obj)
		{
            isEdited = true;

			string clb = obj.ToString();
			
			if(clb.Equals("AddSubToArea"))
			{
				if(SelectedNode != -1)
				{
					makeTransitionMode.isMake = true;
					makeTransitionMode.TransitionMode = MakeTransitionMode.TransMode.ADDSUBTOAREA;
				}
			}
			else if(clb.Equals("AddConnection"))
			{
				if(SelectedNode != -1)
				{
					makeTransitionMode.isMake = true;
					makeTransitionMode.TransitionMode = MakeTransitionMode.TransMode.ADDCONNECTION;
					makeTransitionMode.From = SelectedNode;
				}
			}
			else if(clb.Equals("CreateConnection1"))
			{
				if(SelectedNode != -1)
				{
					makeTransitionMode.isMake = true;
					makeTransitionMode.TransitionMode = MakeTransitionMode.TransMode.SUBAREA1WAY;
					makeTransitionMode.From = SelectedNode;
				}
			}
			else if(clb.Equals("CreateConnection2"))
			{
				if(SelectedNode != -1)
				{
					makeTransitionMode.isMake = true;
					makeTransitionMode.TransitionMode = MakeTransitionMode.TransMode.SUBAREA2WAY;
				}
			}
			else if(clb.Split('?')[0].Equals("Move"))
			{
				int index = int.Parse(clb.Split('?')[2]);
				string id = clb.Split('?')[3];
				Rect des = new Rect();
				if(clb.Split('?')[1].Equals("Child"))
				{
					foreach(var n in Node[index].Child)
					{
						if(n.NodeID.Equals(id))
						{
							des = n.NodeRect;
						}
					}
				}
				if(clb.Split('?')[1].Equals("Parent"))
				{
					foreach(var n in Node[index].Parent)
					{
						if(n.NodeID.Equals(id))
						{
							des = n.NodeRect;
						}
					}
				}
				Vector2 delta = (CanvasRect.center - des.center);
				PanOffset +=  delta * Zoom;
				for (int nodeCnt = 0; nodeCnt < Node.Count; nodeCnt++)
				{
					Rect rectEditorNoderect = Node[nodeCnt].NodeRect;
					rectEditorNoderect.position += delta * Zoom;
					Node[nodeCnt].NodeRect = rectEditorNoderect;
				}
			}
			else if(clb.Split('?')[0].Equals("AddNode") && Mode == EditorMode.Map)
			{
				if(clb.Split('?')[1].Equals("Area"))
				{
					EditorNodeAreaMap node = new EditorNodeAreaMap();
                    node.Chapter = Chapter;
					node.NodeRect = new Rect(mousePos,new Vector2(350f,200f));
					node.NodeID = "Area" + Node.Count.ToString();
					node.nodeType = NodeType.AREA;
                    node.Chapter = Chapter;
                    Node.Add(node);
				}
				else if(clb.Split('?')[1].Equals("SubArea"))
				{
					EditorNodeSubAreaMap node = new EditorNodeSubAreaMap();
					node.NodeRect = new Rect(mousePos,new Vector2(350f,130f));
					node.NodeID = "SubArea" + Node.Count.ToString();
					node.nodeType = NodeType.SUBAREA;
                    node.Chapter = Chapter;
                    Node.Add(node);
                }
			}
			else if(clb.Split('?')[0].Equals("AddChild"))
			{
				SelectedNode = SearchSelected();
			}
			else if(clb.Split('?')[0].Equals("DeleteChild"))
			{
				int index = int.Parse(clb.Split('?')[1]);
				string ChildID = clb.Split('?')[2];
				EditorNode select = new EditorNode();
				foreach(var c in Node[index].Child)
				{
					if(c.NodeID.Equals(ChildID))
					{
						select = c;
					}
				}
				EditorNode selectedParent = new EditorNode();
				foreach(var p in select.Parent)
				{
					if(p.NodeID.Equals(Node[index].NodeID))
					{
						selectedParent = p;
					}
				}
				select.Parent.Remove(selectedParent);
				Node[index].Child.Remove(select);
			}
			else if(clb.Equals("DeleteNode"))
			{
				if(EditorUtility.DisplayDialog("Delete","Delete Node ?","Yes","No"))
				{
					if(Node[SelectedNode].nodeType == NodeType.SUBAREA)
					{
						List<EditorNode> connect = new List<EditorNode>();

						foreach(var c in Node[SelectedNode].Child)//hapus semua connector di dalamnya
						{
							connect.Add(c);
						}
						foreach(var co in connect)
						{
							Node.Remove(co);
							foreach(var n in Node)
							{
								n.AnyDelete(co);
							}
						}
					}
					EditorNode select = Node[SelectedNode];
					Node.Remove(select);
					foreach(var n in Node)
					{
						n.AnyDelete(select);
					}
					SelectedNode = -1;
				}
			}
		} 
	}
}
