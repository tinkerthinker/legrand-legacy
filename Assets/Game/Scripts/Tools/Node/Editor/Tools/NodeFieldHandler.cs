using UnityEngine;
using System.Collections;
using System.Reflection;
using System;
using UnityEditor;
using System.IO;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using Legrand.core;
using Legrand.database;

namespace Legrand.database
{
    public class Copy
    {
        public static T DeepCopy<T>(T obj)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, obj);
                stream.Position = 0;

                return (T)formatter.Deserialize(stream);
            }
        }
    }

    public class NodeFieldHandler : EditorWindow
    {
        static Dictionary<string, string> SearchPopupKey = new Dictionary<string, string>();
        static int DropDownPopup(string name, int index, string[] data, int width = DatabaseState.DROP_DOWN_ENUM_WIDTH)
        {
            GUILayout.BeginHorizontal();
            int result = 0;
            List<int> indexData = new List<int>();
            if (DatabaseState.SearchActive)
            {
                string key = "";
                if (SearchPopupKey.ContainsKey(name))
                    key = SearchPopupKey[name];
                else
                    SearchPopupKey.Add(name, "");

                key = EditorGUILayout.TextField(key);

                List<string> newData = new List<string>();
                for (int i = 0; i < data.Length; i++)
                {
                    if (data[i].ToLower().Replace(" ", "").Contains(key.ToLower().Replace(" ", "")))
                    {
                        newData.Add(data[i]);
                        indexData.Add(i);
                        if (data[i].Equals(data[index]))
                            index = newData.Count - 1;
                    }
                }
                result = EditorGUILayout.Popup(index, newData.ToArray());

                SearchPopupKey[name] = key;

                if (result < indexData.Count)
                    result = indexData[result];
            }
            else
                result = EditorGUILayout.Popup(index, data);
            GUILayout.EndHorizontal();

            return result;
        }

        private static string RemovePrivateMark(string data)
        {
            if(data[0] == '_')
                return data.Replace("_","");
            return data;
        }
        private static void RegularTypeHandler(object objData,Type typeTest,FieldInfo field,float windowWidth,object objRef,int nodeIndex,string NodeID)
        {
            if(field.Name.Equals("material"))//map,subarea
            {
                GUILayout.Label("Material",GUILayout.Width(windowWidth/4f));
                if (GUILayout.Button((string)objData))
                {
                    EditorGUIUtility.ShowObjectPicker<Material>(null, true, null, nodeIndex);
                }
                if(Event.current.commandName == "ObjectSelectorUpdated" && Event.current.GetTypeForControl(nodeIndex) == EventType.ExecuteCommand)
                {
                    object result = objData;
                    result = EditorGUIUtility.GetObjectPickerObject();
                    if (result != null)
                    {
                        Material Mat = (Material)result;
                        objData = Mat.name;
                        field.SetValue(objRef, objData);
                    }
                    else
                    {
                        field.SetValue(objRef, "None");
                    }
                }
            }
            else if(objData is int)
            {
                int value = EditorGUILayout.IntField(RemovePrivateMark(field.Name),int.Parse(objData.ToString()));
                if(value != int.Parse(objData.ToString()))
                {
                    field.SetValue(objRef,value);
                }
            }
            else if(objData is string)
            {
                string value = EditorGUILayout.TextField(RemovePrivateMark(field.Name),objData.ToString());
                if(value != objData.ToString())
                {
                    field.SetValue(objRef,value);
                }
            }
            else if(objData is float)
            {
                float value = EditorGUILayout.FloatField(RemovePrivateMark(field.Name),float.Parse(objData.ToString()));
                if(value != float.Parse(objData.ToString()))
                {
                    field.SetValue(objRef,value);
                }
            }
            else if(objData is bool)
            {
                bool value = EditorGUILayout.Toggle(RemovePrivateMark(field.Name),Convert.ToBoolean(objData));
                if(value != Convert.ToBoolean(objData))
                {
                    field.SetValue(objRef,value);
                }
            }
            else if(typeTest.IsEnum)
            {
                GUILayout.BeginHorizontal();
                GUILayout.Label(RemovePrivateMark(field.Name),GUILayout.Width(windowWidth/2f));
                string[] EnumMember = Enum.GetNames(typeTest);
                int value = DropDownPopup(field.Name + NodeID, (int)objData, EnumMember);
                //int value = EditorGUILayout.Popup((int)objData,EnumMember ,GUILayout.Width(windowWidth/3f));
                if(value != (int)objData)
                {
                    objData = value;
                    field.SetValue(objRef,value);
                }
                GUILayout.EndHorizontal();
            }
        }

        private static void ListTypeHandler(FieldInfo field,object temp,List<string> gameObjectOption,string NodeID)
        {
            float nameWidth = 55f;
            IList test = (IList)temp;
            Type type = test.GetType().GetGenericArguments()[0];

            if(type.IsClass && type.ToString() != "System.String")
            {
                return;
            }

            GUILayout.BeginVertical("Box");
            GUILayout.Label("List : " + RemovePrivateMark(field.Name),EditorStyles.boldLabel);
            GUILayout.Label("Total data : " + test.Count);

            List<int> deletedIndex = new List<int>();

            if (field.Name.Equals("dynamicObjectsName"))//sub area, content bg
            {
                for (int cnt = 0; cnt < test.Count; cnt++)
                {
                    GUILayout.BeginHorizontal();
                    int selected = 0;
                    for (int c = 0; c < gameObjectOption.Count; c++)
                    {
						if (gameObjectOption[c].Equals(test[cnt].ToString()))
                        {
                            selected = c;
                            break;
                        }
                    }
                    GUILayout.Label(cnt.ToString() + " : ");
                    selected = DropDownPopup(field.Name + cnt.ToString() + NodeID, selected, gameObjectOption.ToArray());
                    //selected = EditorGUILayout.Popup(selected, gameObjectOption.ToArray());
                    int idx = DatabaseTools.DeleteThisIndex(test, cnt);
                    if (idx != -1)
                        deletedIndex.Add(idx);
                    if (!gameObjectOption[selected].Equals(test[cnt].ToString()))
                    {
                        test[cnt] = gameObjectOption[selected];
                    }
                    GUILayout.EndHorizontal();
                }
            }
            else if(type == typeof(bool))
            {
                for(int cnt=0;cnt<test.Count;cnt++)
                {
                    GUILayout.BeginHorizontal();
                    GUILayout.Label(RemovePrivateMark(cnt.ToString()),GUILayout.Width(nameWidth));
                    bool value = EditorGUILayout.Toggle(Convert.ToBoolean(test[cnt]));
                    int idx = DatabaseTools.DeleteThisIndex(test,cnt);
                    if(idx != -1)
                        deletedIndex.Add(idx);
                    if(value != Convert.ToBoolean(test[cnt]))
                    {
                        test[cnt] = value;
                    }
                    GUILayout.EndHorizontal();
                }
            }
            else if(type == typeof(int))
            {
                for(int cnt=0;cnt<test.Count;cnt++)
                {
                    GUILayout.BeginHorizontal();
                    int value = EditorGUILayout.IntField(RemovePrivateMark(cnt.ToString()),int.Parse(test[cnt].ToString()));
                    int idx = DatabaseTools.DeleteThisIndex(test,cnt);
                    if(idx != -1)
                        deletedIndex.Add(idx);
                    if(value != int.Parse(test[cnt].ToString()))
                    {
                        test[cnt] = value;
                    }
                    GUILayout.EndHorizontal();
                }
            }
            else if(type == typeof(float))
            {
                for(int cnt=0;cnt<test.Count;cnt++)
                {
                    GUILayout.BeginHorizontal();
                    float value = EditorGUILayout.FloatField(RemovePrivateMark(cnt.ToString()),float.Parse(test[cnt].ToString()));
                    int idx = DatabaseTools.DeleteThisIndex(test,cnt);
                    if(idx != -1)
                        deletedIndex.Add(idx);
                    if(value != float.Parse(test[cnt].ToString()))
                    {
                        test[cnt] = value;
                    }
                    GUILayout.EndHorizontal();
                }
            }
            else if(type == typeof(string) && !type.IsEnum)
            {
                for(int cnt=0;cnt<test.Count;cnt++)
                {
                    GUILayout.BeginHorizontal();
                    string value = EditorGUILayout.TextField(RemovePrivateMark(cnt.ToString()),test[cnt].ToString());
                    int idx = DatabaseTools.DeleteThisIndex(test,cnt);
                    if(idx != -1)
                        deletedIndex.Add(idx);
                    if(value != test[cnt].ToString())
                    {
                        test[cnt] = value;
                    }
                    GUILayout.EndHorizontal();
                }
            }
            else if(type.IsEnum)
            {
                for(int cnt=0;cnt<test.Count;cnt++)
                {
                    GUILayout.BeginHorizontal();
                    string[] EnumMember = Enum.GetNames(type);
                    GUILayout.Label(RemovePrivateMark(cnt.ToString()),GUILayout.Width(nameWidth));
                    int value = DropDownPopup(field.Name + NodeID, (int)test[cnt], EnumMember, Convert.ToInt32(nameWidth));
                    //int value = EditorGUILayout.Popup((int)test[cnt],EnumMember ,GUILayout.Width(nameWidth));
                    int idx = DatabaseTools.DeleteThisIndex(test,cnt);
                    if(idx != -1)
                        deletedIndex.Add(idx);
                    if(value != (int)test[cnt])
                    {
                        test[cnt] = value;
                    }
                    GUILayout.EndHorizontal();
                }
            }
            foreach(var ind in deletedIndex)
            {
                test.RemoveAt(ind);
                DatabaseGeneralTool.UnfocusElement();
            }
            GUILayout.BeginHorizontal();
            if(GUILayout.Button ("Add", GUILayout.Width(DatabaseState.BOTTOM_BUTTON_WIDTH)))
            {
                if(type == typeof(int))
                    test.Add(0);
                if(type == typeof(bool))
                    test.Add(false);
                if(type == typeof(float))
                    test.Add(0.0f);
                if(type == typeof(string) && !type.IsEnum)
                    test.Add("New Value");
                if(type.IsEnum)
                    test.Add(0);
            }
            /*if(test.Count > 0 && GUILayout.Button ("Delete", GUILayout.Width(DatabaseState.BOTTOM_BUTTON_WIDTH)))
            {
                if(EditorUtility.DisplayDialog("Delete","Do you want to delete " + test[test.Count - 1] + "?","Yes","No"))
                {
                    test.RemoveAt(test.Count - 1);
                }
            }*/
            GUILayout.EndHorizontal();
            GUILayout.EndVertical();
        }

        private static void Process(FieldInfo field,object obj,float windowWidth,int nodeIndex,List<string> gameObjectOption,string NodeID)
        {
			if(object.ReferenceEquals(null, obj))
            {
                return;
            }

            object temp = field.GetValue(obj);
			if (temp == null)
				return;
            Type typeTest = temp.GetType();

            if(temp is IList)
            {
                ListTypeHandler(field,temp,gameObjectOption, NodeID);
            }
            else
            {
                RegularTypeHandler(temp,typeTest,field,windowWidth,obj,nodeIndex, NodeID);
            }
        }

        private static void ListClassHandler(FieldInfo fieldx,object obj,float windowWidth,DatabaseStateKey state,int nodeIndex,List<string> GameObjectMapOption,DatabaseClassTypeController ClassControl,string NodeID,bool isDungeon)
        {
            object temps = fieldx.GetValue(obj);
            IList listElement = (IList)temps;

            for(int fieldNum=0;fieldNum<listElement.Count;fieldNum++)
            {
                GUILayout.BeginHorizontal();
                if(!object.ReferenceEquals(null, listElement[fieldNum]) && (fieldNum == state.Get(0,fieldx.Name)))
                {
                    FieldInfo[] fields = listElement[fieldNum].GetType().GetFields(DatabaseState.bindFlags);

                    GUILayout.BeginVertical();
                    foreach (var field in fields)
                    {
						if (field.IsNotSerialized)
							continue;
                        GUILayout.BeginHorizontal();
                        object temp = field.GetValue(listElement[fieldNum]);

                        if(!ClassControl.isKnownClass(field))
                            Process(field,listElement[fieldNum],windowWidth,nodeIndex,GameObjectMapOption,NodeID);
                        if (field.Name.Equals("ListWorldEventID"))
                        {
                            object ObjData = field.GetValue(listElement[fieldNum]);
                            object result = ClassControl.DropDownControl(field,ObjData,windowWidth,NodeID,isDungeon);
                            if(result != ObjData)
                            {
                                temp = result;
                            }
                        }
                        else if (temp is IList)//regular list type
                        {
                            IList test = (IList)temp;
                            Type type = test.GetType().GetGenericArguments()[0];
                            if (type.IsClass && type.ToString() != "System.String")
                            {
                                GUILayout.BeginVertical("Box");
                                GUILayout.Label("List : " + RemovePrivateMark(field.Name), EditorStyles.boldLabel);
                                string[] arrayDropDown = DetailElementDropDown(temp);
                                IList tests = (IList)temp;
                                GUILayout.BeginHorizontal();
                                if (test.Count > 0)
                                {
                                    GUILayout.Label("List index : ", GUILayout.Width(DatabaseState.ID_LIST_WIDTH));
                                    state.Set(0, field.Name, EditorGUILayout.Popup(state.Get(0, field.Name), arrayDropDown, GUILayout.Width(DatabaseState.DROP_DOWN_LIST_WIDTH)));
                                }
                                GUILayout.EndHorizontal();
                                GUILayout.Label("Total data : " + tests.Count);
                                ListClassHandler(field, listElement[fieldNum], windowWidth, state, nodeIndex, GameObjectMapOption,ClassControl,NodeID,isDungeon);
                                GUILayout.EndVertical();
                            }
                        }                       
                        GUILayout.EndHorizontal();
                    }
                    GUILayout.EndVertical();
                }
                GUILayout.EndHorizontal();
            }
            GUILayout.BeginHorizontal();
            int _index = state.Get(0,fieldx.Name);
            if(GUILayout.Button ("Add", GUILayout.Width(DatabaseState.BOTTOM_BUTTON_WIDTH)))
            {
                ////quest
                //if(temps.GetType() == typeof(List<Task>))
                //    listElement.Add(new Task());
                //else if(temps.GetType() == typeof(List<TaskObject>))
                //    listElement.Add(new TaskObject());
                //else if(temps.GetType() == typeof(List<Trigger>))
                //    listElement.Add(new Trigger());
                //else if(temps.GetType() == typeof(List<Reward>))
                //    listElement.Add(new Reward());
                //else if(temps.GetType() == typeof(List<QuestWorldEvents>))
                //    listElement.Add(new QuestWorldEvents());
                //else if(temps.GetType() == typeof(List<NPCTaskDialog>))
                //    listElement.Add(new NPCTaskDialog());
                //map
                if(temps.GetType() == typeof(List<SubAreaTest>))
                    listElement.Add(new SubAreaTest());
                else if(temps.GetType() == typeof(List<ConnectionTest>))
                    listElement.Add(new ConnectionTest());
                else if(temps.GetType() == typeof(List<RoomVersionTest>))
                    listElement.Add(new RoomVersionTest());
                else if (temps.GetType() == typeof(List<DungeonMonster>))
                    listElement.Add(new DungeonMonster());

                _index = listElement.Count -1;
            }
            if((listElement.Count > 0 && _index > -1) && GUILayout.Button ("Delete", GUILayout.Width(DatabaseState.BOTTOM_BUTTON_WIDTH)))
            {
                if(EditorUtility.DisplayDialog("Delete","Do you want to delete " + _index + "?","Yes","No"))
                {
                    listElement.RemoveAt(_index);
                    _index = _index - 1;
                    DatabaseGeneralTool.UnfocusElement();
                }
            }
            GUILayout.EndHorizontal();
            state.Set(0,fieldx.Name,_index);
        }
        private static bool FieldSelector(object obj,FieldInfo[] fields,FieldInfo field,List<EditorNode> Parent,List<EditorNode> Child,NodeType nodeType)
        {
            if(nodeType == NodeType.QUEST)
            {
                if(field.Name.Equals("QuestID"))
                {
                    return false;
                }
                else if (field.Name.Equals ("PrevQuestID"))
                {
                    if(Parent.Count > 0)
                    {
                        GUILayout.BeginVertical("Box");
                        foreach(EditorNode p in Parent)
                        {
                            EditorGUILayout.LabelField ("Previous Quest ID -> " + p.NodeID);
                        }
                        GUILayout.EndVertical();
                    }
                    return false;
                }
                else if (field.Name.Equals ("NextQuestID"))
                {
                    if(Child.Count > 0)
                    {
                        GUILayout.BeginVertical("Box");
                        foreach(EditorNode c in Child)
                        {
                            EditorGUILayout.LabelField ("Next Quest ID -> " + c.NodeID);
                        }
                        GUILayout.EndVertical();
                    }
                    return false;
                }
            }
            else if(nodeType == NodeType.AREA)
            {
                if (field.Name.Equals ("areaName"))
                {
                    return false;
                }
                else if (field.Name.Equals("Chapter"))
                {
                    return false;
                }
                else if (field.Name.Equals ("subArea"))
                {
                    if(Child.Count > 0)
                    {
                        GUILayout.BeginVertical("Box");
                        foreach(EditorNode c in Child)
                        {
                            EditorGUILayout.LabelField ("Sub Area Prefab Name -> " + c.NodeID);
                        }
                        GUILayout.EndVertical();
                    }
                    return false;
                }
            }
            else if(nodeType == NodeType.SUBAREA)
            {
                if (field.Name.Equals ("prefabName"))
                {
                    return false;
                }
                else if (field.Name.Equals("Chapter"))
                {
                    return false;
                }
                else if(field.Name.Equals("connections"))
                {
                    if(Child.Count > 0)
                    {
                        GUILayout.BeginVertical("Box");
                        foreach(EditorNode c in Child)
                        {
                            EditorGUILayout.LabelField ("Connection -> " + c.NodeID);
                        }
                        GUILayout.EndVertical();
                    }
                    return false;
                }
                else if(field.Name.Equals("roomVersion"))
                {
                    foreach (var f in fields)
                    {
                        if(f.Name.Equals("hasVersions"))
                        {
                            if((bool)f.GetValue(obj))
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                    }
                }
            }
            return true;
        }
        private static int getNameIndex(FieldInfo[] fields)
        {
            int name = -1;
            int id = -1;
            for (int f = 0; f < fields.Length; f++)
            {
                if (name == -1 && RemovePrivateMark(fields[f].Name).ToLower().Equals("name"))
                    name = f;
                else if (name == -1 && RemovePrivateMark(fields[f].Name).ToLower().Contains("name"))
                    name = f;
                if (id == -1 && RemovePrivateMark(fields[f].Name).ToLower().Contains("id"))
                    id = f;
            }
            if (name != -1)
                return name;
            if (id != -1)
                return id;
            return 0;
        }
        private static string[] DetailElementDropDown(object element)
        {
            List<string> dropDownList = new List<string>();
            IList listElement = (IList)element;
            int dropDownKe = 0;
            for(int fieldNum=0;fieldNum<listElement.Count;fieldNum++)
            {
                if(!object.ReferenceEquals(null, listElement[fieldNum]))
                {
                    FieldInfo[] fields = listElement[fieldNum].GetType().GetFields(DatabaseState.bindFlags);
                    int inde = getNameIndex(fields);
                    if (inde > -1)
                        dropDownList.Add(dropDownKe.ToString() + "." + RemovePrivateMark(fields[inde].Name) + " = " + fields[inde].GetValue(listElement[fieldNum]).ToString());
                    else
                        dropDownList.Add(dropDownKe.ToString());
                    dropDownKe++;
                }
            }
            return dropDownList.ToArray();
        }

        public static void Handler(object obj,float windowWidth,List<EditorNode> Parent,List<EditorNode> Child,DatabaseStateKey state,NodeType nodeType,DatabaseClassTypeController ClassControl,int nodeIndex,List<string> GameobjectMapOption, string NodeID, bool isDungeon = false)
        {
            FieldInfo[] fields = obj.GetType().GetFields(DatabaseState.bindFlags);
            foreach (var field in fields)
            {
				if (field.IsNotSerialized)
					continue;
                object temp = field.GetValue(obj);
                if(FieldSelector(obj,fields,field,Parent,Child,nodeType))
                {
                    if(!ClassControl.isKnownClass(field))
                    {
                        Process(field,obj,windowWidth,nodeIndex,GameobjectMapOption,NodeID);
                        if(temp is IList && temp.GetType() != typeof(List<string>))
                        {
                            GUILayout.BeginVertical("Box");
                            GUILayout.Label("List : " + RemovePrivateMark(field.Name),EditorStyles.boldLabel);
                            string[] arrayDropDown = DetailElementDropDown(temp);
                            IList test = (IList)temp;
                            GUILayout.BeginHorizontal();
                            if(test.Count > 0)
                            {
                                GUILayout.Label("List index : ",GUILayout.Width(DatabaseState.ID_LIST_WIDTH));
                                state.Set(0,field.Name,EditorGUILayout.Popup(state.Get(0,field.Name),arrayDropDown ,GUILayout.Width(DatabaseState.DROP_DOWN_LIST_WIDTH)));
                            }
                            GUILayout.EndHorizontal();
                            GUILayout.Label("Total data : " + test.Count);
                            ListClassHandler(field, obj, windowWidth, state, nodeIndex, GameobjectMapOption,ClassControl,NodeID,isDungeon);
                            GUILayout.EndVertical();
                        }
                    }
                    else
                    {
                        object ObjData = field.GetValue(obj);
                        object result = ClassControl.DropDownControl(field,ObjData,windowWidth,NodeID,isDungeon);
                        if(result != ObjData)
                        {
                            temp = result;
                        }
                    }
                }
            }
        }
        private static void viewConnectionPortal(List<EditorNode> Parent,List<EditorNode> Child)
        {
            if(Parent.Count > 1 || Child.Count > 0)
            {
                GUILayout.BeginVertical("Box");
                string value = "";
                foreach(EditorNode p in Parent)
                {
                    if(p.nodeType == NodeType.CONNECTION)
                    {
                        value = p.NodeID;
                        string subFrom = "";
                        foreach(var pp in p.Parent)
                        {
                            if(pp.nodeType == NodeType.SUBAREA)
                            {
                                subFrom = pp.NodeID;
                            }
                        }
                        EditorGUILayout.LabelField ("From Portal :" + value);
                        EditorGUILayout.LabelField ("From Sub :" + subFrom);
                    }
                }
                foreach(EditorNode c in Child)
                {
                    if(c.nodeType == NodeType.CONNECTION)
                    {
                        value = c.NodeID;
                        string subTo = "";
                        foreach(var pp in c.Parent)
                        {
                            if(pp.nodeType == NodeType.SUBAREA)
                            {
                                subTo = pp.NodeID;
                            }
                        }
                        EditorGUILayout.LabelField ("To Portal :" + value);
                        EditorGUILayout.LabelField ("To Sub :" + subTo);
                    }
                }
                GUILayout.EndVertical();
            }
        }
        public static List<string> FilterPortal(string name,List<string> option)
        {
            List<string> newList = new List<string>();

            foreach (var opt in option)
            {
                if (name.Equals(opt.Split('+')[0]))
                {
                    newList.Add(opt.Split('+')[1]);
                }
            }
            if (newList.Count > 0)
                return newList;
            return option;
        }
        public static string HandlerForConnection(string ConnName,object obj,List<string> optionPoint,List<string> optionPortal, List<string> worldMapPointList, List<EditorNode> Parent,List<EditorNode> Child,string NodeID)
        {
            string ConnectionName = "";

            FieldInfo[] fields = obj.GetType().GetFields(DatabaseState.bindFlags);

            foreach (var field in fields)
            {
                object temp = field.GetValue(obj);
                if (field.Name.Equals("ConnectionName"))
                {
                    GUILayout.BeginHorizontal();
                    int selected = 0;

                    GUILayout.Label("Portal");
                    if (Parent.Count == 0)
                    {
                        for (int c = 0; c < optionPortal.Count; c++)
                        {
                            if (optionPortal[c].Equals(ConnName))
                            {
                                selected = c;
                                break;
                            }
                        }
                        selected = DropDownPopup(field.Name + NodeID, selected, optionPortal.ToArray());
                        //selected = EditorGUILayout.Popup(selected, optionPortal.ToArray());
                        ConnectionName = optionPortal[selected];
                    }
                    else
                    {
                        string[] newOpt = FilterPortal(Parent[0].NodeID, optionPortal).ToArray();

                        for (int c = 0; c < newOpt.Length; c++)
                        {
                            if (newOpt[c].Equals(ConnName))
                            {
                                selected = c;
                                break;
                            }
                        }
                        selected = DropDownPopup(field.Name + NodeID, selected, newOpt);
                        //selected = EditorGUILayout.Popup(selected, newOpt);
                        if (newOpt.Length > 0)
                            ConnectionName = newOpt[selected];
                        else
                            ConnectionName = "Empty";
                    }

                    GUILayout.EndHorizontal();

                    viewConnectionPortal(Parent, Child);
                }
                else if (temp is IList && field.Name.Equals("unlockPoint"))
                {
                    IList test = (IList)temp;
                    GUILayout.BeginVertical("Box");
                    GUILayout.Label("Unlock Point", EditorStyles.boldLabel);
                    GUILayout.Label("Total data : " + test.Count);

                    List<int> deletedIndex = new List<int>();

                    for (int cnt = 0; cnt < test.Count; cnt++)
                    {
                        GUILayout.BeginHorizontal();
                        int selected = 0;
                        for (int c = 0; c < optionPoint.Count; c++)
                        {
                            if (optionPoint[c].Equals(test[cnt].ToString()))
                            {
                                selected = c;
                                break;
                            }
                        }
                        GUILayout.Label(cnt.ToString());
                        //selected = EditorGUILayout.Popup(selected, optionPoint.ToArray());
                        selected = DropDownPopup(field.Name + NodeID, selected, optionPoint.ToArray());
                        int idx = DatabaseTools.DeleteThisIndex(test, cnt);
                        if (idx != -1)
                            deletedIndex.Add(idx);
                        if (optionPoint[selected] != test[cnt].ToString())
                        {
                            test[cnt] = optionPoint[selected];
                        }
                        GUILayout.EndHorizontal();

                        foreach (var ind in deletedIndex)
                        {
                            test.RemoveAt(ind);
                            DatabaseGeneralTool.UnfocusElement();
                        }
                    }
                    if (GUILayout.Button("Add", GUILayout.Width(DatabaseState.BOTTOM_BUTTON_WIDTH)))
                    {
                        test.Add("");
                    }
                    GUILayout.EndVertical();
                }
                else if (temp is IList && field.Name.Equals("unlockWorldMapPoint"))
                {
                    IList test = (IList)temp;
                    GUILayout.BeginVertical("Box");
                    GUILayout.Label("Unlock World Map Point", EditorStyles.boldLabel);
                    GUILayout.Label("Total data : " + test.Count);

                    List<int> deletedIndex = new List<int>();

                    for (int cnt = 0; cnt < test.Count; cnt++)
                    {
                        GUILayout.BeginHorizontal();
                        int selected = 0;
                        for (int c = 0; c < worldMapPointList.Count; c++)
                        {
                            if (worldMapPointList[c].Equals(test[cnt].ToString()))
                            {
                                selected = c;
                                break;
                            }
                        }
                        GUILayout.Label(cnt.ToString());
                        //selected = EditorGUILayout.Popup(selected, optionPoint.ToArray());
                        selected = DropDownPopup(field.Name + NodeID, selected, worldMapPointList.ToArray());
                        int idx = DatabaseTools.DeleteThisIndex(test, cnt);
                        if (idx != -1)
                            deletedIndex.Add(idx);
                        if (worldMapPointList[selected] != test[cnt].ToString())
                        {
                            test[cnt] = worldMapPointList[selected];
                        }
                        GUILayout.EndHorizontal();

                        foreach (var ind in deletedIndex)
                        {
                            test.RemoveAt(ind);
                            DatabaseGeneralTool.UnfocusElement();
                        }
                    }
                    if (GUILayout.Button("Add", GUILayout.Width(DatabaseState.BOTTOM_BUTTON_WIDTH)))
                    {
                        test.Add("");
                    }
                    GUILayout.EndVertical();
                }
                Type typeTest = temp.GetType();
                if (typeTest == typeof(WwiseManager.WorldFX))
                {
                    GUILayout.BeginHorizontal();
                    GUILayout.Label(RemovePrivateMark(field.Name));
                    string[] EnumMember = Enum.GetNames(typeTest);
                    //int value = EditorGUILayout.Popup((int)temp,EnumMember ,GUILayout.Width(150f));
                    int value = DropDownPopup(field.Name + NodeID, (int)temp, EnumMember, 150);
                    GUILayout.EndHorizontal();
                    field.SetValue(obj, value);
                }
            }
            return ConnectionName;
        }
    }
}
