﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using Legrand.database;

public class MapConnectionEditor : NodeEditorSystem
{
	private static MapConnectionEditor editor;
	[MenuItem("Legrand Legacy/Node Editor/Map Connection Editor")]
	static void CreateEditor () 
	{
		editor = GetWindow<MapConnectionEditor> ();
		editor.titleContent.text = "Map Connection Editor";
		editor.minSize = new Vector2(800f,600f);
		editor.Show();
	}
	void OnEnable()
	{
		Mode = EditorMode.Map;
		init();
	}
	void OnDisable()
	{
        if (isEdited)
        {
            if (EditorUtility.DisplayDialog("Exit Confirm", "Do you want to save changes?", "Yes", "No"))
            {
                Save();
            }
        }
        Exit();
	}
	void OnGUI()
	{
		Draw(editor);
	}
}
