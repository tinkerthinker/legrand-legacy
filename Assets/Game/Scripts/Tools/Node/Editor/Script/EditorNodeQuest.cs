﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Legrand.database
{
	public class EditorNodeQuest : EditorNode
	{
		public Quest QuestData;
		List<string> dummy;

		public EditorNodeQuest()
		{
			dummy = new List<string>();
			QuestData = new Quest();
		}

		public override void DrawNode (int nodeIndex)
		{
			ScrollPosition = GUILayout.BeginScrollView(ScrollPosition);
			NodeID = EditorGUILayout.TextField("ID",NodeID);
			NodeFieldHandler.Handler(QuestData,300f,Parent,Child,state,NodeType.QUEST,ClassControl,nodeIndex,dummy,nodeIndex.ToString());
			GUILayout.EndScrollView();
		}

		public override void DrawCurves ()
		{
			foreach(var p in Parent)
			{
				DrawerClass.DrawNodeCurve(p.NodeRect, NodeRect,false, CurveStatus.QUEST, true);
			}
		}
	}
}
