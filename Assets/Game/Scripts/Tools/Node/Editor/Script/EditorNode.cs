﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Legrand.database
{
	public class EditorNode
	{
        public int Chapter;
		public string NodeID;
		public List<EditorNode> Parent;
		public List<EditorNode> Child;
		public Rect NodeRect = new Rect();
		protected Vector2 ScrollPosition;
		public List<int> SelectedIndex;
		protected DatabaseStateKey state;
		public NodeType nodeType;
		protected DatabaseClassTypeController ClassControl;
        Dictionary<string, string> SearchPopupKey = new Dictionary<string, string>();
        public virtual bool GetDataBoolean()
        {
            return false;
        }
        public void SetChildChapter()
        {
            foreach (EditorNode child in Child)
            {
                child.Chapter = Chapter;
                foreach (EditorNode chil in child.Child)
                {
                    chil.Chapter = Chapter;
                }
            }
        }

        public virtual void SetThisChapter()
        {

        }

        protected int DropDownPopup(string name, int index, string[] data, int width = DatabaseState.DROP_DOWN_ENUM_WIDTH)
        {
            GUILayout.BeginHorizontal();
            int result = 0;
            List<int> indexData = new List<int>();
            if (DatabaseState.SearchActive)
            {
                string key = "";
                if (SearchPopupKey.ContainsKey(name))
                    key = SearchPopupKey[name];
                else
                    SearchPopupKey.Add(name, "");
                
                key = EditorGUILayout.TextField(key);

                List<string> newData = new List<string>();
                for (int i = 0; i < data.Length; i++)
                {
                    if (data[i].ToLower().Replace(" ", "").Replace("_", "").Contains(key.ToLower().Replace(" ", "").Replace("_", "")))
                    {
                        newData.Add(data[i]);
                        indexData.Add(i);
                        if (data[i].Equals(data[index]))
                            index = newData.Count - 1;
                    }
                }
                result = EditorGUILayout.Popup(index, newData.ToArray());

                SearchPopupKey[name] = key;

                if (result < indexData.Count)
                    result = indexData[result];
            }
            else
                result = EditorGUILayout.Popup(index, data);
            GUILayout.EndHorizontal();

            return result;
        }

        public EditorNode()
		{
            Chapter = 0;
			NodeID = "";
			Parent = new List<EditorNode>();
			Child = new List<EditorNode>();
			NodeRect = new Rect();
			ScrollPosition = new Vector2();
			SelectedIndex = new List<int>();
			state = new DatabaseStateKey();
			ClassControl = new DatabaseClassTypeController();
			nodeType = NodeType.QUEST;
		}

		public virtual void DrawNode(int nodeIndex)
		{

		}

		public void AnyDelete(EditorNode deleted)
		{
			List<int> deletedParent = new List<int>();
			for(int i=0;i<Parent.Count;i++)
			{
				if(Parent[i].Equals(deleted))
				{
					deletedParent.Add(i);
				}
			}
			foreach(var p in deletedParent)
			{
				Parent.RemoveAt(p);
			}
			List<int> deletedChild = new List<int>();
			for(int i=0;i<Child.Count;i++)
			{
				if(Child[i].Equals(deleted))
				{
					deletedChild.Add(i);
				}
			}
			foreach(var p in deletedChild)
			{
				Child.RemoveAt(p);
			}
		}

		public void SetInput (EditorNode node, Vector2 clickPos)
		{
			clickPos.x -= NodeRect.x;
			clickPos.y -= NodeRect.y;
			
			Parent.Add(node);
			node.Child.Add(this);
		}

		public virtual void DrawCurves()
		{

		}
	}
}

