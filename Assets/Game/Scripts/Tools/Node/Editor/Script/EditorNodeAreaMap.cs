﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System;

namespace Legrand.database
{
    public static class ListDataMap
    {
        public static List<string> AreaList;
        public static List<string> PrefabSubAreaName;
        public static List<string> UnlockedPortalList;
        public static List<string> PortalListOption;
        public static List<string> WorldMapPointList;

        public static void init()
        {
            AreaList = new List<string>();
            PrefabSubAreaName = new List<string>();
            UnlockedPortalList = new List<string>();
            PortalListOption = new List<string>();
            WorldMapPointList = new List<string>();
        }
    }
	public class EditorNodeAreaMap : EditorNode
	{
		public AreaTest AreaData;
        List<string> AreaList = ListDataMap.AreaList;
		List<string> dummy;
        int ChapterTemp = 0;

        public override bool GetDataBoolean()
        {
            return AreaData.areaType == AreaTest.AreaTypes.Dungeon ? true : false;
        }

        public override void SetThisChapter()
        {
            Chapter = AreaData.Chapter;
            ChapterTemp = Chapter;
        }

        public EditorNodeAreaMap()
		{
			dummy = new List<string>();
			AreaData = new AreaTest();
			GetFolderName();
        }
		private void GetFolderName()
		{
			for(int i=0;i<AreaList.Count;i++)
			{
				AreaList[i] = new DirectoryInfo(@AreaList[i]).Name;
			}
		}
		private int getSelected()
		{
			for(int i=0;i<AreaList.Count;i++)
			{
                if(LegrandUtility.CompareString(AreaList[i], NodeID))
				{
					return i;
				}
			}
			return 0;
		}
		public override void DrawNode (int nodeIndex)
		{
			ScrollPosition = GUILayout.BeginScrollView(ScrollPosition);
            GUILayout.BeginHorizontal();
            ChapterTemp = EditorGUILayout.IntField("Chapter", ChapterTemp);
            if (GUILayout.Button("Set Chapter"))
            {
                Chapter = ChapterTemp;
                AreaData.Chapter = Chapter;
                SetChildChapter();
            }
            GUILayout.EndHorizontal();
			GUILayout.BeginHorizontal();
			GUILayout.Label("Area Name",GUILayout.Width(150f));
            //int selected = EditorGUILayout.Popup(getSelected(),AreaList.ToArray());
            int selected = DropDownPopup("Area", getSelected(), AreaList.ToArray());
			GUILayout.EndHorizontal();
			NodeID = AreaList[selected];
			NodeFieldHandler.Handler(AreaData,300f,Parent,Child,state,NodeType.AREA,ClassControl,nodeIndex,dummy,nodeIndex.ToString());
			GUILayout.EndScrollView();
		}

		public override void DrawCurves ()
		{
			foreach(var p in Parent)
			{
				DrawerClass.DrawNodeCurve(p.NodeRect, NodeRect,false, CurveStatus.AREATOAREA, false);
			}
		}
	}
	public class EditorNodeSubAreaMap : EditorNode
	{
		public SubAreaTest SubAreaData;
        List<string> PrefabSubAreaName = ListDataMap.PrefabSubAreaName;
		List<string> GameObjectOptions;
		int SelectedPrefabName = 0;
        bool isDungeon = false;

        public EditorNodeSubAreaMap()
		{
			SubAreaData = new SubAreaTest();
			GameObjectOptions = new List<string>();
        }

		public void RefreshSubOption()
		{
			PrefabSubAreaName.Clear();
			GetGameObject.Get("Assets/Resources/Worlds/Area",PrefabSubAreaName,false,GetStatus.SUBAREA);
		}

		private int getSelected()
		{
			for(int i=0;i<PrefabSubAreaName.Count;i++)
			{
                if(LegrandUtility.CompareString(PrefabSubAreaName[i], NodeID))
				{
					return i;
				}
			}
			return 0;
		}
		private void setRect()
		{
			BindingFlags bindFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
			FieldInfo[] fields = SubAreaData.GetType().GetFields(bindFlags);

			foreach (var field in fields)
			{
				if(field.Name.Equals ("hasVersions"))
				{
					if(!(bool)field.GetValue(SubAreaData))
					{
						NodeRect.size = new Vector2(350f,300f);
					}
					else
					{
						NodeRect.size = new Vector2(350f,360f);
					}
				}
			}
		}
		void RefreshOption()
		{
			GameObjectOptions.Clear();
			GetGameObject.GetChildOfGameObject("Assets/Resources/Worlds/Area",PrefabSubAreaName[SelectedPrefabName],GameObjectOptions);
		}
		public override void DrawNode (int nodeIndex)
		{
            ScrollPosition = GUILayout.BeginScrollView(ScrollPosition);
			GUILayout.BeginHorizontal();
			GUILayout.Label("Prefab Name",GUILayout.Width(150f));
            //int selected = EditorGUILayout.Popup(getSelected(),PrefabSubAreaName.ToArray());
            int selected = DropDownPopup("SubArea", getSelected(), PrefabSubAreaName.ToArray());
            if (selected != SelectedPrefabName)
			{
				SelectedPrefabName = selected;
				RefreshOption();
			}
			GUILayout.EndHorizontal();
			NodeID = PrefabSubAreaName[selected];
            if (Parent.Count > 0)
                isDungeon = Parent[0].GetDataBoolean();
            else
                isDungeon = true;
			NodeFieldHandler.Handler(SubAreaData,300f,Parent,Child,state,NodeType.SUBAREA,ClassControl,nodeIndex,GameObjectOptions,nodeIndex.ToString(),isDungeon);
			GUILayout.EndScrollView();

			setRect();
		}

		public override void DrawCurves ()
		{
			foreach(var p in Parent)
			{
				DrawerClass.DrawNodeCurve(p.NodeRect, NodeRect,false, CurveStatus.AREATOSUB, false);
			}
		}
	}
	public class EditorNodeConnectionMap : EditorNode
	{
		public ConnectionTest ConnectionData;
        List<string> UnlockedPortalList = ListDataMap.UnlockedPortalList;
		List<string> PortalListOption = ListDataMap.PortalListOption;
        List<string> WorldMapPointList = ListDataMap.WorldMapPointList;

        public EditorNodeConnectionMap()
		{
			ConnectionData = new ConnectionTest();
        }

		public override void DrawNode (int nodeIndex)
		{
			ScrollPosition = GUILayout.BeginScrollView(ScrollPosition);
			NodeID = NodeFieldHandler.HandlerForConnection(NodeID,ConnectionData,UnlockedPortalList,PortalListOption,WorldMapPointList, Parent,Child,nodeIndex.ToString());
			GUILayout.EndScrollView();
		}
		
		public override void DrawCurves ()
		{
            foreach (var p in Parent)
			{
				if(p.nodeType == NodeType.SUBAREA)
				{
					DrawerClass.DrawNodeCurve(p.NodeRect, NodeRect,false, CurveStatus.SUBTOCONNECTOR, false);
				}
				else if(p.nodeType == NodeType.CONNECTION)
				{
					bool twoWay = false;
					foreach(var ch in Child)
					{
                        if(LegrandUtility.CompareString(ch.NodeID, p.NodeID))
						{
							twoWay = true;
							break;
						}
					}
					if(twoWay)
						DrawerClass.DrawNodeCurve(p.NodeRect, NodeRect,false, CurveStatus.CONNECTOR2WAY, false);
					else
						DrawerClass.DrawNodeCurve(p.NodeRect, NodeRect,false, CurveStatus.CONNECTOR1WAY, true);
				}
			}
		}
	}
}