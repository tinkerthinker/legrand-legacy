using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.database;

namespace Legrand.database
{
	public enum NodeType {QUEST,AREA,SUBAREA,CONNECTION};
}

[System.Serializable]
public class EditorNodeData
{
	public string NodeID;
	public List<int> Parent;
	public List<int> Child;
	public Rect NodeRect;
	public NodeType NodeType;
	
	public EditorNodeData()
	{
		NodeType = NodeType.QUEST;
		NodeID = "";
		Parent = new List<int>();
		Child = new List<int>();
		NodeRect = new Rect();
	}
}