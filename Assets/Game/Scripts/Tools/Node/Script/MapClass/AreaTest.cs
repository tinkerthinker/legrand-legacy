using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Legrand.database
{
	[System.Serializable]
	public class AreaTest 
	{
	    public enum AreaTypes
	    {
	        City,
	        Dungeon,
	        WorldMap
	    };

        public int Chapter;
	    public string areaName;
	    public string areaDescription;
	    public AreaTypes areaType;
	    public List<SubAreaTest> subArea;//tambahkan dengan drag drop

        public AreaTest()
		{
            Chapter = 0;
			areaName = "";
			areaDescription = "";
			areaType = AreaTypes.City;
			subArea = new List<SubAreaTest>();
		}
	}
}