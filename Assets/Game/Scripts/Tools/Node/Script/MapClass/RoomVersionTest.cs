using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class RoomVersionTest
{
	[System.Serializable]
	public class Content
	{
		public string material;
		public List<string> gameObjects;

		public Content()
		{
			material = "";
			gameObjects = new List<string>();
		}
	}

	public RoomVersionTest()
	{
		version = "";
		modification = new Content();
	}

	public string version;
	public Content modification;
}
