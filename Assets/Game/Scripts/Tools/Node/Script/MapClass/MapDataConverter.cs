using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class MapDataConverter : MonoBehaviour
{
	public MapAreaDatabase areaData;
	public MapSubAreaData subAreaData;
	public MapConnectionData connectionData;

	List<Area> ReadyAreaData = new List<Area>();
    Dictionary<string, string> FastTravelData;

    public Dictionary<string,string> GetFastTravelData()
    {
        return FastTravelData;
    }

    void GetMapData()
	{
		List<SubArea> subTemp = new List<SubArea>();
		List<RoomVersion> roomVersionTemp = new List<RoomVersion>();		
		List<SubArea.Connection> connectionTemp = new List<SubArea.Connection>();
        FastTravelData = new Dictionary<string, string>();

        for (int a=0;a<areaData.Count;a++)
		{
			subTemp.Clear();
			foreach(var sub in areaData.Get(a).subArea)
			{
				for(int su=0;su<subAreaData.Count;su++)
				{
                    //digunakan untuk memasukkan data portal ke seluruh list sub area.
                    if (sub.prefabName.Equals(subAreaData.Get(su).prefabName))//dilakukan pengecekan seperti ini untuk mendeteksi portal yang sedang dilakukan pengecekan di sub area dan memasukkan data portal sesuai database
					{
						connectionTemp.Clear();
                        if (subAreaData.Get(su).isFastTravelDestination)
                        {
                            FastTravelData.Add(areaData.Get(a).areaName, subAreaData.Get(su).prefabName);
                        }
						foreach(var connect in subAreaData.Get(su).connections)
						{
							for(int c=0;c<connectionData.Count;c++)
							{
								if(connectionData.Get(c).ConnectionName.Equals(connect.ConnectionName))
								{
									if(!connectionData.Get(c).portalTo.Equals(""))
									{
										SubArea.Connection CT = new SubArea.Connection();
										CT.portalFrom = connectionData.Get(c).portalFrom;
										CT.portalTo = connectionData.Get(c).portalTo;
										CT.destination = connectionData.Get(c).destination;
										CT.unlockPoint = connectionData.Get(c).unlockPoint.ToArray();
                                        CT.unlockWorldMapPoint = connectionData.Get(c).unlockWorldMapPoint.ToArray();
                                        CT.SFX = connectionData.Get(c).SFX;
										connectionTemp.Add(CT);
									}
								}
							}
						}
                        //apkah ini digunakan untuk memasukkan material setiap subarea sesuai data di database?
						roomVersionTemp.Clear();
						if (subAreaData.Get (su).hasVersions) {
							foreach (var romver in subAreaData.Get(su).roomVersion) {
								RoomVersion RV = new RoomVersion ();
								RV.version = romver.version;
								if (!romver.modification.material.Equals ("None") && !romver.modification.material.Equals ("")) {
									RV.modification.material = (Material)Resources.Load (romver.modification.material) as Material;
								}
								List<GameObject> gmo = new List<GameObject> ();
								foreach (var rv in romver.modification.gameObjects) {
									gmo.Add (GetGameObjectComponent (rv));
								}
								RV.modification.gameObjects = gmo.ToArray ();
								roomVersionTemp.Add (RV);
							}
						}
                        SubArea subs = new SubArea(subAreaData.Get(su).prefabName, subAreaData.Get(su).subAreaName, (SubArea.Types)subAreaData.Get(su).subAreaType,connectionTemp.ToArray(), subAreaData.Get(su).BGM, 
                            subAreaData.Get(su).hasVersions, roomVersionTemp.ToArray(), subAreaData.Get(su).mainHorizontalFOV, subAreaData.Get(su).secondaryHorizontalFOV, subAreaData.Get(su).dynamicObjectsName.ToArray(), subAreaData.Get(su).dungeonMonsters.ToArray());
						subTemp.Add(subs);
					}
				}
			}
			Area areaTemp = new Area(areaData.Get(a).areaName,areaData.Get(a).areaDescription,(Area.AreaTypes)areaData.Get(a).areaType,subTemp.ToArray());
			ReadyAreaData.Add(areaTemp);
		}
	}
    
	GameObject GetGameObjectFromFullPath(string Path)
	{
		Path = Path.Replace("Resources","?").Split('?')[1].Substring(1).Replace(@"\",@"/").Split('.')[0];

		GameObject Go = (GameObject)Resources.Load(Path) as GameObject;
		return Go;
	}

	void GetGameObjectInSubDir(string objectName,string path,ref Object Go)
	{
		bool found = false;
		string [] subdirectoryEntries = Directory.GetDirectories(path);
		foreach(var sub in subdirectoryEntries)
		{
			DirectoryInfo dir = new DirectoryInfo(sub);
			FileInfo[] info = dir.GetFiles("*.prefab");
			foreach (FileInfo f in info)
			{
				string nama = f.Name.Split('.')[0];
				if(nama.Equals(objectName))
				{
					Go = GetGameObjectFromFullPath(f.FullName);
					found = true;
					break;
				}
			}
			if(!found)
				GetGameObjectInSubDir(objectName,sub,ref Go);
		}
	}

	void GetChilds(string name,GameObject Go,ref Object child)
	{
		for(int a=0;a<Go.transform.childCount;a++)
		{
			if(Go.transform.GetChild(a).name.Equals(name))
			{
				child = Go.transform.GetChild(a).gameObject;
			}
			else
			{
				GetChilds(name,Go.transform.GetChild(a).gameObject,ref child);
			}
		}
	}

	GameObject GetGameObjectComponent(string rawPath)
	{
		string path = "Assets/Resources/Worlds/Area";
		Object Go = Resources.Load (rawPath.Split('?')[1]);

		GameObject parent = (GameObject)Go as GameObject;
		Object child = new Object();
		GetChilds(rawPath.Split('?')[0],parent,ref child);
        Debug.Log(child.name);
		GameObject childObject = (GameObject)child as GameObject;

		return childObject;
	}

	public Area[] GetData()
	{
		ReadyAreaData.Clear();
		GetMapData();
		return ReadyAreaData.ToArray();
	}
}
