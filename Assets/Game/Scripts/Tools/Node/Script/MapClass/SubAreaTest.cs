using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Legrand.database
{
	[System.Serializable]
	public class SubAreaTest
	{
	    public enum Types
	    {
	        Room,
	        CityMap
	    }
        

		public string prefabName;//dropdown
        public string subAreaName;
        public bool isFastTravelDestination;
        public List<ConnectionTest> connections;
	    public Types subAreaType;    
	    public WwiseManager.BGM BGM;
		public float mainHorizontalFOV;
        public float secondaryHorizontalFOV;
        public bool hasVersions;
		public List<RoomVersionTest> roomVersion;//ditampilkn ketika hasversion true
        public List<string> dynamicObjectsName;
        public List<DungeonMonster> dungeonMonsters;

        public SubAreaTest()
		{
			prefabName = "";
            subAreaName = "";
			subAreaType = Types.Room;
			connections = new List<ConnectionTest>();
			BGM = WwiseManager.BGM.NONE;
			dynamicObjectsName = new List<string> ();
			hasVersions = false;
			roomVersion= new List<RoomVersionTest>();
			mainHorizontalFOV = 0f;
            secondaryHorizontalFOV = 0f;
            isFastTravelDestination = false;
            dungeonMonsters = new List<DungeonMonster>();
		}
	}
}
