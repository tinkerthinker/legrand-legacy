﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Legrand.database
{
	[System.Serializable]
	public class ConnectionTest
	{
		public string ConnectionName;//ditampilin aja ah
		public string portalFrom;//dropdown
		public string destination;//dropdown
		public string portalTo;//dropdown
		public List<string> unlockPoint;//setiap bertipe citymap, subnya,subArea destination gk semua punya unlock point, tampilkan nama kota + titiknya tadi
        public List<string> unlockWorldMapPoint;//Point world map yg akan di unlock
        public WwiseManager.WorldFX SFX;
		
		public ConnectionTest()
		{
			ConnectionName = "";
			portalFrom = "";
			destination = "";
			portalTo = "";
			unlockPoint = new List<string>();
            unlockWorldMapPoint = new List<string>();
            SFX = WwiseManager.WorldFX.NONE;
		}
	}
}