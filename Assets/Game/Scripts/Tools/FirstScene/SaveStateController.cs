﻿using UnityEngine;
using System.Collections;

public class SaveStateController : MonoBehaviour
{
    void Start()
    {
        Cursor.visible = true;
    }
    public void ToTitleScene()
    {
        Cursor.visible = false;
        SceneManager.SwitchScene ("TitleScene");
    }
}
