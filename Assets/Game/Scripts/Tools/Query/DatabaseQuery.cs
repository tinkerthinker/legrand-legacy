﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

namespace Legrand.database
{
    public enum Condition
    {
        ANY,EQUAL,LESS,GREATER,LESSANDSAME,GREATERANDSAME
    };
    public class DatabaseQuery : MonoBehaviour
    {
        public static DatabaseQuery Instance;
        [SerializeField]UnityEngine.Object[] databaseFile;
        Dictionary<string,IComponentDatabase> DatabaseDictionary = new Dictionary<string, IComponentDatabase>();
        BindingFlags bindFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static;
        void Awake()
        {
            Instance = this;
            DatabaseDictionary.Clear();
            foreach (UnityEngine.Object file in databaseFile)
            {
                IComponentDatabase data = (IComponentDatabase)file;
                if (!DatabaseDictionary.ContainsKey(data.DatabaseID))
                    DatabaseDictionary.Add(data.DatabaseID, data);
                else
                    Logger.Log("ID database : " + data.DatabaseID + ", sudah ada yg punya");
            }
        }
        bool Compare(Condition condition, int valueA, int valueB)
        {
            if(condition == Condition.ANY)
                return true;
            else if(condition == Condition.LESS && valueA < valueB)
                return true;
            else if(condition == Condition.GREATER && valueA > valueB)
                return true;
            else if(condition == Condition.LESSANDSAME && valueA <= valueB)
                return true;
            else if(condition == Condition.GREATERANDSAME && valueA >= valueB)
                return true;
            return false;
        }
        bool Compare(Condition condition, float valueA, float valueB)
        {
            if(condition == Condition.ANY)
                return true;
            else if(condition == Condition.LESS && valueA < valueB)
                return true;
            else if(condition == Condition.GREATER && valueA > valueB)
                return true;
            else if(condition == Condition.LESSANDSAME && valueA <= valueB)
                return true;
            else if(condition == Condition.GREATERANDSAME && valueA >= valueB)
                return true;
            return false;
        }
        bool CheckCondition(Condition condition,object objectA,object objectB)
        {
            if (objectB == null || condition == Condition.ANY)
                return true;

            if (condition == Condition.EQUAL && objectA.Equals(objectB))
                return true;

            if ((objectA.GetType() == typeof(string) || objectA.GetType() == typeof(bool)) && (condition != Condition.ANY || condition != Condition.EQUAL))
                return false;

            if (objectA.GetType() == typeof(int))
                return Compare(condition, (int)objectA, (int)objectB);

            if (objectA.GetType() == typeof(float))
                return Compare(condition, (float)objectA, (float)objectB);

            return false;
        }
        public T[] Query<T>(string from,string[] fieldName = null,Condition condition = Condition.ANY,object where = null)
        {
            fieldName = fieldName == null ? new string[0] : fieldName;
            List<T> result = new List<T>();
            for (int i = 0; i < DatabaseDictionary[from].Count; i++)
            {
                if (fieldName.Length > 0)
                {
                    object obj = DatabaseDictionary[from].GetObjectFieldValue(i, fieldName[0]);
                    if (obj is IList)
                    {
                        IList listData = (IList)obj;
                        Query<T>(listData, fieldName, 0, condition, where, ref result);
                    }
                    else if (CheckCondition(condition, obj, where))
                    {
                        result.Add((T)DatabaseDictionary[from].GetItem(i));
                    }
                }
                else
                {
                    result.Add((T)DatabaseDictionary[from].GetItem(i));
                }
            }
            return result.ToArray();
        }
        void Query<T>(IList Data,string[] fieldName,int index,Condition condition,object where,ref List<T> result)
        {
            index++;
            foreach (var data in Data)
            {
                FieldInfo[] fields = data.GetType().GetFields(bindFlags);
                foreach (FieldInfo field in fields)
                {
                    object obj = field.GetValue(data);
                    if (field.Name.Equals(fieldName[index]))
                    {
                        if (obj is IList)
                        {
                            IList ilist = (IList)obj;
                            Query<T>(ilist, fieldName, index, condition, where, ref result);
                        }
                        else if ((fieldName.Length <= index) || (field.Name.Equals(fieldName[index]) && CheckCondition(condition, obj, where)))
                        {
                            result.Add((T)data);
                        }
                        break;
                    }
                }
            }
        }
    }
}
