﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

namespace Legrand.War
{
    public class TopPanel : MonoBehaviour
    {
        public CanvasGroup Panel;
        public Morales Morale;
        [SerializeField]
        private Text FieldName;

        public void Initiate()
        {
            SetFieldName(string.Empty);
        }

        void OnEnable()
        {
            WarInfo.TopPanel = this;
        }

        void OnDisable()
        {
            WarInfo.TopPanel = null;
        }

        public void Diactivate()
        {
            Panel.DOFade(0f, 0.5f);
        }

        public void Activate()
        {
            Panel.DOFade(1f, 0.5f);
        }

        public void SetFieldName(string fieldName)
        {
            string newName = fieldName;
            if (fieldName == string.Empty) newName = "-";
            FieldName.text = newName;
        }
    }
}