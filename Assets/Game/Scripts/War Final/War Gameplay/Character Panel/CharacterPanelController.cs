﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using DG.Tweening;
using TeamUtility.IO;

namespace Legrand.War
{
    public class CharacterPanelController : MonoBehaviour {
        [SerializeField]
        private RectTransform AllyPanelsParent;
        [SerializeField]
        private CharacterPanel[] AllyPanels;
        [SerializeField]
        private RectTransform EnemyPanelsParent;
        [SerializeField]
        private CharacterPanel[] EnemyPanels;

        private IEnumerator AllyIenumerator;
        private IEnumerator EnemyIenumerator;

        void Awake()
        {
            if(EventManager.Instance)
            EventManager.Instance.AddListener<ChangeController>(ChangePanelTabInputType);
        }

        void OnDestroy()
        {
            if (EventManager.Instance)
                EventManager.Instance.RemoveListener<ChangeController>(ChangePanelTabInputType);
        }

        public void OnEnable()
        {
            WarInfo.CharacterPanelController = this;
            HideAllyPanel();
            HideEnemyPanel();

            if (InputManager.PlayerOneConfiguration.name == "KeyboardAndMouse")
                ChangePanelTabInputType(new ChangeController(ControllerType.Keyboard));
            else if (InputManager.PlayerOneConfiguration.name == "Gamepad")
                ChangePanelTabInputType(new ChangeController(ControllerType.XboxController));
        }

        public void OnDisable()
        {
            WarInfo.CharacterPanelController = null;
        }

        public void UpdateAllyPanel(int index, WarCharacter character, bool tabActive = false)
        {
            UpdatePanel(index, character, AllyPanels, tabActive);
        }

        public void UpdateAllyPanel(WarCharacter character, bool tabActive = false)
        {
            UpdatePanel(character, AllyPanels, tabActive);
        }

        public void HideAllyPanel()
        {
            HidePanelCharacters(AllyPanels);
        }

        public void HideAllyPanel(int index)
        {
            HidePanelCharacters(AllyPanels[index]);
        }

        public void HideAllyPanel(int[] index)
        {
            for (int i = 0; i < index.Length; i++)
                HideAllyPanel(index[i]);
        }

        public void HideAllyPanel(WarCharacter c)
        {
            bool found = false;
            for(int i=0; i<AllyPanels.Length && !found;i++)
            {
                if(c.UniqueID == AllyPanels[i].GetCharacterID())
                {
                    HidePanelCharacters(AllyPanels[i]);
                    found = true;
                }    
            }
        }

        public void ShowAllyPanel()
        {
            ShowPanelCharacters(AllyPanels);
        }

        public void ShowAllyPanel(int index)
        {
            ShowPanelCharacters(AllyPanels[index]);
        }

        public void ShowAllyPanel(int[] index)
        {
            for (int i = 0; i < index.Length; i++)
                ShowAllyPanel(index[i]);
        }

        public void UpdateEnemyPanel(int index, WarCharacter character)
        {
            UpdatePanel(index, character, EnemyPanels);
        }

        public void UpdateEnemyPanel(WarCharacter character)
        {
            UpdatePanel(character, EnemyPanels);
        }

        public void HideEnemyPanel()
        {
            HidePanelCharacters(EnemyPanels);
        }

        public void HideEnemyPanel(int index)
        {
            HidePanelCharacters(EnemyPanels[index]);
        }

        public void HideEnemyPanel(int[] index)
        {
            HidePanelCharacters(EnemyPanels);
        }

        public void HideEnemyPanel(WarCharacter c)
        {
            bool found = false;
            for (int i = 0; i < EnemyPanels.Length && !found; i++)
            {
                if (c.UniqueID == EnemyPanels[i].GetCharacterID())
                {
                    HidePanelCharacters(EnemyPanels[i]);
                    found = true;
                }
            }
        }

        public void ShowEnemyPanel()
        {
            ShowPanelCharacters(EnemyPanels);
        }

        public void ShowEnemyPanel(int index)
        {
            ShowPanelCharacters(EnemyPanels[index]);
        }

        public void ShowEnemyPanel(int[] index)
        {
            for(int i=0;i<index.Length;i++)
                ShowEnemyPanel(index[i]);
        }

        private void UpdatePanel(WarCharacter character, CharacterPanel[] charPanels, bool tabActive = false)
        {
            if(Array.Exists(charPanels, x => x.GetCharacterID() == character.UniqueID)) return;

            CharacterPanel charPanel = Array.Find(charPanels, x => x.GetCharacterID() == string.Empty);
            charPanel.Set(character, tabActive);
        }

        private void UpdatePanel(int index, WarCharacter character, CharacterPanel[] charPanels, bool tabActive = false)
        {
            charPanels[index].Set(character, tabActive);
        }

        private void ShowPanelCharacters(CharacterPanel[] charPanels)
        {
            for (int i = 0; i < charPanels.Length; i++) ShowPanelCharacters(charPanels[i]);
        }

        private void ShowPanelCharacters(CharacterPanel charPanel)
        {
            charPanel.Show();
        }

        private void HidePanelCharacters(CharacterPanel[] charPanels)
        {
            for (int i = 0; i < charPanels.Length; i++) HidePanelCharacters(charPanels[i]);
        }

        private void HidePanelCharacters(CharacterPanel charPanel)
        {
            charPanel.Hide();
        }

        private void ChangePanelTabInputType(ChangeController e)
        {
            if(e.CtrlType == ControllerType.Keyboard)
            {
                for (int i = 0; i < AllyPanels.Length; i++) AllyPanels[i].ChangeToKeyboard();
            }
            else
            {
                for (int i = 0; i < AllyPanels.Length; i++) AllyPanels[i].ChangeToController();
            }
        }
	}
}
