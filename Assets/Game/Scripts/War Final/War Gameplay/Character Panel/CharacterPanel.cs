﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Legrand.core;
using DG.Tweening;

namespace Legrand.War
{
	public class CharacterPanel : MonoBehaviour {
		public Text Name;
		public Image Avatar;
		public Image Class;
		public Text Troops;
		public Text HP;
		public Text AP;
		public Text STR;
        public Text STRBuff;
        public Text DEF;
        public Text DEFBuff;
        public Text AGI;
        public Text AGIBuff;

        public Image[] Strong;
		public Image[] Weakness;

        public ChangeableKeySprite TabL;
        public ChangeableKeySprite TabR;

        public bool IsShown;

        public float PanelMoveSpeed = 5f;
        public float ShowXPosition;
        public float HideXPosition;

        private Legrand.War.WarCharacter _ThisPanelCharacter;
		private CanvasGroup _CanvasGroup;
        private IEnumerator _CurrentEnumerator;

		public void Awake()
		{
			_CanvasGroup = GetComponent<CanvasGroup> ();
		}

		public void Set(Legrand.War.WarCharacter newCharacter, bool showTab = false)
		{
			_ThisPanelCharacter = newCharacter;
			if (_ThisPanelCharacter == null)
				return;
			Name.text = _ThisPanelCharacter.Name + "";
            Avatar.sprite = newCharacter.Avatar;

            Troops.text = _ThisPanelCharacter.Troops + "";
            HP.text = _ThisPanelCharacter.Health.Value + "/" + _ThisPanelCharacter.Health.MaxValue;
			AP.text = _ThisPanelCharacter.AP + "";
			STR.text = _ThisPanelCharacter.GetSTR() + "";
			DEF.text = _ThisPanelCharacter.GetDEF() + "";
			AGI.text = _ThisPanelCharacter.GetAGI() + "";

            if (TabL != null && TabR != null)
            {
                TabL.gameObject.SetActive(showTab);
                TabR.gameObject.SetActive(showTab);
            }
            

            if (_CanvasGroup == null)
                _CanvasGroup = GetComponent<CanvasGroup>();
            _CanvasGroup.alpha = 1;
		}

		public void Clear()
		{
			_ThisPanelCharacter = null;
			Name.text = "";
			Troops.text = "";
			HP.text = "";
			AP.text = "";
			STR.text = "";
			DEF.text = "";
			AGI.text = "";

			for (int i = 0; i < Strong.Length; i++)
				Strong [i].gameObject.SetActive (false);
			for (int i = 0; i < Weakness.Length; i++)
				Weakness [i].gameObject.SetActive (false);

            if (TabL != null && TabR != null)
            {
                TabL.gameObject.SetActive(false);
                TabR.gameObject.SetActive(false);
            }

            if (_CanvasGroup == null)
				_CanvasGroup = GetComponent<CanvasGroup> ();
			_CanvasGroup.alpha = 0;
		}

        public void Show()
        {
            if (_CurrentEnumerator != null) StopCoroutine(_CurrentEnumerator);
            _CurrentEnumerator = ShowEnumerator();
            StartCoroutine(_CurrentEnumerator);
        }

        private IEnumerator ShowEnumerator()
        {
            // This class always put on uGUI
            RectTransform thisRect = (RectTransform)transform;
            Vector3 target = new Vector3(ShowXPosition, thisRect.anchoredPosition.y);
            while(Mathf.Abs(thisRect.anchoredPosition.x - ShowXPosition) > 1f)
            {
                thisRect.anchoredPosition = Vector3.Lerp(new Vector3(thisRect.anchoredPosition.x, thisRect.anchoredPosition.y), target, Time.deltaTime * PanelMoveSpeed);
                yield return new WaitForEndOfFrame();
            }
        }

        public void Hide()
        {
            if (_CurrentEnumerator != null) StopCoroutine(_CurrentEnumerator);
            _CurrentEnumerator = HideEnumerator();
            StartCoroutine(_CurrentEnumerator);
        }

        private IEnumerator HideEnumerator()
        {
            // This class always put on uGUI
            RectTransform thisRect = (RectTransform)transform;
            Vector3 target = new Vector3(HideXPosition, thisRect.anchoredPosition.y);
            while (Mathf.Abs(thisRect.anchoredPosition.x - HideXPosition) > 1f)
            {
                thisRect.anchoredPosition = Vector3.Lerp(new Vector3(thisRect.anchoredPosition.x, thisRect.anchoredPosition.y), target, Time.deltaTime * PanelMoveSpeed);
                yield return new WaitForEndOfFrame();
            }
            Clear();
        }

        public void ChangeToController()
        {
            if (TabL != null) TabL.ChangeToController();
            if (TabR != null) TabR.ChangeToController();
        }
        public void ChangeToKeyboard()
        {
            if (TabL != null) TabL.ChangeToKeyBoard();
            if (TabR != null) TabR.ChangeToKeyBoard();
        }

        public string GetCharacterID()
        {
            return _ThisPanelCharacter == null ? string.Empty : _ThisPanelCharacter.UniqueID;
        }
    }
}