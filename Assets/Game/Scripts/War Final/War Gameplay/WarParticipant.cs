﻿using UnityEngine;
using System.Collections;

namespace Legrand.War
{
	public class WarParticipant : MonoBehaviour {
		public string WarID;
		public WarParticipantModel[] AllyParticipants;
		public WarParticipantModel[] EnemyParticipants;

		public WarTroopStarter TroopStarter;
		public WarParticipantCondition ParticipantCondition;
		void OnEnable()
		{
			WarInfo.WarParticipant = this;
		}

		void OnDisable()
		{
			WarInfo.WarParticipant = null;
		}

		public void PrepareParticipant()
		{
			ParticipantCondition = new WarParticipantCondition ();
			InitiateParticipant (AllyParticipants, BattlerType.Player);
			InitiateParticipant (EnemyParticipants, BattlerType.Enemy);
            /*
            WarInfo.TopPanel.Morale.SetAllySigil(TroopStarter.AllyTroopName);
            WarInfo.TopPanel.Morale.SetEnemySigil(TroopStarter.EnemyTroopName);
            */
            WarInfo.TopPanel.Morale.UpdateMorale(TroopStarter.MoralBalance);
        }

		private void InitiateParticipant(WarParticipantModel[] warModels, BattlerType type)
		{
			for (int i = 0; i < warModels.Length; i++)
				InitiateParticipant (warModels[i], type);
		}

		private void InitiateParticipant(WarParticipantModel warModel, BattlerType type)
		{
			warModel.ParticipantObject = Instantiate <GameObject> (Resources.Load <GameObject>("Characters/War/Generals/War Player "+warModel.ID));
			Legrand.War.WarCharacter warChar = warModel.ParticipantObject.GetComponent<Legrand.War.WarCharacter> ();
			warChar.Setup (warModel.ID, warModel.UniqueID, type, warModel.Controlable, warModel.AIType);

			warModel.ParticipantObject.SetActive (false);
			if (type == BattlerType.Player) {
				if (warModel.Type == ParticipantType.Active)
					ParticipantCondition.NonActiveAlly.Add (warChar);
				else
					ParticipantCondition.HiddenAlly.Add (warChar);
			} else {
				if(warModel.Type == ParticipantType.Active)	ParticipantCondition.NonActiveEnemy.Add (warChar);
				else ParticipantCondition.HiddenEnemy.Add (warChar);
			}
		}

		public void DeployStarterTroop()
		{
			for(int i=0;i<TroopStarter.EnemyStarter.Length;i++)
				SetActiveEnemy (TroopStarter.EnemyStarter[i].TroopUniqueID, TroopStarter.EnemyStarter[i].GridID, TroopStarter.EnemyStarter[i].Direction );
			
			for(int i=0;i<TroopStarter.AllyStarter.Length;i++)
				SetActiveAlly (TroopStarter.AllyStarter[i].TroopUniqueID, TroopStarter.AllyStarter[i].GridID, TroopStarter.EnemyStarter[i].Direction);
		}

		public void DeployAllyTroop(WarCharacter deployedCharacter, string gridID)
		{
			if (deployedCharacter.gameObject.activeInHierarchy) {
				WarInfo.GridController.SpawnCharacterAt (deployedCharacter, gridID, deployedCharacter.SeeDirection);
				WarInfo.MovementController.MoveCharacterPosition (deployedCharacter);
			} 
			else 
			{
				SetActiveAlly (deployedCharacter.UniqueID, gridID);
			}
		}

		public void RemoveAllyTroop(WarCharacter removedCharacter)
		{
			SetNonActiveAlly (removedCharacter.ID, removedCharacter.CurrentGrid.ParentGrid.ID);
		}

		public bool SetActiveEnemy(string uniqueID, string gridID, WarDirection direction = WarDirection.Random)
		{
			WarParticipantModel activatedEnemy = System.Array.Find (EnemyParticipants, x => x.UniqueID == uniqueID);
			WarCharacter activatedEnemyCharacter = LegrandUtility.GetComponentInChildren<WarCharacter>(activatedEnemy.ParticipantObject);
			if (activatedEnemy != null && ParticipantCondition.CheckEnemyStatus (uniqueID, false) && WarInfo.GridController.SpawnCharacterAt (activatedEnemyCharacter, gridID, direction))
			{
				WarInfo.MovementController.MoveCharacterPosition (activatedEnemyCharacter);
					ParticipantCondition.SetActivateEnemy (uniqueID, true);
					activatedEnemy.ParticipantObject.SetActive (true);
					return true;
			}
			return false;
		}

		public bool SetNonActiveEnemy(string uniqueID, string gridID)
		{
			WarParticipantModel activatedEnemy = System.Array.Find (EnemyParticipants, x => x.UniqueID == uniqueID);
			WarCharacter activatedEnemyCharacter = LegrandUtility.GetComponentInChildren<WarCharacter>(activatedEnemy.ParticipantObject);
			if (activatedEnemy != null && ParticipantCondition.CheckEnemyStatus (uniqueID, true) && WarInfo.GridController.RemoveCharacterAt (activatedEnemyCharacter, gridID))  
			{
				ParticipantCondition.SetActivateEnemy (uniqueID, false);
				activatedEnemy.ParticipantObject.SetActive (false);
				return true;
			}
			return false;
		}

		public bool SetActiveAlly(string uniqueID, string gridID, WarDirection direction = WarDirection.Random)
		{
			WarParticipantModel activatedAlly = System.Array.Find (AllyParticipants, x => x.UniqueID == uniqueID);
			WarCharacter activatedAllyCharacter = LegrandUtility.GetComponentInChildren<WarCharacter>(activatedAlly.ParticipantObject);
			if (activatedAlly != null && ParticipantCondition.CheckAllyStatus (uniqueID, false) && WarInfo.GridController.SpawnCharacterAt (activatedAllyCharacter, gridID, direction))
			{
				WarInfo.MovementController.MoveCharacterPosition (activatedAllyCharacter);
				ParticipantCondition.SetActivateAlly (uniqueID, true);
				activatedAlly.ParticipantObject.SetActive (true);
				return true;
			}
			return false;
		}

		public bool SetNonActiveAlly(string uniqueID, string gridID)
		{
			WarParticipantModel activatedAlly = System.Array.Find (AllyParticipants, x => x.UniqueID == uniqueID);
			WarCharacter activatedAllyCharacter = LegrandUtility.GetComponentInChildren<WarCharacter>(activatedAlly.ParticipantObject);
			if (activatedAlly != null && ParticipantCondition.CheckAllyStatus (uniqueID, true) && WarInfo.GridController.RemoveCharacterAt (activatedAllyCharacter, gridID)) 
			{
				ParticipantCondition.SetActivateAlly (uniqueID, false);
				activatedAlly.ParticipantObject.SetActive (false);
				return true;
			}
			return false;
		}
	}
		
	[System.Serializable]
	public class WarParticipantModel
	{
		public string ID;
		public string UniqueID;
		public ParticipantType Type;
        public WarAIType AIType;
		public bool Controlable = true;
		[HideInInspector]
		public GameObject ParticipantObject; 
	}

	public enum ParticipantType
	{
		Active,
		Reserved
	}

    public enum WarAIType
    {
        Aggresive,
        Defend
    }
}