﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;

namespace Legrand.War
{
    public class WarCommandController : MonoBehaviour
    {
        public delegate void WarCommandSelect(WarCommand c);
        public WarCommandSelect WarCommandSelected;

        public WarCommandButton[] CommandButton;

        public void SetActive(bool active)
        {
            this.gameObject.SetActive(active);
        }

        public void SetSelected()
        {
            Array.Find(CommandButton, x => x.CommandType == WarCommand.Move).CommandButton.Select();
            OnSelect((int)WarCommand.Move);
        }

        public void OnEnable()
        {
            CheckButtonIsValid();
            SetSelected();
            PutOnCharacter();
        }

        public void PutOnCharacter()
        {
            RectTransform thisRectTransform = GetComponent<RectTransform>();
            Transform headPosition = WarInfo.TurnController.PlayedCharacter.GetComponentInChildren<RegObjDialogue>().transform;
            thisRectTransform.anchoredPosition3D = headPosition.position;
        }

        public void CheckButtonIsValid()
        {
            int currentAC = WarInfo.TurnController.PlayedCharacter.AC;
            for (int i = 0; i < CommandButton.Length; i++)
            {
                if (WarDB._Instance.WarCommandCostModels.Find(x => x.CommandType == CommandButton[i].CommandType).Cost <= currentAC)
                    CommandButton[i].CommandButton.interactable = true;
                else
                    CommandButton[i].CommandButton.interactable = false;
            }
        }

        public void ChooseCommand(int command)
        {
            if (WarCommandSelected != null)
                WarCommandSelected((WarCommand)command);
        }

        public void OnSelect(int command)
        {
            for (int i = 0; i < CommandButton.Length; i++)
            {
                if (CommandButton[i].CommandType == (WarCommand)command)
                {
                    CommandButton[i].CommandButton.gameObject.transform.localScale = new Vector3(1f, 1f, 1f);
                }
                else
                {
                    CommandButton[i].CommandButton.gameObject.transform.localScale = new Vector3(0.8f, 0.8f, 1f);
                }
            }
        }
    }



    [System.Serializable]
    public class WarCommandButton
    {
        public WarCommand CommandType;
        public Button CommandButton;
    }

    [System.Serializable]
    public class WarCommandCost
    {
        public WarCommand CommandType;
        public int Cost;
    }
}