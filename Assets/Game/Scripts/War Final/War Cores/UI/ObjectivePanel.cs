﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;

namespace Legrand.War
{
	public class ObjectivePanel : MonoBehaviour {
		public CanvasGroup Panel;
		public Text ListObjective;
		public float FadeTime = 0.5f;
		public void Open()
		{
			WarInfo.WarObjectives.CheckObjective ();
			ListObjective.text = "";
			for (int i = 0; i < WarInfo.WarObjectives.Objectives.Length; i++) 
			{
				string objectiveLine = (i + 1) + ".\t" + WarInfo.WarObjectives.Objectives [i].Description;
				if (WarInfo.WarObjectives.Objectives [i].ObjectiveStatus == ObjectiveStatus.Success)
					objectiveLine = StrikeThrough (objectiveLine);

				ListObjective.text += objectiveLine;
				if (i + 1 < WarInfo.WarObjectives.Objectives.Length)
					ListObjective.text += "\n";
			}
			Panel.DOFade (1f, FadeTime);
		}

		public void Close()
		{
			Panel.DOFade (0f, FadeTime);
		}

		public string StrikeThrough(string s)
		{
			string strikeThrough = "";
			foreach (char c in s)
				strikeThrough = strikeThrough + c + '\u0336';
			return strikeThrough;
		}
	}
}