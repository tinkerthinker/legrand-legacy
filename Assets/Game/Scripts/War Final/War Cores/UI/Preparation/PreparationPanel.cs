﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PreparationPanel : MonoBehaviour {
	public GameObject LastActive;
	public CanvasGroup Panel;

	public void Activate()
	{
		Panel.interactable = true;
		Panel.alpha = 1f;
		EventSystem.current.SetSelectedGameObject (LastActive);
	}

	public void ChangeActive(GameObject g)
	{
		WwiseManager.Instance.PlaySFX (WwiseManager.UIFX.Cursor_Moving);
		LastActive = g;
	}

	public void Diactivate()
	{
		EventSystem.current.SetSelectedGameObject (null);
		Panel.interactable = false;
		Panel.alpha = 0f;
	}
}
