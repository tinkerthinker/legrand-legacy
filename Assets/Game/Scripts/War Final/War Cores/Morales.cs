﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Legrand.War
{
    public class Morales : MonoBehaviour
    {
        /*
        [SerializeField]
        private Image AllySigil;
        [SerializeField]
        private Image EnemySigil;
        */
        [SerializeField]
        private Material MoraleMaterial;
        [SerializeField]
        private float Balance;

        void OnDisable()
        {
            this.gameObject.SetActive(false);
        }

        /*
        public void SetAllySigil(string allyName)
        {
            AllySigil.sprite = LegrandBackend.Instance.GetSpriteData(allyName);
        }

        public void SetEnemySigil(string enemyName)
        {
            EnemySigil.sprite = LegrandBackend.Instance.GetSpriteData(enemyName);
        }
        */

        public void UpdateMorale(float newBalance)
        {
            Balance = newBalance;
            MoraleMaterial.SetFloat("_Scale", Balance * 0.5f);
        }
    }
}