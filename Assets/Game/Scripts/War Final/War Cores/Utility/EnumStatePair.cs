﻿using UnityEngine;
using System.Collections;

namespace Legrand.War
{
	[System.Serializable]
	public struct WarStateEnumPair
	{
		public WarStateEnum StateEnum;
		public WarState State;
	}
}