﻿using UnityEngine;
using System.Collections;

public class ObjectFinder{
	private Transform _Source;

	public Transform FindNearestObject(Transform source)
	{
		return FindNearestObject (source, "");
	}

	public Transform FindNearestObject(Transform source, string layerMask)
	{
		_Source = source;
		RaycastHit raycast;
		Collider[] candidates = Physics.OverlapSphere (_Source.position, 20, 1 << LayerMask.NameToLayer (layerMask));

		if (candidates.Length > 0) {
			System.Array.Sort (candidates, Comparer);
			return candidates [0].transform;
		}
		else return null;
	}

	int Comparer(Collider t1, Collider t2)
	{
		float c1Distance = Vector3.Distance (_Source.transform.position, t1.transform.position);
		float c2Distance = Vector3.Distance (_Source.transform.position, t2.transform.position);
		if (c1Distance > c2Distance)
			return 1;
		else if (c1Distance < c2Distance)
			return -1;
		else
			return 0;
	}
}
