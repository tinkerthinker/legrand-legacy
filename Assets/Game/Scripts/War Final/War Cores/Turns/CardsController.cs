﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Legrand.War
{
    public class CardsController : MonoBehaviour {
        public Transform[] CardParents;

        public Color AllyColor;
        public Color EnemyColor;

        public List<Card> InactiveCards;
        public List<Card> ActiveCards;

        public void InitiateCard(List<TurnData> thisTurnData, List<TurnData> nextTurnData)
        {
            InactiveCards.AddRange(ActiveCards);
            ActiveCards = new List<Card>();
            var allTurnData = thisTurnData.Concat(nextTurnData);

            for (int i = 0; i < allTurnData.Count(); i++)
                AddNewChar(allTurnData.ElementAt(i).Character);
                
        }

        public void ReconstructCard(List<TurnData> thisTurnData, List<TurnData> nextTurnData)
        {
            List<TurnData> allTurnData = thisTurnData.Concat(nextTurnData).ToList();
            
            for (int i = 0; i < allTurnData.Count(); i++)
                SetCard(allTurnData[i].Character.UniqueID, i);
        }

        public void SetCard(string uniqueID, int turn)
        {
            Card card = ActiveCards.Find(x=> x.CardCharacter.UniqueID == uniqueID);
            if (card != null) card.UpdateTurn(CardParents[turn]);
        }

        public void SetActivePanel()
        {

        }

        public void AddNewChar(WarCharacter newChar)
        {
            if (InactiveCards.Count > 0)
            {
                Card newCard = InactiveCards[0];
                Color color = newChar.FactionType == BattlerType.Player ? AllyColor : EnemyColor;
                newCard.SetCard(newChar, color);
                InactiveCards.Remove(newCard);
                newCard.gameObject.SetActive(true);
                ActiveCards.Add(newCard);
            }
            else Debug.Log("Slot Card is empty but need to add character");
        }

        public void DestroyCard(string uniqueID)
        {
            Card card = ActiveCards.Find(x => x.CardCharacter.UniqueID == uniqueID);
            if(card != null)
            {
                ActiveCards.Remove(card);
                card.gameObject.SetActive(false);
                InactiveCards.Add(card);
            }
        }
    }
}