﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Legrand.War
{
    public class Card : MonoBehaviour {
        public Image Avatar;
        public Image Color;

        public WarCharacter CardCharacter;

        public void Reset()
        {
            CardCharacter = null;
            Avatar.sprite = null;
            Color.color = new Color(1f,1f,1f,0f);
        }

        public void SetCard(WarCharacter character, Color color)
        {
            CardCharacter = character;
            Avatar.sprite = character.Avatar;
            Color.color = color;
        }

        public void UpdateTurn(Transform parent)
        {
            this.transform.SetParent(parent);
        }

        void Update()
        {
            Vector3 newPos = Vector3.Lerp(this.transform.localPosition, new Vector3(0f, 0, 0f), Time.deltaTime * 5f);
            this.transform.localPosition = new Vector3(newPos.x, this.transform.localPosition.y, this.transform.localPosition.z);
        }
    }
}
