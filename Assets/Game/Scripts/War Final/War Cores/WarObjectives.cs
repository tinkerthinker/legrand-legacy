﻿using UnityEngine;
using System.Collections;

namespace Legrand.War
{
	public class WarObjectives : MonoBehaviour {
		public WarObjective[] Objectives;

		void OnEnable()
		{
			WarInfo.WarObjectives = this;
		}

		void OnDisable()
		{
			WarInfo.WarObjectives = null;
		}

		public ObjectiveStatus CheckObjective()
		{
			ObjectiveStatus currentObjectiveCondition= ObjectiveStatus.Success;
			for (int i = 0; i < Objectives.Length; i++) 
			{
				ObjectiveStatus thisObjectiveCondition = Objectives [i].IsFinished ();

				if (currentObjectiveCondition == ObjectiveStatus.Failed)
					continue;

				if (thisObjectiveCondition == ObjectiveStatus.OnGoing)
					currentObjectiveCondition = ObjectiveStatus.OnGoing;
				else if(thisObjectiveCondition == ObjectiveStatus.Failed)
					currentObjectiveCondition = ObjectiveStatus.Failed;
			}
			return currentObjectiveCondition;
		}

		public void UpdateObjective()
		{
		}
	}
}