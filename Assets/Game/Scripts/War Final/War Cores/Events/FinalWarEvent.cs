﻿using UnityEngine;
using System.Collections;

namespace Legrand.War
{
	public class SetPointerActive : GameEvent
	{
		public bool PointerActive;
		public bool InputActive;

		public SetPointerActive(bool pointerActive)
		{
			PointerActive = pointerActive;
			InputActive = pointerActive;
		}

		public SetPointerActive(bool pointerActive, bool inputActive)
		{
			PointerActive = pointerActive;
			InputActive = inputActive;
		}
	}

    public class SetInfoActive : GameEvent
    {
        public bool CharacterInfoActive;
        public bool GridInfoActive;
        public bool AutomaticChangeValue;

        public SetInfoActive(bool characterInfoActive, bool gridInfoActive)
        {
            CharacterInfoActive = characterInfoActive;
            GridInfoActive = gridInfoActive;
            AutomaticChangeValue = true;
        }

        public SetInfoActive(bool characterInfoActive, bool gridInfoActive, bool ChangeImmediately)
        {
            CharacterInfoActive = characterInfoActive;
            GridInfoActive = gridInfoActive;
            AutomaticChangeValue = ChangeImmediately;
        }
    }

    public class ChangeCameraFocus : GameEvent
	{
		public Transform NewFocus;

		public ChangeCameraFocus(Transform newFocus)
		{
			NewFocus = newFocus;
		}
	}

    public class WarCharacterDied : GameEvent
    {
        public string UniqueID;

        public WarCharacterDied(string uniqueID)
        {
            UniqueID = uniqueID;
        }
    }

    public class WarCommandSetActiveEvent : GameEvent
    {
        public bool Active;

        public WarCommandSetActiveEvent(bool active)
        {
            Active = active;
        }
    }
}