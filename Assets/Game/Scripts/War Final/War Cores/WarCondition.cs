﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Legrand.War
{
	[System.Serializable]
	public class WarParticipantCondition {
		public List<WarCharacter> ActiveAlly;
		public List<WarCharacter> NonActiveAlly;
		public List<WarCharacter> HiddenAlly;
		public List<WarCharacter> KilledAlly;

		public List<WarCharacter> ActiveEnemy;
		public List<WarCharacter> NonActiveEnemy;
		public List<WarCharacter> HiddenEnemy;
		public List<WarCharacter> KilledEnemy;

		public WarParticipantCondition()
		{
			ActiveAlly = new List<WarCharacter>();
			NonActiveAlly = new List<WarCharacter>();
			HiddenAlly = new List<WarCharacter>();
			KilledAlly = new List<WarCharacter>();

			ActiveEnemy = new List<WarCharacter>();
			NonActiveEnemy = new List<WarCharacter>();
			HiddenEnemy = new List<WarCharacter>();
			KilledEnemy = new List<WarCharacter>();
		}

		public bool IsAllEnemyKilled()
		{
			return ActiveEnemy != null && ActiveEnemy.Count == 0 ? true : false;
		}

		public bool IsAllAlliesKilled()
		{
			return ActiveAlly != null && ActiveAlly.Count == 0 ? true : false;
		}

		public double CounterEnemyKilled()
		{
			return KilledEnemy != null ? KilledEnemy.Count : 0;
		}

		public bool SetActivateAlly(string uniqueID, bool isActive)
		{
			if (isActive) {
				WarCharacter warChar = NonActiveAlly.Find (x => x.UniqueID == uniqueID);
				if (warChar == null)
					return false;

				warChar.gameObject.SetActive (isActive);
				NonActiveAlly.Remove (warChar);
				ActiveAlly.Add (warChar);
			}
			else 
			{
				WarCharacter warChar = ActiveAlly.Find (x => x.UniqueID == uniqueID);
				if (warChar == null)
					return false;
				
				warChar.gameObject.SetActive (isActive);
				ActiveAlly.Remove (warChar);
				NonActiveAlly.Add (warChar);

			}
			return true;
		}

		public bool CheckAllyStatus(string uniqueID, bool isActive)
		{
			WarCharacter warChar = null;
			if (!isActive)
				warChar = NonActiveAlly.Find (x => x.UniqueID == uniqueID);
			else
				warChar = ActiveAlly.Find (x => x.UniqueID == uniqueID);
			return warChar != null;
		}

		public void SetActivateEnemy(string uniqueID, bool isActive)
		{
			if (isActive) {
				WarCharacter warChar = NonActiveEnemy.Find (x => x.UniqueID == uniqueID);
				NonActiveEnemy.Remove (warChar);
				ActiveEnemy.Add (warChar);
			}
			else 
			{
				WarCharacter warChar = ActiveEnemy.Find (x => x.UniqueID == uniqueID);
				ActiveEnemy.Remove (warChar);
				NonActiveEnemy.Add (warChar);
			}
		}

		public bool CheckEnemyStatus(string uniqueID, bool isActive)
		{
			WarCharacter warChar = null;
			if (!isActive)
				warChar = NonActiveEnemy.Find (x => x.UniqueID == uniqueID);
			else
				warChar = ActiveEnemy.Find (x => x.UniqueID == uniqueID);
			return warChar != null;
		}
	}
}