﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;

namespace Legrand.War
{
    public class WarCharacter : MonoBehaviour,ISelectable
    {
        public string ID;
		public double HashID;
		public string UniqueID;
		public double HashUniqueID;
        public Sprite Avatar;
        public string Name;
        public Health Health;
        public int Troops;
        private int _AC;
        public int AC
        {
            get { return _AC; }
            set { _AC = value; if (_AC < 0) _AC = 0; }
        }
        public int MaxAC;
		public int AP;
		public int MaxAP;

        public WarClass WarClass;
        public Dictionary<WarAttribute, object> Attributes;

		public BattlerType FactionType;
        public WarAIType AIType;

		public GridLocalPosition CurrentGrid;
		public WarDirection SeeDirection;
		public bool Controllable;

		public void Setup(string id, string uniqueID, BattlerType faction, bool controllable, WarAIType _aiType)
        {
			ID = id;
			UniqueID = uniqueID;

			HashID = LegrandUtility.GetUniqueSymbol (ID);
			HashUniqueID = LegrandUtility.GetUniqueSymbol (UniqueID);
			FactionType = faction;
            AIType = _aiType;
			Controllable = controllable;
            Attributes = new Dictionary<WarAttribute, object>();

            SetData ();

        }

        private void SetData()
        {
            WarCharacterDataModel model = WarDB._Instance.WarCharacterDataModels.Find(x => LegrandUtility.CompareString(x.ID, this.ID));
            if (model != null)
            {
                Name = model.Name;
                Avatar = LegrandBackend.Instance.GetSpriteData("AvatarWar" + Name);

                WarClass = model.Class;

                MaxAC = model.MaxAC;
                
                AC = 0;
                Attributes[WarAttribute.ACRegen] = 3;
                MaxAP = model.MaxAP;
                AP = 0;

                //Default Value
                Health.ChangeMaximumValue(100f);
                Health.ChangeCurrentValue(100f);
                Troops = 100;
                Attributes = new Dictionary<WarAttribute, object>();
                Attributes[WarAttribute.STR] = 1;
                Attributes[WarAttribute.DEF] = 1;
                Attributes[WarAttribute.AGI] = 1;

                // Karakter secara normal statusnya bergantung pada kelasnya
                WarClassDataModel classModel = WarDB._Instance.WarClassDataModels.Find(x => x.Class == model.Class);
                if(classModel != null)
                {
                    Health.ChangeMaximumValue(classModel.HP);
                    Health.ChangeCurrentValue(classModel.HP);
                    Attributes[WarAttribute.STR] = classModel.STR;
                    Attributes[WarAttribute.DEF] = classModel.DEF;
                    Attributes[WarAttribute.AGI] = classModel.AGI;
                    Attributes[WarAttribute.RangeAttack] = classModel.AttackRange;
                    Attributes[WarAttribute.CanAttackHidden] = classModel.CanAttackHidden;
                    Attributes[WarAttribute.CanMoveHidden] = classModel.CanMoveHidden;

                    Troops = classModel.Troops;
                }

                // Kasus khusus untuk character yang statusnya tidak mengikuti kelas
                AC = model.AC > 0 ? model.AC : 0;
                AP = model.AP > 0 ? model.AP : 0;

                if (model.HP > 0)
                {
                    Health.ChangeMaximumValue(model.HP);
                    Health.ChangeCurrentValue(model.HP);
                }
                if (model.STR > 0) Attributes[WarAttribute.STR] = model.STR;
                if (model.DEF > 0) Attributes[WarAttribute.DEF] = model.DEF;
                if (model.AGI > 0) Attributes[WarAttribute.AGI] = model.AGI;
                if (model.Troops > 0) Troops = model.Troops;
                if (model.ACRegen > 0) Attributes[WarAttribute.ACRegen] = model.ACRegen;
            }
        }

        public RetrieveableInfo RetrieveInfo()
        {
            RetrieveableInfo r;
            r.Character = this;
            r.Grid = null;
            return r;
        }

        public int GetAGI()
        {
            return (int)Attributes[WarAttribute.AGI];
        }

        public int GetSTR()
        {
            return (int)Attributes[WarAttribute.STR];
        }

        public int GetDEF()
        {
            return (int)Attributes[WarAttribute.DEF];
        }

        public int GetAttackRange()
        {
            return (int)Attributes[WarAttribute.RangeAttack];
        }

        public bool IsVisibleToMove(GridConnection connection)
        {
            if (connection.RoadStatus == ConnectionStatus.Destroyed) return false;
            if (connection.RoadStatus == ConnectionStatus.Hidden)
            {
                if ((bool)Attributes[WarAttribute.CanMoveHidden]) return true;
                else return false;
            }
            return true;
        }

        public bool IsVisibleToAttack(GridConnection connection)
        {
            if (connection.RoadStatus == ConnectionStatus.Destroyed) return false;
            if (connection.RoadStatus == ConnectionStatus.Hidden)
            {
                if ((bool)Attributes[WarAttribute.CanAttackHidden]) return true;
                else return false;
            }
            return true;
        }
    }
}
