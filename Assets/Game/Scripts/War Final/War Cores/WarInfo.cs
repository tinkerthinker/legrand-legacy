﻿using UnityEngine;
using System.Collections;

namespace Legrand.War
{
	public class WarInfo : MonoBehaviour {

		public static StateController StateController;
		public static WarObjectives WarObjectives;
		public static WarParticipant WarParticipant;
		public static GridController GridController;
		public static MovementController MovementController;
		public static CharacterPanelController CharacterPanelController;
        public static TopPanel TopPanel;
        public static WarTurnController TurnController;

        public static bool WarDataComplete()
		{
			return TurnController != null && TopPanel != null && CharacterPanelController != null && StateController != null && WarObjectives != null && WarParticipant != null && GridController != null && MovementController != null;
		}
	}
}