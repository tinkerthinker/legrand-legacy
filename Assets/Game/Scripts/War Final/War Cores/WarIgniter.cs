﻿using UnityEngine;
using System.Collections;

namespace Legrand.War
{
	public class WarIgniter : MonoBehaviour {

		void Start()
		{
			InitiateWar ();
		}

		public void InitiateWar()
		{
			StartCoroutine (WaitUntilComponentReady());
		}

		private IEnumerator WaitUntilComponentReady()
		{
			yield return new WaitUntil (() => WarInfo.WarDataComplete ());
			StartWar ();
		}

		private void StartWar()
		{
			WarInfo.WarParticipant.PrepareParticipant ();
			WarInfo.WarParticipant.DeployStarterTroop ();
			WarInfo.StateController.StartController ();
		}
	}
}
