﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Legrand.War
{
    public class WarTurnController : MonoBehaviour {
        public CardsController CardController;
        public WarCharacter PlayedCharacter;
        private int PlayedCharacterCost;

        public List<TurnData> ThisTurnDatas;
        public List<TurnData> NextTurnDatas;

        public void Initiate()
        {
            ThisTurnDatas = new List<TurnData>();
            NextTurnDatas = new List<TurnData>();

            List<WarCharacter> playingCharacter = new List<WarCharacter>(WarInfo.WarParticipant.ParticipantCondition.ActiveAlly);
            playingCharacter.AddRange(WarInfo.WarParticipant.ParticipantCondition.ActiveEnemy);
            ThisTurnDatas = FirstCalculate(playingCharacter);

            ChoosePlayingCharacter();
            CardController.InitiateCard(ThisTurnDatas, NextTurnDatas);
        }

        void OnEnable()
        {
            WarInfo.TurnController = this;

            if (EventManager.Instance)
                EventManager.Instance.AddListener<WarCharacterDied>(CharacterDiedHandler);
        }

        void OnDisable()
        {
            WarInfo.TurnController = null;

            if (EventManager.Instance)
                EventManager.Instance.RemoveListener<WarCharacterDied>(CharacterDiedHandler);
        }

        public void EndCharacterTurn(string uniqueCharacterID, int cost)
        {
            TurnData data = ThisTurnDatas.Find(x=> x.Character.UniqueID == uniqueCharacterID);
            if(data.Character != null)
            {
                ThisTurnDatas.RemoveAll(x=> x.Character.UniqueID == uniqueCharacterID);
                data.LastCost = cost;
                NextTurnDatas.Add(data);
                NextTurnDatas = Recalculate(NextTurnDatas);
                CardController.ReconstructCard(ThisTurnDatas, NextTurnDatas);
                ChoosePlayingCharacter();
            }
            else
            {
                Debug.LogError("End Character Data Error : Can't find character in current turn");
            }
        }

        public void EndCharacterTurn()
        {
            EndCharacterTurn(PlayedCharacter.UniqueID, PlayedCharacterCost);
        }

        void CharacterDiedHandler(WarCharacterDied e)
        {
            if(ThisTurnDatas.Remove(ThisTurnDatas.Find(x=>x.Character.UniqueID == e.UniqueID)))
                Recalculate(ThisTurnDatas);
            else if(NextTurnDatas.Remove(NextTurnDatas.Find(x => x.Character.UniqueID == e.UniqueID)))
                Recalculate(NextTurnDatas);
            CardController.DestroyCard(e.UniqueID);
            CardController.ReconstructCard(ThisTurnDatas, NextTurnDatas);
            if (e.UniqueID == PlayedCharacter.UniqueID) ChoosePlayingCharacter();
        }

        List<TurnData> FirstCalculate(List<WarCharacter> activeCharacter)
        {
            List < TurnData > temp = new List<TurnData>();
            for(int i = 0; i<activeCharacter.Count; i++)
            {
                TurnData t = new TurnData();
                t.Character = activeCharacter[i];
                t.LastCost = -1;
                temp.Add(t);
            }
            temp.Sort(CompareByCost);
            return temp;
        }

        public List<TurnData> Recalculate(List<TurnData> turnDatas)
        {
            turnDatas.Sort(CompareByCost);
            return turnDatas;
        }

        private void ChoosePlayingCharacter()
        {
            if(ThisTurnDatas.Count <= 0)
            {
                ThisTurnDatas = new List<TurnData>(NextTurnDatas);
                NextTurnDatas = new List<TurnData>();
            }
            PlayedCharacter = ThisTurnDatas[0].Character;
            PlayedCharacter.AC += (int)PlayedCharacter.Attributes[core.WarAttribute.ACRegen];
            PlayedCharacterCost = 0;
        }

        public void CurrentCharacterSpendAC(int cost)
        {
            PlayedCharacter.AC -= cost;
            PlayedCharacterCost += cost;
        }

        private int CompareByCost(TurnData t1, TurnData t2)
        {
            if (t1.Character.HashUniqueID == t2.Character.HashUniqueID)
                return 0;

            if (t1.LastCost > t2.LastCost) return 1;
            else if(t1.LastCost < t2.LastCost) return -1;
            else
            {
                WarCharacter c1 = t1.Character;
                WarCharacter c2 = t2.Character;

                if (c1.GetAGI() > c2.GetAGI()) return -1;
                else if (c1.GetAGI() < c2.GetAGI()) return 1;
                else
                {
                    if (t1.Character.FactionType == BattlerType.Player && t2.Character.FactionType == BattlerType.Enemy) return -1;
                    else if (t1.Character.FactionType == BattlerType.Enemy && t2.Character.FactionType == BattlerType.Player) return 1;
                    else
                    {

                        if (c1.HashUniqueID > c2.HashUniqueID) return 1;
                        else return -1;
                    }

                    
                }
            }
        }
        
    }

    [System.Serializable]
    public struct TurnData
    {
        public WarCharacter Character;
        public int LastCost;
    }
}