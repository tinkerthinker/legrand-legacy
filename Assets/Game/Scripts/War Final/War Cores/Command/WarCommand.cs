﻿using UnityEngine;
using System.Collections;

namespace Legrand.War
{
    public enum WarCommand
    {
        Move,
        Attack,
        Magic,
        Defend,
        EndTurn
    }
}