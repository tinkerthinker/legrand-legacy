﻿using UnityEngine;
using System.Collections;

namespace Legrand.War
{
    public struct PassedGrid
    {
        public GridNeighbor PassedConnection;
        public int MovementCost;
        public bool IsValid;
    }
}