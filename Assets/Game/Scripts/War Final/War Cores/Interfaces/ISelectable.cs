﻿using UnityEngine;
using System.Collections;

namespace Legrand.War
{
    public interface ISelectable
    {
        RetrieveableInfo RetrieveInfo();
    }

    public struct RetrieveableInfo
    {
        public WarCharacter Character;
        public Grid Grid;
    }

    public enum SelectableType
    {
        Player,
        Grid
    }
}