﻿using UnityEngine;
using System.Collections;

namespace Legrand.War
{
    public enum WarClass
    {
        Mercenary,
        Bandit,
        Pikeman,
        Cavalry,
        Archer,
        Wyvern
    }
}
