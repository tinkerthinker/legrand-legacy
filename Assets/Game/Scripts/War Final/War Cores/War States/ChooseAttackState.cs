﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TeamUtility.IO;

namespace Legrand.War
{
    public class ChooseAttackState : WarState
    {
        private bool _AcceptInput;
        private WarCharacter _currentCharacter;
        private Grid _currentGrid;

        private List<Grid> PassedGrids;

        public override void StartState()
        {
            EventManager.Instance.TriggerEvent(new SetPointerActive(false));
            EventManager.Instance.TriggerEvent(new SetInfoActive(false, false));
            WarInfo.TopPanel.Activate();

            _currentCharacter = WarInfo.TurnController.PlayedCharacter;
            _currentGrid = _currentCharacter.CurrentGrid.ParentGrid;

            CameraToGridFocus(_currentCharacter.transform);

            PassedGrids = new List<Grid>();

            if (!_currentCharacter.Controllable)
            {

            }

            base.StartState();
        }

        public override void UpdateState()
        {
            base.UpdateState();

            if (!_currentCharacter.Controllable || !StateStarted) return;

            float horizontal = LegrandUtility.GetAxisRawHorizontal();
            float vertical = LegrandUtility.GetAxisRawVertical();

            if (_AcceptInput == false && horizontal == 0f && vertical == 0f)
                _AcceptInput = true;

            if (!_AcceptInput) return;

            if (InputManager.GetButtonDown("Cancel"))
            {
                ChangeToCommand();
                return;
            }

            if (InputManager.GetButtonDown("Confirm"))
            {
                if (_currentGrid.ID != _currentCharacter.CurrentGrid.ParentGrid.ID && _currentGrid.TryToAttack(_currentCharacter))
                {
                    ApplyAttackAC();
                    ChangeToBattle();
                }
                else
                {
                    WwiseManager.Instance.PlaySFX(WwiseManager.UIFX.Cancel);
                }
                return;
            }

            if (horizontal == 0f && vertical == 0f) return;

            WarDirection moveDirection = WarDirection.None;
            if (horizontal != 0f && vertical != 0f)
            {
                if (horizontal > 0 && vertical > 0) moveDirection = WarDirection.NorthEast;
                if (horizontal < 0 && vertical > 0) moveDirection = WarDirection.NorthWest;
                if (horizontal > 0 && vertical < 0) moveDirection = WarDirection.SouthEast;
                if (horizontal < 0 && vertical < 0) moveDirection = WarDirection.SouthWest;
                _AcceptInput = false;
            }
            else if (horizontal != 0f)
            {
                if (horizontal > 0) moveDirection = WarDirection.East;
                else moveDirection = WarDirection.West;
                _AcceptInput = false;
            }
            else if (vertical != 0f)
            {
                if (vertical > 0) moveDirection = WarDirection.North;
                else moveDirection = WarDirection.South;
                _AcceptInput = false;
            }

            if (moveDirection != WarDirection.None)
                SelectGrid(_currentGrid, moveDirection);

        }

        private void SelectGrid(Grid grid, WarDirection inputDirection)
        {
            GridNeighbor neighbour = grid.GetCharacterAttackNeighbour(_currentCharacter, inputDirection, DirectionUtility.GetDirectionNeighbour(inputDirection));

            if (neighbour.NeighborGrid != null)
            {
                if (neighbour.NeighborGrid.ID != _currentCharacter.CurrentGrid.ParentGrid.ID && (int)_currentCharacter.Attributes[core.WarAttribute.RangeAttack] <= PassedGrids.Count)
                    return;
                
                MoveToGrid(neighbour);
            }
        }

        private void MoveToGrid(GridNeighbor neighbourGrid)
        {
            _currentGrid.MarkForAttack(false);

            _currentGrid = neighbourGrid.NeighborGrid;
            CameraToGridFocus(_currentGrid.transform);

            if (_currentGrid.ID == _currentCharacter.CurrentGrid.ParentGrid.ID)
            {
                ReturnAllGridToUnmarked();
                return;
            }

            bool foundSameGrid = false;
            for (int i = 0; !foundSameGrid && i < PassedGrids.Count; i++)
            {
                if (PassedGrids[i].ID == _currentGrid.ID)
                {
                    foundSameGrid = true;
                    i++;
                    while (i < PassedGrids.Count)
                    {
                        PassedGrids.RemoveAt(i);
                    }
                }
            }

            if (!foundSameGrid)
            {
                _currentGrid.MarkForAttack(true);
                PassedGrids.Add(_currentGrid);
            }
        }

        private void ChangeToCommand()
        {
            EndState();
            ReturnAllGridToUnmarked();
            WarInfo.StateController.ChangeState(WarStateEnum.Command);
        }

        private void ChangeToBattle()
        {
            EndState();
            List<WarCharacter> attackingCharacter = _currentCharacter.CurrentGrid.ParentGrid.GetCharacterInsideGrid();
            attackingCharacter.RemoveAll(x => x.GetAttackRange() < PassedGrids.Count);
            List<WarCharacter> attackedCharacter = _currentGrid.GetCharacterInsideGrid();
            WarInfo.StateController.ChangeState(WarStateEnum.Battle, false);
            BattleState bState = WarInfo.StateController.CurrentState.State as BattleState;
            bState.AttackingCharacter = attackingCharacter;
            bState.AttackedCharacter = attackedCharacter;
            ReturnAllGridToUnmarked();
            bState.StartState();
        }

        private void ApplyAttackAC()
        {
            WarInfo.TurnController.CurrentCharacterSpendAC(WarDB._Instance.WarCommandCostModels.Find(x => x.CommandType == WarCommand.Attack).Cost);
        }

        private void ReturnAllGridToUnmarked()
        {
            foreach (Grid g in PassedGrids)
                g.MarkForAttack(false);
            PassedGrids.Clear();
        }

        private void CameraToGridFocus(Transform grid)
        {
            EventManager.Instance.TriggerEvent(new ChangeCameraFocus(grid));
        }
    }
}