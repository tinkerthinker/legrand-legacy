﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;
using DG.Tweening;

namespace Legrand.War
{
    public class BattleState : WarState
    {
        private bool _AcceptInput;
        [HideInInspector]
        public List<WarCharacter> AttackingCharacter;
        [HideInInspector]
        public List<WarCharacter> AttackedCharacter;

        public Image FadeImage;
        public Color FadeColor;
        public float FadeTime;


        public override void StartState()
        {
            WarInfo.TopPanel.Activate();
            EventManager.Instance.TriggerEvent(new SetInfoActive(false, false, false));
            foreach(WarCharacter c in AttackingCharacter.Concat(AttackedCharacter))
            {
                if (c.FactionType == BattlerType.Player)
                    WarInfo.CharacterPanelController.UpdateAllyPanel(c);
                else
                    WarInfo.CharacterPanelController.UpdateEnemyPanel(c);
            }

            WarInfo.CharacterPanelController.ShowAllyPanel();
            WarInfo.CharacterPanelController.ShowEnemyPanel();

            FadeImage.DOColor(FadeColor, FadeTime);

            StartCoroutine(BattleAnimation());
            
            base.StartState();
        }

        IEnumerator BattleAnimation()
        {
            // Penyerang menyerang pertama
            foreach (WarCharacter attacker in AttackingCharacter)
            {
                foreach (WarCharacter attacked in AttackedCharacter)
                {
                    attacked.Health.Decrease(attacker.GetSTR());
                }
                yield return new WaitForSeconds(1f);
            }

            // Lawan Counter
            foreach (WarCharacter attacked in AttackedCharacter)
            {
                foreach (WarCharacter attacker in AttackingCharacter)
                {
                    attacker.Health.Decrease(attacked.GetSTR());
                }
                yield return new WaitForSeconds(1f);
            }
            EndBattle();
        }

        public void EndBattle()
        {
            foreach (WarCharacter c in AttackingCharacter.Concat(AttackedCharacter))
            {
                if (c.UniqueID != WarInfo.TurnController.PlayedCharacter.UniqueID)
                {
                    if (c.FactionType == BattlerType.Player)
                        WarInfo.CharacterPanelController.HideAllyPanel(c);
                    else
                        WarInfo.CharacterPanelController.HideEnemyPanel(c);
                }
            }

            FadeImage.DOColor(Color.clear, FadeTime);
            CheckCharacterDeadState();
        }

        private void CheckCharacterDeadState()
        {
            EndState();
            WarInfo.StateController.ChangeState(WarStateEnum.CheckChracterDead, false);
            CheckDeadState currentState = (CheckDeadState)WarInfo.StateController.CurrentState.State;
            currentState.AttackingCharacter = AttackingCharacter;
            currentState.AttackedCharacter = AttackedCharacter;
            currentState.StateEnd += ChangeToCommand;
            WarInfo.StateController.StartState();
        }

        private void ChangeToCommand()
        {
            WarInfo.StateController.ChangeState(WarStateEnum.Command);
        }
    }
}