﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TeamUtility.IO;

namespace Legrand.War
{
	public class DeploymentState : WarState {
		private bool _AcceptInput;
		private Transform LastFocusedGrid;
		private GridWithNavigation LastFocusedGridNavigation;

		private List<WarCharacter> _DeploymentCandidates;
		int indexSelectedCharacer = 0;

		void OnEnable()
		{
			indexSelectedCharacer = 0;	
		}

		public override void StartState () 
		{
			_AcceptInput = true;
			EventManager.Instance.TriggerEvent (new SetPointerActive(true, false));
            EventManager.Instance.TriggerEvent(new SetInfoActive(false, false, false));
            WarInfo.TopPanel.Activate();
            if (LastFocusedGrid == null) 
			{
				ChangeGridFocus (WarInfo.WarParticipant.TroopStarter.AllyFillableGrid [0].GridID);
			} 
			else 
			{
				CameraToGridFocus ();
			}

			_DeploymentCandidates = new List<WarCharacter> ();
			_DeploymentCandidates.AddRange (WarInfo.WarParticipant.ParticipantCondition.ActiveAlly);
			_DeploymentCandidates.AddRange (WarInfo.WarParticipant.ParticipantCondition.NonActiveAlly);
			_DeploymentCandidates = _DeploymentCandidates.FindAll (x => x.Controllable == true);
            _DeploymentCandidates.Sort(delegate (WarCharacter a, WarCharacter b) {
                return (a.ID.CompareTo(b.ID));
            });

            UpdatePanel();

			base.StartState ();
		}

		public override void EndState () 
		{
			base.EndState ();
		}

		public override void UpdateState () 
		{
			base.UpdateState ();

			if (InputManager.GetButtonDown ("Confirm")) 
			{
				WarInfo.WarParticipant.DeployAllyTroop ( _DeploymentCandidates [indexSelectedCharacer], LastFocusedGridNavigation.GridID);
                WaitEndOfFrameToEnd(ChangeToRotation);
            }

			if (InputManager.GetButtonDown ("Cancel")) 
			{
				WaitEndOfFrameToEnd (ChangeToPreparation);
			}

            if (InputManager.GetButtonDown("ChangeTabL"))
            {
                indexSelectedCharacer--;
                if (indexSelectedCharacer < 0)
                    indexSelectedCharacer = _DeploymentCandidates.Count > 0 ? _DeploymentCandidates.Count - 1 : 0;
                UpdatePanel();
            }

            if (InputManager.GetButtonDown("ChangeTabR"))
            {
                indexSelectedCharacer++;
                if (indexSelectedCharacer >= _DeploymentCandidates.Count)
                    indexSelectedCharacer = 0;
                UpdatePanel();
            }

            if (InputManager.GetKeyDown (KeyCode.LeftAlt)) 
			{
				WaitEndOfFrameToEnd (ChangeToRoaming);
			}

			float horizontal = LegrandUtility.GetAxisRawHorizontal();
			float vertical = LegrandUtility.GetAxisRawVertical();

			if (_AcceptInput == false && horizontal == 0f && vertical == 0f) 
				_AcceptInput = true;

			if(!_AcceptInput) return;

			if (horizontal != 0f) 
			{
				_AcceptInput = false;
				if (horizontal > 0 && LastFocusedGridNavigation.RightInputGridID != "")
					ChangeGridFocus (LastFocusedGridNavigation.RightInputGridID);

				if (horizontal < 0 && LastFocusedGridNavigation.LeftInputGridID != "")
					ChangeGridFocus (LastFocusedGridNavigation.LeftInputGridID);
			}

			if (vertical != 0f) 
			{
				_AcceptInput = false;
				if (vertical > 0 && LastFocusedGridNavigation.UpInputGridID != "")
					ChangeGridFocus (LastFocusedGridNavigation.UpInputGridID);

				if (vertical < 0 && LastFocusedGridNavigation.DownInputGridID != "")
					ChangeGridFocus (LastFocusedGridNavigation.DownInputGridID);
			}
		}

		public void ChangeToRoaming()
        {
            WarInfo.CharacterPanelController.HideAllyPanel();
            EndState ();
			WarInfo.StateController.ChangeState (WarStateEnum.Roaming, false);
			WarInfo.StateController.CurrentState.State.StateEnd += ChangeToDeployment;
			WarInfo.StateController.StartState ();
		}

        public void ChangeToRotation()
        {
            EndState();
            WarInfo.StateController.ChangeState(WarStateEnum.Rotation, false);
            RotationState rotationState = (RotationState) WarInfo.StateController.CurrentState.State;
            rotationState.FocusedCharacter = _DeploymentCandidates[indexSelectedCharacer];
            WarInfo.StateController.CurrentState.State.StateEnd += ChangeToDeployment;
            WarInfo.StateController.StartState();
        }

        public void ChangeToDeployment()
		{
			WarInfo.StateController.ChangeState (WarStateEnum.Deployment);
		}

		public void ChangeToPreparation()
        {
            WarInfo.CharacterPanelController.HideAllyPanel();
            EndState ();
			WarInfo.StateController.ChangeState (WarStateEnum.Preparation);
		}

		private void ChangeGridFocus(string gridID, bool autoCameraFocus = true)
		{
			GridWithNavigation newNavigationGrid =  System.Array.Find (WarInfo.WarParticipant.TroopStarter.AllyFillableGrid, x=>gridID == x.GridID);
			LastFocusedGrid = WarInfo.GridController.GetGrid (newNavigationGrid.GridID).transform;
			LastFocusedGridNavigation = newNavigationGrid;
			if (autoCameraFocus)
				CameraToGridFocus ();
		}

        private void UpdatePanel()
        {
            if (_DeploymentCandidates.Count > 0)
            {
                WarInfo.CharacterPanelController.UpdateAllyPanel(0, _DeploymentCandidates[indexSelectedCharacer], true);
                WarInfo.CharacterPanelController.ShowAllyPanel(0);
            }
        }

		private void CameraToGridFocus()
		{
			EventManager.Instance.TriggerEvent (new ChangeCameraFocus (LastFocusedGrid));
		}
	}
}