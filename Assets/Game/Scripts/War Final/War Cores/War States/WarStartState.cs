﻿using UnityEngine;
using System.Collections;

namespace Legrand.War
{
    public class WarStartState : WarState
    {
        public Animator WarStartAnimator;

        public override void StartState()
        {
            WarInfo.TurnController.Initiate();
            StartCoroutine(WaitAnimation());
            WarStartAnimator.Play("War Start Play", 0);
            base.StartState();
        }

        IEnumerator WaitAnimation()
        {
            yield return new WaitForSeconds(2f);
           
            EndState();
            WarInfo.StateController.ChangeState(WarStateEnum.Command);
        }

        public override void EndState()
        {
            base.EndState();
        }
    }
}