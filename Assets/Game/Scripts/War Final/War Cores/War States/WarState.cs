﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TeamUtility.IO;

public class WarState : MonoBehaviour {
	public delegate void WarStateTrigger();


	protected delegate void UpdateDelegate();

	public WarStateTrigger StateStart;
	public WarStateTrigger StateEnd;

	protected bool StateStarted;

	public void CleanTrigger()
	{
		StateStarted = false;
		StateStart = null;
		StateEnd = null;
	}

	public virtual void StartState()
	{
        StartCoroutine(WaitOneFrameToInitialize());
		if(StateStart != null)
			StateStart ();
	}

    private IEnumerator WaitOneFrameToInitialize()
    {
        yield return new WaitForEndOfFrame();
        StateStarted = true;
    }

    public virtual void UpdateState () 
	{
		if (!StateStarted)
			return;
	}

	public virtual void EndState () 
	{
		if(StateEnd != null)
			StateEnd ();
		CleanTrigger ();
	}

	protected virtual void WaitEndOfFrameToEnd(UpdateDelegate functionToCall)
	{
		StartCoroutine (WaitToEnd(functionToCall));
	}

	private IEnumerator WaitToEnd(UpdateDelegate functionToCall)
	{
		yield return new WaitForEndOfFrame ();
		functionToCall ();
	}
}

public enum WarStateEnum
{
	Opening,
	Objective,
	Preparation,
	Deployment,
	Roaming,
    Rotation,
    WarStart,
    Command,
    ChooseAttack,
    ChooseMove,
    CommandMoveAnimation,
    Battle,
    CheckChracterDead
}
