﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;
using TeamUtility.IO;

namespace Legrand.War
{
	public class PreparationState : WarState {
		public PreparationPanel PreparationPanel;

		public override void StartState () 
		{
			EventManager.Instance.TriggerEvent (new SetPointerActive(false));
            EventManager.Instance.TriggerEvent(new SetInfoActive(false, false));
            WarInfo.TopPanel.Diactivate();
            PreparationPanel.Activate ();
			base.StartState ();
		}

		public override void EndState () 
		{
			PreparationPanel.Diactivate ();
			base.EndState ();
		}

		public void MoveToDeployment()
		{
			EndState ();
			WwiseManager.Instance.PlaySFX (WwiseManager.UIFX.Confirm);
			WaitEndOfFrameToEnd (StartDeploymentState);
		}

		private void StartDeploymentState()
		{
			WarInfo.StateController.ChangeState (WarStateEnum.Deployment);
		}

		public void MoveToObjective()
		{
			EndState ();
			WwiseManager.Instance.PlaySFX (WwiseManager.UIFX.Confirm);
			WaitEndOfFrameToEnd (StartObjectiveState);
		}

		private void StartObjectiveState()
		{
			WarInfo.StateController.ChangeState (WarStateEnum.Objective, false);
			WarInfo.StateController.CurrentState.State.StateEnd += MoveToPreparation;
			WarInfo.StateController.StartState ();
		}

		public void MoveToWarStart()
		{
            EndState();
            WwiseManager.Instance.PlaySFX(WwiseManager.UIFX.Confirm);
            WaitEndOfFrameToEnd(StartWar);
        }

        private void StartWar()
        {
            WarInfo.StateController.ChangeState(WarStateEnum.WarStart);
        }

        public void MoveToPreparation()
		{
			WarInfo.StateController.ChangeState (WarStateEnum.Preparation);
		}
	}
}