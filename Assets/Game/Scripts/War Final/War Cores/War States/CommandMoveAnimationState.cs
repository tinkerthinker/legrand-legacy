﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TeamUtility.IO;

namespace Legrand.War
{
	public class CommandMoveAnimationState : WarState {
        private WarCharacter _currentCharacter;
        [HideInInspector]
        public Grid TargetGrid;

        public override void StartState () 
		{
			EventManager.Instance.TriggerEvent (new SetPointerActive(false));
            EventManager.Instance.TriggerEvent(new SetInfoActive(false, true, false));
            WarInfo.CharacterPanelController.UpdateAllyPanel(0, WarInfo.TurnController.PlayedCharacter, false);
            WarInfo.CharacterPanelController.ShowAllyPanel(0);

            WarInfo.TopPanel.Activate();

            _currentCharacter = WarInfo.TurnController.PlayedCharacter;
            CameraToGridFocus(_currentCharacter.transform);
            WarInfo.MovementController.MoveDoneEvent += MoveDone;
            WarInfo.MovementController.MoveCharacterPosition(_currentCharacter, TargetGrid);

            base.StartState ();
		}

        private void MoveDone()
        {
            EndState();
            WarInfo.StateController.ChangeState(WarStateEnum.Rotation, false);
            WarInfo.StateController.CurrentState.State.StateEnd += ChangeToCommand;
            (WarInfo.StateController.CurrentState.State as RotationState).FocusedCharacter = _currentCharacter;
            WarInfo.StateController.StartState();
        }

        private void ChangeToCommand()
        {
            WarInfo.StateController.CurrentState.State.StateEnd -= ChangeToCommand;
            WarInfo.StateController.ChangeState(WarStateEnum.Command);
        }

        public override void EndState () 
		{
			base.EndState ();
		}

        private void CameraToGridFocus(Transform grid)
		{
			EventManager.Instance.TriggerEvent (new ChangeCameraFocus (grid));
		}
	}
}