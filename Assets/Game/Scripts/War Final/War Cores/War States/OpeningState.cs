﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;
using TeamUtility.IO;

namespace Legrand.War
{
	public class OpeningState : WarState {

		public override void StartState () 
		{
			EventManager.Instance.TriggerEvent (new SetPointerActive(false));
            EventManager.Instance.TriggerEvent( new SetInfoActive(false, false));
            WarInfo.TopPanel.Diactivate();
			base.StartState ();
			MoveToObjective ();
		}

		public override void EndState () 
		{
			base.EndState ();
		}

		public void MoveToDialog()
		{
			EndState ();
		}

		public void MoveToTutorial()
		{
			EndState ();
		}

		public void MoveToObjective()
		{
			EndState ();
			WarInfo.StateController.ChangeState (WarStateEnum.Objective ,false);
			WarInfo.StateController.CurrentState.State.StateEnd += MoveToPreparation;
			WarInfo.StateController.StartState ();
		}

		public void MoveToPreparation()
		{
			WarInfo.StateController.ChangeState (WarStateEnum.Preparation);
		}
	}
}