﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;
using TeamUtility.IO;

namespace Legrand.War
{
	public class RoamingState : WarState {

		public override void StartState () 
		{
			EventManager.Instance.TriggerEvent (new SetPointerActive(true));
            EventManager.Instance.TriggerEvent(new SetInfoActive(true, true));
            WarInfo.TopPanel.Activate();
            base.StartState ();
		}

		public override void EndState () 
		{
			base.EndState ();
		}

		public override void UpdateState () 
		{
			base.UpdateState ();
			if (InputManager.GetButtonDown ("Cancel")) 
			{
				WwiseManager.Instance.PlaySFX (WwiseManager.UIFX.Cancel);
				WaitEndOfFrameToEnd (EndState);
			}
		}
	}
}