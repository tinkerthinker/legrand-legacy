﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;
using DG.Tweening;

namespace Legrand.War
{
    public class CheckDeadState : WarState
    {
        [HideInInspector]
        public List<WarCharacter> AttackingCharacter;
        [HideInInspector]
        public List<WarCharacter> AttackedCharacter;

        private List<WarCharacter> UncheckedCharacter; 

        public override void StartState()
        {
            UncheckedCharacter = AttackingCharacter.Concat(AttackedCharacter).ToList();
            base.StartState();
            IterateUncheckedCharacter();
        }

        private void IterateUncheckedCharacter()
        {
            if (UncheckedCharacter.Count > 0)
            {
                WarCharacter checkedCharacter = UncheckedCharacter[0];
                UncheckedCharacter.RemoveAt(0);
                CheckDead(checkedCharacter);
            }
            else
                EndState();
        }

        private void CheckDead(WarCharacter checkedCharacter)
        {
            if(checkedCharacter.Health.Value <= 0f)
            {
                WarInfo.CharacterPanelController.HideAllyPanel();
                WarInfo.CharacterPanelController.HideEnemyPanel();
                EventManager.Instance.TriggerEvent(new WarCharacterDied(checkedCharacter.UniqueID));
                EventManager.Instance.TriggerEvent(new ChangeCameraFocus(checkedCharacter.transform));
                Renderer[] rends = checkedCharacter.GetComponentsInChildren<Renderer>();
                for (int i = 0; i < rends.Length; i++)
                {
                    if (i == 0)
                        rends[i].material.DOFloat(1f, "_SliceAmount", 1.5f).OnComplete(() => CheckEventAfterDead(checkedCharacter));
                    else
                        rends[i].material.DOFloat(1f, "_SliceAmount", 1.5f);
                }
            }
            else
                IterateUncheckedCharacter();
        }

        private void CheckEventAfterDead(WarCharacter checkedCharacter)
        {
            IterateUncheckedCharacter();
        }
    }
}