﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TeamUtility.IO;

namespace Legrand.War
{
	public class MoveState : WarState {
		private bool _AcceptInput;
        private WarCharacter _currentCharacter;
        private Grid _currentGrid;

        private List<PassedGrid> PassedGrids;
        private int _movementCost;

		public override void StartState () 
		{
			_AcceptInput = true;
			EventManager.Instance.TriggerEvent (new SetPointerActive(false));
            EventManager.Instance.TriggerEvent(new SetInfoActive(false, true, false));
            
            WarInfo.TopPanel.Activate();

            _currentCharacter = WarInfo.TurnController.PlayedCharacter;
            _currentGrid = _currentCharacter.CurrentGrid.ParentGrid;
            WarInfo.CharacterPanelController.UpdateAllyPanel(0,_currentCharacter);
            WarInfo.CharacterPanelController.ShowAllyPanel(0);

            PassedGrids = new List<PassedGrid>();
            _movementCost = 0;

            CameraToGridFocus(_currentCharacter.transform);

            if (!_currentCharacter.Controllable)
            {

            }

            base.StartState ();
		}

		public override void EndState () 
		{
            ReturnAllToNormal();
            base.EndState ();
		}

		public override void UpdateState () 
		{
			base.UpdateState ();

            if (!_currentCharacter.Controllable || !StateStarted) return;

			float horizontal = LegrandUtility.GetAxisRawHorizontal();
			float vertical = LegrandUtility.GetAxisRawVertical();

			if (_AcceptInput == false && horizontal == 0f && vertical == 0f) 
				_AcceptInput = true;
            
            if (!_AcceptInput) return;

            if(InputManager.GetButtonDown("Cancel"))
            {
                ChangeToCommand();
                return;
            }

            if (InputManager.GetButtonDown("Confirm"))
            {
                if (IsMoveValid())
                {
                    _currentGrid = GetLastValidGrid().PassedConnection.NeighborGrid;
                    ApplyACMove();

                    EndState();
                    WarInfo.StateController.ChangeState(WarStateEnum.CommandMoveAnimation, false);
                    CommandMoveAnimationState nextState = WarInfo.StateController.CurrentState.State as CommandMoveAnimationState;
                    nextState.TargetGrid = _currentGrid;
                    WarInfo.StateController.StartState();    
                }
                else
                    WwiseManager.Instance.PlaySFX(WwiseManager.UIFX.Cancel);
                return;
            }

            if (horizontal == 0f && vertical == 0f) return;

            WarDirection moveDirection = WarDirection.None;
            if (horizontal != 0f && vertical != 0f)
            {
                if (horizontal > 0 && vertical > 0) moveDirection = WarDirection.NorthEast;
                if (horizontal < 0 && vertical > 0) moveDirection = WarDirection.NorthWest;
                if (horizontal > 0 && vertical < 0) moveDirection = WarDirection.SouthEast;
                if (horizontal < 0 && vertical < 0) moveDirection = WarDirection.SouthWest;
                _AcceptInput = false;
            }
            else if (horizontal != 0f) 
			{
                if (horizontal > 0) moveDirection = WarDirection.East;
                else moveDirection = WarDirection.West;
                _AcceptInput = false;
			}
            else if (vertical != 0f) 
			{
                if (vertical > 0) moveDirection = WarDirection.North;
                else moveDirection = WarDirection.South;
                _AcceptInput = false;
			}

            if(moveDirection != WarDirection.None)
               SelectGrid(_currentGrid, moveDirection);

        }

        private void ChangeToCommand()
        {
            EndState();
            WarInfo.StateController.ChangeState(WarStateEnum.Command);
        }

        private void MoveToGrid(GridNeighbor neighbourGrid)
        {
            _currentGrid = neighbourGrid.NeighborGrid;
            CameraToGridFocus(neighbourGrid.NeighborGrid.transform);

            if (neighbourGrid.NeighborGrid.ID == _currentCharacter.CurrentGrid.ParentGrid.ID)
            {
                ReturnAllToNormal();
                return;
            }

            bool foundSameGrid = false;
            for(int i=0; !foundSameGrid && i < PassedGrids.Count;i++)
            {
                if (PassedGrids[i].PassedConnection.NeighborGrid.ID == neighbourGrid.NeighborGrid.ID)
                {
                    foundSameGrid = true;
                    i++;
                    while(i<PassedGrids.Count)
                    {
                        PassedGrids[i].PassedConnection.Line.SetToNormal();
                        PassedGrids.RemoveAt(i);
                    }
                }
            }

            if (!foundSameGrid)
            {
                PassedGrid p;
                p.PassedConnection = neighbourGrid;
                p.MovementCost = 1;
                _movementCost += p.MovementCost;
                if (_currentCharacter.AC < _movementCost || neighbourGrid.NeighborGrid.SeachFillable(_currentCharacter) == null)
                {
                    p.IsValid = false;
                    neighbourGrid.Line.SetToRed();
                }
                else
                {
                    p.IsValid = true;
                    neighbourGrid.Line.SetToGreen();
                }
                PassedGrids.Add(p);
            }
            else
            {
                _movementCost = 0;
                for (int i = 0; i < PassedGrids.Count; i++)
                    _movementCost += 1;
            }
        }

        private void SelectGrid(Grid grid, WarDirection inputDirection)
        {
            GridNeighbor neighbour = grid.GetCharacterMovementNeighbour(_currentCharacter, inputDirection,DirectionUtility.GetDirectionNeighbour(inputDirection));
            if (neighbour.NeighborGrid != null) MoveToGrid(neighbour);
        }

        private void ReturnAllToNormal()
        {
            foreach(PassedGrid passedGrid in PassedGrids)
                passedGrid.PassedConnection.Line.SetToNormal();
            _movementCost = 0;  
            PassedGrids.Clear();
        }

        private bool IsMoveValid()
        {
            if (PassedGrids == null || PassedGrids.Count == 0 || !PassedGrids.Exists(x=>x.IsValid == true)) return false;

            return true;
        }

        private PassedGrid GetLastValidGrid()
        {
            return PassedGrids.FindLast(x => x.IsValid);
        }

        private void ApplyACMove()
        {
            for (int i = 0; i < PassedGrids.Count && PassedGrids[i].IsValid; i++)
            {
                WarInfo.TurnController.CurrentCharacterSpendAC(PassedGrids[i].MovementCost);
            }
        }

        private void CameraToGridFocus(Transform grid)
		{
			EventManager.Instance.TriggerEvent (new ChangeCameraFocus (grid));
		}
	}
}