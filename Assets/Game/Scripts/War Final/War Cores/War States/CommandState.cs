﻿using UnityEngine;
using System.Collections;

namespace Legrand.War
{
    public class CommandState : WarState
    {

        public WarCommandController WarCommandController;
        private AIBrain _aiDecisionMaker;

        void Awake()
        {
            _aiDecisionMaker = new AIBrain();
        }

        public override void StartState()
        {
            EventManager.Instance.TriggerEvent(new SetPointerActive(false, false));
            EventManager.Instance.TriggerEvent(new SetInfoActive(false, true, false));
            

            WarInfo.TopPanel.Activate();
            EventManager.Instance.TriggerEvent(new ChangeCameraFocus(WarInfo.TurnController.PlayedCharacter.transform));

            if (WarInfo.TurnController.PlayedCharacter.FactionType == BattlerType.Enemy)
            {
                WarInfo.CharacterPanelController.UpdateEnemyPanel(0, WarInfo.TurnController.PlayedCharacter);
                WarInfo.CharacterPanelController.ShowEnemyPanel(0);
                WarInfo.CharacterPanelController.HideEnemyPanel(1);
                WarInfo.CharacterPanelController.HideEnemyPanel(2);
                WarInfo.CharacterPanelController.HideAllyPanel();

                WarCommandController.SetActive(false);
                StartCoroutine(WaitAISetCommand());
            }
            else if (WarInfo.TurnController.PlayedCharacter.FactionType == BattlerType.Player)
            {
                WarInfo.CharacterPanelController.UpdateAllyPanel(0, WarInfo.TurnController.PlayedCharacter);
                WarInfo.CharacterPanelController.ShowAllyPanel(0);
                WarInfo.CharacterPanelController.HideAllyPanel(1);
                WarInfo.CharacterPanelController.HideAllyPanel(2);
                WarInfo.CharacterPanelController.HideEnemyPanel();

                WarCommandController.WarCommandSelected += SelectCommand;
                WarCommandController.SetActive(true);
            }

            base.StartState();
        }

        public override void EndState()
        {
            base.EndState();
        }

        private void SelectCommand(WarCommand CommandSelected)
        {
            WarCommandController.WarCommandSelected -= SelectCommand;
            if(CommandSelected == WarCommand.EndTurn)
            {
                WarCommandController.SetActive(false);
                WarInfo.TurnController.EndCharacterTurn();
                EndState();
                WarInfo.StateController.ChangeState(WarStateEnum.Command);
            }
            else if (CommandSelected == WarCommand.Move)
            {
                WarCommandController.SetActive(false);
                EndState();
                WarInfo.StateController.ChangeState(WarStateEnum.ChooseMove);
            }
            else if (CommandSelected == WarCommand.Attack)
            {
                WarCommandController.SetActive(false);
                EndState();
                WarInfo.StateController.ChangeState(WarStateEnum.ChooseAttack);
            }
        }

        IEnumerator WaitAISetCommand()
        {
            yield return new WaitForSeconds(1f);
            WarCommand aiCommand = _aiDecisionMaker.SetBasicDecision(WarInfo.TurnController.PlayedCharacter);
            SelectCommand(aiCommand);
        }
    }
}