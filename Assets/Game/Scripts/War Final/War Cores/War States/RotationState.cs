﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;
using TeamUtility.IO;

namespace Legrand.War
{
    public class RotationState : WarState {
        public Transform Arrow;
        public WarCharacter FocusedCharacter;
        private Grid _CharacterGrid;
        private WarDirection _SelectedDirection;

        private bool _AcceptInput;

        public override void StartState () 
	    {
            _AcceptInput = true;
            EventManager.Instance.TriggerEvent(new SetPointerActive(false));

            _CharacterGrid = FocusedCharacter.CurrentGrid.ParentGrid;
            _SelectedDirection = FocusedCharacter.SeeDirection;
            Arrow.gameObject.SetActive(true);
            Arrow.position = new Vector3(_CharacterGrid.transform.position.x, Arrow.position.y, _CharacterGrid.transform.position.z);
            WarInfo.MovementController.RotateObjectToSeeGridNeighbour(Arrow, _CharacterGrid,_SelectedDirection);
            base.StartState ();
	    }

	    public override void UpdateState () 
	    {
		    base.UpdateState ();
            if (InputManager.GetButtonDown("Confirm"))
            {
                WwiseManager.Instance.PlaySFX(WwiseManager.UIFX.Confirm);
                FocusedCharacter.SeeDirection = _SelectedDirection;
                WarInfo.MovementController.RotateCharacter(FocusedCharacter);

                WaitEndOfFrameToEnd(EndState);
                return;
            }

            if (InputManager.GetButtonDown("Cancel"))
            {
                WwiseManager.Instance.PlaySFX(WwiseManager.UIFX.Cancel);
                WaitEndOfFrameToEnd(EndState);
                return;
            }


            float horizontal = LegrandUtility.GetAxisRawHorizontal();

            if (_AcceptInput == false && horizontal == 0f)
                _AcceptInput = true;
            
            if (!_AcceptInput) return;

            if (horizontal > 0)
            {
                _SelectedDirection = WarInfo.MovementController.TryRotate(_SelectedDirection, _CharacterGrid, 1);
                WarInfo.MovementController.RotateObjectToSeeGridNeighbour(Arrow, _CharacterGrid, _SelectedDirection);
                _AcceptInput = false;
            }
            else if (horizontal < 0)
            {
                _SelectedDirection = WarInfo.MovementController.TryRotate(_SelectedDirection, _CharacterGrid, - 1);
                WarInfo.MovementController.RotateObjectToSeeGridNeighbour(Arrow, _CharacterGrid, _SelectedDirection);
                _AcceptInput = false;
            }
	    }

	    public override void EndState () 
	    {
            FocusedCharacter = null;
            Arrow.gameObject.SetActive(false);
            base.EndState ();
	    }
    }
}