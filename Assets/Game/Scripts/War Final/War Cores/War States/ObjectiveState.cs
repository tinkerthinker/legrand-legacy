﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;
using TeamUtility.IO;

namespace Legrand.War
{
public class ObjectiveState : WarState {
	public ObjectivePanel ObjectivePanel;

	public override void StartState () 
	{
		EventManager.Instance.TriggerEvent (new SetPointerActive(false));
            EventManager.Instance.TriggerEvent(new SetInfoActive(false, false));
            WarInfo.TopPanel.Diactivate();
		ObjectivePanel.Open ();
		base.StartState ();
	}

	public override void UpdateState () 
	{
		base.UpdateState ();
		if (InputManager.GetButtonDown ("Confirm") || InputManager.GetButton ("Cancel")) 
		{
			WwiseManager.Instance.PlaySFX (WwiseManager.UIFX.Confirm);
			WaitEndOfFrameToEnd (EndState);
		}
	}

	public override void EndState () 
	{
		ObjectivePanel.Close ();
		base.EndState ();
	}
}
}