﻿using UnityEngine;
using System.Collections;

namespace Legrand.War
{
	public class StateController : MonoBehaviour {

		public WarStateEnum FirstState;

		public WarStateEnumPair[] WarStates;

		public WarStateEnumPair CurrentState;

		void OnEnable()
		{
			WarInfo.StateController = this;
		}

		void OnDisable()
		{
			WarInfo.StateController = null;
		}

		public void StartController()
		{
			ChangeState (FirstState);
		}

		public void StartState()
		{
			CurrentState.State.StartState ();
		}

		void Update()
		{
			if (CurrentState.State != null)
				CurrentState.State.UpdateState ();
		}

		public void ChangeState(WarStateEnum newState, bool StartImmediate = true)
		{
			WarStateEnumPair newWarStatePair =  System.Array.Find (WarStates, x => x.StateEnum == newState);
			if (newWarStatePair.State != null) 
			{
				CurrentState = newWarStatePair;
				CurrentState.State.CleanTrigger ();

				if(StartImmediate) StartState ();
			}
		}

	}
}