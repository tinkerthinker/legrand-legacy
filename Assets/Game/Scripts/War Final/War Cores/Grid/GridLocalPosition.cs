﻿using UnityEngine;
using System.Collections;

namespace Legrand.War
{
	[System.Serializable]
	public class GridLocalPosition
	{
		public Grid ParentGrid;
		public Vector3 Location;
		public WarCharacter CharacterInside;

		public GridLocalPosition(Vector3 location, Grid parentGrid)
		{
			ParentGrid = parentGrid;
			Location = location;
		}

		public void FillCharacter(WarCharacter character)
		{
			this.CharacterInside = character;
			this.CharacterInside.CurrentGrid = this;
		}

		public bool RemoveCharacter(WarCharacter character)
		{
			if (CharacterInside == character) 
			{
				CharacterInside.CurrentGrid = null;
				CharacterInside = null;
				return true;
			}
			return false;
		}

		public bool IsEmpty()
		{
			return CharacterInside == null;
		}
	}
}