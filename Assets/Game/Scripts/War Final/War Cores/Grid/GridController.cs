﻿using UnityEngine;
using System.Collections;

namespace Legrand.War
{
	public class GridController : MonoBehaviour {
		[ContextMenuItem("Fill Grid", "FillGrid")]
		public Grid[] Grids;

		void OnEnable()
		{
			WarInfo.GridController = this;
		}

		void OnDisable()
		{
			WarInfo.GridController = null;
		}

		public Grid GetGrid(string gridID)
		{
			return System.Array.Find (Grids, x=>x.ID == gridID);
		}

		public bool SpawnCharacterAt(WarCharacter spawnedCharacter, string gridID, WarDirection direction = WarDirection.Random)
		{
			Grid grid = System.Array.Find (Grids, x=>x.ID == gridID);
			if (grid == null)
				return false;
			return grid.MoveCharacterToThis (spawnedCharacter, direction);
		}

		public bool RemoveCharacterAt(WarCharacter spawnedCharacter, string gridID)
		{
			Grid grid = System.Array.Find (Grids, x=>x.ID == gridID);
			if (grid == null)
				return false;
			return grid.RemoveCharacter (spawnedCharacter);
		}

		private void FillGrid()
		{
			Grids = this.GetComponentsInChildren<Grid> ();
		}
	}
}