﻿using UnityEngine;
using System.Collections;

#if UNITY_EDITOR
using UnityEditor;

namespace Legrand.War
{
    [CustomEditor(typeof(Grid))]
    public class GridEditor : Editor
    {
        void OnSceneGUI()
        {
            Grid t = target as Grid;

            Handles.color = Color.black;
            Handles.Label(t.transform.position + Vector3.up * 1, t.ID);

            Handles.color = new Color(1, 1, 1, 0.2f);
            Handles.DrawWireArc(t.transform.position, t.transform.up, -t.transform.right,
        180, 5);
            
        }
    }
}
#endif