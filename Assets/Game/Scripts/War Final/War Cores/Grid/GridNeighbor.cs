﻿using UnityEngine;
using System.Collections;

namespace Legrand.War
{
    [System.Serializable]
    public struct GridNeighbor
    {
        public Grid NeighborGrid;
        public GridConnection Line;
        public WarDirection Direction;

        public GridNeighbor(Grid neighborGrid, GridConnection line, WarDirection direction)
        {
            NeighborGrid = neighborGrid;
            Line = line;
            Direction = direction;
        }
    }
}
