﻿using UnityEngine;
using System.Collections;

namespace Legrand.War
{
    public class GridConnection : MonoBehaviour
    {
        public string ID;
        public Grid FirstGrid;
        public Grid SecondGrid;
        public ConnectionStatus RoadStatus;

        public Material RoadMaterial;
        public Material RoadGreenMaterial;
        public Material RoadRedMaterial;
        public Material HiddenMaterial;
        public Material HiddenGreenMaterial;
        public Material HiddenRedMaterial;

        void Start()
        {
            if(RoadStatus == ConnectionStatus.Destroyed)
            {
                this.GetComponent<Renderer>().material = null;
            }
            else if(RoadStatus == ConnectionStatus.Hidden)
            {
                this.GetComponent<Renderer>().material = HiddenMaterial;
            }
            else
            {
                this.GetComponent<Renderer>().material = RoadMaterial;
            }
        }

        void SetRoadStatus(ConnectionStatus newRoadStatus)
        {
            RoadStatus = newRoadStatus;
            if (RoadStatus == ConnectionStatus.Destroyed)
            {
                this.GetComponent<Renderer>().material = null;
            }
            else if (RoadStatus == ConnectionStatus.Hidden)
            {
                this.GetComponent<Renderer>().material = HiddenMaterial;
            }
            else
            {
                this.GetComponent<Renderer>().material = RoadMaterial;
            }
        }

        void RevealHiddenPath(bool canSee)
        {
            if (RoadStatus == ConnectionStatus.Hidden)
            {
                if(canSee)
                    this.GetComponent<Renderer>().material = HiddenMaterial;
                else
                    this.GetComponent<Renderer>().material = null;
            }
        }

        public void BuildGrid()
        {
            FirstGrid.Neighbors.Add(new GridNeighbor(SecondGrid, this, WarDirection.North));
            SecondGrid.Neighbors.Add(new GridNeighbor(FirstGrid, this, WarDirection.North));
        }

        public void SetToRed()
        {
            if (RoadStatus == ConnectionStatus.Normal)
            {
                this.GetComponent<Renderer>().material = RoadRedMaterial;
            }
            if (RoadStatus == ConnectionStatus.Hidden)
            {
                this.GetComponent<Renderer>().material = HiddenRedMaterial;
            }
        }

        public void SetToGreen()
        {
            if (RoadStatus == ConnectionStatus.Normal)
            {
                this.GetComponent<Renderer>().material = RoadGreenMaterial;
            }
            if (RoadStatus == ConnectionStatus.Hidden)
            {
                this.GetComponent<Renderer>().material = HiddenGreenMaterial;
            }
        }

        public void SetToNormal()
        {
            if (RoadStatus == ConnectionStatus.Normal)
            {
                this.GetComponent<Renderer>().material = RoadMaterial;
            }
            if (RoadStatus == ConnectionStatus.Hidden)
            {
                this.GetComponent<Renderer>().material = HiddenMaterial;
            }
        }
    }
}
