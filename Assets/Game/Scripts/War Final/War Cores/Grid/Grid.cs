﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Legrand.core;

namespace Legrand.War
{
	public class Grid : MonoBehaviour,ISelectable
    {
        public string ID;
        public string Name;
        public List<GridNeighbor> Neighbors;

		public GridLocalPosition[] LocalGrid;

        public GameObject GridTarget;
        private Sprite _GridImage;

		private int _MaxTroop = 3;

        void Awake()
        {
            _GridImage = this.GetComponent<Sprite>();
			LocalGrid = new GridLocalPosition [_MaxTroop];
			LocalGrid [0] = new GridLocalPosition (this.transform.position, this);
			LocalGrid [1] = new GridLocalPosition (this.transform.position + new Vector3(0.5f,0f,0.7f), this);
			LocalGrid [2] = new GridLocalPosition (this.transform.position + new Vector3(-0.5f,0f,-0.7f), this);
            MarkForAttack(false);
        }

        void ChangeField()
        {

        }

		public RetrieveableInfo RetrieveInfo()
		{
            RetrieveableInfo r;
            r.Character = null;
            r.Grid = this;
            return r;
        }

		public GridLocalPosition SeachFillable(WarCharacter character)
		{
			GridLocalPosition fillableGrid = null;
			bool hasAnotherFaction = false;
			for(int i=0;i<LocalGrid.Length && hasAnotherFaction == false;i++)
			{
				if (LocalGrid [i].IsEmpty ()) {
					if (fillableGrid == null)
						fillableGrid = LocalGrid [i];
				}
				else 
				{
					hasAnotherFaction = LocalGrid [i].CharacterInside.FactionType != character.FactionType;
                    if (hasAnotherFaction) fillableGrid = null;
				}
			}
			return fillableGrid;
		}

		public bool MoveCharacterToThis(WarCharacter character, WarDirection direction)
		{
			// Hanya menambah informasi ke dala grid bahwa player sudah pindah. Control pergerakannya ada pada kelas MovementController.
			if (character.CurrentGrid != null && character.CurrentGrid.ParentGrid == this)
				return false; 

			GridLocalPosition emptyGrid = SeachFillable (character);
			if(emptyGrid == null) return false;

			if (character.CurrentGrid != null) 
				character.CurrentGrid.RemoveCharacter (character);
			emptyGrid.FillCharacter (character);
			character.SeeDirection = SearchNearbyDirection (direction);
			return true;
		}

		public WarDirection SearchNearbyDirection(WarDirection currentDirection, int clockwise = 0, WarDirection[] ignoredDirection = null)
		{
			// Mendapatkan posisi terdekat sesuai dengan arah pandah karakter yang sekarang.
			// Kalau dalam grid ini tidak ada tetangga yang arahnya sama dengan arah pandang karakter maka dicari yang terdekatnya.
			if (currentDirection == WarDirection.Random) 
			{
				return Neighbors[LegrandUtility.Random (0, Neighbors.Count)].Direction;
			}

			WarDirection[] directionCandidates = new WarDirection[Neighbors.Count];
			for (int i = 0; i < Neighbors.Count; i++) 
				directionCandidates [i] = Neighbors [i].Direction;
        
			return DirectionUtility.ClosestDirection (currentDirection, directionCandidates, clockwise, ignoredDirection);
		}

        public GridNeighbor GetCharacterMovementNeighbour(WarCharacter movedCharacter, WarDirection inputDirection, WarDirection[] maskedDirection = null)
        {
            // This funtion is try to search the visible movement from their grid. Player will input direction and system will it's neighbour
            List<WarDirection> directionCandidates = new List<WarDirection>();
            for (int i = 0; i < Neighbors.Count; i++)
            {
                if (maskedDirection != null && Array.Exists(maskedDirection, x => x == Neighbors[i].Direction) && movedCharacter.IsVisibleToMove(Neighbors[i].Line))
                {
                    directionCandidates.Add(Neighbors[i].Direction);
                }
            }

            WarDirection w = DirectionUtility.ClosestDirection(inputDirection, directionCandidates.ToArray());
            if (w == WarDirection.None) return new GridNeighbor();

            return Neighbors.Find(x => x.Direction == w);
        }

        public GridNeighbor GetCharacterAttackNeighbour(WarCharacter movedCharacter, WarDirection inputDirection, WarDirection[] maskedDirection = null)
        {
            // This funtion is try to search the visible movement from their grid. Player will input direction and system will it's neighbour
            List<WarDirection> directionCandidates = new List<WarDirection>();
            for (int i = 0; i < Neighbors.Count; i++)
            {
                if (maskedDirection != null && Array.Exists(maskedDirection, x => x == Neighbors[i].Direction) && movedCharacter.IsVisibleToAttack(Neighbors[i].Line))
                {
                    directionCandidates.Add(Neighbors[i].Direction);
                }
            }

            WarDirection w = DirectionUtility.ClosestDirection(inputDirection, directionCandidates.ToArray());
            if (w == WarDirection.None) return new GridNeighbor();

            return Neighbors.Find(x => x.Direction == w);
        }

        public Vector3 GetDirectionPosition(WarDirection currentDirection)
		{
			// Mendapatkan posisi grid tetangga sesuai arah direction agar character tau melihat world posistion yang mana
			GridNeighbor neighbour = Neighbors.Find (x=>x.Direction == currentDirection);
			return neighbour.NeighborGrid.transform.position;
		}

		public bool RemoveCharacter(WarCharacter character)
		{
			for (int i = 0; i < LocalGrid.Length; i++) 
			{
				if(LocalGrid [i].RemoveCharacter (character)) return true;
			}
			return false;
		}

        public void MarkForAttack(bool isMarked)
        {
            GridTarget.SetActive(isMarked);
        }

        public bool TryToAttack(WarCharacter attackingCharacter)
        {
            return Array.Exists(LocalGrid, x=> x.CharacterInside != null && x.CharacterInside.FactionType != attackingCharacter.FactionType);
        }

        public List<WarCharacter> GetCharacterInsideGrid()
        {
            List<WarCharacter> characterInside = new List<WarCharacter>();
            for(int i=0; i < LocalGrid.Length; i++)
            {
                if (!LocalGrid[i].IsEmpty()) characterInside.Add(LocalGrid[i].CharacterInside);
            }
            return characterInside;
        }
    }
}
