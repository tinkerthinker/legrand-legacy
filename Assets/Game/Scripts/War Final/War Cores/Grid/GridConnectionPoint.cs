﻿using UnityEngine;
using System.Collections;

namespace Legrand.War
{
    [ExecuteInEditMode]
    public class GridConnectionPoint : MonoBehaviour
    {
        GridConnection _Line;

        void OnEnable()
        {
            _Line = GetComponentInParent<GridConnection>();
        }

        void OnTriggerEnter(Collider c)
        {
            if (_Line.FirstGrid != null && _Line.SecondGrid != null) return;

            if (c.tag == "WarGrid")
            {
                if (this.gameObject.name == "In") _Line.FirstGrid = c.gameObject.GetComponent<Grid>();
                else _Line.SecondGrid = c.gameObject.GetComponent<Grid>();
            }

            if (_Line.FirstGrid != null && _Line.SecondGrid != null) _Line.BuildGrid();

        }

        void OnTriggerExit(Collider c)
        {
            if (c.tag == "WarGrid")
            {
                if (this.gameObject.name == "In") _Line.FirstGrid = null;
                else _Line.SecondGrid = null;
            }
        }
    }
}
