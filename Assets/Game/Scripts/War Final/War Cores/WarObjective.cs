﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Legrand.War
{
	[System.Serializable]
	public class WarObjective{
		public string ObjectiveID;
		public string Description;
		public WarObjectiveEnum ObjectiveType;
		public CriterionStatic CertainActor;
		public CriterionStatic Target;

		public ObjectiveStatus ObjectiveStatus;


		public ObjectiveStatus IsFinished()
		{
			ObjectiveStatus status = ObjectiveStatus.OnGoing;

			if (ObjectiveType == WarObjectiveEnum.KillEveryone && WarInfo.WarParticipant.ParticipantCondition.IsAllEnemyKilled ()) 
			{
				status = ObjectiveStatus.Success;
			}
			else if (ObjectiveType == WarObjectiveEnum.Kills && Target.Compare (WarInfo.WarParticipant.ParticipantCondition.CounterEnemyKilled ())) 
			{
				status = ObjectiveStatus.Success;
			}
			else if (ObjectiveType == WarObjectiveEnum.KillSpecific) 
			{
				bool found = false;
				for (int i = 0; i < WarInfo.WarParticipant.ParticipantCondition.KilledEnemy.Count && !found; i++) 
				{
					if(Target.Compare (WarInfo.WarParticipant.ParticipantCondition.KilledEnemy[i].HashID))
					{
						status = ObjectiveStatus.Success;	found = true	;
					}
				}
			}

			ObjectiveStatus = status;
			return status;
		}
	}



	public enum WarObjectiveEnum
	{
		KillEveryone,
		Kills,
		KillSpecific,
		Protects,
		ProtectSpecific,
		GoTo,
		Prevent
	}

	public enum ObjectiveStatus
	{
		OnGoing,
		Failed,
		Success
	}
}