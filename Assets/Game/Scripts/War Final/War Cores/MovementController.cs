﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

namespace Legrand.War
{
	public class MovementController : MonoBehaviour {
        public delegate void MoveDone();
        public MoveDone MoveDoneEvent;

		void OnEnable()
		{
			WarInfo.MovementController = this;
		}

		void OnDisable()
		{
			WarInfo.MovementController = null;
		}

		public void MoveCharacterPosition(WarCharacter movedCharacter, Grid grid)
		{
            grid.MoveCharacterToThis(movedCharacter, movedCharacter.SeeDirection);
            Vector3 targetPosition = new Vector3(movedCharacter.CurrentGrid.Location.x, movedCharacter.transform.position.y, movedCharacter.CurrentGrid.Location.z);
            movedCharacter.transform.DOJump(targetPosition, 3, 1, 1f).OnComplete(DoneMove);
		}

        private void DoneMove()
        {
            if (MoveDoneEvent != null) MoveDoneEvent();
        }

		public void MoveCharacterPosition(WarCharacter movedCharacter)
		{
			movedCharacter.transform.position = new Vector3(movedCharacter.CurrentGrid.Location.x,movedCharacter.transform.position.y,movedCharacter.CurrentGrid.Location.z);
            RotateCharacter(movedCharacter);
		}

		public WarDirection TryRotate(WarDirection sourceDirection, Grid grid, int direction)
		{
			return grid.SearchNearbyDirection(sourceDirection, direction);
		}

        public void RotateCharacter(WarCharacter movedCharacter)
		{
            RotateObjectToSeeGridNeighbour(movedCharacter.transform, movedCharacter.CurrentGrid.ParentGrid, movedCharacter.SeeDirection);
        }

        public void RotateObjectToSeeGridNeighbour(Transform gameobject, Grid currentGrid, WarDirection direction)
        {
            gameobject.LookAt(currentGrid.GetDirectionPosition(direction));
            gameobject.eulerAngles = new Vector3(0f, gameobject.eulerAngles.y, 0f);
        }
    }
}