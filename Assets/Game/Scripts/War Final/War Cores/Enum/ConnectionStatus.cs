﻿using UnityEngine;
using System.Collections;

namespace Legrand.War
{
    public enum ConnectionStatus
    {
        Normal,
        Hidden,
        Destroyed
    }
}
