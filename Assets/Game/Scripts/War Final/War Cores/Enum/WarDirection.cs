﻿using UnityEngine;
using System.Collections;

public enum WarDirection
{
    North,
    NorthEast,
    East,
    SouthEast,
    South,
    SouthWest,
    West,
    NorthWest,
	Random,
    None
}

public class DirectionUtility
{
	public static WarDirection ClosestDirection(WarDirection sourceDirection, WarDirection[] TargetDirections, int clockwise, WarDirection[] ignoredDirection = null)
	{
		WarDirection closestDirection = sourceDirection;
		int closestDistance = int.MaxValue;

		if (clockwise == 1) {
			for (int i = 0; i < TargetDirections.Length; i++) {
                if (ignoredDirection != null && System.Array.Exists<WarDirection>(ignoredDirection, x => x == TargetDirections[i])) continue;

				int distance = SearchClockwise (sourceDirection, TargetDirections [i]);

				if (distance < closestDistance) {
					closestDirection = TargetDirections [i];
					closestDistance = distance;
				}
			}
		} else if (clockwise == -1) {
			for (int i = 0; i < TargetDirections.Length; i++) {
                if (ignoredDirection != null && System.Array.Exists<WarDirection>(ignoredDirection, x => x == TargetDirections[i])) continue;

                int distance = SearchAntiClockwise (sourceDirection, TargetDirections [i]);

				if (distance < closestDistance) {
					closestDirection = TargetDirections [i];
					closestDistance = distance;
				}
			}
		} 
		else
		{
			for (int i = 0; i < TargetDirections.Length; i++) 
			{
                if (ignoredDirection != null && System.Array.Exists<WarDirection>(ignoredDirection, x => x == TargetDirections[i])) continue;

                int distance1 = SearchClockwise (sourceDirection, TargetDirections [i]);
				int distance2 = SearchAntiClockwise (sourceDirection, TargetDirections [i]);
				int distance = Mathf.Min (distance1, distance2);
				if (distance == 0)
					return TargetDirections [i];

				if (distance < closestDistance) 
				{
					closestDirection = TargetDirections [i];
					closestDistance = distance;
				}
			}
		}

		return closestDirection;
	}

    public static WarDirection ClosestDirection(WarDirection sourceDirection, WarDirection[] TargetDirections)
    {
        WarDirection closestDirection = WarDirection.None;
        int closestDistance = int.MaxValue;
        
        for (int i = 0; i < TargetDirections.Length; i++)
        {
            if (sourceDirection == TargetDirections[i]) return TargetDirections[i];

                int distance1 = SearchClockwise(sourceDirection, TargetDirections[i]);
                int distance2 = SearchAntiClockwise(sourceDirection, TargetDirections[i]);
                int distance = Mathf.Min(distance1, distance2);
                if (distance == 0)
                    return TargetDirections[i];

                if (distance < closestDistance)
                {
                    closestDirection = TargetDirections[i];
                    closestDistance = distance;
                }
        }

        return closestDirection;
    }

    private static int SearchAntiClockwise(WarDirection sourceDirection,WarDirection TargetDirection)
	{
		int i = 1;
		bool found = false;
		while (!found) 
		{
			int integerDirection = (int)sourceDirection - i;

			if (integerDirection < 0) 
				integerDirection = 8 + integerDirection;

			if ((int)TargetDirection == integerDirection)
				found = true;
			else
				i++;
		}
		return Mathf.Abs(i);
	}

	private static int SearchClockwise(WarDirection sourceDirection,WarDirection TargetDirection)
	{
		int i = 1;
		bool found = false;
		while (!found) 
		{
			int integerDirection = (int)sourceDirection + i;
			integerDirection = integerDirection % 8;
			if ((int)TargetDirection == integerDirection)
				found = true;
			else
				i++;
		}
		return i;
	}

    public static WarDirection[] GetDirectionNeighbour(WarDirection sourceDirection)
    {
        // Butuh cepat jadi caranya nya jelek pisan
        if (sourceDirection == WarDirection.North) return new WarDirection[3] { WarDirection.North, WarDirection.NorthEast,WarDirection.NorthWest };
        else if (sourceDirection == WarDirection.NorthEast) return new WarDirection[3] { WarDirection.North, WarDirection.NorthEast, WarDirection.NorthEast };
        else if (sourceDirection == WarDirection.East) return new WarDirection[3] { WarDirection.NorthEast, WarDirection.East, WarDirection.SouthEast };
        else if (sourceDirection == WarDirection.SouthEast) return new WarDirection[3] { WarDirection.East, WarDirection.SouthEast, WarDirection.South };
        else if (sourceDirection == WarDirection.South) return new WarDirection[3] { WarDirection.SouthEast, WarDirection.South, WarDirection.SouthWest };
        else if (sourceDirection == WarDirection.SouthWest) return new WarDirection[3] { WarDirection.South, WarDirection.SouthWest, WarDirection.West };
        else if (sourceDirection == WarDirection.West) return new WarDirection[3] { WarDirection.SouthWest, WarDirection.West, WarDirection.NorthWest };
        else return new WarDirection[3] { WarDirection.West, WarDirection.NorthWest, WarDirection.North };
    }
}