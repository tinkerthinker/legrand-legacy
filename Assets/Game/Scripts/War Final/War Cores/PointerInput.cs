﻿using UnityEngine;
using System.Collections;
using System;
using TeamUtility.IO;

namespace Legrand.War
{
	public class PointerInput : MonoBehaviour {
		public GameObject Pointer;
		public float InputSpeedModifier = 5f;


		[SerializeField]
		private bool _IsActive;
		[SerializeField]
		private Transform _FocusedTarget;
        private RetrieveableInfo CurrentSelectedInfo;


        private ObjectFinder _ObjectFinder;

        [SerializeField]
        private bool _GridInfoActive;
        [SerializeField]
        private bool _CharacterInfoActive;

        void Awake()
		{
			_ObjectFinder = new ObjectFinder ();
			EnableEvent ();
		}

		void OnDestroy()
		{
			DisableEvent ();
		}

		void EnableEvent()
		{
			if (EventManager.Instance) 
			{
				EventManager.Instance.AddListener<SetPointerActive> (SetActivePointer);
				EventManager.Instance.AddListener<ChangeCameraFocus> (ChangeFocus);
                EventManager.Instance.AddListener<SetInfoActive>(SetActiveInfo);
            }
		}

		void DisableEvent()
		{
			if (EventManager.Instance) 
			{
				EventManager.Instance.RemoveListener<SetPointerActive> (SetActivePointer);
				EventManager.Instance.RemoveListener<ChangeCameraFocus> (ChangeFocus);
                EventManager.Instance.RemoveListener<SetInfoActive>(SetActiveInfo);
            }
		}

		// Update is called once per frame
		void Update () {
			if (!_IsActive) 
			{
				MoveToFocus (_FocusedTarget);
				return;
			}


			Vector3 input = new Vector3 (InputManager.GetAxis ("Horizontal"), 0f, InputManager.GetAxis ("Vertical")); 

			if (Vector3.Distance (input, Vector3.zero) == 0f) {
				if (_FocusedTarget == null)
					SearchNearbyFocuseable ();
				MoveToFocus (_FocusedTarget);
				return;
			} else
				ChangeFocus (null);
			
			float finalSpeed = InputSpeedModifier;
			if (InputManager.GetKey (KeyCode.LeftShift))
				finalSpeed *= 2f;
			
			this.transform.Translate (input * Time.deltaTime * finalSpeed);
		}

		void OnTriggerEnter(Collider c)
		{
			ISelectable selectable = c.GetComponent<ISelectable> ();
			if (selectable != null) 
			{
				RetrieveableInfo r = selectable.RetrieveInfo();
                if (r.Character != null) CurrentSelectedInfo.Character = r.Character;
                else if(r.Grid != null) CurrentSelectedInfo.Grid = r.Grid;
                ChangeInfo();
            }
		}

		void OnTriggerExit(Collider c)
		{
			if (CurrentSelectedInfo.Character == null && CurrentSelectedInfo.Grid == null)
				return;

            if ((CurrentSelectedInfo.Character != null &&CurrentSelectedInfo.Character.transform == c.transform) || (CurrentSelectedInfo.Grid != null && CurrentSelectedInfo.Grid.transform == c.transform))
            {
                ISelectable selectable = c.GetComponent<ISelectable>();
                RetrieveableInfo r = selectable.RetrieveInfo();
                if (r.Character != null) CurrentSelectedInfo.Character = null;
                else if (r.Grid != null) CurrentSelectedInfo.Grid = null;
                ChangeInfo();
            }
		}

		void SearchNearbyFocuseable()
		{
			_FocusedTarget = _ObjectFinder.FindNearestObject (this.transform, "War Object");
		}

		void ChangeFocus(ChangeCameraFocus newFocus)
		{
			_FocusedTarget = newFocus == null ? null : newFocus.NewFocus;
		}

		void MoveToFocus(Transform focusedObject)
		{
			if (focusedObject != null) {
				Vector3 newPosition = Vector3.Lerp (this.transform.position, focusedObject.position, Time.deltaTime * InputSpeedModifier);
				this.transform.position = new Vector3(newPosition.x, this.transform.position.y, newPosition.z);
			}
		}

		void SetInputActive(bool active)
		{
			_IsActive = active;
		}

		void SetPointerActive(bool active)
		{
			Pointer.SetActive(active);
		}

		void SetActivePointer(SetPointerActive e)
		{
			SetInputActive (e.InputActive);
			SetPointerActive (e.PointerActive);
		}

        void SetActiveInfo(SetInfoActive e)
        {
            SetActiveGridInfo(e.GridInfoActive);
            SetActiveCharacterInfo(e.CharacterInfoActive);
            ChangeInfo(e.AutomaticChangeValue);
        }

        void SetActiveGridInfo(bool active)
        {
            _GridInfoActive = active;
        }

        void SetActiveCharacterInfo(bool active)
        {
            _CharacterInfoActive = active;
        }

        public void ChangeInfo(bool changeValue = false)
        {
            if (_CharacterInfoActive)
            {
                if(CurrentSelectedInfo.Character != null)
                {
                    if (CurrentSelectedInfo.Character.FactionType == BattlerType.Player)
                    {
                        WarInfo.CharacterPanelController.UpdateAllyPanel(0, CurrentSelectedInfo.Character);
                        WarInfo.CharacterPanelController.ShowAllyPanel();

                        WarInfo.CharacterPanelController.HideEnemyPanel();
                    }
                    else
                    {
                        WarInfo.CharacterPanelController.UpdateEnemyPanel(0, CurrentSelectedInfo.Character);
                        WarInfo.CharacterPanelController.ShowEnemyPanel();

                        WarInfo.CharacterPanelController.HideAllyPanel();
                    }
                }
                else
                {
                    WarInfo.CharacterPanelController.HideAllyPanel();
                    WarInfo.CharacterPanelController.HideEnemyPanel();
                }
            }
            else
            {
                if (changeValue)
                {
                    WarInfo.CharacterPanelController.HideAllyPanel();
                    WarInfo.CharacterPanelController.HideEnemyPanel();
                }
            }

            if (_GridInfoActive)
            {
                string newGridName = CurrentSelectedInfo.Character != null ? CurrentSelectedInfo.Character.CurrentGrid.ParentGrid.Name : string.Empty;
                if(newGridName == string.Empty) newGridName = CurrentSelectedInfo.Grid != null ? CurrentSelectedInfo.Grid.Name : string.Empty;
                WarInfo.TopPanel.SetFieldName(newGridName);
            }
            else
            {
                WarInfo.TopPanel.SetFieldName(string.Empty);
            }
        }
    }
}