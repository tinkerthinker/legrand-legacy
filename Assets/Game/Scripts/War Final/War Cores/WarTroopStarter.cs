﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Legrand.War
{
	[System.Serializable]
	public class WarTroopStarter{
        public string AllyTroopName = "";
        public string EnemyTroopName = "";

        [Range(-1f,1f)]
        public float MoralBalance = 0f;

		public GridWithNavigation[] AllyFillableGrid;
		public string[] EnemyFillableGridID;

		public TroopStarter[] AllyStarter;
		public TroopStarter[] EnemyStarter;
	}

	[System.Serializable]
	public class TroopStarter
	{
		public string TroopUniqueID;
		public string GridID;
		public WarDirection Direction;

		public TroopStarter()
		{
			TroopUniqueID = "";
			GridID = "";
			Direction = WarDirection.Random;
		}
	}

	[System.Serializable]
	public class GridWithNavigation
	{
		public string GridID;
		public string UpInputGridID;
		public string RightInputGridID;
		public string DownInputGridID;
		public string LeftInputGridID;
	}
}