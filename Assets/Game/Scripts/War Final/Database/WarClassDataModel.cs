﻿using UnityEngine;
using System.Collections;
using Legrand.War;

[System.Serializable]
public class WarClassDataModel{
    public WarClass Class;

    public int HP;
    public int STR;
    public int DEF;
    public int AGI;
    public int Troops;

    public int AttackRange;
    public bool CanAttackHidden;
    public bool CanMoveHidden;

    public WarClassDataModel()
    {
        Class = WarClass.Mercenary;
        HP = 100;
        STR = 1;
        DEF = 1;
        AGI = 1;
        Troops = 100;
        AttackRange = 1;
        CanAttackHidden = false;
        CanMoveHidden = false;
    }
}
