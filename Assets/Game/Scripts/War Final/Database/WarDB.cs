﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Legrand.War;

public class WarDB : MonoBehaviour {
    public static WarDB _Instance;

    void Awake()
    {
        _Instance = this;
    }

    public List<WarCharacterDataModel> WarCharacterDataModels;
    public List<WarClassDataModel> WarClassDataModels;
    public List<WarCommandCost> WarCommandCostModels;
}
